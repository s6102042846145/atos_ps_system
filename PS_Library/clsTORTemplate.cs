﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsTORTemplate
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private DbControllerBase db = new DbControllerBase();
     
        public clsTORTemplate() { }

        public void insertTORT (string xCode, string temp_name,string temp_desc,string is_active,string strUserLogin)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";

                sql = "select * from ps_m_tor_template where tor_temp_code ='" + xCode + "'";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
                if (dt.Rows.Count > 0)
                {
                    sql = "update ps_m_tor_template set tor_temp_name='" + temp_name + "',tor_temp_desc='" + temp_desc + "',is_active=" + is_active + ",updated_datetime=GETDATE() where tor_temp_code='" + xCode + "'";
                }
                else
                {
                    sql = @"insert into ps_m_tor_template (tor_temp_code,tor_temp_name,tor_temp_desc,is_active,created_by,created_datetime,updated_by,updated_datetime)
             values  ('" + xCode + @"'
           , '" + temp_name + @"'
           , '" + temp_desc + @"'
           , '" + is_active + @"'
           , '" + strUserLogin + @"'
           , GETDATE()
            , '" + strUserLogin + @"'
           , GETDATE())";

                }
                db.ExecNonQuery(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            

        }
        public string insertBidDoc(string section_name, string article_name, string doctype_code, string strUserLogin, string hidCode,string vol, string format, string stamp)
        {
            DataTable dt = new DataTable();
            string xCode = "";
            try
            {
           
                if (hidCode == "") xCode = GetTORCode();
                else xCode = hidCode;

                string sql = "";

                sql = "select  * from ps_m_bid_document where tor_temp_code ='" + xCode + "' and vol_no='" + vol + "' and section_name= '" + section_name + "' and article_name ='" + article_name + "'";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
                if (dt.Rows.Count > 0)
                {
                    sql = "delete from ps_m_bid_document where tor_temp_code ='" + xCode + "' and vol_no='" + vol + "' and section_name= '" + section_name + "' and article_name ='" + article_name + "' and doctype_code =''";
                    dt = db.ExecSql_DataTable(sql, dbConnectionString);
                }

                sql = "select  * from ps_m_bid_document where tor_temp_code ='" + xCode + "' and vol_no='" + vol + "' and section_name= '" + section_name + "' and article_name ='" + article_name + "' and doctype_code ='" + doctype_code + "'";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
                if (dt.Rows.Count > 0)
                {

                }
                else
                {
                    sql = @"insert into ps_m_bid_document (tor_temp_code
      ,vol_no
      ,section_name
      ,article_name
      ,doctype_code
      ,page_no_format
      ,is_stamp_bidno
      ,created_by
      ,created_datetime
      ,updated_by
      ,updated_datetime)
          values ('" + xCode + @"'
           , '" + vol + @"'
           , '" + section_name + @"'
           , '" + article_name + @"'
           , '" + doctype_code + @"'
           , '" + format + @"'
           , '" + stamp + @"'
           , '" + strUserLogin + @"'
           , GETDATE()
            , '" + strUserLogin + @"'
           , GETDATE())";

                    db.ExecNonQuery(sql, dbConnectionString);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
           
            return xCode;

        }
        public DataTable sel_Doctype()
        {
            DataTable dt = new DataTable();
            try
            {
               
                string sql = "select * from ps_m_doctype order by doctype_code";

                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable sel_BidDoc(string xCode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select *,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = ps_m_bid_document.doctype_code) as doctype_name from ps_m_bid_document where tor_temp_code='" + xCode + "' order by row_id";

                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            
            return dt;
        }
        public DataTable Dissel_BidDoc(string xCode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select distinct vol_no,tor_temp_code from ps_m_bid_document where tor_temp_code='" + xCode + "'";

                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
           
            return dt;
        }
        public DataTable sel_TOR(string search,string status)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (search != "" && status != "")
                {
                    sql = "select * from ps_m_tor_template where tor_temp_name like '%" + search + "%' and is_active in (" + status + ")";
                }
                else if (search != "")
                {
                    sql = "select * from ps_m_tor_template where tor_temp_name like '%" + search + "%'";
                }
                else if (status != "")
                {
                    sql = "select * from ps_m_tor_template where is_active in (" + status + ")";
                }
                else
                {
                    sql = "select * from ps_m_tor_template";
                }
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            
            return dt;
        }
        private string GetTORCode()
        {
            string xCode = "";
            try
            {
                int xRunno = 0;

                string sql = " select * from wf_runno where prefix = 'TOR' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                DataTable dt = db.ExecSql_DataTable(sql, dbConnectionString);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    xRunno = int.Parse(dr["runno"].ToString());
                    xRunno++;
                    sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'TOR' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                    db.ExecNonQuery(sql, dbConnectionString);
                }
                else
                {
                    xRunno++;
                    sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('TOR', " + xRunno + ", " + (DateTime.Now.Year + 543).ToString() + ")  ";
                    db.ExecNonQuery(sql, dbConnectionString);
                }
                xCode = "TOR-" + (DateTime.Now.Year + 543).ToString() + xRunno.ToString("000000");
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
           
            return xCode;
        }
    }

}
