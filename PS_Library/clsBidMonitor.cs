﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsBidMonitor
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string db_common = ConfigurationManager.AppSettings["db_common"].ToString();
        public string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();
        DbControllerBase db = new DbControllerBase();
        static oraDBUtil oraDBUtil = new oraDBUtil();
        public clsBidMonitor() { }

        public DataTable getDept(string bidno,string rev)
        {
            DataTable dt = new DataTable();
            string sql = @"select distinct tj.depart_position_name,tb.status
                     ,(CASE WHEN tb.status ='NEW' THEN '1'  WHEN tb.status ='ASSIGNED' THEN '2' WHEN tb.status ='WF in progress' THEN '3' WHEN tb.status ='FINISHED' THEN '4'
                     ELSE tb.status END) as statusorder
	                ,(select order_no from ps_m_department de where de.depart_code = tj.depart_position_name) as order_no
                    from ps_t_job_dashboard tj
                    left join ps_t_bid_dashboard tb
                    on tj.depart_position_name = tb.depart_position_name
                    where bid_no='" + bidno + "' and bid_revision='" + rev + "'";
            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
        public DataTable getDeptKor(string bidno, string rev)
        {
            DataTable dt = new DataTable();
            string sql = @"	select *,(CASE WHEN tb.status ='NEW' THEN '1'  WHEN tb.status ='ASSIGNED' THEN '2' WHEN tb.status ='WF in progress' THEN '3' WHEN tb.status ='FINISHED' THEN '4' ELSE tb.status END)  as statusorder  from ps_t_bid_dashboard tb where bid_no='" + bidno + "' and bid_revision='" + rev + "' and  depart_position_name = 'กวอ-ส.'";
            sql += " order by statusorder";
            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
        public string getAssign(string login)
        {
            string strBidList = "";
            DataTable dt = new DataTable();
            string sql = @"select bid_no from ps_t_bid_monitoring_staff where emp_id ='" + login + "'";
            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            var rows = dt.AsEnumerable().Select(r => string.Format("{0}", string.Join(",", r.ItemArray)));

            strBidList = string.Join(",", rows.ToArray());
            return strBidList;
        }
        public DataTable getDocview(string bidno, string rev)
        {
            DataTable dt = new DataTable();
            string sql = @"select * from wf_pcsa_request_details where bid_no='" + bidno + "' and bid_revision='" + rev + "'";
            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
        public DataTable getDisDocview()
        {
            DataTable dt = new DataTable();
            string sql = "select distinct bid_no,bid_revision from wf_pcsa_request_details ";
            sql += " order by bid_no";

            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
        public DataTable getDisMonitor(string bidlist)
        {
            DataTable dt = new DataTable();
            string sql = "";
            sql = "select distinct bid_no,bid_revision,TOR_Approve_Date from vw_ps_bid_monitor where job_no in (select job_no from ps_t_p6_task_assign )";
            if (bidlist != "") sql += " and bid_no in('" + bidlist + "')";
            
        
            sql += " order by TOR_Approve_Date";

            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
        public DataTable getDataMonitor(string bidno)
        {
            DataTable dt = new DataTable();

            string sql = "select * from vw_ps_bid_monitor bm where bid_no='" + bidno + "' and job_revision = (select max(job_revision) from vw_ps_bid_monitor bb where bm.job_no = bb.job_no)";
            sql += " order by job_no";

            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
        public string getActivityBid(string xBidNo)
        {
            string strPositionList = "";
            try
            {
                string filterBid = ConfigurationManager.AppSettings["P6_filterBid"].ToString();
                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');

                foreach (string bid in prefixBid)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + bid + "%')";
                }

                string sql = @"select distinct Org_Name
                                from 
                                (
                                        select
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                             where p.proj_short_name like '%" + xBidNo + @"%' 
                                                and ( t.task_name not like '%WRK%'  )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @") 
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where org_name != 'PLS' and org_name != 'Budget'";

                DataSet ds = oraDBUtil._getDS(sql, strConn_P6);
                DataTable dt = ds.Tables[0];

                //string[] arrPosition = dt.Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
                //strPositionList = String.Join(",", arrPosition);

                var rows = dt.AsEnumerable().Select(r => string.Format("{0}", string.Join(",", r.ItemArray)));

                strPositionList = string.Join(",", rows.ToArray());
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return strPositionList;
        }
        public void AssignToStaff(string bidno,string rev,string emp,string position, string login)
        {
            string strQuery = "";
            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from ps_t_bid_monitoring_staff where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = "update ps_t_bid_monitoring_staff set emp_id ='" + emp + "',emp_position ='" + position + "', updated_datetime=GETDATE() where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "'";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
                else
                {
                    strQuery = @"INSERT INTO ps_t_bid_monitoring_staff
                              ( [bid_no]
                              ,[bid_revision]
                              ,[emp_id]
                              ,[emp_position]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + bidno + "' " +
                            " , '" + rev + "' " +
                            " , '" + emp + "' " +
                            " , '" + position + "' " +
                            " , '" + login + "' " +
                            " , GETDATE() " +
                            " , '" + login + "' " +
                            " , GETDATE())";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void insertStatus(string bidno, string rev,string status,string login)
        {
            string strQuery = "";
            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from ps_t_bid_monitoring where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "'";
               
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = "update ps_t_bid_monitoring set bid_monitor_status ='"+ status + "' where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "'";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
                else
                {
                    strQuery = @"INSERT INTO ps_t_bid_monitoring
                              ( [bid_no]
                              ,[bid_revision]
                              ,[bid_monitor_status]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + bidno + "' " +
                            " , '" + rev + "' " +
                            " , '" + status + "' " +
                            " , '" + login + "' " +
                            " , GETDATE() " +
                            " , '" + login + "' " +
                            " , GETDATE())";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
            
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public DataTable chkSupply(string bidno)
        {
            DataTable dt = new DataTable();

            string sql = @"              select * 
										 from bid_no bd
										 left join tx_bid_type bt on bd.bid_id = bt.bid_type_id
										 where bid_no = '"+ bidno + "' ";

            dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            return dt;
        }
    }
}
