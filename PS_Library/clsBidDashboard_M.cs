﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsBidDashboard_M
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        public clsBidDashboard_M() { }
        public DataTable getDDLDept()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct depart_position_name from vw_ps_dashboard_doc where depart_position_name <> '' and  is_select_dashboard =1"; //where is_select_dashboard =1
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDoctype()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct doctype_code,doctype_name from ps_m_doctype where is_select_dashboard =1";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetDataAllDoc(string Jobno, string chkdraw, string rev, string mode)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @" SELECT *           
							  ,(select order_no from ps_m_department de where de.depart_code = dd.depart_position_name) as order_no
							FROM vw_ps_dashboard_doc dd";// and depart_position_name ='" + dept + "' and section_position_name ='" + section + "'
                if (mode == "job") strQuery += " where job_no = '" + Jobno + "' and job_revision = '" + rev + "'";
                else if (mode == "bid") strQuery += " where bid_no = '" + Jobno + "' and bid_revision = '" + rev + "' and  is_select_dashboard =1";
                if (chkdraw != "")
                {
                    string rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code in ('DD','TD'))";
                    strQuery += rowfilter;
                }
                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getGVSelPart(string bidNo, string bidrev, string jobNo, string jobrev, string mode, string schName)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (mode == "bid")
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit 
                from  ps_t_mytask_equip 
                where bid_no = '" + bidNo + "' and bid_revision='" + bidrev + "' and schedule_name='" + schName + "' and job_no='"+ jobNo + "'";// and path_code='" + part + "'
                }
                else if (mode == "job")
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit 
                from  ps_t_mytask_equip 
                where job_no = '" + jobNo + "' and job_revision='" + jobrev + "'";// and path_code='" + part + "'
                }
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getGVSelPart_sup(string part, string bidno)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit,part_code_b,equip_item_no_b
                from  ps_t_mytask_equip 
                where bid_no='" + bidno + "'";
                if (part != "") sql += " and part_code_b ='" + part + "'";
             
                sql += " order by equip_code";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public DataTable getDataDoc(string bidno, string rev, string doccode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct depart_position_name,* from vw_ps_dashboard_doc where bid_no='" + bidno + "' and bid_revision='" + rev + "' and doctype_code ='" + doccode + "' and  is_select_dashboard =1"; //where is_select_dashboard =1
                sql += "  order by doctype_orderno";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getPart(string bidno,string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_t_mytask_part where bid_no='" + bidno + "' and bid_revision='" + rev + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void Verify_Document(string bidno, string rev, string jobno,string jobrev,string docid, string doccode, string login,string position,string verify)
        {
            try
            {
                string sql = @"  update ps_t_mytask_doc set is_verify='" + verify + @"'
								 ,verify_by='" + login + @"'
								 ,verify_position= '" + position + @"'
								 ,verify_datetime =getdate()
                where bid_no= '" + bidno + "' and bid_revision= '" + rev + "' and job_no= '" + jobno + "' and job_revision= '" + jobrev + "' and doctype_code= '" + doccode + "' and doc_nodeid= '" + docid + "'";
                if (verify == "1")
                {
                    sql += " and is_verify <> 1";
                }
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
    }
}
