﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PS_Library.DocumentManagement;
using PS_Library.WorkflowService;

namespace PS_Library
{
    public class clsWfStock
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string dbOraConnectionString = ConfigurationManager.AppSettings["db_common"].ToString();
        private string strWfMapId = ConfigurationManager.AppSettings["wf_stock_map_id"].ToString();
        public string strConn_Common = ConfigurationManager.AppSettings["db_common"].ToString();

        private OTFunctions zotFunctions = new OTFunctions(); 
        private DbControllerBase db = new DbControllerBase();

        public clsWfStock() { }

        public string GetUserInApproverGroup()
        {
            string strGroupName = ConfigurationManager.AppSettings["wf_stock_group_audit"].ToString();
            string strUserId = string.Empty;
            DataTable dt = zotFunctions.getMembers(strGroupName, "");
            if (dt.Rows.Count > 0)
            {
                strUserId = dt.Rows[0]["name"].ToString();
            }
            return strUserId;
        }

       
        public void StartWf(string strStockReqNo, string strRequestorDept)
        {
            string result = string.Empty;
            string strDeptType = string.Empty; // line or sub
            string strRequestorType = string.Empty;
            string strRequestorId = string.Empty;
            string strSectionId = string.Empty;
            string strAuditId = string.Empty;
            string strDepId = string.Empty;
            string strReqNo = string.Empty;
            DataTable dtWfData = GetWfStockDataByReqNo(strStockReqNo);
            if (dtWfData.Rows.Count > 0)
            {




                // db section
                strRequestorId = dtWfData.Rows[0]["requestor_id"].ToString();
                //EmpInfoClass emp = new EmpInfoClass();
                //Employee thisEmp = emp.getInFoByEmpID(strRequestorId);
                strRequestorType = GetRequestorType(strRequestorId);
                strDeptType = GetDeptTypeByDept(strRequestorDept);
                strAuditId = GetUserInApproverGroup();
                strSectionId = dtWfData.Rows[0]["reviewer_id"].ToString();
                strDepId = dtWfData.Rows[0]["pm_id"].ToString(); //thisEmp.MANAGER_ID;

                strReqNo = dtWfData.Rows[0]["stock_reqno"].ToString();
                //wf section
                ApplicationData[] appDatas = zotFunctions.getWFStartAppData(strWfMapId, "");
                AttributeData wfAttr = new AttributeData();

                foreach (ApplicationData appData in appDatas)
                {
                    if (appData.GetType().Name == "AttributeData")
                    {
                        wfAttr = (AttributeData)appData;
                        break;
                    }
                }
                if (wfAttr.Attributes != null)
                {
                    // set attribute date
                    zotFunctions.setwfAttrValue(ref wfAttr, "PID", dtWfData.Rows[0]["process_id"].ToString());
                    zotFunctions.setwfAttrValue(ref wfAttr, "WF_Req_no", strReqNo);
                    zotFunctions.setwfAttrValue(ref wfAttr, "Requestor_ID", strRequestorId);
                    if(strRequestorType == "staff")
                        zotFunctions.setwfAttrValue(ref wfAttr, "Approve1_ID", strSectionId);
                    zotFunctions.setwfAttrValue(ref wfAttr, "Approve2_ID", strAuditId);
                    zotFunctions.setwfAttrValue(ref wfAttr, "Approve3_ID", strDepId);
                    zotFunctions.setwfAttrValue(ref wfAttr, "isStaff", strRequestorType == "staff" ? "y" : "n");
                    zotFunctions.setwfAttrValue(ref wfAttr, "IsSameApprove1_2", strSectionId == strAuditId ? "y" : "n");
                    zotFunctions.setwfAttrValue(ref wfAttr, "IsSub", strDeptType == "sub" ? "y" : "n");

                    string strTitle = strReqNo + " - " + DateTime.Now.ToString();

                    var procInst = zotFunctions.startProcess("", strWfMapId, appDatas, strTitle);
                    try
                    {
                        result = procInst.ProcessID.ToString();
                        UpdateAuditApproverByRequestNo(strReqNo, strAuditId);
                        UpdateWfProcessIdByReqNo(strReqNo, result);
                    }
                    catch { }
                    // create memo
                    //string strMemoNodeId = UploadMemo(wfPsDa.ps_da_no, wfPsDa.emp_name, wfPsDa.emp_position, wfPsDa.lstDocApvDetail[0].apv_position, wfPsDa.memo_subject, wfPsDa.memo_content);
                    //wfPsDa.doc_node_id_memo = strMemoNodeId;

                }
            }
        }

        private string GetRequestorType(string strEmpId)
        {
            string strRequestorType = string.Empty;
            strEmpId = strEmpId.Replace("z", "");
            string strSql = @"SELECT priox  FROM m_syn_employee   where pernr = '" + strEmpId + "'  and priox is null and orglevel >4";
            oraDBUtil ora = new oraDBUtil();
            DataTable dt = ora._getDT(strSql, strConn_Common);
            if (dt.Rows.Count > 0)
            {
                strRequestorType = "staff";
            }
            else
            {
                strRequestorType = "section";
            }

            return strRequestorType;
        }

        private string GetDeptTypeByDept(string strDept)
        {
            string strType = "";
            string strSubDept = ConfigurationManager.AppSettings["wf_stock_sub"];
            string strLineDept = ConfigurationManager.AppSettings["wf_stock_line"];
            int iCheckSub = strSubDept.IndexOf(strDept);
            int iCheckLine = strSubDept.IndexOf(strLineDept);
            if (iCheckSub >= 0) strType = "sub";
            if (iCheckLine >= 0) strType = "line";
            return strType;
        }

        public DataTable GetWfStockDataByReqNo(string strReqNo)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT rowid
                          ,process_id
                          ,stock_reqno
                          ,stock_subject
                          ,project_id
                          ,reference
                          ,note
                          ,requestor_id
                          ,requestor_name
                          ,requestor_position
                          ,requestor_position_full
                          ,requestor_submitdate 
                          ,requestor_status
                          ,reviewer_id
                          ,reviewer_name
                          ,reviewer_position
                          ,reviewer_position_full
                          ,reviewer_apvdate
                          ,reviewer_status
                          ,approver_id
                          ,approver_name
                          ,approver_position
                          ,approver_position_full
                          ,approver_apvdate
                          ,approver_status
                          ,pm_id
                          ,pm_name
                          ,pm_position
                          ,pm_position_full
                          ,pm_apvdate
                          ,pm_status
                          ,assistdirector_id
                          ,assistdirector_name
                          ,assistdirector_position
                          ,assistdirector_position_full
                          ,assistdirector_apvdate
                          ,assistdirector_status
                          ,director_id
                          ,director_name
                          ,director_position
                          ,director_position_full
                          ,director_apvdate
                          ,director_status
                          ,form_status
                          ,form_to
                          ,form_cc
                          ,created_by
                          ,created_datetime
                          ,updated_by
                          ,updated_datetime
                          ,wf_processid
                          ,wf_subprocessid
                          ,wf_taskid
                      FROM wf_ps_stock_request
                      where stock_reqno = '" + strReqNo + @"'";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetWfStockDataByWfProcessId(string strProcessId)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT rowid
                          ,process_id
                          ,stock_reqno
                          ,stock_subject
                          ,project_id
                          ,reference
                          ,note
                          ,requestor_id
                          ,requestor_name
                          ,requestor_position
                          ,requestor_position_full
                          ,requestor_submitdate 
                          ,requestor_status
                          ,reviewer_id
                          ,reviewer_name
                          ,reviewer_position
                          ,reviewer_position_full
                          ,reviewer_apvdate
                          ,reviewer_status
                          ,approver_id
                          ,approver_name
                          ,approver_position
                          ,approver_position_full
                          ,approver_apvdate
                          ,approver_status
                          ,pm_id
                          ,pm_name
                          ,pm_position
                          ,pm_position_full
                          ,pm_apvdate
                          ,pm_status
                          ,form_status
                          ,form_to
                          ,form_cc
                          ,created_by
                          ,created_datetime
                          ,updated_by
                          ,updated_datetime
                          ,wf_processid
                          ,wf_subprocessid
                          ,wf_taskid
                          ,doc_memo_nodeid
                      FROM wf_ps_stock_request
                      where wf_processid = " + strProcessId;
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public string UploadMemo(string strPsStockNo, string strRequestorName, string strRequestorPostion, string strApproverPosition, string strSubject, string strContent)
        {
            string strNodeId = string.Empty;
            string strFileName = strPsStockNo + "_" + DateTime.Now.ToString("yyyy-MM-dd");
            string strFolderAttNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString();


            memoDocClass memo = new memoDocClass();
            byte[] result = memo.createMemoDocApproveByte("", strPsStockNo, DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", strRequestorName,
              strRequestorPostion, "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", "", strApproverPosition, "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"),
              strSubject, strContent);


            OTFunctions ot = new OTFunctions();
            var nodeId = ot.uploadDoc(strFolderAttNodeId, strFileName + ".pdf", strFileName + ".pdf", result, "");
            strNodeId = nodeId.ToString();
            return strNodeId;
        }

        public void UpdateWfProcessIdByReqNo(string strReqNo, string strProcessId)
        {
            string strSql = "update wf_ps_stock_request set wf_processid = '" + strProcessId + "' ,wf_subprocessid = '" + strProcessId + "' where stock_reqno = '" + strReqNo + "'";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        private void UpdateAuditApproverByRequestNo(string strReqNo, string strAuditApproverId)
        {
            EmpInfoClass emp = new EmpInfoClass();
            try
            {
                Employee thisEmp = emp.getInFoByEmpID(strAuditApproverId);
                string strEmpName = thisEmp.SNAME;
                string strEmpPosition = thisEmp.MC_SHORT;
                string strEmpPositionFull = thisEmp.POSITION;
                string strSql = "update wf_ps_stock_request set approver_id = '" + strAuditApproverId + "' , approver_name = '" + strEmpName + "' , approver_position = '" + strEmpPosition + "',approver_position_full = '" + strEmpPositionFull + "' where stock_reqno = '" + strReqNo + "'";
                db.ExecNonQuery(strSql, dbConnectionString);
            }
            catch(Exception ex)
            { 
                LogHelper.WriteEx(ex);
            }
        }



    }
}
