﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Oracle.DataAccess;
using System.Data;
using Oracle.ManagedDataAccess.Client;


namespace PS_Library
{
    public class oraDBUtil
    {
        public string connstr { get; set; }

        public DataSet _getDS(string cmdstr, string constr)
        {
            DataSet ds = new DataSet();
            try
            {
                OracleDataAdapter adapter = new OracleDataAdapter(cmdstr, constr);

                // Create the builder for the adapter to automatically generate
                // the Command when needed
                OracleCommandBuilder builder = new OracleCommandBuilder(adapter);

                // Create and fill the DataSet using the EMP

                adapter.Fill(ds, "1");
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return ds;
        }
        public DataTable _getDT(string cmdstr, string constr)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            try
            {
                OracleDataAdapter adapter = new OracleDataAdapter(cmdstr, constr);

                // Create the builder for the adapter to automatically generate
                // the Command when needed
                OracleCommandBuilder builder = new OracleCommandBuilder(adapter);

                // Create and fill the DataSet using the EMP

                adapter.Fill(ds, "1");
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            dt = ds.Tables[0];
            return dt;
        }
        public void _runSQL(string cmdstr, string constr)
        {
            try
            {
                var conn = new Oracle.ManagedDataAccess.Client.OracleConnection();
                conn.ConnectionString = constr;
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = cmdstr;
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
    }
}