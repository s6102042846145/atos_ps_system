﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsJobDashboard
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string db_common = ConfigurationManager.AppSettings["db_common"].ToString();
        DbControllerBase db = new DbControllerBase();
        static oraDBUtil oraDBUtil = new oraDBUtil();
        public clsJobDashboard() { }

        public DataTable getHead(string org4, string org5)
        {
            DataTable dt = new DataTable();

            string strQuery = @"select * from 
(
select distinct
1 as orderby,
't' as default_select,
orgsname5
from m_syn_employee where orgsname4 = '" + org4 + @"' and orgsname5 is not null
union
select distinct
2 as orderby,
't' as default_select,
orgsname4
from m_syn_employee where orgsname4 = '" + org4 + @"'
union
select distinct 
3 as orderby,
case when ROW_NUMBER() OVER (ORDER BY mc_short) = 1 then 't' else 'f' end as default_select,
mc_short 
from m_syn_employee where orgsname3 in (select orgsname3 from m_syn_employee where orgsname4 = '" + org4 + @"' and rownum=1) 
and mc_short like (select 'ช.' || replace(orgsname3,'.','') || '%' from m_syn_employee where orgsname4 = '" + org4 + @"' and rownum=1) --'ช.อวส%'
union
select distinct
4 as orderby,
't' as default_select,
orgsname3
from m_syn_employee where orgsname4 = '" + org4 + @"'
) tb_approve
order by orderby, default_select desc";
            //    string strQuery = @"SELECT 
            //  distinct
            //  ORGSNAME1, --รอง
            //  ORGSNAME2, --ช.รอง
            //   ORGSNAME3, --ฝ่าย
            //  ORGSNAME4, --หัวหน้ากอง
            //  ORGSNAME5 --หัวหน้าแผนก
            //FROM M_SYN_EMPLOYEE 
            //where (orgsname4 = '" + mc_short + "' or orgsname5 = '" + mc_short + "') and orgsname5 is not null";

            dt = oraDBUtil._getDT(strQuery, db_common);

            return dt;
        }
        public DataTable getHead2()
        {
            DataTable dt = new DataTable();

            string strQuery = @"SELECT 
  orgsname
  ,orgname
FROM M_SYN_EMPLOYEE 
where  mc_short like 'หก-%'";

            dt = oraDBUtil._getDT(strQuery, db_common);

            return dt;
        }
        public DataTable getAttachWA(string jobno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select [job_no]
                      ,[job_revision]
                      ,[document_name]
                      ,[upload_datetime]
                      ,[att_url] 
                     from vw_ps_job_dashboard_att where job_no='" + jobno + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public JobInfo getJobInfo(string JobNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"select [job_no]
                                  ,[revision]
                                  ,[sub_line_name]
                                  ,[job_desc]
                                  ,[remark]
                                  ,[completion_date]
                                  ,[p6_task_id]
                                  ,[p6_task_name]
                                  ,[p6_plan_start]
                                  ,[p6_plan_end] 
                                from vw_ps_job_dashboard_info where job_no ='" + JobNo + "'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
               
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            JobInfo jinfo = new JobInfo();
            if (dt.Rows.Count > 0)
            {
                jinfo.job_no = dt.Rows[0]["job_no"].ToString();
                //  dbo.udf_StripHTML(job.remark) as remark, waa.reference
                jinfo.revision = dt.Rows[0]["revision"].ToString();
                jinfo.sub_line_name = dt.Rows[0]["sub_line_name"].ToString();
                jinfo.job_desc = dt.Rows[0]["job_desc"].ToString();
                jinfo.remark = dt.Rows[0]["remark"].ToString();

                jinfo.completion_date = dt.Rows[0]["completion_date"].ToString();
                jinfo.p6_task_id = dt.Rows[0]["p6_task_id"].ToString();
                jinfo.p6_task_name = dt.Rows[0]["p6_task_name"].ToString();
                jinfo.p6_plan_start = dt.Rows[0]["p6_plan_start"].ToString();
                jinfo.p6_plan_end = dt.Rows[0]["p6_plan_end"].ToString();
            }
            return jinfo;
        }

        public DataTable GetHeadActiv(string Jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "";
                string sql = "select * from ps_t_job_dashboard where job_no = '" + Jobno + "' and job_revision = '" + rev + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = "SELECT depart_position_name, ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS positionid FROM vw_ps_job_dashboard_activity  WHERE job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "') GROUP BY depart_position_name";
                }
                else
                {
                    strQuery = "SELECT depart_position_name, ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS positionid FROM vw_ps_job_activity_default  WHERE job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "') GROUP BY depart_position_name";
                }


                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public DataTable GetHeadDoc(string Jobno)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "SELECT depart_position_name, ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS posDocid FROM vw_ps_dashboard_doc WHERE job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "') GROUP BY depart_position_name";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
     
        public DataTable GetJobRelate(string Jobno)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT [eq_desc]
                              ,[eq_group_no]
                              ,[bid_no]
                              ,[bid_desc]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[schedule_status]
                              ,[job_no]
                              ,[job_revision]
                              ,[ps_url]
                          FROM vw_ps_job_dashboard_relate
                          WHERE job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "')";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable GetJobActiv(string Jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "";
                string sql = "select * from ps_t_job_dashboard where job_no = '" + Jobno + "' and job_revision = '" + rev + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = @"SELECT DISTINCT [activity_id]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[activity_name]
                              ,[req_drawing]
                              ,[assignee_drawing]
                              ,[req_equip]
                              ,[assignee_equip]
                              ,[req_tech_doc]
                              ,[assignee_tech_doc]
                              ,[next_activity_id]
                              ,[approval_routing]
                              ,[email_noti_id]
                              ,[email_content]
                              ,[status_drawing]
                              ,[status_equip]
                              ,[status_tech_doc]
                           ,[job_no]
                           ,[job_revision]
                              ,[is_select]
                              ,[status_activity]
                          FROM vw_ps_job_dashboard_activity
                          WHERE job_no = '" + Jobno + "' and job_revision = '" + rev + "'";
                }
                else
                {
                    strQuery = @"SELECT DISTINCT [activity_id]
                              ,[is_select]
                              ,[job_no]
                              ,[job_revision]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[activity_name]
                              ,[req_drawing]
                              ,[assignee_drawing]
                              ,[status_drawing]
                              ,[req_equip]
                              ,[assignee_equip]
                              ,[status_equip]
                              ,[req_tech_doc]
                              ,[assignee_tech_doc]
                              ,[status_tech_doc]
                              ,[next_activity_id]
                              ,[approval_routing]
                              ,[status_activity]
                              ,[email_noti_id]
                              ,[email_content]
                        from vw_ps_job_activity_default where job_no = '" + Jobno + "' and job_revision = '" + rev + "'";
                }

                strQuery += " order by activity_id";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable GetJobDoc(string Jobno)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT distinct [depart_position_name]  
                              ,[section_position_name]
                              ,[doctype_name]
                              ,[doc_no]
                              ,[doc_desc]
                              ,[doctype_code]
                              ,[doc_sheetno]
                              ,[doc_revision]
                              ,[bid_no]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[link_doc]
	                          ,[job_no]
	                          ,[job_revision]
                              ,[updated_by]
                              ,[updated_datetime]
                          FROM vw_ps_dashboard_doc
                          WHERE job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "')";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }

        public void AssignJob(string job_no, string job_revision, string depart_position_name, string section_position_name, string activity_id, string req_drawing, string assignee_drawing, string req_equip, string assignee_equip,
            string req_tech_doc, string assignee_tech_doc, string approval_routing, string next_activity_id, string email_noti_id, string created_by, string updated_by,string status, string is_select, string chkDept)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from ps_t_job_dashboard where job_no = '" + job_no + "' and  job_revision = (select max(revision) from job_no where job_no = '" + job_no + "') and activity_id = '" + activity_id + "'";
                string strQuery = "";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    if (chkDept == "1")
                    {
                        //if (assignee_drawing !="" || assignee_equip != "" || assignee_tech_doc !="")
                        //{
                            strQuery = "update ps_t_job_dashboard set assignee_drawing = '" + assignee_drawing + "' , assignee_equip = '" + assignee_equip + "' , assignee_tech_doc = '" + assignee_tech_doc +
                 @"', status ='" + status + "', is_select ='" + is_select + "' , updated_by ='" + updated_by + "' , created_by='" + created_by + "' , updated_datetime = GETDATE(),approval_routing='" + approval_routing + "' where job_no = '" + job_no + "' and  job_revision = (select max(revision) from job_no where job_no = '" + job_no + "') and activity_id = '" + activity_id + "'";
                            db.ExecNonQuery(strQuery, zetl4eisdb);
                        //}
                      
                    }
                }
                else
                {
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count == 0)
                    {
                        strQuery = @"INSERT INTO ps_t_job_dashboard
                              ([job_no]
                              ,[job_revision]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[activity_id]
                              ,[req_drawing]
                              ,[assignee_drawing]
                              ,[req_equip]
                              ,[assignee_equip]
                              ,[req_tech_doc]
                              ,[assignee_tech_doc]
                              ,[approval_routing]
                              ,[next_activity_id]
                              ,[email_noti_id]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime]
                              ,[status]
                              ,[is_select])
                            VALUES
                                ('" + job_no + "' " +
                                 " , '" + job_revision + "' " +
                                 " , '" + depart_position_name + "' " +
                                 " , '" + section_position_name + "' " +
                                 " , '" + activity_id + "' " +
                                 " , '" + req_drawing + "' " +
                                 " , '" + assignee_drawing + "' " +
                                 " , '" + req_equip + "' " +
                                 " , '" + assignee_equip + "' " +
                                 " , '" + req_tech_doc + "' " +
                                 " , '" + assignee_tech_doc + "' " +
                                 " , '" + approval_routing + "' " +
                                 " , '" + next_activity_id + "' " +
                                 " , '" + email_noti_id + "' " +
                                 " , '" + created_by + "' " +
                                 " , GETDATE() " +
                                 " , '" + updated_by + "' " +
                                 " , GETDATE() " +
                                  " , '" + status + "' " +
                                   " , '" + is_select + "')";
                        db.ExecNonQuery(strQuery, zetl4eisdb);
                    }
                    
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }

        public void DeleteActivity(string job_no, string job_revision, string depart_position_name, string section_position_name)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from ps_t_job_dashboard where job_no = '" + job_no + "' and  job_revision = '" + job_revision + "' and depart_position_name ='" + depart_position_name + "' and section_position_name ='"+ section_position_name + "'  and status = ''";
                string strQuery = "";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = "delete from ps_t_job_dashboard where job_no = '" + job_no + "' and  job_revision = '" + job_revision + "' and depart_position_name ='" + depart_position_name + "' and section_position_name ='" + section_position_name + "' and status = ''";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
             
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
         
        }
        public void AssignReq(string job_no, string job_revision, string depart_position_name, string section_position_name, string activity_id, string req_drawing, string assignee_drawing, string req_equip, string assignee_equip,
          string req_tech_doc, string assignee_tech_doc, string next_activity_id, string email_noti_id, string created_by, string updated_by, string status, string approval_routing,string chkupdate)
        {
            DataTable dt = new DataTable();
            try
            {
                DataTable dtchk = new DataTable();
                string sqlchk = "select * from ps_t_job_dashboard where job_no = '" + job_no + "' and  job_revision = '" + job_revision + "' and activity_id = '" + activity_id + "'";
                dtchk = db.ExecSql_DataTable(sqlchk, zetl4eisdb);
                string strQuery = "";

                string sql = "select * from ps_t_job_dashboard where job_no = '" + job_no + "' and  job_revision = '" + job_revision + "' and activity_id = '" + activity_id + "' and depart_position_name ='" + depart_position_name + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                //if (dt.Rows.Count > 0)
                //{
                if (dtchk.Rows.Count == 0)
                {
                    strQuery = @"INSERT INTO ps_t_job_dashboard
                              ([job_no]
                              ,[job_revision]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[activity_id]
                              ,[req_drawing]
                              ,[assignee_drawing]
                              ,[req_equip]
                              ,[assignee_equip]
                              ,[req_tech_doc]
                              ,[assignee_tech_doc]
                              ,[approval_routing]
                              ,[next_activity_id]
                              ,[email_noti_id]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime]
                              ,[status]
                              ,[is_select])
                            VALUES
                                ('" + job_no + "' " +
                            " , '" + job_revision + "' " +
                            " , '" + depart_position_name + "' " +
                            " , '" + section_position_name + "' " +
                            " , '" + activity_id + "' " +
                            " , '" + req_drawing + "' " +
                            " , '" + assignee_drawing + "' " +
                            " , '" + req_equip + "' " +
                            " , '" + assignee_equip + "' " +
                            " , '" + req_tech_doc + "' " +
                            " , '" + assignee_tech_doc + "' " +
                            " , '" + approval_routing + "' " +
                            " , '" + next_activity_id + "' " +
                            " , '" + email_noti_id + "' " +
                            " , '" + created_by + "' " +
                            " , GETDATE() " +
                            " , '" + updated_by + "' " +
                            " , GETDATE() " +
                             " , '" + status + "' " +
                              " , '0')";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
                else
                {
                    if (chkupdate != "")
                    {
                        if (dtchk.Rows[0]["status"].ToString() != "WF in progress" && dtchk.Rows[0]["status"].ToString() != "FINISHED" && dtchk.Rows[0]["status"].ToString() != "ASSIGNED")
                        {
                            if (dtchk.Rows[0]["assignee_drawing"].ToString() != "") assignee_drawing = dtchk.Rows[0]["assignee_drawing"].ToString();
                            if (dtchk.Rows[0]["assignee_equip"].ToString() != "") assignee_equip = dtchk.Rows[0]["assignee_equip"].ToString();
                            if (dtchk.Rows[0]["assignee_tech_doc"].ToString() != "") assignee_tech_doc = dtchk.Rows[0]["assignee_tech_doc"].ToString();

                            strQuery = "update ps_t_job_dashboard set assignee_drawing = '" + assignee_drawing + "' , assignee_equip = '" + assignee_equip + "' , assignee_tech_doc = '" + assignee_tech_doc +
@"', status ='" + status + "' , updated_by ='" + updated_by + "' , created_by='" + created_by + "' , updated_datetime = GETDATE(),approval_routing='" + approval_routing + "' where job_no = '" + job_no + "' and  job_revision = '" + job_revision + "' and activity_id = '" + activity_id + "' and depart_position_name ='" + depart_position_name + "'";
                            db.ExecNonQuery(strQuery, zetl4eisdb);

                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public DataTable Process(string Jobno,string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "select * from vw_ps_bid_monitor where job_no = '" + Jobno + "' and job_revision ='" + rev + "'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable AssignActivityReq(string dept,string temp)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"select distinct [temp_name]
                                  ,[job_type]
                                  ,[activity_id]
                                  ,[activity_name]
                                  ,[depart_position_name]
                                  ,[section_position_name]
                                  ,[req_drawing]
                                  ,[assignee_drawing]
                                  ,[p6_taskcode_dwg]
                                  ,[req_equip]
                                  ,[assignee_equip]
                                  ,[p6_taskcode_equip]
                                  ,[req_tech_doc]
                                  ,[assignee_tech_doc]
                                  ,[p6_taskcode_tech_doc]
                                  ,[next_activity_id]
                                  ,[approval_routing]
                                  ,[email_noti_id]
                                  ,[remark]
                                  ,[created_by]
                                  ,[created_datetime]
                                  ,[updated_by]
                                  ,[updated_datetime]
                                  ,[is_active]
                                  ,[weight_factor]
                                from ps_m_activity_job where depart_position_name ='" + dept + "' and temp_name ='" + temp + "'";

                //string strQuery = @"select distinct [temp_name]
                //                  ,[job_type]
                //                  ,[activity_id]
                //                  ,[activity_name]
                //                  ,[depart_position_name]
                //                  ,[section_position_name]
                //                  ,[req_drawing]
                //                  ,[assignee_drawing]
                //                  ,[p6_taskcode_dwg]
                //                  ,[req_equip]
                //                  ,[assignee_equip]
                //                  ,[p6_taskcode_equip]
                //                  ,[req_tech_doc]
                //                  ,[assignee_tech_doc]
                //                  ,[p6_taskcode_tech_doc]
                //                  ,[next_activity_id]
                //                  ,[approval_routing]
                //                  ,[email_noti_id]
                //                  ,[remark]
                //                  ,[created_by]
                //                  ,[created_datetime]
                //                  ,[updated_by]
                //                  ,[updated_datetime]
                //                  ,[is_active]
                //                  ,[weight_factor]
                //            from ps_m_activity_job where depart_position_name ='" + dept + "' and temp_name ='" + temp + @"' 
                //            and activity_id not in (select activity_id from ps_t_job_dashboard where job_no='" + jobno + "' and depart_position_name = '" + dept + "')";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable SelectActivityReq(string dept)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"select distinct [temp_name]  from ps_m_activity_job where depart_position_name = '" + dept + "'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable CheckCountTrans(string jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"select * from ps_t_job_dashboard where job_no = '"+ jobno + "' and  job_revision ='"+ rev + "'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable chkInsertActivity(string jobno, string rev, string dept, string temp)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_m_activity_job where depart_position_name ='" + dept + "' and temp_name ='" + temp + @"' 
                and activity_id not in (select activity_id from ps_t_job_dashboard where job_no='" + jobno + "' and depart_position_name ='" + dept + "')";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                 
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable ViewNextActivityJob(string jobno, string rev, string acid)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            try
            {
                string strQuery = @"select * from vw_ps_job_dashboard_activity where job_no = '" + jobno + "' and  job_revision ='" + rev + "' and activity_id='" + acid + "'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = @"select * from vw_ps_job_dashboard_activity where job_no = '" + jobno + "' and  job_revision ='" + rev + "' and activity_id in('" + dt.Rows[0]["next_activity_id"].ToString().Replace(",", "','") + "')";
                    dt2 = db.ExecSql_DataTable(strQuery, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt2;
        }
        public DataTable ViewBeforeActivityJob(string jobno, string rev, string acid)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"select * from vw_ps_job_dashboard_activity where job_no = '" + jobno + "' and  job_revision ='" + rev + "' and next_activity_id like'%" + acid + "%'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable ViewLogActivityJob(string jobno, string rev, string acid, DataTable dtlog)
        {
            DataTable dt = new DataTable();
            DateTime dttime = new DateTime();
            try
            {
                string strQuery = @"select * from wf_ps_request
								 where job_no = '" + jobno + "' and job_revision='" + rev + "' and activity_id='" + acid + "' order by rowid desc";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = @"select *
								from wf_ps_request pr
								left
								join wf_ps_request_detail rd on pr.process_id = rd.process_id
								where job_no = '" + jobno + "' and job_revision='" + rev + "' and activity_id = '" + acid + "' and pr.rowid = " + dt.Rows[0]["rowid"].ToString() + "";
                    dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr2 in dt.Rows)
                        {
                            DataRow dr = dtlog.NewRow();
                            if (dr2["emp_apvdate"].ToString() != "")
                            {
                                dttime = Convert.ToDateTime(dr2["emp_apvdate"]);
                                dr["emp_apvdate"] = dttime.ToString("dd/MM/yyyy");
                            }
                            else
                            {
                                dr["emp_apvdate"] = dr2["emp_apvdate"];
                            }


                            dr["apv_order"] = dr2["apv_order"];
                            dr["emp_id"] = dr2["emp_id"];
                            dr["emp_name"] = dr2["emp_name"];
                            dr["emp_position"] = dr2["emp_position"];
                            dr["apv_status"] = dr2["apv_status"];
                            dr["ps_subject"] = dr2["ps_subject"];
                            dr["process_id"] = dr2["process_id"];

                            dtlog.Rows.Add(dr);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dtlog;
        }
        public class JobInfo
        {
            public string job_no { get; set; }
            public string revision { get; set; }
            public string sub_line_name { get; set; }
            public string job_desc { get; set; }
            public string remark { get; set; }
            public string reference { get; set; }
            public string completion_date { get; set; }
            public string p6_task_id { get; set; }
            public string p6_task_name { get; set; }
            public string p6_plan_start { get; set; }
            public string p6_plan_end { get; set; }
        }
       
    }
}
