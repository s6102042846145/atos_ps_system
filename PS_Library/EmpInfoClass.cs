﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace PS_Library
{
    public class EmpInfoClass
    {
        public string isDev = ConfigurationManager.AppSettings["isDev"].ToString();

        public string strConn_PS = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string strConn_Common = ConfigurationManager.AppSettings["db_common"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();
        static oraDBUtil zoraDBUtil = new oraDBUtil();
        public Employee Employee = new Employee();
        public Employee getInFoByEmpID(string xEMP_ID)
        {
            if (isDev.ToLower() == "true") { xEMP_ID = xEMP_ID.Replace("z", ""); }
            string sql = "";// "select * from V_SYN_EMPLOYEE where pernr = '"+xEMP_ID+"'";

            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            
            where e.pernr = '" + xEMP_ID + "' and d.PERNR is not null ";

            // DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                Employee.PERNR = dr["PERNR"].ToString();
                Employee.SNAME = dr["SNAME"].ToString();
                Employee.EMAIL = dr["EMAIL"].ToString();
                if (dr["ORGLEVEL"].ToString() == "5")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME5"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "4")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME4"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "3")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME3"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "2")
                {
                    Employee.POSITION = dr["ORGNAME2"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "1")
                {
                    Employee.POSITION = dr["ORGNAME1"].ToString();
                }
                Employee.MC_SHORT = dr["MC_SHORT"].ToString();
                Employee.MC_STEXT = dr["MC_STEXT"].ToString();
                Employee.ORGLEVEL = dr["ORGLEVEL"].ToString();
                Employee.ORGSNAME1 = dr["ORGSNAME1"].ToString();
                Employee.ORGSNAME2 = dr["ORGSNAME2"].ToString();
                Employee.ORGSNAME3 = dr["ORGSNAME3"].ToString();
                Employee.ORGSNAME4 = dr["ORGSNAME4"].ToString();
                Employee.ORGSNAME5 = dr["ORGSNAME5"].ToString();
                Employee.ORGNAME1 = dr["ORGNAME1"].ToString();
                Employee.ORGNAME2 = dr["ORGNAME2"].ToString();
                Employee.ORGNAME3 = dr["ORGNAME3"].ToString();
                Employee.ORGNAME4 = dr["ORGNAME4"].ToString();
                Employee.ORGNAME5 = dr["ORGNAME5"].ToString();

                Employee.SECTION_HEAD_ID = dr["SECTION_HEAD_ID"].ToString();
                Employee.SECTION_HEAD_NAME = dr["SECTION_HEAD_NAME"].ToString();
                Employee.SECTION_HEAD_EMAIL = dr["SECTION_HEAD_EMAIL"].ToString();
                Employee.SECTION_HEAD_POSITION = dr["SECTION_HEAD_POSITION"].ToString();
                Employee.SECTION_HEAD_POSITION_FULL = dr["SECTION_HEAD_POSITION_FULL"].ToString() + dr["ORGNAME5"].ToString();

                Employee.MANAGER_ID = dr["MANAGER_ID"].ToString();
                Employee.MANAGER_NAME = dr["MANAGER_NAME"].ToString();
                Employee.MANAGER_EMAIL = dr["MANAGER_EMAIL"].ToString();
                Employee.MANAGER_POSITION = dr["MANAGER_POSITION"].ToString();
                Employee.MANAGER_POSITION_FULL = dr["MANAGER_POSITION_FULL"].ToString();

                Employee.DIRECTOR_ID = dr["DIRECTOR_ID"].ToString();
                Employee.DIRECTOR_NAME = dr["DIRECTOR_NAME"].ToString();
                Employee.DIRECTOR_EMAIL = dr["DIRECTOR_EMAIL"].ToString();
                Employee.DIRECTOR_POSITION = dr["DIRECTOR_POSITION"].ToString();
                Employee.DIRECTOR_POSITION_FULL = dr["DIRECTOR_POSITION_FULL"].ToString();

                Employee.AVP_ID = dr["AVP_ID"].ToString();
                Employee.AVP_NAME = dr["AVP_NAME"].ToString();
                Employee.AVP_EMAIL = dr["AVP_EMAIL"].ToString();
                Employee.AVP_POSITION = dr["AVP_POSITION"].ToString();
                Employee.AVP_POSITION_FULL = dr["AVP_POSITION_FULL"].ToString();

                Employee.VP_ID = dr["VP_ID"].ToString();
                Employee.VP_NAME = dr["VP_NAME"].ToString();
                Employee.VP_EMAIL = dr["VP_EMAIL"].ToString();
                Employee.VP_POSITION = dr["VP_POSITION"].ToString();
                Employee.VP_POSITION_FULL = dr["VP_POSITION_FULL"].ToString();

            }
            if (isDev.ToLower() == "true")
            {
                Employee.PERNR = "z" + Employee.PERNR;
                Employee.SECTION_HEAD_ID = "z" + Employee.SECTION_HEAD_ID;
                Employee.MANAGER_ID = "z" + Employee.MANAGER_ID;
                Employee.DIRECTOR_ID = "z" + Employee.DIRECTOR_ID;
                Employee.AVP_ID = "z" + Employee.AVP_ID;
                Employee.VP_ID = "z" + Employee.VP_ID;

            }
            return Employee;
        }
        public Employee getInFoByEmpID(string xEMP_ID, string position_short)
        {
            if (isDev.ToLower() == "true") { xEMP_ID = xEMP_ID.Replace("z", ""); }
            string sql = "";// "select * from V_SYN_EMPLOYEE where pernr = '"+xEMP_ID+"'";

            sql = @"select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            
            where e.pernr = '" + xEMP_ID + "' and e.mc_short = '" + position_short + "' ";

            // DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                Employee.PERNR = dr["PERNR"].ToString();
                Employee.SNAME = dr["SNAME"].ToString();
                Employee.EMAIL = dr["EMAIL"].ToString();
                if (dr["ORGLEVEL"].ToString() == "5")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME5"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "4")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME4"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "3")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME3"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "2")
                {
                    Employee.POSITION = dr["ORGNAME2"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "1")
                {
                    Employee.POSITION = dr["ORGNAME1"].ToString();
                }
                Employee.MC_SHORT = dr["MC_SHORT"].ToString();
                Employee.MC_STEXT = dr["MC_STEXT"].ToString();
                Employee.ORGLEVEL = dr["ORGLEVEL"].ToString();
                Employee.ORGSNAME1 = dr["ORGSNAME1"].ToString();
                Employee.ORGSNAME2 = dr["ORGSNAME2"].ToString();
                Employee.ORGSNAME3 = dr["ORGSNAME3"].ToString();
                Employee.ORGSNAME4 = dr["ORGSNAME4"].ToString();
                Employee.ORGSNAME5 = dr["ORGSNAME5"].ToString();
                Employee.ORGNAME1 = dr["ORGNAME1"].ToString();
                Employee.ORGNAME2 = dr["ORGNAME2"].ToString();
                Employee.ORGNAME3 = dr["ORGNAME3"].ToString();
                Employee.ORGNAME4 = dr["ORGNAME4"].ToString();
                Employee.ORGNAME5 = dr["ORGNAME5"].ToString();

                Employee.SECTION_HEAD_ID = dr["SECTION_HEAD_ID"].ToString();
                Employee.SECTION_HEAD_NAME = dr["SECTION_HEAD_NAME"].ToString();
                Employee.SECTION_HEAD_EMAIL = dr["SECTION_HEAD_EMAIL"].ToString();
                Employee.SECTION_HEAD_POSITION = dr["SECTION_HEAD_POSITION"].ToString();
                Employee.SECTION_HEAD_POSITION_FULL = dr["SECTION_HEAD_POSITION_FULL"].ToString() + dr["ORGNAME5"].ToString();

                Employee.MANAGER_ID = dr["MANAGER_ID"].ToString();
                Employee.MANAGER_NAME = dr["MANAGER_NAME"].ToString();
                Employee.MANAGER_EMAIL = dr["MANAGER_EMAIL"].ToString();
                Employee.MANAGER_POSITION = dr["MANAGER_POSITION"].ToString();
                Employee.MANAGER_POSITION_FULL = dr["MANAGER_POSITION_FULL"].ToString();

                Employee.DIRECTOR_ID = dr["DIRECTOR_ID"].ToString();
                Employee.DIRECTOR_NAME = dr["DIRECTOR_NAME"].ToString();
                Employee.DIRECTOR_EMAIL = dr["DIRECTOR_EMAIL"].ToString();
                Employee.DIRECTOR_POSITION = dr["DIRECTOR_POSITION"].ToString();
                Employee.DIRECTOR_POSITION_FULL = dr["DIRECTOR_POSITION_FULL"].ToString();


                Employee.AVP_ID = dr["AVP_ID"].ToString();
                Employee.AVP_NAME = dr["AVP_NAME"].ToString();
                Employee.AVP_EMAIL = dr["AVP_EMAIL"].ToString();
                Employee.AVP_POSITION = dr["AVP_POSITION"].ToString();
                Employee.AVP_POSITION_FULL = dr["AVP_POSITION_FULL"].ToString();

                Employee.VP_ID = dr["VP_ID"].ToString();
                Employee.VP_NAME = dr["VP_NAME"].ToString();
                Employee.VP_EMAIL = dr["VP_EMAIL"].ToString();
                Employee.VP_POSITION = dr["VP_POSITION"].ToString();
                Employee.VP_POSITION_FULL = dr["VP_POSITION_FULL"].ToString();

            }
            if (isDev.ToLower() == "true")
            {
                Employee.PERNR = "z" + Employee.PERNR;
                Employee.SECTION_HEAD_ID = "z" + Employee.SECTION_HEAD_ID;
                Employee.MANAGER_ID = "z" + Employee.MANAGER_ID;
                Employee.DIRECTOR_ID = "z" + Employee.DIRECTOR_ID;
                Employee.AVP_ID = "z" + Employee.AVP_ID;
                Employee.VP_ID = "z" + Employee.VP_ID;

            }
            return Employee;
        }
        public Employee getInFoByEmpPosition(string xEMP_Position)
        {

            string sql = ""; //"select * from V_SYN_EMPLOYEE where MC_SHORT = '" + xEMP_Position + "'";

            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.MC_SHORT = '" + xEMP_Position + "' ";
            //DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                Employee.PERNR = dr["PERNR"].ToString();
                Employee.SNAME = dr["SNAME"].ToString();
                Employee.EMAIL = dr["EMAIL"].ToString();
                if (dr["ORGLEVEL"].ToString() == "5")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME5"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "4")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME4"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "3")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME3"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "2")
                {
                    Employee.POSITION = dr["ORGNAME2"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "1")
                {
                    Employee.POSITION = dr["ORGNAME1"].ToString();
                }
                Employee.MC_SHORT = dr["MC_SHORT"].ToString();
                Employee.MC_STEXT = dr["MC_STEXT"].ToString();
                Employee.ORGLEVEL = dr["ORGLEVEL"].ToString();
                Employee.ORGSNAME1 = dr["ORGSNAME1"].ToString();
                Employee.ORGSNAME2 = dr["ORGSNAME2"].ToString();
                Employee.ORGSNAME3 = dr["ORGSNAME3"].ToString();
                Employee.ORGSNAME4 = dr["ORGSNAME4"].ToString();
                Employee.ORGSNAME5 = dr["ORGSNAME5"].ToString(); Employee.ORGNAME1 = dr["ORGNAME1"].ToString();
                Employee.ORGNAME2 = dr["ORGNAME2"].ToString();
                Employee.ORGNAME3 = dr["ORGNAME3"].ToString();
                Employee.ORGNAME4 = dr["ORGNAME4"].ToString();
                Employee.ORGNAME5 = dr["ORGNAME5"].ToString();

                Employee.SECTION_HEAD_ID = dr["SECTION_HEAD_ID"].ToString();
                Employee.SECTION_HEAD_NAME = dr["SECTION_HEAD_NAME"].ToString();
                Employee.SECTION_HEAD_EMAIL = dr["SECTION_HEAD_EMAIL"].ToString();
                Employee.SECTION_HEAD_POSITION = dr["SECTION_HEAD_POSITION"].ToString();
                Employee.SECTION_HEAD_POSITION_FULL = dr["SECTION_HEAD_POSITION_FULL"].ToString() + dr["ORGNAME5"].ToString();

                Employee.MANAGER_ID = dr["MANAGER_ID"].ToString();
                Employee.MANAGER_NAME = dr["MANAGER_NAME"].ToString();
                Employee.MANAGER_EMAIL = dr["MANAGER_EMAIL"].ToString();
                Employee.MANAGER_POSITION = dr["MANAGER_POSITION"].ToString();
                Employee.MANAGER_POSITION_FULL = dr["MANAGER_POSITION_FULL"].ToString();

                Employee.DIRECTOR_ID = dr["DIRECTOR_ID"].ToString();
                Employee.DIRECTOR_NAME = dr["DIRECTOR_NAME"].ToString();
                Employee.DIRECTOR_EMAIL = dr["DIRECTOR_EMAIL"].ToString();
                Employee.DIRECTOR_POSITION = dr["DIRECTOR_POSITION"].ToString();
                Employee.DIRECTOR_POSITION_FULL = dr["DIRECTOR_POSITION_FULL"].ToString();


                Employee.AVP_ID = dr["AVP_ID"].ToString();
                Employee.AVP_NAME = dr["AVP_NAME"].ToString();
                Employee.AVP_EMAIL = dr["AVP_EMAIL"].ToString();
                Employee.AVP_POSITION = dr["AVP_POSITION"].ToString();
                Employee.AVP_POSITION_FULL = dr["AVP_POSITION_FULL"].ToString();

                Employee.VP_ID = dr["VP_ID"].ToString();
                Employee.VP_NAME = dr["VP_NAME"].ToString();
                Employee.VP_EMAIL = dr["VP_EMAIL"].ToString();
                Employee.VP_POSITION = dr["VP_POSITION"].ToString();
                Employee.VP_POSITION_FULL = dr["VP_POSITION_FULL"].ToString();
            }
            if (isDev.ToLower() == "true")
            {
                Employee.PERNR = "z" + Employee.PERNR;
                Employee.MANAGER_ID = "z" + Employee.MANAGER_ID;
                Employee.SECTION_HEAD_ID = "z" + Employee.SECTION_HEAD_ID;
                Employee.AVP_ID = "z" + Employee.AVP_ID;
                Employee.VP_ID = "z" + Employee.VP_ID;

            }
            return Employee;
        }
        public string[] getEmpListInDepartment_WithoutMD(string xOrgName)
        {
            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME4 = '" + xOrgName + "'";

            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME4 = '" + xOrgName + "'  and  e.MC_SHORT <> '" + xOrgName + "' ";

            //DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString();
                }
                else
                {
                    xEmpList[i] = dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString();
                }
            }
            return xEmpList;
        }
        public string[] getEmpListInDepartment(string xOrgName)
        {
            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME4 = '" + xOrgName + "'";

            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME4 = '" + xOrgName + "' ";

            //DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString();
                }
                else
                {
                    xEmpList[i] = dr["PERNR"].ToString();
                }
            }
            return xEmpList;
        }
        public string[] getEmpIDAndNameInDepartment(string xOrgName)
        {
            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME4 = '" + xOrgName + "'";

            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME4 = '" + xOrgName + @"' 
            order by e.SNAME";
            //DataTable dt = zdbUtil._findDT(sql, strConn_PS);'
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString() + ":" + dr["MC_SHORT"].ToString();
                }
                else
                {
                    xEmpList[i] = dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString() + ":" + dr["MC_SHORT"].ToString();
                }
            }
            return xEmpList;
        }
        public string[] getEmpIDAndNameSectionHead(string xDep)
        {
            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME4 = '" + xDep + "' and ORGSNAME5 = MC_SHORT ";
            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME4 = '" + xDep + "'   and e.ORGSNAME5 = e.MC_SHORT ";
            // DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString() + ":" + dr["MC_SHORT"].ToString();
                }
                else
                {
                    xEmpList[i] = dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString() + ":" + dr["MC_SHORT"].ToString();
                }
            }
            return xEmpList;
        }
        public string[] getEmpIDAndNameDepHead(string xDiv)
        {
            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME3 = '" + xDiv + "' and ORGSNAME4 = MC_SHORT ";
            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME3 = '" + xDiv + "' and e.ORGSNAME4 = e.MC_SHORT ";
            // DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString() + ":" + dr["MC_SHORT"].ToString();
                }
                else
                {
                    xEmpList[i] = dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString() + ":" + dr["MC_SHORT"].ToString();
                }
            }
            return xEmpList;
        }
        public string[] getEmpListInSection(string xOrgName)
        {

            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME5 = '" + xOrgName + "'";
            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME5 = '" + xOrgName + "' ";
            // DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString();

                }
                else { xEmpList[i] = dr["PERNR"].ToString(); }
            }
            return xEmpList;
        }
        public string[] getEmpIDAndNameInSection(string xOrgName)
        {

            string sql = "";// "select * from V_SYN_EMPLOYEE where ORGSNAME5 = '" + xOrgName + "'";
            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            where e.ORGSNAME5 = '" + xOrgName + "' ";
            // DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];

                if (isDev.ToLower() == "true")
                {
                    xEmpList[i] = "z" + dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString();

                }
                else { xEmpList[i] = dr["PERNR"].ToString() + ":" + dr["SNAME"].ToString(); }
            }
            return xEmpList;
        }
        public string findAD(string xDepShortName)
        {
            string xEmpID = "";
            string sql = "";
            // select* from V_SYN_EMPLOYEE where MC_SHORT like 'ช.อ%' and ORGSNAME3 like '" + xDepShortName + "%'";

            sql = @"select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)

            where e.MC_SHORT like 'ช.อ%' and e.ORGSNAME3 like '" + xDepShortName + "%' ";
            //DataTable dt = zdbUtil._findDT(sql, strConn_PS);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xEmpID = dr["PERNR"].ToString();
                //Employee.PERNR = dr["PERNR"].ToString();
                //Employee.SNAME = dr["SNAME"].ToString();
                //Employee.EMAIL = dr["EMAIL"].ToString();
                //Employee.MC_SHORT = dr["MC_SHORT"].ToString();
                //Employee.MC_STEXT = dr["MC_STEXT"].ToString();
                //Employee.ORGLEVEL = dr["ORGLEVEL"].ToString();
                //Employee.ORGSNAME1 = dr["ORGSNAME1"].ToString();
                //Employee.ORGSNAME2 = dr["ORGSNAME2"].ToString();
                //Employee.ORGSNAME3 = dr["ORGSNAME3"].ToString();
                //Employee.ORGSNAME4 = dr["ORGSNAME4"].ToString();
                //Employee.ORGSNAME5 = dr["ORGSNAME5"].ToString();
                //Employee.SECTION_HEAD_ID = dr["SECTION_HEAD_ID"].ToString();
                //Employee.SECTION_HEAD_NAME = dr["SECTION_HEAD_NAME"].ToString();
                //Employee.SECTION_HEAD_EMAIL = dr["SECTION_HEAD_EMAIL"].ToString();
                //Employee.SECTION_HEAD_POSITION = dr["SECTION_HEAD_POSITION"].ToString();
                //Employee.MANAGER_ID = dr["MANAGER_ID"].ToString();
                //Employee.MANAGER_NAME = dr["MANAGER_NAME"].ToString();
                //Employee.MANAGER_EMAIL = dr["MANAGER_EMAIL"].ToString();
                //Employee.MANAGER_POSITION = dr["MANAGER_POSITION"].ToString();
                //Employee.DIRECTOR_ID = dr["DIRECTOR_ID"].ToString();
                //Employee.DIRECTOR_NAME = dr["DIRECTOR_NAME"].ToString();
                //Employee.DIRECTOR_EMAIL = dr["DIRECTOR_EMAIL"].ToString();
                //Employee.DIRECTOR_POSITION = dr["DIRECTOR_POSITION"].ToString();
                //Employee.AVP_ID = dr["AVP_ID"].ToString();
                //Employee.AVP_NAME = dr["AVP_NAME"].ToString();
                //Employee.AVP_EMAIL = dr["AVP_EMAIL"].ToString();
                //Employee.AVP_POSITION = dr["AVP_POSITION"].ToString();
                //Employee.VP_ID = dr["VP_ID"].ToString();
                //Employee.VP_NAME = dr["VP_NAME"].ToString();
                //Employee.VP_EMAIL = dr["VP_EMAIL"].ToString();
                //Employee.VP_POSITION = dr["VP_POSITION"].ToString();
            }
            if (isDev.ToLower() == "true")
            {
                xEmpID = "z" + xEmpID;
                //Employee.PERNR = "z" + Employee.PERNR;
                //Employee.MANAGER_ID = "z" + Employee.MANAGER_ID;
                //Employee.SECTION_HEAD_ID = "z" + Employee.SECTION_HEAD_ID;
                //Employee.AVP_ID = "z" + Employee.AVP_ID;
                //Employee.VP_ID = "z" + Employee.VP_ID;

            }
            return xEmpID;
        }
        public List<Employee> getProjectManager()
        {
            List<Employee> liemp = new List<Employee>();
            Employee emp = new Employee();
            string sql = @"select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            Where e.MC_STEXT like 'หัวหน้าโครงการ%' and e.ORGSNAME1 like 'รวส%'
            ";
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            string[] xEmpList = new string[dt.Rows.Count];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i];
                emp = new Employee();
                emp = getInFoByEmpID(dr["PERNR"].ToString());
                liemp.Add(emp);

            }
            return liemp;
        }
        public string getEmailListByPositionList(string xEMP_Position)
        {
            string sql = "";

            sql = @" select e.EMAIL
                    from M_SYN_EMPLOYEE e
                    where e.MC_SHORT in ('" + xEMP_Position.Replace(",", "','") + "') ";
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];

            //string[] arrEmail = dt.Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
            //string strEmailList = String.Join(";", arrEmail);

            var rows = dt.AsEnumerable().Select(r => string.Format("{0}", string.Join(",", r.ItemArray)));

            string strEmailList = string.Join(";", rows.ToArray());

            return strEmailList;
        }
        public DataTable getEmailListByPositionListSupNotice(string xEMP_Position)
        {
            string sql = "";
           
            sql = @" select e.EMAIL,e.PERNR
                    from M_SYN_EMPLOYEE e
                    where e.MC_SHORT in ('" + xEMP_Position.Replace(",", "','") + "') ";
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];

            //string[] arrEmail = dt.Rows.OfType<DataRow>().Select(k => k[0].ToString()).ToArray();
            //string strEmailList = String.Join(";", arrEmail);

            //var rows = dt.AsEnumerable().Select(r => string.Format("{0}", string.Join(",", r.ItemArray)));

            //string strEmailList = string.Join(";", rows.ToArray());

            return dt;
        }
        public Employee getInFoByEmpID_PM(string xEMP_ID)
        {
            if (isDev.ToLower() == "true") { xEMP_ID = xEMP_ID.Replace("z", ""); }
            string sql = "";// "select * from V_SYN_EMPLOYEE where pernr = '"+xEMP_ID+"'";

            sql = @" select e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            
            where e.PERNR = '" + xEMP_ID + "' and e.MC_SHORT like 'หก-%'";

            // DataTable dt = zdbUtil._findDT(sql, zetl4eisdb);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                Employee.PERNR = dr["PERNR"].ToString();
                Employee.SNAME = dr["SNAME"].ToString();
                Employee.EMAIL = dr["EMAIL"].ToString();
                if (dr["ORGLEVEL"].ToString() == "5")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME5"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "4")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME4"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "3")
                {
                    Employee.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME3"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "2")
                {
                    Employee.POSITION = dr["ORGNAME2"].ToString();
                }
                else if (dr["ORGLEVEL"].ToString() == "1")
                {
                    Employee.POSITION = dr["ORGNAME1"].ToString();
                }
                Employee.MC_SHORT = dr["MC_SHORT"].ToString();
                Employee.MC_STEXT = dr["MC_STEXT"].ToString();
                Employee.ORGLEVEL = dr["ORGLEVEL"].ToString();
                Employee.ORGSNAME1 = dr["ORGSNAME1"].ToString();
                Employee.ORGSNAME2 = dr["ORGSNAME2"].ToString();
                Employee.ORGSNAME3 = dr["ORGSNAME3"].ToString();
                Employee.ORGSNAME4 = dr["ORGSNAME4"].ToString();
                Employee.ORGSNAME5 = dr["ORGSNAME5"].ToString();
                Employee.ORGNAME1 = dr["ORGNAME1"].ToString();
                Employee.ORGNAME2 = dr["ORGNAME2"].ToString();
                Employee.ORGNAME3 = dr["ORGNAME3"].ToString();
                Employee.ORGNAME4 = dr["ORGNAME4"].ToString();
                Employee.ORGNAME5 = dr["ORGNAME5"].ToString();

                Employee.SECTION_HEAD_ID = dr["SECTION_HEAD_ID"].ToString();
                Employee.SECTION_HEAD_NAME = dr["SECTION_HEAD_NAME"].ToString();
                Employee.SECTION_HEAD_EMAIL = dr["SECTION_HEAD_EMAIL"].ToString();
                Employee.SECTION_HEAD_POSITION = dr["SECTION_HEAD_POSITION"].ToString();
                Employee.SECTION_HEAD_POSITION_FULL = dr["SECTION_HEAD_POSITION_FULL"].ToString() + dr["ORGNAME5"].ToString();

                Employee.MANAGER_ID = dr["MANAGER_ID"].ToString();
                Employee.MANAGER_NAME = dr["MANAGER_NAME"].ToString();
                Employee.MANAGER_EMAIL = dr["MANAGER_EMAIL"].ToString();
                Employee.MANAGER_POSITION = dr["MANAGER_POSITION"].ToString();
                Employee.MANAGER_POSITION_FULL = dr["MANAGER_POSITION_FULL"].ToString();

                Employee.DIRECTOR_ID = dr["DIRECTOR_ID"].ToString();
                Employee.DIRECTOR_NAME = dr["DIRECTOR_NAME"].ToString();
                Employee.DIRECTOR_EMAIL = dr["DIRECTOR_EMAIL"].ToString();
                Employee.DIRECTOR_POSITION = dr["DIRECTOR_POSITION"].ToString();
                Employee.DIRECTOR_POSITION_FULL = dr["DIRECTOR_POSITION_FULL"].ToString();

                Employee.AVP_ID = dr["AVP_ID"].ToString();
                Employee.AVP_NAME = dr["AVP_NAME"].ToString();
                Employee.AVP_EMAIL = dr["AVP_EMAIL"].ToString();
                Employee.AVP_POSITION = dr["AVP_POSITION"].ToString();
                Employee.AVP_POSITION_FULL = dr["AVP_POSITION_FULL"].ToString();

                Employee.VP_ID = dr["VP_ID"].ToString();
                Employee.VP_NAME = dr["VP_NAME"].ToString();
                Employee.VP_EMAIL = dr["VP_EMAIL"].ToString();
                Employee.VP_POSITION = dr["VP_POSITION"].ToString();
                Employee.VP_POSITION_FULL = dr["VP_POSITION_FULL"].ToString();

            }
            if (isDev.ToLower() == "true")
            {
                Employee.PERNR = "z" + Employee.PERNR;
                Employee.SECTION_HEAD_ID = "z" + Employee.SECTION_HEAD_ID;
                Employee.MANAGER_ID = "z" + Employee.MANAGER_ID;
                Employee.DIRECTOR_ID = "z" + Employee.DIRECTOR_ID;
                Employee.AVP_ID = "z" + Employee.AVP_ID;
                Employee.VP_ID = "z" + Employee.VP_ID;

            }
            return Employee;
        }
        public List<Employee> getInFoByOrgName_PM(string xORGSName3, string xORGLevel)
        {
            List<Employee> liemp = new List<Employee>();
            Employee emp = new Employee();
            string sql = "";

            sql = @" select distinct e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
            , s.PERNR SECTION_HEAD_ID, s.SNAME SECTION_HEAD_NAME, s.EMAIL SECTION_HEAD_EMAIL, s.MC_SHORT SECTION_HEAD_POSITION,  (s.MC_STEXT || ' ' || e.ORGNAME5) SECTION_HEAD_POSITION_FULL   
            , m.PERNR MANAGER_ID, m.SNAME MANAGER_NAME, m.EMAIL MANAGER_EMAIL, m.MC_SHORT MANAGER_POSITION,  (m.MC_STEXT ||' '|| e.ORGNAME4) MANAGER_POSITION_FULL             
            , d.PERNR DIRECTOR_ID, d.SNAME DIRECTOR_NAME, d.EMAIL DIRECTOR_EMAIL, d.MC_SHORT DIRECTOR_POSITION,   (d.MC_STEXT||' ' || d.ORGNAME3) DIRECTOR_POSITION_FULL  
            , a.PERNR AVP_ID, a.SNAME AVP_NAME, a.EMAIL AVP_EMAIL, a.MC_SHORT AVP_POSITION,   (a.ORGNAME2) AVP_POSITION_FULL  
            , v.PERNR VP_ID, v.SNAME VP_NAME, v.EMAIL VP_EMAIL, v.MC_SHORT VP_POSITION,   (v.ORGNAME1) VP_POSITION_FULL  
            from M_SYN_EMPLOYEE e
            left
            join M_SYN_EMPLOYEE s on (e.ORGSNAME5 = s.MC_SHORT)
            left
            join M_SYN_EMPLOYEE m on (e.ORGSNAME4 = m.MC_SHORT)
            left
            join M_SYN_EMPLOYEE d on (e.ORGSNAME3 = d.MC_SHORT)
            left
            join M_SYN_EMPLOYEE a on (e.ORGSNAME2 = a.MC_SHORT)
            left
            join M_SYN_EMPLOYEE v on (e.ORGSNAME1 = v.MC_SHORT)
            
            where e.ORGSNAME3 = '" + xORGSName3 + "' and e.ORGLEVEL > " + xORGLevel;

            // DataTable dt = zdbUtil._findDT(sql, zetl4eisdb);
            DataTable dt = zoraDBUtil._getDS(sql, strConn_Common).Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow dr = dt.Rows[i];
                    emp = new Employee();
                    emp.PERNR = dr["PERNR"].ToString();
                    emp.SNAME = dr["SNAME"].ToString();
                    emp.EMAIL = dr["EMAIL"].ToString();
                    if (dr["ORGLEVEL"].ToString() == "5")
                    {
                        emp.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME5"].ToString();
                    }
                    else if (dr["ORGLEVEL"].ToString() == "4")
                    {
                        emp.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME4"].ToString();
                    }
                    else if (dr["ORGLEVEL"].ToString() == "3")
                    {
                        emp.POSITION = dr["MC_STEXT"].ToString() + " " + dr["ORGNAME3"].ToString();
                    }
                    else if (dr["ORGLEVEL"].ToString() == "2")
                    {
                        emp.POSITION = dr["ORGNAME2"].ToString();
                    }
                    else if (dr["ORGLEVEL"].ToString() == "1")
                    {
                        emp.POSITION = dr["ORGNAME1"].ToString();
                    }
                    emp.MC_SHORT = dr["MC_SHORT"].ToString();
                    emp.MC_STEXT = dr["MC_STEXT"].ToString();
                    emp.ORGLEVEL = dr["ORGLEVEL"].ToString();
                    emp.ORGSNAME1 = dr["ORGSNAME1"].ToString();
                    emp.ORGSNAME2 = dr["ORGSNAME2"].ToString();
                    emp.ORGSNAME3 = dr["ORGSNAME3"].ToString();
                    emp.ORGSNAME4 = dr["ORGSNAME4"].ToString();
                    emp.ORGSNAME5 = dr["ORGSNAME5"].ToString();
                    emp.ORGNAME1 = dr["ORGNAME1"].ToString();
                    emp.ORGNAME2 = dr["ORGNAME2"].ToString();
                    emp.ORGNAME3 = dr["ORGNAME3"].ToString();
                    emp.ORGNAME4 = dr["ORGNAME4"].ToString();
                    emp.ORGNAME5 = dr["ORGNAME5"].ToString();

                    emp.SECTION_HEAD_ID = dr["SECTION_HEAD_ID"].ToString();
                    emp.SECTION_HEAD_NAME = dr["SECTION_HEAD_NAME"].ToString();
                    emp.SECTION_HEAD_EMAIL = dr["SECTION_HEAD_EMAIL"].ToString();
                    emp.SECTION_HEAD_POSITION = dr["SECTION_HEAD_POSITION"].ToString();
                    emp.SECTION_HEAD_POSITION_FULL = dr["SECTION_HEAD_POSITION_FULL"].ToString() + dr["ORGNAME5"].ToString();

                    emp.MANAGER_ID = dr["MANAGER_ID"].ToString();
                    emp.MANAGER_NAME = dr["MANAGER_NAME"].ToString();
                    emp.MANAGER_EMAIL = dr["MANAGER_EMAIL"].ToString();
                    emp.MANAGER_POSITION = dr["MANAGER_POSITION"].ToString();
                    emp.MANAGER_POSITION_FULL = dr["MANAGER_POSITION_FULL"].ToString();

                    emp.DIRECTOR_ID = dr["DIRECTOR_ID"].ToString();
                    emp.DIRECTOR_NAME = dr["DIRECTOR_NAME"].ToString();
                    emp.DIRECTOR_EMAIL = dr["DIRECTOR_EMAIL"].ToString();
                    emp.DIRECTOR_POSITION = dr["DIRECTOR_POSITION"].ToString();
                    emp.DIRECTOR_POSITION_FULL = dr["DIRECTOR_POSITION_FULL"].ToString();

                    emp.AVP_ID = dr["AVP_ID"].ToString();
                    emp.AVP_NAME = dr["AVP_NAME"].ToString();
                    emp.AVP_EMAIL = dr["AVP_EMAIL"].ToString();
                    emp.AVP_POSITION = dr["AVP_POSITION"].ToString();
                    emp.AVP_POSITION_FULL = dr["AVP_POSITION_FULL"].ToString();

                    emp.VP_ID = dr["VP_ID"].ToString();
                    emp.VP_NAME = dr["VP_NAME"].ToString();
                    emp.VP_EMAIL = dr["VP_EMAIL"].ToString();
                    emp.VP_POSITION = dr["VP_POSITION"].ToString();
                    emp.VP_POSITION_FULL = dr["VP_POSITION_FULL"].ToString();

                    if (isDev.ToLower() == "true")
                    {
                        emp.PERNR = "z" + emp.PERNR;
                        emp.SECTION_HEAD_ID = "z" + emp.SECTION_HEAD_ID;
                        emp.MANAGER_ID = "z" + emp.MANAGER_ID;
                        emp.DIRECTOR_ID = "z" + emp.DIRECTOR_ID;
                        emp.AVP_ID = "z" + emp.AVP_ID;
                        emp.VP_ID = "z" + emp.VP_ID;

                    }
                    liemp.Add(emp);
                }
               
            }

            return liemp;
        }
    }
    public class Employee
    {

        public string PERNR { get; set; }
        public string SNAME { get; set; }
        public string EMAIL { get; set; }
        public string POSITION { get; set; }
        public string MC_SHORT { get; set; }
        public string MC_STEXT { get; set; }
        public string ORGLEVEL { get; set; }
        public string ORGSNAME1 { get; set; }
        public string ORGSNAME2 { get; set; }
        public string ORGSNAME3 { get; set; }
        public string ORGSNAME4 { get; set; }
        public string ORGSNAME5 { get; set; }
        public string ORGNAME1 { get; set; }
        public string ORGNAME2 { get; set; }
        public string ORGNAME3 { get; set; }
        public string ORGNAME4 { get; set; }
        public string ORGNAME5 { get; set; }
        public string SECTION_HEAD_ID { get; set; }
        public string SECTION_HEAD_NAME { get; set; }
        public string SECTION_HEAD_EMAIL { get; set; }
        public string SECTION_HEAD_POSITION { get; set; }
        public string SECTION_HEAD_POSITION_FULL { get; set; }
        public string MANAGER_ID { get; set; }
        public string MANAGER_NAME { get; set; }
        public string MANAGER_EMAIL { get; set; }
        public string MANAGER_POSITION { get; set; }
        public string MANAGER_POSITION_FULL { get; set; }
        public string DIRECTOR_ID { get; set; }
        public string DIRECTOR_NAME { get; set; }
        public string DIRECTOR_EMAIL { get; set; }
        public string DIRECTOR_POSITION { get; set; }
        public string DIRECTOR_POSITION_FULL { get; set; }
        public string AVP_ID { get; set; }
        public string AVP_NAME { get; set; }
        public string AVP_EMAIL { get; set; }
        public string AVP_POSITION { get; set; }
        public string AVP_POSITION_FULL { get; set; }
        public string VP_ID { get; set; }
        public string VP_NAME { get; set; }
        public string VP_EMAIL { get; set; }
        public string VP_POSITION { get; set; }
        public string VP_POSITION_FULL { get; set; }
    }
}