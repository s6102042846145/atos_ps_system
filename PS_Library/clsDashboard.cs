﻿using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsDashboard
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        public DataTable getDDL(string type)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                if (type != "") sql = @"select distinct bid_no,bid_revision from ps_t_bid_selling_cmpo_item ";
                else sql = @"select * from ps_t_bid_selling_cmpo_item where schedule_name <>''";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataDB(string search)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                //sql = @"SELECT bid_no,item_no,equip_code,po_item_desc from ps_t_bid_selling_cmpo_item";
                sql = @"SELECT * from ps_t_bid_selling_cmpo_item ";
                sql += search == "" ? " where 1=0" : "where " + search;
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        
        public DataTable getDataINV(string po_number, string po_item, string hist_type)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                /*
                sql = @" select row_id,doc_year,mat_doc,matdoc_itm,FORMAT(pstng_date, 'dd-MM-yy') pstng_date,quantity,FORMAT(val_loccur, 'C') val_loccur,currency ";
                sql += " from ps_t_sap_postatus_history where hist_type='" + hist_type + "' and po_number='" + po_number + "' and po_item='" + po_item + "' ";
                sql += " order by row_id desc";
                */
                sql = @"select a.row_id 
                        ,b.doc_year,b.mat_doc,b.matdoc_itm
                        ,FORMAT(b.pstng_date, 'dd-MM-yy') pstng_date
                        ,a.po_qty quantity
                        ,FORMAT(b.val_loccur, 'C') val_loccur
                        ,a.po_currency currency
                        from ps_t_bid_selling_cmpo_item a
                        left join ps_t_sap_postatus_history b on a.sap_po_num=b.po_number
                        where a.sap_po_num = '" + po_number + "' and a.po_item_no='" + po_item + "' ";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataGoodsReceive(string po_number, string po_item, string hist_type)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                
                sql = @" select row_id,doc_year,mat_doc,matdoc_itm,FORMAT(pstng_date, 'dd-MM-yy') pstng_date,quantity,FORMAT(val_loccur, 'C') val_loccur,currency ";
                sql += " from ps_t_sap_postatus_history where hist_type='" + hist_type + "' and po_number='" + po_number + "' and po_item='" + po_item + "' ";
                sql += " order by row_id desc";
                
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public DataTable getDataSHIPMENT(string po_number, string po_item)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                //sql = @"select row_id,FORMAT(delivery_date, 'dd-MM-yy') delivery_date,quantity from ps_t_sap_postatus_poschedule where po_number='" + po_number + "' and po_item='" + po_item + "' ";
                sql = @"select row_id,FORMAT(delivery_date, 'dd-MM-yy') delivery_date,quantity from ps_t_sap_postatus_poschedule where po_number='" + po_number + "' and po_item='" + po_item + "' ";
                sql += " order by row_id desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        private string getTemplateID(string xbid_no, string xbid_revision, string xschedule_name)
        {
            string xtemplate_id = "";
            string sql = @"   select * from ps_xls_template 
                  where bid_no = '" + xbid_no + "' and bid_revision = '" + xbid_revision + "' and schedule_name = '" + xschedule_name + @"' 
                  order by rowid desc ";
            var ds = db.ExecSql_DataSet(sql, zetl4eisdb);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                xtemplate_id = dr["template_id"].ToString();
            }
            return xtemplate_id;
        }

        public DataTable getDataDBQC(string BidNo, string BidRevision, string ScheduleName, string ContractNo)
        {
            DataTable dt = new DataTable();
            try
            {
                var xtemplate_id = getTemplateID(BidNo, BidRevision, ScheduleName);
                string sql = @"SELECT top 5 a.*,b.contract_no 
                        ,isnull(c.ms,0) ms,isnull(c.qp,0) qp,isnull(c.ts,0) ts,isnull(c.tp,0) tp
                        ,isnull(c.iw,0) iw,isnull(c.ir,0) ir,isnull(c.fir,0) fir,isnull(c.cr,0) cr,isnull(c.tr,0) tr
                        FROM [dbo].[ps_xls_template] a
                        left join ps_t_bid_selling_cmpo b on 
                        (a.bid_no = b.bid_no and a.bid_revision = b.bid_revision 
                        and a.schedule_name = b.schedule_name and b.vendor_type!='vendor')
                        left join ps_t_qc_item c on (a.bid_no=c.bid_no and a.bid_revision=c.bid_revision
                        and b.contract_no=c.contract_no and a.schedule_name=c.schedule_name and a.item_no=c.item_no)
                 --where b.contract_no='" + ContractNo + "' and a.template_id = '" + xtemplate_id + "' ";

                sql += " order by a.part, a.item_no ";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

                //    var ds = db.ExecSql_DataSet(sql, zetl4eisdb);
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        gv1.DataSource = ds.Tables[0];
                //        gv1.DataBind();

                //    }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;







            //string sql = "";
            //try
            //{
            //    ////sql = @"SELECT bid_no,item_no,equip_code,po_item_desc from ps_t_bid_selling_cmpo_item";
            //    //sql = @"SELECT * from ps_t_bid_selling_cmpo_item ";
            //    //sql += search == "" ? " where 1=0" : "where " + search;
            //    //dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            //    //sql = @"SELECT a.*,b.contract_no FROM [dbo].[ps_xls_template] a
            //    //        left join ps_t_bid_selling_cmpo_item b on a.equip_code=b.equip_code
            //    //"; 


            //    //sql = @"select a.* ,b.contract_no,b.contract_no
            //    //        ,b.ms,b.qp,b.ts,b.tp,b.iw,b.ir,b.fir,b.cr,b.tr
            //    //        from ps_xls_template a
            //    //        --INNER JOIN ps_t_qc_item b on (
            //    //        left join ps_t_qc_item b on (
            //    //        a.bid_no=b.bid_no 
            //    //        and a.bid_revision=b.bid_revision 
            //    //        and a.item_no=b.item_no 
            //    //        and a.schedule_name=b.schedule_name 
            //    //        and a.part=b.part)
            //    //";
            //    //if (search != "") sql += " where b.contract_no='" + search +"'";
            //    //sql += " order by a.rowid desc";




            //    dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            //}
            //catch (Exception ex)
            //{
            //    LogHelper.WriteEx(ex);
            //}
        }
        public DataTable getDataDBQC2(string search,string job_no)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                ////sql = @"SELECT bid_no,item_no,equip_code,po_item_desc from ps_t_bid_selling_cmpo_item";
                //sql = @"SELECT * from ps_t_bid_selling_cmpo_item ";
                //sql += search == "" ? " where 1=0" : "where " + search;
                //dt = db.ExecSql_DataTable(sql, zetl4eisdb);

                //sql = @"select a.contract_no,a.schedule_name,
                //        concat(DAY(a.loa_date),'  ', CASE MONTH(a.loa_date)
                //         WHEN '1' THEN 'มกราคม'
                //         WHEN '2' THEN 'กุมภาพันธ์'
                //         WHEN '3' THEN 'มีนาคม'
                //         WHEN '4' THEN 'เมษายน'
                //         WHEN '5' THEN 'พฤษภาคม'
                //         WHEN '6' THEN 'มิถุนายน'
                //         WHEN '7' THEN 'กรกฎาคม'
                //         WHEN '8' THEN 'สิงหาคม'
                //         WHEN '9' THEN 'กันยายน'
                //         WHEN '10' THEN 'ตุลาคม'
                //         WHEN '11' THEN 'พฤศจิกายน'
                //         ELSE 'ธันวาคม' END ,'  ',YEAR(a.loa_date)) loa_date,
                //        concat(DAY(a.loa_confirm_date),'  ', CASE MONTH(a.loa_confirm_date)
                //         WHEN '1' THEN 'มกราคม'
                //         WHEN '2' THEN 'กุมภาพันธ์'
                //         WHEN '3' THEN 'มีนาคม'
                //         WHEN '4' THEN 'เมษายน'
                //         WHEN '5' THEN 'พฤษภาคม'
                //         WHEN '6' THEN 'มิถุนายน'
                //         WHEN '7' THEN 'กรกฎาคม'
                //         WHEN '8' THEN 'สิงหาคม'
                //         WHEN '9' THEN 'กันยายน'
                //         WHEN '10' THEN 'ตุลาคม'
                //         WHEN '11' THEN 'พฤศจิกายน'
                //         ELSE 'ธันวาคม' END ,'  ',YEAR(a.loa_confirm_date)) loa_confirm_date
                //        from ps_t_bid_selling_cmpo a";
                //if (search != "") sql += " where a.contract_no='" + search + "'";
                //sql += " order by row_id";

                sql = @"select a.row_id,a.contract_no,a.schedule_name,a.bid_no,a.bid_revision,
                        concat(DAY(a.loa_date),'  ', CASE MONTH(a.loa_date)
                         WHEN '1' THEN 'มกราคม'
                         WHEN '2' THEN 'กุมภาพันธ์'
                         WHEN '3' THEN 'มีนาคม'
                         WHEN '4' THEN 'เมษายน'
                         WHEN '5' THEN 'พฤษภาคม'
                         WHEN '6' THEN 'มิถุนายน'
                         WHEN '7' THEN 'กรกฎาคม'
                         WHEN '8' THEN 'สิงหาคม'
                         WHEN '9' THEN 'กันยายน'
                         WHEN '10' THEN 'ตุลาคม'
                         WHEN '11' THEN 'พฤศจิกายน'
                         ELSE 'ธันวาคม' END ,'  ',YEAR(a.loa_date)) loa_date,
                        concat(DAY(a.loa_confirm_date),'  ', CASE MONTH(a.loa_confirm_date)
                         WHEN '1' THEN 'มกราคม'
                         WHEN '2' THEN 'กุมภาพันธ์'
                         WHEN '3' THEN 'มีนาคม'
                         WHEN '4' THEN 'เมษายน'
                         WHEN '5' THEN 'พฤษภาคม'
                         WHEN '6' THEN 'มิถุนายน'
                         WHEN '7' THEN 'กรกฎาคม'
                         WHEN '8' THEN 'สิงหาคม'
                         WHEN '9' THEN 'กันยายน'
                         WHEN '10' THEN 'ตุลาคม'
                         WHEN '11' THEN 'พฤศจิกายน'
                         ELSE 'ธันวาคม' END ,'  ',YEAR(a.loa_confirm_date)) loa_confirm_date
                        ,  '<table cellspacing=0 border=1 style=border-color:LightGrey;width:100%;border-collapse:collapse;><tr><td style=width:115px;color:White;background-color:lightslategrey;text-align:center;>Job No.</td><td style=width:10px;color:White;background-color:lightslategrey;text-align:center;>Rev.</td><td style=width:115px;color:White;background-color:lightslategrey;text-align: center;>Completion Date</td></tr>' + replace(replace((
                                        SELECT distinct '<tr><td style=text-align:center;>' + job.job_no + '</td><td style=text-align: center;>' + convert(nvarchar, job.revision) + '</td><td style=text-align:center;>' + job.k_display_date + '</td></tr>'
                                        FROM bid_job_relate relate inner join job_no job on relate.job_id = job.job_id ";
                sql += "                WHERE relate.bid_id = bid.bid_id and relate.status in ('New')  ";
                if (job_no != "") {
                    sql += "                and job.job_no='"+ job_no + "' ";
                }
                sql += @"               and job.revision = (SELECT max(revision) from job_no job2 where (job2.job_no = job.job_no) and LOWER(job2.job_status) not in ('new') ) 
                                        FOR XML PATH('')
                            ), '&lt;', '<'), '&gt;', '>') + '</table>' as  job_no,b.assign_to 
                        ,concat(DAY(b.shipment_date),'  ', CASE MONTH(b.shipment_date)
                         WHEN '1' THEN 'มกราคม'
                         WHEN '2' THEN 'กุมภาพันธ์'
                         WHEN '3' THEN 'มีนาคม'
                         WHEN '4' THEN 'เมษายน'
                         WHEN '5' THEN 'พฤษภาคม'
                         WHEN '6' THEN 'มิถุนายน'
                         WHEN '7' THEN 'กรกฎาคม'
                         WHEN '8' THEN 'สิงหาคม'
                         WHEN '9' THEN 'กันยายน'
                         WHEN '10' THEN 'ตุลาคม'
                         WHEN '11' THEN 'พฤศจิกายน'
                         WHEN '12' THEN 'ธันวาคม'
                         ELSE '' END ,'  ',YEAR(b.shipment_date)) shipment_date
                        ,b.remark
                        from ps_t_bid_selling_cmpo a 
                        left outer join bid_no bid on bid.bid_no = a.bid_no and bid.bid_revision = a.bid_revision  													
					    left join ps_t_qc_assignment2 b on a.row_id=b.selling_id ";
                //if (search != "") sql += " where a.contract_no='" + search + "'";
                if (search != "") sql += " where " + search ;
                //sql += search == "" ? " where 1=0" : "where " + search;
                sql += " order by a.row_id";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public QC02 getDataDBQC2_display(int id)
        {
            QC02 data = new QC02();
            string sql = "";
            try
            {
                sql += " select a.row_id,a.contract_no,a.schedule_name,FORMAT (b.shipment_date, 'dd/MM/yyyy ') shipment_date ,b.remark from ps_t_bid_selling_cmpo a ";
                sql += " left join ps_t_qc_assignment2 b on a.row_id = b.selling_id where a.row_id=" + id;
                var dd = db.ExecSql_DataTable(sql, zetl4eisdb);
                foreach (DataRow dtRow in dd.Rows)
                {
                    // On all tables' columns
                    foreach (DataColumn dc in dd.Columns)
                    {
                        data.row_id = Convert.ToInt32(dtRow["row_id"].ToString());
                        data.contract_no = dtRow["contract_no"].ToString();
                        data.schedule_name = dtRow["schedule_name"].ToString();
                        data.shipment_date =dtRow["shipment_date"].ToString();
                        data.remark = dtRow["remark"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return data;
        }
        




        public void AssignQC1(List<QC01> model)
        {
            DataTable dt = new DataTable();
            string strQuery = "";
            try
            {
                foreach (var item in model)
                {


                    DataTable dtchk = new DataTable();
                    string sqlchk = "select * from ps_t_qc_item where bid_no = '" + item.bid_no + "' and bid_revision = '" + item.bid_revision + "' " +
                                    "and contract_no = '" + item.contract_no + "' and schedule_name = '" + item.schedule_name + "' and item_no = '" + item.item_no + "'";
                    dtchk = db.ExecSql_DataTable(sqlchk, zetl4eisdb);
                    if (dtchk.Rows.Count != 0)
                    {
                        strQuery = @"update ps_t_qc_item set " +
                                   "ms='" + item.ms + "',qp='" + item.qp + "',ts='" + item.ts + "',tp='" + item.tp + "',iw='" + item.iw + "',ir='" + item.ir + "' " +
                                   ",fir='" + item.fir + "',cr='" + item.cr + "',tr='" + item.tr + "' " +
                                   "where bid_no='" + item.bid_no + "'  and bid_revision='" + item.bid_revision + "'  and item_no='" + item.item_no + "' " +
                                   "and schedule_name='" + item.schedule_name + "'  and part='" + item.part + "'  and contract_no='" + item.contract_no + "' ";
                    }
                    else
                    {
                        strQuery = @"insert into ps_t_qc_item (bid_no,bid_revision,contract_no " +
                                    ",schedule_name,part,item_no" +
                                    ",description,qty,unit,ms,qp,ts,tp,iw,ir,fir,cr,tr)" +
                                    "values ('" + item.bid_no + "','" + item.bid_revision + "','" + item.contract_no + "'" +
                                    ",'" + item.schedule_name + "','" + item.part + "','" + item.item_no + "'" +
                                    ",'" + item.description.Replace("'","''") + "','" + item.qty + "','" + item.unit + "','" + item.ms + "','" + item.qp + "','" + item.ts + "','" + item.tp + "'" +
                                    ",'" + item.iw + "','" + item.ir + "','" + item.fir + "','" + item.cr + "','" + item.tr + "') ";

                    }

                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void AssignQC2Display(QC02 model)
        {
            DataTable dt = new DataTable();
            string strQuery = "";
            try
            {
                    DataTable dtchk = new DataTable();
                    string sqlchk = "select * from ps_t_qc_assignment2 where selling_id = '" + model.row_id + "' ";
                    dtchk = db.ExecSql_DataTable(sqlchk, zetl4eisdb);

                    if (dtchk.Rows.Count != 0)
                    {
                        strQuery = @"update ps_t_qc_assignment2 set shipment_date='" + model.shipment_date + "',remark = '" + model.remark + "' " +
                                    "where selling_id=" + model.row_id;
                    }
                    else
                    {
                        strQuery = @"insert into ps_t_qc_assignment2 (create_date,shipment_date,remark)" +
                                    "values (getdate(),'" + model.shipment_date + "','" + model.remark + "')";
                    }
                    
                    db.ExecNonQuery(strQuery, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void AssignQC2(List<QC02> model)
        {
            DataTable dt = new DataTable();
            string strQuery = "";
            try
            {
                foreach (var item in model)
                {
                    DataTable dtchk = new DataTable();
                    string sqlchk = "select * from ps_t_qc_assignment2 where selling_id = '" + item.row_id + "' ";
                    dtchk = db.ExecSql_DataTable(sqlchk, zetl4eisdb);

                    if (dtchk.Rows.Count != 0)
                    {
                        strQuery = @"update ps_t_qc_assignment2 set " +
                                    "bid_no='" + item.bid_no + "',bid_revision=" + item.bid_revision + ",schedule_name='" + item.schedule_name + "' " +
                                    ",contract_no='" + item.contract_no + "',assign_to = '" + item.assign_to + "',update_date=getdate() " +
                                    "where selling_id=" + item.row_id;
                    }
                    else
                    {
                        strQuery = @"insert into ps_t_qc_assignment2 (selling_id,bid_no,bid_revision,schedule_name,contract_no,assign_to,create_date)" +
                                    "values ('" + item.row_id + "','" + item.bid_no + "','" + item.bid_revision + "','" + item.schedule_name + "','" + item.contract_no + "','" + item.assign_to + "',getdate())";

                    }
                    db.ExecNonQuery(strQuery, zetl4eisdb);

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            } 
        }
        public DataTable getDataDBQC04(string BidNo, string BidRevision, string ScheduleName, string ContractNo)
        {
            DataTable dt = new DataTable();
            try 
            {
                if (ContractNo != "")
                {
                    var xtemplate_id = getTemplateID(BidNo, BidRevision, ScheduleName);
                    //string sql = @"SELECT a.*,b.contract_no 
                    //    ,isnull(c.ms,0) ms,isnull(c.qp,0) qp,isnull(c.ts,0) ts,isnull(c.tp,0) tp
                    //    ,isnull(c.iw,0) iw,isnull(c.ir,0) ir,isnull(c.fir,0) fir,isnull(c.cr,0) cr,isnull(c.tr,0) tr, c.dac
                    //    FROM [dbo].[ps_xls_template] a
                    //    left join ps_t_bid_selling_cmpo b on 
                    //    (a.bid_no = b.bid_no and a.bid_revision = b.bid_revision 
                    //    and a.schedule_name = b.schedule_name and b.vendor_type!='vendor')
                    //    left join ps_t_qc_item c on (a.bid_no=c.bid_no and a.bid_revision=c.bid_revision
                    //    and b.contract_no=c.contract_no and a.schedule_name=c.schedule_name and a.item_no=c.item_no)
                    //where b.contract_no='" + ContractNo + "' and a.template_id = '" + xtemplate_id + "' ";

                    string sql = @"select a.*
                                , case when a.amt>a.qty then 100 
			                                 else 
					                                case when a.qty>0 then a.amt*100/a.qty 
					                                else 
					                                0 end
	                                end cnt 
                                 from (select 
                                (CASE WHEN b.chk_ms !='' THEN 1 ELSE 0 END + CASE WHEN c.chk_qp !='' THEN 1 ELSE 0 END
                                + CASE WHEN d.chk_ts !='' THEN 1 ELSE 0 END + CASE WHEN e.chk_tp !='' THEN 1 ELSE 0 END
                                + CASE WHEN f.chk_iw !='' THEN 1 ELSE 0 END + CASE WHEN g.chk_ir !='' THEN 1 ELSE 0 END
                                + CASE WHEN h.chk_fir !='' THEN 1 ELSE 0 END + CASE WHEN i.chk_cr !='' THEN 1 ELSE 0 END
                                --+ CASE WHEN j.chk_tr !='' THEN 1 ELSE 0 END )*100/9 amt
                                + CASE WHEN j.chk_tr !='' THEN 1 ELSE 0 END ) amt
                                ,CASE WHEN isnull(a.lci_amount,0) = 0 THEN 0 ELSE ((isnull(a.lci_unit_price,0)/isnull(a.lci_amount,0))*100) END total
                                ,a.* 
                                ,b.chk_ms,c.chk_qp,d.chk_ts,e.chk_tp,f.chk_iw,g.chk_ir,h.chk_fir,i.chk_cr,j.chk_tr
                                ,k.dep1,m.dep2,n.dep3,o.dep4,p.dep5,q.dep6
                                --,k.dep1_name,m.dep2_name,n.dep3_name,o.dep4_name,p.dep5_name,q.dep6_name
                                ,qq.doc_nodeid
							    ,r.doc_li,s.doc_cd,t.doc_da,u.doc_cor,v.doc_pd,w.doc_rf,x.doc_sp,y.doc_typ
                                from (SELECT a.*,b.contract_no ,c.dac,b.bidder_id
                                ,isnull(c.ms,0) ms,isnull(c.qp,0) qp,isnull(c.ts,0) ts,isnull(c.tp,0) tp
                                ,isnull(c.iw,0) iw,isnull(c.ir,0) ir,isnull(c.fir,0) fir,isnull(c.cr,0) cr,isnull(c.tr,0) tr
                                FROM [dbo].[ps_xls_template] a
                                left join ps_t_bid_selling_cmpo b on 
                                (a.bid_no = b.bid_no and a.bid_revision = b.bid_revision 
                                and a.schedule_name = b.schedule_name and b.vendor_type!='vendor')
                                left join ps_t_qc_item c on (a.bid_no=c.bid_no and a.bid_revision=c.bid_revision
                                and b.contract_no=c.contract_no and a.schedule_name=c.schedule_name and a.item_no=c.item_no)
                                where b.contract_no='" + ContractNo + "' and a.template_id = '" + xtemplate_id + "'  ";
                    sql += ") a " +
                            "left join (select c.*,c.step_code chk_ms from ps_t_qc_item_upload c where c.step_code='ms') b on (a.bid_no=b.bid_no and a.bid_revision=b.bid_revision " +
                            "and a.contract_no=b.contract_no and a.schedule_name=b.schedule_name and a.item_no=b.item_no) " +
                            "left join (select c.*,c.step_code chk_qp from ps_t_qc_item_upload c where c.step_code='qp') c on (a.bid_no=c.bid_no and a.bid_revision=c.bid_revision " +
                            "and a.contract_no=c.contract_no and a.schedule_name=c.schedule_name and a.item_no=c.item_no) " +
                            "left join (select c.*,c.step_code chk_ts from ps_t_qc_item_upload c where c.step_code='ts') d on (a.bid_no=d.bid_no and a.bid_revision=d.bid_revision " +
                            "and a.contract_no=d.contract_no and a.schedule_name=d.schedule_name and a.item_no=d.item_no) " +
                            "left join (select c.*,c.step_code chk_tp from ps_t_qc_item_upload c where c.step_code='tp') e on (a.bid_no=e.bid_no and a.bid_revision=e.bid_revision " +
                            "and a.contract_no=e.contract_no and a.schedule_name=e.schedule_name and a.item_no=e.item_no) " +
                            "left join (select c.*,c.step_code chk_iw from ps_t_qc_item_upload c where c.step_code='iw') f on (a.bid_no=f.bid_no and a.bid_revision=f.bid_revision " +
                            "and a.contract_no=f.contract_no and a.schedule_name=f.schedule_name and a.item_no=f.item_no) " +
                            "left join (select c.*,c.step_code chk_ir from ps_t_qc_item_upload c where c.step_code='ir') g on (a.bid_no=g.bid_no and a.bid_revision=g.bid_revision " +
                            "and a.contract_no=g.contract_no and a.schedule_name=g.schedule_name and a.item_no=g.item_no) " +
                            "left join (select c.*,c.step_code chk_fir from ps_t_qc_item_upload c where c.step_code='fir') h on (a.bid_no=h.bid_no and a.bid_revision=h.bid_revision " +
                            "and a.contract_no=h.contract_no and a.schedule_name=h.schedule_name and a.item_no=h.item_no) " +
                            "left join (select c.*,c.step_code chk_cr from ps_t_qc_item_upload c where c.step_code='cr') i on (a.bid_no=i.bid_no and a.bid_revision=i.bid_revision " +
                            "and a.contract_no=i.contract_no and a.schedule_name=i.schedule_name and a.item_no=i.item_no) " +
                            "left join (select c.*,c.step_code chk_tr from ps_t_qc_item_upload c where c.step_code='tr') j on (a.bid_no=j.bid_no and a.bid_revision=j.bid_revision " +
                            "and a.contract_no=j.contract_no and a.schedule_name=j.schedule_name and a.item_no=j.item_no) ";

                    sql += "left join(select  top 1 c.*, c.depart_position_name dep1 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวศ-ส.') k " +
                            "on (a.bid_no=k.bid_no and a.bid_revision = k.bid_revision and a.schedule_name = k.schedule_name and a.equip_code=k.equip_code) " +
                            "left join(select top 1 c.*,c.depart_position_name dep2 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวอ-ส.') m " +
                            //"left join(select c.rowid dep2_id,c.*,c.depart_position_name dep2 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวอ-ส.') m " +
                            "on (a.bid_no=m.bid_no and a.bid_revision = m.bid_revision and a.schedule_name = m.schedule_name and a.equip_code=m.equip_code) " +
                            "left join(select  top 1 c.*,c.depart_position_name dep3 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวป-ส.') n " +
                            "on (a.bid_no=n.bid_no and a.bid_revision = n.bid_revision and a.schedule_name = n.schedule_name and a.equip_code=n.equip_code) " +
                            "left join(select  top 1 c.*,c.depart_position_name dep4 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวธ-ส.') o " +
                            "on (a.bid_no=o.bid_no and a.bid_revision = o.bid_revision and a.schedule_name = o.schedule_name and a.equip_code=o.equip_code) " +
                            "left join(select  top 1 c.*,c.depart_position_name dep5 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวสส-ส.') p " +
                            "on (a.bid_no=p.bid_no and a.bid_revision = p.bid_revision and a.schedule_name = p.schedule_name and a.equip_code=p.equip_code) " +
                            "left join(select  top 1 c.*,c.depart_position_name dep6 from ps_t_mytask_equip_doc_design c where c.depart_position_name='กวส-ส.') q " +
                            "on (a.bid_no=q.bid_no and a.bid_revision = q.bid_revision and a.schedule_name = q.schedule_name and a.equip_code=q.equip_code) ";

                    sql += "left join(select top 1 c.* from ps_t_mytask_equip_doc_design c) qq " +
                            "on (a.bid_no=qq.bid_no and a.bid_revision = qq.bid_revision and a.schedule_name = qq.schedule_name and a.equip_code=qq.equip_code) " +
                            "left join(select *, doc_type doc_li from ps_t_bid_selling_fileupload where doc_type = 'li') r " +
                            "on(a.bid_no= r.bid_no and a.bid_revision = r.bid_revision and a.schedule_name = r.ref_schedule_no and a.bidder_id= r.bidder_id and qq.doc_nodeid= r.doc_node_id) " +
                            "left join(select *, doc_type doc_cd from ps_t_bid_selling_fileupload where doc_type = 'cd') s " +
                            "on(a.bid_no= s.bid_no and a.bid_revision = s.bid_revision and a.schedule_name = s.ref_schedule_no and a.bidder_id= s.bidder_id and qq.doc_nodeid= s.doc_node_id) " +
                            "left join(select *, doc_type doc_da from ps_t_bid_selling_fileupload where doc_type = 'da') t " +
                            "on(a.bid_no= t.bid_no and a.bid_revision = t.bid_revision and a.schedule_name = t.ref_schedule_no and a.bidder_id= t.bidder_id and qq.doc_nodeid= t.doc_node_id) " +
                            "left join(select *, doc_type doc_cor from ps_t_bid_selling_fileupload where doc_type = 'cor') u " +
                            "on(a.bid_no= u.bid_no and a.bid_revision = u.bid_revision and a.schedule_name = u.ref_schedule_no and a.bidder_id= u.bidder_id and qq.doc_nodeid= u.doc_node_id) " +
                            "left join(select *, doc_type doc_pd from ps_t_bid_selling_fileupload where doc_type = 'pd') v " +
                            "on(a.bid_no= v.bid_no and a.bid_revision = v.bid_revision and a.schedule_name = v.ref_schedule_no and a.bidder_id= v.bidder_id and qq.doc_nodeid= v.doc_node_id) " +
                            "left join(select *, doc_type doc_rf from ps_t_bid_selling_fileupload where doc_type = 'rf') w " +
                            "on(a.bid_no= w.bid_no and a.bid_revision = w.bid_revision and a.schedule_name = w.ref_schedule_no and a.bidder_id= w.bidder_id and qq.doc_nodeid= w.doc_node_id) " +
                            "left join(select *, doc_type doc_sp from ps_t_bid_selling_fileupload where doc_type = 'sp') x " +
                            "on(a.bid_no= x.bid_no and a.bid_revision = x.bid_revision and a.schedule_name = x.ref_schedule_no and a.bidder_id= x.bidder_id and qq.doc_nodeid= x.doc_node_id) " +
                            "left join(select *, doc_type doc_typ from ps_t_bid_selling_fileupload where doc_type = 'typ') y " +
                            "on(a.bid_no= y.bid_no and a.bid_revision = y.bid_revision and a.schedule_name = y.ref_schedule_no and a.bidder_id= y.bidder_id and qq.doc_nodeid= y.doc_node_id) " +

                            ") a  " +
                            "order by a.part, a.item_no ";

                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                }

                //    var ds = db.ExecSql_DataSet(sql, zetl4eisdb);
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        gv1.DataSource = ds.Tables[0];
                //        gv1.DataBind();

                //    }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        //public DataTable getDataMsg(int id)
        //{
        //    DataTable dt = new DataTable();
        //    try
        //    {
        //        string sql = @"select * from ps_t_mytask_equip_doc_design where rowid=" + id;
        //        dt = db.ExecSql_DataTable(sql, zetl4eisdb);
        //    }
        //    catch (Exception ex)
        //    {
        //        LogHelper.WriteEx(ex);
        //    }
        //    return dt;
        //}
        public DataTable getDataMsg(string bid_no, string bid_revision, string schedule_name, string equip_code, string dep_name)
        {
            DataTable dt = new DataTable();
            try
            {/*
                string sql = @"select b.doc_name,b.doc_desc,b.doc_sheetno, a.* from ps_t_mytask_equip_doc_design a
                            left join ps_t_mytask_doc b on (a.bid_no=b.bid_no and a.bid_revision = b.bid_revision and a.schedule_name = b.schedule_name and a.doc_nodeid=b.doc_nodeid)
                            where a.rowid=" + dep_name;
                */
                string sql = @"select b.doc_name,b.doc_desc,b.doc_sheetno, a.* 
	                        from ps_t_mytask_equip_doc_design a
	                        left join ps_t_mytask_doc b on (a.bid_no=b.bid_no and a.bid_revision = b.bid_revision and a.schedule_name = b.schedule_name and a.doc_nodeid=b.doc_nodeid) ";
                sql += " where a.bid_no='" + bid_no + "' ";
                sql += " and a.bid_revision='" + bid_revision + "' ";
                sql += " and a.schedule_name='" + schedule_name + "' ";
                sql += " and a.equip_code='" + equip_code + "'  ";
                sql += " and a.depart_position_name='" + dep_name  + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
    }
}
