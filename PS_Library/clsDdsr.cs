﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsDdsr
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();
        private DbControllerBase db = new DbControllerBase();
        //oraDBUtil ora = new oraDBUtil();


        public clsDdsr() { }

        public DataTable GetDrawingListByBidNo(string strBidNo, string strJobNo, string strDept)
        {
            DataTable dt = new DataTable();
            string strSql = @"select distinct excel.bid_no
                              ,dbo.udf_StripHTML(bid.bid_desc) as bid_desc
                              ,doc.depart_position_name 
                              ,job_no.sub_line_name 
                              ,doc.doc_nodeid
                              ,excel.type
							  ,left(doc.doc_name, len(doc.doc_name) - charindex('.', Reverse(doc.doc_name))) as drawing_no
                              --,REVERSE(SUBSTRING(REVERSE(doc.doc_name), CHARINDEX('.', REVERSE(doc.doc_name)) + 1, 999)) as drawing_no
                              ,doc.doc_desc as drawing_name 
                              ,doc.doc_revision_no as revision 
                              ,doc.job_no as ref_job
                              ,excel.schedule_name as ref_sch
                              ,item_no as ref_part 
                              ,'' as to_contractor
                              ,'For Bid' as status
                              from ps_up_excel_transection as excel
                              left outer join ps_t_mytask_doc as doc
                                left outer join job_no 
                                on job_no.job_no = doc.job_no and job_no.revision = doc.job_revision
                              on doc.bid_no = excel.bid_no and doc.bid_revision in (select max(bid_revision) from ps_t_mytask_doc where bid_no = doc.bid_no) and doc.doctype_code = 'DD'
                              left join bid_no bid on excel.bid_no = bid.bid_no
                              where excel.bid_no = '" + strBidNo + "'  " +
                              " and doc.job_no = '" + strJobNo + "' " +
                              " and doc.depart_position_name = '" + strDept + "'" +
                              "order by bid_no asc,bid_desc asc,ref_sch asc,drawing_no asc ,drawing_name asc,ref_part asc";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }

        public DataTable GetDrawingListByNodeId(string strConcatNodeIds)
        {
            DataTable dt = new DataTable();
            string strSql = @" select left(mtd.doc_name, len(mtd.doc_name) - charindex('.', Reverse(mtd.doc_name))) as drawing_no, mtdd.schedule_name,mtdd.bid_no,mtdd.doc_nodeid,mteq.part_code_b,mteq.equip_code,mteq.equip_item_no_b
							  from ps_t_mytask_equip_doc_design mtdd
							  left join ps_t_mytask_equip mteq on mtdd.mytask_eq_id = mteq.rowid
							  left join ps_t_mytask_doc mtd on mtdd.doc_nodeid = mtd.doc_nodeid
							  where mtdd.doc_nodeid in(" + strConcatNodeIds + @") 
							  and mtdd.mytask_eq_id is not null
                              order by drawing_no asc,schedule_name asc,bid_no asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDtExportQcByType(string strBidNo, string strSchName, string strQcType)
        {
            DataTable dt = new DataTable();
            // string strQueryCondistion = string.Empty;
            //string strSql2 = @"SELECT [schedule_name] ,part ,part_description
		          //              ,STUFF((SELECT distinct ',' + item_no 
				        //                FROM ps_t_qc_item 
				        //                WHERE [schedule_name] = aa.schedule_name 
				        //                --AND part = aa.part 
				        //                --order by item_no asc
				        //                and bid_no = '" + strBidNo + @"'
				        //                and schedule_name = '" + strSchName + @"'
				        //                and " + strQcType + @" =1
				        //                FOR XML PATH (''))
				        //                , 1, 1, '')  AS item_list
            //            FROM ps_t_qc_item AS aa
            //            GROUP BY [schedule_name],part,part_description";

            string strSql = @"SELECT [schedule_name] ,part,part_description,schedule_code
		                        ,STUFF((SELECT distinct ',' + item_no 
				                        FROM ps_t_qc_item 
				                        WHERE [schedule_name] = aa.schedule_name 
				                        AND part = aa.part 
				                        --order by item_no asc
				                        and bid_no = '" + strBidNo + @"'
				                        and schedule_name = '" + strSchName + @"'
				                        and " + strQcType + @" =1
				                        FOR XML PATH (''))
				                        , 1, 1, '')  AS item_list
                        FROM ps_t_qc_item AS aa
						where part is not null
                        GROUP BY [schedule_name],part,part_description,schedule_code;";

            string strSqlBk = @"SELECT [schedule_name] ,part,part_description,schedule_code
		                        ,STUFF((SELECT distinct ',' + item_no 
				                        FROM ps_t_qc_item 
				                        WHERE [schedule_name] = aa.schedule_name 
				                        AND part = aa.part 
				                        --order by item_no asc
				                        and bid_no = '" + strBidNo + @"'
				                        and schedule_name = '230 KV ANG THONG 1 SUBSTATION'
				                        and " + strQcType + @" =1
				                        FOR XML PATH (''))
				                        , 1, 1, '')  AS item_list
                        FROM ps_t_qc_item AS aa
						where part is not null
                        GROUP BY [schedule_name],part,part_description,schedule_code;";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetBidNo()
        {
            DataTable dt = new DataTable();
            string strSql = @"select distinct bid_no from ps_up_excel_transection order by bid_no asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }

        public DataTable GetJobNo()
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct job_no
                                FROM ps_t_mytask_doc
                                where job_no is not null
                                and job_no != ''";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }

        public DataTable GetDepartment()
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct depart_position_name FROM ps_t_mytask_doc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public byte[] GenExcel(DataTable dtToUpload)
        {
            string strFileName = "Master_drawing_list_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + ".xlsx";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage();
            var workbook = package.Workbook;

            // -----------------------start sheet1 ----------------------------------------
            var worksheet = workbook.Worksheets.Add("Sheet1");

            worksheet.Column(1).Width = 10;
            worksheet.Column(2).Width = 20;
            worksheet.Column(3).Width = 30;
            worksheet.Column(4).Width = 20;
            worksheet.Column(5).Width = 10;
            worksheet.Column(6).Width = 30;
            worksheet.Column(7).Width = 10;
            worksheet.Column(8).Width = 20;
            worksheet.Column(9).Width = 20;
            worksheet.Column(10).Width = 20;
            worksheet.Column(11).Width = 20;
            worksheet.Column(12).Width = 10;

            // title
            worksheet.Cells["A1:B1"].Merge = true;
            worksheet.Cells["A1:B1"].Value = "Raw Data DDSR Report ";
            worksheet.Cells["A1:B1"].Style.Font.Bold = true;
            //worksheet.Cells["A1:B1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //worksheet.Cells["C1"].Value = DateTime.Now.Year;
            //worksheet.Cells["C1"].Style.Font.Bold = true;
            //worksheet.Cells["C1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;


            // gv header
            // worksheet.Cells["A3:I3"].Value = "Item No'";
            worksheet.Cells["A3:L3"].Style.Font.Bold = true;
            worksheet.Cells["A3:L3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:L3"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["A3:L3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A3:L3"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
            //set border
            worksheet.Cells["A3:L3"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:L3"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:L3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:L3"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //set header value
            worksheet.Cells["A3"].Value = "Item No";
            worksheet.Cells["B3"].Value = "Drawing No.";
            worksheet.Cells["C3"].Value = "Drawing Name";
            worksheet.Cells["D3"].Value = "Bid No.";
            worksheet.Cells["E3"].Value = "กอง";
            worksheet.Cells["F3"].Value = "Subline Name";
            worksheet.Cells["G3"].Value = "Revision";
            worksheet.Cells["H3"].Value = "Ref. Job";
            worksheet.Cells["I3"].Value = "Ref. Schedule";
            worksheet.Cells["J3"].Value = "Ref. Part";
            worksheet.Cells["K3"].Value = "To Contractor";
            worksheet.Cells["L3"].Value = "Status";
            //worksheet.Cells["M3"].Value = "";
            //worksheet.Cells["N3"].Value = "";


            int iRow = 4;
            string strCellId = string.Empty;

            for (int i = 0; i < dtToUpload.Rows.Count; i++)
            {
                strCellId = "A" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = (i + 1).ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "B" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["drawing_no"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "C" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["drawing_name"];
                //worksheet.Cells[strCellId].Style.Numberformat.Format = "#,##0.00";
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "D" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["bid_no"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "E" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["depart_position_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                //strCellId = "D" + (iRow + i).ToString();
                //worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["station_id"].ToString();
                //worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //if (i == (dtToUpload.Rows.Count - 1))
                //{
                //    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //}

                strCellId = "F" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["sub_line_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }


                strCellId = "G" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["revision"];
                //worksheet.Cells[strCellId].Style.Numberformat.Format = "#,##0.00";
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "H" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["ref_job"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "I" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["ref_sch"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "J" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["ref_part"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "K" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["to_contractor"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "L" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["status"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
            }



            // -----------------------end sheet1 ----------------------------------------
            // -----------------------start sheet2 ----------------------------------------

            worksheet = workbook.Worksheets.Add("Sheet2");
            worksheet.Column(1).Width = 10;
            worksheet.Column(2).Width = 20;
            worksheet.Column(3).Width = 10;
            worksheet.Column(4).Width = 10;
            worksheet.Column(5).Width = 30;
            worksheet.Column(6).Width = 10;
            worksheet.Column(7).Width = 10;
            worksheet.Column(8).Width = 10;

            // title
            worksheet.Cells["A1:B1"].Merge = true;
            worksheet.Cells["A1:B1"].Value = "Raw Data DDSR Report ";
            worksheet.Cells["A1:B1"].Style.Font.Bold = true;


            // gv header
            // worksheet.Cells["A3:I3"].Value = "Item No'";
            worksheet.Cells["A3:H3"].Style.Font.Bold = true;
            worksheet.Cells["A3:H3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:H3"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["A3:H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A3:H3"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
            //set border
            worksheet.Cells["A3:H3"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            //set header value
            worksheet.Cells["A3"].Value = "Item No";
            worksheet.Cells["B3"].Value = "Drawing No.";
            worksheet.Cells["C3"].Value = "doc_id";
            worksheet.Cells["D3"].Value = "Bid No.";
            worksheet.Cells["E3"].Value = "Ref. Schedule";
            worksheet.Cells["F3"].Value = "Part";
            worksheet.Cells["G3"].Value = "Equip_code";
            worksheet.Cells["H3"].Value = "Equip item";

            iRow = 4;

            DataView view = new DataView(dtToUpload);
            DataTable dtNodeIds = view.ToTable(true, "doc_nodeid");
            string strDocNodeId = GetConcatStringByDt(dtNodeIds, "doc_nodeid", true);
            DataTable dtExport2 = GetDrawingListByNodeId(strDocNodeId);


            for (int i = 0; i < dtExport2.Rows.Count; i++)
            {
                strCellId = "A" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = (i + 1).ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "B" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["drawing_no"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "C" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["doc_nodeid"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "D" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["bid_no"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "E" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["schedule_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "F" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["part_code_b"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "G" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["equip_code"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "H" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport2.Rows[i]["equip_item_no_b"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport2.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
            }
            // -----------------------end sheet2 ----------------------------------------



            return package.GetAsByteArray();
        }

        private string GetConcatStringByDt(DataTable dt, string strFieldName, bool isOutputStringSql)
        {
            string strResult = string.Empty;

            foreach (DataRow dr in dt.Rows)
            {
                if (isOutputStringSql)
                {
                    strResult += strResult == string.Empty ? "'" + dr[strFieldName].ToString() + "'" : ",'" + dr[strFieldName].ToString() + "'";
                }
                else
                {
                    strResult += strResult == string.Empty ? dr[strFieldName].ToString() : "," + dr[strFieldName].ToString();
                }
            }

            return strResult;
        }

        public byte[] GenExcelDdSr(DataTable dtToUpload)
        {
            var package = new ExcelPackage();
            var workbook = package.Workbook;
            if (dtToUpload.Rows.Count > 0)
            {
                // DataRow[] results = dtToUpload.Select("A = 'foo' AND B = 'bar' AND C = 'baz'");
                DataView view = new DataView(dtToUpload);
                DataTable dtDistinctValues = view.ToTable(true, "bid_no", "ref_sch", "bid_desc");
                string strBidNo, strBidDesc, strSchName;
                strBidNo = strBidDesc = strSchName = string.Empty;
                DataRow[] drListGv;

                if (dtDistinctValues.Rows.Count > 0)
                {

                    string strFileName = "Master_drawing_list_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + ".xlsx";
                    ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                    //var package = new ExcelPackage();
                    //var workbook = package.Workbook;

                    for (int i = 0; i < dtDistinctValues.Rows.Count; i++)
                    {
                        strBidNo = dtDistinctValues.Rows[i]["bid_no"].ToString();
                        strBidDesc = dtDistinctValues.Rows[i]["bid_desc"].ToString();
                        strSchName = dtDistinctValues.Rows[i]["ref_sch"].ToString();

                        drListGv = dtToUpload.Select("bid_no = '" + strBidNo + "' AND ref_sch = '" + strSchName + "' ");
                        var worksheet = workbook.Worksheets.Add("Sheet" + (i + 1).ToString());

                        worksheet.Column(1).Width = 20;
                        worksheet.Column(2).Width = 30;
                        worksheet.Column(3).Width = 20;
                        worksheet.Column(4).Width = 10;
                        worksheet.Column(5).Width = 10;

                        // title
                        worksheet.Cells["A1:B1"].Merge = true;
                        worksheet.Cells["A1:B1"].Value = "DDSR Report ";
                        worksheet.Cells["A1:B1"].Style.Font.Bold = true;
                        worksheet.Cells["A1:B1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        //Label bid sch
                        worksheet.Cells["A3"].Value = "Bid No. :";
                        worksheet.Cells["A4"].Value = "Bid Description :";
                        worksheet.Cells["A5"].Value = "Schedule Name :";
                        worksheet.Cells["A3:A5"].Style.Font.Bold = true;
                        worksheet.Cells["A3:A5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        //Label bid sch Value
                        worksheet.Cells["B3:F3"].Merge = true;
                        worksheet.Cells["B4:F4"].Merge = true;
                        worksheet.Cells["B5:F5"].Merge = true;
                        worksheet.Cells["B3:F5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells["B3:F3"].Value = strBidNo;
                        worksheet.Cells["B4:F4"].Value = strBidDesc;
                        worksheet.Cells["B5:F5"].Value = strSchName;
                        // gv header
                        worksheet.Cells["A7:E7"].Style.Font.Bold = true;
                        worksheet.Cells["A7:E7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells["A7:E7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                        worksheet.Cells["A7:E7"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells["A7:E7"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
                        //set border
                        worksheet.Cells["A7:E7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells["A7:E7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells["A7:E7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells["A7:E7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        //set header value
                        worksheet.Cells["A7"].Value = "Drawing No.";
                        worksheet.Cells["B7"].Value = "Drawing Name";
                        worksheet.Cells["C7"].Value = "Ref Job.";
                        worksheet.Cells["D7"].Value = "Ref Part";
                        worksheet.Cells["E7"].Value = "Discipline";
                        int iRow = 8;
                        string strCellId = string.Empty;

                        for (int j = 0; j < drListGv.Length; j++)
                        {

                            strCellId = "A" + (iRow + j).ToString();
                            worksheet.Cells[strCellId].Value = drListGv[j]["drawing_no"].ToString();
                            worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            if (j == (drListGv.Length - 1))
                            {
                                worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }

                            strCellId = "B" + (iRow + j).ToString();
                            worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["drawing_name"].ToString();
                            worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            if (j == (drListGv.Length - 1))
                            {
                                worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }

                            strCellId = "C" + (iRow + j).ToString();
                            worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["ref_job"];
                            //worksheet.Cells[strCellId].Style.Numberformat.Format = "#,##0.00";
                            worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            if (j == (drListGv.Length - 1))
                            {
                                worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }

                            strCellId = "D" + (iRow + j).ToString();
                            worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["ref_part"].ToString();
                            worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            if (j == (drListGv.Length - 1))
                            {
                                worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            strCellId = "E" + (iRow + j).ToString();
                            worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["depart_position_name"].ToString();
                            worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            if (j == (drListGv.Length - 1))
                            {
                                worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                        }
                    }
                }
            }
            return package.GetAsByteArray();
        }

        public byte[] GenExcelDdsrQc(DataTable dtToUpload)
        {
            var package = new ExcelPackage();
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var workbook = package.Workbook;
            if (dtToUpload.Rows.Count > 0)
            {
                DataView view = new DataView(dtToUpload);
                DataTable dtDistinctValues = view.ToTable(true, "bid_no", "ref_sch", "bid_desc");
                string strBidNo, strBidDesc, strSchName;
                strBidNo = strBidDesc = strSchName = string.Empty;
                DataRow[] drListGv;
                if (dtDistinctValues.Rows.Count > 0)
                {
                    string strFileName = "Master_drawing_list_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + ".xlsx";

                    //for (int i = 0; i < dtDistinctValues.Rows.Count; i++)
                    //{
                    strBidNo = dtDistinctValues.Rows[0]["bid_no"].ToString();
                    strBidDesc = dtDistinctValues.Rows[0]["bid_desc"].ToString();
                    strSchName = dtDistinctValues.Rows[0]["ref_sch"].ToString();

                    drListGv = dtToUpload.Select("bid_no = '" + strBidNo + "' AND ref_sch = '" + strSchName + "' ");
                    var worksheet = workbook.Worksheets.Add("Design Drawing");

                    worksheet.Column(1).Width = 20;
                    worksheet.Column(2).Width = 30;
                    worksheet.Column(3).Width = 20;
                    worksheet.Column(4).Width = 10;
                    worksheet.Column(5).Width = 10;

                    // title
                    worksheet.Cells["A1:B1"].Merge = true;
                    worksheet.Cells["A1:B1"].Value = "DDSR Report ";
                    worksheet.Cells["A1:B1"].Style.Font.Bold = true;
                    worksheet.Cells["A1:B1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //Label bid sch
                    worksheet.Cells["A3"].Value = "Bid No. :";
                    worksheet.Cells["A4"].Value = "Bid Description :";
                    worksheet.Cells["A5"].Value = "Schedule Name :";
                    worksheet.Cells["A7"].Value = "Design Drawing List";
                    worksheet.Cells["A3:A7"].Style.Font.Bold = true;
                    worksheet.Cells["A3:A7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    //Label bid sch Value
                    worksheet.Cells["B3:F3"].Merge = true;
                    worksheet.Cells["B4:F4"].Merge = true;
                    worksheet.Cells["B5:F5"].Merge = true;
                    worksheet.Cells["B3:F5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells["B3:F3"].Value = strBidNo;
                    worksheet.Cells["B4:F4"].Value = strBidDesc;
                    worksheet.Cells["B5:F5"].Value = strSchName;
                    // gv header
                    worksheet.Cells["A8:E8"].Style.Font.Bold = true;
                    worksheet.Cells["A8:E8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells["A8:E8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                    worksheet.Cells["A8:E8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells["A8:E8"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
                    //set border
                    worksheet.Cells["A8:E8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["A8:E8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["A8:E8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells["A8:E8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    //set header value
                    worksheet.Cells["A8"].Value = "Drawing No.";
                    worksheet.Cells["B8"].Value = "Drawing Name";
                    worksheet.Cells["C8"].Value = "Ref Job.";
                    worksheet.Cells["D8"].Value = "Ref Part";
                    worksheet.Cells["E8"].Value = "Discipline";
                    int iRow = 9;
                    string strCellId = string.Empty;

                    for (int j = 0; j < drListGv.Length; j++)
                    {

                        strCellId = "A" + (iRow + j).ToString();
                        worksheet.Cells[strCellId].Value = drListGv[j]["drawing_no"].ToString();
                        worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        if (j == (drListGv.Length - 1))
                        {
                            worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCellId = "B" + (iRow + j).ToString();
                        worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["drawing_name"].ToString();
                        worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        if (j == (drListGv.Length - 1))
                        {
                            worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCellId = "C" + (iRow + j).ToString();
                        worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["ref_job"];
                        //worksheet.Cells[strCellId].Style.Numberformat.Format = "#,##0.00";
                        worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        if (j == (drListGv.Length - 1))
                        {
                            worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCellId = "D" + (iRow + j).ToString();
                        worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["ref_part"].ToString();
                        worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        if (j == (drListGv.Length - 1))
                        {
                            worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        strCellId = "E" + (iRow + j).ToString();
                        worksheet.Cells[strCellId].Value = dtToUpload.Rows[j]["depart_position_name"].ToString();
                        worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        if (j == (drListGv.Length - 1))
                        {
                            worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                    }
                    //}

                    // ---------------------  qc session ---------------------------

                    DataTable dtMS = GetDataByQcType("MS", strBidNo, strSchName);
                    DataTable dtQP = GetDataByQcType("QP", strBidNo, strSchName);
                    DataTable dtTS = GetDataByQcType("TS", strBidNo, strSchName);
                    DataTable dtTP = GetDataByQcType("TP", strBidNo, strSchName);
                    DataTable dtTR = GetDataByQcType("TR", strBidNo, strSchName);
                    DataTable dtIW = GetDataByQcType("IW", strBidNo, strSchName);
                    DataTable dtExport = new DataTable();

                    // MS
                    if (dtMS.Rows.Count > 0)
                    {
                        dtExport = GetDtExportQcByType(strBidNo, strSchName, "MS");
                        worksheet = workbook.Worksheets.Add("MS");
                        SetSheetQcType(worksheet, strBidNo, strBidDesc, strSchName, dtExport, "MS");
                    }

                    // QP
                    if (dtQP.Rows.Count > 0)
                    {
                        dtExport = GetDtExportQcByType(strBidNo, strSchName, "QP");
                        worksheet = workbook.Worksheets.Add("QP");
                        SetSheetQcType(worksheet, strBidNo, strBidDesc, strSchName, dtExport, "QP");
                    }

                    // TS
                    if (dtTS.Rows.Count > 0)
                    {
                        dtExport = GetDtExportQcByType(strBidNo, strSchName, "TS");
                        worksheet = workbook.Worksheets.Add("TS");
                        SetSheetQcType(worksheet, strBidNo, strBidDesc, strSchName, dtExport, "TS");
                    }

                    // TP
                    if (dtTP.Rows.Count > 0)
                    {
                        dtExport = GetDtExportQcByType(strBidNo, strSchName, "TP");
                        worksheet = workbook.Worksheets.Add("TP");
                        SetSheetQcType(worksheet, strBidNo, strBidDesc, strSchName, dtExport, "TP");
                    }

                    // TR
                    if (dtTR.Rows.Count > 0)
                    {
                        dtExport = GetDtExportQcByType(strBidNo, strSchName, "TR");
                        worksheet = workbook.Worksheets.Add("TR");
                        SetSheetQcType(worksheet, strBidNo, strBidDesc, strSchName, dtExport, "TR");
                    }

                    // IW
                    if (dtIW.Rows.Count > 0)
                    {
                        dtExport = GetDtExportQcByType(strBidNo, strSchName, "IW");
                        worksheet = workbook.Worksheets.Add("IW");
                        SetSheetQcType(worksheet, strBidNo, strBidDesc, strSchName, dtExport, "IW");
                    }

                     


                    // ---------------------  qc session ---------------------------
                }
            }
            return package.GetAsByteArray();
        }

        private DataTable GetDataByQcType(string strQcType, string strBidNo, string strSchName)
        {
            DataTable dt = new DataTable();
            string strSql_bk = @"select * from ps_t_qc_item where bid_no = '" + strBidNo + @"' 
						and schedule_name = '" + strSchName + @"'
						and " + strQcType + " = 1";
            string strSql = @"select * from ps_t_qc_item where bid_no = '" + strBidNo + @"' 
						 and schedule_name = '230 KV ANG THONG 1 SUBSTATION'
						and " + strQcType + " = 1";

           
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        private OfficeOpenXml.ExcelWorksheet SetSheetQcType(OfficeOpenXml.ExcelWorksheet wk, string strBidNo, string strBidDesc, string strSchName, DataTable dtExport, string strQcType)
        {
            var worksheet = wk;
            string strTitleGv = string.Empty;
            switch (strQcType)
            {
                case "MS":
                    strTitleGv = "Manufacturing and Delivery Schedule";
                    break;
                case "QP":
                    strTitleGv = "Quality Assurance Program";
                    break;
                case "TS":
                    strTitleGv = "Test Schedule";
                    break;
                case "TP":
                    strTitleGv = "Test Procedure";
                    break;
                case "TR":
                    strTitleGv = "Test Report";
                    break;
                case "IW":
                    strTitleGv = "Quality Control and Inspection Work";
                    break;
                default:
                    // code block
                    strTitleGv = "Design Drawing";
                    break;
            }

            worksheet.Column(1).Width = 20;
            worksheet.Column(2).Width = 30;
            worksheet.Column(3).Width = 20;
            worksheet.Column(4).Width = 10;
            worksheet.Column(5).Width = 10;

            // title
            worksheet.Cells["A1:B1"].Merge = true;
            worksheet.Cells["A1:B1"].Value = "DDSR Report ";
            worksheet.Cells["A1:B1"].Style.Font.Bold = true;
            worksheet.Cells["A1:B1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            //Label bid sch
            worksheet.Cells["A3"].Value = "Bid No. :";
            worksheet.Cells["A4"].Value = "Bid Description :";
            worksheet.Cells["A5"].Value = "Schedule Name :";
            worksheet.Cells["A7"].Value = strTitleGv;
            worksheet.Cells["A3:A7"].Style.Font.Bold = true;
            worksheet.Cells["A3:A7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            //Label bid sch Value
            worksheet.Cells["B3:F3"].Merge = true;
            worksheet.Cells["B4:F4"].Merge = true;
            worksheet.Cells["B5:F5"].Merge = true;
            worksheet.Cells["B3:F5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells["B3:F3"].Value = strBidNo;
            worksheet.Cells["B4:F4"].Value = strBidDesc;
            worksheet.Cells["B5:F5"].Value = strSchName;
            // gv header
            worksheet.Cells["A8:C8"].Style.Font.Bold = true;
            worksheet.Cells["A8:C8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A8:C8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["A8:C8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A8:C8"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
            //set border
            worksheet.Cells["A8:C8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A8:C8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A8:C8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A8:C8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //set header value
            worksheet.Cells["A8"].Value = "Document No.";
            worksheet.Cells["B8"].Value = "Description";
            worksheet.Cells["C8"].Value = "Ref. ItemNo";
            int iRow = 9;
            string strCellId = string.Empty;
            for (int j = 0; j < dtExport.Rows.Count; j++)
            {

                strCellId = "A" + (iRow + j).ToString();
                worksheet.Cells[strCellId].Value = strQcType.ToUpper() +"-"+dtExport.Rows[j]["schedule_code"].ToString()+"-"+ dtExport.Rows[j]["part"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (j == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "B" + (iRow + j).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[j]["part_description"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (j == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "C" + (iRow + j).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[j]["item_list"];
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (j == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
            }

            return worksheet;
        }

        private string GenStdDocFomat(string strPrfix)
        {
            string strSql = string.Empty;
            string strStdDocFomat = string.Empty;
            int iRunNo = 0;
            DataTable dtCheck = new DataTable();
            strSql = @"select  code_value,run_no from ps_m_ddsr_runno where code_value = '" + strPrfix + "'";
            dtCheck = db.ExecSql_DataTable(strSql, dbConnectionString);
            if (dtCheck.Rows.Count > 0)
            {
                iRunNo = int.Parse(dtCheck.Rows[0]["run_no"].ToString()) + 1;
                strSql = @"update ps_m_ddsr_runno set run_no = " + iRunNo + " where  code_value = '" + strPrfix + "'";
                db.ExecNonQuery(strSql, dbConnectionString);
            }
            else
            {
                iRunNo = 1;
                strSql = @"insert into ps_m_ddsr_runno (code_value,run_no) values ('" + strPrfix + "', " + iRunNo + " ) ";
                db.ExecNonQuery(strSql, dbConnectionString);
            }
            strStdDocFomat = strPrfix + "-" + iRunNo;

            return strStdDocFomat;
        }

        public void AddDdsrFormat(DataTable dt)
        {
            string strStdFormat, strDdsrNo, strType, strSubline, strDiscipline, strPart, strBidNo, str;
            strStdFormat = strDdsrNo = strType = strSubline = strDiscipline = strPart = string.Empty;

            //   --excel.type - job_no.sub_line_name - doc.depart_position_name - excel.part
            foreach (DataRow dr in dt.Rows)
            {
                // get isExist by Bidno,schdule
                // if(exist){
                //     do nothing
                // }
                // else{
                // create

                strType = !String.IsNullOrEmpty(dr["type"].ToString()) ? dr["type"].ToString() : string.Empty;
                strSubline = !String.IsNullOrEmpty(dr["sub_line_name"].ToString()) ? dr["sub_line_name"].ToString() : string.Empty;
                strDiscipline = !String.IsNullOrEmpty(dr["depart_position_name"].ToString()) ? dr["depart_position_name"].ToString() : string.Empty;
                strPart = !String.IsNullOrEmpty(dr["part"].ToString()) ? dr["part"].ToString() : string.Empty;

                strStdFormat = strType + "-" + strSubline + "-" + strDiscipline + "-" + strPart;
                strDdsrNo = GenStdDocFomat(strStdFormat);
                // insert()

                // }


            }
        }
    }

    public class DdsrExportMaster
    {
        public DdsrExportMaster() { }

        public string strBidNo { get; set; }
        public string strBidDesc { get; set; }
        public string strScheduleName { get; set; }
        public List<DdsrExportDetail> lstDdsrExportDetail { get; set; }

    }

    public class DdsrExportDetail
    {
        public DdsrExportDetail() { }

        public string strDrawingNo { get; set; }
        public string strDrawingName { get; set; }
        public string strRefJob { get; set; }
        public string strRefPart { get; set; }
        public string strDiscipline { get; set; }

    }
}
