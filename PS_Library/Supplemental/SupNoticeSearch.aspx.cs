﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Supplemental
{
    public partial class SupNoticeSearch : System.Web.UI.Page
    {
        clsSupNotice objsn = new clsSupNotice();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z594900";

                if (Request.QueryString["zmode"] != null)
                    this.hidMode.Value = Request.QueryString["zmode"].ToLower();
                else
                    this.hidMode.Value = "";


                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                BindDDL();

            }
        }
        private void BindDDL()
        {
            ddlBidno.Items.Clear();
            DataTable dt = objsn.getDDL_Master();
            if (dt.Rows.Count > 0)
            {
                ddlBidno.DataSource = dt;
                ddlBidno.DataTextField = "bid_no";
                ddlBidno.DataValueField = "bid_no";
                ddlBidno.DataBind();
                ddlBidno.Items.Insert(0, new ListItem("<-- Select -->", ""));
            }
        }
        private void BindingGv()
        {
            DataTable dt = objsn.getDataSN_Master(ddlBidno.SelectedValue, txtSNno.Text.Trim(), txtSubject.Text.Trim());
            gvSNlist.DataSource = dt;
            gvSNlist.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGv();
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }


        protected void gvSNlist_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSNlist.PageIndex = e.NewPageIndex;
            this.BindingGv();
        }

        public string GetUrlSN(object snCode, object Bidno)
        {
            string url = "SupNotice.aspx?zmode=edit&zuserlogin=" + hidLogin.Value + "&sncode=" + snCode+ "&bidno=" + Bidno;
            return url;
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("SupNotice.aspx?zmode=create&zuserlogin=" + hidLogin.Value);
        }
    }
}