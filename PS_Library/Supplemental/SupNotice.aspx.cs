﻿using PS_Library;
using PS_Library.DocumentManagement;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


namespace PS_System.form.Supplemental
{
    public partial class SupNotice : System.Web.UI.Page
    {
        public string strFolderAttTaskNodeId = ConfigurationManager.AppSettings["wfps_att_folder"].ToString();
        clsSupNotice objsn = new clsSupNotice();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z594900";
                if (Request.QueryString["zmode"] != null)
                    this.hidMode.Value = Request.QueryString["zmode"].ToLower();
                else
                    this.hidMode.Value = ""; 
                if (Request.QueryString["sncode"] != null)
                    this.hidSNcode.Value = Request.QueryString["sncode"];
                else
                    this.hidSNcode.Value = "";
                if (Request.QueryString["bidno"] != null)
                    this.hidBidno.Value = Request.QueryString["bidno"];
                else
                    this.hidBidno.Value = "";
           

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                bindDDL();
                bindData();
                if (hidMode.Value == "view")
                {
                    ddlBidno.Enabled = false;
                    txtbiddesc.Enabled = false;
                    txtSN.Enabled = false;
                    txtSubject.Enabled = false;
                    FileUpload1.Enabled = false;
                    UpFileMulti.Enabled = false;
                    btnOpen.Visible = false;
                    btnSubmit.Visible = false;
                    cbDept_list.Enabled = false;

                }
                else if(hidMode.Value == "edit")
                {
                    txtSN.Enabled = false;
                    dvViewComent.Style["display"] = "none";
                }
                else
                {
                    dvViewComent.Style["display"] = "none";
                }
               
            }
        }
        protected void bindData()
        {
            DataTable dt = objsn.getDataSN(hidSNcode.Value);
            DataTable dtData = objsn.getDataTop(hidSNcode.Value, hidBidno.Value);
            if (dtData.Rows.Count > 0)
            {
                if (hidMode.Value == "view" || hidMode.Value == "edit")
                {
                    ddlBidno.Items.FindByText(hidBidno.Value).Selected = true;
                   // ddlBidno.SelectedItem.Text = hidBidno.Value;
                    txtSN.Text = hidSNcode.Value;
                    txtbiddesc.Text = dtData.Rows[0]["bid_desc"].ToString();
                    txtSubject.Text = dtData.Rows[0]["subject"].ToString();
                }
            }
          
            if (dt.Rows.Count > 0)
            {
                gvSNDoc.DataSource = dt;
                gvSNDoc.DataBind();
            }
            else
            {
                gvSNDoc.DataSource = null;
                gvSNDoc.DataBind();
            }
         
        }
        protected void bindDDL()
        {
            ddlBidno.Items.Clear();
            DataTable dt = objsn.getDDL();
            foreach (DataRow dr in dt.Rows)
            {
                ListItem item1 = new ListItem();
                item1.Text = dr["bid_no"].ToString();
                item1.Value = dr["bid_no"].ToString() + "|" + dr["bid_revision"].ToString();
                ddlBidno.Items.Add(item1);
            }
            ddlBidno.DataBind();
            ddlBidno.Items.Insert(0, new ListItem("<-- Select -->", ""));

            ddlDoctype.Items.Clear();
            DataTable dtDocall = objsn.getDocTypeSN();
            if (dtDocall.Rows.Count > 0)
            {
                ddlDoctype.DataSource = dtDocall;
                ddlDoctype.DataTextField = "doctype_name";
                ddlDoctype.DataValueField = "doctype_code";
                ddlDoctype.DataBind();
                ddlDoctype.Items.Insert(0, new ListItem("<-- Select -->", ""));
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void ddlBidno_SelectedIndexChanged(object sender, EventArgs e)
        { 
            string bidno = "";
            string rev = "";
            if (ddlBidno.SelectedValue != "")
            {
                string[] sp = ddlBidno.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
            DataTable dt = objsn.getData(bidno);
            if (dt.Rows.Count > 0)
            {
                txtbiddesc.Text = dt.Rows[0]["bid_desc"].ToString();
            }
        }

        protected void gvSNDoc_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "deletedata")
            {

                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvSNDoc = (GridView)sender;

                string sncode = ((Label)gvSNDoc.Rows[xrowid].FindControl("gvSNDoc_sncode")).Text;
                string rowid = ((Label)gvSNDoc.Rows[xrowid].FindControl("gvSNDoc_rowid")).Text;
                string doctype = ((Label)gvSNDoc.Rows[xrowid].FindControl("gvSNDoc_code")).Text;
                string nodeid = ((Label)gvSNDoc.Rows[xrowid].FindControl("gvSNDoc_nodeid")).Text;

                objsn.deleteAttach(doctype, nodeid, sncode, rowid);
                bindData();
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully.');", true);
            }
        }

        protected void btnSaveDoc_Click(object sender, EventArgs e)
        {
            string bidno = "";
            string rev = "";
            bool chk = false;
            if (ddlDoctype.SelectedValue!="")
            {
                if (ddlBidno.SelectedValue != "")
                {
                    string[] sp = ddlBidno.SelectedValue.Split('|');
                    if (sp.Length > 1)
                    {
                        bidno = sp[0];
                        rev = sp[1];
                    }
                    Node jobFoldernode = new Node();
                    Node DeptFoldernode = new Node();
                    Node SectionFoldernode = new Node();
                    Node Doctypenode = new Node();

                    string docid = "";
                    string DoctypeFoldername = ddlDoctype.SelectedValue + " - " + ddlDoctype.SelectedItem.Text;

                    OTFunctions ot = new OTFunctions();
                    jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, bidno);
                    if (jobFoldernode == null)
                    {
                        jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, bidno);
                    }
                    DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
                    if (DeptFoldernode == null)
                    {
                        DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
                    }
                    SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
                    if (SectionFoldernode == null)
                    {
                        SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
                    }
                    Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
                    if (Doctypenode == null)
                    {
                        Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
                    }
                    if (FileUpload1.FileName != "")
                    {
                        docid = ot.uploadDoc(Doctypenode.ID.ToString(), FileUpload1.FileName, FileUpload1.FileName, FileUpload1.FileBytes, "").ToString();
                        objsn.insertAttach(ddlDoctype.SelectedValue, docid, FileUpload1.FileName, hidLogin.Value, hidSNcode.Value, bidno, rev,txtSubject.Text,"", txtSN.Text);
                        chk = true;

                       
                    }
                    foreach (HttpPostedFile uploadedFile in UpFileMulti.PostedFiles)
                    {
                        if (uploadedFile.FileName !="")
                        {
                            byte[] fileData = null;
                            using (var binaryReader = new BinaryReader(uploadedFile.InputStream))
                            {
                                fileData = binaryReader.ReadBytes(uploadedFile.ContentLength);
                                docid = ot.uploadDoc(Doctypenode.ID.ToString(), uploadedFile.FileName, uploadedFile.FileName, fileData, "").ToString();
                                objsn.insertAttach(ddlDoctype.SelectedValue, docid, uploadedFile.FileName, hidLogin.Value, hidSNcode.Value, bidno, rev, txtSubject.Text, "", txtSN.Text);
                                chk = true;
                            }
                        }
                      
                    }
                    if(chk == true) ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Upload Successfully.');", true);
                    else ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Choose File to Upload.');", true);
                    hidSNcode.Value = txtSN.Text;
                    bindData();
                }
                else
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select Bid No.');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select Document Type.');", true);
            }

        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Redirect("SupNoticeSearch.aspx?zuserlogin=" + hidLogin.Value);
        }

        protected void gvSNDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                if (hidMode.Value == "view")
                    e.Row.Cells[4].Visible = false;
                

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //ImageButton imgDelete = (ImageButton)e.Row.FindControl("imgDelete");
                if (hidMode.Value == "view")
                    e.Row.Cells[4].Visible = false;
                   
                Label SNDoc_by = (Label)e.Row.FindControl("gvSNDoc_by");
                EmpInfoClass empinfo = new EmpInfoClass();
                var emp = empinfo.getInFoByEmpID(drv["updated_by"].ToString());
                SNDoc_by.Text = emp.SNAME + " (" + emp.MC_SHORT + ")";
            }

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string bidno = "";
            string rev = "";
            string dept = "";
            if (ddlBidno.SelectedValue != "")
            {
                string[] sp = ddlBidno.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
       
            foreach (ListItem item in cbDept_list.Items)
            {
                if (item.Selected)
                {
                    //dept += (dept == "" ? "" : ",") + item.Text;
                }
            }
            if (hidSNcode.Value != "") objsn.updateAtt(bidno, txtSubject.Text, hidSNcode.Value, dept, rev, hidLogin.Value);
            else objsn.updateAtt(bidno, txtSubject.Text, txtSN.Text, dept, rev, hidLogin.Value);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true);
        }
   
    }
}