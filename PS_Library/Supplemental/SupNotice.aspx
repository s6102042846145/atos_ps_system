﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupNotice.aspx.cs" Inherits="PS_System.form.Supplemental.SupNotice" %>



<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
        .auto-style1 {
            width: 1000px;
        }

        .auto-style5 {
            width: 58px;
        }
       .picker{

            vertical-align:middle;
        }
          .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
         .docview {
            height: 650px;
            width: 100%;
        }
    </style>
      
 <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
      <script type="text/javascript">
          $(document).keypress(
              function (event) {
                  if (event.which == '13') {
                      event.preventDefault();
                  }
              });
         
<%--
          $(document).ready(function () {
              maintainScrollPosition();
          });

          function pageLoad() {
              maintainScrollPosition();
          }

          function setScrollPosition(scrollValue) {
              $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }

        function maintainScrollPosition() {
            $("#bigDiv").scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
          }--%>

     
      </script>
</head>
<body>
    <form id="form1" runat="server">
      
        <div>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Supplemental Notice"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
       <div style="margin:10px">
       <div style="width: 100%">
                <table>
                    <tr>
                        <td style="width:150px">
                            <asp:Label ID="Label1" runat="server" Text="Bid No.:"></asp:Label>
                        </td>
                        <td style="width:500px">
                            <asp:DropDownList ID="ddlBidno" runat="server" Width="200px" OnSelectedIndexChanged="ddlBidno_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                        </td>
                    </tr>
                        <tr>
                        <td >
                            <asp:Label ID="Label4" runat="server" Text="Bid Description:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtbiddesc" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="500px" ReadOnly="true" Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:Label ID="Label2" runat="server" Text="S/N No.:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSN" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td >
                            <asp:Label ID="Label3" runat="server" Text="Subject:"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubject" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                                &nbsp;&nbsp;
                        </td>
                    </tr>
                  
                </table>
            </div>
        <div>
            <table style="width:100%">
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="หน่วยงานที่ต้องการแก้ไข Price Schedule:"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left:8%">
                        
                     <asp:CheckBoxList id="cbDept_list" runat="server"  >
                         <asp:ListItem>Item 1</asp:ListItem>
                         <asp:ListItem>Item 2</asp:ListItem>
                         <asp:ListItem>Item 3</asp:ListItem>
                         <asp:ListItem>Item 4</asp:ListItem>
                       
                      </asp:CheckBoxList>
                    </td>
                </tr>
                   <tr>
                    <td  >
                        <asp:Label ID="Label8" runat="server" Text="Upload File:"></asp:Label>
                        <br />
                        <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-flex;" ></asp:FileUpload>
                           <asp:FileUpload ID="UpFileMulti" runat="server"  AllowMultiple="true" style="display:inline"/>  
    <%--                   <asp:Button runat="server" ID="uploadedFile" Text="Upload" OnClick="uploadFile_Click" />  
                       <asp:Label ID="listofuploadedfiles" runat="server" /> --%> 
                         <button id="btnOpen" runat="server" onclick="document.getElementById('myModal').style.display='block';return false;" class="btn btn-primary">Add</button>
                          <%--<asp:Button ID="btnUploadDoc" runat="server" Text="Add" CssClass="btn btn-primary" width="100px"  Font-Names="tahoma" Font-Size="10pt" />--%>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvSNDoc"  runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="100%" 
                                    CellPadding="4" ForeColor="#333333" Visible="true" OnRowCommand="gvSNDoc_RowCommand" OnRowDataBound="gvSNDoc_RowDataBound" >
                                      <AlternatingRowStyle BackColor="#EFF3FB" />
                                <Columns>
                                  

                                    <asp:TemplateField HeaderText="Document Type" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvSNDoctype_name" runat="server" Text='<%# Bind("doctype_name")%>'></asp:Label>
                                      <asp:Label ID="gvSNDoc_code" runat="server" Text='<%# Bind("doctype_code")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Name" >
                                        <ItemTemplate>
                                                   <asp:Label ID="gvSNDoc_link" runat="server" Text='<%# Bind("doc_namelnk")%>'></asp:Label>
                                            <asp:Label ID="gvSNDoc_name" runat="server" Text='<%# Bind("doc_name")%>' Visible="false"></asp:Label>
                                              <asp:Label ID="gvSNDoc_nodeid" runat="server" Text='<%# Bind("doc_nodeid")%>' Visible="false"></asp:Label>
                                             <asp:Label ID="gvSNDoc_sncode" runat="server" Text='<%# Bind("supplementalnotice_code")%>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Document Date" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvSNDoc_date" runat="server" Text='<%# Eval("created_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="By" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvSNDoc_by" runat="server"></asp:Label>
                                             <asp:Label ID="gvSNDoc_rowid" runat="server" Visible="false" Text='<%# Bind("rowid")%>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="" >
                                        <ItemTemplate>
                              <asp:ImageButton ID="imgDelete" ClientIDMode="Static" runat="server" ImageUrl="../../Images/bin.png" CommandName="deletedata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Delete" Width="20" ImageAlign="NotSet"  />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                    </asp:TemplateField>
                                       </Columns>
                          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="White" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="dvViewComent" runat="server">
                            <asp:Label ID="Label7" runat="server" Text="Comment:"></asp:Label>
                            <br /> <asp:TextBox ID="txtCommentView" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="800px" Rows="3" TextMode="MultiLine"></asp:TextBox>
                            <br />         <br />
                            <asp:Button ID="btnAppr" runat="server" Text="Approve" CssClass="btn btn-primary"  Font-Names="tahoma" Font-Size="10pt" />
                       &nbsp; <asp:Button ID="btnReg" runat="server" Text="Reject" CssClass="btn btn-danger"  Font-Names="tahoma" Font-Size="10pt" />
                        </div>
                    </td>
                </tr>
                 
                <tr>
                    <td style="text-align:right">
                         <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnSubmit_Click" />
                       &nbsp; <asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn btn-danger"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnClose_Click" />
                    </td>
                </tr>
            </table>
        </div>
            </div>
           <div id="myModal"  runat="server" class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                <table style="width:100%">
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Text="Document Type:"></asp:Label>
                            &nbsp;<asp:DropDownList ID="ddlDoctype" runat="server" Width="200px"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align:right">
                            <asp:Button ID="btnSaveDoc" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSaveDoc_Click"/>
                           &nbsp; <button onclick="document.getElementById('myModal').style.display='none';return false;" class="btn btn-danger">Colse</button>
                        </td>
                    </tr>
                </table>
                </div>
               </div>
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" /> 
        <asp:HiddenField ID="hidSNcode" runat="server" /> 
        <asp:HiddenField ID="hidBidno" runat="server" /> 
        <asp:HiddenField ID="hidRev" runat="server" /> 
    </form>
</body>
</html>