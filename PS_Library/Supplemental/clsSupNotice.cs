﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
   public class clsSupNotice
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        public clsSupNotice() { }
        public DataTable getDDL()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct bid_no,bid_revision from ps_t_bid_monitoring order by bid_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDL_Master()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct bid_no from ps_t_supplementalnotice order by bid_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getData(string bidno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select bm.bid_no,bm.bid_revision,(select dbo.udf_StripHTML(bid_desc) from bid_no bo where bo.bid_no=bm.bid_no and bid_revision =(select max(bid_revision) from bid_no where bid_no = '" + bidno + @"')) as bid_desc
                        from ps_t_bid_monitoring bm 
                        where bid_no = '" + bidno + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDocTypeSN()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct doctype_code,doctype_name from ps_m_doctype order by doctype_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataSN_Master(string bidno,string sn,string sub)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select  distinct supplementalnotice_code,bid_no,subject from ps_t_supplementalnotice ";
                if (bidno != "" || sn !="" || sub!="")
                {
                    sql += " where bid_no like '%"+ bidno + "%' and  supplementalnotice_code like '%"+ sn + "%' and subject like '%"+ sub + "%'";
 
                }
                sql += " order by supplementalnotice_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataTop(string sncode,string bidno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select *,(select dbo.udf_StripHTML(bid_desc) from bid_no bo where bo.bid_no=ts.bid_no and bid_revision =(select max(bid_revision) from bid_no where bid_no = '"+ bidno +@"')) as bid_desc
						 from ps_t_supplementalnotice ts  where supplementalnotice_code='" + sncode + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataSN(string sncode)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select *,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code=ps_t_supnotice_att.doctype_code) as doctype_name
,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk 
from ps_t_supnotice_att where supplementalnotice_code='" + sncode + "'";
                sql += " order by doctype_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void deleteAttach(string doccode, string nodeid, string sncode,string rowid)
        {
            string sql = "";
            try
            {
                sql = @"delete from ps_t_supnotice_att where doctype_code='"+ doccode + "' and doc_nodeid ='"+ nodeid + "' and supplementalnotice_code ='"+ sncode + "' and rowid='"+ rowid + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public void updateAtt(string bidno, string subject, string sncode,string dept,string rev, string login)
        {
            DataTable dt = new DataTable();
            string sql = "";
            string sqlchk = "";
            try
            {
                sqlchk = @"select * from ps_t_supplementalnotice  where supplementalnotice_code ='"+ sncode + "'";
                dt = db.ExecSql_DataTable(sqlchk, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"update ps_t_supplementalnotice set subject='" + subject + "', bid_no='" + bidno + "',depart_list='" + dept + "' where supplementalnotice_code ='" + sncode + "'";
                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice
                               ( [supplementalnotice_code]
                              ,[subject]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[depart_list]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + sncode + "' " +
                             " , '" + subject + "' " +
                             " , '" + bidno + "' " +
                             " , '" + rev + "' " +
                             " , '" + dept + "' " +
                             " , '' " +
                             " , '' " +
                             " , '" + login + "' " +
                             " , GETDATE() " +
                             " , '" + login + "' " +
                             " , GETDATE())";
                }

        
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public void insertAttach(string doccode, string nodeid,string docname,string login,string sncode,string bidno,string rev,string subject,string depart,string sncreate)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            string sql = "";
            string strSNCode = "";
            try
            {
                if (sncode != "")
                {
                    strSNCode = sncode;
                }
                else
                {
                    strSNCode = sncreate;
                }
                sql = "delete from ps_t_supplementalnotice where supplementalnotice_code='" + strSNCode + "' and bid_no='" + bidno + "' and bid_revision='" + rev + "' and doc_nodeid=0";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = "select * from ps_t_supplementalnotice where supplementalnotice_code='" + strSNCode + "' and bid_no='" + bidno + "' and bid_revision='" + rev + "' and doc_nodeid='" + nodeid + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {

                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice
                               ( [supplementalnotice_code]
                              ,[subject]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[depart_list]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + strSNCode + "' " +
                             " , '" + subject + "' " +
                             " , '" + bidno + "' " +
                             " , '" + rev + "' " +
                             " , '" + depart + "' " +
                             " , '" + nodeid + "' " +
                             " , '" + docname + "' " +
                             " , '" + login + "' " +
                             " , GETDATE() " +
                             " , '" + login + "' " +
                             " , GETDATE())";
                    db.ExecNonQuery(sql, zetl4eisdb);

                }
                sql = @"select * from ps_t_supnotice_att where supplementalnotice_code='"+ strSNCode + "' and doctype_code='" + doccode + "' and doc_nodeid='" + nodeid + "'";
                dt2 = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt2.Rows.Count > 0)
                {
                    sql = @"update ps_t_supnotice_att set doc_nodeid='"+ nodeid + "',doc_name='"+ docname + "' where supplementalnotice_code='" + strSNCode + "' and doctype_code='" + doccode + "' and doc_nodeid='" + nodeid + "'";
                }
                else
                {
                    sql = @"INSERT INTO ps_t_supnotice_att
                              ( [supplementalnotice_code]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + strSNCode + "' " +
                          " , '" + doccode + "' " +
                          " , '" + nodeid + "' " +
                          " , '" + docname + "' " +
                          " , '" + login + "' " +
                          " , GETDATE() " +
                          " , '" + login + "' " +
                          " , GETDATE())";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }


            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            
        }
        private string genSNCode()
        {
            string xBOMCode = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'SN'";
            DataTable dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'SN' ";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('SN', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            xBOMCode = "SN-" + xRunno.ToString("0000");
            return xBOMCode;
        }
    }
}
