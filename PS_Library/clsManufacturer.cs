﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;

namespace PS_Library
{
    public class clsManufacturer
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        public clsManufacturer() { }
        public DataTable getData()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select * from ps_m_manufacturer where is_delete<>1";
                sql += " order by manufac_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void deleteData(string code,string rowid,string login)
        {
            try
            {
                string sql = "update ps_m_manufacturer set is_delete=1,updated_by='" + login + "',updated_datetime=getdate() where manufac_code='" + code + "' and rowid='"+ rowid + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }


        }
        public void editData(string code, string name, string addr, string remark, string login,string rowid)
        {
            DataTable dt = new DataTable();
            string strQuery = "";
            try
            {
                strQuery = "update ps_m_manufacturer set manufac_name='"+ name + "',manufac_address='"+ addr + "',manufac_remark='"+ remark + "',updated_by='"+login+ "',updated_datetime=getdate() where manufac_code='" + code + "' and rowid='" + rowid + "'";
                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void insertManuCode(string code, string name, string addr, string remark, string login)
        {
            DataTable dt = new DataTable();
            string sql = "";
            string strQuery = "";
            try
            {
                sql = @"select * from ps_m_manufacturer where manufac_code='"+ code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    
                }
                else
                {
                    strQuery = @"INSERT INTO ps_m_manufacturer
                             ( [manufac_code]
                              ,[manufac_name]
                              ,[manufac_address]
                              ,[manufac_remark]
                              ,[is_delete]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + code + "' " +
                          " , '" + name + "' " +
                          " , '" + addr + "' " +
                          " , '" + remark + "' " +
                          " , '0' " +
                          " , '" + login + "' " +
                          " , GETDATE() " +
                          " , '" + login + "' " +
                          " , GETDATE())";

                }
                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }

    }
}
