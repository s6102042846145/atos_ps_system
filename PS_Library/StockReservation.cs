﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using Oracle.DataAccess;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Configuration;
using System.Globalization;
using System.Reflection;
using System.Web.UI.WebControls;

namespace PS_Library
{
    public class StockReservation
    {
        public string connstr { get; set; }

        public string cmdstrFull { get; set; }
        public string cmdstr { get; set; }

        public string cmdvalue { get; set; }
        public string condi { get; set; }
        public string order { get; set; }

        public string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();
        static oraDBUtil oraDBUtil = new oraDBUtil();

        public string psDB = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();


        public string plantFillter = ConfigurationManager.AppSettings["plantFillter"].ToString();
        public string stoLoFillter = ConfigurationManager.AppSettings["stoLoFillter"].ToString();

        //datatbase
        public DataTable _getDT(string cmdstr)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();

            try
            {
                OracleDataAdapter adapter = new OracleDataAdapter(cmdstr, commonDB);

                // Create the builder for the adapter to automatically generate
                // the Command when needed
                OracleCommandBuilder builder = new OracleCommandBuilder(adapter);

                // Create and fill the DataSet using the EMP

                adapter.Fill(ds, "1");
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            dt = ds.Tables[0];
            return dt;
        }

        //SAP
        public DataTable get_sap_stockBalance_old(string condition)
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select    plant_code,STORAGELOCATION_CODE,b.code_old,b.materialgroup_code,material_code,batch_number,b.DESCRIPTION,QUANTITY,a.unit_code,amount,egatcontractno,wbs_code,lastgrdate,lidate from p_m_sap_stockbalance a left join p_m_sap_material b on a.material_code = b.code ";
            condi = "WHERE rownum <= 100 ";
            order = " order by materialgroup_code,batch_number asc";

            bool chk = false;

            //if (condition.Length > 0)
            //{
            //    condi = "where material_code = " + condition + "  and rownum <= 5 "+ " and QUANTITY > 0 and b.code_old = 'SC01N1' and material_code ='3000042941' and batch_number='44941' ";
            //}
            if (condition.Length > 0)
            {
                condi = "where code_old like '%" + condition + "%' or material_code like '%" + condition + "%' or b.DESCRIPTION like '%" + condition + "%' or egatcontractno like '%" + condition + "%'";
            }
           

            cmdstrFull = cmdstr + condi + order;
            try
            {

                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        public DataTable get_sap_stockBalance(string jobNo,string matCode,string contract,string typeEqu,string matGroup, string WBSElement,string ValueType,string stroLo,string des,string fillter)
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select    *  from (select    a.plant_code,STORAGELOCATION_CODE,b.code_old,b.materialgroup_code,d.Name as materialgroup_name,( '('||c.plant_code||'-'||c.code||')'||' '|| c.name) as STORAGELOCATION_Name,material_code,batch_number,b.DESCRIPTION,QUANTITY,a.unit_code,amount,egatcontractno,wbs_code, ";
            cmdstr = cmdstr+ " CASE TO_CHAR(SUBSTR(wbs_code, 11, 3)) WHEN 'RSV' THEN 'Reserve' ELSE 'Assign' END as WBSElement,lastgrdate,lidate,floor(months_between( SYSDATE, lidate) )  as calYear from p_m_sap_stockbalance a left join p_m_sap_material b on a.material_code = b.code left join p_m_sap_storagelocation c on a.STORAGELOCATION_CODE =  c.CODE left join p_m_sap_materialgroup d on b.materialgroup_code = d.CODE ";
            condi = "WHERE rownum <= 100  and IS_ACTIVE = 1";
            order = " order by materialgroup_code,batch_number asc";

            bool chk = false;

            //if (condition.Length > 0)
            //{
            //    condi = "where material_code = " + condition + "  and rownum <= 5 "+ " and QUANTITY > 0 and b.code_old = 'SC01N1' and material_code ='3000042941' and batch_number='44941' ";
            //}
           
            condi = "where  c.name not like '%(x)%' and  wbs_code <> '-' and a.IS_ACTIVE = 1 and wbs_code like '%" + jobNo + "%'"  + " and   UPPER(egatcontractno) like   UPPER('%" + contract + "%')" + " and b.materialgroup_code like '%" + matGroup + "%'"+ " and STORAGELOCATION_CODE like '%" + stroLo + "%'";

            if(typeEqu == "0")
            {
                condi = condi + " and material_code like '%" + matCode + "%'";
            }
            if (typeEqu == "1")
            {
                condi = condi + " and material_code like '6%'  ";
            }
            if (typeEqu == "2")
            {
                condi = condi + " and material_code not like '6%'  ";
            }

            condi = condi + " and a.plant_code = '" + plantFillter + "'";

            var splitFillter = stoLoFillter.Split(',');

            if (splitFillter.Count() > 0)
            {
                foreach (var item in splitFillter)
                {
                    condi = condi + " and SUBSTR(b.code,3,2) not like '%" + item + "%'";
                }
            }
            if (fillter == "SUB")
            {
                condi = condi + "  and SUBSTR(d.code,4,3) = '" + fillter + "' ";
            }
            if (fillter == "LN")
            {
                condi = condi + "  and SUBSTR(d.code,4,2) = '" + fillter + "' ";
            }
           

            condi = condi + ") where WBSElement like '%"+ WBSElement + "%'";

            if(des != "")
            {
                var desSplit = des.Split('+');
                int i = 1;
                foreach (var desss in desSplit)
                {
                    if(i==1)
                    {
                        condi = condi + " and ( ";
                        condi = condi + "    UPPER(DESCRIPTION)  like   UPPER('%" + desss + "%')";
                    }
                    else
                    {
                        condi = condi + " or    UPPER(DESCRIPTION)  like   UPPER('%" + desss + "%')";
                    }
                    
                    if (i == desSplit.Count())
                    {
                        condi = condi + ")";
                    }
                    i++;
                }
               
            }

            if(ValueType =="1")
            {
                condi = condi + " and calYear  <= 12 ";
            }
            if (ValueType == "2")
            {
                condi = condi + " and calYear  > 12 and calYear <=24";
            }
            if (ValueType == "3")
            {
                condi = condi + " and calYear  > 24 and calYear <=60";
            }
            if (ValueType == "4")
            {
                condi = condi + " and calYear  > 60 and calYear <=120";
            }
            if (ValueType == "5")
            {
                condi = condi + " and calYear  > 120 ";
            }


            if(jobNo=="")
            {
                condi = condi + " and  rownum <= 1000 ";
            }

           



            cmdstrFull = cmdstr + condi + order;
            try
            {

                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        public DataTable get_sap_material_plant()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select  DISTINCT(plant_code)  from p_m_sap_material_plant";
            condi = " ";
            order = " ";

            
            //if (condition.Length > 0)
            //{
            //    //condi = "where material_code = " + condition + "  and rownum <= 5 " + " and QUANTITY > 0";
            //}

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }
        public DataTable get_sap_materialgroup()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select * from p_m_sap_materialgroup left join p_m_sap_material b on  a.code = b.materialgroup_code";
            condi = " where   SUBSTR(a.code,0,2) = 'TS'";
            order = " ";


            //if (condition.Length > 0)
            //{
            //    //condi = "where material_code = " + condition + "  and rownum <= 5 " + " and QUANTITY > 0";
            //}

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        public DataTable get_sap_materialgroup(string typeEqu)
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select DISTINCT a.code,a.name from p_m_sap_materialgroup a left join p_m_sap_material b on  a.code = b.materialgroup_code";
            condi = " where   SUBSTR(a.code,0,2) = 'TS' ";
            order = " ";

            if (typeEqu == "0")
            {
                condi = condi + "";
            }
            if (typeEqu == "1")
            {
                condi = condi + " and b.code like '6%'  ";
            }
            if (typeEqu == "2")
            {
                condi = condi + " and b.code not like '6%'  ";
            }

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        public DataTable get_sap_materialgroup(string typeEqu,string filter)
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select DISTINCT a.code,a.name from p_m_sap_materialgroup a left join p_m_sap_material b on  a.code = b.materialgroup_code";
            condi = " where   SUBSTR(a.code,0,2) = 'TS' ";
            order = " ";

            if (typeEqu == "0")
            {
                condi = condi + "";
            }
            if (typeEqu == "1")
            {
                condi = condi + " and b.code like '6%'  ";
            }
            if (typeEqu == "2")
            {
                condi = condi + " and b.code not like '6%'  ";
            }

            if(filter =="SUB")
            {
                condi = condi + " and SUBSTR(a.code,4,3) = 'SUB'  "; 
            }
            if (filter == "LN")
            {
                condi = condi + " and SUBSTR(a.code,4,2) = 'LN'  ";
            }

                cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }



        public DataTable get_sap_storage_location()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select  * from p_m_sap_storagelocation";
            condi = " where name not like '%(x)%'";
            order = " ";


            //if (condition.Length > 0)
            //{
            //    //condi = "where material_code = " + condition + "  and rownum <= 5 " + " and QUANTITY > 0";
            //}
            condi = condi + " and plant_code = '"+ plantFillter+"'";

            var splitFillter = stoLoFillter.Split(',');

            if(splitFillter.Count() >0)
            {
                foreach(var item in splitFillter)
                {
                    condi = condi + " and SUBSTR(code,3,2) not like '%" + item + "%'";
                }
            }

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

       

        public DataTable get_sap_material_plantsloc()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select  DISTINCT(storagelocation_code)  from p_m_sap_material_plantsloc";
            condi = " ";
            order = " ";


            //if (condition.Length > 0)
            //{
            //    //condi = "where material_code = " + condition + "  and rownum <= 5 " + " and QUANTITY > 0";
            //}

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        public DataTable get_m_sap_material()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select  code_old,name  from p_m_sap_material";
            condi = " WHERE rownum <= 100";
            order = " ";


            //if (condition.Length > 0)
            //{
            //    //condi = "where material_code = " + condition + "  and rownum <= 5 " + " and QUANTITY > 0";
            //}

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        public DataTable get_m_sap_storagelocation()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select  code,name  from p_m_sap_storagelocation";
            condi = " WHERE rownum <= 100";
            order = " ";


            //if (condition.Length > 0)
            //{
            //    //condi = "where material_code = " + condition + "  and rownum <= 5 " + " and QUANTITY > 0";
            //}

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

 

        public DataTable get_p_m_sap_stockbalance_ExData()
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select ' ' as QTY,plant_code, STORAGELOCATION_CODE,' ' as STORAGELOCATION_Name, b.code_old,b.materialgroup_code, ' ' as materialgroup_name,material_code,batch_number,b.DESCRIPTION,QUANTITY,a.unit_code,amount,egatcontractno,wbs_code,' ' as WBSElement,' ' as Substation,lastgrdate,lidate 
                             ,' ' as tojobno, ' ' as tosubstation,' ' as toStorageLocation,' ' as toStorageLocationName, ' ' as tobidno, ' ' as toScheduleName,' ' as  Status, ' ' as toEqCode
                            from p_m_sap_stockbalance a left join p_m_sap_material b on a.material_code = b.code";
            condi = " WHERE rownum <= 1";
            order = " order by materialgroup_code,batch_number asc";


           
            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }



        public DataTable get_sap_stockBalance_report(string palnt,string matG,string storageLoc,string eqmCode)
        {
            bool chkFrist = true;
            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @"select    plant_code,STORAGELOCATION_CODE,b.code_old,b.materialgroup_code,material_code,batch_number,b.DESCRIPTION,QUANTITY,a.unit_code,amount,egatcontractno,wbs_code,lastgrdate,lidate from p_m_sap_stockbalance a left join p_m_sap_material b on a.material_code = b.code ";
            condi = "WHERE rownum <= 100";
            order = " order by materialgroup_code,batch_number asc";

       

            if (palnt.Length > 0)
            {
                if (chkFrist)
                {
                    condi = condi+ " and plant_code in (" + palnt + ") ";
                    chkFrist = true;
                }
            }
            if (matG.Length > 0)
            {
                if (chkFrist)
                {
                    condi = condi + " and b.materialgroup_code in (" + matG + ") ";
                    chkFrist = true;
                }
                else
                {
                    condi = condi + "  b.materialgroup_code in (" + matG + ") ";
                }
            }

            if (storageLoc.Length > 0)
            {
                if (chkFrist)
                {
                    condi = condi + " and STORAGELOCATION_CODE in (" + storageLoc + ") ";
                    chkFrist = true;
                }
                else
                {
                    condi = condi + "  STORAGELOCATION_CODE in (" + storageLoc + ") ";
                }
            }

            if (eqmCode.Length > 0)
            {
                if (chkFrist)
                {
                    condi = condi + " and b.code_old in (" + eqmCode + ") ";
                    chkFrist = true;
                }
                else
                {
                    condi = condi + "  b.code_old in (" + eqmCode + ") ";
                }
            }

            cmdstrFull = cmdstr + condi + order;

            try
            {
                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }

            return dt;
        }
        //sql
        public DataTable getStock()
        {
            string sqlFull = "";
            string sql = @"SELECT * FROM [dbo].[ps_m_stock_reserved] ";
            string condi = "Where ";
            string order = " order by mat_desc desc";
            bool chk = false;
            DataTable dtList = new DataTable();
            //if(!ddlPlant.SelectedIndex.Equals(0))
            //{
            //    condi = condi + " [plant] = '" + ddlPlant.SelectedValue +"'";
            //    chk = true;
            //}
            //if (!ddlMatGroup.SelectedIndex.Equals(0))
            //{   
            //    if(chk)
            //    {
            //        condi = condi + " AND ";
            //    }
            //    condi = condi + " [mat_group] = '" + ddlMatGroup.SelectedValue + "'";
            //    chk = true;
            //}
            //if (!ddlStorageLo.SelectedIndex.Equals(0))
            //{
            //    if (chk)
            //    {
            //        condi = condi + " AND ";
            //    }
            //    condi = condi + " [storage_loc] = '" + ddlStorageLo.SelectedValue + "'";
            //    chk = true;
            //}
            //if (!ddlEqCode.SelectedIndex.Equals(0))
            //{
            //    if (chk)
            //    {
            //        condi = condi + " AND ";
            //    }
            //    condi = condi + " [equipment_code] = '" + ddlEqCode.SelectedValue + "'";
            //    chk = true;
            //}

            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                 dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;
        }

        public DataTable getJob()
        {
            string sqlFull = "";
            string sql = @"SELECT DISTINCT(job_no)  FROM [dbo].[job_no] ";
            string condi = " ";
            string order = " ";
            bool chk = false;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;
        }

        public DataTable getJob_sub(string jobNO)
        {
            string sqlFull = "";
            string sql = @"SELECT TOP 1 *  FROM [dbo].[job_no] ";
            string condi = "where  job_no like '%" + jobNO + "%'";
            string order = " ";
            bool chk = false;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;
        }
        public DataTable getEquipcode(string code)
        {
            string sqlFull = "";
            string sql = @"SELECT *  FROM [dbo].[ps_m_equipcode] ";
            string condi = "where  equip_code like '%"+ code+"%'";
            string order = " ";
            bool chk = false;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;
        }

        public DataTable getLocation()
        {
            string sqlFull = "";
            string sql = @"SELECT  * FROM [dbo].[tx_location] ";
            string condi = " ";
            string order = " ";
            bool chk = false;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;
        }


        public string getPID()
        {

            string pidStr = "PID-0001";
            string pid = "";
            string result = UpdatePID();
            // string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            DataTable dtRunno = new DataTable();

            try
            {
                if (result.Equals("success"))
                {
                    cmdstrFull = "";
                    cmdstr = @"select runno from wf_runno ";
                    condi = " WHERE prefix = 'PID' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + condi;

                    dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);
                    if (checkRowData(dtRunno))
                    {
                        pid = dtRunno.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // result = ex.Message;
            }

            if(pid.Length==1)
            {
                pidStr = "PID-000" + pid;
            }
            if (pid.Length == 2)
            {
                pidStr = "PID-00" + pid;
            }
            if (pid.Length == 3)
            {
                pidStr = "PID-0" + pid;
            }
            if (pid.Length == 4)
            {
                pidStr = "PID-" + pid;
            }

            return pidStr;
        }

        public string UpdatePID()
        {
            string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            int runno = 0;
            int runnoNew = 0;
            DataTable dtRunno = new DataTable();
            try
            {
                cmdstrFull = "";
                cmdstr = @"select runno from wf_runno ";
                condi = " WHERE prefix ='PID' and year_thai = '" + yearThai + "' ";
                order = " ";
                cmdstrFull = cmdstr + condi;

                dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);

                if (checkRowData(dtRunno))
                {
                    runno = Convert.ToInt32(dtRunno.Rows[0][0].ToString());
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"update wf_runno ";
                    cmdvalue = " set runno = " + runnoNew;
                    condi = " WHERE prefix ='PID' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }
                else
                {
                    runno = 0;
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"insert into wf_runno ( prefix, runno,year_thai)";
                    cmdvalue = "VALUES  ( 'PID', '" + runnoNew + "' , '" + yearThai+"' )";
                    condi = " ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        public string getWFID()
        {
            string wfid = "PJSTK-0001";
            string result = UpdateWFID();
           // string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            DataTable dtRunno = new DataTable();

            try
            {
                if (result.Equals("success"))
                {
                    cmdstrFull = "";
                    cmdstr = @"select runno from wf_runno ";
                    condi = " WHERE prefix = 'PJSTK' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + condi;

                    dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);
                    if(checkRowData(dtRunno))
                    {
                        wfid = dtRunno.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
               // result = ex.Message;
            }

            return wfid;
        }

        public string UpdateWFID()
        {
            string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            int runno = 0;
            int runnoNew = 0;
            DataTable dtRunno = new DataTable();
            try
            {
                cmdstrFull = "";
                cmdstr = @"select runno from wf_runno ";
                condi = " WHERE prefix ='PJSTK' and year_thai = '" + yearThai + "' ";
                order = " ";
                cmdstrFull = cmdstr + condi;

                dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);

                if (checkRowData(dtRunno))
                {
                    runno = Convert.ToInt32(dtRunno.Rows[0][0].ToString());
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"update wf_runno ";
                    cmdvalue = " set runno = " + runnoNew;
                    condi = " WHERE prefix ='PJSTK' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }
                else
                {
                    runno = 0;
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"insert into wf_runno ( prefix, runno,year_thai)";
                    cmdvalue = "VALUES  ( 'PJSTK', '" + runnoNew + "' , '" + yearThai +"' )";
                    condi = " ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public DataTable get_equipment_code(string code)
        {
            string sqlFull = "";
            string sql = @"SELECT * FROM [dbo].[ps_m_equipcode] ";
            string condi = "where equip_code = '"+ code + "'";
            string order = " ";
            bool chk = true;

            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            return dtList;
        }
        public DataTable get_wf_ps_stock_details_byID(List<int> listID)
        {
            string sqlFull = "";
            string sql = @"SELECT * FROM [dbo].[ps_m_stock_reserved] ";
            string condi = "Where ";
            string order = " order by mat_desc desc";
            bool chk = false;

            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            return dtList;
        }

        public DataTable get_wf_ps_stock_details_Revenue(string equipment_code,string material_code ,string batch)
        {
            string sqlFull = "";
            string sql = @"SELECT sum(reserve_qty) as reserve_qty,sum(confirm_qty) as confirm_qty FROM wf_ps_stock_details ";
            string condi = "Where equipment_code = '" + equipment_code + "' and material_code =" + "'" + material_code + "' and batch = '" + batch + "'";
            string order = " order by mat_desc desc";
            bool chk = false;

          
                sqlFull = sql + condi ;
            
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            return dtList;
        }

        public DataTable get_wf_ps_stock_details_samedata(string process_id, string equipment_code, string material_code, string batch)
        {
            string sqlFull = "";
            string sql = @"SELECT rowid,reserve_qty FROM wf_ps_stock_details ";
            string condi = "Where process_id = '"+ process_id + "' and  equipment_code = '" + equipment_code + "' and material_code =" + "'" + material_code + "' and batch = '" + batch + "'";
            string order = " order by mat_desc desc";
            bool chk = false;


            sqlFull = sql + condi;

            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            return dtList;
        }


        public List<int> insert_wf_ps_stock_request(wf_ps_stock_request wfPsStockRequest)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";
               

                    cmdstr = @"insert into wf_ps_stock_request (";
                    cmdstr = cmdstr + getNameForInsert(wfPsStockRequest) + ")";

                    cmdvalue = "VALUES (";
                    cmdvalue = cmdvalue + getValueForInsert(wfPsStockRequest) + ");";

                    condi = " SELECT SCOPE_IDENTITY();";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                    int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                    listID.Add(id);
                

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }

        public string update_wf_ps_stock_request_status(string id,string reqno,string strMemoNodeId)
        {
            string result = "success";
            DataTable dtRunno = new DataTable(); 

            try
            {

                cmdstrFull = "";

                    cmdstr = @"update wf_ps_stock_request ";
                    cmdvalue = " Set form_status ='Submitted' , doc_memo_nodeid = '"+ strMemoNodeId + "' , stock_reqno ='" + reqno + "' ";
                    condi = " where rowid = " + id;
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public List<int>  insert_wf_ps_stock_details(List<wf_ps_stock_details> listWfPsStockDetail)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {
              
                    cmdstrFull = "";
                foreach (var item in listWfPsStockDetail) {

                    cmdstr = @"insert into wf_ps_stock_details (";
                    cmdstr = cmdstr + getNameForInsert(item) + ")";

                    cmdvalue = "VALUES (";
                    cmdvalue = cmdvalue + getValueForInsert(item) + ");";

                    condi = " SELECT SCOPE_IDENTITY();";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                    int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);
                   
                    listID.Add(id);
                }
                
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }

        public string update_wf_ps_stock_details_status(List<wf_ps_stock_details> listWfPsStockDetail, string reqno)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();

            try
            {

                cmdstrFull = "";
                foreach (var item in listWfPsStockDetail)
                {

                    cmdstr = @"update wf_ps_stock_details ";
                    cmdstr = cmdstr + "Set item_status = '";
                    cmdvalue = item.item_status + "' , ";
                    cmdvalue = cmdvalue + " stock_reqno ='" + reqno + "' ";
                    condi = "where rowid = " +item.rowid;
                    cmdstrFull = cmdstr + cmdvalue+condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string update_wf_ps_stock_details_reserve(List<wf_ps_stock_details> listWfPsStockDetail)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();

            try
            {

                cmdstrFull = "";
                foreach (var item in listWfPsStockDetail)
                {

                    cmdstr = @"update wf_ps_stock_details ";
                    cmdstr = cmdstr + "Set reserve_qty = ";
                    cmdvalue = item.reserve_qty + " ";
                    condi = "where rowid = " + item.rowid;
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public string delete_wf_ps_stock_details(int id)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();

            try
            {

                cmdstrFull = "";
              

                    cmdstr = @"delete wf_ps_stock_details ";
                  
                    cmdvalue = "";
                    condi = "where rowid = " + id + " and item_status = 'New' ";
                    cmdstrFull = cmdstr +  condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                

            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }



        //calculate
        public string calAge(string date)
        {
            string result = "";
            DateTime dateNow = DateTime.Now;


            DateTime dt1 = Convert.ToDateTime(date);
            TimeSpan ts = dateNow - dt1;



            result = calDistanceDate(ts.Days.ToString());

            return result;
        }

        public string calDistanceDate(string days)
        {
            string result = "";

            int years = (Convert.ToInt32(days) / 365);

            int months = (Convert.ToInt32(days) - (years * 365)) / 30;

            string dates = (Convert.ToInt32(days) - ((years * 365) + (months * 30))).ToString();

            result = years + " yrs " + months + " months";

            return result;
        }

        public int checkBooked()
        {
            int countBooked = 5;


            return countBooked;
        }

        public DataTable checkDataDetails(string process_id, string equipment_code, string material_code, string batch)
        {
            int rowid = 0;
            DataTable dt = get_wf_ps_stock_details_samedata(process_id,equipment_code, material_code, batch);

            //if(checkRowData(dt))
            //{
            //    rowid = Convert.ToInt32(dt.Rows[0]["rowid"].ToString());
            //}

            return dt;
        }


        public decimal checkRevenue(string equipment_code, string material_code, string batch)
        {
            Decimal countRevenue = 0;
            DataTable dt = get_wf_ps_stock_details_Revenue(equipment_code, material_code, batch);

            if(checkRowData(dt))
            {
                if(!dt.Rows[0]["confirm_qty"].ToString().Equals(""))
                {
                    if(Convert.ToDecimal( dt.Rows[0]["confirm_qty"].ToString()) >0)
                    {
                        countRevenue = Convert.ToDecimal(dt.Rows[0]["confirm_qty"].ToString());
                    }
                    else
                    {
                        countRevenue = Convert.ToDecimal(dt.Rows[0]["reserve_qty"].ToString());
                    }

                }
            }

            return countRevenue;
        }

        public bool checkReserveQty(int notBook, int ReserveQty)
        {
            bool chk = false;

            if (ReserveQty > notBook)
            {
                chk = false;
            }
            else
            {
                chk = true;
            }

            return chk;
        }

       

        public string getYearThai()
        {
            string yearThai = "";



            string strdate = DateTime.Now.Year.ToString();

            yearThai = (Convert.ToInt32(strdate) + 543).ToString();
            return yearThai;
        }

        public bool checkRowData(DataTable dt)
        {
            bool check = false;
            if (dt.Rows.Count > 0)
            {
                check = true;
            }
            else
            {
                check = false;
            }
            return check;
        }

        public string getNameForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;
            PropertyInfo[] properties = _class.GetType().GetProperties();
          
            foreach (PropertyInfo property in properties)
            {
                if (!property.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {
                        value = property.Name;
                    }
                    else
                    {
                        value = property.Name + ",";
                    }
                    result = result + value;
                }
                count++;
                    
            }
            return result;
        }

        public string cutLastString(string item)
        {
            string result = "";
            result = item.Substring(0, item.Length-1);
            return result;
        }
        public string getValueForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;

            PropertyInfo[] properties = _class.GetType().GetProperties();

           
            foreach (var p in properties)
            {
                if (!p.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {


                        value = "'"+ p.GetValue(_class) + "'";
                    }
                    else
                    {
                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {
                            value = "GETDATE()" + ",";
                        }
                        else
                        {
                            value = "'" +  p.GetValue(_class) + "'"+ ",";
                        }

                    }
                    result = result + value;
                }
                count++;
            }


            return result;
        }

        public bool checkSelectData(GridView gridView,string nameCheck,string qty)
        {
            bool check = false;
            foreach (GridViewRow row in gridView.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl(nameCheck) as CheckBox);
                    if (CheckRow.Checked)
                    {
                        TextBox laQty = (row.Cells[0].FindControl(qty) as TextBox);
                        if(laQty.Text.Length>0)
                        {
                            check = true;
                        }
                        else
                        {
                            check = false;
                            break;
                        }
                    }
                }
            }

              return check;
        }


        public string getSRNo()
        {
            string xSRNo = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'SR' and year_thai = " + (DateTime.Now.Year + 543).ToString();
           // DataTable dt = zdbUtil._findDT(sql, zetl4eisdb);
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, psDB);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'SR' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                zdbUtil.ExecNonQuery(sql, psDB);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('SR', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                zdbUtil.ExecNonQuery(sql, psDB);
            }
            xSRNo = "SR-" + xRunno.ToString("0000") + " /" + (DateTime.Now.Year + 543).ToString();
            return xSRNo;
        }

        //gen serial number
        #region gen serial number
        public List<string> genSerialNo(string eqGroup,string  eqCode,int running,int identifyQty)
        {
            string serialNo = "";
            List<string> listSerialNo = new List<string>();
            string strRunning = "";
            for (int i =0;i < identifyQty;i++)
            {
                
                strRunning = genNumberFormat(running + i, 5,"0");
                serialNo = "[" + eqGroup + "]" + "-" + "[" + eqCode + "]" + "-" + strRunning;
                listSerialNo.Add(serialNo);
                serialNo = "";
            }

            return listSerialNo;
        }

        public string genNumberFormat(int number ,int countFormat,string format)
        {
            string strNumber = "";
            int countPos = 0;

            countPos = number.ToString().Length-1;

            for(int i =1; i <= countFormat; i++)
            {
                if(countFormat - countPos != i)
                {
                    strNumber += format;
                }else
                {
                    strNumber = strNumber + number;
                    break;
                }
            }


            return strNumber;
        }
        #endregion

    }

    //modle

    public class wf_ps_stock_details
    {
        public int rowid { get; set; } = 0;
        public string process_id { get; set; } 
        public string stock_reqno { get; set; } 
        public string job_no { get; set; } 
        public int job_revision { get; set; } = 0;
        public string bid_no { get; set; }
        public int bid_revision { get; set; } = 0;
        public string equipment_code { get; set; } 
        public string material_code { get; set; }
        public string stge_loc { get; set; }
        public string plant { get; set; }
        public string batch { get; set; }
        public decimal reserve_qty { get; set; } = 0;
        public decimal confirm_qty { get; set; } = 0;
        public string uom { get; set; }
        public string to_job_no { get; set; }
        public string to_substation { get; set; }
        public string to_storageLocation { get; set; }
        public string to_bid_no { get; set; }
        public string to_schedule_name { get; set; } 
        public string wbs { get; set; }
        public string item_status { get; set; } 
        public string created_by { get; set; } 
        public DateTime created_datetime { get; set; } = DateTime.Now;
        public string updated_by { get; set; } 
        public DateTime updated_datetime { get; set; } = DateTime.Now;
        public int document_nodeid { get; set; } = 0;
        public string document_name { get; set; } 

    }
    public class wf_ps_stock_request
    {
        public int rowid { get; set; }
        public string process_id { get; set; }
        public string stock_reqno { get; set; }
        public string stock_subject { get; set; }
        public string project_id { get; set; }
        public string reference { get; set; }
        public string note { get; set; }
        public string requestor_id { get; set; }
        public string requestor_name { get; set; }
        public string requestor_position { get; set; }
        public string requestor_position_full { get; set; }
        public DateTime requestor_submitdate { get; set; }
        public string requestor_status { get; set; }
        public string reviewer_id { get; set; }
        public string reviewer_name { get; set; }
        public string reviewer_position { get; set; }
        public string reviewer_position_full { get; set; }
        public DateTime reviewer_apvdate { get; set; }
        public string reviewer_status { get; set; }
        public string approver_id { get; set; }
        public string approver_name { get; set; }
        public string approver_position { get; set; }
        public string approver_position_full { get; set; }
        public DateTime approver_apvdate { get; set; }
        public string approver_status { get; set; }
        public string pm_id { get; set; }
        public string pm_name { get; set; }
        public string pm_position { get; set; }
        public string pm_position_full { get; set; }
        public DateTime pm_apvdate { get; set; }
        public string pm_status { get; set; }
        public string assistdirector_id { get; set; }
        public string assistdirector_name { get; set; }
        public string assistdirector_position { get; set; }
        public string assistdirector_position_full { get; set; }
        public DateTime assistdirector_apvdate { get; set; }
        public string assistdirector_status { get; set; }
        public string director_id { get; set; }
        public string director_name { get; set; }
        public string director_position { get; set; }
        public string director_position_full { get; set; }
        public DateTime director_apvdate { get; set; }
        public string director_status { get; set; }
        public string form_status { get; set; }
        public string form_to { get; set; }
        public string form_cc { get; set; }
        public string created_by { get; set; }
        public DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public DateTime updated_datetime { get; set; }
        public string wf_processid { get; set; }
        public string wf_subprocessid { get; set; }
        public string wf_taskid { get; set; }

    }
}



