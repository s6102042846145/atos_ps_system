﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace PS_Library
{
    public class clsStockReqReport
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private DbControllerBase db = new DbControllerBase();
        public clsStockReqReport() { }

        public DataTable getStockReq(string type)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (type == "reqno")
                {
                    sql = @"select distinct stock_reqno from wf_ps_stock_request where stock_reqno <> ''";
                }
                else if (type == "subject")
                {
                    sql = @"select distinct stock_subject from  wf_ps_stock_request where stock_subject <> ''";
                }
                else if (type == "status")
                {
                    sql = @"select distinct form_status from  wf_ps_stock_request where form_status <> ''";
                }
                else
                {
                    sql = @"select a.rowid
      ,a.process_id
      ,a.stock_reqno
      ,[stock_subject]
      ,[project_id]
      ,[reference]
      ,[note]
      ,[requestor_id]
      ,[requestor_name]
      ,[requestor_position]
      ,[requestor_position_full]
	   ,CONVERT(DATE, requestor_submitdate) requestor_submitdate
      ,[requestor_status]
      ,[reviewer_id]
      ,[reviewer_name]
      ,[reviewer_position]
      ,[reviewer_position_full]
      ,[reviewer_apvdate]
      ,[reviewer_status]
      ,[approver_id]
      ,[approver_name]
      ,[approver_position]
      ,[approver_position_full]
      ,[approver_apvdate]
      ,[approver_status]
      ,[pm_id]
      ,[pm_name]
      ,[pm_position]
      ,[pm_position_full]
      ,[pm_apvdate]
      ,[pm_status]
      ,[assistdirector_id]
      ,[assistdirector_name]
      ,[assistdirector_position]
      ,[assistdirector_position_full]
      ,[assistdirector_apvdate]
      ,[assistdirector_status]
      ,[director_id]
      ,[director_name]
      ,[director_position]
      ,[director_position_full]
      ,[director_apvdate]
      ,[director_status]
      ,[form_status]
      ,[form_to]
      ,[form_cc]
      ,a.created_by
      ,a.created_datetime
      ,a.updated_by
      ,a.updated_datetime
      ,[wf_processid]
      ,[wf_subprocessid]
      ,[wf_taskid]
from wf_ps_stock_request a
left  join wf_ps_stock_details b
on a.stock_reqno = b.stock_reqno
where a.stock_reqno <> ''";


                    sql += " order by process_id";
                }

                dt = db.ExecSql_DataTable(sql, dbConnectionString);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
         
            return dt;
        }

        public DataTable getStockReqFilter(List<string> tojobno, List<string> tosubstation)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
               
                    sql = @"select distinct a.rowid
      ,a.process_id
      ,a.stock_reqno
      ,[stock_subject]
      ,[project_id]
      ,[reference]
      ,[note]
      ,[requestor_id]
      ,[requestor_name]
      ,[requestor_position]
      ,[requestor_position_full]
	   ,CONVERT(DATE, requestor_submitdate) requestor_submitdate
      ,[requestor_status]
      ,[reviewer_id]
      ,[reviewer_name]
      ,[reviewer_position]
      ,[reviewer_position_full]
      ,[reviewer_apvdate]
      ,[reviewer_status]
      ,[approver_id]
      ,[approver_name]
      ,[approver_position]
      ,[approver_position_full]
      ,[approver_apvdate]
      ,[approver_status]
      ,[pm_id]
      ,[pm_name]
      ,[pm_position]
      ,[pm_position_full]
      ,[pm_apvdate]
      ,[pm_status]
      ,[assistdirector_id]
      ,[assistdirector_name]
      ,[assistdirector_position]
      ,[assistdirector_position_full]
      ,[assistdirector_apvdate]
      ,[assistdirector_status]
      ,[director_id]
      ,[director_name]
      ,[director_position]
      ,[director_position_full]
      ,[director_apvdate]
      ,[director_status]
      ,[form_status]
      ,[form_to]
      ,[form_cc]
      ,a.created_by
      ,a.created_datetime
      ,a.updated_by
      ,a.updated_datetime
      ,[wf_processid]
      ,[wf_subprocessid]
      ,[wf_taskid]
from wf_ps_stock_request a
left  join wf_ps_stock_details b
on a.stock_reqno = b.stock_reqno
where a.stock_reqno <> '' ";


                int i = 0;

                if (tojobno.Count > 0 || tosubstation.Count > 0)
                {
                    sql = sql + " and";
                }
                foreach (var job in tojobno)
                {
                    if (i == 0)
                    {
                        sql = sql + " (  to_job_No in ('" + job;
                    }
                    else
                    {
                        sql = sql + "','" + job;
                    }

                    if (i == tojobno.Count - 1)
                    {
                        sql = sql + "')";
                    }
                    i++;
                }

                i = 0;
                foreach (var sub in tosubstation)
                {
                    if (i == 0)
                    {
                        if (tojobno.Count > 0)
                        {
                            sql = sql + " or to_substation in ('" + sub;
                        }
                        else
                        {
                            sql = sql + " ( to_substation in ('" + sub;
                        }

                    }
                    else
                    {
                        sql = sql + "','" + sub;
                    }

                    if (i == tosubstation.Count - 1)
                    {
                        sql = sql + "')";
                    }
                    i++;
                }
                if (tojobno.Count > 0 || tosubstation.Count > 0)
                {
                    sql = sql + ")";
                }




                sql += " order by process_id";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }

        public DataTable getStockDetail(string processid,List< string> tojobno, List<string> tosubstation)
        {
            DataTable dt = new DataTable();
            string sql = "";

            sql = @"select distinct *,b.equip_desc_short from wf_ps_stock_details a left join ps_m_equipcode b on a.equipment_code = b.equip_code where process_id ='" + processid + "' and a.stock_reqno <> ''  ";
            int i = 0;

            if (tojobno.Count > 0 || tosubstation.Count > 0)
            {
                sql = sql + "and";
            }
            foreach (var job in tojobno)
            {
                if (i==0 )
                {
                    sql = sql + " (  to_job_No in ('" + job;
                }
                else
                {
                    sql = sql + "','" + job;
                }

                if(i== tojobno.Count-1)
                {
                    sql = sql + "')";
                }
                i++;
            }
            
            i = 0;
            foreach (var sub in tosubstation)
            {
                if (i == 0 )
                {
                    if( tojobno.Count > 0)
                    {
                        sql = sql + " or to_substation in ('" + sub;
                    }
                    else
                    {
                        sql = sql + " ( to_substation in ('" + sub;
                    }
                   
                }
                else
                {
                    sql = sql + "','" + sub;
                }

                if (i == tosubstation.Count - 1)
                {
                    sql = sql + "')";
                }
                i++;
            }
            if(tojobno.Count>0 || tosubstation.Count>0)
            {
                sql = sql + ")";
            }

            dt = db.ExecSql_DataTable(sql, dbConnectionString);
            return dt;
        }
    }
}
