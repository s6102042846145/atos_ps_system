﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
 

namespace PS_Library
{ 
    public class clsActivityTemplate
    { 
        public clsActivityTemplate() { }
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();

        DbControllerBase db = new DbControllerBase(); 
        oraDBUtil ora = new oraDBUtil();

        #region Job
        public static DataTable GetJobTemplateByPrefix(string strPrefix)
        {
            string dbConString = ConfigurationManager.AppSettings["db_ps"].ToString();
            DataTable dt = new DataTable();
            string strSql = @"select distinct temp_name 
                                from ps_m_activity_job
                                where temp_name like '" + strPrefix + @"%'
                                order by temp_name asc;";
            dt = DbControllerBase.ExecSql_DataTableStatic(strSql, dbConString);

            return dt;
        }
        public DataTable GetJobTemplate()
        {
            DataTable dt = new DataTable();
            string strSql = @"select distinct temp_name 
                                from ps_m_activity_job
                                order by temp_name asc;";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }
        public DataTable GetJobActivityByTemplate(string strTemplateNameSearch, string strActivityNameSearch)
        {
            DataTable dt = new DataTable();
            //string strSql = @"select temp_name,job_type,activity_id,activity_name,depart_position_name,req_drawing,
            string strSql = @"select temp_name,job_type,activity_id,activity_name,section_position_name,req_drawing,
                                assignee_drawing,p6_taskcode_dwg,req_equip,assignee_equip,p6_taskcode_equip,
                                req_tech_doc,assignee_tech_doc,p6_taskcode_tech_doc,
                                next_activity_id,approval_routing,e.email_noti_id,e.email_noti_name,remark,weight_factor                             
                                from ps_m_activity_job 
                                left join ps_m_email_noti e on ps_m_activity_job.email_noti_id =e.email_noti_id 
                                where is_active = 1 ";

            if (!String.IsNullOrEmpty(strTemplateNameSearch))
            {
                strSql += " and temp_name = '" + strTemplateNameSearch + "'";
            }
            if (!String.IsNullOrEmpty(strActivityNameSearch))
            {
                strSql += " and UPPER(activity_name) like '%" + strActivityNameSearch.ToUpper() + "%'";
            }
            strSql += " order by section_position_name asc,activity_id asc , activity_name asc ";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        public DataTable GetJobActivityByActivityId(string strActivityId)
        {
            DataTable dt = new DataTable();
            if (!String.IsNullOrEmpty(strActivityId))
            {
                String strSql = @"select temp_name,activity_id,activity_name
                                from ps_m_activity_job 
                                where activity_id = '" + strActivityId +"'"; 
                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            }
           
            return dt;
        }
        public DataTable GetJobNextActivityList()
        {
            DataTable dt = new DataTable();
            
                String strSql = @"select activity_id as code ,CONCAT( activity_id,':',activity_name,'(',temp_name,')') as name
                                    from ps_m_activity_job order by activity_id asc,activity_name asc";

                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
       
        public void InsertJobActivity(JobActivity objJobActivity)
        {
            string strSqlInsert = "INSERT INTO ps_m_activity_job "
          + " (temp_name "
          + "  ,activity_id "
          + "  , activity_name "
          + "  , req_drawing "
          + "  , assignee_drawing "
          + "  , req_equip "
          + "  , assignee_equip "
          + "  , req_tech_doc "
          + "  , assignee_tech_doc "
          + "  , next_activity_id "
          + "  , email_noti_id "
          + "  , remark "
          + "  , weight_factor "
          + "  , approval_routing "
          + "  , created_by "
          + "  , created_datetime "
          + "  , updated_by "
          + "  , updated_datetime "
          + "  , is_active) "
          + "    VALUES "
          + "    ('" + objJobActivity.temp_name + "'"
          + " , '" + objJobActivity.activity_id + "'"
          + " , '" + objJobActivity.activity_name + "'"
          + " , '" + objJobActivity.req_drawing + "'"
          + " , '" + objJobActivity.assignee_drawing + "'"
          + " , '" + objJobActivity.req_equip + "'"
          + " , '" + objJobActivity.assignee_equip + "'"
          + " , '" + objJobActivity.req_tech_doc + "'"
          + " , '" + objJobActivity.assignee_tech_doc + "'"
          + " , '" + objJobActivity.next_activity_id + "'"
          + " , '" + objJobActivity.email_noti_id + "'"
          + " , '" + objJobActivity.remark + "'"
          + " , " + objJobActivity.weight_factor 
          + " , '" + objJobActivity.approval_routing + "'"
          + " , '" + objJobActivity.created_by + "'"
          + " , GETDATE()"
          + " , '" + objJobActivity.updated_by + "'"
          + " , GETDATE()"
          + " , '" + true + "'"
          + ")";
            db.ExecNonQuery(strSqlInsert, dbConnectionString);
        }
        public void UpdateJobActivity(JobActivity objJobActivity)
        {

            string strSQL = "update ps_m_activity_job set temp_name='" + objJobActivity.temp_name + "'" +
                ",activity_name='" + objJobActivity.activity_name + "'" +
                ",section_position_name='" + objJobActivity.section_position_name + "'" +
                ",req_drawing='" + objJobActivity.req_drawing + "'" +
                ",assignee_drawing='" + objJobActivity.assignee_drawing + "'" +
                ",req_equip='" + objJobActivity.req_equip + "'" +
                ",assignee_equip='" + objJobActivity.assignee_equip + "'" +
                ",req_tech_doc='" + objJobActivity.req_tech_doc + "'" +
                ",assignee_tech_doc='" + objJobActivity.assignee_tech_doc + "'" +
                ",next_activity_id='" + objJobActivity.next_activity_id + "'" +
                ",email_noti_id='" + objJobActivity.email_noti_id + "'" +
                ",remark='" + objJobActivity.remark + "'" +
                ",weight_factor=" + objJobActivity.weight_factor +
                ",approval_routing='" + objJobActivity.approval_routing + "'"+
                ",updated_by='" + objJobActivity.updated_by + "'" +
                ",updated_datetime='" + objJobActivity.updated_datetime + "'" +

                "where activity_id='" + objJobActivity.activity_id + "'";


            db.ExecNonQuery(strSQL, dbConnectionString);

        }
        public void gvJobListCmdDelete(string strActid)

        {
            try
            {
               // string strSQL = "delete from ps_m_activity_job where activity_id='" + strActid + "'";
                string strSQL = "update ps_m_activity_job set is_active = 0 where activity_id='" + strActid + "'";
                db.ExecNonQuery(strSQL, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #endregion
         
        #region both
        public static string GetEmailTemplateContentByIdSta(string strEmailNotiId)
        {
            string dbConString = ConfigurationManager.AppSettings["db_ps"].ToString();
            string strResult = string.Empty;
            DataTable dt = new DataTable();
            string strSql = @"select email_content from ps_m_email_noti 
                                where isdelete is null 
                                and email_noti_id = " + strEmailNotiId + @"
                                order by email_noti_name asc";

            dt = DbControllerBase.ExecSql_DataTableStatic(strSql, dbConString);
            if (dt.Rows.Count > 0)
                strResult = dt.Rows[0]["email_content"].ToString();

            return strResult;
        }
        public DataTable GetEmailTemplateList()
        {
            DataTable dt = new DataTable();
            string strSql = "select email_noti_id, email_noti_name from ps_m_email_noti where isdelete is null order by email_noti_name asc ";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        public DataTable GetCommonOrgByDeptShort(string strDeptNameShort)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct orgsname4 as department ,orgsname5 as section
                                FROM m_syn_employee where orgsname4 = '" + strDeptNameShort + "'";
            dt = ora._getDT(strSql, commonDB);
            return dt;
        }
        public string GetEmailTemplateContentById(string strEmailNotiId)
        {
            string strResult = string.Empty;
            if (!String.IsNullOrEmpty(strEmailNotiId))
            {
                DataTable dt = new DataTable();
                string strSql = @"select email_content from ps_m_email_noti 
                                where isdelete is null 
                                and email_noti_id = " + strEmailNotiId + @"
                                order by email_noti_name asc";

                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
                if (dt.Rows.Count > 0)
                    strResult = dt.Rows[0]["email_content"].ToString();
            }
            return strResult;
        }
        #endregion

        #region Bid
        public static DataTable GetBidTemplateByPrefix(string strPrefix)
        {
            string dbConString = ConfigurationManager.AppSettings["db_ps"].ToString();
            DataTable dt = new DataTable();
            string strSql = @"select distinct temp_name 
                                from ps_m_activity_bid
                                where temp_name like '" + strPrefix + @"%'
                                order by temp_name asc;";
            dt = DbControllerBase.ExecSql_DataTableStatic(strSql, dbConString);

            return dt;
        }
        public DataTable GetBidTemplate()
        {
            DataTable dt = new DataTable();
            string strSql = @"select distinct temp_name 
                                from ps_m_activity_bid
                                order by temp_name asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }
        public DataTable GetBidActivityByTemplate(string strTemplateNameSearch, string strActivityNameSearch)
        {
            DataTable dt = new DataTable();
            //string strSql = @"select temp_name,job_type,activity_id,activity_name,depart_position_name,req_drawing,
            string strSql = @"select temp_name,activity_id,activity_name,section_position_name,req_drawing,
                                assignee_drawing,p6_taskcode_dwg,req_equip,assignee_equip,p6_taskcode_equip,
                                req_tech_doc,assignee_tech_doc,p6_taskcode_tech_doc,
                                next_activity_id,approval_routing,e.email_noti_id,e.email_noti_name,remark,weight_factor                             
                                from ps_m_activity_bid 
                                left join ps_m_email_noti e on ps_m_activity_bid.email_noti_id =e.email_noti_id 
                                where is_active = 1 ";

            if (!String.IsNullOrEmpty(strTemplateNameSearch))
            {
                strSql += " and temp_name = '" + strTemplateNameSearch + "'";
            }
            if (!String.IsNullOrEmpty(strActivityNameSearch))
            {
                strSql += " and UPPER(activity_name) like '%" + strActivityNameSearch.ToUpper() + "%'";
            }
            strSql += " order by section_position_name asc,activity_id asc , activity_name asc ";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        public DataTable GetBidActivityByActivityId(string strActivityId)
        {
            DataTable dt = new DataTable();
            if (!String.IsNullOrEmpty(strActivityId))
            {
                String strSql = @"select temp_name,activity_id,activity_name
                                from ps_m_activity_bid 
                                where activity_id = '" + strActivityId + "'";
                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            }

            return dt;
        }
        public DataTable GetBidNextActivityList()
        {
            DataTable dt = new DataTable();

            String strSql = @"select activity_id as code ,CONCAT( activity_id,':',activity_name,'(',temp_name,')') as name
                                    from ps_m_activity_bid order by activity_id asc,activity_name asc";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public void InsertBidActivity(BidActivity objBidActivity)
        {
            string strSqlInsert = "INSERT INTO ps_m_activity_bid "
          + " (temp_name "
          + "  ,activity_id "
          + "  , activity_name "
          + "  , req_drawing "
          + "  , assignee_drawing "
          + "  , req_equip "
          + "  , assignee_equip "
          + "  , req_tech_doc "
          + "  , assignee_tech_doc "
          + "  , next_activity_id "
          + "  , email_noti_id "
          + "  , remark "
          + "  , weight_factor "
          + "  , approval_routing "
          + "  , created_by "
          + "  , created_datetime "
          + "  , updated_by "
          + "  , updated_datetime "
          + "  , is_active) "
          + "    VALUES "
          + "    ('" + objBidActivity.temp_name + "'"
          + " , '" + objBidActivity.activity_id + "'"
          + " , '" + objBidActivity.activity_name + "'"
          + " , '" + objBidActivity.req_drawing + "'"
          + " , '" + objBidActivity.assignee_drawing + "'"
          + " , '" + objBidActivity.req_equip + "'"
          + " , '" + objBidActivity.assignee_equip + "'"
          + " , '" + objBidActivity.req_tech_doc + "'"
          + " , '" + objBidActivity.assignee_tech_doc + "'"
          + " , '" + objBidActivity.next_activity_id + "'"
          + " , '" + objBidActivity.email_noti_id + "'"
          + " , '" + objBidActivity.remark + "'"
          + " , " + objBidActivity.weight_factor
          + " , '" + objBidActivity.approval_routing + "'"
          + " , '" + objBidActivity.created_by + "'" 
          + " , GETDATE()"
          + " , '" + objBidActivity.updated_by + "'"
          + " , GETDATE()"
          + " , '" + true + "'"
          + ")";
            db.ExecNonQuery(strSqlInsert, dbConnectionString);
        }
        public void UpdateBidActivity(BidActivity objBidActivity)
        {

            string strSQL = "update ps_m_activity_bid set temp_name='" + objBidActivity.temp_name + "'" +
                ",activity_name='" + objBidActivity.activity_name + "'" +
                ",section_position_name='" + objBidActivity.section_position_name + "'" +
                ",req_drawing='" + objBidActivity.req_drawing + "'" +
                ",assignee_drawing='" + objBidActivity.assignee_drawing + "'" +
                ",req_equip='" + objBidActivity.req_equip + "'" +
                ",assignee_equip='" + objBidActivity.assignee_equip + "'" +
                ",req_tech_doc='" + objBidActivity.req_tech_doc + "'" +
                ",assignee_tech_doc='" + objBidActivity.assignee_tech_doc + "'" +
                ",next_activity_id='" + objBidActivity.next_activity_id + "'" +
                ",email_noti_id='" + objBidActivity.email_noti_id + "'" +
                ",remark='" + objBidActivity.remark + "'" +
                ",weight_factor=" + objBidActivity.weight_factor +
                ",approval_routing='" + objBidActivity.approval_routing + "'" +
                ",updated_by='" + objBidActivity.updated_by + "'" +
                ",updated_datetime='" + objBidActivity.updated_datetime + "'" +

                "where activity_id='" + objBidActivity.activity_id + "'";


            db.ExecNonQuery(strSQL, dbConnectionString);

        }
        public void gvBidListCmdDelete(string strActid)

        {
            try
            {
                // string strSQL = "delete from ps_m_activity_job where activity_id='" + strActid + "'";
                string strSQL = "update ps_m_activity_bid set is_active = 0 where activity_id='" + strActid + "'";
                db.ExecNonQuery(strSQL, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #endregion


        public class JobActivity
        {
            public string temp_name { get; set; }
            public string job_type { get; set; }
            public string activity_id { get; set; }
            public string activity_name { get; set; }
            public string depart_position_name { get; set; }
            public string section_position_name { get; set; }
            public bool req_drawing { get; set; }
            public string assignee_drawing { get; set; }
            public bool req_equip { get; set; }
            public string assignee_equip { get; set; }
            public bool req_tech_doc { get; set; }
            public string assignee_tech_doc { get; set; }
            public string next_activity_id { get; set; }
            public int email_noti_id { get; set; }
            public string remark { get; set; }
            public string created_by { get; set; }
            public DateTime created_datetime { get; set; }
            public string updated_by { get; set; }
            public DateTime updated_datetime { get; set; }
            public int is_active { get; set; }
            public float weight_factor { get; set; }
            public string approval_routing { get; set; }

        }

        public class BidActivity
        {
            public string temp_name { get; set; }
            public string activity_id { get; set; }
            public string activity_name { get; set; }
            public string depart_position_name { get; set; }
            public string section_position_name { get; set; }
            public bool req_bid_schedulename { get; set; }
            public bool req_drawing { get; set; }
            public string assignee_drawing { get; set; }
            public bool req_equip { get; set; }
            public string assignee_equip { get; set; }
            public bool req_tech_doc { get; set; }
            public string assignee_tech_doc { get; set; }
            public string next_activity_id { get; set; }
            public string approval_routing { get; set; }
            public int email_noti_id { get; set; }
            public string remark { get; set; }
            public string created_by { get; set; }
            public DateTime created_datetime { get; set; }
            public string updated_by { get; set; }
            public DateTime updated_datetime { get; set; }
            public float weight_factor { get; set; }
            public int is_active { get; set; }

        }

    }

}
