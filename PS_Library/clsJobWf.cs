﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsJobWf
    {
        public clsJobWf()
        {

        }
        public string strJobNo { get; set; }
        public int iJobRevision { get; set; }
        public string strActivityId { get; set; }

        public string strBidNo { get; set; }
        public int iBidRevision { get; set; }
        public string strActivityIdBid { get; set; }
    }
}
