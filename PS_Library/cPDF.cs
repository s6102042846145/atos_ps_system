﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Collections;
using GemBox.Spreadsheet;
using System.Text.RegularExpressions;
using System.Linq;
using System.Drawing;

namespace PS_Library
{
    public  class cPDF
    { 
        public  Microsoft.Office.Interop.Word.Document wordDocument { get; set; }
        public  string ExportWordToPdf(FileInfo wordFile, string output_PDF_Path, ref string strErrorMessage)
        {
            string exportFilePath = string.Empty;
            Microsoft.Office.Interop.Word.Application appWord = new Microsoft.Office.Interop.Word.Application();
            try
            {
                if (!Directory.Exists(output_PDF_Path))
                    Directory.CreateDirectory(output_PDF_Path);

                wordDocument = appWord.Documents.Open(wordFile.FullName);

                try
                {
                    wordDocument.FitToPages();
                    wordDocument.PageSetup.TopMargin = 1;
                    wordDocument.PageSetup.LeftMargin = 1;
                    wordDocument.PageSetup.RightMargin = 1;
                    wordDocument.PageSetup.BottomMargin = 1;
                    wordDocument.PageSetup.Orientation = Microsoft.Office.Interop.Word.WdOrientation.wdOrientPortrait;
                    wordDocument.PageSetup.PaperSize = Microsoft.Office.Interop.Word.WdPaperSize.wdPaperA4;
                }
                catch { }

                wordDocument.ExportAsFixedFormat(output_PDF_Path + "\\" + wordFile.Name.Replace(wordFile.Extension, ".pdf"), Microsoft.Office.Interop.Word.WdExportFormat.wdExportFormatPDF);
                exportFilePath = output_PDF_Path + "\\" + wordFile.Name.Replace(wordFile.Extension, ".pdf");
            }
            catch (System.Exception ex)
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                ((Microsoft.Office.Interop.Word._Document)wordDocument).Close();
                ((Microsoft.Office.Interop.Word._Application)appWord).Quit();

                wordDocument = null;
                appWord = null;

                // Mark the export as failed for the return value...
                exportFilePath = string.Empty;
                LogHelper.WriteEx(ex);
                strErrorMessage += ex.Message;
            }
            finally
            {
                // Close the workbook, quit the Excel, and clean up regardless of the results...
                ((Microsoft.Office.Interop.Word._Document)wordDocument).Close();
                ((Microsoft.Office.Interop.Word._Application)appWord).Quit();

                wordDocument = null;
                appWord = null;
            }

            return exportFilePath;
        }

        public  string[] SplitWordToPDF(FileInfo fileInfo, string strPDF_Path, ref string strError)
        {
            string strResult = string.Empty;
            string[] arrFileList = null;
            try
            {
                string strPDF_FileName = ExportWordToPdf(fileInfo, strPDF_Path, ref strError);
                if (strPDF_FileName != "")
                {
                    FileInfo pdfFile = new FileInfo(strPDF_FileName);
                    arrFileList = SplitPagePDF(pdfFile, strPDF_Path, ref strError);

                    pdfFile.Delete();
                    pdfFile = null;
                }
            }
            catch (System.Exception ex)
            {
                strError += ex.Message;
            }
            return arrFileList;
        }

        #region " For File PDF "
        public  string[] SplitPagePDF(FileInfo input_PDF_File, string output_PDF_Path, ref string strErrorMessage)
        {
            PdfSharp.Pdf.PdfDocument doc = new PdfSharp.Pdf.PdfDocument();
            try
            {
                doc = PdfReader.Open(input_PDF_File.FullName, PdfDocumentOpenMode.Import);
            }
            catch (System.Exception ex)
            {
                //workaround if pdfsharp doesn't support this pdf
                MemoryStream sourceStream = new MemoryStream(File.ReadAllBytes(input_PDF_File.FullName));
                MemoryStream outputStream = new MemoryStream();

                sourceStream.Position = 0;

                iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(sourceStream);
                iTextSharp.text.pdf.PdfStamper pdfStamper = new iTextSharp.text.pdf.PdfStamper(reader, outputStream);
                if (outputStream != null)
                {
                    pdfStamper.FormFlattening = true;
                    pdfStamper.Writer.SetPdfVersion(iTextSharp.text.pdf.PdfWriter.PDF_VERSION_1_4);
                    pdfStamper.Writer.CloseStream = false;
                    pdfStamper.Close();

                    doc = PdfReader.Open(outputStream, PdfDocumentOpenMode.Import);
                }
                else
                    strErrorMessage = ex.Message;
            }

            int iPageCount = 1;
            try
            {
                iPageCount = doc.PageCount;
            }
            catch (System.Exception ex)
            {
                string satrError = ex.Message;
                //workaround if pdfsharp doesn't support this pdf
                MemoryStream sourceStream = new MemoryStream(File.ReadAllBytes(input_PDF_File.FullName));
                MemoryStream outputStream = new MemoryStream();

                sourceStream.Position = 0;

                iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(sourceStream);
                iTextSharp.text.pdf.PdfStamper pdfStamper = new iTextSharp.text.pdf.PdfStamper(reader, outputStream);
                if (outputStream != null)
                {
                    pdfStamper.FormFlattening = true;
                    pdfStamper.Writer.SetPdfVersion(iTextSharp.text.pdf.PdfWriter.PDF_VERSION_1_4);
                    pdfStamper.Writer.CloseStream = false;
                    pdfStamper.Close();

                    doc = PdfReader.Open(outputStream, PdfDocumentOpenMode.Import);
                }
            }

            iPageCount = doc.PageCount;
            string[] arrFileList = new string[iPageCount];
            try
            {
                for (int index = 0; index < iPageCount; index++)
                {
                    PdfSharp.Pdf.PdfDocument docSplit = new PdfSharp.Pdf.PdfDocument();
                    docSplit.AddPage(doc.Pages[index]);

                    string strFileSave = output_PDF_Path + "\\" + input_PDF_File.Name.Replace(".PDF", ".pdf").Replace(".pdf", "_p_" + (index + 1).ToString("000") + ".pdf");
                    docSplit.Save(strFileSave);
                    docSplit.Close();

                    arrFileList.SetValue(strFileSave, index);
                }
            }
            catch (System.Exception ex)
            {
                strErrorMessage += ex.Message;
            }
            //finally
            //{
            //    doc.Close();
            //}
            return arrFileList;
        }
        #endregion
    }
}
