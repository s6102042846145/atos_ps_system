﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsGroupingSchedule
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();

        public clsGroupingSchedule() { }
        public DataTable getDataSch(string bidno,string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from vw_ps_bid_new_schedule where bid_no='" + bidno + "' and bid_revision='" + rev + "'";
                sql += " order by part_name";
                dt= db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDisDataSch(string bidno, string rev,string project,string part)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct project_id,part_name from vw_ps_bid_new_schedule where bid_no='" + bidno + "' and bid_revision='" + rev + "' and project_id in('" + project + "') and part_name in('" + part + "')";
                sql += " order by part_name";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataEquip(string bidno, string rev,string part,string code,string itmno,string refpart,string ischk, string jobno)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"SELECT distinct [equip_code]
                              ,[equip_desc]
                              ,[equip_qty]
	                          ,[bid_no]
	                          ,[bid_revision]
                              ,[job_no]
                              ,[refer_part]
                              ,[substation]
                              ,[equip_group]
                              ,[part_assign]
                              ,[item_no]
                              ,case when ([MFC1] <> '') and ([MFC2] = '') then replace([MFC1],',','<br/>')
                                 when ([MFC1] = '') and ([MFC2] <> '') then replace([MFC2],',','<br/>')
                                 when ([MFC1] <> '') and ([MFC2] <> '') then replace([MFC1],',','<br/>') + '<br/>' + replace([MFC2],',','<br/>')
                           else  '' end menufac_file
                           ,isnull(is_new_schedule,0) as is_new_schedule
                          FROM vw_ps_bid_new_schedule_equip  where bid_no='" + bidno + "' and bid_revision='" + rev + "' and part_assign in('" + part + "')";
                if (code != "")
                    sql += " and equip_code in('" + code + "')";
                if (itmno != "")
                    sql += " and item_no in('" + itmno + "')";
                if (refpart != "")
                    sql += " and refer_part in('" + refpart + "')";
                if (ischk == "1")
                    sql += " and is_new_schedule =" + ischk + "";
                else if (ischk == "0")
                    sql += "and (is_new_schedule is null or is_new_schedule=0)";
                if (jobno != "")
                    sql += " and job_no in('" + jobno + "')";
                sql += " order by equip_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void updateNotSelect(string bidno, string rev, string eqcode,string jobno, string itmno)
        {
            try
            {
                string strQueryChk = @"update ps_t_mytask_equip set is_new_schedule=0 where  bid_no = '" + bidno + "' and bid_revision = '" + rev + "' and equip_code = '" + eqcode + "' and job_no ='" + jobno + "' and equip_item_no_b ='" + itmno + "'";
                db.ExecNonQuery(strQueryChk, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void updateIsSelect(string bidno, string rev, string eqcode, string jobno, string itmno)
        {
            try
            {
                string strQueryChk = @"update ps_t_mytask_equip set is_new_schedule=1 where  bid_no = '" + bidno + "' and bid_revision = '" + rev + "' and equip_code in ('" + eqcode + "') and job_no in ('" + jobno + "') and equip_item_no_b in('" + itmno + "')";
                db.ExecNonQuery(strQueryChk, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void DeleteBeforSave(string bidno, string rev, string project)
        {
            try
            {
                string strQueryDel = @"delete from ps_t_bid_grp_schedule  where bid_no = '" + bidno + "' and bid_revision = '" + rev + "' and project_id='" + project + "'";
                db.ExecNonQuery(strQueryDel, zetl4eisdb);

                string strQueryChk = @"update ps_t_mytask_equip set is_new_schedule=0 where  bid_no = '" + bidno + "' and bid_revision = '" + rev + "'";
                db.ExecNonQuery(strQueryChk, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void InsertNewSch(string bidno, string rev, string project, string schOld, string schNew, string sch_descNew, string itmnoNew,string eqcode, string deliNew, string login,string jobno, string itmno_old)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "";
              
                     strQuery = @"INSERT INTO ps_t_bid_grp_schedule
                              ([bid_no]
                          ,[bid_revision]
                          ,[project_id]
                          ,[schedule_old]
                          ,[schedule_new]
                          ,[schedule_desc_new]
                          ,[item_no_new]
                          ,[equip_code]
                          ,[delivery_date]
                          ,[created_by]
                          ,[created_datetime]
                          ,[updated_by]
                          ,[updated_datetime])
                              VALUES
               ('" + bidno + "'"
         + " , '" + rev + "'"
         + " , '" + project + "'"
         + " , '" + schOld + "'"
         + " , '" + schNew + "'"
         + " , '" + sch_descNew + "'"
         + " , '" + itmnoNew + "'"
         + " , '" + eqcode + "'"
         + " , '" + deliNew + "'"
         + " , '" + login + "'"
         + " , GETDATE()"
         + " , '" + login + "'"
         + " , GETDATE())";
                db.ExecNonQuery(strQuery, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public DataTable getisSelect(string bidno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select [bid_no]
      ,[bid_revision]
      ,[project_id]
      ,[schedule_old]
      ,[schedule_new]
      ,[schedule_desc_new]
      ,[item_no_new]
      ,[equip_code]
      ,CONVERT(varchar(20), [delivery_date],1) as delivery_date
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime] from ps_t_bid_grp_schedule   where bid_no='" + bidno + "' and bid_revision='" + rev + "'";
                sql += " order by equip_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
    }
}
