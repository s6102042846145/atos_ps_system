﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsEquipGroup
    {
        static DbControllerBase zdbUtil = new DbControllerBase();
        public string conn = ConfigurationManager.AppSettings["db_ps"].ToString();
        public object gvBid;
        public object gvMyBid;
        public string strBidno;

        public DataTable getEquipInfo(string strEquipId, string strDepartName = "", string strSectionName = "", string strLevel = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = "";
                if (strLevel == "0-SVR")
                {
                    strSQL = @"select equip_code as 'item_code'
                                        ,equip_desc_full as 'item_name'
                                        ,case when sap_material_type = 'ZNV' then 'Major'
                                              when sap_material_type = 'ZCM' then 'Miscellensous'
                                              when sap_material_type = 'SVR' then 'Service'
                                              else '' end as 'item_type'
                                        ,legend as 'item_legend'
                                  from ps_m_equipcode
                                  where isactive = 1 and is_service = 1 ";
                }
                else
                {
                    strSQL = @"select equip_code as 'item_code'
                                        ,equip_desc_full as 'item_name'
                                        ,case when sap_material_type = 'ZNV' then 'Major'
                                              when sap_material_type = 'ZCM' then 'Miscellensous'
                                              when sap_material_type = 'SVR' then 'Service'
                                              else '' end as 'item_type'
                                        ,legend as 'item_legend'
                                  from ps_m_equipcode
                                  where isactive = 1 and is_service = 0 ";
                }

                if (strEquipId != "")
                {
                    strSQL += " and (equip_code LIKE '%" + strEquipId.Trim() + "%' OR equip_desc_full LIKE '%" + strEquipId.Trim() + "%') ";
                }

                //if (strDepartName != "")
                //    strSQL += " AND equip_gong = '" + strDepartName + "'";
                //if (strSectionName != "")
                //    strSQL += " AND equip_pnag = '" + strSectionName + "'";

                strSQL += "order by equip_code";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getEquipGroup_Search(string strSearchValue)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select * from 
                                    (select eg_code,eg_name,eg_desc,eg_order,eg_method
                                            ,REPLACE(STUFF((SELECT equip.equip_code + ' , ' + equip.equip_desc_short
                                                     FROM ps_m_equip_grp_list eg_list
                                                            left outer join ps_m_equipcode equip
                                                            on equip.equip_code = eg_list.equip_code
                                                            where eg_list.eg_code = eg.eg_code 
                                            FOR XML PATH('')),1,1,''),',','<br/>') as equip_list
                                       from ps_m_equip_grp eg) as tb_search
                                  where 1=1 ";
                if (strSearchValue != "")
                {
                    strSQL += " and (tb_search.eg_name LIKE '%" + strSearchValue.Trim() + "%' OR tb_search.equip_list LIKE '%" + strSearchValue.Trim() + "%') ";
                }

                strSQL += "order by tb_search.eg_order";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        private string genEquipGroupCode()
        {
            string xBOMCode = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'EG'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, conn);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'EG' ";
                zdbUtil.ExecNonQuery(sql, conn);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('EG', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                zdbUtil.ExecNonQuery(sql, conn);
            }
            xBOMCode = "EG-" + xRunno.ToString("000000");
            return xBOMCode;
        }
        public void insertEquipGroup(string strEG_Name, string strEG_Desc, string strEquipValueData, string strDocData, string strUserLogin,string strEq_Group, string strMethod, string strAfter)
        {
            try
            {
                DataTable dt = new DataTable();
                DataTable dtchkMet = new DataTable();
                DataTable dtRun = new DataTable();
                string strEquipGroupCode = "";
                string strSQL = "";
                int order = 0;

                strSQL = "select  * from ps_m_equip_grp where eg_name ='" + strAfter + "'";
                dtchkMet = zdbUtil.ExecSql_DataTable(strSQL, conn);
                if (dtchkMet.Rows.Count > 0)
                {
                    order = dtchkMet.Rows[0]["eg_order"].ToString() == "" ? 0 : Convert.ToInt32(dtchkMet.Rows[0]["eg_order"].ToString()) + 1;
                }

                if (strEq_Group != "")
                {
                   
                    strSQL = "update ps_m_equip_grp set eg_name='" + strEG_Name.Replace("'", "''").Trim() + "',eg_order='" + order + "' , updated_datetime = GETDATE() where eg_code ='" + strEq_Group + "'";
                    zdbUtil.ExecNonQuery(strSQL, conn);

                }
                else
                {
                    strEquipGroupCode = genEquipGroupCode();
                    strSQL = @"insert into ps_m_equip_grp(eg_code,eg_name,eg_desc,isdelete,eg_method,eg_order,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strEquipGroupCode + "','" + strEG_Name.Replace("'", "''").Trim() + "','" + strEG_Desc.Replace("'", "''").Trim() + "',0,'" + strMethod + "','"+ order + @"'
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                    zdbUtil.ExecNonQuery(strSQL, conn);

                }

                strSQL = "select row_number() over (order by eg_order )as 'row',* from ps_m_equip_grp where eg_method ='" + strMethod + "' order by eg_order";
                dtRun = zdbUtil.ExecSql_DataTable(strSQL, conn);
                foreach (DataRow dr in dtRun.Rows)
                {
                    strSQL = "update ps_m_equip_grp set eg_order='" + dr["row"] + "' where eg_method ='" + strMethod + "' and eg_name ='" + dr["eg_name"] + "'";
                    zdbUtil.ExecNonQuery(strSQL, conn);
                }

                string[] arrEquip = strEquipValueData.Split('|');
                foreach (string equip in arrEquip)
                {
                    if (strEq_Group != "")
                    {
                        strSQL = "select * from ps_m_equip_grp_list where equip_code ='" + equip + "' and eg_code ='" + strEq_Group + "'";
                        dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
                        if (dt.Rows.Count == 0)
                        {
                            strSQL = @"insert into ps_m_equip_grp_list(eg_code,equip_code,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strEq_Group + "','" + equip + "','" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                        }
                    }
                    else
                    {
                        strSQL = @"insert into ps_m_equip_grp_list(eg_code,equip_code,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strEquipGroupCode + "','" + equip + "','" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                    }

                    zdbUtil.ExecNonQuery(strSQL, conn);
                }

                string[] arrDoc = strDocData.Split('|');
                foreach (string doc in arrDoc)
                {
                    string[] arrData = doc.Split('@');
                    if (strEq_Group != "")
                    {
                        strSQL = "select * from ps_m_equip_grp_doc  where doc_nodeid ='" + arrData[0].ToString() + "' and eg_code ='" + strEq_Group + "'";
                        dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
                        if (dt.Rows.Count == 0)
                        {
                            strSQL = @"insert into ps_m_equip_grp_doc(eg_code,doc_nodeid,doc_name,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strEq_Group + "'," + arrData[0].ToString() + ",'" + arrData[1].ToString() + @"'
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                        }
                    }
                    else
                    {
                        strSQL = @"insert into ps_m_equip_grp_doc(eg_code,doc_nodeid,doc_name,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strEquipGroupCode + "'," + arrData[0].ToString() + ",'" + arrData[1].ToString() + @"'
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                    }


                    zdbUtil.ExecNonQuery(strSQL, conn);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public DataTable searchBOM(string strTextFilter, string strDepartName, string strSectionName)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"SELECT distinct bom.bom_code,bom.bom_name,bom.bom_desc,bom.depart_position_name,bom.section_position_name
                                            ,bom.bomlevel
                                            ,case when bom.bomlevel ='0-EQI' then 'Equipment'
                                                  when bom.bomlevel ='1-ZNV' then 'Major'
                                                  when bom.bomlevel ='2-ZCM' then 'Miscellensous'
                                                  when bom.bomlevel ='3-ASM' then 'Assembly'
                                                  when bom.bomlevel ='4-MOD' then 'Module'
                                                  end  as 'bomlevel_name'
                                            ,bom.legend,bom.cate_code,bom.misc_per_unit
                                            ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                                                                                      when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                                                                                      when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                                                                                      when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                                                                                      when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                                                                                      end 
                                                    FROM ps_m_bom eq_list 
                                                            left outer join ps_m_equipcode as eq
                                                            on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                                                            left outer join ps_m_bom as bom_select
                                                            on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                                                    where eq_list.bom_code = bom.bom_code
                                                    group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                                                    order by eq_list.equip_type
                                                FOR XML PATH('')),1,1,''),',','<br/>')  as equip_list
                                        ,bom.equip_qty
                                        ,bom.equip_unit
                                        ,bom.equip_buyinglot
                                        ,bom.equip_reserve
                                  FROM ps_m_bom as bom
                                  WHERE (bom.bom_name like '%" + strTextFilter + "%') or (bom.bom_desc like '%" + strTextFilter + @"%')
                                        and bom.depart_position_name = '" + strDepartName + "' and bom.section_position_name = '" + strSectionName + "'";

                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable selectBOM_EquipList(string strTextFilter, string strDepartName = "", string strSectionName = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select distinct
                                         case when bom.equip_type ='0-EQI' then eq_main.equip_gong
                                              else bom_child.depart_position_name end as 'department_name'
                                        ,case when bom.equip_type ='0-EQI' then eq_main.equip_pnag
                                              else bom_child.section_position_name end as 'section_name'
                                        ,bom.equip_type  as 'bomlevel_code'
                                        ,case when bom.equip_type ='0-EQI' then 'Equipment'
                                              when bom.equip_type ='1-ZNV' then 'Major'
                                              when bom.equip_type ='2-ZCM' then 'Miscellensous'
                                              when bom.equip_type ='3-ASM' then 'Assembly'
                                              when bom.equip_type ='4-MOD' then 'Module'
                                              end  as 'bomlevel_name'
                                        ,bom.equip_code as 'item_code'
                                        ,case when bom.equip_type ='0-EQI' then eq_main.equip_desc_short + ' (' + eq_main.equip_code + ')'
                                              else bom_child.bom_name end as 'item_name'
                                        ,case when bom.equip_type ='0-EQI' then eq_main.legend
                                              else bom_child.legend end as 'item_legend'
                                        ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                                                                                      when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                                                                                      when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                                                                                      when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                                                                                      when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                                                                                      end 
                                                    FROM ps_m_bom eq_list 
                                                            left outer join ps_m_equipcode as eq
                                                            on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                                                            left outer join ps_m_bom as bom_select
                                                            on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                                                    where eq_list.bom_code = bom.bom_code
                                                    group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                                                    order by eq_list.equip_type
                                                FOR XML PATH('')),1,1,''),',','<br/>') as item_relate
                                        ,bom.equip_qty
                                        ,bom.equip_unit
                                        ,bom.equip_buyinglot
                                        ,bom.equip_reserve
                                  from ps_m_bom as bom
                                        left outer join ps_m_equipcode as eq_main
                                        on eq_main.equip_code = bom.equip_code and bom.equip_type = '0-EQI'
                                        left outer join ps_m_bom as bom_child
                                        on bom_child.bom_code = bom.equip_code and bom.equip_type <> '0-EQI'
                                  WHERE (bom.bom_code = '" + strTextFilter + "') ";

                if (strDepartName != "")
                    strSQL += " AND bom_child.department_name = '" + strDepartName + "' ";
                //if (strSectionName != "")
                //    strSQL += " AND bom_child.section_name = '" + strSectionName + "' ";

                strSQL += " order by bom.equip_type";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable selectBOM_DocList(string strTextFilter, string strDepartName = "", string strSectionName = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"SELECT distinct bom_doc.doc_nodeid,doc_apv.doc_name,doc_apv.doc_revision_display,doc_apv.doc_desc 
                                  FROM ps_m_bom as bom
                                        inner join ps_m_bom_doc as bom_doc
		                                    left outer join ps_m_doc_approve as doc_apv
		                                    on doc_apv.doc_nodeid = bom_doc.doc_nodeid
                                        on bom_doc.bom_code = bom.bom_code
                                  WHERE (bom.bom_code = '" + strTextFilter + "')";

                if (strDepartName != "")
                    strSQL += " AND bom.depart_position_name = '" + strDepartName + "'";
                //if (strSectionName != "")
                //    strSQL += " AND bom.section_position_name = '" + strSectionName + "'";

                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable selectDocByEquipCode(string strEquipCodeList)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select equip.equip_code,equip.doc_nodeid, doc_apv.doc_name, doc_apv.doc_revision_display, doc_apv.doc_desc
                                    from ps_m_equip_doc as equip 
	                                    left outer join ps_m_doc_approve as doc_apv
	                                    on doc_apv.doc_nodeid = equip.doc_nodeid
                                  WHERE equip.equip_code in ('" + strEquipCodeList.Replace(",", "','") + "')";

                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetAttachEquip()
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT [rowid]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[doc_desc]
                              ,[doc_revision_no]
                              ,[doc_revision_display]
                              ,[doc_revision_date]
                              ,[doc_sheetno]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doc_status]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime]
                          FROM ps_m_doc_approve";
                dt = zdbUtil.ExecSql_DataTable(strQuery, conn);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLDcoTypeEquip()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"SELECT DISTINCT doctype_code,doctype_name FROM ps_m_doctype";
                dt = zdbUtil.ExecSql_DataTable(sql, conn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable select_Equiplist(string eq_code)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select eqc.equip_code as 'item_code'
                                        ,equip_desc_full as 'item_name'
                                        ,case when sap_material_type = 'ZNV' then 'Major'
                                              when sap_material_type = 'ZCM' then 'Miscellensous'
                                              when sap_material_type = 'SVR' then 'Service'
                                              else '' end as 'item_type'
                                        ,legend as 'item_legend', eqg.eg_code 
                                  from ps_m_equipcode eqc,ps_m_equip_grp_list eqg where eqc.equip_code = eqg.equip_code and eqg.eg_code = '" + eq_code + "'";
                sql += "order by eqg.eg_code";
                dt = zdbUtil.ExecSql_DataTable(sql, conn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable select_Doclist(string eq_code)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"SELECT docapp.rowid,doctype_code,docapp.doc_nodeid,docapp.doc_name,doc_desc,doc_revision_no,doc_revision_display,docgr.eg_code
FROM ps_m_doc_approve docapp, ps_m_equip_grp_doc docgr where docapp.doc_nodeid = docgr.doc_nodeid and docapp.doc_name = docgr.doc_name
and docgr.eg_code = '" + eq_code + "'";
                sql += "order by docgr.eg_code";
                dt = zdbUtil.ExecSql_DataTable(sql, conn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getEquipGroup_edit(string eq_code)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_m_equip_grp where eg_code = '" + eq_code + "'";
                dt = zdbUtil.ExecSql_DataTable(sql, conn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getEquipName()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_m_equip_grp order by eg_order";
                dt = zdbUtil.ExecSql_DataTable(sql, conn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void delete_EquipList(string eq_code,string eq_name)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"delete from ps_m_equip_grp_list where equip_code in(" + eq_name + ") and eg_code = '" + eq_code + "'";
                zdbUtil.ExecNonQuery(strSQL, conn);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public void delete_DocList(string eq_code, string eq_name)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"delete from ps_m_equip_grp_doc where doc_nodeid in(" + eq_name + ") and eg_code = '" + eq_code + "'";
                zdbUtil.ExecNonQuery(strSQL, conn);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
    }
}

