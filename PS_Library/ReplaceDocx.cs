﻿using Spire.Doc;
using Spire.Doc.Documents;
using Spire.Doc.Fields;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;

namespace EgatED.Class
{
    public class ReplaceDocx
    {
        public byte[] ReplaceData(List<TagData> tagdata, string template_filepath,string output_directory, string output_filepath, bool delete_output)
        {
            var xtemp = template_filepath.Split("\\".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
            string template_fn = xtemp[xtemp.Length - 1];
            string success_fn = "out_"+System.DateTime.Now.ToString("yyyyMMdd_HHmmss")+"_" + template_fn; 
            string temp_processDirectory = output_directory + "\\process_"+ System.DateTime.Now.ToString("yyyyMMdd_HHmmss") ;
            string template_filepath2 = temp_processDirectory + "\\" + template_fn;
            System.IO.Directory.CreateDirectory(temp_processDirectory);
            System.IO.File.Copy(template_filepath, template_filepath2);

            byte[] xoutput_content = new byte[0];
            var templateFile = template_filepath2; // "Template_letter.docx";
            var outPutFile = output_filepath; // @"C:\workspace13\wordtemplate\"+hidDocxName.Value  ;
            Document myDoc = new Document();
            myDoc.LoadFromFile(templateFile);

            for (int i = 0; i < tagdata.Count; i++)
            {
                if (tagdata[i].string_tag == true)  // Replace String
                {
                    myDoc.Replace(tagdata[i].tagname, tagdata[i].tagvalue, true, true);
                }
              
            }
            success_fn = temp_processDirectory + "\\" + success_fn;
            myDoc.SaveToFile(success_fn, FileFormat.Docx);
            myDoc = new Document();
            myDoc.LoadFromFile(success_fn);
           
            for (int i = 0; i < tagdata.Count; i++)
            {
                if (tagdata[i].string_tag == true)  // Replace String
                {
                   // myDoc.Replace(tagdata[i].tagname, tagdata[i].tagvalue, true, true);
                }
                else if (tagdata[i].image_tag == true) // Replace Image
                {
                    Section section = myDoc.Sections[0];
                    TextSelection[] selections = myDoc.FindAllString(tagdata[i].tagname, true, true);

                    int index = 0;
                    TextRange range = null;
                    foreach (TextSelection selection in selections)
                    {
                        DocPicture pic = new DocPicture(myDoc);
                        pic.LoadImage(tagdata[i].imagecontent);
                        range = selection.GetAsOneRange();
                        index = range.OwnerParagraph.ChildObjects.IndexOf(range);
                        range.OwnerParagraph.ChildObjects.Insert(index, pic);
                        range.OwnerParagraph.ChildObjects.Remove(range);
                        
                    }

                }
                else if (tagdata[i].table_tag == true) // Replace TABLE
                {
                    string vertical_alignment = "";
                    string text_alignment = "";
                    Section section = myDoc.Sections[0];
                    TextSelection selection = myDoc.FindString(tagdata[i].tagname, true, true);
                    TextRange range = selection.GetAsOneRange();
                    Paragraph paragraph = range.OwnerParagraph;
                    Body body = paragraph.OwnerTextBody;
                    int index = body.ChildObjects.IndexOf(paragraph);

                    Table table = section.AddTable(true);
                    var dtData = tagdata[i].datatable;
                    string[] Header = new string[dtData.Columns.Count];
                    string xheader_value = "";
                    for (int acol = 0; acol < dtData.Columns.Count; acol++)
                    {
                        xheader_value = dtData.Rows[0][acol].ToString();
                        Header.SetValue(xheader_value, acol);
                    }
                    
                    table.ResetCells(dtData.Rows.Count, dtData.Columns.Count);
                    //Data Headeer
                    TableRow FRow = table.Rows[0];
                    FRow.IsHeader = true;
                    if (tagdata[i].datatable_header_rowheight > 0)
                    { FRow.Height = tagdata[i].datatable_header_rowheight; }
                    else { FRow.Height = 23; }
                    if (tagdata[i].datatable_header_backcolor != null) { FRow.RowFormat.BackColor = tagdata[i].datatable_header_backcolor; }
                    else { FRow.RowFormat.BackColor = Color.Blue; }
                 
                    for (int a = 0; a < Header.Length; a++)
                    {
                        //Cell Alignment
                        Paragraph p = FRow.Cells[a].AddParagraph();
                        try { FRow.Cells[a].Width = tagdata[i].datatable_header_colwidths[a]; } catch { }
                        try { vertical_alignment = tagdata[i].datatable_header_valign_TopMiddleBottom[a]; }
                        catch { vertical_alignment = "Middle"; }
                        try { text_alignment= tagdata[i].datatable_header_textalign_LeftCenterRight[a];  }
                        catch { text_alignment = "Center"; }
                        if (vertical_alignment.Contains("Middle"))
                        {
                            FRow.Cells[a].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                        }else if (vertical_alignment.Contains("Top"))
                        {
                            FRow.Cells[a].CellFormat.VerticalAlignment = VerticalAlignment.Top;
                        }
                        else if (vertical_alignment.Contains("Bottom"))
                        {
                            FRow.Cells[a].CellFormat.VerticalAlignment = VerticalAlignment.Bottom;
                        }
                        if (text_alignment.Contains("Center"))
                        {
                            p.Format.HorizontalAlignment = HorizontalAlignment.Center;
                        }
                        else if (text_alignment.Contains("Left"))
                        {
                            p.Format.HorizontalAlignment = HorizontalAlignment.Left;
                        }
                        else if (text_alignment.Contains("Right"))
                        {
                            p.Format.HorizontalAlignment = HorizontalAlignment.Right;
                        }
                        TextRange TR = p.AppendText(Header[a]);
                        if (tagdata[i].datatable_header_font_name != "")
                        {
                            TR.CharacterFormat.FontName = tagdata[i].datatable_header_font_name;
                        }
                        else
                        {
                            TR.CharacterFormat.FontName = "Tahoma";
                        }
                        if (tagdata[i].datatable_header_font_size > 0)
                        {
                            TR.CharacterFormat.FontSize = tagdata[i].datatable_header_font_size;
                        }
                        else
                        {
                            TR.CharacterFormat.FontSize = 11;
                        }
                        if (tagdata[i].datatable_header_font_color != null)
                        {
                            TR.CharacterFormat.TextColor = tagdata[i].datatable_header_font_color;
                        }
                        else { TR.CharacterFormat.TextColor = Color.White; }
                        if (tagdata[i].datatable_header_font_bold == true) { TR.CharacterFormat.Bold = true; }
                        else { TR.CharacterFormat.Bold = false; }


                    }
                    //Data Row

                    for (int r = 1; r < tagdata[i].datatable.Rows.Count; r++)
                    {
                        
                        string[] dataCol = new string[Header.Length];

                        for (int b = 0; b < Header.Length; b++)
                        {
                           
                            dataCol.SetValue(tagdata[i].datatable.Rows[r][b].ToString(), b);
                            TableRow DataRow = table.Rows[r ];
                            if (tagdata[i].datatable_row_height > 0) { DataRow.Height = tagdata[i].datatable_row_height; } 
                            else { DataRow.Height = 20; }

                            //Cell Alignment
                            try { vertical_alignment = tagdata[i].datatable_col_valign_TopMiddleBottom[b]; }
                            catch { vertical_alignment = "Middle"; }
                            try { text_alignment = tagdata[i].datatable_col_textalign_LeftCenterRight[b]; }
                            catch { text_alignment = "Center"; }
                            if (vertical_alignment.Contains("Middle"))
                            {
                                DataRow.Cells[b].CellFormat.VerticalAlignment = VerticalAlignment.Middle;
                            }
                            else if (vertical_alignment.Contains("Top"))
                            {
                                DataRow.Cells[b].CellFormat.VerticalAlignment = VerticalAlignment.Top;
                            }
                            else if (vertical_alignment.Contains("Bottom"))
                            {
                                DataRow.Cells[b].CellFormat.VerticalAlignment = VerticalAlignment.Bottom;
                            }
                            //Fill Data in Rows
                             Paragraph p2 = DataRow.Cells[b].AddParagraph();
                             TextRange TR2 = p2.AppendText(dataCol[b]);

                            //Format Cells
                            if (text_alignment.Contains("Left"))
                            {
                                p2.Format.HorizontalAlignment = HorizontalAlignment.Left;
                            }
                            else if (text_alignment.Contains("Center"))
                            {
                                p2.Format.HorizontalAlignment = HorizontalAlignment.Center;
                            }
                            else if (text_alignment.Contains("Right"))
                            {
                                p2.Format.HorizontalAlignment = HorizontalAlignment.Right;
                            }
                            else { p2.Format.HorizontalAlignment = HorizontalAlignment.Center; }
                            if (tagdata[i].datatable_col_font_name != "")
                            {
                                TR2.CharacterFormat.FontName = "Tahoma";
                            } else { TR2.CharacterFormat.FontName = "Tahoma"; }
                            if (tagdata[i].datatable_col_font_size > 0)
                            {
                                TR2.CharacterFormat.FontSize = tagdata[i].datatable_col_font_size;
                            }
                            else { TR2.CharacterFormat.FontSize = 10; }
                            if (tagdata[i].datatable_col_font_color != null)
                            {
                                TR2.CharacterFormat.TextColor = tagdata[i].datatable_col_font_color;
                            }
                            else
                            {
                                TR2.CharacterFormat.TextColor = Color.Black;
                            }
                        }
                        

                    }
                    body.ChildObjects.Remove(paragraph);
                    body.ChildObjects.Insert(index, table);
                }
            }
            success_fn = success_fn.Replace("out_", "out2_");
          //  success_fn = temp_processDirectory + "\\" + success_fn; 
            myDoc.SaveToFile( success_fn, FileFormat.Docx);
            xoutput_content = System.IO.File.ReadAllBytes(success_fn);
            System.IO.File.Copy(success_fn, outPutFile,true);
           
            System.IO.Directory.Delete(temp_processDirectory,true);
            if (delete_output == true) {System.IO.File.Delete(outPutFile); }
            return xoutput_content;
        }
        public byte[] convertDOCtoPDF(string xpath_doc_filename, string xpath_pdf_filename, bool delete_output)
        {
           
            object misValue = System.Reflection.Missing.Value;
            String PATH_APP_PDF = xpath_pdf_filename;
            var inputFile = xpath_doc_filename;

            var WORD = new Microsoft.Office.Interop.Word.Application();

            Microsoft.Office.Interop.Word.Document doc = WORD.Documents.Open(inputFile);
            doc.Activate();

            doc.SaveAs(@PATH_APP_PDF, Microsoft.Office.Interop.Word.WdSaveFormat.wdFormatPDF, misValue, misValue, misValue,
            misValue, misValue, misValue, misValue, misValue, misValue, misValue);

            doc.Close();
            WORD.Quit();


            ReleaseObject(doc);
            ReleaseObject(WORD);
            byte[] content = new byte[0];
            if (File.Exists(PATH_APP_PDF) == true)
            {
                content = File.ReadAllBytes(PATH_APP_PDF);
                if (delete_output == true)
                {
                    File.Delete(PATH_APP_PDF);
                }
            }
            return content;
        }
        private void ReleaseObject(object obj)
        {
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(obj);
                obj = null;
            }
            catch (Exception ex)
            {
                //TODO
            }
            finally
            {
                GC.Collect();
            }
        }
        public class TagData
        {
            //Required
            public bool table_tag { get; set; }
            public bool string_tag { get; set; }
            public bool image_tag { get; set; }
            public string tagname { get; set; }

            // For Replace Text
            public string tagvalue { get; set; }
           
            // For Replace Table
            public DataTable datatable { get; set; }
            public Color datatable_header_backcolor { get; set; }
            public string datatable_header_font_name { get; set; }
            public float datatable_header_font_size { get; set; }
            public Color datatable_header_font_color { get; set; }
            public bool datatable_header_font_bold { get; set; }
            public float[] datatable_header_colwidths { get; set; }
            public string[] datatable_header_textalign_LeftCenterRight { get; set; }
            public string[] datatable_header_valign_TopMiddleBottom { get; set; }
            public int   datatable_header_rowheight { get; set; }
            public int   datatable_row_height { get; set; }
            public Color datatable_row_color { get; set; }
            public string[] datatable_col_textalign_LeftCenterRight { get; set; }
            public string[] datatable_col_valign_TopMiddleBottom { get; set; }
            public string datatable_col_font_name { get; set; }
            public float datatable_col_font_size { get; set; }
            public Color datatable_col_font_color { get; set; }
            
            // For Replace Image
            public byte[] imagecontent { get; set; }

        }


    }
}