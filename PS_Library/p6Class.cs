﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using PS_Library.P6ActivityService;
using PS_Library.P6UDFValueService;
using PS_Library.P6AuthenticationService;

namespace PS_Library
{
    public class p6Class
    {
        static System.Net.CookieContainer cookieContainer;

        public string strConn_PS = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string strConn_Common = ConfigurationManager.AppSettings["db_common"].ToString();
        public string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();

        public string filterBid = ConfigurationManager.AppSettings["P6_filterBid"].ToString();
        public string filterJob = ConfigurationManager.AppSettings["P6_filterJob"].ToString();

        static DbControllerBase zdbUtil = new DbControllerBase();
        public Employee Employee = new Employee();
        public oraDBUtil zoraDBUtil = new oraDBUtil();

        public p6Data P6DATA = new p6Data();

        public static void AuthP6()
        {
            string zp6_username = ConfigurationManager.AppSettings["p6_user"].ToString();
            string zp6_password = ConfigurationManager.AppSettings["p6_password"].ToString();

            AuthenticationService authService = new AuthenticationService();
            authService.CookieContainer = new System.Net.CookieContainer();

            Login loginObj = new Login();
            loginObj.UserName = zp6_username;
            loginObj.Password = zp6_password;
            loginObj.DatabaseInstanceId = 1;
            loginObj.DatabaseInstanceIdSpecified = true;
            LoginResponse loginReturn = authService.Login(loginObj);
            if (loginReturn.Return == true) cookieContainer = authService.CookieContainer;
            else
            {
                Console.WriteLine("login failed");
                return;
            }
            ActivityService acService = new ActivityService();
            acService.CookieContainer = cookieContainer;
        }
        public static void Logout()
        {
            try
            {
                string p6_AuthenServiceUrl = ConfigurationManager.AppSettings["p6_authenservice"].ToString();
                AuthenticationService authService = new AuthenticationService();
                authService.CookieContainer = cookieContainer;
                //authService.Url = "http://" + host + ":" + port + "/p6ws/services/AuthenticationService";
                authService.Url = p6_AuthenServiceUrl;
                LogoutResponse logoutReturn = authService.Logout("");
            }
            catch { }
        }
        public static void UpdatepercentActivity(int taskid, Double percent)
        {
            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;

                ReadActivities rp = new ReadActivities();

                var field = new ActivityFieldType[2];
                field[0] = ActivityFieldType.ObjectId;
                field[1] = ActivityFieldType.PercentComplete;
                rp.Field = field;
                rp.Filter = "ObjectId='" + taskid + "'";
                var res = apb.ReadActivities(rp);

                foreach (var item in res)
                {
                    Activity[] fieldUpdate = new Activity[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].PercentComplete = percent;

                    var resUpdate = apb.UpdateActivities(fieldUpdate);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static activitymodel ReadActivitybytaskid(int taskid)
        {
            activitymodel am = new activitymodel();
            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;

                ReadActivities ra = new ReadActivities();

                var cnt = Enum.GetValues(typeof(ActivityFieldType)).Length + 1;
                ActivityFieldType[] field = new ActivityFieldType[cnt];
                field = (ActivityFieldType[])Enum.GetValues(typeof(ActivityFieldType));
                ra.Field = field;
                ra.Filter = "ObjectId='" + taskid + "'";
                var res = apb.ReadActivities(ra);
                if (res.Length > 0)
                {
                    am.taskid = res[0].ObjectId;
                    am.projectid = res[0].ProjectObjectId;
                    am.projectshortname = res[0].ProjectId;
                    am.projectfullname = res[0].ProjectName;
                    am.taskname = res[0].Name;
                    am.taskcode = res[0].Id;
                    am.wbsname = res[0].WBSName;
                    var ted = res[0].Status;
                    am.status = ted.ToString();
                    am.pctcomplete = res[0].PercentComplete;
                    am.pctplan = res[0].SchedulePercentComplete;
                    am.planstart = res[0].PlannedStartDate;
                    am.planfinish = res[0].PlannedFinishDate;
                    am.actualstart = res[0].ActualStartDate;
                    am.actualfinish = res[0].ActualFinishDate;
                    am.datadate = res[0].DataDate;
                    am.reenddate = res[0].FinishDate;
                    am.lastupdateedate = res[0].LastUpdateDate;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return am;
        }
        public static double? Readpercent(int taskid)
        {
            double? percentduration = 0;
            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;
                ReadActivities ra = new ReadActivities();
                var cnt = Enum.GetValues(typeof(ActivityFieldType)).Length + 1;
                ActivityFieldType[] field = new ActivityFieldType[cnt];
                field = (ActivityFieldType[])Enum.GetValues(typeof(ActivityFieldType));
                ra.Field = field;
                ra.Filter = "ObjectId='" + taskid + "'";
                var res = apb.ReadActivities(ra);
                if (res.Length > 0)
                {
                    percentduration = res[0].PercentComplete;
                }
                percentduration = percentduration * 100;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return percentduration;

        }
        public static void UpdatetaskStatus(int taskid, string status)
        {
            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;

                ReadActivities ra = new ReadActivities();

                var field = new ActivityFieldType[1];
                field[0] = ActivityFieldType.Status;

                ra.Field = field;
                ra.Filter = "ObjectId='" + taskid + "'";
                var res = apb.ReadActivities(ra);

                foreach (var item in res)
                {
                    Activity[] fieldUpdate = new Activity[1];
                    fieldUpdate[0] = item;
                    if (status == "NotStarted")
                    {
                        fieldUpdate[0].Status = ActivityStatus.NotStarted;
                    }
                    else if (status == "InProgress")
                    {
                        fieldUpdate[0].Status = ActivityStatus.InProgress;
                    }
                    else if (status == "Completed")
                    {
                        fieldUpdate[0].Status = ActivityStatus.Completed;
                    }

                    var resUpdate = apb.UpdateActivities(fieldUpdate);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void updateTaskStatus(int taskid, DateTime newdate, double actualpercent, bool updateToStart, bool updateToFinish)
        {
            try
            {
                activitymodel resam = ReadActivitybytaskid(taskid);

                if (updateToStart == true && updateToFinish == false)
                {
                    if (resam.status == "NotStarted")
                    {
                        UpdatetaskStatus(taskid, "InProgress");

                        UpdateActualStartDate(taskid, newdate);

                        if (actualpercent > 0)
                        {
                            UpdatepercentActivity(taskid, actualpercent);
                        }

                    }
                    else if (resam.status == "InProgress")
                    {
                        if (actualpercent > 0)
                        {
                            UpdatepercentActivity(taskid, actualpercent);
                        }
                    }
                }
                if (updateToFinish == true)
                {
                    UpdatetaskStatus(taskid, "Completed");

                    UpdateActualFinishDate(taskid, newdate);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void UpdateActualFinishDate(int taskid, DateTime newdate)
        {
            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;

                ReadActivities ra = new ReadActivities();

                var field = new ActivityFieldType[1];
                field[0] = ActivityFieldType.FinishDate;

                ra.Field = field;
                ra.Filter = "ObjectId='" + taskid + "'";
                var res = apb.ReadActivities(ra);

                foreach (var item in res)
                {
                    Activity[] fieldUpdate = new Activity[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].FinishDate = newdate;

                    var resUpdate = apb.UpdateActivities(fieldUpdate);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void UpdateActualStartDate(int taskid, DateTime newdate)
        {
            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;

                ReadActivities ra = new ReadActivities();

                var field = new ActivityFieldType[2];
                field[0] = ActivityFieldType.StartDate;
                field[1] = ActivityFieldType.PercentComplete;

                ra.Field = field;
                ra.Filter = "ObjectId='" + taskid + "'";
                var res = apb.ReadActivities(ra);

                foreach (var item in res)
                {
                    Activity[] fieldUpdate = new Activity[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].StartDate = newdate;

                    var resUpdate = apb.UpdateActivities(fieldUpdate);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #region UDF Comment
        public static void ActionUDFcomment(int taskid, string textinput)
        {
            try
            {
                int cmdid = Getcommenttypeid();

                activitymodel result = Readcomment(taskid, cmdid);

                if (result.comment != null)
                {
                    Updatecomment(taskid, cmdid, textinput);
                }
                else
                {
                    Creatcomment(taskid, cmdid, textinput);

                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static int Getcommenttypeid()
        {
            int resid = 0;
            try
            {
                string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();

                oraDBUtil zoraDBUtil = new oraDBUtil();
                //chenge like to =
                string sql = @"SELECT UDF_TYPE_ID FROM admuser.udftype WHERE udf_type_label = 'Comments' and  disp_data_flag = 'Y' and Table_name = 'TASK'";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    resid = Int32.Parse(dr["UDF_TYPE_ID"].ToString());
                }
                else
                {
                    resid = 0;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return resid;
        }
        public static activitymodel Readcomment(int taskid, int commenttypeid)
        {
            activitymodel am = new activitymodel();
            try
            {
                UDFValueService apb = new UDFValueService();
                apb.CookieContainer = cookieContainer;

                var ru = new ReadUDFValues();
                var cnt = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                var field = new UDFValueFieldType[cnt];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;
                ru.Filter = "ForeignObjectId='" + taskid + "' and UDFTypeObjectId = '" + commenttypeid + "' ";
                var res = apb.ReadUDFValues(ru);


                if (res != null && res.Length > 0)
                {
                    am.taskid = res[0].ForeignObjectId;
                    am.comment = res[0].Text;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return am;
        }
        public static void Creatcomment(int taskid, int commenttypeid, string commenttext)
        {
            try
            {
                UDFValueService uvs = new UDFValueService();
                uvs.CookieContainer = cookieContainer;


                ReadUDFValues ru = new ReadUDFValues();

                var count = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                UDFValueFieldType[] field = new UDFValueFieldType[count];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;

                ru.Filter = "ForeignObjectId='" + taskid + "' ";
                var res = uvs.ReadUDFValues(ru);

                foreach (var item in res)
                {
                    UDFValue[] fieldUpdate = new UDFValue[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].UDFTypeObjectId = commenttypeid;
                    fieldUpdate[0].ForeignObjectId = taskid;
                    fieldUpdate[0].Text = commenttext;

                    var resUpdate = uvs.CreateUDFValues(fieldUpdate);

                    break;
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void Updatecomment(int taskid, int commenttypeid, string commenttext)
        {
            try
            {
                UDFValueService uvs = new UDFValueService();
                uvs.CookieContainer = cookieContainer;

                ReadUDFValues ru = new ReadUDFValues();

                var count = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                UDFValueFieldType[] field = new UDFValueFieldType[count];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;
                ru.Filter = "ForeignObjectId='" + taskid + "' and UDFTypeObjectId = '" + commenttypeid + "' ";
                var res = uvs.ReadUDFValues(ru);

                foreach (var item in res)
                {
                    UDFValue[] fieldUpdate = new UDFValue[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].Text = commenttext;

                    var resUpdate = uvs.UpdateUDFValues(fieldUpdate);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #endregion
        public static bool SendStatus(int taskid)
        {
            bool sentdata = true;
            try
            {
                string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();
                oraDBUtil zoraDBUtil = new oraDBUtil();

                string sql = @"select t.task_id, p.proj_short_name, t.status_code, t.Task_code, task_name, 
                         t.target_start_date, t.target_end_date, t.act_start_date, t.act_end_date, t.reend_date
                         , (select ac_major.actv_code_name 
                            from admuser.taskactv ta 
                            inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                            and ac_major.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                            where ta.task_id = t.task_id and ROWNUM = 1 ) AS MAJOR_Org
                         , (select ac_minor.actv_code_name 
                            from admuser.taskactv ta 
                            inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                            and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                            where ta.task_id = t.task_id  and ROWNUM = 1 ) AS MINOR_Org
                        ,   (SELECT ac.short_name
                                FROM admuser.taskactv ta
                                left join admuser.actvtype act on (act.actv_code_type_id = ta.actv_code_type_id)
                                left join admuser.actvcode ac on (ac.actv_code_id = ta.actv_code_id and act.actv_code_type_id = ac.actv_code_type_id) 
                                where  ta.task_id = '" + taskid + @"' and act.actv_code_type = 'Bid Key Date' and rownum = 1) BidKeyDate 
                        from admuser.project p
                        left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                        where t.task_id = '" + taskid + @"'";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                string bidkeydate;
                string majororg;
                DataRow dr = ds.Tables[0].Rows[0];
                string sPID = dr["task_name"].ToString();
                string[] words = sPID.Split(' ');
                string s1 = words[0].Trim();

                bidkeydate = dr["BidKeyDate"].ToString();
                majororg = dr["MAJOR_Org"].ToString();

                if (bidkeydate == "Yes" || (majororg == "อกส." || majororg == "อผฟ.") || s1 == "WRK")
                {
                    sentdata = false;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return sentdata;
        }
        public static DataTable ReadActivitybyprojectname(string projectname)
        {
            DataTable table = new DataTable();
            table.Columns.Add("projectid", typeof(int));
            table.Columns.Add("taskid", typeof(int));
            table.Columns.Add("projectshortname", typeof(string));
            table.Columns.Add("projectfullname", typeof(string));
            table.Columns.Add("taskname", typeof(string));
            table.Columns.Add("taskcode", typeof(string));
            table.Columns.Add("wbsname", typeof(string));
            table.Columns.Add("status", typeof(string));
            table.Columns.Add("pctplan", typeof(double));
            table.Columns.Add("pctcomplete", typeof(double));
            table.Columns.Add("planstart", typeof(DateTime));
            table.Columns.Add("planfinish", typeof(DateTime));
            table.Columns.Add("actualstart", typeof(DateTime));
            table.Columns.Add("actualfinish", typeof(DateTime));
            table.Columns.Add("reenddate", typeof(DateTime));
            table.Columns.Add("datadate", typeof(DateTime));

            try
            {
                ActivityService apb = new ActivityService();
                apb.CookieContainer = cookieContainer;

                ReadActivities ra = new ReadActivities();

                var cnt = Enum.GetValues(typeof(ActivityFieldType)).Length + 1;
                ActivityFieldType[] field = new ActivityFieldType[cnt];
                field = (ActivityFieldType[])Enum.GetValues(typeof(ActivityFieldType));
                ra.Field = field;
                ra.Filter = "ProjectId like '%" + projectname + "%'";

                Activity[] res = apb.ReadActivities(ra);

                DataRow row;

                foreach (Activity item in res)
                {
                    string sPID = item.ProjectId;
                    string[] arr = sPID.Split('-');
                    if (arr.Length == 4)
                    {
                        row = table.NewRow();

                        row["projectid"] = item.ProjectObjectId;
                        row["taskid"] = item.ObjectId;
                        row["projectid"] = item.ProjectObjectId;
                        row["projectshortname"] = item.ProjectId;
                        row["projectfullname"] = item.ProjectName;
                        row["taskname"] = item.Name;
                        row["taskcode"] = item.Id;
                        row["wbsname"] = item.WBSName;
                        var ted = item.Status;
                        row["status"] = ted.ToString();
                        row["pctcomplete"] = item.PercentComplete;
                        row["pctplan"] = item.SchedulePercentComplete;
                        row["planstart"] = item.PlannedStartDate;
                        row["planfinish"] = item.PlannedFinishDate;

                        if (item.ActualStartDate != null)
                        {
                            row["actualstart"] = item.ActualStartDate;
                        }
                        else
                        {
                            row["actualstart"] = DBNull.Value;
                        }

                        if (item.ActualFinishDate != null)
                        {
                            row["actualfinish"] = item.ActualFinishDate;
                        }
                        else
                        {
                            row["actualfinish"] = DBNull.Value;
                        }

                        if (item.FinishDate != null)
                        {
                            row["reenddate"] = item.FinishDate;
                        }
                        else
                        {
                            row["reenddate"] = DBNull.Value;
                        }

                        if (item.DataDate != DateTime.MinValue)
                        {
                            row["datadate"] = item.DataDate;
                        }
                        else
                        {
                            row["datadate"] = DBNull.Value;
                        }
                        table.Rows.Add(row);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return table;
        }
        public List<p6Data> getTaskList(string xproject)
        {
            List<p6Data> li = new List<p6Data>();
            try
            {
                string sql = @"select  p.proj_id, p.proj_short_name, t.status_code, t.Task_code, w.wbs_id, w.wbs_short_name, w.seq_num,
                         t.Task_ID, t.task_name, w.wbs_name, ta.actv_code_id, acx.codetypename, ac.short_name, ac.actv_code_name,
                         t.target_start_date, t.target_end_date, t.act_start_date, t.act_end_date, t.reend_date
                         from admuser.project p
                         left join admuser.projwbs w on (p.proj_id = w.proj_id)
                         left join admuser.Task t on (p.Proj_ID = t.Proj_ID and t.wbs_id = w.wbs_id)
                         left join admuser.TASKACTV ta on (t.task_id = ta.task_id)
                         left join admuser.actvcode ac on (ta.actv_code_id = ac.actv_code_id)
                         left join admuser.actvcodex acx on (ac.actv_code_id = acx.actv_code_id)
                         where p.proj_short_name like '%" + xproject + @"%' order by w.seq_num";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        DataRow dr = ds.Tables[0].Rows[i];
                        P6DATA = new p6Data();
                        P6DATA.PROJ_ID = dr["proj_id"].ToString();
                        P6DATA.PROJ_SHORT_NAME = dr["proj_short_name"].ToString();
                        P6DATA.TASK_STATUS_CODE = dr["status_code"].ToString();
                        P6DATA.TASK_CODE = dr["task_code"].ToString();
                        P6DATA.WBS_ID = dr["wbs_id"].ToString();
                        P6DATA.WBS_SHORT_NAME = dr["wbs_short_name"].ToString();

                        P6DATA.SEQ_ID = int.Parse(dr["seq_num"].ToString());
                        P6DATA.TASK_ID = dr["task_id"].ToString();
                        P6DATA.TASK_NAME = dr["task_name"].ToString();
                        P6DATA.WBS_NAME = dr["wbs_name"].ToString();

                        P6DATA.ATCV_CODE_ID = dr["actv_code_id"].ToString();
                        P6DATA.ACTV_CODETYPENAME = dr["codetypename"].ToString();
                        P6DATA.ACTV_SHORT_NAME = dr["short_name"].ToString();
                        P6DATA.ACTV_CODE_NAME = dr["actv_code_name"].ToString();

                        P6DATA.TASK_TARGET_START_DATE = dr["target_start_date"].ToString();
                        P6DATA.TASK_TARGET_END_DATE = dr["target_end_date"].ToString();
                        P6DATA.TASK_ACT_START_DATE = dr["act_start_date"].ToString();
                        P6DATA.TASK_ACT_END_DATE = dr["act_end_date"].ToString();
                        P6DATA.TASK_REEND_DATE = dr["reend_date"].ToString();

                        li.Add(P6DATA);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return li;
        }
        public p6Data getTaskData(string xtaskid)
        {
            try
            {
                string sql = @"select  p.proj_id, p.proj_short_name, t.status_code, t.Task_code, w.wbs_id, 
                         w.wbs_short_name, w.seq_num, t.Task_ID, t.task_name, w.wbs_name, 
                         t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date
                         from admuser.project p
                         left join admuser.projwbs w on (p.proj_id = w.proj_id)
                         left join admuser.Task t on (p.Proj_ID = t.Proj_ID and t.wbs_id = w.wbs_id)
                         where t.task_id = '" + xtaskid + "' ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    P6DATA.PROJ_ID = dr["proj_id"].ToString();
                    P6DATA.PROJ_SHORT_NAME = dr["proj_short_name"].ToString();
                    P6DATA.TASK_STATUS_CODE = dr["status_code"].ToString();
                    P6DATA.TASK_CODE = dr["task_code"].ToString();
                    P6DATA.WBS_ID = dr["wbs_id"].ToString();
                    P6DATA.WBS_SHORT_NAME = dr["wbs_short_name"].ToString();

                    P6DATA.SEQ_ID = int.Parse(dr["seq_num"].ToString());
                    P6DATA.TASK_ID = dr["task_id"].ToString();
                    P6DATA.TASK_NAME = dr["task_name"].ToString();
                    P6DATA.WBS_NAME = dr["wbs_name"].ToString();

                    //  P6DATA.ATCV_CODE_ID = dr["actv_code_id"].ToString();
                    //  P6DATA.ACTV_CODETYPENAME = dr["codetypename"].ToString();
                    //  P6DATA.ACTV_SHORT_NAME = dr["short_name"].ToString();
                    //  P6DATA.ACTV_CODE_NAME = dr["actv_code_name"].ToString();

                    P6DATA.TASK_TARGET_START_DATE = dr["target_start_date"].ToString();
                    P6DATA.TASK_TARGET_END_DATE = dr["target_end_date"].ToString();
                    P6DATA.TASK_ACT_START_DATE = dr["act_start_date"].ToString();
                    P6DATA.TASK_ACT_END_DATE = dr["act_end_date"].ToString();
                    P6DATA.TASK_REEND_DATE = dr["reend_date"].ToString();

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return P6DATA;
        }
        public p6Data getTaskData(string xproject, string xwbs_id, string xtaskid)
        {
            try
            {
                string sql = @"select  p.proj_id, p.proj_short_name, t.status_code, t.Task_code, w.wbs_id, 
                         w.wbs_short_name, w.seq_num, t.Task_ID, t.task_name, w.wbs_name, 
                         t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date
                         from admuser.project p
                         left admuser.join projwbs w on (p.proj_id = w.proj_id)
                         left admuser.join Task t on (p.Proj_ID = t.Proj_ID and t.wbs_id = w.wbs_id)
                         where p.proj_short_name = '" + xproject + "' and t.task_id = '" + xtaskid + "' and w.wbs_id = '" + xwbs_id + "' ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    P6DATA.PROJ_ID = dr["proj_id"].ToString();
                    P6DATA.PROJ_SHORT_NAME = dr["proj_short_name"].ToString();
                    P6DATA.TASK_STATUS_CODE = dr["status_code"].ToString();
                    P6DATA.TASK_CODE = dr["task_code"].ToString();
                    P6DATA.WBS_ID = dr["wbs_id"].ToString();
                    P6DATA.WBS_SHORT_NAME = dr["wbs_short_name"].ToString();

                    P6DATA.SEQ_ID = int.Parse(dr["seq_num"].ToString());
                    P6DATA.TASK_ID = dr["task_id"].ToString();
                    P6DATA.TASK_NAME = dr["task_name"].ToString();
                    P6DATA.WBS_NAME = dr["wbs_name"].ToString();

                    //  P6DATA.ATCV_CODE_ID = dr["actv_code_id"].ToString();
                    //  P6DATA.ACTV_CODETYPENAME = dr["codetypename"].ToString();
                    //  P6DATA.ACTV_SHORT_NAME = dr["short_name"].ToString();
                    //  P6DATA.ACTV_CODE_NAME = dr["actv_code_name"].ToString();

                    P6DATA.TASK_TARGET_START_DATE = dr["target_start_date"].ToString();
                    P6DATA.TASK_TARGET_END_DATE = dr["target_end_date"].ToString();
                    P6DATA.TASK_ACT_START_DATE = dr["act_start_date"].ToString();
                    P6DATA.TASK_ACT_END_DATE = dr["act_end_date"].ToString();
                    P6DATA.TASK_REEND_DATE = dr["reend_date"].ToString();

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return P6DATA;
        }
        public DataTable getProjectTaskBid(string tasknamebid)
        {
            //string sql = @"

            //          Select TL.TASK_CODE, TL.TASK_NAME
            // from (
            //    Select max(task_id) as TaskID, w.wbs_short_name, w.seq_num,t.status_code, t.Task_code, t.task_name
            //       --t.Task_ID, t.task_name, t.target_start_date, t.target_end_date, t.act_start_date, t.act_end_date, t.reend_date
            //       from project p
            //       left join projwbs w on (p.proj_id = w.proj_id)
            //       left join Task t on (p.Proj_ID = t.Proj_ID and t.wbs_id = w.wbs_id)
            //       where  t.task_name like 'B-"+ tasknamebid + @"%' 
            //        and t.task_code like 'B-%' 
            //        and t.task_name not like 'WRK%'
            //       and REGEXP_COUNT(t.task_name, '-') = 3 

            //       group by w.wbs_short_name, w.seq_num,t.status_code, t.Task_code, t.task_name
            //       order by t.task_name, t.task_code, w.seq_num
            //        ) TL

            //";
            string sql = @"select bid_no as colvalue, bid_no as colname  from bid_no where bid_no like '%" + tasknamebid + @"%' and status = 'Active' ";


            DataSet ds = zdbUtil.ExecSql_DataSet(sql, strConn_PS);
            return ds.Tables[0];
        }
        public DataTable getProjectTaskJob(string tasknamejob)
        {
            // string sql = @"

            //Select TL.TASK_CODE, TL.TASK_NAME
            //  from (
            //     Select max(task_id) as TaskID, w.wbs_short_name, w.seq_num,t.status_code, t.Task_code, t.task_name
            //        --t.Task_ID, t.task_name, t.target_start_date, t.target_end_date, t.act_start_date, t.act_end_date, t.reend_date
            //        from project p
            //        left join projwbs w on (p.proj_id = w.proj_id)
            //        left join Task t on (p.Proj_ID = t.Proj_ID and t.wbs_id = w.wbs_id)
            //        where  t.task_name like '%"+ tasknamejob +@"%' 
            //         and t.task_code like 'J-%' 
            //         and t.task_name not like 'WRK%'
            //        and REGEXP_COUNT(t.task_name, '-') = 3 
            //        group by w.wbs_short_name, w.seq_num,t.status_code, t.Task_code, t.task_name
            //        order by t.task_name, t.task_code, w.seq_num
            //         ) TL
            // ";

            string sql = @"select job_no as colvalue, job_no as colname from job_no where job_no like '%" + tasknamejob + "%' and status = 'Active'";

            DataSet ds = zdbUtil.ExecSql_DataSet(sql, strConn_PS);
            return ds.Tables[0];
        }
        public DataTable getActivity(string xproject, string xTaskCode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"
                    
                   select t.task_id, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                    t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date
                ,
                CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                    from admuser.taskactv ta 
                    inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                    and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                    where ta.task_id = t.task_id and ROWNUM = 1) 
                    WHEN '-' THEN  (select ac_major.actv_code_name 
                    from admuser.taskactv ta 
                    inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                    and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                    where ta.task_id = t.task_id and ROWNUM = 1 )
                    WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                    from admuser.taskactv ta 
                    inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                    and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                    where ta.task_id = t.task_id and ROWNUM = 1 )
                    ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                    from admuser.taskactv ta 
                    inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                    and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                    where ta.task_id = t.task_id  and ROWNUM = 1) 
                END AS wbs_name
                , (SELECT ac.short_name 
                        FROM admuser.actvcode ac
                        left join admuser.taskactv ta on (ac.actv_code_id = ta.actv_code_id) and ac.actv_code_id in 
                        (select actv_code_id from admuser.actvcodex where codetypename = 'Bid Key Date')
                        where ta.task_id = t.task_id and ac.short_name != 'Yes') as Bid_Key_Date 

                from admuser.project p 
                left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                where p.proj_short_name like '%" + xproject + @"%' 
                    and t.task_name not like 'WRK%' 
                    and REGEXP_COUNT(proj_short_name, '-') = 3 
                    and p.proj_short_name like '%" + xTaskCode + @"%' 
                    and (t.Task_code like concat( p.proj_short_name , '%'))
                    and p.proj_short_name not like '%!_%' escape '!'
                    and p.proj_short_name not like '%(%'

                    ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetProjectnameDropdown()
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');
                string[] prefixJob = filterJob.Split(',');

                foreach (string bid in prefixBid)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(proj_short_name like '" + bid + "%')";
                }

                foreach (string job in prefixJob)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(proj_short_name like '" + job + "%')";
                }

                string sql = @"
                            select distinct substr(proj_short_name, 3, 4) as proj_name0, substr(proj_short_name,3,4) as proj_name
                            from admuser.project
                            where (" + strFilter + @") and REGEXP_COUNT(proj_short_name, '-') = 3
                            order by substr(proj_short_name, 3, 4)";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        #region getActivity
        public DataTable getActivityBid(string xproject)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');

                foreach (string bid in prefixBid)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + bid + "%')";
                }

                string sql = @"
                                select task_id, wbs_name, proj_short_name,  status_code, Task_code, task_name, 
                                       target_start_date, target_end_date,  act_start_date, act_end_date, reend_date,
                                       Org_Name, rsrc_id,
                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%'  )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @") 
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget'
                                order by proj_short_name
                    ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getActivityBid(string xproject, string statusCode)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');

                foreach (string bid in prefixBid)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + bid + "%')";
                }

                string sql = @"
                                select task_id, wbs_name, proj_short_name,  status_code, Task_code, task_name, 
                                       target_start_date, target_end_date,  act_start_date, act_end_date, reend_date,
                                       Org_Name, rsrc_id,
                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%'  )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @")
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget' and status_code = '" + statusCode + @"'
                                order by task_name, proj_short_name
                    
                    ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getActivityJob(string xproject)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixJob = filterJob.Split(',');

                foreach (string job in prefixJob)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + job + "%')";
                }

                string sql = @"
                                select task_id, wbs_name, proj_short_name,  status_code, Task_code, task_name, 
                                       target_start_date, target_end_date,  act_start_date, act_end_date, reend_date,
                                       org_Name, rsrc_id,
                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%' )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @")
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget'
                                order by proj_short_name
                    
                    ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetAssignedTask(string origtypeid)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"
                                select task_id, udf_text
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id, u.udf_text 
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            inner join admuser.udfvalue u on (t.task_id = u.fk_id)
                                            where p.proj_short_name not like '%!_%' escape '!'
                                                and ( t.task_name not like '%WRK%')
                                                and REGEXP_COUNT(proj_short_name, '-') = 3             
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and u.udf_type_id = " + origtypeid + @"
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget'
                                order by task_name, proj_short_name
                    ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        #endregion 
        #region UDFStaff
        public static void ActionUDFstaff(int taskid, string textinput)
        {
            try
            {
                if (textinput != "")
                {
                    int origid = Getoriginatortypeid();

                    var ores = Readoriginator(taskid, origid);

                    if (ores.p6staff == null)
                    {
                        Createoriginator(taskid, origid, textinput);
                    }
                    else
                    {
                        Updateoriginator(taskid, origid, textinput);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static int Getoriginatortypeid()
        {
            int resid = 0;
            try
            {
                string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();

                oraDBUtil zoraDBUtil = new oraDBUtil();

                string sql = @"SELECT UDF_TYPE_ID FROM admuser.udftype WHERE udf_type_label = 'Staff' and  disp_data_flag = 'Y' and Table_name = 'TASK'";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    resid = Int32.Parse(dr["UDF_TYPE_ID"].ToString());
                }
                else
                {
                    resid = 0;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return resid;
        }
        public static activitymodel Readoriginator(int taskid, int originatortypeid)
        {
            activitymodel am = new activitymodel();
            try
            {
                UDFValueService apb = new UDFValueService();
                apb.CookieContainer = cookieContainer;

                var ru = new ReadUDFValues();
                var cnt = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                var field = new UDFValueFieldType[cnt];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;
                ru.Filter = "ForeignObjectId='" + taskid + "' and UDFTypeObjectId = '" + originatortypeid + "' ";
                var res = apb.ReadUDFValues(ru);


                if (res != null && res.Length > 0)
                {
                    am.taskid = res[0].ForeignObjectId;
                    am.p6staff = res[0].Text;
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return am;
        }
        public static void Createoriginator(int taskid, int originatortypeid, string Originator)
        {
            try
            {
                UDFValueService uvs = new UDFValueService();
                uvs.CookieContainer = cookieContainer;


                ReadUDFValues ru = new ReadUDFValues();

                var count = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                UDFValueFieldType[] field = new UDFValueFieldType[count];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;

                ru.Filter = "ForeignObjectId='" + taskid + "' ";
                var res = uvs.ReadUDFValues(ru);

                foreach (var item in res)
                {
                    UDFValue[] fieldUpdate = new UDFValue[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].UDFTypeObjectId = originatortypeid;
                    fieldUpdate[0].ForeignObjectId = taskid;
                    fieldUpdate[0].Text = Originator;

                    var resUpdate = uvs.CreateUDFValues(fieldUpdate);

                    break;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void Updateoriginator(int taskid, int originatortypeid, string Originator)
        {
            try
            {
                UDFValueService uvs = new UDFValueService();
                uvs.CookieContainer = cookieContainer;

                ReadUDFValues ru = new ReadUDFValues();

                var count = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                UDFValueFieldType[] field = new UDFValueFieldType[count];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;
                ru.Filter = "ForeignObjectId='" + taskid + "' and UDFTypeObjectId = '" + originatortypeid + "' ";
                var res = uvs.ReadUDFValues(ru);

                foreach (var item in res)
                {
                    UDFValue[] fieldUpdate = new UDFValue[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].Text = Originator;

                    var resUpdate = uvs.UpdateUDFValues(fieldUpdate);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #endregion
        #region JobLocal
        public static string getlocaljob(int projid)
        {
            string joblocalnum = "";
            try
            {
                string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();
                oraDBUtil zoraDBUtil = new oraDBUtil();
                string sql = "";
                sql = @"select ph.parentcodevalueshortname as job_local
                    from admuser.PROJECTCODEHIERARCHY ph
                    inner join admuser.PROJPCAT pc on (ph.CODETYPEOBJECTID = pc.proj_catg_type_id and ph.PARENTCODEVALUEOBJECTID = pc.proj_catg_id)
                    where ph.codetypename = '00_Job Local' and proj_id = " + projid + " ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);

                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];
                    joblocalnum = dr["job_local"].ToString();
                }
                else
                {
                    joblocalnum = "";
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return joblocalnum;
        }
        #endregion
        #region Staff Last Update
        public static int getudfstafflastupdateid()
        {
            int resid = 0;
            try
            {
                string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();

                oraDBUtil zoraDBUtil = new oraDBUtil();

                string sql = @"SELECT UDF_TYPE_ID FROM admuser.udftype WHERE udf_type_label = 'Staff_LastUpdate' and  disp_data_flag = 'Y' and Table_name = 'TASK'";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);


                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = ds.Tables[0].Rows[0];

                    resid = Int32.Parse(dr["UDF_TYPE_ID"].ToString());
                }
                else
                {
                    resid = 0;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return resid;

        }
        public static void ActionUDFStaffLastupdate(int taskid, string newdate)
        {
            try
            {
                int stafflastupdatetypeid = getudfstafflastupdateid();

                string readresult = Readstafflastupdate(taskid, stafflastupdatetypeid);

                if (readresult != "")
                {
                    Updatestafflastupdate(taskid, stafflastupdatetypeid, newdate);
                }
                else
                {
                    Createstafflastupdate(taskid, stafflastupdatetypeid, newdate);

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static string Readstafflastupdate(int taskid, int stafflastupdatetypeid)
        {
            string stafflastupdate = "";
            try
            {
                UDFValueService apb = new UDFValueService();
                apb.CookieContainer = cookieContainer;

                var ru = new ReadUDFValues();
                var cnt = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                var field = new UDFValueFieldType[cnt];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;
                ru.Filter = "ForeignObjectId= '" + taskid + "' and UDFTypeObjectId = '" + stafflastupdatetypeid + "' ";
                var res = apb.ReadUDFValues(ru);


                if (res != null && res.Length > 0)
                {

                    stafflastupdate = res[0].Text;
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return stafflastupdate;
        }
        public static void Createstafflastupdate(int taskid, int stafflastupdatetypeid, string newdate)
        {
            try
            {
                UDFValueService uvs = new UDFValueService();
                uvs.CookieContainer = cookieContainer;

                ReadUDFValues ru = new ReadUDFValues();

                var count = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                UDFValueFieldType[] field = new UDFValueFieldType[count];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;

                ru.Filter = "ForeignObjectId='" + taskid + "' ";
                var res = uvs.ReadUDFValues(ru);

                foreach (var item in res)
                {
                    UDFValue[] fieldUpdate = new UDFValue[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].UDFTypeObjectId = stafflastupdatetypeid;
                    fieldUpdate[0].ForeignObjectId = taskid;
                    fieldUpdate[0].Text = newdate;

                    var resUpdate = uvs.CreateUDFValues(fieldUpdate);

                    break;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void Updatestafflastupdate(int taskid, int stafflastupdatetypeid, string newdate)
        {
            try
            {
                UDFValueService uvs = new UDFValueService();
                uvs.CookieContainer = cookieContainer;

                ReadUDFValues ru = new ReadUDFValues();

                var count = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
                UDFValueFieldType[] field = new UDFValueFieldType[count];
                field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));

                ru.Field = field;
                ru.Filter = "ForeignObjectId='" + taskid + "' and UDFTypeObjectId = '" + stafflastupdatetypeid + "' ";
                var res = uvs.ReadUDFValues(ru);

                foreach (var item in res)
                {
                    UDFValue[] fieldUpdate = new UDFValue[1];
                    fieldUpdate[0] = item;

                    fieldUpdate[0].Text = newdate;

                    var resUpdate = uvs.UpdateUDFValues(fieldUpdate);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #endregion
        public DataTable getBidDetail(string xBidNo)
        {
            DataTable dtResult = new DataTable();
            try
            {
                xBidNo = xBidNo.Replace("'", "").Replace("Enq.", "").Trim();
                //string xBid = "B-" + xBidNo + "";

                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');

                foreach (string bid in prefixBid)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(w.wbs_name like '%" + bid + xBidNo + @"%' and p.PROJ_SHORT_NAME like '" + bid + "%')";
                }

                string sql = @"
                    select task_id, wbs_name, proj_short_name,  status_code, Task_code, task_name, 
                           target_start_date, target_end_date,  act_start_date, act_end_date, reend_date,
                           Org_Name, rsrc_id,
                           case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                    from 
                    (
                            select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                    t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                ,
                                CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                    from admuser.taskactv ta 
                                    inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                    and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                    where ta.task_id = t.task_id and ROWNUM = 1) 
                                    WHEN '-' THEN  (select ac_major.actv_code_name 
                                    from admuser.taskactv ta 
                                    inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                    and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                    where ta.task_id = t.task_id and ROWNUM = 1 )
                                    WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                    from admuser.taskactv ta 
                                    inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                    and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                    where ta.task_id = t.task_id and ROWNUM = 1 )
                                    ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                    from admuser.taskactv ta 
                                    inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                    and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                    where ta.task_id = t.task_id  and ROWNUM = 1) 
                                END AS Org_Name
                                , w.status_code project_status
                                from admuser.project p 
                                left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                where (" + strFilter + @")
                                    and ( t.task_name not like '%WRK%')
                                    and REGEXP_COUNT(proj_short_name, '-') = 3
                                    and p.proj_short_name not like '%!_%' escape '!'
                                    and w.wbs_name not like '%!_%' escape '!'
                                    and p.proj_short_name not like '%(%'
                                    and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                    and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                    and (w.status_code = 'WS_Open')
                                    and t.delete_session_id is null
                    ) 
                   where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget' 
                    order by task_name, proj_short_name
    ";
                var ds = zoraDBUtil._getDS(sql, strConn_P6);
                dtResult = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dtResult;
        }
        public string getSQLScriptForRawData(string xproject)
        {
            string sql = "";
            try
            {
                sql = @"
                        SELECT T2.proj_id ,T2.TaskClassify , T2.proj_short_name, T2.Task_code, T2.wbs_id
                        ,T2.wbs_short_name, T2.seq_num, T2.Task_ID, T2.task_name, T2.wbs_name
                        ,TO_CHAR(T2.target_start_date, 'dd/MM/yyyy') target_start_date
                        ,TO_CHAR(T2.Target_end_date, 'dd/MM/yyyy') target_end_date
                        ,TO_CHAR(T2.SystemDate, 'dd/MM/yyyy') SystemDate
                        ,TO_CHAR(T2.Act_start_date, 'dd/MM/yyyy') act_start_date
                        ,TO_CHAR(T2.Act_end_date, 'dd/MM/yyyy') act_end_date
                        ,TO_CHAR(T2.Reend_date,'dd/MM/YYYY') reend_date
                        , T2.status_code, T2.Total_Duration, T2.Flag_Count
                        ,T2.Flag_Delay, T2.Total_Delay
                        ,T2.Total_Plan, T2.Total_Actual
                        , case when (T2.status_code = 'TK_Complete')
                               then 1.0 
                               when (T2.status_code <> 'TK_Complete' and (sysdate >= T2.target_start_date))
                               then ((T2.Total_duration - T2.Total_Plan)/T2.Total_duration)
                               when (T2.status_code <> 'TK_Complete' and (sysdate < T2.target_start_date))
                               then (0)
                               End As Percent_Plan
                         , case when (T2.status_code = 'TK_Complete' and T2.act_end_date <= T2.target_end_date)
                               then 1.0 
                               when ((T2.status_code = 'TK_Active' and T2.act_start_date >= T2.target_start_date) and (T2.act_end_date is null))
                               then ((T2.Total_plan - T2.Total_actual)/T2.Total_duration)
                               when ((T2.status_code = 'TK_Active' and T2.act_start_date < T2.target_start_date) and (T2.act_end_date is null))
                               then ((T2.Total_actual - T2.Total_plan)/T2.Total_duration)
                               when (T2.status_code = 'TK_NotStart' and (sysdate > T2.target_start_date))
                               then (0)
                               End As Percent_Actual     
                        ,T2.Major_Org, T2.Minor_Org
                        ,  (select UDF_NUMBER from admuser.UDFVALUE 
                              where FK_ID = T2.Task_ID 
                              and UDF_TYPE_ID =   (select udf_type_id 
                              from admuser.UDFTYPE  
                              where UDF_Type_Label like 'WF to%')) WF_TO_RESOURCE
                         from(
                         SELECT  T.proj_id
                          ,case WHEN(REGEXP_count(T.proj_short_name, '-') = 3) THEN 'BASE' ELSE 'REFLECTION' END AS TaskClassify
                          , T.proj_short_name, T.Task_code, T.wbs_id
                                                , T.wbs_short_name, T.seq_num, T.Task_ID, T.task_name, T.wbs_name
                                                , T.target_start_date, T.Target_end_date, T.status_code
                          , (TRUNC(t.target_end_date) - TRUNC(t.target_start_date)) + 1 -
                          ((((TRUNC(t.target_end_date, 'D')) - (TRUNC(t.target_start_date, 'D'))) / 7) * 2) -
                          (CASE WHEN TO_CHAR(t.target_end_date, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                          (CASE WHEN TO_CHAR(t.target_start_date,'DY','nls_date_language=english')= 'SAT' THEN 1 ELSE 0 END) as Total_Duration
                          ,T.Act_start_date, T.Act_end_date, T.reend_date, T.SystemDate, T.Cal_Method, T.Flag_Count, T.Flag_Delay, T.Total_Delay
                          ,T.Total_Plan, T.Total_Actual
                          ,T.Major_Org, T.Minor_Org        
                                    from
                                       (
                                        select  p.proj_id, p.proj_short_name, t.status_code, t.Task_code, w.wbs_id,
                                                 w.wbs_short_name, w.seq_num, t.Task_ID, t.task_name, w.wbs_name,
                                                 t.target_start_date, t.target_end_date, t.act_start_date, t.act_end_date, t.reend_date
                                                 , sysdate as SystemDate
                                                 , case when(t.act_start_date is null) then 'Method0'
                                                        when(t.act_start_date < t.target_start_date) then 'Method1' else 'Method2' end as Cal_Method
                                                , case when(sysdate >= t.target_start_date) then 'Yes' else 'No' end as Flag_Count
                                                , case when((sysdate > t.target_start_date and sysdate <= t.target_end_date) and t.act_start_date is null)
                                                       then 'Start Delayed' 
                                                       when(sysdate > t.target_end_date and t.status_code = 'TK_NotStart')
                                                       then 'Totally Delayed'
                                                       when(sysdate > t.target_end_date and t.status_code = 'TK_Start')
                                                       then 'Delayed'
                                                       when(t.act_end_date > t.target_end_date and t.status_code = 'TK_Complete')
                                                       then 'Late Completed'
                                                       when(t.act_end_date <= t.target_end_date and t.status_code = 'TK_Complete')
                                                       then 'Completed'
                                                       when(t.act_start_date <= t.target_end_date and t.status_code = 'TK_Active')
                                                       then 'On Progress'
                                                       when(sysdate < t.target_start_date and t.status_code = 'TK_NotStart')
                                                       then 'Not Due'
                                                       --CONCAT( CONCAT('Remaining ', TO_CHAR(TRUNC(t.target_start_date) - TRUNC(sysdate))),' Days to Start')
                                                       else '-' end as Flag_Delay
                                                , case when(sysdate > t.target_start_date and t.act_start_date is null)
                                                then(

                                                    (TRUNC(sysdate) - TRUNC(t.target_start_date)) + 1 -
                                                    ((((TRUNC(sysdate, 'D')) - (TRUNC(t.target_start_date, 'D'))) / 7) * 2) -
                                                    (CASE WHEN TO_CHAR(sysdate, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                                                    (CASE WHEN TO_CHAR(t.target_start_date, 'DY', 'nls_date_language=english') = 'SAT' THEN 1 ELSE 0 END)
                            
                                                ) END as Total_Delay
                                              , case when(sysdate >= t.target_start_date and t.status_code <> 'TK_Complete' )
                                                then(

                                                    (TRUNC(sysdate) - TRUNC(t.target_start_date)) + 1 -
                                                    ((((TRUNC(sysdate, 'D')) - (TRUNC(t.target_start_date, 'D'))) / 7) * 2) -
                                                    (CASE WHEN TO_CHAR(sysdate, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                                                    (CASE WHEN TO_CHAR(t.target_start_date, 'DY', 'nls_date_language=english') = 'SAT' THEN 1 ELSE 0 END)
                            
                                                ) when(sysdate >= t.target_start_date and t.status_code = 'TK_Complete' )
                                                then(

                                                    (TRUNC(t.target_end_date) - TRUNC(t.target_start_date)) + 1 -
                                                    ((((TRUNC(t.target_end_date, 'D')) - (TRUNC(t.target_start_date, 'D'))) / 7) * 2) -
                                                    (CASE WHEN TO_CHAR(t.target_end_date, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                                                    (CASE WHEN TO_CHAR(t.target_start_date, 'DY', 'nls_date_language=english') = 'SAT' THEN 1 ELSE 0 END)
                            
                                                )  when(sysdate < t.target_start_date and t.status_code = 'TK_Active' )
                                                then(

                                                    (TRUNC(t.target_start_date) - TRUNC(sysdate)) + 1 -
                                                    ((((TRUNC(t.target_start_date, 'D')) - (TRUNC(sysdate, 'D'))) / 7) * 2) -
                                                    (CASE WHEN TO_CHAR(t.target_start_date, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                                                    (CASE WHEN TO_CHAR(sysdate, 'DY', 'nls_date_language=english') = 'SAT' THEN 1 ELSE 0 END)
                            
                                                ) 
                                                else 0
                                                END as Total_Plan
                                              , case when((sysdate >= t.target_start_date and t.act_start_date is not null) and t.act_end_date is null)
                                                then(

                                                    (TRUNC(sysdate) - TRUNC(t.act_start_date)) + 1 -
                                                    ((((TRUNC(sysdate, 'D')) - (TRUNC(t.act_start_date, 'D'))) / 7) * 2) -
                                                    (CASE WHEN TO_CHAR(sysdate, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                                                    (CASE WHEN TO_CHAR(t.act_start_date, 'DY', 'nls_date_language=english') = 'SAT' THEN 1 ELSE 0 END)
                            
                                                ) when((sysdate >= t.target_start_date and t.act_start_date is not null) and t.act_end_date is not null)
                                                then(

                                                    (TRUNC(t.act_end_date) - TRUNC(t.act_start_date)) + 1 -
                                                    ((((TRUNC(t.act_end_date, 'D')) - (TRUNC(t.act_start_date, 'D'))) / 7) * 2) -
                                                    (CASE WHEN TO_CHAR(t.act_end_date, 'DY', 'nls_date_language=english') = 'SUN' THEN 1 ELSE 0 END) -
                                                    (CASE WHEN TO_CHAR(t.act_start_date, 'DY', 'nls_date_language=english') = 'SAT' THEN 1 ELSE 0 END)
                            
                                                ) when (sysdate >= t.target_start_date and t.act_start_date is  null )
                                                  then 0 
                                                END as Total_Actual
                                              ,(select ac_major.actv_code_name 
                                                    from admuser.taskactv ta 
                                                    inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                                                    and ac_major.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                    where ta.task_id = T.task_id and ROWNUM = 1 ) AS MAJOR_Org
                                               , (select ac_minor.actv_code_name 
                                                  from admuser.taskactv ta 
                                                  inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                                                  and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                  where ta.task_id = T.task_id  and ROWNUM = 1 ) AS MINOR_Org

                                                from admuser.project p
                                                 left
                                                join admuser.projwbs w on (p.proj_id = w.proj_id)

                                           left
                                                join admuser.Task t on (p.Proj_ID = t.Proj_ID and t.wbs_id = w.wbs_id)
                                                 where
                                                  t.task_code like   '%" + xproject + @"%'
                                       )T
                                       order by T.proj_short_name , T.seq_num
                               )T2
                               WHERE T2.TASKCLASSIFY = 'BASE' 
                               and  ( T2.proj_short_name not like '%R_%' 
                               and T2.proj_short_name not like '%Test%' 
                               and T2.proj_short_name not like '%(%') 
                               and T2.WBS_NAME <> 'PLS'
            ";
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return sql;
        }
        public static string GetDrawing(string taskid)
        {
            string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();
            oraDBUtil zoraDBUtil = new oraDBUtil();

            string drawing = "";
            try
            {
                string sql = @"select ac.short_name , ta.* from admuser.taskactv ta
                            inner join admuser.actvcode ac on (ac.actv_code_id = ta.actv_code_id)
                            left join admuser.actvtype at on (at.actv_code_type_id = ac.actv_code_type_id)
                            where at.actv_code_type = '001_จุดส่งแบบ' and task_id = '" + taskid + "'";
                DataSet dataSet = zoraDBUtil._getDS(sql, strConn_P6);

                if (dataSet.Tables[0].Rows.Count > 0)
                {
                    DataRow dataRow = dataSet.Tables[0].Rows[0];
                    drawing = dataRow["short_name"].ToString();
                }
                else
                {
                    drawing = "";
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return drawing;

        }
        public static string GetDTSType(int projid)
        {
            string dts_type = "";
            try
            {
                string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();
                oraDBUtil zoraDBUtil = new oraDBUtil();
                string sql = "";
                sql = @"select ph.parentcodevalueshortname as DTS_Type
                    from admuser.PROJECTCODEHIERARCHY ph
                    inner join admuser.PROJPCAT pc on (ph.CODETYPEOBJECTID = pc.proj_catg_type_id and ph.PARENTCODEVALUEOBJECTID = pc.proj_catg_id)
                    where ph.codetypename = '00_DTS Type' and proj_id = " + projid + "";
                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow dataRow = ds.Tables[0].Rows[0];
                    dts_type = dataRow["DTS_Type"].ToString();
                }
                else
                {
                    dts_type = "";
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dts_type;
        }
        #region BidJobFlagDelay
        public DataTable getTotalBidJobFlagDelay(string xproject)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = @" select R2.BJ,  R2.Flag_Delay, Count (R2.Task_code)  Total_Activities
                    from
                    (
                     select  Distinct R.PROJ_SHORT_NAME BJ, R.Minor_Org,R.Task_code, R.Status_code, R.Flag_Delay
                     from (" + getSQLScriptForRawData(xproject) + @"
                        )R Where ( R.Minor_org <> 'Timeline' and R.Minor_ORG <> '-')
                               )R2
                               Group by R2.BJ, R2.Flag_Delay
                               Order by R2.BJ
                    ";
                dt = zoraDBUtil._getDS(sql, strConn_P6).Tables[0];

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        #endregion

        public static int GetSubLineNameTypeId()
        {
            string strConn_P6 = ConfigurationManager.AppSettings["p6db"].ToString();

            oraDBUtil zoraDBUtil = new oraDBUtil();
            //chenge like to =
            string sql = @"SELECT UDF_TYPE_ID FROM admuser.udftype WHERE udf_type_label = 'SubLine Name'";

            DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);

            int resid;

            if (ds.Tables[0].Rows.Count > 0)
            {
                DataRow dr = ds.Tables[0].Rows[0];
                resid = Int32.Parse(dr["UDF_TYPE_ID"].ToString());
            }
            else
            {
                resid = 0;
            }
            return resid;
        }
        public static activitymodel ReadSubLineName(int taskid)
        {
            int typeid = GetSubLineNameTypeId();
            UDFValueService apb = new UDFValueService();
            apb.CookieContainer = cookieContainer;
            var ru = new ReadUDFValues();
            var cnt = Enum.GetValues(typeof(UDFValueFieldType)).Length + 1;
            var field = new UDFValueFieldType[cnt];
            field = (UDFValueFieldType[])Enum.GetValues(typeof(UDFValueFieldType));
            ru.Field = field;
            ru.Filter = "ForeignObjectId='" + taskid + "' and UDFTypeObjectId = '" + typeid + "' ";
            var res = apb.ReadUDFValues(ru);
            activitymodel am = new activitymodel();
            if (res != null && res.Length > 0)
            {
                am.taskid = res[0].ForeignObjectId;
                am.comment = res[0].Text;
            }
            return am;
        }
        public DataTable getActivityBidDis(string xproject, string status)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');

                foreach (string bid in prefixBid)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + bid + "%')";
                }

                string sql = @"
                                select distinct wbs_name, proj_short_name,Org_Name,
                         
                                
                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%'  )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @") 
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget'";

                if (status != "") sql += status;

                sql += " order by proj_short_name";
                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getActivityJobDis(string xproject, string status)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixJob = filterJob.Split(',');

                foreach (string job in prefixJob)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + job + "%')";
                }

                string sql = @"
                                select distinct wbs_name, proj_short_name,Org_Name,

                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%' )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @")
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget' ";

                if (status != "") sql += status;

                sql += " order by proj_short_name";
                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }


        #region " Get TaskID P6 By Activity ECM "

        public DataTable getTaskBid_ByActivityECM(string xproject, string strActivityECM)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixBid = filterBid.Split(',');

                foreach (string bid in prefixBid)
                { 
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + bid + "%')";
                }
                 
                string sql = @"
                                select task_id, wbs_name, proj_short_name,  status_code, Task_code, task_name, 
                                       target_start_date, target_end_date,  act_start_date, act_end_date, reend_date,
                                       Org_Name, rsrc_id,
                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                       ,ecmActivityID
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name 
                                            , w.status_code project_status
                                            ,(select NVL(ac_ecm.short_name, '-') as ecmActivityID
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_ecm on (ac_ecm.actv_code_id = ta.actv_code_id 
                                                and ac_ecm.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'ecmActivityID')) 
                                                where ta.task_id = t.task_id --and ROWNUM = 1
                                                ) as ecmActivityID
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%'  )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @")
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget' and ecmActivityID = '" + strActivityECM + @"'
                                order by task_name, proj_short_name
                    
                    ";
                LogHelper.Write(sql);
                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getTaskJob_ByActivityECM(string xproject, string strActivityECM)
        {
            DataTable dt = new DataTable();
            try
            {
                string strFilter = "";
                string[] prefixJob = filterJob.Split(',');

                foreach (string job in prefixJob)
                {
                    strFilter += (strFilter == "" ? "" : " or ") + "(t.task_code like '" + job + "%')";
                }

                string sql = @"
                                select task_id, wbs_name, proj_short_name,  status_code, Task_code, task_name, 
                                       target_start_date, target_end_date,  act_start_date, act_end_date, reend_date,
                                       org_Name, rsrc_id,
                                       case when project_status = 'WS_Open' then 'Active' when project_status = 'WS_Close' then 'InActive' else '' end as Project_Status
                                       ,ecmActivityID
                                from 
                                (
                                        select t.task_id, w.wbs_name, p.proj_short_name,  t.status_code, t.Task_code, t.task_name, 
                                                t.target_start_date, t.target_end_date,  t.act_start_date, t.act_end_date, t.reend_date, t.rsrc_id
                                            ,
                                            CASE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1) 
                                                WHEN '-' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                WHEN 'Timeline' THEN  (select ac_major.actv_code_name 
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_major on (ac_major.actv_code_id = ta.actv_code_id 
                                                and ac_major.actv_code_id in (select distinct actv_code_id from admuser.actvcodex where codetypename = 'Major Organize (Act)')) 
                                                where ta.task_id = t.task_id and ROWNUM = 1 )
                                                ELSE (select NVL(ac_minor.actv_code_name, '-') as actv_code_name
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_minor on (ac_minor.actv_code_id = ta.actv_code_id 
                                                and ac_minor.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'Minor Organize (Act)')) 
                                                where ta.task_id = t.task_id  and ROWNUM = 1) 
                                            END AS Org_Name
                                            , w.status_code project_status
                                            ,(select NVL(ac_ecm.short_name, '-') as ecmActivityID
                                                from admuser.taskactv ta 
                                                inner join admuser.actvcode ac_ecm on (ac_ecm.actv_code_id = ta.actv_code_id 
                                                and ac_ecm.actv_code_id in (select actv_code_id from admuser.actvcodex where codetypename = 'ecmActivityID')) 
                                                where ta.task_id = t.task_id --and ROWNUM = 1
                                                ) as ecmActivityID
                                            from admuser.project p 
                                            left join admuser.task t on (p.Proj_ID = t.Proj_ID)
                                            left join admuser.projwbs w on (w.proj_id = p.proj_id)
                                            where p.proj_short_name like '%" + xproject + @"%' 
                                                and ( t.task_name not like '%WRK%' )
                                                and REGEXP_COUNT(proj_short_name, '-') = 3
                                                and (" + strFilter + @")
                                                and p.proj_short_name not like '%!_%' escape '!'
                                                and w.wbs_name not like '%!_%' escape '!'
                                                and p.proj_short_name not like '%(%'
                                                and (w.wbs_short_name like concat( p.proj_short_name , '%'))
                                                and ( w.wbs_name not like '%(WF%' and w.wbs_name not like '%( WF%')
                                                and (w.status_code = 'WS_Open')
                                                and t.delete_session_id is null
                                ) 
                               where rsrc_id is not null and org_name != 'PLS' and org_name != 'Budget' and ecmActivityID = '" + strActivityECM + @"'
                                order by proj_short_name
                    
                    ";

                DataSet ds = zoraDBUtil._getDS(sql, strConn_P6);
                dt = ds.Tables[0];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        #endregion

    }
    public class p6Data
    {
        // DATA
        public string PROJ_ID { get; set; }
        public string PROJ_SHORT_NAME { get; set; }
        public string WBS_ID { get; set; }
        public string WBS_SHORT_NAME { get; set; }
        public string WBS_NAME { get; set; }
        public int SEQ_ID { get; set; }
        public string TASK_ID { get; set; }
        public string TASK_NAME { get; set; }
        public string TASK_CODE { get; set; }
        public string ATCV_CODE_ID { get; set; }
        public string ACTV_CODETYPENAME { get; set; }
        public string ACTV_SHORT_NAME { get; set; }
        public string ACTV_CODE_NAME { get; set; }

        // DATE
        public string TASK_STATUS_CODE { get; set; }
        public string TASK_ACT_START_DATE { get; set; }
        public string TASK_ACT_END_DATE { get; set; }
        public string TASK_TARGET_START_DATE { get; set; }
        public string TASK_TARGET_END_DATE { get; set; }
        public string TASK_REM_START_DATE { get; set; }//
        public string TASK_REM_END_DATE { get; set; }//
        public string TASK_RESTART_DATE { get; set; }
        public string TASK_REEND_DATE { get; set; }



        // TK TARGET START, TARGET END DATE
        // TK STATUS
        // TK ACTUAL START, END
        // ACTIVITY TYPE
        // ACTIVITY ID, NAME, VALUE


    }
    public class activitymodel
    {
        public int taskid { get; set; }
        public int projectid { get; set; }
        public string projectshortname { get; set; }
        public string projectfullname { get; set; }
        public string taskname { get; set; }
        public string taskcode { get; set; }
        public string wbsname { get; set; }
        public string status { get; set; }
        public string comment { get; set; }
        public string p6staff { get; set; }
        public string joblocal { get; set; }
        public double? pctplan { get; set; }
        public double? pctcomplete { get; set; }
        public double? pctactualresource { get; set; }
        public double? planedcost { get; set; }
        public double? actualcost { get; set; }
        public double? remaincost { get; set; }
        public DateTime? planstart { get; set; }
        public DateTime? planfinish { get; set; }
        public DateTime? actualstart { get; set; }
        public DateTime? actualfinish { get; set; }
        public DateTime? reenddate { get; set; }
        public DateTime? datadate { get; set; }
        public DateTime? lastupdateedate { get; set; }
    }
}