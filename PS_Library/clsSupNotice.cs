﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
   public class clsSupNotice
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        private string db_common = ConfigurationManager.AppSettings["db_common"].ToString();
        static oraDBUtil oraDBUtil = new oraDBUtil();
        public clsSupNotice() { }
        public DataTable getDDL()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct bid_no,bid_revision from ps_t_bid_monitoring order by bid_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDL_Master()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct bid_no from ps_t_supplementalnotice order by bid_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getData(string bidno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select bm.bid_no,bm.bid_revision,(select dbo.udf_StripHTML(bid_desc) from bid_no bo where bo.bid_no=bm.bid_no and bid_revision =(select max(bid_revision) from bid_no where bid_no = '" + bidno + @"')) as bid_desc
                        from ps_t_bid_monitoring bm 
                        where bid_no = '" + bidno + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDeptinCb(string bidno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct depart_position_name  from ps_t_mytask_equip where bid_no ='" + bidno + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDocTypeSN()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct doctype_code,doctype_name from ps_m_doctype order by doctype_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataSN_Master(string bidno,string sn,string sub)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select  distinct supplementalnotice_code,bid_no,subject from ps_t_supplementalnotice ";
                if (bidno != "" || sn !="" || sub!="")
                {
                    sql += " where bid_no like '%"+ bidno + "%' and  supplementalnotice_code like '%"+ sn + "%' and subject like '%"+ sub + "%'";
 
                }
                sql += " order by supplementalnotice_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataTop(string sncode,string bidno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select *,(select dbo.udf_StripHTML(bid_desc) from bid_no bo where bo.bid_no=ts.bid_no and bid_revision =(select max(bid_revision) from bid_no where bid_no = '"+ bidno +@"')) as bid_desc
						 from ps_t_supplementalnotice ts  where supplementalnotice_code='" + sncode + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataSN(string sncode)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select *,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code=ps_t_supnotice_att.doctype_code) as doctype_name
,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk 
from ps_t_supnotice_att where supplementalnotice_code='" + sncode + "'";
                sql += " order by doctype_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getPartSN(string sncode, string sect, string jobNo, string schNo, string bidNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from (select distinct part_name,part_description,part_remark,important,supplementalnotice_code from ps_t_supplementalnotice_part
where  job_no = '" + jobNo + "' and bid_no = '" + bidNo + "' and supplementalnotice_code ='"+ sncode + "') tb  order by  ASCII(part_name),len(part_name), part_name";
             
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getAssignSN(string sncode, string dept)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = sql = "select * from ps_t_supplementalnotice_assign where supplementalnotice_code='" + sncode + "' and depart_name='" + dept + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void UpdateBidderDateSN(string sncode, string date)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = "select * from ps_t_supplementalnotice where supplementalnotice_code='"+ sncode + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = "update ps_t_supplementalnotice set send_bidder_date='"+ date + "',status='COMPLETED' where supplementalnotice_code='" + sncode + "'";
                }
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void AssigntoStaffSN(string sncode,string login,string assign,string dept)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = "select * from ps_t_supplementalnotice_assign where supplementalnotice_code='"+ sncode + "' and depart_name='"+ dept + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = "update ps_t_supplementalnotice_assign set assignee_id='"+ assign + "',updated_by='"+ login + "',updated_datetime=getdate() where supplementalnotice_code='" + sncode + "' and depart_name='"+ dept + "'";
                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice_assign
           ([supplementalnotice_code]
      ,[assignee_id]
      ,[depart_name]
      ,[assignee_status]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime])
     VALUES
           ('" + sncode + @"'
           ,'" + assign + @"'
           ,'" + dept + @"'
           ,'ASSIGNED'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE())";
                }

                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public DataTable getDrawSN(string sncode, string rev, string bidNo, string chkdraw)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select [supplementalnotice_code]
      ,[supplementalnotice_version]
      ,[activity_id]
      ,[job_no]
      ,[job_revision]
      ,[bid_no]
      ,[bid_revision]
      ,[schedule_no]
      ,[schedule_name]
      ,[depart_position_name]
      ,[section_position_name]
      ,[doctype_code]
      ,[doc_nodeid] as doc_no
      ,[doc_revision_no] as doc_revision
      ,[doc_name]
      ,[doc_sheetno]
      ,[doc_desc]
      ,[is_verify]
      ,[verify_by]
      ,[verify_position]
      ,[verify_datetime]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime]
,(select doctype_name from ps_m_doctype md where md.doctype_code = sd.doctype_code) as doctype_name
,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>  ViewDoc </a>' as link_doc 
from  ps_t_supplementalnotice_doc sd where supplementalnotice_code='" + sncode + "' and bid_no='"+ bidNo + "' and bid_revision='"+ rev + "'";
                if (chkdraw != "") sql += " and doctype_code in ('DD','TD')";
                else sql += " and doctype_code not in ('DD','TD')";

                sql += " order by doctype_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void DeleteDrawingSN(AddDoc objAddDoc)
        {
            try
            {
                string strQuery = "delete from ps_t_supplementalnotice_doc where bid_no = '" + objAddDoc.bid_no + "' and bid_revision = '" + objAddDoc.bid_revision + @"'
						    and depart_position_name = '" + objAddDoc.depart_position_name + "' and section_position_name = '" + objAddDoc.section_position_name + "' and doctype_code = '" + objAddDoc.doctype_code + "' and doc_name = '" + objAddDoc.doc_name + "' and doc_nodeid='" + objAddDoc.doc_nodeid + "'";

                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void InsertAddDocSN(AddDocbid objAddDoc,string sncode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_t_supplementalnotice_doc 
                            where bid_no = '" + objAddDoc.bid_no + "' and bid_revision = '" + objAddDoc.bid_revision + @"'
                            and depart_position_name = '" + objAddDoc.depart_position_name + "' and section_position_name = '" + objAddDoc.section_position_name + "' and doctype_code = '" + objAddDoc.doctype_code + "' and doc_name = '" + objAddDoc.doc_name + "' and doc_nodeid ='" + objAddDoc.doc_nodeid + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string strQuery = @"update ps_t_supplementalnotice_doc set doc_nodeid ='" + objAddDoc.doc_nodeid + "', bid_no ='" + objAddDoc.bid_no + @"' , doc_desc ='" + objAddDoc.doc_desc + "' , created_datetime ='" + objAddDoc.created_datetime + "' , updated_datetime ='" + objAddDoc.updated_datetime + "' ,updated_by ='" + objAddDoc.updated_by + "' , schedule_name ='" + objAddDoc.schedule_name + "'  where job_no = '" + objAddDoc.job_no + "' and job_revision = (select max(revision) from job_no where job_no = '" + objAddDoc.job_no + @"')
						    and depart_position_name = '" + objAddDoc.depart_position_name + "' and section_position_name = '" + objAddDoc.section_position_name + "' and doctype_code = '" + objAddDoc.doctype_code + "' and doc_name = '" + objAddDoc.doc_name + "' ";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
                else
                {
                    string strQuery = @"INSERT INTO ps_t_supplementalnotice_doc
                              ([supplementalnotice_code]
                              ,[supplementalnotice_version]
                              ,[activity_id]
                              ,[job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_revision_no]
                              ,[doc_name]
                              ,[doc_sheetno]
                              ,[doc_desc]
                              ,[is_verify]
                              ,[verify_by]
                              ,[verify_position]
                              ,[verify_datetime]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                              VALUES
               ('" + sncode + "'"
         + " , '0'"
         + " , ''"
         + " , '" + objAddDoc.job_revision + "'"
         + " , '" + objAddDoc.job_revision + "'"
         + " , '" + objAddDoc.bid_no + "'"
         + " , '" + objAddDoc.bid_revision + "'"
         + " , '" + objAddDoc.schedule_no + "'"
         + " , '" + objAddDoc.schedule_name + "'"
         + " , '" + objAddDoc.depart_position_name + "'"
         + " , '" + objAddDoc.section_position_name + "'"
         + " , '" + objAddDoc.doctype_code + "'"
         + " , '" + objAddDoc.doc_nodeid + "'"
         + " , '" + objAddDoc.doc_revision_no + "'"
         + " , '" + objAddDoc.doc_name + "'"
         + " , '" + objAddDoc.doc_sheetno + "'"
         + " , '" + objAddDoc.doc_desc + "'"
         + " , '0'"
         + " , ''"
         + " , ''"
         + " , '"+ DBNull.Value+"'"
         + " , '" + objAddDoc.created_by + "'"
         + " , '" + objAddDoc.created_datetime + "'"
         + " , '" + objAddDoc.updated_by + "'"
         + " , '" + objAddDoc.updated_datetime + "')";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void getDrawToInsertSN(string sncode, string rev, string bidNo, string login, string chkdraw)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = @"select * from ps_t_mytask_doc 
                    where bid_no ='"+ bidNo + "' and bid_revision='"+ rev + "'";
                if (chkdraw != "") sql += " and doctype_code in ('DD','TD')";
                else sql += " and doctype_code not in ('DD','TD')";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sql = @"select * from ps_t_supplementalnotice_doc
where  job_no = '" + dr["job_no"].ToString() + "' and bid_no = '" + dr["bid_no"].ToString() + "' and doc_nodeid ='" + dr["doc_nodeid"].ToString() + "' and doctype_code ='"+ dr["doctype_code"].ToString() + "'  and supplementalnotice_code='" + sncode + "'";

                        dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                        if (dt.Rows.Count == 0)
                        {
                            sql = @"INSERT INTO ps_t_supplementalnotice_doc
           ([supplementalnotice_code]
      ,[supplementalnotice_version]
      ,[activity_id]
      ,[job_no]
      ,[job_revision]
      ,[bid_no]
      ,[bid_revision]
      ,[schedule_no]
      ,[schedule_name]
      ,[depart_position_name]
      ,[section_position_name]
      ,[doctype_code]
      ,[doc_nodeid]
      ,[doc_revision_no]
      ,[doc_name]
      ,[doc_sheetno]
      ,[doc_desc]
      ,[is_verify]
      ,[verify_by]
      ,[verify_position]
      ,[verify_datetime]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime])
     VALUES
           ('" + sncode + @"'
           ,'0'
           ,'" + dr["activity_id"].ToString() + @"'
           ,'" + dr["job_no"].ToString() + @"'
           ,'" + dr["job_revision"].ToString() + @"'
           ,'" + dr["bid_no"].ToString() + @"'
           ,'" + dr["bid_revision"].ToString() + @"'
           ,'" + dr["schedule_no"].ToString() + @"'
           ,'" + dr["schedule_name"].ToString() + @"'
           ,'" + dr["depart_position_name"].ToString() + @"'
           ,'" + dr["section_position_name"].ToString() + @"'
           ,'" + dr["doctype_code"].ToString() + @"'
           ,'" + dr["doc_nodeid"].ToString() + @"'
           ,'" + dr["doc_revision_no"].ToString() + @"'
           ,'" + dr["doc_name"].ToString() + @"'
           ,'" + dr["doc_sheetno"].ToString() + @"'
           ,'" + dr["doc_desc"].ToString() + @"'
           ,'" + dr["is_verify"].ToString() + @"'
           ,'" + dr["verify_by"].ToString() + @"'
           ,'" + dr["verify_position"].ToString() + @"'
           ,'" + dr["verify_datetime"].ToString() + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE())";
                            db.ExecNonQuery(sql, zetl4eisdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public void getChkToInsertPartSN(string sncode, string jobNo, string bidNo,string login)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                    sql = @"select * from ps_t_mytask_part
where  job_no = '" + jobNo + "' and bid_no = '" + bidNo + "'  order by  ASCII(part_name),len(part_name), part_name";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sql = @"select * from ps_t_supplementalnotice_part
where  job_no = '" + dr["job_no"].ToString() + "' and bid_no = '" + dr["bid_no"].ToString() + "' and part_name ='"+ dr["part_name"].ToString() + "'  and supplementalnotice_code='" + sncode + "' order by  ASCII(part_name),len(part_name), part_name";

                        dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                        if (dt.Rows.Count == 0)
                        {
                            sql = @"INSERT INTO ps_t_supplementalnotice_part
           ([supplementalnotice_code]
      ,[supplementalnotice_version]
      ,[activity_id]
      ,[step]
      ,[job_no]
      ,[job_revision]
      ,[bid_no]
      ,[bid_revision]
      ,[schedule_no]
      ,[schedule_name]
      ,[depart_position_name]
      ,[section_position_name]
      ,[project_id]
      ,[part_name]
      ,[part_description]
      ,[important]
      ,[part_remark]
      ,[is_new_schedule]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime])
     VALUES
           ('" + sncode + @"'
           ,'0'
           ,'" + dr["activity_id"].ToString() + @"'
           ,'" + dr["step"].ToString() + @"'
           ,'" + dr["job_no"].ToString() + @"'
           ,'" + dr["job_revision"].ToString() + @"'
           ,'" + dr["bid_no"].ToString() + @"'
           ,'" + dr["bid_revision"].ToString() + @"'
           ,'" + dr["schedule_no"].ToString() + @"'
           ,'" + dr["schedule_name"].ToString() + @"'
           ,'" + dr["depart_position_name"].ToString() + @"'
           ,'" + dr["section_position_name"].ToString() + @"'
           ,'" + dr["project_id"].ToString() + @"'
           ,'" + dr["part_name"].ToString() + @"'
           ,'" + dr["part_description"].ToString() + @"'
           ,'" + dr["important"].ToString() + @"'
           ,'" + dr["part_remark"].ToString() + @"'
           ,'" + dr["is_new_schedule"].ToString() + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE())";
                            db.ExecNonQuery(sql, zetl4eisdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            
        }
        public void UpdateSNMyTaskEquip(addMytaskEquip objMytask)
        {
            int rowid = 0;
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid  from ps_t_mytask_equip where job_no = '" + objMytask.job_no + "' and job_revision = '" + objMytask.job_revision + "' and bid_no = '" + objMytask.bid_no + "' and bid_revision = '" + objMytask.bid_revision + @"'
and schedule_no = '" + objMytask.schedule_no + "' and schedule_name = '" + objMytask.schedule_name + "' and depart_position_name = '" + objMytask.depart_position_name + "' and section_position_name = '" + objMytask.section_position_name + @"'
and path_code = '" + objMytask.path_code + "' and equip_code='" + objMytask.equip_code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

                if (dt.Rows.Count > 0)
                {
                    rowid = Convert.ToInt32(dt.Rows[0]["rowid"]);
                    sql = @"UPDATE ps_t_mytask_equip
   SET 
      [equip_item_no] = '" + objMytask.equip_item_no + @"'
      ,[equip_add_desc] = '" + objMytask.equip_add_desc + @"'
      ,[equip_standard_drawing] = '" + objMytask.equip_standard_drawing + @"'
      ,[equip_qty] = '" + objMytask.equip_qty + @"'
      ,[equip_unit] = '" + objMytask.equip_unit + @"'
      ,[equip_bid_qty] = '" + (objMytask.equip_bid_qty == null ? 0 : objMytask.equip_bid_qty) + @"'
      ,[equip_incoterm] = '" + objMytask.equip_incoterm + @"'
      ,[factory_test] = '" + objMytask.factory_test + @"'
      ,[manual_test] = '" + objMytask.manual_test + @"'
      ,[routine_test] = '" + objMytask.routine_test + @"'
      ,[is_breakdown] = '" + objMytask.is_breakdown + @"'
      ,[currency] = '" + objMytask.currency + @"'
      ,[supply_equip_unit_price] = '" + objMytask.supply_equip_unit_price + @"'
      ,[supply_equip_amonut] = '" + objMytask.supply_equip_amonut + @"'
      ,[local_exwork_unit_price] = '" + objMytask.local_exwork_unit_price + @"'
      ,[local_exwork_amonut] = '" + objMytask.local_exwork_amonut + @"'
      ,[local_tran_unit_price] = '" + objMytask.local_exwork_unit_price + @"'
      ,[local_tran_amonut] = '" + objMytask.local_tran_amonut + @"'
      ,[updated_by] = '" + objMytask.updated_by + @"'
      ,[updated_datetime] = '" + objMytask.updated_datetime + @"'
 WHERE job_no = '" + objMytask.job_no + "' and job_revision = '" + objMytask.job_revision + "' and bid_no = '" + objMytask.bid_no + "' and bid_revision = '" + objMytask.bid_revision + @"'
and schedule_no = '" + objMytask.schedule_no + "' and schedule_name = '" + objMytask.schedule_name + "' and depart_position_name = '" + objMytask.depart_position_name + "' and section_position_name = '" + objMytask.section_position_name + @"'
and path_code = '" + objMytask.path_code + "' and equip_code='" + objMytask.equip_code + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                else
                {
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
          
        }
        public DataTable validateItmSN(string jobno, string bidno, string part, string itm,string sncode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid from ps_t_supplementalnotice_equip where supplementalnotice_code='" + sncode + "' and bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and equip_item_no='" + itm + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void delPartSN(string part, string jobNo, string schNo, string dept, string sect, string bidNo,string sncode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"delete from ps_t_supplementalnotice_part 
where supplementalnotice_code='"+ sncode + "' and job_no = '" + jobNo + "' and bid_no = '" + bidNo + "' and part_name='" + part + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
                sql = @"select rowid from ps_t_supplementalnotice_equip
where supplementalnotice_code='" + sncode + "' and  bid_no = '" + bidNo + "'  and path_code='" + part + "' and job_no='" + jobNo + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sql = @"delete ps_t_supplementalnotice_equip where supplementalnotice_code='" + sncode + "' and rowid='" + dr["rowid"].ToString() + "'";
                        db.ExecNonQuery(sql, zetl4eisdb);
                        sql = @"delete ps_t_supplementalnotice_equip_doc_design where supplementalnotice_code='" + sncode + "' and mytask_eq_id ='" + dr["rowid"].ToString() + "'";
                        db.ExecNonQuery(sql, zetl4eisdb);
                    }
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public void savePartRemarkSN(string dept, string sect, string jobno, string schno, string part, string rmk, string desc, string imp,string sncode)
        {
            try
            {
                string sql = @"update ps_t_supplementalnotice_part set part_remark='" + rmk + "',part_description='" + desc + "',important='" + imp + @"'
where supplementalnotice_code='" + sncode + "' and depart_position_name ='" + dept + @"' 
and job_no = '" + jobno + "' and schedule_no = '" + schno + "' and part_name = '" + part + "'";

                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public void insertSNPart(addMytaskEquip obj,string sncode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select part_name from ps_t_supplementalnotice_part
where supplementalnotice_code='"+ sncode +"' and depart_position_name = '" + obj.depart_position_name + "' and section_position_name = '" + obj.section_position_name + @"'
and job_no = '" + obj.job_no + "' and schedule_no = '" + obj.schedule_no + "' and part_name = '" + obj.part_name + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"update ps_t_supplementalnotice_part set part_description='" + obj.part_description + "',important='" + obj.important + "',updated_by='" + obj.updated_by + "',updated_datetime='" + obj.updated_datetime + "'" +
                        "where supplementalnotice_code='" + sncode + "' and depart_position_name = '" + obj.depart_position_name + "' and section_position_name = '" + obj.section_position_name + @"'
and job_no = '" + obj.job_no + "' and schedule_no = '" + obj.schedule_no + "' and part_name = '" + obj.part_name + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice_part
           (supplementalnotice_code
            ,supplementalnotice_version
           ,[activity_id]
           ,[step]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[part_name]
           ,[part_description]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
            ,[important])
     VALUES
           ('" + sncode + @"'
           ,'0'
           ,'" + obj.activity_id + @"'
           ,'" + obj.step + @"'
           ,'" + obj.job_no + @"'
           ,'" + obj.job_revision + @"'
           ,'" + obj.bid_no + @"'
           ,'" + obj.bid_revision + @"'
           ,'" + obj.schedule_no + @"'
           ,'" + obj.schedule_name + @"'
           ,'" + obj.depart_position_name + @"'
           ,'" + obj.section_position_name + @"'
           ,'" + obj.part_name + @"'
           ,'" + obj.part_description + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + @"'
           ,'" + obj.important + @"')";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public int insertSNEquip(addMytaskEquip objMytask,string sncode)
        {
            int rowid = 0;
            int version = 0;
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid,supplementalnotice_version  from ps_t_supplementalnotice_equip where supplementalnotice_code ='" + sncode + "' and job_no = '" + objMytask.job_no + "' and job_revision = '" + objMytask.job_revision + "' and bid_no = '" + objMytask.bid_no + "' and bid_revision = '" + objMytask.bid_revision + @"'
and schedule_no = '" + objMytask.schedule_no + "' and schedule_name = '" + objMytask.schedule_name + "' and depart_position_name = '" + objMytask.depart_position_name + "' and section_position_name = '" + objMytask.section_position_name + @"'
and path_code = '" + objMytask.path_code + "' and equip_code='" + objMytask.equip_code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

                if (dt.Rows.Count > 0)
                {
                    version = Convert.ToInt32(dt.Rows[0]["supplementalnotice_version"]) + 1;
                    rowid = Convert.ToInt32(dt.Rows[0]["rowid"]);
                    sql = @"UPDATE ps_t_supplementalnotice_equip
   SET 
       supplementalnotice_version='" + version + @"'
      ,[equip_item_no] = '" + objMytask.equip_item_no + @"'
      ,[equip_add_desc] = '" + objMytask.equip_add_desc + @"'
      ,[equip_standard_drawing] = '" + objMytask.equip_standard_drawing + @"'
      ,[equip_qty] = '" + objMytask.equip_qty + @"'
      ,[equip_unit] = '" + objMytask.equip_unit + @"'
      ,[equip_bid_qty] = '" + (objMytask.equip_bid_qty == null ? 0 : objMytask.equip_bid_qty) + @"'
      ,[equip_incoterm] = '" + objMytask.equip_incoterm + @"'
      ,[factory_test] = '" + objMytask.factory_test + @"'
      ,[manual_test] = '" + objMytask.manual_test + @"'
      ,[routine_test] = '" + objMytask.routine_test + @"'
      ,[is_breakdown] = '" + objMytask.is_breakdown + @"'
      ,[currency] = '" + objMytask.currency + @"'
      ,[supply_equip_unit_price] = '" + objMytask.supply_equip_unit_price + @"'
      ,[supply_equip_amonut] = '" + objMytask.supply_equip_amonut + @"'
      ,[local_exwork_unit_price] = '" + objMytask.local_exwork_unit_price + @"'
      ,[local_exwork_amonut] = '" + objMytask.local_exwork_amonut + @"'
      ,[local_tran_unit_price] = '" + objMytask.local_exwork_unit_price + @"'
      ,[local_tran_amonut] = '" + objMytask.local_tran_amonut + @"'
      ,[updated_by] = '" + objMytask.updated_by + @"'
      ,[updated_datetime] = '" + objMytask.updated_datetime + @"'
 WHERE job_no = '" + objMytask.job_no + "' and job_revision = '" + objMytask.job_revision + "' and bid_no = '" + objMytask.bid_no + "' and bid_revision = '" + objMytask.bid_revision + @"'
and schedule_no = '" + objMytask.schedule_no + "' and schedule_name = '" + objMytask.schedule_name + "' and depart_position_name = '" + objMytask.depart_position_name + "' and section_position_name = '" + objMytask.section_position_name + @"'
and path_code = '" + objMytask.path_code + "' and equip_code='" + objMytask.equip_code + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);

      //               ,[equip_reserve_qty]
      //,[equip_confirm_qty]
      //,[equip_bid_qty]
      //,[proposed_price]
      //,[evaluation_price]
      //,[import_duty]
                }
                else
                {
                    sql = @"insert into ps_t_supplementalnotice_equip([supplementalnotice_code]
              ,[supplementalnotice_version],section_position_name,depart_position_name
,job_no
,bid_no
,equip_code
,created_by
,created_datetime
,updated_by
,updated_datetime
,equip_add_desc
,bid_revision
,job_revision
,schedule_no
,is_breakdown
,path_code
,equip_item_no
,activity_id
,step
,equip_full_desc
,equip_unit
,schedule_name
,equip_qty
,legend
,equip_incoterm
,factory_test
,manual_test
,routine_test
,supply_equip_unit_price
,supply_equip_amonut
,local_exwork_unit_price
,local_exwork_amonut
,local_tran_unit_price
,local_tran_amonut
,equip_standard_drawing
,currency
,equip_mas_grp)values('" +sncode + "','0','" + objMytask.section_position_name + "','" + objMytask.depart_position_name + "','" + objMytask.job_no + "','" + objMytask.bid_no + "','" + objMytask.equip_code + "'," +
"'" + objMytask.created_by + "','" + objMytask.created_datetime + "','" + objMytask.updated_by + "'," +
"'" + objMytask.updated_datetime + "','" + objMytask.equip_add_desc + "','" + objMytask.bid_revision + "','" + objMytask.job_revision + "'," +
"'" + objMytask.schedule_no + "','" + objMytask.is_breakdown + "','" + objMytask.path_code + "','" + objMytask.equip_item_no + "'," +
"'" + objMytask.activity_id + "','" + objMytask.step + "','" + objMytask.equip_full_desc + "','" + objMytask.equip_unit + "','" + objMytask.schedule_name + "','" + objMytask.equip_qty + "','" + objMytask.legend + "'," +
"'" + objMytask.equip_incoterm + "','" + objMytask.factory_test + "','" + objMytask.manual_test + "','" + objMytask.routine_test + "','" + objMytask.supply_equip_unit_price + "'," +
"'" + objMytask.supply_equip_amonut + "','" + objMytask.local_exwork_unit_price + "','" + objMytask.local_exwork_amonut + "','" + objMytask.local_tran_unit_price + "','" + objMytask.local_tran_amonut + "'," +
"'" + objMytask.equip_standard_drawing + "','" + objMytask.currency + "','" + objMytask.equip_mas_grp + "')";
                    db.ExecNonQuery(sql, zetl4eisdb);
                    sql = @"select max(rowid) as rowid from ps_t_supplementalnotice_equip";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0) rowid = Convert.ToInt32(dt.Rows[0]["rowid"]);
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return rowid;
        }
        public DataTable getallEQSN(string bidno, string rev, string jobno,string sncode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct equip_code from ps_t_supplementalnotice_equip where supplementalnotice_code='" + sncode + "' and bid_no ='" + bidno + "' and bid_revision='" + rev + "' and job_no in (" + jobno + ")";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void insertDocFromAllEQSN(string eq_code, string Dept, string Sect, string bidno, string rev, string AcID, string login, string sncode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = @"select doc_nodeid from ps_m_equip_doc where equip_code ='" + eq_code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string code = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (code == "") code += "'" + dr["doc_nodeid"].ToString() + "'";
                        else code += ",'" + dr["doc_nodeid"].ToString() + "'";
                    }
                    sql = @"select ps_m_doc_approve.*,doctype_name from ps_m_doc_approve,ps_m_doctype 
where ps_m_doc_approve.doctype_code = ps_m_doctype.doctype_code
and doc_nodeid in(" + code + ") and doc_status='Active' and depart_position_name='" + Dept + "' ";//and section_position_name='" + Sect + "'
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sql = @"select rowid from ps_t_supplementalnotice_doc
where supplementalnotice_code='" + sncode + "' and doc_nodeid ='" + dr["doc_nodeid"].ToString() + @"' and bid_no='" + bidno + @"' 
and depart_position_name='" + Dept + "' and section_position_name='" + Sect + @"'
and doctype_code='" + dr["doctype_code"].ToString() + "' and doc_name='" + dr["doc_name"].ToString() + @"' and job_revision='" + rev + "'";
                            dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                            if (dt.Rows.Count == 0)
                            {
                                sql = @"INSERT INTO [ps_t_supplementalnotice_doc]
           ([supplementalnotice_code]
                              ,[supplementalnotice_version]
                              ,[activity_id]
                              ,[job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_revision_no]
                              ,[doc_name]
                              ,[doc_sheetno]
                              ,[doc_desc]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
     VALUES
           ('" + sncode + @"'
           ,'0'
           ,'" + AcID + @"'
           ,''
           ,''
           ,'" + bidno + @"'
           ,'" + rev + @"'
           ,''
           ,''
           ,'" + Dept + @"'
           ,'" + Sect + @"'
           ,'" + dr["doctype_code"].ToString() + @"'
           ,'" + dr["doc_nodeid"].ToString() + @"'
           ,'" + dr["doc_revision_no"].ToString() + @"'
           ,'" + dr["doc_name"].ToString() + @"'
           ,'" + dr["doc_sheetno"].ToString() + @"'
           ,'" + dr["doc_desc"].ToString() + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE())";
                                db.ExecNonQuery(sql, zetl4eisdb);

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void insertSNEqDoc(string Dept, string Sect, string jobno, string jobrev, string bidno, string bidrev, string schname, string schno, string eq_code, string eq_desc, string login, string docid, string pid, int rowid,string sncode)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select * from ps_t_supplementalnotice_equip_doc_design where bid_no='" + bidno + "' and bid_revision='" + bidrev + "' and job_no='" + jobno + "' and doc_nodeid='" + docid + "' and equip_code='" + eq_code + "' and mytask_eq_id='"+ rowid + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

                if (dt.Rows.Count > 0)
                {

                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice_equip_doc_design
           ([supplementalnotice_code]
              ,[supplementalnotice_version]
              ,[job_no]
              ,[job_revision]
              ,[bid_no]
              ,[bid_revision]
              ,[schedule_no]
              ,[schedule_name]
              ,[depart_position_name]
              ,[section_position_name]
              ,[equip_code]
              ,[doc_nodeid]
              ,[created_by]
              ,[created_datetime]
              ,[updated_by]
              ,[updated_datetime]
              ,[equip_full_desc]
              ,[mytask_eq_id])
     VALUES
           ('" + sncode + @"'
           ,'0'
           ,'" + jobno + @"'
           ,'" + jobrev + @"'
           ,'" + bidno + @"'
           ,'" + bidrev + @"'
           ,'" + schno + @"'
           ,'" + schname + @"'
           ,'" + Dept + @"'
           ,'" + Sect + @"'
           ,'" + eq_code + @"'
           ,'" + docid + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
           ,'" + eq_desc + @"'
            ,'" + rowid + "')";
                }
            
                db.ExecNonQuery(sql, zetl4eisdb);


            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public void getChkToInsertEquipSN(string sncode, string jobNo, string bidNo, string schname,string part ,string login)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                 sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_name,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,step,activity_id,is_new_schedule,equip_item_no_b,equip_reserve_qty,equip_confirm_qty,waive_test,part_code_b   
 ,[service_account_assign]
      ,[service_cost_object]
      ,[service_fund_center]
      ,[service_gl]
      ,[breakdown_tab]
      ,[proposed_price]
      ,[evaluation_price]
      ,[preference_status]
      ,[import_duty]
      ,[irrelevant]
      ,[turnkey_account_assign]
      ,[turnkey_cost_object]
      ,[turnkey_fund_center]
      ,[turnkey_gl]
,is_breakdown
  ,[supply_account_assign]
      ,[supply_cost_object]
      ,[supply_fund_center]
      ,[supply_gl]
         , local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit,'' as Part_Sort
                from  ps_t_mytask_equip 
                where bid_no='" + bidNo + "' order by len(equip_item_no) asc,ASCII(equip_item_no) ,equip_item_no";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        sql = @"select * from  ps_t_supplementalnotice_equip 
                where job_no = '" + dr["job_no"].ToString() + "' and schedule_name ='" + dr["schedule_name"].ToString() + "'  and path_code='" + dr["path_code"].ToString() + "'and bid_no='" + dr["bid_no"].ToString() + "' and supplementalnotice_code='" + sncode + "' order by len(equip_item_no) asc,ASCII(equip_item_no) ,equip_item_no";

                     DataTable dtinsert = db.ExecSql_DataTable(sql, zetl4eisdb);
                        if (dtinsert.Rows.Count == 0)
                        {

                            sql = @"INSERT INTO ps_t_supplementalnotice_equip
           ([supplementalnotice_code]
      ,[supplementalnotice_version]
      ,[step]
      ,[activity_id]
      ,[job_no]
      ,[job_revision]
      ,[bid_no]
      ,[bid_revision]
      ,[schedule_no]
      ,[schedule_name]
      ,[is_new_schedule]
      ,[depart_position_name]
      ,[section_position_name]
      ,[equip_mas_grp]
      ,[equip_item_no]
      ,[equip_item_no_b]
      ,[equip_code]
      ,[equip_add_desc]
      ,[equip_standard_drawing]
      ,[equip_qty]
      ,[equip_unit]
      ,[equip_reserve_qty]
      ,[equip_confirm_qty]
      ,[equip_bid_qty]
      ,[equip_incoterm]
      ,[factory_test]
      ,[manual_test]
      ,[routine_test]
      ,[waive_test]
      ,[legend]
      ,[path_code]
      ,[part_code_b]
      ,[is_breakdown]
      ,[breakdown_tab]
      ,[currency]
      ,[proposed_price]
      ,[evaluation_price]
      ,[preference_status]
      ,[import_duty]
      ,[irrelevant]
      ,[turnkey_account_assign]
      ,[turnkey_cost_object]
      ,[turnkey_fund_center]
      ,[turnkey_gl]
      ,[supply_account_assign]
      ,[supply_cost_object]
      ,[supply_fund_center]
      ,[supply_gl]
      ,[supply_equip_unit_price]
      ,[supply_equip_amonut]
      ,[local_exwork_unit_price]
      ,[local_exwork_amonut]
      ,[local_tran_unit_price]
      ,[local_tran_amonut]
      ,[service_account_assign]
      ,[service_cost_object]
      ,[service_fund_center]
      ,[service_gl]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime]
      ,[equip_full_desc])
     VALUES
           ('" + sncode + @"'
           ,'0'
           ,'" + dr["step"].ToString() + @"'
           ,'" + dr["activity_id"].ToString() + @"'
           ,'" + dr["job_no"].ToString() + @"'
           ,'" + dr["job_revision"].ToString() + @"'
           ,'" + dr["bid_no"].ToString() + @"'
           ,'" + dr["bid_revision"].ToString() + @"'
           ,'" + dr["schedule_no"].ToString() + @"'
           ,'" + dr["schedule_name"].ToString() + @"'
           ,'" + dr["is_new_schedule"].ToString() + @"'
           ,'" + dr["depart_position_name"].ToString() + @"'
           ,'" + dr["section_position_name"].ToString() + @"'
           ,'" + dr["equip_mas_grp"].ToString() + @"'
           ,'" + dr["equip_item_no"].ToString() + @"'
           ,'" + dr["equip_item_no_b"].ToString() + @"'
           ,'" + dr["equip_code"].ToString() + @"'
           ,'" + dr["equip_add_desc"].ToString() + @"'
           ,'" + dr["equip_standard_drawing"].ToString() + @"'
           ,'" + dr["equip_qty"].ToString() + @"'
           ,'" + dr["equip_unit"].ToString() + @"'
           ,'0'
           ,'0'
           ,'0'
           ,'" + dr["equip_incoterm"].ToString() + @"'
           ,'" + dr["factory_test"].ToString() + @"'
           ,'" + dr["manual_test"].ToString() + @"'
           ,'" + dr["routine_test"].ToString() + @"'
           ,'" + dr["waive_test"].ToString() + @"'
           ,'" + dr["legend"].ToString() + @"'
           ,'" + dr["path_code"].ToString() + @"'
           ,'" + dr["part_code_b"].ToString() + @"'
           ,'" + dr["is_breakdown"].ToString() + @"'
           ,'" + dr["breakdown_tab"].ToString() + @"'
           ,'" + dr["currency"].ToString() + @"'
           ,'0'
           ,'0'
           ,'" + dr["preference_status"].ToString() + @"'
           ,'0'
           ,'" + dr["irrelevant"].ToString() + @"'
           ,'" + dr["turnkey_account_assign"].ToString() + @"'
           ,'" + dr["turnkey_cost_object"].ToString() + @"'
           ,'" + dr["turnkey_fund_center"].ToString() + @"'
           ,'" + dr["turnkey_gl"].ToString() + @"'
           ,'" + dr["supply_account_assign"].ToString() + @"'
           ,'" + dr["supply_cost_object"].ToString() + @"'
           ,'" + dr["supply_fund_center"].ToString() + @"'
           ,'" + dr["supply_gl"].ToString() + @"'
           ,'" + dr["supply_equip_unit_price"].ToString() + @"'
           ,'" + dr["supply_equip_amonut"].ToString() + @"'
           ,'" + dr["local_exwork_unit_price"].ToString() + @"'
           ,'" + dr["local_exwork_amonut"].ToString() + @"'
           ,'" + dr["local_tran_unit_price"].ToString() + @"'
           ,'" + dr["local_tran_amonut"].ToString() + @"'
           ,'" + dr["service_account_assign"].ToString() + @"'
           ,'" + dr["service_cost_object"].ToString() + @"'
           ,'" + dr["service_fund_center"].ToString() + @"'
           ,'" + dr["service_gl"].ToString() + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
          ,'" + dr["equip_full_desc"].ToString() + @"')";
                            db.ExecNonQuery(sql, zetl4eisdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public DataTable getGVSelPartSN(string sncode, string sect, string jobNo, string schNo, string part, string bidno, string schname)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select supplementalnotice_code,supplementalnotice_version,rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit,'' as Part_Sort
                from  ps_t_supplementalnotice_equip 
                where supplementalnotice_code ='" + sncode + "' and job_no = '" + jobNo + "' and schedule_name ='" + schname + "'  and path_code='" + part + "'and bid_no='" + bidno + @"'
                order by len(equip_item_no) asc,ASCII(equip_item_no) ,equip_item_no";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getHead2()
        {
            DataTable dt = new DataTable();

            string strQuery = @"SELECT 
 distinct e.PERNR, e.SNAME, e.EMAIL, e.MC_SHORT, e.MC_STEXT, e.ORGLEVEL, e.ORGSNAME1,e.ORGSNAME2, e.ORGSNAME3, e.ORGSNAME4, e.ORGSNAME5, e.ORGNAME1,e.ORGNAME2, e.ORGNAME3, e.ORGNAME4, e.ORGNAME5
FROM M_SYN_EMPLOYEE e
where  e.mc_short like 'หก-%'";

            dt = oraDBUtil._getDT(strQuery, db_common);

            return dt;
        }
        public DataTable chkStaff(string sncode, string dept, string login)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_t_supplementalnotice_assign where supplementalnotice_code='"+ sncode + "' and assignee_id like'%"+ login + "%' and depart_name ='"+ dept + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public string getRunSN(string bidno)
        {
            DataTable dt = new DataTable();
            string strSNcode = "";
            string sql = "";
            try
            {
                sql = @"select * from ps_t_supplementalnotice where supplementalnotice_code like'%" + bidno + "%'";
                sql += " order by ASCII(supplementalnotice_code), len(supplementalnotice_code),supplementalnotice_code desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string[] sp = dt.Rows[0]["supplementalnotice_code"].ToString().Split('_');
                    if (sp.Length > 1)
                    {
                        int i = (sp[1] != "" ? Convert.ToInt32(sp[1]) : 0) + 1;
                        strSNcode = bidno + "_" + i;
                    }
                }
                else
                {
                    strSNcode = bidno + "_0";
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return strSNcode;
        }
        public void deleteAttach(string doccode, string nodeid, string sncode,string rowid)
        {
            string sql = "";
            try
            {
                sql = @"delete from ps_t_supnotice_att where doctype_code='"+ doccode + "' and doc_nodeid ='"+ nodeid + "' and supplementalnotice_code ='"+ sncode + "' and rowid='"+ rowid + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
       
        public void updateAtt(string bidno, string subject, string sncode,string dept,string rev, string login)
        {
            DataTable dt = new DataTable();
            string sql = "";
            string sqlchk = "";
            try
            {
                sqlchk = @"select * from ps_t_supplementalnotice  where supplementalnotice_code ='"+ sncode + "'";
                dt = db.ExecSql_DataTable(sqlchk, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"update ps_t_supplementalnotice set subject='" + subject + "', bid_no='" + bidno + "',depart_list='" + dept + "' where supplementalnotice_code ='" + sncode + "'";
                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice
                               ( [supplementalnotice_code]
                              ,[subject]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[depart_list]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + sncode + "' " +
                             " , '" + subject + "' " +
                             " , '" + bidno + "' " +
                             " , '" + rev + "' " +
                             " , '" + dept + "' " +
                             " , '' " +
                             " , '' " +
                             " , '" + login + "' " +
                             " , GETDATE() " +
                             " , '" + login + "' " +
                             " , GETDATE())";
                }

        
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public void insertAttach(string doccode, string nodeid,string docname,string login,string sncode,string bidno,string rev,string subject,string depart,string sncreate)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = new DataTable();
            string sql = "";
            string strSNCode = "";
            try
            {
                if (sncode != "")
                {
                    strSNCode = sncode;
                }
                else
                {
                    strSNCode = sncreate;
                }
                sql = "delete from ps_t_supplementalnotice where supplementalnotice_code='" + strSNCode + "' and bid_no='" + bidno + "' and bid_revision='" + rev + "' and doc_nodeid=0";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = "select * from ps_t_supplementalnotice where supplementalnotice_code='" + strSNCode + "' and bid_no='" + bidno + "' and bid_revision='" + rev + "' and doc_nodeid='" + nodeid + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {

                }
                else
                {
                    sql = @"INSERT INTO ps_t_supplementalnotice
                               ( [supplementalnotice_code]
                              ,[subject]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[depart_list]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + strSNCode + "' " +
                             " , '" + subject + "' " +
                             " , '" + bidno + "' " +
                             " , '" + rev + "' " +
                             " , '" + depart + "' " +
                             " , '" + nodeid + "' " +
                             " , '" + docname + "' " +
                             " , '" + login + "' " +
                             " , GETDATE() " +
                             " , '" + login + "' " +
                             " , GETDATE())";
                    db.ExecNonQuery(sql, zetl4eisdb);

                }
                sql = @"select * from ps_t_supnotice_att where supplementalnotice_code='"+ strSNCode + "' and doctype_code='" + doccode + "' and doc_nodeid='" + nodeid + "'";
                dt2 = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt2.Rows.Count > 0)
                {
                    sql = @"update ps_t_supnotice_att set doc_nodeid='"+ nodeid + "',doc_name='"+ docname + "' where supplementalnotice_code='" + strSNCode + "' and doctype_code='" + doccode + "' and doc_nodeid='" + nodeid + "'";
                }
                else
                {
                    sql = @"INSERT INTO ps_t_supnotice_att
                              ( [supplementalnotice_code]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                            VALUES
                                ('" + strSNCode + "' " +
                          " , '" + doccode + "' " +
                          " , '" + nodeid + "' " +
                          " , '" + docname + "' " +
                          " , '" + login + "' " +
                          " , GETDATE() " +
                          " , '" + login + "' " +
                          " , GETDATE())";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }


            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            
        }

        private string genSNCode()
        {
            string xBOMCode = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'SN'";
            DataTable dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'SN' ";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('SN', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            xBOMCode = "SN-" + xRunno.ToString("0000");
            return xBOMCode;
        }
    }
}
