﻿using PS_Library.WorkflowService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsWfPsAPV01
    {
        #region declare variable
        //public string strJobNo { get; set; }
        //public int iJobRevision { get; set; }
        //public string strActivityId { get; set; }

        private object OTCSFunctions { get; set; }
        private string strWfMapId = ConfigurationManager.AppSettings["wf_job_map_id"].ToString();
        private string strWfBidMapId = ConfigurationManager.AppSettings["wf_bid_map_id"].ToString();
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();

        private oraDBUtil zdb = new oraDBUtil();
        private DbControllerBase db = new DbControllerBase();
        private OTFunctions zotFunctions = new OTFunctions();
        private DocumentManagement.Node doc_node = new DocumentManagement.Node();
        EmpInfoClass emp = new EmpInfoClass();
        #endregion

        public DataTable GetDrawing(string Jobno, string chkdraw, string rev, string mode, string pid)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT distinct [depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doctype_name]
                              ,[doc_no]
                              ,[doc_desc]
                              ,[doc_sheetno]
                              ,[doc_revision]
                              ,[job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[link_doc]
                              ,[updated_by]
                              ,CONVERT(varchar(20), [updated_datetime],103) as updated_datetime
                              ,[tr_doc_desc]
                              ,[tr_updated_by]
                               ,CONVERT(varchar(20), [tr_updated_datetime],103) as tr_updated_datetime
                          FROM vw_ps_dashboard_doc ";// and depart_position_name ='" + dept + "' and section_position_name ='" + section + "'
                if (mode == "job") strQuery += " where job_no = '" + Jobno + "' and job_revision = '" + rev + "'";
                else if (mode == "bid") strQuery += " where bid_no = '" + Jobno + "' and bid_revision = '" + rev + "'";
                string rowfilter = "";
                if (chkdraw != "")
                {
                    rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code in ('DD','TD'))";
                    strQuery += rowfilter;
                }
                else
                {
                    rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code not in ('DD','TD'))";
                    strQuery += rowfilter;
                }
                strQuery += " and process_id ='" + pid + "'";
                dt = db.ExecSql_DataTable(strQuery, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getGVSelPart(string bidNo, string bidrev, string jobNo, string jobrev, string mode, string schName, string pid)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (mode == "bid")
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit 
                from  ps_t_mytask_equip 
                where bid_no = '" + bidNo + "' and bid_revision='" + bidrev + "' and schedule_name='" + schName + "' and process_id='" + pid + "'";// and path_code='" + part + "'
                }
                else if (mode == "job")
                {
                    if (bidNo == "Bid Supply")
                    {
                        sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit 
                from  ps_t_mytask_equip 
                where job_no = '" + jobNo + "' and job_revision='" + jobrev + "' and process_id='" + pid + "'";// and path_code='" + part + "'

                    }
                    else
                    {
                        sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit 
                from  ps_t_mytask_equip 
                where job_no = '" + jobNo + "' and job_revision='" + jobrev + "' and process_id='" + pid + "' and bid_no = '" + bidNo + "'";// and path_code='" + part + "'
                    }

                }
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getBtnPaths_bid(string bidNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct part_name,part_description,part_remark,important from ps_t_mytask_part
where  bid_no = '" + bidNo + "' order by part_name";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getBtnPaths_job(string jobNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct part_name,part_description,part_remark,important from ps_t_mytask_part
where  job_no = '" + jobNo + "'";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getTabBid(string jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from(
select bid_no,schedule_no,bid_revision,schedule_name from vw_ps_job_dashboard_relate
where job_no = '" + jobno + "' and job_revision = '" + rev + @"' and schedule_no <>''
union 
select 'Bid Supply'as bid_no,'0'as schedule_no,'0' as bid_revision,'' as schedule_name from vw_ps_job_dashboard_relate
where job_no = '" + jobno + "' and job_revision = '" + rev + @"' and schedule_no  is null)
as tmp order by bid_no desc";
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetSummary(string bidNo)
        {
            DataTable dt = new DataTable();
            try
            {

                string strQuery = @"SELECT [Rowno]
			 ,[job_no]
			 ,[job_desc]
			 ,[job_revision]
			 ,[schedule_no]
			 ,[schedule_name]
			 ,[schedule_status]
			 ,[bid_no]
			 ,[bid_revision]
		     ,[wa_url]
			FROM [vw_ps_bid_dashboard_relate] where bid_no ='" + bidNo + "'";

                dt = db.ExecSql_DataTable(strQuery, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public clsWfPsAPV01() { }

        #region web form both

        public DataTable GetDPDataByDpPsNo(string strPsNo)
        {
            DataTable dt = new DataTable();
            string strSql = " select * from wf_ps_request where ps_reqno = '" + strPsNo + "'  ";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDPDataByWorkId_BK(string strWorkId)
        {
            DataTable dt = new DataTable();
            string strSql = " select * from wf_ps_request where wf_processid = " + strWorkId;
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDPDataByWorkId(string strWorkId)
        {
            DataTable dt = new DataTable();
            string strSql = @" select d.process_id,d.apv_order,d.ps_reqno,d.emp_id,m.note,m.activity_id,m.job_revision,m.job_no,m.bid_revision,m.bid_no,
                                d.emp_name,d.emp_position,d.emp_position_full,
                                d.emp_apvdate,d.apv_status,m.wf_processid,m.ps_subject,m.doc_node_id
                                from wf_ps_request_detail d
                                left join wf_ps_request m
                                on m.process_id = d.process_id 
                                where m.wf_processid = '" + strWorkId + "' " +
                                "order by d.apv_order asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }


        #endregion

        #region web form job
        public DataTable GetWfDetailByProcessId(string strProcessId)
        {
            DataTable dt = new DataTable();
            string strSql = @" select process_id,apv_order,ps_reqno,emp_id,
                                emp_name,emp_position,emp_position_full,
                                emp_apvdate,apv_status
								from wf_ps_request_detail
								where  process_id = '" + strProcessId + @"'
								order by apv_order asc";


            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }


        //private void getMemo(string WAANo, string xProcessID)
        //{
        //    string xdoc_url = "";
        //    var xparent_node = zotFunctions.getNodeByName(zwfwaa_attachment_nodeid, WAANo);
        //    if (xparent_node == null)
        //    {
        //        xparent_node = zotFunctions.getNodeByName(zwfwaa_attachment_nodeid, xProcessID);

        //    }
        //    if (xparent_node != null)
        //    {
        //        doc_node = zotFunctions.getNodeByName(xparent_node.ID.ToString(), WAANo + ".pdf");
        //        if (doc_node != null)
        //        {
        //            xdoc_url = "viewCSDoc.aspx?docnodeid=" + doc_node.ID.ToString();

        //        }
        //        else
        //        {
        //            xdoc_url = "http://" + zhost_name + "/tmps/forms/WAA01Preview?waareq_no=" + WAANo;

        //        }
        //        Response.Headers.Remove("X-Frame-Options");
        //        Response.AddHeader("X-Frame-Options", "AllowAll");
        //        ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);

        //    }
        //}


        public int GetJobApproverRoutingCount(string strJobNo, string strJobRevision, string strActivityId)
        {
            int iCountStep = 0;
            string strApproverRoute = string.Empty;
            DataTable dt = new DataTable();

            string strSql = @"SELECT job_no
                              ,job_revision
                              ,activity_id
                              ,approval_routing
							FROM ps_t_job_dashboard 
							WHERE ";
            strSql += " job_revision = " + strJobRevision;
            strSql += " and activity_id = '" + strActivityId + "' ";
            strSql += " and job_no = '" + strJobNo + "' ";

            try
            {
                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
                if (dt.Rows.Count > 0)
                {
                    strApproverRoute = dt.Rows[0]["approval_routing"].ToString();
                    string[] arrRoute = GetRoutingApproval(strApproverRoute);
                    iCountStep = arrRoute.Length;
                }
            }
            catch { }
            return iCountStep;
        }


        #endregion

        #region web form bid

        //kie
        public int GetBidApproverRoutingCount(string strBidNo, string strBidRevision, string strActivityId)
        {
            int iCountStep = 0;
            string strApproverRoute = string.Empty;
            DataTable dt = new DataTable();

            string strSql = @"SELECT bid_no
                              ,bid_revision
                              ,activity_id
                              ,approval_routing
							FROM ps_t_bid_dashboard 
							WHERE ";
            strSql += " bid_revision = " + strBidRevision;
            strSql += " and activity_id = '" + strActivityId + "' ";
            strSql += " and bid_no = '" + strBidNo + "' ";

            try
            {
                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
                if (dt.Rows.Count > 0)
                {
                    strApproverRoute = dt.Rows[0]["approval_routing"].ToString();
                    string[] arrRoute = GetRoutingApproval(strApproverRoute);
                    iCountStep = arrRoute.Length;
                }
            }
            catch { }
            return iCountStep;
        }

        #endregion


        #region Start wf both

        public void InsertWfRequest(WfPsRequest objWf)
        {
            string strSql, strFields, strValue;
            strSql = strFields = strValue = string.Empty;
            if (!String.IsNullOrEmpty(objWf.activity_id))
            {
                strFields += strFields == string.Empty ? "activity_id" : ",activity_id";
                strValue += strValue == string.Empty ? "'" + objWf.activity_id + "'" : ",'" + objWf.activity_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.bid_no))
            {
                strFields += strFields == string.Empty ? "bid_no" : ",bid_no";
                strValue += strValue == string.Empty ? "'" + objWf.bid_no + "'" : ",'" + objWf.bid_no + "'";
            }
            if (!String.IsNullOrEmpty(objWf.bid_revision))
            {
                strFields += strFields == string.Empty ? "bid_revision" : ",bid_revision";
                strValue += strValue == string.Empty ? "'" + objWf.bid_revision + "'" : ",'" + objWf.bid_revision + "'";
            }
            if (!String.IsNullOrEmpty(objWf.created_by))
            {
                strFields += strFields == string.Empty ? "created_by" : ",created_by";
                strValue += strValue == string.Empty ? "'" + objWf.created_by + "'" : ",'" + objWf.created_by + "'";
            }
            if (!String.IsNullOrEmpty(objWf.current_step))
            {
                strFields += strFields == string.Empty ? "current_step" : ",current_step";
                strValue += strValue == string.Empty ? "'" + objWf.current_step + "'" : ",'" + objWf.current_step + "'";
            }
            if (!String.IsNullOrEmpty(objWf.form_cc))
            {
                strFields += strFields == string.Empty ? "form_cc" : ",form_cc";
                strValue += strValue == string.Empty ? "'" + objWf.form_cc + "'" : ",'" + objWf.form_cc + "'";
            }
            if (!String.IsNullOrEmpty(objWf.form_status))
            {
                strFields += strFields == string.Empty ? "form_status" : ",form_status";
                strValue += strValue == string.Empty ? "'" + objWf.form_status + "'" : ",'" + objWf.form_status + "'";
            }
            if (!String.IsNullOrEmpty(objWf.form_to))
            {
                strFields += strFields == string.Empty ? "form_to" : ",form_to";
                strValue += strValue == string.Empty ? "'" + objWf.form_to + "'" : ",'" + objWf.form_to + "'";
            }
            if (!String.IsNullOrEmpty(objWf.job_no))
            {
                strFields += strFields == string.Empty ? "job_no" : ",job_no";
                strValue += strValue == string.Empty ? "'" + objWf.job_no + "'" : ",'" + objWf.job_no + "'";
            }
            if (!String.IsNullOrEmpty(objWf.job_revision))
            {
                strFields += strFields == string.Empty ? "job_revision" : ",job_revision";
                strValue += strValue == string.Empty ? "'" + objWf.job_revision + "'" : ",'" + objWf.job_revision + "'";
            }
            if (!String.IsNullOrEmpty(objWf.next_step))
            {
                strFields += strFields == string.Empty ? "next_step" : ",next_step";
                strValue += strValue == string.Empty ? "'" + objWf.next_step + "'" : ",'" + objWf.next_step + "'";
            }
            if (!String.IsNullOrEmpty(objWf.note))
            {
                strFields += strFields == string.Empty ? "note" : ",note";
                strValue += strValue == string.Empty ? "'" + objWf.note + "'" : ",'" + objWf.note + "'";
            }
            if (!String.IsNullOrEmpty(objWf.process_id))
            {
                strFields += strFields == string.Empty ? "process_id" : ",process_id";
                strValue += strValue == string.Empty ? "'" + objWf.process_id + "'" : ",'" + objWf.process_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.project_id))
            {
                strFields += strFields == string.Empty ? "project_id" : ",project_id";
                strValue += strValue == string.Empty ? "'" + objWf.project_id + "'" : ",'" + objWf.project_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.ps_reqno))
            {
                strFields += strFields == string.Empty ? "ps_reqno" : ",ps_reqno";
                strValue += strValue == string.Empty ? "'" + objWf.ps_reqno + "'" : ",'" + objWf.ps_reqno + "'";
            }
            if (!String.IsNullOrEmpty(objWf.ps_subject))
            {
                strFields += strFields == string.Empty ? "ps_subject" : ",ps_subject";
                strValue += strValue == string.Empty ? "'" + objWf.ps_subject + "'" : ",'" + objWf.ps_subject + "'";
            }
            if (!String.IsNullOrEmpty(objWf.reference))
            {
                strFields += strFields == string.Empty ? "reference" : ",reference";
                strValue += strValue == string.Empty ? "'" + objWf.reference + "'" : ",'" + objWf.reference + "'";
            }

            if (!String.IsNullOrEmpty(objWf.updated_by))
            {
                strFields += strFields == string.Empty ? "updated_by" : ",updated_by";
                strValue += strValue == string.Empty ? "'" + objWf.updated_by + "'" : ",'" + objWf.updated_by + "'";
            }
            if (objWf.wf_processid.HasValue)
            {
                strFields += strFields == string.Empty ? "wf_processid" : ",wf_processid";
                strValue += strValue == string.Empty ? objWf.wf_processid.ToString() : "," + objWf.wf_processid.ToString();
            }
            if (objWf.wf_subprocessid.HasValue)
            {
                strFields += strFields == string.Empty ? "wf_subprocessid" : ",wf_subprocessid";
                strValue += strValue == string.Empty ? objWf.wf_subprocessid.ToString() : "," + objWf.wf_subprocessid.ToString();
            }
            if (objWf.wf_taskid.HasValue)
            {
                strFields += strFields == string.Empty ? "wf_taskid" : ",wf_taskid";
                strValue += strValue == string.Empty ? objWf.wf_taskid.ToString() : "," + objWf.wf_taskid.ToString();
            }
            if (objWf.created_datetime != null)
            {
                strFields += strFields == string.Empty ? "created_datetime" : ",created_datetime";
                strValue += strValue == string.Empty ? "GETDATE()" : ",GETDATE()";
            }
            if (objWf.updated_datetime != null)
            {
                strFields += strFields == string.Empty ? "updated_datetime" : ",updated_datetime";
                strValue += strValue == string.Empty ? "GETDATE()" : ",GETDATE()";
            }
            strSql = "INSERT INTO wf_ps_request ( ";
            strSql += strFields;
            strSql += " ) VALUES ( ";
            strSql += strValue;
            strSql += " ) ";

            db.ExecNonQuery(strSql, dbConnectionString);

        }

        public void InsertWfRequestDetail(WfPsRequestDetails objWfDetail)
        {
            string strSql, strFields, strValue;
            strSql = strFields = strValue = string.Empty;

            if (!String.IsNullOrEmpty(objWfDetail.process_id))
            {
                strFields += strFields == string.Empty ? "process_id" : ",process_id";
                strValue += strValue == string.Empty ? "'" + objWfDetail.process_id + "'" : ",'" + objWfDetail.process_id + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.ps_reqno))
            {
                strFields += strFields == string.Empty ? "ps_reqno" : ",ps_reqno";
                strValue += strValue == string.Empty ? "'" + objWfDetail.ps_reqno + "'" : ",'" + objWfDetail.ps_reqno + "'";
            }
            if (objWfDetail.apv_order != null)
            {
                strFields += strFields == string.Empty ? "apv_order" : ",apv_order";
                strValue += strValue == string.Empty ? "" + objWfDetail.apv_order.ToString() + "" : "," + objWfDetail.apv_order.ToString() + "";
            }
            if (!String.IsNullOrEmpty(objWfDetail.emp_id))
            {
                strFields += strFields == string.Empty ? "emp_id" : ",emp_id";
                strValue += strValue == string.Empty ? "'" + objWfDetail.emp_id + "'" : ",'" + objWfDetail.emp_id + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.emp_name))
            {
                strFields += strFields == string.Empty ? "emp_name" : ",emp_name";
                strValue += strValue == string.Empty ? "'" + objWfDetail.emp_name + "'" : ",'" + objWfDetail.emp_name + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.emp_position))
            {
                strFields += strFields == string.Empty ? "emp_position" : ",emp_position";
                strValue += strValue == string.Empty ? "'" + objWfDetail.emp_position + "'" : ",'" + objWfDetail.emp_position + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.emp_position_full))
            {
                strFields += strFields == string.Empty ? "emp_position_full" : ",emp_position_full";
                strValue += strValue == string.Empty ? "'" + objWfDetail.emp_position_full + "'" : ",'" + objWfDetail.emp_position_full + "'";
            }
            //if (objWfDetail.emp_apvdate != null)
            //{
            //    strFields += strFields == string.Empty ? "emp_apvdate" : ",emp_apvdate";
            //    strValue += strValue == string.Empty ? "GETDATE()" : ",GETDATE()";
            //}
            if (!String.IsNullOrEmpty(objWfDetail.apv_status))
            {
                strFields += strFields == string.Empty ? "apv_status" : ",apv_status";
                strValue += strValue == string.Empty ? "'" + objWfDetail.apv_status + "'" : ",'" + objWfDetail.apv_status + "'";
            }


            strSql = "INSERT INTO wf_ps_request_detail ( ";
            strSql += strFields;
            strSql += " ) VALUES ( ";
            strSql += strValue;
            strSql += " ) ";

            db.ExecNonQuery(strSql, dbConnectionString);

        }

        public List<Assignee> GetDistictAssignee(List<Assignee> assignees)
        {
            List<Assignee> lstUniqueAssignees = new List<Assignee>();
            if (assignees.Count > 2)
            {
                if (assignees[0].id == assignees[1].id)
                {
                    if (assignees[0].id == assignees[2].id)
                    {
                        lstUniqueAssignees.Add(new Assignee { id = assignees[0].id, type = assignees[0].type + "," + assignees[1].type + "," + assignees[2].type });
                    }
                    else
                    {
                        lstUniqueAssignees.Add(new Assignee { id = assignees[0].id, type = assignees[0].type + "," + assignees[1].type });
                        lstUniqueAssignees.Add(new Assignee { id = assignees[2].id, type = assignees[2].type });
                    }
                }
                else
                {
                    if (assignees[0].id == assignees[2].id)
                    {
                        lstUniqueAssignees.Add(new Assignee { id = assignees[0].id, type = assignees[0].type + "," + assignees[2].type });
                        lstUniqueAssignees.Add(new Assignee { id = assignees[1].id, type = assignees[1].type });
                    }
                    else if (assignees[1].id == assignees[2].id)
                    {
                        lstUniqueAssignees.Add(new Assignee { id = assignees[0].id, type = assignees[0].type });
                        lstUniqueAssignees.Add(new Assignee { id = assignees[1].id, type = assignees[1].type + "," + assignees[2].type });
                    }
                    else
                    {
                        lstUniqueAssignees = assignees;
                    }
                }
            }
            else if (assignees.Count > 1)
            {
                if (assignees[0].id == assignees[1].id) lstUniqueAssignees.Add(new Assignee { id = assignees[0].id, type = assignees[0].type + "," + assignees[1].type });
                else
                {
                    lstUniqueAssignees.Add(new Assignee { id = assignees[0].id, type = assignees[0].type });
                    lstUniqueAssignees.Add(new Assignee { id = assignees[1].id, type = assignees[1].type });
                }
            }
            else
            {
                lstUniqueAssignees = assignees;
            }
            return lstUniqueAssignees;
        }

        private string GetProcessID()
        {
            string xPidNo = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'PID-PS' and year_thai = " + (DateTime.Now.Year + 543).ToString();
            DataTable dt = db.ExecSql_DataTable(sql, dbConnectionString);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'PID-PS' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                db.ExecNonQuery(sql, dbConnectionString);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('PID-PS', " + xRunno + ", " + (DateTime.Now.Year + 543).ToString() + ")  ";
                db.ExecNonQuery(sql, dbConnectionString);
            }
            xPidNo = "PID-PS-" + (DateTime.Now.Year + 543).ToString() + xRunno.ToString("000000");
            return xPidNo;
        }

        private string getDpNo()
        {
            string xWAANo = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'DP' and year_thai = " + (DateTime.Now.Year + 543).ToString();
            DataTable dt = db.ExecSql_DataTable(sql, dbConnectionString);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'DP' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                db.ExecNonQuery(sql, dbConnectionString);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('DP', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                db.ExecNonQuery(sql, dbConnectionString);
            }
            xWAANo = "DP-" + xRunno.ToString("0000") + " /" + (DateTime.Now.Year + 543).ToString();
            return xWAANo;
        }

        private string[] GetRoutingApproval(string strRouteApproval)
        {
            return strRouteApproval.Split(new char[] { ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
        }

        public DataTable GetPsRequestByProcessId_BK(String strProcessId)
        {
            DataTable dt = new DataTable();
            string strSql = "SELECT * from wf_ps_request where  process_id = '" + strProcessId + "'";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        public DataTable GetPsRequestByProcessId(String strProcessId)
        {
            DataTable dt = new DataTable();
            string strSql = @" select d.process_id,d.apv_order,d.ps_reqno,d.emp_id,m.note,m.activity_id,m.job_revision,m.job_no,m.bid_no,m.bid_revision,
                                d.emp_name,d.emp_position,d.emp_position_full,m.doc_node_id,
                                d.emp_apvdate,d.apv_status,m.wf_processid,m.ps_subject
                                from wf_ps_request_detail d
                                left join wf_ps_request m
                                on m.process_id = d.process_id
                            where d.apv_order = 1 and  d.process_id = '" + strProcessId + "'";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        public DataTable GetApproverWf(string[] arrApprover)
        {
            DataTable dt = new DataTable();
            if (arrApprover.Length > 0)
            {
                string strPositionCon = string.Empty;
                foreach (string strApprover in arrApprover)
                {
                    strPositionCon += strPositionCon == string.Empty ? "'" + strApprover + "'" : ",'" + strApprover + "'";
                }

                string strSql = @"SELECT DISTINCT PERNR, mc_short
                                    FROM m_syn_employee
                                    where mc_short in (" + strPositionCon + ")";
                zdb._getDT(strSql, commonDB);
            }

            return dt;
        }

        #endregion

        #region start wf Job

        public string StartWf(List<clsJobWf> lstWfObj, List<Assignee> lstUniqueAssignee)
        {
            string strErrorMsg = string.Empty;
            // get [] obj job dashboard submit
            string strJobNo, strJobRevision, strActivityId, strActivityName, strAssigneeDrw, strAssigneeEquip, strAssigneeDoc, strApprvRoute, strEmailNoti, strUpdateBy, strUpdateDate, strAssignType;
            DataTable dtJobInfo;
            // DataTable dtApprover;
            WfPsRequest objPfPs;
            foreach (clsJobWf objInlst in lstWfObj)
            {
                objPfPs = new WfPsRequest();
                strJobNo = strJobRevision = strActivityId = strActivityName = strAssigneeDrw = strAssigneeEquip = strAssigneeDoc = strApprvRoute = strUpdateBy = strAssignType = strUpdateDate = strEmailNoti = string.Empty;
                dtJobInfo = GetJobDetail(objInlst);
                if (dtJobInfo.Rows.Count > 0)
                {
                    strActivityId = dtJobInfo.Rows[0]["activity_id"].ToString();
                    strActivityName = dtJobInfo.Rows[0]["activity_name"].ToString();
                    strJobNo = dtJobInfo.Rows[0]["job_no"].ToString();
                    strJobRevision = dtJobInfo.Rows[0]["job_revision"].ToString();
                    //strAssigneeDrw = dtJobInfo.Rows[0]["assignee_drawing"].ToString();
                    //strAssigneeEquip = dtJobInfo.Rows[0]["assignee_equip"].ToString();
                    //strAssigneeDoc = dtJobInfo.Rows[0]["assignee_tech_doc"].ToString();
                    strUpdateBy = dtJobInfo.Rows[0]["updated_by"].ToString();
                    strUpdateDate = dtJobInfo.Rows[0]["updated_datetime"].ToString();
                    strApprvRoute = dtJobInfo.Rows[0]["approval_routing"].ToString();
                    strEmailNoti = dtJobInfo.Rows[0]["email_noti_id"].ToString();


                    //List<Assignee> lstAssignee = new List<Assignee>();
                    //if (!String.IsNullOrEmpty(strAssigneeDrw)) lstAssignee.Add(new Assignee { id = strAssigneeDrw, type = "DRW" });
                    //if (!String.IsNullOrEmpty(strAssigneeEquip)) lstAssignee.Add(new Assignee { id = strAssigneeEquip, type = "E" });
                    //if (!String.IsNullOrEmpty(strAssigneeDoc)) lstAssignee.Add(new Assignee { id = strAssigneeDoc, type = "DOC" });
                    //List<Assignee> lstUniqueAssignee = GetDistictAssignee(lstAssignee);

                    foreach (Assignee strAssignee in lstUniqueAssignee)
                    {
                        objPfPs = new WfPsRequest();
                        objPfPs.activity_id = strActivityId;
                        objPfPs.activity_name = strActivityName;
                        objPfPs.job_no = strJobNo;
                        objPfPs.job_revision = strJobRevision;
                        objPfPs.ps_reqno = getDpNo();//GetProcessID();
                        objPfPs.process_id = GetProcessID();
                        objPfPs.updated_by = strUpdateBy;
                        objPfPs.updated_datetime = DateTime.Parse(strUpdateDate);
                        objPfPs.approver_route = strApprvRoute;

                        GetProcessIdWorkflow(ref objPfPs, strAssignee);
                        // insert to customize table by call InsertWfRequest()
                        InsertWfRequest(objPfPs);
                        foreach (WfPsRequestDetails objDt in objPfPs.lstReqDetail)
                        {
                            InsertWfRequestDetail(objDt);
                        }
                        UpdateJobActivtyStatus("WF in progress", strJobNo, strActivityId, strJobRevision);
                    }
                }
                else
                {
                    strErrorMsg += "No job to start.";
                }
            }
            return strErrorMsg;
        }

        public void GetProcessIdWorkflow(ref WfPsRequest wfPsReq, Assignee assignee)
        {
            string result = string.Empty;

            ApplicationData[] appDatas = zotFunctions.getWFStartAppData(strWfMapId, "");
            AttributeData wfAttr = new AttributeData();

            foreach (ApplicationData appData in appDatas)
            {
                if (appData.GetType().Name == "AttributeData")
                {
                    wfAttr = (AttributeData)appData;
                    break;
                }
            }
            if (wfAttr.Attributes != null)
            {
                Employee thisEmp = emp.getInFoByEmpID(assignee.id);
                string strDate = DateTime.Now.ToString();
                var xDate = DateTime.Parse(strDate);
                string strTitle = wfPsReq.job_no + " : " + wfPsReq.activity_name;

                zotFunctions.setwfAttrValue(ref wfAttr, "PID", wfPsReq.process_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "JOB_NO", wfPsReq.job_no);
                zotFunctions.setwfAttrValue(ref wfAttr, "JOB_REV", wfPsReq.job_revision);
                zotFunctions.setwfAttrValue(ref wfAttr, "ACTIVITY_ID", wfPsReq.activity_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "ACTIVITY_NAME", strTitle);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_BY", wfPsReq.updated_by);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TYPE", assignee.type);
                wfPsReq.ps_subject = wfPsReq.activity_name;
                wfPsReq.created_by = assignee.id;

                WfPsRequestDetails objDetail = new WfPsRequestDetails();
                objDetail.process_id = wfPsReq.process_id;
                objDetail.ps_reqno = wfPsReq.ps_reqno;
                objDetail.apv_order = 1;
                objDetail.emp_id = assignee.id;
                objDetail.emp_name = thisEmp.SNAME;
                objDetail.emp_position = thisEmp.MC_SHORT;
                objDetail.emp_position_full = thisEmp.POSITION;
                wfPsReq.lstReqDetail = new List<WfPsRequestDetails>();
                wfPsReq.lstReqDetail.Add(objDetail);

                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO", assignee.id);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", wfPsReq.updated_datetime.ToString("yyyy-MM-dd HH:mm:sss"));
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "");

                if (wfPsReq.approver_route != null)
                {
                    string[] arrApproverPostion = GetRoutingApproval(wfPsReq.approver_route);
                    if (arrApproverPostion.Length > 0)
                    {
                        for (int i = 0; i < arrApproverPostion.Length; i++)
                        {
                            thisEmp = new Employee();
                            thisEmp = emp.getInFoByEmpPosition(arrApproverPostion[i]);
                            objDetail = new WfPsRequestDetails();
                            objDetail.process_id = wfPsReq.process_id;
                            objDetail.ps_reqno = wfPsReq.ps_reqno;
                            objDetail.apv_order = wfPsReq.lstReqDetail.Count + 1;
                            objDetail.emp_id = thisEmp.PERNR;
                            objDetail.emp_name = thisEmp.SNAME;
                            objDetail.emp_position = thisEmp.MC_SHORT;
                            objDetail.emp_position_full = thisEmp.POSITION;
                            wfPsReq.lstReqDetail.Add(objDetail);

                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVER" + (i + 1).ToString() + "_ID", thisEmp.PERNR);
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE" + (i + 1).ToString() + "_DATE", "");
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE" + (i + 1).ToString() + "_STATUS", "");
                        }
                    }
                }

                zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "");

                var procInst = zotFunctions.startProcess("", strWfMapId, appDatas, wfPsReq.ps_reqno + " - " + DateTime.Now.ToString());
                result = procInst.ProcessID.ToString();
                wfPsReq.wf_processid = int.Parse(result);
                wfPsReq.wf_subprocessid = int.Parse(result);

            }
        }

        private bool IsDependency(string strJobRevision, string strJobNo, string strActivityId)
        {
            bool isDepend = false;
            DataTable dt = new DataTable();
            string strSql = @"SELECT job_no
                              ,job_revision
                              ,activity_id
                              ,next_activity_id
                              ,approval_routing
                              ,status
                              ,is_select
                          FROM ps_t_job_dashboard
                        WHERE  job_revision = " + strJobRevision + @"
                        AND job_no = '" + strJobNo + @"'
                        AND next_activity_id like '%" + strActivityId + "%' " +
                        "AND status != 'complete'";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            if (dt.Rows.Count > 0)
            {
                isDepend = true;
            }

            return isDepend;
        }

        private DataTable GetJobDetail(clsJobWf objJob)
        {
            DataTable dt = new DataTable();

            string strSql = @"SELECT tj.job_no
                              ,tj.job_revision
                              ,tj.depart_position_name
                              ,tj.section_position_name
                              ,tj.activity_id
							  ,aj.activity_name
                              ,tj.req_drawing
                              ,tj.assignee_drawing
                              ,tj.req_equip
                              ,tj.assignee_equip
                              ,tj.req_tech_doc
                              ,tj.assignee_tech_doc
                              ,tj.approval_routing
                              ,tj.next_activity_id
                              ,tj.email_noti_id
                              ,tj.created_by
                              ,tj.created_datetime
                              ,tj.updated_by
                              ,tj.updated_datetime
                              ,status
                              ,is_select
							FROM ps_t_job_dashboard tj
							left join ps_m_activity_job aj on tj.activity_id = aj.activity_id
                          where  UPPER(tj.status) = 'ASSIGNED' ";
            strSql += " and tj.job_revision = " + objJob.iJobRevision.ToString();
            strSql += " and tj.activity_id = '" + objJob.strActivityId + "' ";
            strSql += " and tj.job_no = '" + objJob.strJobNo + "' ";


            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public void UpdateJobActivtyStatus(string strStatus, string strJobId, string strActivityId, string strJobRevision)
        {
            string strSql = "UPDATE ps_t_job_dashboard SET status  = '" + strStatus + "' " +
                            "WHERE job_no  = '" + strJobId + "' and job_revision  = " + strJobRevision + " and activity_id  = '" + strActivityId + "'";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void EmpSubmitWf_BK(string strProcessID, string strWorkId, string strSubWorkId, string strTaskId)
        {
            try
            {
                DataTable dt = GetPsRequestByProcessId(strProcessID);
                ApplicationData[] appDatas;
                appDatas = zotFunctions.getWFAppDatas(strWorkId, strSubWorkId);
                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                if (wfAttr != null)
                {
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "SUBMIT");
                    zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Assignee submit");
                    int iApproverCount = GetJobApproverRoutingCount(dt.Rows[0]["job_no"].ToString(), dt.Rows[0]["job_revision"].ToString(), dt.Rows[0]["activity_id"].ToString());
                    if (iApproverCount > 1)
                        zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "REVIEW");
                    else { }
                    zotFunctions.completeWorkItem(dt.Rows[0]["requestor_id"].ToString(), strWorkId, strSubWorkId, strTaskId, appDatas);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        public void EmpSubmitWf(string strProcessID, string strWorkId, string strSubWorkId, string strTaskId)
        {
            try
            {
                DataTable dt = GetPsRequestByProcessId(strProcessID);
                ApplicationData[] appDatas;
                appDatas = zotFunctions.getWFAppDatas(strWorkId, strSubWorkId);
                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                if (wfAttr != null)
                {
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "SUBMIT");
                    zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Assignee submit");
                    int iApproverCount = GetJobApproverRoutingCount(dt.Rows[0]["job_no"].ToString(), dt.Rows[0]["job_revision"].ToString(), dt.Rows[0]["activity_id"].ToString());
                    if (iApproverCount >= 1)
                        zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER1");
                    else { }
                    zotFunctions.completeWorkItem(dt.Rows[0]["emp_id"].ToString(), strWorkId, strSubWorkId, strTaskId, appDatas);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        public byte[] GetMemoByPsRequest_BK(string strJobNo, string strJobRevision, string strWfProcessId)
        {
            string strSql = @"select * from wf_ps_request
                                where job_no = '" + strJobNo + @"'
                                and job_revision = " + strJobRevision + @"
                                and process_id = '" + strWfProcessId + "'";

            DataTable dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            memoDocClass memo = new memoDocClass();
            byte[] byteResult = memo.createAttachmentByte(dt.Rows[0]["ps_reqno"].ToString(), dt.Rows[0]["ps_reqno"].ToString(),
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", dt.Rows[0]["requestor_name"].ToString(),
               dt.Rows[0]["requestor_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"),
               "", "", "", "", dt.Rows[0]["reviewer_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), new string[] { "" },
              dt.Rows[0]["ps_subject"].ToString(), "", new string[] { "Price schedule" }, new string[] { "paragraph1", "paragraph2" },
                                "cc", true);
            return byteResult;
        }

        public byte[] GetMemoByPsRequest(string strJobNo, string strJobRevision, string strWfProcessId)
        {
            string strSql = @"select d.process_id,d.apv_order,d.ps_reqno,d.emp_id,m.note,m.activity_id,m.job_revision,m.job_no,
                                d.emp_name,d.emp_position,d.emp_position_full,
                                d.emp_apvdate,d.apv_status,m.wf_processid,m.ps_subject
                                from wf_ps_request_detail d
                                left join wf_ps_request m
                                on m.process_id = d.process_id
                                where m.job_no = '" + strJobNo + @"'
                                and m.job_revision = " + strJobRevision + @"
                                and m.process_id = '" + strWfProcessId + @"'
                                order by d.apv_order asc";

            DataTable dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            memoDocClass memo = new memoDocClass();
            byte[] byteResult = memo.createAttachmentByte(dt.Rows[0]["ps_reqno"].ToString(), dt.Rows[0]["ps_reqno"].ToString(),
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", dt.Rows[0]["emp_name"].ToString(),
               dt.Rows[0]["emp_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"),
               "", "", "", "", dt.Rows[dt.Rows.Count-1]["emp_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), new string[] { "" },
              dt.Rows[0]["ps_subject"].ToString(), "", new string[] { "Price schedule" }, new string[] { "paragraph1", "paragraph2" },
                                "cc", true);
            return byteResult;
        }


        #endregion

        #region start wf Bid
        //kie //Ging
        public string StartWfBid(List<clsJobWf> lstWfObj, List<Assignee> lstUniqueAssignee)
        {
            string strErrorMsg = string.Empty;
            string strBidNo, strBidRevision, strActivityId, strActivityName, strAssigneeDrw, strAssigneeEquip, strAssigneeDoc, strApprvRoute, strEmailNoti, strUpdateBy, strUpdateDate, strAssignType;
            DataTable dtBidInfo;
            WfPsRequest objPfPs;
            foreach (clsJobWf objInlst in lstWfObj)
            {
                objPfPs = new WfPsRequest();
                strBidNo = strBidRevision = strActivityId = strActivityName = strAssigneeDrw = strAssigneeEquip = strAssigneeDoc = strApprvRoute = strUpdateBy = strAssignType = strUpdateDate = strEmailNoti = string.Empty;
                dtBidInfo = GetBidDetail(objInlst);
                if (dtBidInfo.Rows.Count > 0)
                {
                    strActivityId = dtBidInfo.Rows[0]["activity_id"].ToString();
                    strActivityName = dtBidInfo.Rows[0]["activity_name"].ToString();
                    strBidNo = dtBidInfo.Rows[0]["bid_no"].ToString();
                    strBidRevision = dtBidInfo.Rows[0]["bid_revision"].ToString();
                    //strAssigneeDrw = dtBidInfo.Rows[0]["assignee_drawing"].ToString();
                    //strAssigneeEquip = dtBidInfo.Rows[0]["assignee_equip"].ToString();
                    //strAssigneeDoc = dtBidInfo.Rows[0]["assignee_tech_doc"].ToString();
                    strUpdateBy = dtBidInfo.Rows[0]["updated_by"].ToString();
                    strUpdateDate = dtBidInfo.Rows[0]["updated_datetime"].ToString();
                    strApprvRoute = dtBidInfo.Rows[0]["approval_routing"].ToString();
                    strEmailNoti = dtBidInfo.Rows[0]["email_noti_id"].ToString();
                    string[] arrApprover = GetRoutingApproval(strApprvRoute);

                    //List<Assignee> lstAssignee = new List<Assignee>();
                    //if (!String.IsNullOrEmpty(strAssigneeDrw)) lstAssignee.Add(new Assignee { id = strAssigneeDrw, type = "DRW" });
                    //if (!String.IsNullOrEmpty(strAssigneeEquip)) lstAssignee.Add(new Assignee { id = strAssigneeEquip, type = "E" });
                    //if (!String.IsNullOrEmpty(strAssigneeDoc)) lstAssignee.Add(new Assignee { id = strAssigneeDoc, type = "DOC" });
                    //List<Assignee> lstUniqueAssignee = GetDistictAssignee(lstAssignee);

                    foreach (Assignee strAssignee in lstUniqueAssignee)
                    {
                        objPfPs = new WfPsRequest();
                        objPfPs.activity_id = strActivityId;
                        objPfPs.activity_name = strActivityName;
                        objPfPs.bid_no = strBidNo;
                        objPfPs.bid_revision = strBidRevision;
                        objPfPs.ps_reqno = getDpNo();
                        objPfPs.process_id = GetProcessID();
                        objPfPs.updated_by = strUpdateBy;
                        objPfPs.updated_datetime = DateTime.Parse(strUpdateDate);
                        objPfPs.approver_route = strApprvRoute;

                        GetProcessIdWorkflowBid(ref objPfPs, strAssignee);
                        // insert to customize table by call InsertWfRequest()
                        InsertWfRequest(objPfPs);
                        foreach (WfPsRequestDetails objDt in objPfPs.lstReqDetail)
                        {
                            InsertWfRequestDetail(objDt);
                        }
                        UpdateBidActivtyStatus("WF in progress", strBidNo, strActivityId, strBidRevision);
                    }
                }
                else
                {
                    strErrorMsg += "No bid to start.";
                }
            }
            return strErrorMsg;
        }
        //kie
        public string GetProcessIdWorkflowBid_BK(ref WfPsRequest wfPsReq, Assignee assignee)
        {
            string result = string.Empty;

            ApplicationData[] appDatas = zotFunctions.getWFStartAppData(strWfMapId, "");
            Employee thisEmp = emp.getInFoByEmpID(assignee.id);
            AttributeData wfAttr = new AttributeData();
            foreach (ApplicationData appData in appDatas)
            {
                if (appData.GetType().Name == "AttributeData")
                {
                    wfAttr = (AttributeData)appData;
                    break;
                }
            }
            if (wfAttr.Attributes != null)
            {

                string strDate = DateTime.Now.ToString();
                var xDate = DateTime.Parse(strDate);
                // wfAttr.Attributes;
                zotFunctions.setwfAttrValue(ref wfAttr, "PID", GetProcessID());
                zotFunctions.setwfAttrValue(ref wfAttr, "BID_NO", wfPsReq.bid_no);
                zotFunctions.setwfAttrValue(ref wfAttr, "BID_REV", wfPsReq.bid_revision);
                zotFunctions.setwfAttrValue(ref wfAttr, "ACTIVITY_ID", wfPsReq.activity_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "ACTIVITY_NAME", wfPsReq.activity_name);
                wfPsReq.ps_subject = wfPsReq.activity_name;
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_BY", wfPsReq.updated_by);
                wfPsReq.created_by = assignee.id;
                //wfPsReq.requestor_id = assignee.id;
                //wfPsReq.requestor_name = thisEmp.SNAME;
                //wfPsReq.requestor_position = thisEmp.POSITION;
                //wfPsReq.requestor_position_full = "";
                //wfPsReq.requestor_status = "";
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TYPE", assignee.type);
                //zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", wfPsReq.updated_datetime.ToString("yyyy-MM-dd HH:mm:sss"));

                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO", assignee.id);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "");

                zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW_BY", thisEmp.SECTION_HEAD_ID);
                zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW_DATE", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW_STATUS", "");
                //wfPsReq.reviewer_id = thisEmp.SECTION_HEAD_ID;
                //wfPsReq.reviewer_name = thisEmp.SECTION_HEAD_NAME;
                //wfPsReq.reviewer_position = thisEmp.SECTION_HEAD_POSITION;
                //wfPsReq.reviewer_position_full = thisEmp.SECTION_HEAD_POSITION_FULL;


                zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE_BY", thisEmp.MANAGER_ID);
                zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE_DATE", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE_STATUS", "");
                //wfPsReq.approver_id = thisEmp.MANAGER_ID;
                //wfPsReq.approver_name = thisEmp.MANAGER_NAME;
                //wfPsReq.approver_position = thisEmp.MANAGER_POSITION;
                //wfPsReq.approver_position_full = thisEmp.MANAGER_POSITION_FULL;


                zotFunctions.setwfAttrValue(ref wfAttr, "PM_ID", thisEmp.AVP_ID);
                zotFunctions.setwfAttrValue(ref wfAttr, "PM_DATE", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "PM_STATUS", "");
                //wfPsReq.pm_id = thisEmp.AVP_ID;
                //wfPsReq.pm_name = thisEmp.AVP_NAME;
                //wfPsReq.pm_position = thisEmp.AVP_POSITION;
                //wfPsReq.pm_position_full = thisEmp.AVP_POSITION_FULL;



                zotFunctions.setwfAttrValue(ref wfAttr, "ASST_DIRECTOR_ID", thisEmp.VP_ID);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASST_DIRECTOR_DATE", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "ASST_DIRECTOR_STATUS", "");
                //wfPsReq.assistdirector_id = thisEmp.VP_ID;
                //wfPsReq.assistdirector_name = thisEmp.VP_NAME;
                //wfPsReq.assistdirector_position = thisEmp.VP_POSITION;
                //wfPsReq.assistdirector_position_full = thisEmp.VP_POSITION_FULL;


                zotFunctions.setwfAttrValue(ref wfAttr, "DIRECTOR_ID", thisEmp.DIRECTOR_ID);
                zotFunctions.setwfAttrValue(ref wfAttr, "DIRECTOR_DATE", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "DIRECTOR_STATUS", "");
                //wfPsReq.director_id = thisEmp.DIRECTOR_ID;
                //wfPsReq.director_name = thisEmp.DIRECTOR_NAME;
                //wfPsReq.director_position = thisEmp.DIRECTOR_POSITION;
                //wfPsReq.director_position_full = thisEmp.DIRECTOR_POSITION_FULL;

                zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "");

                // var procInst = zotFunctions.startProcess(assignee.id, strWfMapId, appDatas, "Title : Job"); otadmin @otds.admin
                //var procInst = zotFunctions.startProcess(wfPsReq.updated_by, strWfMapId, appDatas, "Title : Job");
                var procInst = zotFunctions.startProcess("", strWfMapId, appDatas, wfPsReq.ps_reqno + " - " + DateTime.Now.ToString());
                result = procInst.ProcessID.ToString();
                wfPsReq.wf_processid = int.Parse(result);
                wfPsReq.wf_subprocessid = int.Parse(result);

            }

            return result;
        }
        //Ging

        public void GetProcessIdWorkflowBid(ref WfPsRequest wfPsReq, Assignee assignee)
        {
            string result = string.Empty;

            ApplicationData[] appDatas = zotFunctions.getWFStartAppData(strWfBidMapId, "");
            AttributeData wfAttr = new AttributeData();

            foreach (ApplicationData appData in appDatas)
            {
                if (appData.GetType().Name == "AttributeData")
                {
                    wfAttr = (AttributeData)appData;
                    break;
                }
            }
            if (wfAttr.Attributes != null)
            {
                Employee thisEmp = emp.getInFoByEmpID(assignee.id);
                string strDate = DateTime.Now.ToString();
                var xDate = DateTime.Parse(strDate);
                string strTitle = wfPsReq.bid_no + " : " + wfPsReq.activity_name;

                zotFunctions.setwfAttrValue(ref wfAttr, "PID", wfPsReq.process_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "BID_NO", wfPsReq.bid_no);
                zotFunctions.setwfAttrValue(ref wfAttr, "BID_REV", wfPsReq.bid_revision);
                zotFunctions.setwfAttrValue(ref wfAttr, "ACTIVITY_ID", wfPsReq.activity_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "ACTIVITY_NAME", strTitle);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_BY", wfPsReq.updated_by);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TYPE", assignee.type);
                wfPsReq.ps_subject = wfPsReq.activity_name;
                wfPsReq.created_by = assignee.id;

                WfPsRequestDetails objDetail = new WfPsRequestDetails();
                objDetail.process_id = wfPsReq.process_id;
                objDetail.ps_reqno = wfPsReq.ps_reqno;
                objDetail.apv_order = 1;
                objDetail.emp_id = assignee.id;
                objDetail.emp_name = thisEmp.SNAME;
                objDetail.emp_position = thisEmp.MC_SHORT;
                objDetail.emp_position_full = thisEmp.POSITION;
                wfPsReq.lstReqDetail = new List<WfPsRequestDetails>();
                wfPsReq.lstReqDetail.Add(objDetail);

                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO", assignee.id);
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", wfPsReq.updated_datetime.ToString("yyyy-MM-dd HH:mm:sss"));
                zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "");

                if (wfPsReq.approver_route != null)
                {
                    string[] arrApproverPostion = GetRoutingApproval(wfPsReq.approver_route);
                    if (arrApproverPostion.Length > 0)
                    {
                        for (int i = 0; i < arrApproverPostion.Length; i++)
                        {
                            thisEmp = new Employee();
                            thisEmp = emp.getInFoByEmpPosition(arrApproverPostion[i]);
                            objDetail = new WfPsRequestDetails();
                            objDetail.process_id = wfPsReq.process_id;
                            objDetail.ps_reqno = wfPsReq.ps_reqno;
                            objDetail.apv_order = wfPsReq.lstReqDetail.Count + 1;
                            objDetail.emp_id = thisEmp.PERNR;
                            objDetail.emp_name = thisEmp.SNAME;
                            objDetail.emp_position = thisEmp.MC_SHORT;
                            objDetail.emp_position_full = thisEmp.POSITION;
                            wfPsReq.lstReqDetail.Add(objDetail);

                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVER" + (i + 1).ToString() + "_ID", thisEmp.PERNR);
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE" + (i + 1).ToString() + "_DATE", "");
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE" + (i + 1).ToString() + "_STATUS", "");
                        }
                    }
                }

                zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "");
                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "");

                var procInst = zotFunctions.startProcess("", strWfBidMapId, appDatas, wfPsReq.ps_reqno + " - " + DateTime.Now.ToString());
                result = procInst.ProcessID.ToString();
                wfPsReq.wf_processid = int.Parse(result);
                wfPsReq.wf_subprocessid = int.Parse(result);

            }
        }
        //kie
        private bool IsDependencyBid(string strBidRevision, string strBidNo, string strActivityId)
        {
            bool isDepend = false;
            DataTable dt = new DataTable();
            string strSql = @"SELECT bid_no
                              ,bid_revision
                              ,activity_id
                              ,next_activity_id
                              ,approval_routing
                              ,status
                              ,is_select
                          FROM ps_t_bid_dashboard
                        WHERE  bid_revision = " + strBidRevision + @"
                        AND bid_no = '" + strBidNo + @"'
                        AND next_activity_id like '%" + strActivityId + "%' " +
                        "AND status != 'complete'";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            if (dt.Rows.Count > 0)
            {
                isDepend = true;
            }

            return isDepend;
        }
        //kie
        private DataTable GetBidDetail(clsJobWf objJob)
        {
            DataTable dt = new DataTable();

            string strSql = @"		SELECT tj.bid_no
                              ,tj.bid_revision
                              ,tj.depart_position_name
                              ,tj.section_position_name
                              ,tj.activity_id
							  ,aj.activity_name
                              ,tj.req_drawing
                              ,tj.assignee_drawing
                              ,tj.req_equip
                              ,tj.assignee_equip
                              ,tj.req_tech_doc
                              ,tj.assignee_tech_doc
                              ,tj.approval_routing
                              ,tj.next_activity_id
                              ,tj.email_noti_id
                              ,tj.created_by
                              ,tj.created_datetime
                              ,tj.updated_by
                              ,tj.updated_datetime
                              ,status
                              ,is_select
							FROM ps_t_bid_dashboard tj
							left join ps_m_activity_bid aj on tj.activity_id = aj.activity_id
                          where  UPPER(tj.status) = 'ASSIGNED' ";
            strSql += " and tj.bid_revision = " + objJob.iBidRevision.ToString();
            strSql += " and tj.activity_id = '" + objJob.strActivityIdBid + "' ";
            strSql += " and tj.bid_no = '" + objJob.strBidNo + "' ";


            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        //kie
        private void UpdateBidActivtyStatus(string strStatus, string strBidId, string strActivityId, string strBidRevision)
        {
            string strSql = "UPDATE ps_t_bid_dashboard SET status  = 'WF in progress' " +
                            "WHERE bid_no  = '" + strBidId + "' and bid_revision  = " + strBidRevision + " and activity_id  = '" + strActivityId + "'";
            db.ExecNonQuery(strSql, dbConnectionString);
        }
        //kie
        public void EmpSubmitWfbid_BK(string strProcessID, string strWorkId, string strSubWorkId, string strTaskId)
        {
            try
            {

                DataTable dt = GetPsRequestByProcessId(strProcessID);
                ApplicationData[] appDatas;
                appDatas = zotFunctions.getWFAppDatas(strWorkId, strSubWorkId);
                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                if (wfAttr != null)
                {
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "SUBMIT");
                    zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Assignee submit");
                    int iApproverCount = GetBidApproverRoutingCount(dt.Rows[0]["bid_no"].ToString(), dt.Rows[0]["bid_revision"].ToString(), dt.Rows[0]["activity_id"].ToString());
                    if (iApproverCount > 1)
                        zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "REVIEW");
                    else { }
                    zotFunctions.completeWorkItem(dt.Rows[0]["requestor_id"].ToString(), strWorkId, strSubWorkId, strTaskId, appDatas);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        //ging
        public void EmpSubmitWfbid(string strProcessID, string strWorkId, string strSubWorkId, string strTaskId)
        {
            try
            {
                DataTable dt = GetPsRequestByProcessId(strProcessID);
                ApplicationData[] appDatas;
                appDatas = zotFunctions.getWFAppDatas(strWorkId, strSubWorkId);
                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                if (wfAttr != null)
                {
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                    zotFunctions.setwfAttrValue(ref wfAttr, "ASSIGN_TO_STATUS", "SUBMIT");
                    zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Assignee submit");
                    int iApproverCount = GetBidApproverRoutingCount(dt.Rows[0]["bid_no"].ToString(), dt.Rows[0]["bid_revision"].ToString(), dt.Rows[0]["activity_id"].ToString());
                    if (iApproverCount >= 1)
                        zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER1");
                    else { }
                    zotFunctions.completeWorkItem(dt.Rows[0]["emp_id"].ToString(), strWorkId, strSubWorkId, strTaskId, appDatas);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        //ging
        public void DocReview_Approver(string strProcessID, string strWorkId, string strSubWorkId, string strTaskId, string strStatus, string login)
        {
            try
            {
                DataTable dt = GetPsRequestByProcessId(strProcessID);
                ApplicationData[] appDatas;
                appDatas = zotFunctions.getWFAppDatas(strWorkId, strSubWorkId);
                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                } 
                if (wfAttr != null)
                {
                    string strNext = zotFunctions.getwfAttrValue(ref wfAttr, "NEXT_STEP");
                    if (strNext != "APPROVER2")
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_STATUS", strStatus);
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Approver submit");
                        int iApproverCount = GetBidApproverRoutingCount(dt.Rows[0]["bid_no"].ToString(), dt.Rows[0]["bid_revision"].ToString(), dt.Rows[0]["activity_id"].ToString());
                        if (iApproverCount >= 1)
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER2");
                        else
                        {
                            if (strStatus == "APPROVED")
                            {
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                                // UpdateP6Bid(dt.Rows[0]["bid_no"].ToString(), dt.Rows[0]["activity_id"].ToString());
                            }
                        }
                    }
                    else
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE2_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE2_STATUS", strStatus);
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Approver Arrrove");
                        zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                        UpdateP6Bid(dt.Rows[0]["bid_no"].ToString(), dt.Rows[0]["activity_id"].ToString());
                    }
                    //if (strStatus == "APPROVED")
                    //    zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                    //else { }
                    zotFunctions.completeWorkItem(login, strWorkId, strSubWorkId, strTaskId, appDatas);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        //kie
        //public byte[] GetMemoByPsRequestbid(string strBidNo, string strBidRevision, string strWfProcessId)
        //{
        //    string strSql = @"select * from wf_ps_request
        //                        where bid_no = '" + strBidNo + @"'
        //                        and bid_revision = " + strBidRevision + @"
        //                        and process_id = '" + strWfProcessId + "'";

        //    DataTable dt = db.ExecSql_DataTable(strSql, dbConnectionString);

        //    memoDocClass memo = new memoDocClass();
        //    byte[] byteResult = memo.createAttachmentByte(dt.Rows[0]["ps_reqno"].ToString(), dt.Rows[0]["ps_reqno"].ToString(),
        //        DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", dt.Rows[0]["requestor_name"].ToString(),
        //       dt.Rows[0]["requestor_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"),
        //       "", "", "", "", dt.Rows[0]["reviewer_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), new string[] { "" },
        //      dt.Rows[0]["ps_subject"].ToString(), "", new string[] { "Price schedule" }, new string[] { "paragraph1", "paragraph2" },
        //                        "cc", true);
        //    return byteResult;
        //}

        public byte[] GetMemoByPsRequestbid(string strBidNo, string strBidRevision, string strWfProcessId)
        {
            string strSql = @"select d.process_id,d.apv_order,d.ps_reqno,d.emp_id,m.note,m.activity_id,m.bid_revision,m.bid_no,
                                d.emp_name,d.emp_position,d.emp_position_full,
                                d.emp_apvdate,d.apv_status,m.wf_processid,m.ps_subject
                                from wf_ps_request_detail d
                                left join wf_ps_request m
                                on m.process_id = d.process_id
                                where m.bid_no = '" + strBidNo + @"'
                                and m.bid_revision = " + strBidRevision + @"
                                and m.process_id = '" + strWfProcessId + "' order by d.apv_order asc";

            DataTable dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            memoDocClass memo = new memoDocClass();
            byte[] byteResult = memo.createAttachmentByte(dt.Rows[0]["ps_reqno"].ToString(), dt.Rows[0]["ps_reqno"].ToString(),
                DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", dt.Rows[0]["emp_name"].ToString(),
               dt.Rows[0]["emp_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"),
               "", "", "", "", dt.Rows[dt.Rows.Count - 1]["emp_position"].ToString(), "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), new string[] { "" },
              dt.Rows[0]["ps_subject"].ToString(), "", new string[] { "Price schedule" }, new string[] { "paragraph1", "paragraph2" },
                                "cc", true);
            return byteResult;
        }
        #endregion

        private void UpdateP6Bid(string strBidNo, string strActivityId)
        {
            // ------------------- update P6 -------------------
            try
            {
                p6Class.AuthP6();
                p6Class p6 = new p6Class();
                DataTable dt = p6.getTaskBid_ByActivityECM(strBidNo, strActivityId);
                if (dt.Rows.Count > 0)
                {
                    string strTaskId = dt.Rows[0]["task_id"].ToString();

                    DateTime actualStartDate = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    p6Class.UpdateActualStartDate(int.Parse(strTaskId), actualStartDate);
                    p6Class.UpdatetaskStatus(int.Parse(strTaskId), "Completed");
                    p6Class.ActionUDFStaffLastupdate(int.Parse(strTaskId), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    string strPrefixMainAc = strActivityId.Substring(0, 1);
                    string staMainActivity = strActivityId.Substring(0, 1) + "0000";
                    DataTable dtMain = p6.getTaskBid_ByActivityECM(strBidNo, staMainActivity);
                    if (dtMain.Rows.Count > 0)
                    {
                        string strTaskIdMain = dtMain.Rows[0]["task_id"].ToString();
                        string strSql = @"select (weight_factor*100) / (select sum(weight_factor)  
                                                     from ps_m_activity_job
                                                     where activity_id like '" + strPrefixMainAc + @"%') as percent_complete
                                                from ps_m_activity_job
                                                where activity_id = '" + strActivityId + "' and is_active = 1";
                        DataTable dtPercentage = db.ExecSql_DataTable(strSql, dbConnectionString);
                        if (dtPercentage.Rows.Count > 0)
                        {
                            p6Class.UpdatepercentActivity(int.Parse(strTaskIdMain), double.Parse(dtPercentage.Rows[0]["percent_complete"].ToString()));
                            p6Class.UpdatetaskStatus(int.Parse(strTaskIdMain), "InProgress");
                            p6Class.ActionUDFStaffLastupdate(int.Parse(strTaskIdMain), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                    }
                }
                p6Class.Logout();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            // ------------------- update P6 -------------------
        }
    }
    #region extended class
    public class Assignee
    {
        public Assignee() { }
        public string id { get; set; }
        public string type { get; set; }
    }

    public class WfPsRequest
    {
        public WfPsRequest() { }
        public string process_id { get; set; }
        public string ps_reqno { get; set; }
        public string job_no { get; set; }
        public string job_revision { get; set; }
        public string bid_no { get; set; }
        public string bid_revision { get; set; }
        public string activity_id { get; set; }
        public string activity_name { get; set; } // not map
        public string ps_subject { get; set; }
        public string project_id { get; set; }
        public string reference { get; set; }
        public string note { get; set; }
        public string current_step { get; set; }
        public string next_step { get; set; }
        public string form_status { get; set; }
        public string form_to { get; set; }
        public string form_cc { get; set; }
        public string created_by { get; set; }
        public DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public DateTime updated_datetime { get; set; }
        public int? wf_processid { get; set; }
        public int? wf_subprocessid { get; set; }
        public int? wf_taskid { get; set; }
        public string approver_route { get; set; } // not map
        public List<WfPsRequestDetails> lstReqDetail { get; set; }

    }

    public class WfPsRequestDetails
    {
        public WfPsRequestDetails() { }

        public string process_id { get; set; }
        public string ps_reqno { get; set; }
        public int? apv_order { get; set; }
        public string emp_id { get; set; }
        public string emp_name { get; set; }
        public string emp_position { get; set; }
        public string emp_position_full { get; set; }
        public DateTime emp_apvdate { get; set; }
        public string apv_status { get; set; }

    }
    #endregion
}
