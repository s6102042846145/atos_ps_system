﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace PS_Library
{
    public class clsMyTaskJob
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase db = new DbControllerBase();
        public clsMyTaskJob() { }

        public void insertMytaskEqDoc(string Dept, string Sect, string jobno, string jobrev, string bidno, string bidrev, string schname, string schno, string eq_code, string eq_desc,string login, string docid,string mode,string pid,int rowid)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                if (mode == "")
                {
                     sql = @"INSERT INTO ps_t_mytask_equip_doc_design
           ([job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_code]
           ,[doc_nodeid]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc]
            ,[process_id]
            ,[mytask_eq_id])
     VALUES
           ('" + jobno + @"'
           ,'" + jobrev + @"'
           ,'" + bidno + @"'
           ,'" + bidrev + @"'
           ,'" + schno + @"'
           ,'" + schname + @"'
           ,'" + Dept + @"'
           ,'" + Sect + @"'
           ,'" + eq_code + @"'
           ,'" + docid + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
           ,'" + eq_desc + @"'
            ,'"+pid+@"'
            ,'"+rowid+"')";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                else
                {
                    sql = @"select rowid from ps_t_mytask_equip_doc_design
where job_no = '"+jobno+"' and job_revision = '"+jobrev+@"'
and bid_no = '"+bidno+"' and bid_revision = '"+bidrev+@"'
and schedule_no = '"+schno+"' and schedule_name = '"+schname+ "' and equip_code='"+eq_code+"'";
                    dt = db.ExecSql_DataTable(sql,zetl4eisdb);
                    if (dt.Rows.Count==0)
                    {
                        sql = @"INSERT INTO ps_t_mytask_equip_doc_design
           ([job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_code]
           ,[doc_nodeid]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc]
            ,[process_id]
            ,[mytask_eq_id])
     VALUES
           ('" + jobno + @"'
           ,'" + jobrev + @"'
           ,'" + bidno + @"'
           ,'" + bidrev + @"'
           ,'" + schno + @"'
           ,'" + schname + @"'
           ,'" + Dept + @"'
           ,'" + Sect + @"'
           ,'" + eq_code + @"'
           ,'" + docid + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
           ,'" + eq_desc + @"'
            ,'"+pid+@"'
            ,'"+rowid+"')";
                        db.ExecNonQuery(sql, zetl4eisdb);
                    }
                    else
                    {
                        sql = @"UPDATE ps_t_mytask_equip_doc_design
   SET
       [doc_nodeid] = '"+docid+@"'
      ,[updated_by] = '"+login+ @"'
      ,[updated_datetime] = GETDATE()
       ,[mytask_eq_id] ='"+rowid+@"'
  WHERE job_no = '" + jobno + "' and job_revision = '" + jobrev + @"'
and bid_no = '" + bidno + "' and bid_revision = '" + bidrev + @"'
and schedule_no = '" + schno + "' and schedule_name = '" + schname + "' and equip_code='" + eq_code + "'";
                        db.ExecNonQuery(sql, zetl4eisdb);
                    }       
                }
                
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            
        }
        public DataTable getWaivTest(string eq_code)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select incoterm,factory_test,manual_test,routine_test,waive_test from ps_m_equipcode where equip_code='"+ eq_code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getListDoc(string eq_code,string dept,string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select doc_name from ps_m_doc_approve
where doc_nodeid in(select doc_nodeid from ps_m_equip_doc where equip_code ='"+ eq_code + @"') 
and doc_status='Active'
";//and depart_position_name='"+ dept + "' and section_position_name='"+ sect + "'
                dt = db.ExecSql_DataTable(sql,zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void getPeiceInsertToEQ(string eq_code,string jobno,string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select base_price from ps_t_bp_upload where equip_code='"+eq_code+"' and base_price is not null order by  updated_datetime desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"update ps_t_mytask_equip set  supply_equip_unit_price ='"+dt.Rows[0]["base_price"].ToString()+"' where job_no='"+ jobno + "' and job_revision='"+rev+"' and equip_code='"+eq_code+"'";
                    db.ExecNonQuery(sql,zetl4eisdb);                 
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void loadTemp(string bidno, string jobno, string part, string tempID,string dept)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"delete  from ps_t_mytask_part where bid_no='" + bidno + "' and job_no='" + jobno + "' and part_name='" + part + "' and depart_position_name='"+ dept + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
                sql = @"select equip_code from ps_t_mytask_equip where bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and depart_position_name='" + dept + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                string eq_code = "";
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (eq_code == "") eq_code = "'" + dr["equip_code"].ToString() + "'";
                        else eq_code += ",'" + dr["equip_code"].ToString() + "'";
                    }
                }
                sql = @"delete from ps_t_mytask_equip where bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and depart_position_name='" + dept + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
                if(eq_code!="")
                {
                    sql = @"delete from ps_t_mytask_equip_doc_design where bid_no='" + bidno + "' and job_no='" + jobno + "' and equip_code in (" + eq_code + ") and depart_position_name='" + dept + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                
                sql = @"INSERT INTO [ps_t_mytask_part]
           ([process_id]
           ,[activity_id]
           ,[step]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[project_id]
           ,[part_name]
           ,[part_description]
           ,[important]
           ,[part_remark]
           ,[is_new_schedule]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime])
 select[process_id]
           ,[activity_id]
           ,[step]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[project_id]
           ,[part_name]
           ,[part_description]
           ,[important]
           ,[part_remark]
           ,[is_new_schedule]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime] from ps_t_mytask_part_temp where mytemp_id = '" + tempID + "'";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = @"INSERT INTO [ps_t_mytask_equip_doc_design]
           ([process_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_code]
           ,[doc_nodeid]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc]
           ,[mytask_eq_id])
select [process_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_code]
           ,[doc_nodeid]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc]
           ,[mytask_eq_id] from ps_t_mytask_equip_doc_design_temp where mytemp_id= '" + tempID + "'";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = @"INSERT INTO [dbo].[ps_t_mytask_equip]
           ([step]
           ,[process_id]
           ,[activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[is_new_schedule]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_mas_grp]
           ,[equip_item_no]
           ,[equip_item_no_b]
           ,[equip_code]
           ,[equip_add_desc]
           ,[equip_standard_drawing]
           ,[equip_qty]
           ,[equip_unit]
           ,[equip_reserve_qty]
           ,[equip_confirm_qty]
           ,[equip_bid_qty]
           ,[equip_incoterm]
           ,[factory_test]
           ,[manual_test]
           ,[routine_test]
           ,[waive_test]
           ,[legend]
           ,[path_code]
           ,[part_code_b]
           ,[is_breakdown]
           ,[breakdown_tab]
           ,[currency]
           ,[proposed_price]
           ,[evaluation_price]
           ,[preference_status]
           ,[import_duty]
           ,[irrelevant]
           ,[turnkey_account_assign]
           ,[turnkey_cost_object]
           ,[turnkey_fund_center]
           ,[turnkey_gl]
           ,[supply_account_assign]
           ,[supply_cost_object]
           ,[supply_fund_center]
           ,[supply_gl]
           ,[supply_equip_unit_price]
           ,[supply_equip_amonut]
           ,[local_exwork_unit_price]
           ,[local_exwork_amonut]
           ,[local_tran_unit_price]
           ,[local_tran_amonut]
           ,[service_account_assign]
           ,[service_cost_object]
           ,[service_fund_center]
           ,[service_gl]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc])
  select [step]
           ,[process_id]
           ,[activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[is_new_schedule]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_mas_grp]
           ,[equip_item_no]
           ,[equip_item_no_b]
           ,[equip_code]
           ,[equip_add_desc]
           ,[equip_standard_drawing]
           ,[equip_qty]
           ,[equip_unit]
           ,[equip_reserve_qty]
           ,[equip_confirm_qty]
           ,[equip_bid_qty]
           ,[equip_incoterm]
           ,[factory_test]
           ,[manual_test]
           ,[routine_test]
           ,[waive_test]
           ,[legend]
           ,[path_code]
           ,[part_code_b]
           ,[is_breakdown]
           ,[breakdown_tab]
           ,[currency]
           ,[proposed_price]
           ,[evaluation_price]
           ,[preference_status]
           ,[import_duty]
           ,[irrelevant]
           ,[turnkey_account_assign]
           ,[turnkey_cost_object]
           ,[turnkey_fund_center]
           ,[turnkey_gl]
           ,[supply_account_assign]
           ,[supply_cost_object]
           ,[supply_fund_center]
           ,[supply_gl]
           ,[supply_equip_unit_price]
           ,[supply_equip_amonut]
           ,[local_exwork_unit_price]
           ,[local_exwork_amonut]
           ,[local_tran_unit_price]
           ,[local_tran_amonut]
           ,[service_account_assign]
           ,[service_cost_object]
           ,[service_fund_center]
           ,[service_gl]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc] from  ps_t_mytask_equip_temp where mytemp_id= '" + tempID + "'";
                db.ExecNonQuery(sql, zetl4eisdb);

                if(eq_code!="")
                {
                    sql = @"select mytask_equip.equip_code, equip_doc.doc_nodeid
,doc_approve.[doctype_code]
      ,doc_approve.[doc_revision_no]
      ,doc_approve.[doc_name]
      ,doc_approve.[doc_sheetno]
      ,doc_approve.[doc_desc]
	  ,mytask_equip.rowid
from ps_t_mytask_equip as mytask_equip
		inner join ps_m_equip_doc as equip_doc
			inner join ps_m_doc_approve as doc_approve
			on doc_approve.doc_nodeid = equip_doc.doc_nodeid
		on equip_doc.equip_code = mytask_equip.equip_code
		where mytask_equip.equip_code in (" + eq_code + ") and bid_no='" + bidno + "' and job_no='" + jobno + "'";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sql = @"UPDATE [ps_t_mytask_equip_doc_design]
   SET [mytask_eq_id] = '" + dr["rowid"].ToString() + @"'
   where bid_no='" + bidno + "' and job_no='" + jobno + "' and equip_code='" + dr["equip_code"].ToString() + "' and doc_nodeid='" + dr["doc_nodeid"].ToString() + "'";
                            db.ExecNonQuery(sql, zetl4eisdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public DataTable loadTempDDL(string dept,string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select mytemp_id,mytemp_name from ps_t_my_template where depart_position_name = '" + dept + "' order by mytemp_name asc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void updateTemp(int tmpID,string tmpName, addMytaskEquipJob obj, string bidno, string bidrev, string jobno, string jobrev, string part, string pid, string schNo, string schName, string dept, string sect, string login)
        {
            DataTable dt = new DataTable();
            try
            {
                int thisID = 0;
                string sql = @"delete from ps_t_my_template where mytemp_id='"+ tmpID + "'";
                db.ExecNonQuery(sql,zetl4eisdb);
                 sql = @"delete from ps_t_mytask_part_temp where mytemp_id='" + tmpID + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
                 sql = @"delete from ps_t_mytask_equip_doc_design_temp where mytemp_id='" + tmpID + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
                 sql = @"delete from ps_t_mytask_equip_temp where mytemp_id='" + tmpID + "'";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = @"INSERT INTO [ps_t_my_template]
           ([mytemp_name]
           ,[job_no]
           ,[job_revision]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[depart_position_name]
           ,[section_position_name])
     VALUES
           ('" + tmpName + @"'
           ,'" + jobno + @"'
           ,'" + jobrev + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
           ,'" + dept + @"'
           ,'" + sect + "')";
                db.ExecNonQuery(sql, zetl4eisdb);
                sql = @"select mytemp_id from ps_t_my_template where mytemp_name='" + tmpName + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0) thisID = Convert.ToInt32(dt.Rows[0]["mytemp_id"]);

                sql = @"INSERT INTO [ps_t_mytask_part_temp]
           ([mytemp_id]
            ,[activity_id]
           ,[step]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[part_name]
           ,[part_description]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
            ,[important]
            ,[process_id]
            ,[part_remark])
     VALUES
           ('" + thisID + @"'
           ,'" + obj.activity_id + @"'
           ,'" + obj.step + @"'
           ,'" + obj.job_no + @"'
           ,'" + obj.job_revision + @"'
           ,'" + obj.bid_no + @"'
           ,'" + obj.bid_revision + @"'
           ,'" + obj.schedule_no + @"'
           ,'" + obj.schedule_name + @"'
           ,'" + obj.depart_position_name + @"'
           ,'" + obj.section_position_name + @"'
           ,'" + obj.part_name + @"'
           ,'" + obj.part_description + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + @"'
           ,'" + obj.important + @"'
           ,'" + obj.process_id + @"'
           ,'" + obj.part_remark + "')";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = @"INSERT INTO ps_t_mytask_equip_temp  ([mytemp_id]
           ,[step]
           ,[process_id]
           ,[activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[is_new_schedule]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_mas_grp]
           ,[equip_item_no]
           ,[equip_item_no_b]
           ,[equip_code]
           ,[equip_add_desc]
           ,[equip_standard_drawing]
           ,[equip_qty]
           ,[equip_unit]
           ,[equip_reserve_qty]
           ,[equip_confirm_qty]
           ,[equip_bid_qty]
           ,[equip_incoterm]
           ,[factory_test]
           ,[manual_test]
           ,[routine_test]
           ,[waive_test]
           ,[legend]
           ,[path_code]
           ,[part_code_b]
           ,[is_breakdown]
           ,[breakdown_tab]
           ,[currency]
           ,[proposed_price]
           ,[evaluation_price]
           ,[preference_status]
           ,[import_duty]
           ,[irrelevant]
           ,[turnkey_account_assign]
           ,[turnkey_cost_object]
           ,[turnkey_fund_center]
           ,[turnkey_gl]
           ,[supply_account_assign]
           ,[supply_cost_object]
           ,[supply_fund_center]
           ,[supply_gl]
           ,[supply_equip_unit_price]
           ,[supply_equip_amonut]
           ,[local_exwork_unit_price]
           ,[local_exwork_amonut]
           ,[local_tran_unit_price]
           ,[local_tran_amonut]
           ,[service_account_assign]
           ,[service_cost_object]
           ,[service_fund_center]
           ,[service_gl]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc])
select '" + thisID + @"',[step]
           ,[process_id]
           ,[activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[is_new_schedule]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_mas_grp]
           ,[equip_item_no]
           ,[equip_item_no_b]
           ,[equip_code]
           ,[equip_add_desc]
           ,[equip_standard_drawing]
           ,[equip_qty]
           ,[equip_unit]
           ,[equip_reserve_qty]
           ,[equip_confirm_qty]
           ,[equip_bid_qty]
           ,[equip_incoterm]
           ,[factory_test]
           ,[manual_test]
           ,[routine_test]
           ,[waive_test]
           ,[legend]
           ,[path_code]
           ,[part_code_b]
           ,[is_breakdown]
           ,[breakdown_tab]
           ,[currency]
           ,[proposed_price]
           ,[evaluation_price]
           ,[preference_status]
           ,[import_duty]
           ,[irrelevant]
           ,[turnkey_account_assign]
           ,[turnkey_cost_object]
           ,[turnkey_fund_center]
           ,[turnkey_gl]
           ,[supply_account_assign]
           ,[supply_cost_object]
           ,[supply_fund_center]
           ,[supply_gl]
           ,[supply_equip_unit_price]
           ,[supply_equip_amonut]
           ,[local_exwork_unit_price]
           ,[local_exwork_amonut]
           ,[local_tran_unit_price]
           ,[local_tran_amonut]
           ,[service_account_assign]
           ,[service_cost_object]
           ,[service_fund_center]
           ,[service_gl]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc] from ps_t_mytask_equip where bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and depart_position_name='" + dept + "'";
                db.ExecNonQuery(sql, zetl4eisdb);

                sql = @"select equip_code from ps_t_mytask_equip where bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and depart_position_name='" + dept + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string eq_code = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (eq_code == "") eq_code = "'" + dr["equip_code"].ToString() + "'";
                        else eq_code += ",'" + dr["equip_code"].ToString() + "'";
                    }
                    if (eq_code != "")
                    {
                        sql = @"INSERT INTO [dbo].[ps_t_mytask_equip_doc_design_temp]
           ([mytemp_id]
           ,[process_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_code]
           ,[doc_nodeid]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc]
           ,[mytask_eq_id])
select '" + thisID + "','" + pid + "','" + jobno + "','" + jobrev + "','" + bidno + "','" + bidrev + "','" + schNo + "','" + schName + "',mytask_equip.depart_position_name,mytask_equip.section_position_name,mytask_equip.equip_code, equip_doc.doc_nodeid,'" + login + "',GETDATE(),'" + login + @"',GETDATE()
      ,doc_approve.[doc_desc]
	  ,mytask_equip.rowid
from ps_t_mytask_equip as mytask_equip
		inner join ps_m_equip_doc as equip_doc
			inner join ps_m_doc_approve as doc_approve
			on doc_approve.doc_nodeid = equip_doc.doc_nodeid
		on equip_doc.equip_code = mytask_equip.equip_code
		where mytask_equip.equip_code in (" + eq_code + ") and mytask_equip.depart_position_name='" + dept + "'";
                        db.ExecNonQuery(sql, zetl4eisdb);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void saveTemp(int tempID, addMytaskEquipJob obj, string bidno, string bidrev, string jobno, string jobrev, string part, string pid, string schNo, string schName, string dept, string sect, string login)
        {
            DataTable dt = new DataTable();
            try
            {
               string  sql = @"select mytemp_id from ps_t_mytask_equip_temp where mytemp_id='" + tempID + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count == 0)
                {
                    sql = @"INSERT INTO [ps_t_mytask_part_temp]
           ([mytemp_id]
            ,[activity_id]
           ,[step]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[part_name]
           ,[part_description]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
            ,[important]
            ,[process_id]
            ,[part_remark])
     VALUES
           ('" + tempID + @"'
           ,'" + obj.activity_id + @"'
           ,'" + obj.step + @"'
           ,'" + obj.job_no + @"'
           ,'" + obj.job_revision + @"'
           ,'" + obj.bid_no + @"'
           ,'" + obj.bid_revision + @"'
           ,'" + obj.schedule_no + @"'
           ,'" + obj.schedule_name + @"'
           ,'" + obj.depart_position_name + @"'
           ,'" + obj.section_position_name + @"'
           ,'" + obj.part_name + @"'
           ,'" + obj.part_description + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + @"'
           ,'" + obj.important + @"'
           ,'" + obj.process_id + @"'
           ,'"+obj.part_remark+"')";
                    db.ExecNonQuery(sql, zetl4eisdb);

                    sql = @"INSERT INTO ps_t_mytask_equip_temp  ([mytemp_id]
           ,[step]
           ,[process_id]
           ,[activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[is_new_schedule]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_mas_grp]
           ,[equip_item_no]
           ,[equip_item_no_b]
           ,[equip_code]
           ,[equip_add_desc]
           ,[equip_standard_drawing]
           ,[equip_qty]
           ,[equip_unit]
           ,[equip_reserve_qty]
           ,[equip_confirm_qty]
           ,[equip_bid_qty]
           ,[equip_incoterm]
           ,[factory_test]
           ,[manual_test]
           ,[routine_test]
           ,[waive_test]
           ,[legend]
           ,[path_code]
           ,[part_code_b]
           ,[is_breakdown]
           ,[breakdown_tab]
           ,[currency]
           ,[proposed_price]
           ,[evaluation_price]
           ,[preference_status]
           ,[import_duty]
           ,[irrelevant]
           ,[turnkey_account_assign]
           ,[turnkey_cost_object]
           ,[turnkey_fund_center]
           ,[turnkey_gl]
           ,[supply_account_assign]
           ,[supply_cost_object]
           ,[supply_fund_center]
           ,[supply_gl]
           ,[supply_equip_unit_price]
           ,[supply_equip_amonut]
           ,[local_exwork_unit_price]
           ,[local_exwork_amonut]
           ,[local_tran_unit_price]
           ,[local_tran_amonut]
           ,[service_account_assign]
           ,[service_cost_object]
           ,[service_fund_center]
           ,[service_gl]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc])
select '" + tempID + @"',[step]
           ,[process_id]
           ,[activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[is_new_schedule]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_mas_grp]
           ,[equip_item_no]
           ,[equip_item_no_b]
           ,[equip_code]
           ,[equip_add_desc]
           ,[equip_standard_drawing]
           ,[equip_qty]
           ,[equip_unit]
           ,[equip_reserve_qty]
           ,[equip_confirm_qty]
           ,[equip_bid_qty]
           ,[equip_incoterm]
           ,[factory_test]
           ,[manual_test]
           ,[routine_test]
           ,[waive_test]
           ,[legend]
           ,[path_code]
           ,[part_code_b]
           ,[is_breakdown]
           ,[breakdown_tab]
           ,[currency]
           ,[proposed_price]
           ,[evaluation_price]
           ,[preference_status]
           ,[import_duty]
           ,[irrelevant]
           ,[turnkey_account_assign]
           ,[turnkey_cost_object]
           ,[turnkey_fund_center]
           ,[turnkey_gl]
           ,[supply_account_assign]
           ,[supply_cost_object]
           ,[supply_fund_center]
           ,[supply_gl]
           ,[supply_equip_unit_price]
           ,[supply_equip_amonut]
           ,[local_exwork_unit_price]
           ,[local_exwork_amonut]
           ,[local_tran_unit_price]
           ,[local_tran_amonut]
           ,[service_account_assign]
           ,[service_cost_object]
           ,[service_fund_center]
           ,[service_gl]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc] from ps_t_mytask_equip where bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and depart_position_name='"+dept+"'";
                    db.ExecNonQuery(sql, zetl4eisdb);

                    sql = @"select equip_code from ps_t_mytask_equip where bid_no='" + bidno + "' and job_no='" + jobno + "' and path_code='" + part + "' and depart_position_name='" + dept + "'";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        string eq_code = "";
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (eq_code == "") eq_code = "'" + dr["equip_code"].ToString() + "'";
                            else eq_code += ",'" + dr["equip_code"].ToString() + "'";
                        }
                        if(eq_code!="")
                        {
                            sql = @"INSERT INTO [dbo].[ps_t_mytask_equip_doc_design_temp]
           ([mytemp_id]
           ,[process_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[equip_code]
           ,[doc_nodeid]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[equip_full_desc]
           ,[mytask_eq_id])
select '" + tempID + "','" + pid + "','" + jobno + "','" + jobrev + "','" + bidno + "','" + bidrev + "','" + schNo + "','" + schName + "',mytask_equip.depart_position_name,mytask_equip.section_position_name,mytask_equip.equip_code, equip_doc.doc_nodeid,'" + login + "',GETDATE(),'" + login + @"',GETDATE()
      ,doc_approve.[doc_desc]
	  ,mytask_equip.rowid
from ps_t_mytask_equip as mytask_equip
		inner join ps_m_equip_doc as equip_doc
			inner join ps_m_doc_approve as doc_approve
			on doc_approve.doc_nodeid = equip_doc.doc_nodeid
		on equip_doc.equip_code = mytask_equip.equip_code
		where mytask_equip.equip_code in (" + eq_code + ") and mytask_equip.depart_position_name='" + dept+"'";
                            db.ExecNonQuery(sql, zetl4eisdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public int getTempID(string tmpName, string jobno, string rev, string login, string dept, string sect)
        {
            int val = 0;
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select mytemp_id from ps_t_my_template where mytemp_name='" + tmpName + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                { 
                    val = Convert.ToInt32(dt.Rows[0]["mytemp_id"]);

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return val;
        }
        public int chkTempName(string tmpName, string jobno, string rev, string login,string dept,string sect)
        {
            int val = 0;
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select mytemp_name from ps_t_my_template where mytemp_name='" + tmpName + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count == 0)
                {
                    sql = @"INSERT INTO [ps_t_my_template]
           ([mytemp_name]
           ,[job_no]
           ,[job_revision]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[depart_position_name]
           ,[section_position_name])
     VALUES
           ('" + tmpName + @"'
           ,'" + jobno + @"'
           ,'" + rev + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
           ,'"+dept+@"'
           ,'"+sect+"')";
                    db.ExecNonQuery(sql, zetl4eisdb);
                    sql = @"select mytemp_id from ps_t_my_template where mytemp_name='" + tmpName + "'";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0) val = Convert.ToInt32(dt.Rows[0]["mytemp_id"]);
                }
                else val = 0;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return val;
        }
        public DataTable getallEQ(string jobno,string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct equip_code from ps_t_mytask_equip where job_no ='"+ jobno + "' and job_revision='"+ rev + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void insertDocFromAllEQ( string eq_code,string Dept,string Sect,string jobno,string rev,string AcID,string login,string pid)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = @"select doc_nodeid from ps_m_equip_doc where equip_code ='" + eq_code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string code = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (code == "") code += "'" + dr["doc_nodeid"].ToString() + "'";
                        else code += ",'" + dr["doc_nodeid"].ToString() + "'";
                    }
                    sql = @"select ps_m_doc_approve.*,doctype_name from ps_m_doc_approve,ps_m_doctype 
where ps_m_doc_approve.doctype_code = ps_m_doctype.doctype_code
and doc_nodeid in(" + code + ") and doc_status='Active' and depart_position_name='" + Dept + "' ";//and section_position_name='" + Sect + "'
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sql = @"select rowid from ps_t_mytask_doc
where doc_nodeid ='" + dr["doc_nodeid"].ToString() + @"' and job_no='" + jobno + @"' 
and depart_position_name='" + Dept + "' and section_position_name='" + Sect + @"'
and doctype_code='" + dr["doctype_code"].ToString() + "' and doc_name='" + dr["doc_name"].ToString() + @"' and job_revision='" + rev + "'";
                            dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                            if (dt.Rows.Count == 0)
                            {
                                sql = @"INSERT INTO [ps_t_mytask_doc]
           ([activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[doctype_code]
           ,[doc_nodeid]
           ,[doc_revision_no]
           ,[doc_name]
           ,[doc_sheetno]
           ,[doc_desc]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
            ,[process_id])
     VALUES
           ('" + AcID + @"'
           ,'" + jobno + @"'
           ,'" + rev + @"'
           ,''
           ,''
           ,''
           ,''
           ,'" + Dept + @"'
           ,'" + Sect + @"'
           ,'" + dr["doctype_code"].ToString() + @"'
           ,'" + dr["doc_nodeid"].ToString() + @"'
           ,'" + dr["doc_revision_no"].ToString() + @"'
           ,'" + dr["doc_name"].ToString() + @"'
           ,'" + dr["doc_sheetno"].ToString() + @"'
           ,'" + dr["doc_desc"].ToString() + @"'
           ,'" + login + @"'
           ,GETDATE()
           ,'" + login + @"'
           ,GETDATE()
            ,'"+pid+"')";
                                db.ExecNonQuery(sql, zetl4eisdb);

                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public DataTable validateItm(string jobno,string bidno, string part,string itm)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid from ps_t_mytask_equip where bid_no='"+ bidno + "' and job_no='"+ jobno + "' and path_code='"+ part + "' and equip_item_no='"+ itm + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void getDocNode(string eq_code, string Dept, string Sect, string JobNo, string JobRev, string BidNo, string AcID, string Login,string BidRev,string schName,string schNo,string pid)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = @"select doc_nodeid from ps_m_equip_doc where equip_code ='"+ eq_code + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if(dt.Rows.Count > 0) {
                    string code = "";
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (code == "") code += "'"+ dr["doc_nodeid"].ToString()+"'";
                        else code += ",'"+dr["doc_nodeid"].ToString()+"'";
                    }
                    sql = @"select ps_m_doc_approve.*,doctype_name from ps_m_doc_approve,ps_m_doctype 
where ps_m_doc_approve.doctype_code = ps_m_doctype.doctype_code
and doc_nodeid in("+ code + ") and doc_status='Active' and depart_position_name='" + Dept + "' ";//and section_position_name='" + Sect + "'
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sql = @"select rowid from ps_t_mytask_doc
where doc_nodeid ='"+dr["doc_nodeid"].ToString()+@"' and job_no='"+JobNo+"' and bid_no='"+BidNo+@"'
and depart_position_name='"+Dept+"' and section_position_name='"+Sect+ @"'
and doctype_code='" + dr["doctype_code"].ToString() + "' and doc_name='" + dr["doc_name"].ToString() + @"' and job_revision='" + JobRev + "'";
                            dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                            if (dt.Rows.Count==0)
                            {
                                sql = @"INSERT INTO [ps_t_mytask_doc]
           ([activity_id]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[doctype_code]
           ,[doc_nodeid]
           ,[doc_revision_no]
           ,[doc_name]
           ,[doc_sheetno]
           ,[doc_desc]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[process_id])
     VALUES
           ('" + AcID+@"'
           ,'"+JobNo+@"'
           ,'"+JobRev+@"'
           ,'"+BidNo+@"'
           ,'"+BidRev+@"'
           ,'"+schNo+@"'
           ,'"+ schName + @"'
           ,'"+Dept+@"'
           ,'"+Sect+@"'
           ,'"+ dr["doctype_code"].ToString() + @"'
           ,'" + dr["doc_nodeid"].ToString() + @"'
           ,'" + dr["doc_revision_no"].ToString() + @"'
           ,'" + dr["doc_name"].ToString() + @"'
           ,'" + dr["doc_sheetno"].ToString() + @"'
           ,'" + dr["doc_desc"].ToString() + @"'
           ,'"+Login+ @"'
           ,GETDATE()
           ,'" + Login + @"'
           ,GETDATE()
            ,'"+pid+"')";
                                db.ExecNonQuery(sql,zetl4eisdb);
                                
                            }
                        }
                    }
                }

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
   
        }
        public DataTable getEquipBom(string equipcode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"";
                sql = @"select equip_code from ps_m_bom where bom_code='"+ equipcode + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                string equip_code = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (i==0)
                    {
                        equip_code = "'"+dt.Rows[i]["equip_code"].ToString()+"'";
                    }
                    else
                    {
                        equip_code+= ",'" + dt.Rows[i]["equip_code"].ToString() + "'";
                    }
                }
              if(equip_code!="")  sql = @"select equip_code,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,legend,parts,isactive,remark,unit,
                materialgroup,parts,created_by,created_datetime,updated_by,updated_datetime
                from ps_m_equipcode where equip_code in (" + equip_code + ")";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable sumPart(string jobno,string rev,string cond)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select part_list,bid_no from vw_ps_job_dashboard_relate
where job_no = '" + jobno+"' and job_revision = '"+rev+"' and schedule_no  is null";
                if (cond != "") sql += " and part_list like '%"+ cond + "%'";
                 dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getProject_no(string project_id)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select *
                     from project_no where project_id='" + project_id + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable getTabBid(string jobno,string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from(
select bid_no,schedule_no,bid_revision,schedule_name from vw_ps_job_dashboard_relate
where job_no = '" + jobno + "' and job_revision = '"+rev+ @"' and schedule_no <>''
union 
select 'Bid Supply'as bid_no,'0'as schedule_no,'0' as bid_revision,'' as schedule_name from vw_ps_job_dashboard_relate
where job_no = '" + jobno+"' and job_revision = '"+rev+ @"' and schedule_no  is null)
as tmp order by bid_no desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getTabBid_sup(string jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select  distinct bid_no,bid_revision from vw_ps_job_dashboard_relate
where job_no = '" + jobno + "' and job_revision = '" + rev + @"' and schedule_no  is null";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void insertconfig_cate(addconfig_cateJob obj)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select  rowid from ps_t_config_cate where cate_code ='" + obj.cate_code + "' and job_no='" + obj.job_no + "' and job_revision ='" + obj.job_revision + "'" +
                    "and depart_position_name='" + obj.depart_position_name + "' and section_position_name='" + obj.section_position_name + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"UPDATE ps_t_config_cate
   SET percent_reservation='" + obj.percent_reservation + @"'
      ,is_round_up='" + obj.is_round_up + @"'
      ,[created_by]='" + obj.created_by + @"'
      ,[created_datetime]='" + obj.created_datetime + @"'
      ,[updated_by]='" + obj.updated_by + @"'
      ,[updated_datetime]='" + obj.updated_datetime + @"'
 WHERE cate_code ='" + obj.cate_code + "' and job_no='" + obj.job_no + "' and job_revision ='" + obj.job_revision + "'" +
 "and depart_position_name='" + obj.depart_position_name + "' and section_position_name='" + obj.section_position_name + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                else
                {
                    sql = @"INSERT INTO ps_t_config_cate
           ([job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[depart_position_name]
           ,[section_position_name]
           ,[cate_code]
           ,[cate_name]
           ,[percent_reservation]
           ,[is_round_up]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime])
     VALUES
           ('" + obj.job_no + @"'
           ,'" + obj.job_revision + @"'
           ,'" + obj.bid_no + @"'
           ,'" + obj.bid_revision + @"'
           ,'" + obj.depart_position_name + @"'
           ,'" + obj.section_position_name + @"'
           ,'" + obj.cate_code + @"'
           ,'" + obj.cate_name + @"'
           ,'" + obj.percent_reservation + @"'
           ,'" + obj.is_round_up + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + "')";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public DataTable getconfigure(string job_no, string rev, string dept, string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select cate_code,cate_name,percent_reservation,is_round_up from ps_t_config_cate
where job_no='" + job_no + "' and job_revision ='" + rev + @"'
and depart_position_name = '" + dept + "' and section_position_name = '" + sect + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count == 0)
                {
                    sql = @"select cate_code,cate_name,percent_reservation,is_round_up,created_by,created_datetime,updated_by,updated_datetime
from ps_m_category";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void savePartRemark(string dept, string sect, string bidno, string schno, string part, string rmk,string desc,string imp)
        {
            try
            {
                string sql = @"update ps_t_mytask_part set part_remark='" + rmk + "',part_description='"+ desc + "',important='"+imp+@"'
where depart_position_name ='" + dept + @"' 
and bid_no = '" + bidno + "' and schedule_no = '" + schno + "' and part_name = '" + part + "'";

                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                
                LogHelper.WriteEx(ex);
            }

        }
        public void delPart(string part, string BidNo, string schNo, string dept, string sect,string JobNo)
        {
            DataTable dt = new DataTable();
            try
            {
                if (BidNo == "Bid Supply")
                {
                   string  sql = @"select  distinct bid_no from vw_ps_job_dashboard_relate
where job_no = '"+ JobNo + "' and schedule_no  is null";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count >0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sql = @"delete from ps_t_mytask_part 
where bid_no = '" + dr["bid_no"].ToString() + "'  and part_name='" + part + "' and job_no='" + JobNo + "'";
                            db.ExecNonQuery(sql, zetl4eisdb);
                            sql = @"select rowid from ps_t_mytask_equip
where  bid_no = '" + dr["bid_no"].ToString() + "'  and path_code='" + part + "' and job_no='" + JobNo + "'";
                            dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                            if (dt.Rows.Count > 0)
                            {
                                foreach (DataRow dr2 in dt.Rows)
                                {
                                    sql = @"delete ps_t_mytask_equip where rowid='" + dr2["rowid"].ToString() + "'";
                                    db.ExecNonQuery(sql, zetl4eisdb);
                                    sql = @"delete ps_t_mytask_equip_doc_design where mytask_eq_id ='" + dr2["rowid"].ToString() + "'";
                                    db.ExecNonQuery(sql, zetl4eisdb);
                                }
                            }

                        }

                    }
                }
                else
                {
                    string sql = @"delete from ps_t_mytask_part 
where bid_no = '" + BidNo + "'  and part_name='" + part + "' and job_no='" + JobNo + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                    sql = @"select rowid from ps_t_mytask_equip
where  bid_no = '" + BidNo + "'  and path_code='" + part + "' and job_no='" + JobNo + "'";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        foreach (DataRow dr in dt.Rows)
                        {
                            sql = @"delete ps_t_mytask_equip where rowid='" + dr["rowid"].ToString() + "'";
                            db.ExecNonQuery(sql, zetl4eisdb);
                            sql = @"delete ps_t_mytask_equip_doc_design where mytask_eq_id ='" + dr["rowid"].ToString() + "'";
                            db.ExecNonQuery(sql, zetl4eisdb);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public DataTable viewLegend(string strLegend)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts from ps_m_equipcode
                                  where  legend = '" + strLegend + "'";
                dt = db.ExecSql_DataTable(strSQL, zetl4eisdb);
                if (dt.Rows.Count == 0)
                {
                    strSQL = @"select rowid,bom_code as equip_code,bom_name as equip_desc_short,bom_desc as equip_desc_full,depart_position_name,section_position_name,bomlevel,legend,cate_code,
misc_per_unit,equip_type,equip_code,equip_qty,equip_unit as unit,equip_buyinglot,equip_reserve,created_by,
created_datetime,updated_by,updated_datetime,'' as materialgroup,'' as equip_desc_short,bom_part_code as parts from ps_m_bom where legend = '" + strLegend + "'";
                    dt = db.ExecSql_DataTable(strSQL, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getAllPart(string type)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select part_code,parts_name from ps_m_part_bidtype,ps_m_parts
where ps_m_part_bidtype.part_code=ps_m_parts.parts_code
  and bid_type = '" + type + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count == 0)
                {
                    sql = @"select part_code,parts_name from ps_m_part_bidtype,ps_m_parts
where ps_m_part_bidtype.part_code=ps_m_parts.parts_code
  and bid_type = 'ALL'";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getGVSelPart(string dept, string sect, string bidNo, string schNo, string part,string jobNo,string rev,string schname)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (bidNo!= "Bid Supply")
                {
                  sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,waive_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit,'' as  Part_Sort
                from  ps_t_mytask_equip 
                where bid_no = '" + bidNo + "' and schedule_name ='" + schname + "'  and path_code='" + part + "' and job_no='" + jobNo + "' and job_revision='" + rev + "' order by len(equip_item_no) asc,ASCII(equip_item_no) ,equip_item_no";
                }
                else
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,waive_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,legend,path_code as part,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit,'' as  Part_Sort 
                from  ps_t_mytask_equip 
                where  schedule_name ='" + schname + "'  and path_code='" + part + "' and job_no='" + jobNo + "' and job_revision='" + rev + "' order by len(equip_item_no) asc,ASCII(equip_item_no) ,equip_item_no";
                }
                // and section_position_name='" + sect + "'and depart_position_name ='" + dept + "'

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable chkSectionWork(string login, string jobno, string rev, string activity_id)
        {


            DataTable dt = new DataTable();
            try
            {
                string sql = "select * from ps_t_job_dashboard where job_no = '" + jobno + "' and job_revision ='" + rev + "' and activity_id = '" + activity_id + "' and (assignee_drawing like '%" + login + "%' or assignee_equip like '%" + login + "%' or assignee_tech_doc like '%" + login + "%')";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }


            return dt;
        }
        public void insertMytaskPart(addMytaskEquipJob obj)
        {
            DataTable dt = new DataTable();
            try
            {
                string desc = "";
                if (obj.part_description == "")
                {
                    string getpart = @"select distinct part_code,parts_name from ps_m_part_bidtype,ps_m_parts
where ps_m_part_bidtype.part_code=ps_m_parts.parts_code and  part_code='" + obj.part_name + "'";
                    dt = db.ExecSql_DataTable(getpart, zetl4eisdb);
                    if (dt.Rows.Count > 0) desc = dt.Rows[0]["parts_name"].ToString();
                    else desc = "";
                }
                else
                {
                    desc = obj.part_description;
                }
                string sql = @"select part_name from ps_t_mytask_part
where  job_no = '" + obj.job_no + "' and schedule_no = '" + obj.schedule_no + "' and part_name = '" + obj.part_name + "' and bid_no='"+obj.bid_no+"'";
                //depart_position_name = '" + obj.depart_position_name + "' and section_position_name = '" + obj.section_position_name + @"'and
                                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"update ps_t_mytask_part set part_description='" + desc + "',important='" + obj.important + "',updated_by='" + obj.updated_by+ "',part_remark='',updated_datetime='" + obj.updated_datetime+"'" +
                        "where depart_position_name = '" + obj.depart_position_name + "' and section_position_name = '" + obj.section_position_name + @"'
and job_no = '" + obj.job_no + "' and schedule_no = '" + obj.schedule_no + "' and part_name = '" + obj.part_name + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                else
                {
                    sql = @"INSERT INTO [dbo].[ps_t_mytask_part]
           ([activity_id]
           ,[step]
           ,[job_no]
           ,[job_revision]
           ,[bid_no]
           ,[bid_revision]
           ,[schedule_no]
           ,[schedule_name]
           ,[depart_position_name]
           ,[section_position_name]
           ,[part_name]
           ,[part_description]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime]
           ,[important]
           ,[process_id]
            ,[part_remark])
     VALUES
           ('" + obj.activity_id + @"'
           ,'" + obj.step + @"'
           ,'" + obj.job_no + @"'
           ,'" + obj.job_revision + @"'
           ,'" + obj.bid_no + @"'
           ,'" + obj.bid_revision + @"'
           ,'" + obj.schedule_no + @"'
           ,'" + obj.schedule_name + @"'
           ,'" + obj.depart_position_name + @"'
           ,'" + obj.section_position_name + @"'
           ,'" + obj.part_name + @"'
           ,'" + desc + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + @"'
           ,'"+obj.important + @"'
           ,'"+obj.process_id+@"'
           ,'')";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public JobInfo getJobInfo(string JobNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"select [job_no]
                                  ,[revision]
                                  ,[sub_line_name]
                                  ,[job_desc]
                                  ,[remark]
                                  ,[completion_date]
                                  ,[p6_task_id]
                                  ,[p6_task_name]
                                  ,[p6_plan_start]
                                  ,[p6_plan_end] 
                                from vw_ps_job_dashboard_info where job_no ='" + JobNo + "'";

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            JobInfo jinfo = new JobInfo();
            if (dt.Rows.Count > 0)
            {
                jinfo.job_no = dt.Rows[0]["job_no"].ToString();
                //  dbo.udf_StripHTML(job.remark) as remark, waa.reference
                jinfo.revision = dt.Rows[0]["revision"].ToString();
                jinfo.sub_line_name = dt.Rows[0]["sub_line_name"].ToString();
                jinfo.job_desc = dt.Rows[0]["job_desc"].ToString();
                jinfo.remark = dt.Rows[0]["remark"].ToString();
                //jinfo.reference = dt.Rows[0]["reference"].ToString();
                jinfo.completion_date = dt.Rows[0]["completion_date"].ToString();
                jinfo.p6_task_id = dt.Rows[0]["p6_task_id"].ToString();
                jinfo.p6_task_name = dt.Rows[0]["p6_task_name"].ToString();
                jinfo.p6_plan_start = dt.Rows[0]["p6_plan_start"].ToString();
                jinfo.p6_plan_end = dt.Rows[0]["p6_plan_end"].ToString();
            }
            return jinfo;
        }
        public DataTable GetItmNo_sup(string jobno, string part)
        {
            DataTable dt = new DataTable();

            try
            {
                string sql = @"select * from (select equip_item_no from ps_t_mytask_equip where  path_code='" + part + "' and job_no='" + jobno + "' ) tb  order by  len(equip_item_no),equip_item_no ";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetItmNo(string jobno,string part,string bidno) {
            DataTable dt = new DataTable();
           
            try
            {
                string sql = @"select * from (select equip_item_no from ps_t_mytask_equip where  path_code='" + part + "' and job_no='" + jobno + "'and bid_no='" + bidno + "') tb  order by  len(equip_item_no),equip_item_no";
                //string sql = @"select equip_item_no from ps_t_mytask_equip where  path_code='"+ part + "' and job_no='"+ jobno + "'and bid_no='"+bidno+"' order by equip_item_no desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLScheNo(string jobno,string rev)
        {

            DataTable dt = new DataTable();
            try
            {
                string sql = @"select  ps.schedule_no,ps.schedule_name from ps_m_bid_schedule ps
						  left join  vw_ps_job_dashboard_relate vw
						  on vw.bid_no = ps.bid_no
						  where vw.job_no = '"+ jobno + @"'
						  and vw.job_revision = '"+rev+"'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetJobRelate(string Jobno, string Bidno)
        {
            DataTable dt = new DataTable();
            try
            {
                string rowfilter = "";
                string strQuery = @"SELECT ROW_NUMBER() OVER(ORDER BY eq_group_no) AS runno
                              ,[eq_desc]
                              ,[eq_group_no]
                              ,[bid_no]
                              ,[bid_desc]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[schedule_status]
                              ,[job_no]
                              ,[job_revision]
                              ,[ps_url]
                          FROM vw_ps_job_dashboard_relate
                          WHERE job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "')";
                if (Bidno != "")
                {
                    rowfilter = " and bid_no = '" + Bidno + "'";
                    strQuery += rowfilter;
                }

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public void DeleteDrawing(AddDoc objAddDoc)
        {
            try
            {
                string strQuery = "delete from ps_t_mytask_doc where job_no = '" + objAddDoc.job_no + "' and job_revision = '" + objAddDoc.job_revision + @"'
						    and depart_position_name = '" + objAddDoc.depart_position_name + "' and section_position_name = '" + objAddDoc.section_position_name + "' and doctype_code = '" + objAddDoc.doctype_code + "' and doc_name = '" + objAddDoc.doc_name + "'";

                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }


        }
        public DataTable bindMultiBid(string Jobno)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = "select bid_no,schedule_name,schedule_no from vw_ps_job_dashboard_relate where job_no = '" + Jobno + "' and job_revision = (select max(revision) from job_no where job_no =  '" + Jobno + "') GROUP BY bid_no,schedule_name,schedule_no";
  
                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable getDDLDcoType(string type)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"SELECT DISTINCT doctype_code,doctype_name FROM ps_m_doctype";
                if (type == "design")
                {
                    sql += " where  is_select_design ='1'";
                }
                else if (type == "uploaddoc") {
                    sql += " where  is_upload_manual ='1'";
                }
                else if (type == "uploadsel")
                {
                    sql += " where  is_select_tech ='1'";
                }
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetAttach(string draws, string dept, string section)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT [rowid]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[doc_desc]
                              ,[doc_revision_no]
                              ,[doc_revision_date]
                              ,[doc_sheetno]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doc_status]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime]
                          FROM ps_m_doc_approve
                          where depart_position_name ='" + dept + "' and section_position_name ='" + section + "'";

                if (draws != "")
                {
                    strQuery += " and doctype_code in (select doctype_code from ps_m_doctype where doctype_name  in('Design Drawing','Typical Drawing'))";
                }

                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
             

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
           

            return dt;
        }
        public DataTable GetDrawingDDL(string Jobno, string chkdraw, string dept, string section, string rev, string bidno)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT distinct [depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doctype_name]
                              ,[doc_no]
                              ,[doc_desc]
                              ,[doc_sheetno]
                              ,[doc_revision]
                              ,[job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[link_doc]
                              ,[updated_by]
                              ,[updated_datetime]
                              ,[tr_doc_desc]
                              ,[tr_updated_by]
                              ,[tr_updated_datetime]
                              ,[process_id]
                          FROM vw_ps_dashboard_doc
                          where job_no = '" + Jobno + "' and job_revision = '" + rev + "' and bid_no in ("+bidno+")";
                string rowfilter = "";
                if (chkdraw != "")
                {
                    rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code in ('DD','TD'))";
                    strQuery += rowfilter;
                }
                else
                {
                    rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code not in ('DD','TD'))";
                    strQuery += rowfilter;
                }


                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetDrawing(string Jobno, string chkdraw, string dept, string section, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT distinct [depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doctype_name]
                              ,[doc_no]
                              ,[doc_desc]
                              ,[doc_sheetno]
                              ,[doc_revision]
                              ,[job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[link_doc]
                              ,[updated_by]
                              ,[updated_datetime]
                              ,[tr_doc_desc]
                              ,[tr_updated_by]
                              ,[tr_updated_datetime]
                              ,[process_id]
                          FROM vw_ps_dashboard_doc
                          where job_no = '" + Jobno + "' and job_revision = '" + rev + "'";// and depart_position_name ='" + dept + "' and section_position_name ='" + section + "'
                string rowfilter = "";
                if (chkdraw != "")
                {
                    rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code in ('DD','TD'))";
                    strQuery += rowfilter;
                }
                else
                {
                    rowfilter = " and doctype_name in (select doctype_name from ps_m_doctype where doctype_code not in ('DD','TD'))";
                    strQuery += rowfilter;
                }


                dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void InsertAddDoc(AddDoc objAddDoc)
        {
            //string strQuery = " update ps_t_mytask_doc set created_by = 'test' where job_no = 'SPPC-01-S03'";
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from ps_t_mytask_doc 
                            where job_no = '" + objAddDoc.job_no + "' and job_revision = '" + objAddDoc.job_revision + @"'

                            and depart_position_name = '" + objAddDoc.depart_position_name + "' and section_position_name = '" + objAddDoc.section_position_name + "' and doctype_code = '" + objAddDoc.doctype_code + "' and doc_name = '" + objAddDoc.doc_name + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string strQuery = @"update ps_t_mytask_doc set doc_nodeid ='" + objAddDoc.doc_nodeid + "', bid_no ='" + objAddDoc.bid_no + @"' , doc_desc ='" + objAddDoc.doc_desc + "' , created_datetime ='" + objAddDoc.created_datetime + "' , updated_datetime ='" + objAddDoc.updated_datetime + "' ,updated_by ='" + objAddDoc.updated_by + "' , schedule_name ='" + objAddDoc.schedule_name + "'  where job_no = '" + objAddDoc.job_no + "' and job_revision = (select max(revision) from job_no where job_no = '" + objAddDoc.job_no + @"')
						    and depart_position_name = '" + objAddDoc.depart_position_name + "' and section_position_name = '" + objAddDoc.section_position_name + "' and doctype_code = '" + objAddDoc.doctype_code + "' and doc_name = '" + objAddDoc.doc_name + "' ";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
                else
                {
                    string strQuery = @"INSERT INTO ps_t_mytask_doc
                              ([job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_revision_no]
                              ,[doc_name]
                              ,[doc_desc]
                              ,[doc_sheetno]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime]
                              ,[process_id]
                              ,[activity_id])
                              VALUES
               ('" + objAddDoc.job_no + "'"
         + " , '" + objAddDoc.job_revision + "'"
         + " , '" + objAddDoc.bid_no + "'"
         + " , '" + objAddDoc.bid_revision + "'"
         + " , '" + objAddDoc.schedule_no + "'"
         + " , '" + objAddDoc.schedule_name + "'"
         + " , '" + objAddDoc.depart_position_name + "'"
         + " , '" + objAddDoc.section_position_name + "'"
         + " , '" + objAddDoc.doctype_code + "'"
         + " , '" + objAddDoc.doc_nodeid + "'"
         + " , '" + objAddDoc.doc_revision_no + "'"
         + " , '" + objAddDoc.doc_name + "'"
         + " , '" + objAddDoc.doc_desc + "'"
         + " , '" + objAddDoc.doc_sheetno + "'"
         + " , '" + objAddDoc.created_by + "'"
         + " , '" + objAddDoc.created_datetime + "'"
         + " , '" + objAddDoc.updated_by + "'"
         + " , '" + objAddDoc.updated_datetime + "'"
         + " , '" + objAddDoc.process_id + "'" 
         + " , '"+objAddDoc.activId+"')";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public DataTable GetBid(string Jobno,string Rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select 
                        j.job_no , j.revision, j.sub_line,j.sub_line_name,j.voltage_level,
                        j.job_type,  dbo.udf_StripHTML(j.job_desc) as job_desc,
                        b.bid_no, dbo.udf_StripHTML(b.bid_desc) as bid_desc, b.bid_revision ,  dbo.udf_StripHTML(b.remark) as remark
                        from job_no j
                        left join bid_job_relate bj on (bj.job_id= j.job_id)
                        left join bid_no b on (bj.bid_id = b.bid_id)
                         where (b.status in ('Active','New','Suspended') ) 
                         and j.job_no = '" + Jobno + "' and j.revision='" + Rev + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            

            return dt;
        }
//        public DataTable getEqui(string tbSch, string ddlSchjob, string ddlSchBom)
//        {
//            DataTable dt = new DataTable();
//            try
//            {
//                string sql = "";
//                //                string sql = @"select rowid,bom_code,bom_name,bom_desc,depart_position_name,section_position_name,ps_m_bom.equip_code,equip_qty,equip_unit,
//                //ps_m_bom.created_by,ps_m_bom.created_datetime,ps_m_bom.updated_by,ps_m_bom.updated_datetime,
//                //equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit
//                //from ps_m_bom,ps_m_equipcode
//                //where ps_m_bom.equip_code = ps_m_equipcode.equip_code";
//                if (tbSch != "" || ddlSchBom != "")
//                {
//                    sql = @"select rowid,bom_code,bom_name,bom_desc,depart_position_name,section_position_name,ps_m_bom.equip_code,equip_qty,equip_unit,
//ps_m_bom.created_by,ps_m_bom.created_datetime,ps_m_bom.updated_by,ps_m_bom.updated_datetime,
//equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit
//from ps_m_bom,ps_m_equipcode
//where ps_m_bom.equip_code = ps_m_equipcode.equip_code
//and (bom_code = '" + ddlSchBom + "' or ps_m_bom.equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'" +
//    "or equip_desc_short like '%" + tbSch.Trim() + "%')";
//                    //" and (bom_name like '%"+ tbSch.Trim() + "%' or ps_m_bom.equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%')";
//                }
//                else if (ddlSchjob != "")
//                {
//                    sql = @"select depart_position_name,section_position_name,mteq.equip_code,equip_qty,
//equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit
//from ps_t_mytask_equip mteq
//left join ps_m_equipcode eq 
//on eq.equip_code = mteq.equip_code
//where mteq.job_no = '" + ddlSchjob + "'";
//                }
//                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

//            }
//            catch (Exception ex)
//            {

//                LogHelper.WriteEx(ex);
//            }
//            return dt;
//        }

        public int insertMyTaskEquip(addMytaskEquipJob objMytask)
        {
            int rowid = 0;
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                sql = @"select rowid  from ps_t_mytask_equip where job_no = '" + objMytask.job_no + "' and job_revision = '" + objMytask.job_revision + "' and bid_no = '" + objMytask.bid_no + "' and bid_revision = '" + objMytask.bid_revision + @"'
and schedule_no = '" + objMytask.schedule_no + "' and schedule_name = '" + objMytask.schedule_name + "' and depart_position_name = '" + objMytask.depart_position_name + "' and section_position_name = '" + objMytask.section_position_name + @"'
and path_code = '" + objMytask.path_code + "' and equip_code='" + objMytask.equip_code + "' and equip_item_no='" + objMytask.equip_item_no + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                
                if (dt.Rows.Count >0)
                {
                    rowid = Convert.ToInt32(dt.Rows[0]["rowid"]);
                    sql = @"UPDATE ps_t_mytask_equip
   SET 
      [equip_item_no] = '"+objMytask.equip_item_no+@"'
      ,[equip_add_desc] = '"+objMytask.equip_add_desc+@"'
      ,[equip_standard_drawing] = '"+objMytask.equip_standard_drawing+@"'
      ,[equip_qty] = '"+objMytask.equip_qty+@"'
      ,[equip_unit] = '"+objMytask.equip_unit+@"'
      ,[equip_bid_qty] = '"+objMytask.equip_bid_qty+@"'
      ,[equip_incoterm] = '"+objMytask.equip_incoterm+@"'
      ,[factory_test] = '"+objMytask.factory_test+@"'
      ,[manual_test] = '"+objMytask.manual_test+@"'
      ,[routine_test] = '"+objMytask.routine_test+ @"'
      ,[waive_test] = '" + objMytask.waive_test + @"'
      ,[is_breakdown] = '" + objMytask.is_breakdown+@"'
      ,[currency] = '"+objMytask.currency+@"'
      ,[supply_equip_unit_price] = '"+objMytask.supply_equip_unit_price+@"'
      ,[supply_equip_amonut] = '"+objMytask.supply_equip_amonut+@"'
      ,[local_exwork_unit_price] = '"+objMytask.local_exwork_unit_price+@"'
      ,[local_exwork_amonut] = '"+objMytask.local_exwork_amonut+@"'
      ,[local_tran_unit_price] = '"+objMytask.local_exwork_unit_price+@"'
      ,[local_tran_amonut] = '"+objMytask.local_tran_amonut+@"'
      ,[updated_by] = '"+objMytask.updated_by+@"'
      ,[updated_datetime] = '"+objMytask.updated_datetime+@"'
 WHERE job_no = '" + objMytask.job_no + "' and job_revision = '" + objMytask.job_revision + "' and bid_no = '" + objMytask.bid_no + "' and bid_revision = '" + objMytask.bid_revision + @"'
and schedule_no = '" + objMytask.schedule_no + "' and schedule_name = '" + objMytask.schedule_name + "' and depart_position_name = '" + objMytask.depart_position_name + "' and section_position_name = '" + objMytask.section_position_name + @"'
and path_code = '" + objMytask.path_code + "' and equip_code='" + objMytask.equip_code + "' and equip_item_no='" + objMytask.equip_item_no + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
                else
                {
                    sql = @"insert into ps_t_mytask_equip(section_position_name,depart_position_name
,job_no
,bid_no
,equip_code
,created_by
,created_datetime
,updated_by
,updated_datetime
,equip_add_desc
,bid_revision
,job_revision
,schedule_no
,is_breakdown
,path_code
,equip_item_no
,activity_id
,step
,equip_full_desc
,equip_unit
,schedule_name
,equip_qty
,legend
,equip_incoterm
,factory_test
,manual_test
,routine_test
,waive_test
,supply_equip_unit_price
,supply_equip_amonut
,local_exwork_unit_price
,local_exwork_amonut
,local_tran_unit_price
,local_tran_amonut
,equip_standard_drawing
,currency
,equip_mas_grp
,process_id)values('" + objMytask.section_position_name + "','" + objMytask.depart_position_name + "','" + objMytask.job_no + "','" + objMytask.bid_no + "','" + objMytask.equip_code + "'," +
"'" + objMytask.created_by + "','" + objMytask.created_datetime + "','" + objMytask.updated_by + "'," +
"'" + objMytask.updated_datetime + "','" + objMytask.equip_add_desc + "','" + objMytask.bid_revision + "','" + objMytask.job_revision + "'," +
"'" + objMytask.schedule_no + "','" + objMytask.is_breakdown + "','" + objMytask.path_code + "','" + objMytask.equip_item_no + "'," +
"'" + objMytask.activity_id + "','" + objMytask.step + "','" + objMytask.equip_full_desc + "','" + objMytask.equip_unit + "','" + objMytask.schedule_name + "','" + objMytask.equip_qty + "','" + objMytask.legend + "'," +
"'" + objMytask.equip_incoterm + "','" + objMytask.factory_test + "','" + objMytask.manual_test + "','" + objMytask.routine_test + "','" + objMytask.waive_test + "','" + objMytask.supply_equip_unit_price + "'," +
"'" + objMytask.supply_equip_amonut + "','" + objMytask.local_exwork_unit_price + "','" + objMytask.local_exwork_amonut + "','" + objMytask.local_tran_unit_price + "','" + objMytask.local_tran_amonut + "'," +
"'" + objMytask.equip_standard_drawing + "','" + objMytask.currency + "','"+objMytask.equip_mas_grp+"','"+objMytask.process_id+"')";
                    db.ExecNonQuery(sql, zetl4eisdb);

                }
                sql = @"select max(rowid) as rowid from ps_t_mytask_equip";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if(dt.Rows.Count>0) rowid = Convert.ToInt32(dt.Rows[0]["rowid"]);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return rowid;
        }
        public DataTable getDDlItem(string bidNo, string rev, string dept, string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct job_no,job_revision   from  ps_t_mytask_equip 
where job_no='" + bidNo + "' and job_revision <>'" + rev + "'";
                //                string sql = @"select distinct job_no,job_revision   from  ps_t_mytask_equip 
                //where depart_position_name ='" + dept + "' and section_position_name='" + sect + @"'
                //and job_no='" + bidNo + "' and job_revision <>'" + rev + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);

            }
            return dt;
        }
        public DataTable getDDLBom(string dept, string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct bom_code,bom_name from ps_m_bom ";
                //string sql = @"select distinct bom_code,bom_name from ps_m_bom where depart_position_name ='" + dept + "' and section_position_name='" + sect + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getTabEqui(string dept, string sect,string jobNo, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct bid_no,schedule_no,schedule_name,bid_revision from ps_t_mytask_part
where job_no = '"+ jobNo + "' and job_revision='"+ rev + @"'
and depart_position_name = '"+ dept + "' and section_position_name = '"+ sect + "'";
//                string sql = @"select distinct bid_no,schedule_no,schedule_name from  ps_t_mytask_equip
//where job_no = '" + jobNo + "' and job_revision='" + rev + "'";
                //string sql = @"select distinct bid_no,schedule_no from  ps_t_mytask_equip";
                //                string sql = @"select distinct bid_no,schedule_no from  ps_t_mytask_equip 
                //where depart_position_name= '" + dept + "' and section_position_name ='" + sect + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getGV(string dept, string sect, string bidNo, string schNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
equip_add_desc,equip_qty,equip_unit,equip_bid_qty,
supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc 
from  ps_t_mytask_equip 
where  bid_no = '" + bidNo + "' and schedule_no ='" + schNo + "'";
                //                string sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                //equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
                //equip_add_desc,equip_qty,equip_unit,equip_bid_qty,
                //supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                //local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp 
                //from  ps_t_mytask_equip 
                //where depart_position_name ='" + dept + @"' and section_position_name='" + sect + @"'
                //and bid_no = '" + bidNo + "' and schedule_no ='" + schNo + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getBtnPaths(string dept, string sect, string bidNo, string schNo,string JobNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (bidNo=="Bid Supply")
                {
                    sql = @"select  distinct bid_no from vw_ps_job_dashboard_relate
where job_no = '" + JobNo + "' and schedule_no  is null";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count > 0)
                    {
                        string allBid = "";
                        foreach (DataRow dr in dt.Rows)
                        {
                            if (allBid == "") allBid = "'"+dr["bid_no"].ToString()+ "'";
                            else allBid += ",'" + dr["bid_no"].ToString()+"'";
                        }
                        sql = @"select distinct part_name,part_description,part_remark,important
,Convert(decimal(10,2),replace(replace(replace(stuff(part_name, 1, patindex('%[0-9]%', part_name)-1, ''),'_Breakdown','.1'),'_Supervisor','.2'),'_Option','.3')) as orderPart
,replace(replace(replace(Stuff(part_name, PatIndex('%[0-9]%', part_name), 2, ''),'Breakdown',''),'Supervisor','') ,'Option','') as orderPrefix
from ps_t_mytask_part where  bid_no in (" + allBid + ") and job_no='"+JobNo+ "' order by  orderPrefix,orderPart";
                        dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    }
                }
                else
                {
                     sql = @"select distinct part_name,part_description,part_remark,important
,Convert(decimal(10,2),replace(replace(replace(stuff(part_name, 1, patindex('%[0-9]%', part_name)-1, ''),'_Breakdown','.1'),'_Supervisor','.2'),'_Option','.3')) as orderPart
,replace(replace(replace(Stuff(part_name, PatIndex('%[0-9]%', part_name), 2, ''),'Breakdown',''),'Supervisor','') ,'Option','') as orderPrefix
from ps_t_mytask_part where  bid_no = '" + bidNo + "' and job_no='"+JobNo+ "'  order by  orderPrefix,orderPart";
                    dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                }
               
                //and section_position_name='" + sect + @"' depart_position_name ='" + dept + @"' and
                //                string sql = @"select distinct path_code from  ps_t_mytask_equip 
                //where bid_no = '" + bidNo + "' and schedule_no = '" + schNo + "'";
                //                string sql = @"select distinct path_code from  ps_t_mytask_equip 
                //where depart_position_name ='" + dept + @"' and section_position_name='" + sect + @"'
                //and bid_no = '" + bidNo + "' and schedule_no = '" + schNo + "'";
                
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void updateProcessID(string jobno,string rev,string PID)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from(
select bid_no from vw_ps_job_dashboard_relate
where job_no = '"+ jobno + "' and job_revision = '"+rev+ @"' and schedule_no <>''
union 
select bid_no from vw_ps_job_dashboard_relate
where job_no = '" + jobno + "' and job_revision = '" + rev + @"' and schedule_no  is null)
as tmp order by bid_no desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                string allBid = "";
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (allBid == "") allBid = "'" + dr["bid_no"].ToString() + "'";
                        else allBid += ",'" + dr["bid_no"].ToString() + "'";
                    }
                    sql = @"update ps_t_mytask_doc set process_id ='"+PID+@"'
where job_no = '" + jobno + "' and job_revision = '" + rev + "'and doctype_code in  ('DD', 'TD')";
                    db.ExecNonQuery(sql,zetl4eisdb);

                    sql = @"update ps_t_mytask_doc set process_id ='" + PID + @"'
where job_no = '" + jobno + "' and job_revision = '" + rev + "'and doctype_code not in  ('DD', 'TD')";
                    db.ExecNonQuery(sql, zetl4eisdb);

                    if (allBid != "")
                    {
                        sql = @"update ps_t_mytask_part set process_id='" + PID + @"'
where  bid_no in (" + allBid + ") and job_no='" + jobno + "'";
                        db.ExecNonQuery(sql, zetl4eisdb);

                        sql = @"update ps_t_mytask_equip set process_id='" + PID + @"'
where job_no='" + jobno + "' and job_revision='" + rev + "' and bid_no in (" + allBid + ")";
                        db.ExecNonQuery(sql, zetl4eisdb);
                    }
                    
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public DataTable chkPartAcc(string dept, string sect, string bidNo, string schNo,string login)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select  part_name,created_by from ps_t_mytask_part
where depart_position_name ='"+ dept + "' and section_position_name='"+ sect + @"'
and bid_no = '"+ bidNo + "' and schedule_no = '"+ schNo + "' and created_by='"+ login + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLIncoterm()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select incoterm_code,incoterm_name from ps_m_incoterm";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable getGVBrakDown(string dept, string sect, string bidNo, string schNo)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
equip_add_desc,equip_qty,equip_unit,equip_bid_qty,
supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,is_breakdown  
from  ps_t_mytask_equip 
where depart_position_name ='" + dept + @"' and section_position_name='" + sect + @"'
and bid_no = '" + bidNo + "' and schedule_no ='" + schNo + "'and is_breakdown='1'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void delMyTask(string row)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"delete from ps_t_mytask_equip where rowid ='" + row + "'";
                db.ExecNonQuery(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public void updateMyTaskEqui(addMytaskEquipJob obj)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid from ps_t_mytask_equip where rowid ='" + obj.rowid + "' and job_no='" + obj.job_no + "' " +
                    "and bid_no='" + obj.bid_no + "' and schedule_no='" + obj.schedule_no + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    sql = @"UPDATE ps_t_mytask_equip
SET equip_incoterm='" + obj.equip_incoterm + "',factory_test='" + obj.factory_test + "',manual_test='" + obj.manual_test + "',routine_test='" + obj.routine_test + @"',
equip_add_desc='" + obj.equip_add_desc + "',equip_qty='" + obj.equip_qty + "',equip_unit='" + obj.equip_unit + "',equip_bid_qty='" + obj.equip_bid_qty + @"',
supply_equip_unit_price=" + obj.supply_equip_unit_price + ",supply_equip_amonut=" + obj.supply_equip_amonut + ",local_exwork_unit_price=" + obj.local_exwork_unit_price + @",
local_exwork_amonut=" + obj.local_exwork_amonut + ",local_tran_unit_price=" + obj.local_exwork_unit_price + ",local_tran_amonut=" + obj.local_tran_amonut + @",
currency='" + obj.currency + "',equip_standard_drawing='" + obj.equip_standard_drawing + "',equip_mas_grp='" + obj.equip_mas_grp + "',equip_item_no='" + obj.equip_item_no + @"'
where rowid ='" + obj.rowid + "' and job_no='" + obj.job_no + "' and bid_no='" + obj.bid_no + "' and schedule_no='" + obj.schedule_no + "'";
                    db.ExecNonQuery(sql, zetl4eisdb);
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        public DataTable getEqui(string tbSch, string ddlSchjob, string ddlSchBom, string dept, string sect, string flag,string allPart,string filterpart)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                //                string sql = @"select rowid,bom_code,bom_name,bom_desc,depart_position_name,section_position_name,ps_m_bom.equip_code,equip_qty,equip_unit,
                //ps_m_bom.created_by,ps_m_bom.created_datetime,ps_m_bom.updated_by,ps_m_bom.updated_datetime,
                //equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit
                //from ps_m_bom,ps_m_equipcode
                //where ps_m_bom.equip_code = ps_m_equipcode.equip_code";
                if (flag == "Bom")
                {
                    if (allPart == "")
                    {
                        sql = @"select distinct '' as rowid,bom_code as equip_code,bom_name,bom_name as equip_desc_short,
bom_desc as equip_desc_full,depart_position_name,section_position_name,bomlevel,legend,cate_code,
misc_per_unit,'BOM' as equip_type,'' as equip_code,equip_qty,equip_unit as unit,equip_buyinglot,equip_reserve,part_code as parts,
'' as created_by,'' as created_datetime,'' as updated_by,'' as updated_datetime,'' as materialgroup,'' as equip_desc_short
from vw_ps_bom where (bom_name  like '%" + tbSch.Trim() + "%' or bom_desc like '%" + tbSch.Trim() + "%' or bom_code like '%"+ tbSch.Trim() + "%')";
                        if (filterpart != "") sql += " and part_code in (" + filterpart + ")";
                    }
                    else
                    {
                        sql = @"select distinct '' as rowid,bom_code as equip_code,bom_name,bom_name as equip_desc_short,
bom_desc as equip_desc_full,depart_position_name,section_position_name,bomlevel,legend,cate_code,
misc_per_unit,'BOM' as equip_type,'' as equip_code,equip_qty,equip_unit as unit,equip_buyinglot,equip_reserve,part_code as parts,
'' as created_by,'' as created_datetime,'' as updated_by,'' as updated_datetime,'' as materialgroup,'' as equip_desc_short
from vw_ps_bom where (bom_name  like '%" + tbSch.Trim() + "%' or bom_desc like '%" + tbSch.Trim() + "%' or bom_code like '%" + tbSch.Trim() + "%') and part_code in (" + allPart + @")";

                    }

                    //                    sql = @"select rowid,bom_code as equip_code,bom_name as equip_desc_full,bom_desc,depart_position_name,section_position_name,bomlevel,legend,cate_code,
                    //misc_per_unit,equip_type,equip_code,equip_qty,equip_unit as unit,equip_buyinglot,equip_reserve,created_by,
                    //created_datetime,updated_by,updated_datetime,'' as materialgroup,'' as equip_desc_short
                    //from ps_m_bom where bom_code = '" + ddlSchBom + "'";

                }
                else if (flag == "Equi")
                {
                    if (allPart == "")
                    {
                        if (dept == "กวศ-ส." || dept == "กวอ-ส.") {
                            if (tbSch == "") sql = "select top(200)* from";
                            else sql = "select * from";
                            sql += @" 
(
select 1 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and(equip_gong = 'กวศ-ส.' or equip_gong='กวอ-ส.') 
union
select 2 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where(equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and not(equip_gong = 'กวศ-ส.' or equip_gong='กวอ-ส.') 
) tb ";
                            //order by tb.orderby, tb.equip_code
                            if (filterpart == "") sql += "order by tb.orderby, tb.equip_code";
                            else sql += " where parts in (" + filterpart + @") order by tb.orderby, tb.equip_code";
                        }
                        else if (dept == "กวส-ส." || dept == "กวสส-ส.")
                        {
                            if (tbSch == "") sql = "select top(200)* from";
                            else sql = "select * from";
                            sql += @" 
(
select 1 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and(equip_gong = 'กวส-ส.' or equip_gong = 'กวสส-ส.') 
union
select 2 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where(equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and not(equip_gong = 'กวส-ส.' or equip_gong = 'กวสส-ส.') 
) tb ";
                            //order by tb.orderby, tb.equip_code
                            if (filterpart == "") sql += "order by tb.orderby, tb.equip_code";
                            else sql += " where parts in (" + filterpart + @") order by tb.orderby, tb.equip_code";
                        }
                        else
                        {
                            if (tbSch == "") sql = "select top(200)* from";
                            else sql = "select * from";
                            sql += @" 
(
select 1 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and(equip_gong = '" + dept + @"') 
union
select 2 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where(equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and not(equip_gong = '" + dept + @"') 
) tb ";
                            //order by tb.orderby, tb.equip_code
                            if (filterpart == "") sql += "order by tb.orderby, tb.equip_code";
                            else sql += " where parts in (" + filterpart + @") order by tb.orderby, tb.equip_code";
                        }

                    }
                    else
                    {
                        if (dept == "กวศ-ส." || dept == "กวอ-ส.")
                        {
                            sql = @"select * from 
(
select 1 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and(equip_gong = 'กวศ-ส.' or equip_gong='กวอ-ส.') and parts in (" + allPart + @")
union
select 2 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where(equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and not(equip_gong = 'กวศ-ส.' or equip_gong='กวอ-ส.') and parts in (" + allPart + @")
) tb order by tb.orderby, tb.equip_code";
                        }
                        else if (dept == "กวส-ส." || dept == "กวสส-ส.")
                        {
                            sql = @"select * from 
(
select 1 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and(equip_gong = 'กวส-ส.' or equip_gong = 'กวสส-ส.') and parts in (" + allPart + @")
union
select 2 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where(equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and not(equip_gong = 'กวส-ส.' or equip_gong = 'กวสส-ส.') and parts in (" + allPart + @")
) tb order by tb.orderby, tb.equip_code";
                        }
                        else
                        {
                            sql = @"select * from 
(
select 1 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and(equip_gong = '" + dept + @"') and parts in (" + allPart + @")
union
select 2 as orderby, equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
,created_by,created_datetime,updated_by,updated_datetime,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit,parts,'' as equip_type
from ps_m_equipcode
where(equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + @"%')
and not(equip_gong = '" + dept + @"') and parts in (" + allPart + @")
) tb order by tb.orderby, tb.equip_code";
                        }
                        
                    }
                    
                    //                    sql = @"select equip_id as rowid,equip_gong as depart_position_name,equip_pnag as section_position_name,equip_code
                    //,created_by,created_datetime,updated_by,updated_datetime,
                    //equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit
                    //from ps_m_equipcode
                    //where (equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'or equip_desc_short like '%" + tbSch.Trim() + "%')";
                    //                    sql = @"select rowid,bom_code,bom_name,bom_desc,depart_position_name,section_position_name,ps_m_bom.equip_code,equip_qty,equip_unit,
                    //ps_m_bom.created_by,ps_m_bom.created_datetime,ps_m_bom.updated_by,ps_m_bom.updated_datetime,
                    //equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,ps_m_bom.legend,unit
                    //from ps_m_bom,ps_m_equipcode
                    //where ps_m_bom.equip_code = ps_m_equipcode.equip_code
                    //and (ps_m_equipcode.equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'" +
                    //"or equip_desc_short like '%" + tbSch.Trim() + "%')";
                    //                    sql = @"select rowid,bom_code,bom_name,bom_desc,depart_position_name,section_position_name,ps_m_bom.equip_code,equip_qty,equip_unit,
                    //ps_m_bom.created_by,ps_m_bom.created_datetime,ps_m_bom.updated_by,ps_m_bom.updated_datetime,
                    //equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,ps_m_bom.legend,unit
                    //from ps_m_bom,ps_m_equipcode
                    //where ps_m_bom.equip_code = ps_m_equipcode.equip_code
                    //and (ps_m_equipcode.equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%'" +
                    //"or equip_desc_short like '%" + tbSch.Trim() + "%')";
                    //"and depart_position_name ='"+ dept + "' and section_position_name='"+ sect + "'
                    //" and (bom_name like '%"+ tbSch.Trim() + "%' or ps_m_bom.equip_code like '%" + tbSch.Trim() + "%' or equip_desc_full like '%" + tbSch.Trim() + "%')";
                }
                else if (flag == "Job")
                {
                    sql = @"select depart_position_name,section_position_name,mteq.equip_code,equip_qty,
equip_id,equip_desc_full,equip_desc_short,equip_gong,equip_pnag,isactive,materialgroup,legend,unit
from ps_t_mytask_equip mteq
left join ps_m_equipcode eq 
on eq.equip_code = mteq.equip_code
where mteq.job_no = '" + ddlSchjob + "'" +
"and depart_position_name ='" + dept + "' and section_position_name='" + sect + "'";
                }
              if(sql!="")  dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getPs_req(string Jobno, string workid, string Jobrev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select d.process_id,d.apv_order,d.ps_reqno,d.emp_id,m.note,m.activity_id,m.job_revision,m.job_no,
                                d.emp_name,d.emp_position,d.emp_position_full,
                                d.emp_apvdate,d.apv_status as requestor_status,m.wf_processid,m.ps_subject
                                from wf_ps_request_detail d
                                left join wf_ps_request m
                                on m.process_id = d.process_id
        where d.apv_order = 1
        and job_no = '" + Jobno + "' and job_revision = '" + Jobrev + "' and wf_processid = '" + workid + "' ;";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

            return dt;
        }

        public byte[] ConcatAndAddContent(List<byte[]> pdfByteContent)
        {
            using (var ms = new MemoryStream())
            {
                using (var doc = new Document())
                {
                    using (var copy = new PdfSmartCopy(doc, ms))
                    {
                        doc.Open();
                        //Loop through each byte array
                        foreach (var p in pdfByteContent)
                        {
                            //Create a PdfReader bound to that byte array
                            using (var reader = new PdfReader(p))
                            {
                                //Add the entire document instead of page-by-page
                                copy.AddDocument(reader);
                                
                            }
                        }
                        doc.Close();

                    }
                }
                //Return just before disposing
                return ms.ToArray();
            }
        }
        public string Update_wf_req(string Jobno, string ProcessId, string jobrev, string Userlogin, string nodeid)
        {
            string ischk = "";

            DataTable dt = new DataTable();
            try
            {
                string sql = @"select * from wf_ps_request
                                where job_no = '" + Jobno + @"'
                                and job_revision = " + jobrev + @"
                                and process_id = '" + ProcessId + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    string strQuery = @"update wf_ps_request set updated_by = '" + Userlogin + "' , updated_datetime = GETDATE() , doc_node_id ='" + nodeid + "' where job_no = '" + Jobno + @"'
                                and job_revision = " + jobrev + @"
                                and process_id = '" + ProcessId + "'";
                    db.ExecNonQuery(strQuery, zetl4eisdb);
                    string strQueryDetail = @"update wf_ps_request_detail 
                                                set apv_status = 'APPROVED' ,
                                                emp_apvdate = getdate()
                                                where process_id = '" + ProcessId + @"'
                                                and apv_order = 1";
                    db.ExecNonQuery(strQueryDetail, zetl4eisdb);
                    ischk = "1";
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return ischk;
        }
        public DataTable getRelDoc(string rowid) {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select doc_nodeid from ps_t_mytask_equip_doc_design where mytask_eq_id = '"+ rowid + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLHistory(string dept, string sect, string activityid, string jobno)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct job_no,job_revision from ps_t_mytask_equip
where activity_id ='" + activityid + "' and depart_position_name = '" + dept + "' and section_position_name = '" + sect + "'";
                if (jobno != "")
                {
                    sql += " and job_no = '" + jobno + "'";
                }

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getPartEqup(string dept, string sect, string activityid, string jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct bid_no,schedule_no,schedule_name,bid_revision,activity_id,depart_position_name,section_position_name,part_name
,part_description,created_by,created_datetime,updated_by,updated_datetime from ps_t_mytask_part
where job_no = '" + jobno + "' and job_revision = '" + rev + "' and activity_id ='" + activityid + "' and depart_position_name = '" + dept + "' and section_position_name = '" + sect + "' and step ='J'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getEquipCopy(string dept, string sect, string jobno, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,activity_id,schedule_no,schedule_name,depart_position_name,section_position_name,
equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,path_code,equip_incoterm,factory_test,manual_test,routine_test,
equip_add_desc,equip_qty,equip_unit,equip_bid_qty,
supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_item_no,equip_mas_grp,equip_full_desc,step 
from  ps_t_mytask_equip 
where job_no = '" + jobno + "' and job_revision = '" + rev + "' and depart_position_name = '" + dept + "' and section_position_name = '" + sect + "' and step ='J'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getAttachWAjob(string jobno)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select [job_no]
                      ,[job_revision]
                      ,[document_name]
                      ,[upload_datetime]
                      ,[att_url] 
                     from vw_ps_job_dashboard_att where job_no='" + jobno + "'";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
    }
   
    public class AddDoc
    {
        public long rowid { get; set; }
        public string job_no { get; set; }
        public string process_id { get; set; }
        public Nullable<int> job_revision { get; set; }
        public string bid_no { get; set; }
        public Nullable<int> bid_revision { get; set; }
        public Nullable<long> schedule_no { get; set; }
        public string depart_position_name { get; set; }
        public string section_position_name { get; set; }
        public string doctype_code { get; set; }
        public int doc_nodeid { get; set; }
        public Nullable<int> doc_revision_no { get; set; }
        public string doc_name { get; set; }
        public string doc_desc { get; set; }
        public string doc_sheetno { get; set; }
        public string created_by { get; set; }
        public Nullable<System.DateTime> created_datetime { get; set; }
        public string updated_by { get; set; }
        public Nullable<System.DateTime> updated_datetime { get; set; }
        public string activId { get; set; }
        public string schedule_name { get; set; }


    }
    public class JobInfo
    {
        public string job_no { get; set; }
        public string revision { get; set; }
        public string sub_line_name { get; set; }
        public string job_desc { get; set; }
        public string remark { get; set; }
        public string reference { get; set; }
        public string completion_date { get; set; }
        public string p6_task_id { get; set; }
        public string p6_task_name { get; set; }
        public string p6_plan_start { get; set; }
        public string p6_plan_end { get; set; }
    }
    public class addMytaskEquipJob
    {
        public long rowid { get; set; }
        public string job_no { get; set; }
        public string process_id { get; set; }
        public Nullable<int> job_revision { get; set; }
        public string bid_no { get; set; }
        public Nullable<int> bid_revision { get; set; }
        public Nullable<long> schedule_no { get; set; }
        public string depart_position_name { get; set; }
        public string section_position_name { get; set; }
        public string equip_mas_grp { get; set; }
        public string part_name { get; set; }
        public string part_remark { get; set; }
        public string part_description { get; set; }
        public string important { get; set; }
        public string schedule_name { get; set; }
        public string equip_item_no { get; set; }
        public string equip_code { get; set; }
        public string equip_add_desc { get; set; }
        public string equip_full_desc { get; set; }
        public string activity_id { get; set; }
        public string step { get; set; }
        public string legend { get; set; }
        public string equip_standard_drawing { get; set; }
        public Nullable<decimal> equip_qty { get; set; }
        public string equip_unit { get; set; }
        public Nullable<int> equip_reserve_qty { get; set; }
        public Nullable<int> equip_confirm_qty { get; set; }
        public Nullable<int> equip_bid_qty { get; set; }
        public string equip_incoterm { get; set; }
        public Nullable<bool> factory_test { get; set; }
        public Nullable<bool> manual_test { get; set; }
        public Nullable<bool> routine_test { get; set; }

        public Nullable<bool> waive_test { get; set; }
        public string path_code { get; set; }
        public Nullable<bool> is_breakdown { get; set; }
        public string breakdown_tab { get; set; }
        public string currency { get; set; }
        public Nullable<decimal> proposed_price { get; set; }
        public Nullable<decimal> evaluation_price { get; set; }
        public Nullable<bool> preference_status { get; set; }
        public Nullable<decimal> import_duty { get; set; }
        public string irrelevant { get; set; }
        public string turnkey_account_assign { get; set; }
        public string turnkey_cost_object { get; set; }
        public string turnkey_fund_center { get; set; }
        public string turnkey_gl { get; set; }
        public string supply_account_assign { get; set; }
        public string supply_cost_object { get; set; }
        public string supply_fund_center { get; set; }
        public string supply_gl { get; set; }
        public Nullable<decimal> supply_equip_unit_price { get; set; }
        public Nullable<decimal> supply_equip_amonut { get; set; }
        public Nullable<decimal> local_exwork_unit_price { get; set; }
        public Nullable<decimal> local_exwork_amonut { get; set; }
        public Nullable<decimal> local_tran_unit_price { get; set; }
        public Nullable<decimal> local_tran_amonut { get; set; }
        public string service_account_assign { get; set; }
        public string service_cost_object { get; set; }
        public string service_fund_center { get; set; }
        public string service_gl { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public System.DateTime updated_datetime { get; set; }
    }
    public partial class addconfig_cateJob
    {
        public string job_no { get; set; }
        public Nullable<int> job_revision { get; set; }
        public string bid_no { get; set; }
        public Nullable<int> bid_revision { get; set; }
        public string depart_position_name { get; set; }
        public string section_position_name { get; set; }
        public string cate_code { get; set; }
        public string cate_name { get; set; }
        public Nullable<decimal> percent_reservation { get; set; }
        public Nullable<int> is_round_up { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public System.DateTime updated_datetime { get; set; }

    }

}
