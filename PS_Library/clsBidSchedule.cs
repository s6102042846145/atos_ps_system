﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsBidSchedule
    {
        static DbControllerBase zdbUtil = new DbControllerBase();
        public string conn = ConfigurationManager.AppSettings["db_ps"].ToString();
        public object gvBid;
        public object gvMyBid;
        public string strBidno;


        public DataTable getBidInfo(string strSearchBidno)
        {

            DataTable dt = new DataTable();
            try
            {
                //string strSQL = @"select bid_no,dbo.udf_StripHTML(bid_desc) as bid_desc,bid_revision from bid_no
                //                  where bid_revision = (select max(bid_revision) from bid_no b1 where b1.bid_no = bid_no.bid_no)
                //                  ";

                string strSQL = @"select distinct [bid_no]
                	  ,[bid_desc]
                	  ,[bid_revision]
                	  ,[completion_date]
                from vw_ps_bid_dashboard_info ";

                if (strSearchBidno != "")
                {
                    strSQL += " where (bid_no LIKE '%" + strSearchBidno.Trim() + "%' OR bid_desc LIKE '%" + strSearchBidno.Trim() + "%') ";
                }

                strSQL += "order by bid_no";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public DataTable getSchedule(string strBidno, string strBidRevision)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select schedule_no,schedule_name, pq ,fe,lc,construction,total,sow,line_length 
,(select sub_line_name from job_no jn where jn.job_no=ps_m_bid_schedule.job_no and  (status like'1Active%' or status like'Active%')) as sub_line_name
                                         , job_no + '_' + convert(nvarchar(100),job_revision) as jobno_revision 
                                  FROM ps_m_bid_schedule
                                  WHERE bid_no = '" + strBidno + "' and bid_revision = " + strBidRevision;


                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable loadBid_Job_Relate(string strBidno, string strBidRevision)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select distinct job_no.job_no + ' (Rev. ' + convert(nvarchar(100),job_no.revision) + ')' as jobno_re_display 
                                         , job_no.job_no + '_' + convert(nvarchar(100),job_no.revision) as jobno_revision 
                                    from job_no
		                                    inner join bid_job_relate
		                                    on bid_job_relate.job_id = job_no.job_id
			                                    and bid_job_relate.bid_id = (select bid_id from bid_no where bid_no = '" + strBidno + "' and bid_revision = " + strBidRevision + ")";


                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void insertSchedule(string strBidno, string strBidRevision, string strValueData, string strUserLogin)
        {
            try
            {
                if (strValueData != "")
                {
                    string strSQL = "delete from ps_m_bid_schedule where bid_no='" + strBidno + "' and bid_revision = " + strBidRevision;
                    zdbUtil.ExecNonQuery(strSQL, conn);

                    string[] arrValues = strValueData.Split('|');
                    foreach (string value in arrValues)
                    {
                        string[] arrData = value.Split('_');

                        strSQL = @"INSERT INTO [dbo].[ps_m_bid_schedule]
                                           ([bid_no]
                                           ,[bid_revision]
                                           ,[job_no]
                                           ,[job_revision]
                                           ,[schedule_no]
                                           ,[schedule_name]
                                           ,[pq]
                                           ,[fe]
                                           ,[lc]
                                           ,[construction]
                                           ,[total]
                                           ,[sow]
                                           ,[line_length]
                                           ,[created_by]
                                           ,[created_datetime]
                                           ,[updated_by]
                                           ,[updated_datetime])
                                      values ('" + strBidno + "'" + @"
                                             ," + strBidRevision + @"
                                             ,'" + arrData[0].ToString() + "'" + @"
                                             ," + arrData[1].ToString() + @"
                                             ," + arrData[2].ToString() + @"
                                             ,'" + arrData[3].ToString() + "'" + @"
                                             ," + arrData[4].ToString() + @"
                                             ," + arrData[5].ToString() + @"
                                             ," + arrData[6].ToString() + @"
                                             ," + arrData[7].ToString() + @"
                                             ," + arrData[8].ToString() + @"
                                             ,'" + arrData[9].ToString() + @"'
                                             ,'" + arrData[10].ToString() + @"'
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";

                        zdbUtil.ExecNonQuery(strSQL, conn);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

    }

}
