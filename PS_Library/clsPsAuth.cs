﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsPsAuth
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private DbControllerBase db = new DbControllerBase();

        public clsPsAuth() { }

        public DataTable GetAuthByUserLogIn(string strUserLogin)
        {
            DataTable dt = new DataTable();

            string strSql = @"  select doctype_code,can_read,can_edit,can_delete 
                                  from ps_m_employee_auth
                                  where empid = '"+ strUserLogin + "'";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }

        public DataTable GetAuthByDocTypeLogIn(string strUserLogin,string strDocType)
        {
            DataTable dt = new DataTable();

            string strSql = @"  select doctype_code,can_read,can_edit,can_delete 
                                  from ps_m_employee_auth
                                  where empid = '" + strUserLogin + "' and doctype_code = '"+ strDocType + "'";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        }

    }
}
