﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsBasePrice
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string dbOraConnectionString = ConfigurationManager.AppSettings["db_common"].ToString();
        private OTFunctions zotFunctions = new OTFunctions();
        private DbControllerBase db = new DbControllerBase();
         
        public clsBasePrice() { }

        public DataTable GetEquipGrpByDept(string strDeptShortName)
        {
            DataTable dt = new DataTable();
            string strSql = @"  select eg_code as CODE ,eg_name as NAME 
                                  from ps_m_equip_grp
                                   where depart_position_name = '" + strDeptShortName + @"'
                                   order by eg_name asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }


        public DataTable GetEquipByBidSchedule(string strBidNo, string strSchduleName)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct equip_code
                                FROM ps_t_mytask_equip
                                where bid_no = '" + strBidNo + @"' ";
            if (!String.IsNullOrEmpty(strSchduleName))
            {
                strSql += " and schedule_name = '" + strSchduleName + "'";
            }
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetBidNo(string strDept)
        { 
            DataTable dt = new DataTable(); 
            string strSql = @"SELECT distinct bid_no
                              FROM ps_t_mytask_equip  t
                              left join ps_m_equipcode e on t.equip_code = e.equip_code
                              where  bid_no is not null
                              and e.equip_gong = '" + strDept + @"'
                              order by bid_no asc
                            "; 
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetSchedulByBidNo(string strBidNo,string strDept)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct schedule_name
                              FROM ps_t_mytask_equip  t
                              left join ps_m_equipcode e on t.equip_code = e.equip_code
                              where  bid_no = '"+ strBidNo + @"'
                              and e.equip_gong = '"+ strDept + @"'
                              order by schedule_name asc
                            ";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
        public DataTable GetAvgPriceByEquipCodes(string strEquipCodeConcat, string strDept)
        {
            DataTable dt = new DataTable();
            if (!String.IsNullOrEmpty(strEquipCodeConcat))
            {
                string strSql = @"select egl.eg_code
                              ,egl.equip_code
                              ,eg.depart_position_name
                              ,eg.eg_name
                              ,e.equip_desc_short
                              ,e.equip_desc_full
                              ,e.isactive
                              ,e.equip_gong
                              ,dt2.currency
                              ,dt2.avg_price as avg_last_price
                              ,case 
							  when ul2.base_price is not null 
							  then ul2.base_price
							  else 
							      case when dt2.avg_price is not null then dt2.avg_price else null end
								  end  as base_price
                              ,case 
							  when ul2.base_price is not null 
							  then 'ราคาฐานที่กำหนดล่าสุด'
							  else 
							      case when dt2.avg_price is not null then 'ราคาเฉลี่ย' else 'สำรวจราคาตลาด' end
								  end  as channel
                              from ps_m_equip_grp_list egl
                              left join ps_m_equip_grp eg on egl.eg_code = eg.eg_code
                              left join ps_m_equipcode e on egl.equip_code = e.equip_code 
							  left join (select dt.equip_code,dt.currency
                            ,avg(price_per_unit) avg_price
                            from (select row_number() over (partition by equip_code,currency
                                                             order by po_date desc) as rn,
                                                equip_code,
                                                price_per_unit,po_date,currency
                                           from ps_t_po_sap ) dt
                            where rn <4
                            group by dt.equip_code,currency) dt2 on dt2.equip_code = e.equip_code

							left join (select equip_code,
                            base_price,year,currency
                            from
                            (select row_number() over (partition by equip_code
                            order by year desc) as rn,
                            equip_code,
                            base_price,year,currency
                            from ps_t_bp_upload) ul
                            where rn=1) ul2
                             on ul2.equip_code = e.equip_code
                            where e.isactive= 1 
							and egl.equip_code in (" + strEquipCodeConcat + @")  
                            and eg.depart_position_name = '" + strDept + @"'
                            and ul2.currency = 'THB'
                            order by eg.depart_position_name asc, eg.eg_name asc ,egl.equip_code asc ";
                dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            }
            return dt;
        }

        public DataTable GetAvgPriceByEquipGrps(string strEquipGrpConcat, string strDepartment)
        {
            DataTable dt = new DataTable();


            //   string strSql11 = @"select egl.eg_code
            //                     ,egl.equip_code
            //                     ,eg.depart_position_name
            //                     ,eg.eg_name
            //                     ,e.equip_desc_short
            //                     ,e.equip_desc_full
            //                     ,e.isactive
            //                     ,e.equip_gong
            //                     ,dt2.currency
            //                     ,dt2.avg_price as avg_last_price
            //                     ,case when dt2.avg_price is not null then dt2.avg_price else null end  as base_price
            //                     ,case when dt2.avg_price is null then 'สำรวจราคาตลาด' else 'ราคาเฉลี่ย' end as channel
            //                     from ps_m_equip_grp_list egl
            //                     left join ps_m_equip_grp eg on egl.eg_code = eg.eg_code
            //                     left join ps_m_equipcode e on egl.equip_code = e.equip_code 
            //left join (select dt.equip_code,dt.currency
            //                   ,avg(price_per_unit) avg_price
            //                   from (select row_number() over (partition by equip_code,currency
            //                                                    order by po_date desc) as rn,
            //                                       equip_code,
            //                                       price_per_unit,po_date,currency
            //                                  from ps_t_po_sap ) dt
            //                   where rn <4
            //                   group by dt.equip_code,currency) dt2 on dt2.equip_code = e.equip_code
            //                     where e.isactive= 1  and egl.eg_code in (" + strEquipGrpConcat + ")	 order by eg.depart_position_name asc, eg.eg_name asc ,egl.equip_code asc";

            string strSql1 = @"select egl.eg_code
                              ,egl.equip_code
                              ,eg.depart_position_name
                              ,eg.eg_name
                              ,e.equip_desc_short
                              ,e.equip_desc_full
                              ,e.isactive
                              ,e.equip_gong
                              ,dt2.currency
                              ,dt2.avg_price as avg_last_price
                              ,case 
							  when ul2.base_price is not null 
							  then ul2.base_price
							  else 
							      case when dt2.avg_price is not null then dt2.avg_price else null end
								  end  as base_price
                              ,case 
							  when ul2.base_price is not null 
							  then 'ราคาฐานที่กำหนดล่าสุด'
							  else 
							      case when dt2.avg_price is not null then 'ราคาเฉลี่ย' else 'สำรวจราคาตลาด' end
								  
								  end  as channel
                              from ps_m_equip_grp_list egl
                              left join ps_m_equip_grp eg on egl.eg_code = eg.eg_code
                              left join ps_m_equipcode e on egl.equip_code = e.equip_code 
							  left join (select dt.equip_code,dt.currency
                            ,avg(price_per_unit) avg_price
                            from (select row_number() over (partition by equip_code,currency
                                                             order by po_date desc) as rn,
                                                equip_code,
                                                price_per_unit,po_date,currency
                                           from ps_t_po_sap ) dt
                            where rn <4
                            group by dt.equip_code,currency) dt2 on dt2.equip_code = e.equip_code

							left join (select equip_code,
                            base_price,year,currency
                            from
                            (select row_number() over (partition by equip_code
                            order by year desc) as rn,
                            equip_code,
                            base_price,year,currency
                            from ps_t_bp_upload) ul
                            where rn=1) ul2
                             on ul2.equip_code = e.equip_code
                            where e.isactive= 1  and egl.eg_code in (" + strEquipGrpConcat + ")	and ul2.currency = 'THB' order by eg.depart_position_name asc, eg.eg_name asc ,egl.equip_code asc";

            //   string strSql22 = @"select '' as eg_code
            //                       ,e.equip_code
            //                       ,e.equip_gong as depart_position_name
            //                       ,'' as eg_name
            //                       ,e.equip_desc_short
            //                       ,e.equip_desc_full
            //                       ,e.isactive
            //                       ,dt2.currency
            //                       ,dt2.avg_price as avg_last_price
            //                       ,case when dt2.avg_price is not null then dt2.avg_price else null end  as base_price
            //                       ,case 
            //when ul2.base_price is not null 
            //then 'ราคาฐานที่กำหนดล่าสุด'
            //else 
            //    case when dt2.avg_price is not null then 'ราคาเฉลี่ย' else 'สำรวจราคาตลาด' end

            // end  as channel
            //                       from ps_m_equipcode e
            //                       left join (select dt.equip_code,dt.currency
            //                                                   ,avg(price_per_unit) avg_price
            //                                                   from (select row_number() over (partition by equip_code,currency
            //                                                                                    order by po_date desc) as rn,
            //                                                                       equip_code,
            //                                                                       price_per_unit,po_date,currency
            //                                                                  from ps_t_po_sap ) dt
            //                                                   where rn <4
            //                                                   group by dt.equip_code,currency) dt2 on dt2.equip_code = e.equip_code
            //                                                     where e.isactive= 1  
            //                                and e.equip_gong = '" + strDepartment + @"'  
            //                                order by e.equip_code asc";

            string strSql2 = @"select '' as eg_code
                                ,e.equip_code
                                ,e.equip_gong as depart_position_name
                                ,'' as eg_name
                                ,e.equip_desc_short
                                ,e.equip_desc_full
                                ,e.isactive
                                ,dt2.currency
                                ,dt2.avg_price as avg_last_price
                                ,case 
							  when ul2.base_price is not null 
							  then ul2.base_price
							  else 
							      case when dt2.avg_price is not null then dt2.avg_price else null end
								  
								  end  as base_price
                                ,case 
							  when ul2.base_price is not null 
							  then 'ราคาฐานที่กำหนดล่าสุด'
							  else 
							      case when dt2.avg_price is not null then 'ราคาเฉลี่ย' else 'สำรวจราคาตลาด' end
								  
								  end  as channel
                                from ps_m_equipcode e
                                left join (select dt.equip_code,dt.currency
                                                            ,avg(price_per_unit) avg_price
                                                            from (select row_number() over (partition by equip_code,currency
                                                                                             order by po_date desc) as rn,
                                                                                equip_code,
                                                                                price_per_unit,po_date,currency
                                                                           from ps_t_po_sap ) dt
                                                            where rn <4
                                                            group by dt.equip_code,currency) dt2 on dt2.equip_code = e.equip_code
                                left join (select equip_code,
                                                base_price,year,currency
                                                from
                                                (select row_number() over (partition by equip_code
                                                order by year desc) as rn,
                                                equip_code,
                                                base_price,year,currency
                                                from ps_t_bp_upload) ul
                                                where rn=1) ul2
                                                 on ul2.equip_code = e.equip_code
															  
															  where e.isactive= 1  
							                                  and e.equip_gong = '" + strDepartment + @"'  
							                                  order by e.equip_code asc;";



            string strSql = !String.IsNullOrEmpty(strEquipGrpConcat) ? strSql1 : strSql2;
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        //public DataTable GetAveragePriceByEquipCodes()
        //{
        //    DataTable dt = new DataTable();
        //    string strSql = @"select dt.equip_code
        //                    ,avg(price) avg_price
        //                    from (select row_number() over (partition by equip_code
        //                                                     order by date_add desc) as rn,
        //                                        equip_code,
        //                                        price,date_add
        //                                   from ps_t_test ) dt
        //                    where rn <4
        //                    and equip_code in ('SWSV003','TB0001')
        //                    group by dt.equip_code";

        //    dt = db.ExecSql_DataTable(strSql, dbConnectionString);
        //    return dt;
        //}

        public byte[] GenExcel(DataTable dtToUpload)
        {
            string strFileName = "Bese_Price_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + ".xlsx";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage();
            var workbook = package.Workbook;
            var worksheet = workbook.Worksheets.Add("Sheet1");

            worksheet.Column(1).Width = 10;
            worksheet.Column(2).Width = 10;
            worksheet.Column(3).Width = 20;
            worksheet.Column(4).Width = 20;
            worksheet.Column(5).Width = 40;
            worksheet.Column(6).Width = 10;
            worksheet.Column(7).Width = 20;
            worksheet.Column(8).Width = 20;
            worksheet.Column(9).Width = 20;


            // title
            worksheet.Cells["A1:B1"].Merge = true;
            worksheet.Cells["A1:B1"].Value = "ราคาฐานประจำปี ";
            worksheet.Cells["A1:B1"].Style.Font.Bold = true;
            worksheet.Cells["A1:B1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            worksheet.Cells["C1"].Value = DateTime.Now.Year;
            worksheet.Cells["C1"].Style.Font.Bold = true;
            worksheet.Cells["C1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;


            // gv header
            // worksheet.Cells["A3:I3"].Value = "Item No'";
            worksheet.Cells["A3:I3"].Style.Font.Bold = true;
            worksheet.Cells["A3:I3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:I3"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["A3:I3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A3:I3"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
            //set border
            worksheet.Cells["A3:I3"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:I3"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:I3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:I3"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //set header value
            worksheet.Cells["A3"].Value = "Item No";
            worksheet.Cells["B3"].Value = "Dept.";
            worksheet.Cells["C3"].Value = "Equipment Group";
            worksheet.Cells["D3"].Value = "Equipment Code";
            worksheet.Cells["E3"].Value = "Description";
            worksheet.Cells["F3"].Value = "Currency";
            worksheet.Cells["G3"].Value = "Avg. Last PO Price";
            worksheet.Cells["H3"].Value = "Base Price";
            worksheet.Cells["I3"].Value = "ที่มา";

            int iRow = 4;
            string strCellId = string.Empty;

            for (int i = 0; i < dtToUpload.Rows.Count; i++)
            {
                strCellId = "A" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = (i + 1).ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "B" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["depart_position_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "C" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["eg_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "D" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["equip_code"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "E" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["equip_desc_full"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "F" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["currency"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "G" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["avg_last_price"];
                worksheet.Cells[strCellId].Style.Numberformat.Format = "#,##0.00";
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "H" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["base_price"];
                worksheet.Cells[strCellId].Style.Numberformat.Format = "#,##0.00";
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "I" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtToUpload.Rows[i]["channel"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtToUpload.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
            }


            return package.GetAsByteArray();

            //package.SaveAs(new FileInfo("C:\\Xls\\" + strFileName));

        }

        public void ReadExcel(MemoryStream msFile, string strUserLogin)
        {
            ExcelPackage package = new ExcelPackage(msFile);
            ExcelWorksheet worksheet = package.Workbook.Worksheets.FirstOrDefault();

            // get number of rows and columns in the sheet
            int rows = worksheet.Dimension.Rows; // 20
            int columns = worksheet.Dimension.Columns; // 7

            DataTable dtUpload = new DataTable();
            DataColumn dc = new DataColumn("depart_position_name");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("eg_name");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("equip_code");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("equip_desc_full");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("currency");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("avg_last_price");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("base_price");
            dtUpload.Columns.Add(dc);
            dc = new DataColumn("year");
            dtUpload.Columns.Add(dc);
            DataRow dr;

            int startRow = 4;
            string strYear = worksheet.Cells["C1"].Value.ToString();
            for (int i = startRow; i <= worksheet.Dimension.End.Row; i++)
            {
                dr = dtUpload.NewRow();
                dr["year"] = strYear;
                dr["depart_position_name"] = worksheet.Cells["B" + i.ToString()].Value.ToString();
                dr["eg_name"] = worksheet.Cells["C" + i.ToString()].Value.ToString();
                dr["equip_code"] = worksheet.Cells["D" + i.ToString()].Value.ToString();
                dr["equip_desc_full"] = worksheet.Cells["E" + i.ToString()].Value.ToString();
                dr["currency"] = worksheet.Cells["F" + i.ToString()].Value.ToString();
                dr["avg_last_price"] = worksheet.Cells["G" + i.ToString()].Value.ToString();
                dr["base_price"] = worksheet.Cells["H" + i.ToString()].Value.ToString();
                dtUpload.Rows.Add(dr);
            }

            if (dtUpload.Rows.Count > 0)
            {
                // insert to DB 
                foreach (DataRow drUpload in dtUpload.Rows)
                {
                    InsertUpdatePrice(drUpload, strUserLogin);
                }
            }
        }

        public void InsertUpdatePrice(DataRow drUpload, string strUserLogin)
        {
            string strEquipCode, strYear, strEgName, strDeptName, strEquipDesc, strCurrency, strAvgLastPrice, strBasePrice;
            strEquipCode = strYear = strEgName = strDeptName = strEquipDesc = strCurrency = strAvgLastPrice = strBasePrice = string.Empty;
            try { strEquipCode = drUpload["equip_code"].ToString(); } catch { };
            try { strYear = drUpload["year"].ToString(); } catch { };
            try { strEgName = drUpload["eg_name"].ToString(); } catch { };
            try { strDeptName = drUpload["depart_position_name"].ToString(); } catch { };
            try { strEquipDesc = drUpload["equip_desc_full"].ToString(); } catch { };
            try { strCurrency = drUpload["currency"].ToString(); } catch { };
            try
            {
                if (String.IsNullOrEmpty(drUpload["avg_last_price"].ToString()))
                {
                    strAvgLastPrice = "NULL";
                }
                else
                {
                    strAvgLastPrice = drUpload["avg_last_price"].ToString();

                }
            }
            catch { strAvgLastPrice = "NULL"; };
            try
            {
                if (String.IsNullOrEmpty(drUpload["base_price"].ToString()))
                {
                    strBasePrice = "NULL";
                }
                else
                {
                    strBasePrice = drUpload["base_price"].ToString();

                }
            }
            catch { strAvgLastPrice = "NULL"; };
            if (!String.IsNullOrEmpty(strEquipCode) && !String.IsNullOrEmpty(strYear))
            {
                string strSql = @"BEGIN
                                  IF EXISTS (SELECT * FROM ps_t_bp_upload WHERE equip_code = '" + strEquipCode + @"' AND year = '" + strYear + @"')
                                     BEGIN
                                          UPDATE ps_t_bp_upload 
		                                  SET updated_by = '" + strUserLogin + @"'
		                                     ,updated_datetime = GETDATE()
			                                 ,eg_name = '" + strEgName + @"'
			                                 ,depart_position_name = '" + strDeptName + @"'
			                                 ,equip_desc_full ='" + strEquipDesc + @"'
			                                 ,currency = '" + strCurrency + @"'
			                                 ,avg_last_price = " + strAvgLastPrice + @"
			                                 ,base_price = " + strBasePrice + @"
		                                  where equip_code = '" + strEquipCode + "' AND year = '" + strYear + @"'
                                     END
                                  ELSE
	                                 BEGIN
		                                  INSERT INTO ps_t_bp_upload (equip_code,year,eg_name,depart_position_name,equip_desc_full,currency,avg_last_price,base_price,created_by,created_datetime,updated_by,updated_datetime)
			                                 values('" + strEquipCode + "','" + strYear + "','" + strEgName + "','" + strDeptName + "','" + strEquipDesc + "','" + strCurrency + "'," + strAvgLastPrice + "," + strBasePrice + ",'" + strUserLogin + "',getdate(),'" + strUserLogin + @"',getdate()) 
	                                 END
                                 END";

                db.ExecNonQuery(strSql, dbConnectionString);
            }
        }
    }
}
