﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PS_Library
{
    public class NodeInfo
    {
        public string NodeID { get; set; }
        public string NodeName { get; set; }
        public string FileName { get; set; }
        public string NodeURL { get; set; }
        public string NodeType { get; set; }
        public string NodeParentID { get; set; }
        public string NodeParentName { get; set; }
       

        public NodeInfo()
        {

        }
    }
}