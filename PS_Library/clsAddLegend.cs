﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsAddLegend
    {
        static DbControllerBase zdbUtil = new DbControllerBase();
        public string conn = ConfigurationManager.AppSettings["db_ps"].ToString();

        public DataTable getLegendByJob(string strJobNo, string strJobRevision)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select jobno
                                         , job_revision
                                         , legend
                                         , legend_qty
                                  from ps_t_legend_qty
                                  where 1=1 ";
                if (strJobNo != "")
                    strSQL += " and jobno ='" + strJobNo + "' ";

                if (strJobRevision != "")
                    strSQL += " and job_revision ='" + strJobRevision + "' ";

                strSQL += "order by legend";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public DataTable viewLegend(string strLegend)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select equip_code
                                         , equip_desc_full
                                         , unit
                                         , materialgroup
                                  from ps_m_equipcode
                                  where  legend = '" + strLegend + "'";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void insertLegend(string strJobNo, string strJobRevision, string strValueData, string strUserLogin)
        {
            try
            {
                string[] arrValues = strValueData.Split('|');
                foreach (string value in arrValues)
                {
                    string[] arrData = value.Split('_');

                    string strSQL = @"insert into ps_t_legend_qty(jobno, job_revision, legend, legend_qty
                                                                 ,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strJobNo + "'" + @"
                                             ,'" + strJobRevision + "'" + @"
                                             ,'" + arrData[0].ToString() + "'" + @"
                                             ,'" + arrData[1].ToString() + "'" + @"
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";

                    zdbUtil.ExecNonQuery(strSQL, conn);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
    }
}
