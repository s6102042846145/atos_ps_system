﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.BID
{
    public class Bid
    {
        public string Bidno { get; set; }
        public string Bidname { get; set; }
        public string Bidname2 { get; set; }
        public string Bidname3 { get; set; }
        public string Bidstat { get; set; }
        public string Bidtype { get; set; }
        public string Approve { get; set; }
        public string Appdate { get; set; }
        public string Remark { get; set; }
        public string PBidissue { get; set; }
        public string PBidcloseN { get; set; }
        public string PBidopent { get; set; }
        public string PBidopenc { get; set; }
        public string PLi { get; set; }
        public string PContract { get; set; }
        public string PComplete { get; set; }
        public string ABidissue { get; set; }
        public string ABidcloseN { get; set; }
        public string ABidopent { get; set; }
        public string ABidopenc { get; set; }


    }
}
