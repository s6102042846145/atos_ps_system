﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{  
    public class clsEquipmentMasterdb 
    {
        public string zconnstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string commonconstr = ConfigurationManager.AppSettings["db_common"].ToString();
        public DbControllerBase zdbUtil = new DbControllerBase();
        public oraDBUtil oraDBUtil = new oraDBUtil();
        public DataTable GetDataList()
        {
            string sql = @" select du.rowid, du.doctype_code, dt.doctype_name, du.doc_desc, du.doc_revision_no, du.updated_by,
                            du.depart_position_name, du.section_position_name from ps_m_doc_approve du
                            inner join ps_m_doctype dt on(du.doctype_code = dt.doctype_code)";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);

            return dt;
        }
        public DataTable GetDataList(string searchType, string searchText, string dept)
        {
            string sql = @" select du.rowid, du.doctype_code, dt.doctype_name, du.doc_desc, du.doc_revision_no, du.updated_by,doc_nodeid,doc_name,
                            du.depart_position_name, du.section_position_name 
,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk  
                            from ps_m_doc_approve du
                            inner join ps_m_doctype dt on(du.doctype_code = dt.doctype_code)";
            if (dept != "")
            {
                //if (searchType == "") 
                    sql = string.Format("{0} where depart_position_name = '{1}'", sql, dept);
                //else sql = string.Format("{0} and depart_position_name = '{1}'", sql, dept);
            }
            if (searchType != "")
            {
                sql = string.Format("{0} and du.doctype_code = '{1}'", sql, searchType);
            }
            if (searchText != "")
            {
               // if (searchType == "") sql = string.Format("{0} where doc_name like '%{1}%'", sql, searchText);
                //else 
                    sql = string.Format("{0} and doc_name like '%{1}%'", sql, searchText);
            }
       

            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);

            return dt;
        }
        public string CreateEquipCode(EquipmentMasterModel model)
        {
            string strErrorMsg = string.Empty;
            //Check Equipment Code
            string checksql = "select * from ps_m_equipcode where equip_code = '" + model.equip_code + "'";
            DataTable checkdt = zdbUtil.ExecSql_DataTable(checksql, zconnstr);
            int cnt = checkdt.Rows.Count;

            if (cnt > 0)
            {
                strErrorMsg = "Equip code : " + model.equip_code + " is already exist";
            }
            else
            {
                //Create
                string sql = " insert into ps_m_equipcode (equip_code, equip_desc_full, equip_desc_short, equip_gong, equip_pnag, " +
                    "isactive, is_service, remark, unit, materialgroup, groupsort, isimport, legend, cate_code, misc_per_unit, parts, " +
                    "sap_material_type, sap_material_group, sap_base_unit, sap_plant, sap_batch_manage, sap_batch_class, sap_value_class, " +
                    "sap_price_control, sap_mrp_type, sap_availability_check, turnkey_buying_lot, turnkey_buying_unit, turnkey_reservation," +
                    " turnkey_remark, sn_code_a,sn_code_b,sn_code_c,sn_code_d,sn_code_e,supply_buying_lot, supply_buying_unit, " +
                    "supply_reservation, supply_remark, created_by, created_datetime, updated_by, updated_datetime) values (";
                //sql += model.equip_id == null ? "''" : model.equip_code + "," +
                sql += model.equip_code == "" ? "''" + "," : "'" + model.equip_code + "',";
                sql += model.equip_desc_full == "" ? "''" + "," : "'" + model.equip_desc_full + "',";
                sql += model.equip_desc_short == "" ? "''" + "," : "'" + model.equip_desc_short + "',";
                sql += model.equip_gong == "" ? "''" + "," : "'" + model.equip_gong + "',";
                sql += model.equip_pnag == "" ? "''" + "," : "'" + model.equip_pnag + "',";
                sql += model.isactive == "" ? "''" + "," : "'" + model.isactive + "',";
                sql += model.is_service == "" ? "''" + "," : "'" + model.is_service + "',";
                sql += model.remark == "" ? "''" + "," : "'" + model.remark + "',";
                sql += model.unit == "" ? "''" + "," : "'" + model.unit + "',";
                sql += model.materialgroup == "" ? "''" + "," : "'" + model.materialgroup + "',";
                sql += model.groupsort == "" ? "''" + "," : "'" + model.groupsort + "',";
                sql += model.isimport == "" ? "''" + "," : "'" + model.isimport + "',";
                sql += model.legend == "" ? "''" + "," : "'" + model.legend + "',";
                sql += model.cate_code == "" ? "''" + "," : "'" + model.cate_code + "',";
                sql += model.misc_per_unit == "" ? "''" + "," : "'" + model.misc_per_unit + "',";
                sql += model.parts == "" ? "''" + "," : "'" + model.parts + "',";
                sql += model.sap_material_type == "" ? "''" + "," : "'" + model.sap_material_type + "',";
                sql += model.sap_material_group == "" ? "''" + "," : "'" + model.sap_material_group + "',";
                sql += model.sap_base_unit == "" ? "''" + "," : "'" + model.sap_base_unit + "',";
                sql += model.sap_plant == "" ? "''" + "," : "'" + model.sap_plant + "',";
                sql += model.sap_batch_manage == "" ? "''" + "," : "'" + model.sap_batch_manage + "',";
                sql += model.sap_batch_class == "" ? "''" + "," : "'" + model.sap_batch_class + "',";
                sql += model.sap_value_class == "" ? "''" + "," : "'" + model.sap_value_class + "',";
                sql += model.sap_price_control == "" ? "''" + "," : "'" + model.sap_price_control + "',";
                sql += model.sap_mrp_type == "" ? "''" + "," : "'" + model.sap_mrp_type + "',";
                sql += model.sap_availability_check == "" ? "''" + "," : "'" + model.sap_availability_check + "',";
                sql += model.turnkey_buying_lot == "" ? "''" + "," : "'" + model.turnkey_buying_lot + "',";
                sql += model.turnkey_buying_unit == "" ? "''" + "," : "'" + model.turnkey_buying_unit + "',";
                sql += model.turnkey_reservation == "" ? "''" + "," : "'" + model.turnkey_reservation + "',";
                sql += model.turnkey_remark == "" ? "''" + "," : "'" + model.turnkey_remark + "',";
                sql += model.sn_code_a == "" ? "''" + "," : "'" + model.sn_code_a + "',";
                sql += model.sn_code_b == "" ? "''" + "," : "'" + model.sn_code_b + "',";
                sql += model.sn_code_c == "" ? "''" + "," : "'" + model.sn_code_c + "',";
                sql += model.sn_code_d == "" ? "''" + "," : "'" + model.sn_code_d + "',";
                sql += model.sn_code_e == "" ? "''" + "," : "'" + model.sn_code_e + "',";
                sql += model.supply_buying_lot == "" ? "''" + "," : "'" + model.supply_buying_lot + "',";
                sql += model.supply_buying_unit == "" ? "''" + "," : "'" + model.supply_buying_unit + "',";
                sql += model.supply_reservation == "" ? "''" + "," : "'" + model.supply_reservation + "',";
                sql += model.supply_remark == "" ? "''" + "," : "'" + model.supply_remark + "',";
                sql += model.created_by == "" ? "''" + "," : "'" + model.created_by + "',";
                sql += model.created_datetime == "" ? "''" + "," : "'" + model.created_datetime + "',";
                sql += model.updated_by == "" ? "''" + "," : "'" + model.updated_by + "',";
                sql += model.updated_datetime == "" ? "''" + "," : "'" + model.updated_datetime + "')";
                try
                {
                    zdbUtil.ExecNonQuery(sql, zconnstr);
                }
                catch (Exception ex)
                {
                    strErrorMsg = "This equipment code cannot create, please contact admin.";
                    LogHelper.WriteEx(ex);
                }
            }

            return strErrorMsg;
        }
        public string UpdateEquipCode(EquipmentMasterModel model)
        {
            string strErrorMsg = string.Empty;
            string strSql = @"UPDATE ps_m_equipcode
                           SET updated_datetime = GETDATE()";
            strSql += ",equip_desc_full = '" + model.equip_desc_full + "'";
            strSql += ",equip_desc_short = '" + model.equip_desc_short + "'";
            strSql += ",equip_gong = '" + model.equip_gong + "'";
            strSql += ",equip_pnag = '" + model.equip_pnag + "'";
            strSql += ",isactive = '" + model.isactive + "'";
            strSql += ",remark = '" + model.remark + "'";
            strSql += ",unit = '" + model.unit + "'";
            strSql += ",materialgroup = '" + model.materialgroup + "'";
            strSql += ",legend = '" + model.legend + "'";
            strSql += ",cate_code = '" + model.cate_code + "'";
            strSql += ",misc_per_unit = '" + model.misc_per_unit + "'";
            strSql += ",parts = '" + model.parts + "'";
            strSql += ",sap_material_type = '" + model.sap_material_type + "'";
            strSql += ",sap_material_group = '" + model.sap_material_group + "'";
            strSql += ",sap_base_unit = '" + model.sap_base_unit + "'";
            strSql += ",sap_plant = '" + model.sap_plant + "'";
            strSql += ",sap_batch_manage = '" + model.sap_batch_manage + "'";
            strSql += ",sap_batch_class = '" + model.sap_batch_class + "'";
            strSql += ",sap_value_class = '" + model.sap_value_class + "'";
            strSql += ",sap_price_control = '" + model.sap_price_control + "'";
            strSql += ",sap_mrp_type = '" + model.sap_mrp_type + "'";
            strSql += ",sap_availability_check = '" + model.sap_availability_check + "'";
            strSql += ",turnkey_buying_lot = " + model.turnkey_buying_lot + "";
            strSql += ",turnkey_buying_unit = '" + model.turnkey_buying_unit + "'";
            strSql += ",turnkey_reservation = " + model.turnkey_reservation + "";
            strSql += ",turnkey_remark = '" + model.turnkey_remark + "'";
            strSql += ",supply_buying_lot = " + model.supply_buying_lot + "";
            strSql += ",supply_buying_unit = '" + model.supply_buying_unit + "'";
            strSql += ",supply_reservation = " + model.supply_reservation + "";
            strSql += ",supply_remark = '" + model.supply_remark + "'";
            strSql += ",updated_by = '" + model.updated_by + "'";
            strSql += ",sn_code_a = '" + model.sn_code_a + "'";
            strSql += ",sn_code_b = '" + model.sn_code_b + "'";
            strSql += ",sn_code_c = '" + model.sn_code_c + "'";
            strSql += ",sn_code_d = '" + model.sn_code_d + "'";
            strSql += ",sn_code_e = '" + model.sn_code_e + "'";
            strSql += " WHERE equip_code = '" + model.equip_code + "' ";

            try
            {
                zdbUtil.ExecNonQuery(strSql, zconnstr);
            }
            catch (Exception ex)
            {
                strErrorMsg = "Update fail. Please contact admin.";
                LogHelper.WriteEx(ex);
            }
            return strErrorMsg;
        }
        public void CreateEquipDoc(EquipmentMasterModel model)
        {

        }
        public void UpdateEquipDoc(EquipmentMasterModel model)
        {

        }
        public EquipmentMasterModel GetSearchResult(string equipment_code)
        {
            EquipmentMasterModel model = new EquipmentMasterModel();
            string sql = "select * from ps_m_equipcode where equip_code = '" + equipment_code + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);

            if (!string.IsNullOrEmpty(equipment_code))
            {
                if (dt.Rows.Count == 1)
                {
                    //for (int i = 0; i < dt.Columns.Count; i++)
                    //{
                    //    var value = dt.Rows[0][dt.Columns[i].ColumnName];
                    //    if (!string.IsNullOrEmpty(value.ToString()) && value.ToString() != equipment_code)
                    //    {
                    //        var datatype = dt.Rows[0][dt.Columns[i].ColumnName].GetType();

                    //        switch (datatype.Name)
                    //        {
                    //            case "Int64":
                    //                {
                    //                    if (value.ToString() == "0")
                    //                    {
                    //                        value = 0;
                    //                    }
                    //                    break;
                    //                };
                    //            case "String":
                    //                if (string.IsNullOrEmpty( value.ToString()))
                    //                {
                    //                    value = "No Data";
                    //                }
                    //                    break;

                    //            default:
                    //                break;
                    //        }

                    //    }

                    //    //if (dt.Rows[0][dt.Columns[i].ColumnName].ToString() == string.Empty)
                    //    //{
                    //    //    try
                    //    //    {
                    //    //        if ( Int64.Parse(dt.Rows[0][dt.Columns[i].ColumnName].ToString()) != 0)
                    //    //        {
                    //    //            dt.Rows[0][dt.Columns[i].ColumnName] = 0;
                    //    //        }
                    //    //    }
                    //    //    catch (Exception ex) { string f = ex.ToString(); }
                    //    //}
                    //    //else
                    //    //{

                    //    //    dt.Rows[0][dt.Columns[i].ColumnName] = "No Data";
                    //    //}
                    //}
                    model.equip_id = dt.Rows[0]["equip_id"].ToString();
                    model.equip_code = dt.Rows[0]["equip_code"].ToString();
                    model.equip_desc_full = dt.Rows[0]["equip_desc_full"].ToString();
                    model.equip_desc_short = dt.Rows[0]["equip_desc_short"].ToString();
                    model.equip_gong = dt.Rows[0]["equip_gong"].ToString();
                    model.equip_pnag = dt.Rows[0]["equip_pnag"].ToString();
                    model.isactive = dt.Rows[0]["isactive"].ToString();
                    model.is_service = dt.Rows[0]["is_service"].ToString();
                    model.remark = dt.Rows[0]["remark"].ToString();
                    model.unit = dt.Rows[0]["unit"].ToString();
                    model.materialgroup = dt.Rows[0]["materialgroup"].ToString();
                    model.groupsort = dt.Rows[0]["groupsort"].ToString();
                    model.isimport = dt.Rows[0]["isimport"].ToString();
                    model.legend = dt.Rows[0]["legend"].ToString();
                    model.cate_code = dt.Rows[0]["cate_code"].ToString();
                    model.misc_per_unit = dt.Rows[0]["misc_per_unit"].ToString();
                    model.parts = dt.Rows[0]["parts"].ToString();
                    model.sap_material_type = dt.Rows[0]["sap_material_type"].ToString();
                    model.sap_material_group = dt.Rows[0]["sap_material_group"].ToString();
                    model.sap_base_unit = dt.Rows[0]["sap_base_unit"].ToString();
                    model.sap_plant = dt.Rows[0]["sap_plant"].ToString();
                    model.sap_batch_manage = dt.Rows[0]["sap_batch_manage"].ToString();
                    model.sap_batch_class = dt.Rows[0]["sap_batch_class"].ToString();
                    model.sap_value_class = dt.Rows[0]["sap_value_class"].ToString();
                    model.sap_price_control = dt.Rows[0]["sap_price_control"].ToString();
                    model.sap_mrp_type = dt.Rows[0]["sap_mrp_type"].ToString();
                    model.sap_availability_check = dt.Rows[0]["sap_availability_check"].ToString();
                    model.turnkey_buying_lot = dt.Rows[0]["turnkey_buying_lot"].ToString();
                    model.turnkey_buying_unit = dt.Rows[0]["turnkey_buying_unit"].ToString();
                    model.turnkey_reservation = dt.Rows[0]["turnkey_reservation"].ToString();
                    model.turnkey_remark = dt.Rows[0]["turnkey_remark"].ToString();
                    model.supply_buying_lot = dt.Rows[0]["supply_buying_lot"].ToString();
                    model.supply_buying_unit = dt.Rows[0]["supply_buying_unit"].ToString();
                    model.supply_reservation = dt.Rows[0]["supply_reservation"].ToString();
                    model.supply_remark = dt.Rows[0]["supply_remark"].ToString();
                    model.created_by = dt.Rows[0]["created_by"].ToString();
                    model.created_datetime = dt.Rows[0]["created_datetime"].ToString();
                    model.updated_by = dt.Rows[0]["updated_by"].ToString();
                    model.updated_datetime = dt.Rows[0]["updated_datetime"].ToString();
                }
            }
            return model;
        }
        public DataTable GetSapMaterialGroup()
        {
            string sql = "select CODE, concat(concat(CODE, ' : '),NAME) NAME from p_m_sap_materialgroup where code like 'TS%' order by NAME asc";
            DataTable dt = oraDBUtil._getDS(sql, commonconstr).Tables[0];

            return dt;
        }
        public DataTable GetSapUnit()
        {
            string sql = "select CODE, NAME, concat(concat(CODE, ' : '), DESCRIPTION) DESCRIPTION from  p_m_sap_unit";
            DataTable dt = oraDBUtil._getDS(sql, commonconstr).Tables[0];

            return dt;
        } 
        public DataTable GetSapPlant()
        {
            string sql = "select CODE, concat(concat(CODE,' : '),NAME) NAME from p_m_sap_plant ORDER BY DECODE(CODE, 'HQ31', 1) ASC";
            DataTable dt = oraDBUtil._getDS(sql, commonconstr).Tables[0];

            return dt;
        }
        public DataTable GetSapBatchClass()
        {
            string sql = "select CLASS_NUMBER, concat(concat(CLASS_NUMBER, ' : '), DESCRIPTION) DESCRIPTION from p_m_sap_batchclass order by CLASS_NUMBER asc";
            DataTable dt = oraDBUtil._getDS(sql, commonconstr).Tables[0];

            return dt;
        }
        public DataTable GetSapIncoterms()
        {
            string sql = "select CODE, concat(concat(CODE, ' : '), NAME) NAME from p_m_sap_incoterms order by NAME asc";
            DataTable dt = oraDBUtil._getDS(sql, commonconstr).Tables[0];

            return dt;
        }
        public DataTable GetFixMajorParts()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("CODE", typeof(string));
            dt.Columns.Add("NAME", typeof(string));

            List<string> listparts = new List<string>()
            {
                "A : PRELIMINARY WORK",
                "B : TOWER FOUNDATIONS",
                "C : TOWERS",
                "D : INSULATOR STRING AND OVERHEAD GROUND WIRE ASSEMBLIES",
                "E : CONDUCTOR AND OVERHEAD GROUND WIRE",
                "F : LINE ACCESSORIES",
                "G : GROUNDING MATERIALS",
                "H : OPTICAL FIBER AND LINE ACCESSORIES",
                "I : DISMANTLING WORK",
                "J : TEMPORARY DETOUR LINES",
                "K : LINE CONNECTION",
                "L : AIR NAVIGATION OBSTRUCTION MARKING AND LIGHTING",
                "M : ADDITIONAL STRINGING WORK",
                "N : TEST OF EQUIPMENT",
                "O : SUPPLY OF SERVICE OF VEHICLES",
                "P : SUPPLY OF SPARE EQUIPMENT",
                "Z : DISMANTLED EQUIPMENT TO BE PURCHASED BY THE CONTRACTOR",
                "AB1 : Power Transformer and Marshalling Control Cubicle",
                "AB2 : Distribution Transformer",
                "AB3 : Shunt Reactor",
                "AB4 : Surge Arrester",
                "AB5 : Current Transformer and Junction Box",
                "AB6 : Coupling Capacitor Voltage Transformer, Coupling Capacitor, Voltage Transformer and Junction Box",
                "AB7 : SF6 Gas Insulated Switchgear",
                "AB8 : Shunt Capacitor Bank",
                "AB9 : Power Circuit Breaker",
                "AB10 : Disconnecting Switch",
                "AB11 : Power Fuse, Fuse Link and Hook Stick",
                "AB12 : AC&DC Distribution Board and Termination Box",
                "AB13 : Stationary Battery and Battery Charger",
                "AB14 : Substation Steel Structure",
                "AB15 : Insulator",
                "AB16 : Cable Terminations",
                "AB17 : XLPE Power Cable",
                "AB18 : Low Voltage Cable and Conductor",
                "AB19 : Switchyard Lighting Fixtures",
                "AB20 : Aluminum Tube, Connector and Miscellaneous Hardware",
                "AB21 : Bus Fitting",
                "AB22 : Grounding Material",
                "AB23 : Substation Miscellaneous",
                "AB24 : Control and Protection System",
                "AB25 : Fault Recording System",
                "AB26 : Emergency Generating Set",
                "AB27 : Resistor",
                "AB28 : Compact Switchgear ",
                "AB29 : Battery Energy Storage System",
                "AB32 : Line Trap and Coupling Equipment",
                "AB33 : CCTV",
                "AB34 : 48 VDC Stationary Battery, Battery Charger and DC Power Panel",
                "AB35 : Communication Cable",
                "AB36 : Static VAR Compensator (SVC)",
                "AB37 : Medium Voltage Switchgear",
                "AB38 : Remote Terminal Unit",
                "AB39 : Commissioning",
                "AB40 : Installation of Equipment and Steel Structure Supplied by EGAT",
                "C1 : Foundation Work",
                "C2 : Cable Trench",
                "C3 : Control Building",
                "C4 : Earth Work, Road and Crushed Rock Surfacing",
                "C5 : Water Supply System",
                "C6 : Drainage System",
                "C7 : Special Construction Works",
                "C8 : Miscellaneous",
                "C9 : Fire Protection System",
                "D1 : Spare Parts for Power Transformer and Marshalling Control Cubicle",
                "D3 : Spare Parts for Shunt Reactor",
                "D6 : Spare Parts for Coupling Capacitor Voltage Transformer, Coupling Capacitor, Voltage Transformer and Junction Box",
                "D7 : Spare Parts for SF6 Gas Insulated Switchgear",
                "D8 : Spare Parts for Shunt Capacitor Bank",
                "D9 : Spare Parts for Power Circuit Breaker",
                "D10 : Spare Parts for Disconnecting Switch",
                "D11 : Spare Parts for Power Fuse, Fuse Link and Hook Stick",
                "D12 : Spare Parts for AC&DC Distribution Board and Termination Box",
                "D16 : Spare Parts for Cable Terminations",
                "D24 : Spare Parts for Control and Protection System",
                "D25 : Spare Parts for Fault Recording System",
                "D28 : Spare Parts for Compact Switchgear ",
                "D29 : Spare Parts for Battery Energy Storage System (BESS)",
                "D35 : Spare Parts for Communication Cable",
                "D36 : Spare Parts for Static VAR Compensator (SVC)",
                "D37 : Spare Parts for Medium Voltage Switchgear",
                "D1 : Optional Item for Power Transformer and Marshalling Control Cubicle",
                "D3 : Optional Item for Shunt Reactor",
                "D6 : Optional Item for Coupling Capacitor Voltage Transformer, Coupling Capacitor, Voltage Transformer and Junction Box",
                "D7 : Optional Item for SF6 Gas Insulated Switchgear",
                "D8 : Optional Item for Shunt Capacitor Bank",
                "D9 : Optional Item for Power Circuit Breaker",
                "D10 : Optional Item for Disconnecting Switch",
                "D11 : Optional Item for Power Fuse, Fuse Link and Hook Stick",
                "D12 : Optional Item for AC&DC Distribution Board and Termination Box",
                "D16 : Optional Item for Cable Terminations",
                "D24 : Optional Item for Control and Protection System",
                "D25 : Optional Item for Fault Recording System",
                "D28 : Optional Item for Compact Switchgear ",
                "D29 : Optional Item for Battery Energy Storage System (BESS)",
                "D35 : Optional Item for Communication Cable",
                "D36 : Optional Item for Static VAR Compensator (SVC)",
                "D37 : Optional Item for Medium Voltage Switchgear",
                "D38 : Optional Item for Remote Terminal Unit",
                "E2 : Distribution Transformer",
                "E3 : Shunt Reactor",
                "E4 : Surge Arrester",
                "E5 : Current Transformer and Junction Box",
                "E6 : Coupling Capacitor Voltage Transformer, Coupling Capacitor, Voltage Transformer and Junction Box",
                "E9 : Power Circuit Breaker",
                "E10 : Disconnecting Switch",
                "E15 : Insulator",
                "E24 : Control and Protection System",
                "E28 : Compact Switchgear ",
                "E32 : Line Trap and Coupling Equipment "
            };

            foreach (var item in listparts)
            {
                string[] arr = item.Split(':');
                dt.Rows.Add(arr[0].Trim(), arr[0].Trim() + " : " + arr[1].Trim());

            }

            return dt;
        }
        public DataTable GetEquipment(string strEquipCode, string strEquipDesc, string strLegend,string strStatus)
        {
            string strSql = @"SELECT equip_id
                              ,equip_code
                              ,equip_desc_full
                              ,equip_desc_short
                              ,equip_gong
                              ,equip_pnag
                              ,legend
                              ,unit 
                              ,isactive
                            ,(case when isactive=1 then 'Active' else 'InActive' end ) as acStatus
                          FROM ps_m_equipcode
                          where isactive is not null ";
            if (!String.IsNullOrEmpty(strEquipCode))
                strSql += " and equip_code like '%" + strEquipCode + "%'";
            if (!String.IsNullOrEmpty(strEquipDesc))
            {
                strSql += " and (equip_desc_full like '%" + strEquipDesc + "%'";
                strSql += " or equip_desc_short like '%" + strEquipDesc + "%')";
            }
            if (!String.IsNullOrEmpty(strLegend))
                strSql += " and legend like '%" + strLegend + "%' ";
            if (strStatus != "")
                strSql += " and isactive ='"+ strStatus + "'";

            strSql += " order by equip_code asc,legend asc ";
            strSql += " ,equip_desc_full asc,equip_desc_short asc,equip_gong asc";
            DataTable dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;

        }
        public DataTable GetEquipDocByEquipCode(string strEquipCode,string dept)
        {
            DataTable dt = new DataTable();
            //string sql = @"SELECT doc.rowid,eqd.equip_code
	           //               ,doct.doctype_name
	           //               ,doc.doc_name
	           //               ,doc.doc_desc
	           //               ,doc.doc_revision_display
	           //               ,doc.depart_position_name
	           //               ,doc.section_position_name
            //                  ,doc.updated_by
            //                  ,doc.updated_datetime
            //              FROM ps_m_doc_approve doc 
            //              left join ps_m_equip_doc eqd on eqd.doc_nodeid = doc.doc_nodeid
            //              left join ps_m_doctype doct on doc.doctype_code = doct.doctype_code
            //              where equip_code = '" + strEquipCode + "'";

            string sql = @"select * from vw_ps_equip_doc where equip_code = '" + strEquipCode + "'";
            if (dept != "")
            {
                sql += " and depart_position_name='" + dept + "'";
            }
            dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable getDoctype()
        {
            DataTable dt = new DataTable();
            string strSql = @"select doctype_code,doctype_name from ps_m_doctype order by doctype_code";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }
        public DataTable getDoctypeDash()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"select distinct doctype_code,doctype_name from ps_m_doctype where is_select_dashboard =1";
                sql += " order by doctype_code";
                dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetSNCodeA()
        {
            DataTable dt = new DataTable();
            string strSql = @"  select code_a,code_value 
                                  from ps_m_sn_code_a
                                  order by code_a asc";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }
        public DataTable GetSNCodeB(string strCodeA)
        {
            DataTable dt = new DataTable();
            string strSql = @"select code_b, code_value
                              from ps_m_sn_code_b
                              where code_a = '" + strCodeA + @"'
                              order by code_b asc";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }

        public DataTable GetSNCodeC(string strCodeA, string strCodeB)
        {
            DataTable dt = new DataTable();
            string strSql = @"  select code_c,code_value
                                  from ps_m_sn_code_c
                                  where code_a = '" + strCodeA + @"' and code_b = '" + strCodeB + @"'
                                  order by code_c asc";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }
         
        public DataTable GetSNCodeD()
        {
            DataTable dt = new DataTable();
            string strSql = @"select code_d, code_value
                              from ps_m_sn_code_d
                              order by code_d asc";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }

        public DataTable GetSNCodeE(string strCodeA, string strCodeB)
        {
            DataTable dt = new DataTable();
            string strSql = @"  select code_e,code_value
                                  from ps_m_sn_code_e
                                  where code_a = '" + strCodeA + @"' and code_b = '" + strCodeB + @"'
                                  order by code_e asc";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }
        public DataTable GetEQGroup()
        {
            DataTable dt = new DataTable();
            string strSql = @"select distinct egl.eg_code,eg_desc   
                            from ps_m_equip_grp eqg
                            left join ps_m_equip_grp_list egl on eqg.eg_code = egl.eg_code
                            where isdelete =0 and egl.eg_code is not null";
            dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
            return dt;
        }
        public void deleteDocument(string eqcode,string strdocid)
        {
            try
            {
                string strSql = @"delete from ps_m_equip_doc where equip_code in('" + eqcode + "') and doc_nodeid in('"+ strdocid + "')";
                zdbUtil.ExecNonQuery(strSql, zconnstr);
                //string[] arrValues = strdocid.Split('|');
                //foreach (string value in arrValues)
                //{
                //    string[] arrData = value.Split('_');
                //    string strSql = @"delete from ps_m_equip_doc where equip_code in('" + eqcode + "') and doc_nodeid ='" + arrData[0] + "' and equip_code ='" + arrData[1] + "'";
                //    zdbUtil.ExecNonQuery(strSql, zconnstr);
                //}

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public string inserDocument(EquipmentDoc obj)
        {
            string chkupdate = "";
            try
            {
                DataTable dt = new DataTable();
                string strSqlinsert = "";
                string strSql = @"select * from ps_m_equip_doc where equip_code ='" + obj.equip_code + "' and doc_nodeid ='" + obj.doc_nodeid + "'";
                dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
                if (dt.Rows.Count > 0)
                {
                    strSql = @"delete from ps_m_equip_doc where equip_code ='" + obj.equip_code + "' and doc_nodeid ='" + obj.doc_nodeid + "'";
                    zdbUtil.ExecNonQuery(strSql, zconnstr);
                }
                strSqlinsert = @"INSERT INTO ps_m_equip_doc( 
                      [equip_code]
                      ,[doc_nodeid]
                      ,[created_by]
                      ,[created_datetime]
                      ,[updated_by]
                      ,[updated_datetime]) 
                VALUES
            ('" + obj.equip_code + @"'
           ,'" + obj.doc_nodeid + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + "')";
                zdbUtil.ExecNonQuery(strSqlinsert, zconnstr);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
     
            return chkupdate;
        }

        public void insertToEQlist(EquipmentEQGroup obj)
        {
            try
            {
                DataTable dt = new DataTable();
                string strSqlinsert = "";
                string strSql = @"select * from ps_m_equip_grp_list where equip_code ='" + obj.equip_code + "' and eg_code ='" + obj.eg_code + "'";
                dt = zdbUtil.ExecSql_DataTable(strSql, zconnstr);
                if (dt.Rows.Count > 0)
                {
                    strSql = @"delete from ps_m_equip_grp_list where equip_code ='" + obj.equip_code + "' and eg_code ='" + obj.eg_code + "'";
                    zdbUtil.ExecNonQuery(strSql, zconnstr);
                }
                strSqlinsert = @"INSERT INTO ps_m_equip_grp_list( 
                       [eg_code]
                      ,[equip_code]
                      ,[created_by]
                      ,[created_datetime]
                      ,[updated_by]
                      ,[updated_datetime]) 
                VALUES
            ('" + obj.eg_code + @"'
           ,'" + obj.equip_code + @"'
           ,'" + obj.created_by + @"'
           ,'" + obj.created_datetime + @"'
           ,'" + obj.updated_by + @"'
           ,'" + obj.updated_datetime + "')";
                zdbUtil.ExecNonQuery(strSqlinsert, zconnstr);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        public byte[] GenExcel(DataTable dtExport)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage();
            var workbook = package.Workbook;
            var worksheet = workbook.Worksheets.Add("Sheet1");

            worksheet.Column(1).Width = 5;
            worksheet.Column(2).Width = 15;
            worksheet.Column(3).Width = 35;
            worksheet.Column(4).Width = 45;
            worksheet.Column(5).Width = 15;
            worksheet.Column(6).Width = 15;
            worksheet.Column(7).Width = 15;
            worksheet.Column(8).Width = 15;
            worksheet.Column(9).Width = 15;


            // title
            worksheet.Cells["A1:C1"].Merge = true;
            worksheet.Cells["A1:C1"].Value = "Equipment Master Data ";
            worksheet.Cells["A1:C1"].Style.Font.Bold = true;
            worksheet.Cells["A1:C1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;


            // gv header
            // worksheet.Cells["A3:I3"].Value = "Item No'";
            worksheet.Cells["A3:H3"].Style.Font.Bold = true;
            worksheet.Cells["A3:H3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:H3"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["A3:H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A3:H3"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
            //set border
            worksheet.Cells["A3:H3"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //set header value
            worksheet.Cells["A3"].Value = "No.";
            worksheet.Cells["B3"].Value = "Equipment Code";
            worksheet.Cells["C3"].Value = "Equipment Desc";
            worksheet.Cells["D3"].Value = "Equipment Desc Full";
            worksheet.Cells["E3"].Value = "Department";
            worksheet.Cells["F3"].Value = "Section";
            worksheet.Cells["G3"].Value = "Legend";
            worksheet.Cells["H3"].Value = "Unit";

            int iRow = 4;
            string strCellId = string.Empty;

            for (int i = 0; i < dtExport.Rows.Count; i++)
            {
                strCellId = "A" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = (i + 1).ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "B" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_code"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "C" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_desc_short"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "D" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_desc_full"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "E" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_gong"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "F" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_pnag"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "G" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["legend"];
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "H" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["unit"];
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
            }


            return package.GetAsByteArray();

            //package.SaveAs(new FileInfo("C:\\Xls\\" + strFileName));

        }
    }
    public class EquipmentMasterModel
    {
        public string equip_id { get; set; }
        public string equip_code { get; set; }
        public string equip_desc_full { get; set; }
        public string equip_desc_short { get; set; }
        public string equip_gong { get; set; }
        public string equip_pnag { get; set; }
        public string isactive { get; set; }
        public string is_service { get; set; }
        public string remark { get; set; }
        public string unit { get; set; }
        public string materialgroup { get; set; }
        public string groupsort { get; set; }
        public string isimport { get; set; }
        public string legend { get; set; }
        public string cate_code { get; set; }
        public string misc_per_unit { get; set; }
        public string parts { get; set; }
        public string sap_material_type { get; set; }
        public string sap_material_group { get; set; }
        public string sap_base_unit { get; set; }
        public string sap_plant { get; set; }
        public string sap_batch_manage { get; set; }
        public string sap_batch_class { get; set; }
        public string sap_value_class { get; set; }
        public string sap_price_control { get; set; }
        public string sap_mrp_type { get; set; }
        public string sap_availability_check { get; set; }
        public string turnkey_buying_lot { get; set; }
        public string turnkey_buying_unit { get; set; }
        public string turnkey_reservation { get; set; }
        public string turnkey_remark { get; set; }
        public string supply_buying_lot { get; set; }
        public string supply_buying_unit { get; set; }
        public string supply_reservation { get; set; }
        public string supply_remark { get; set; }
        public string created_by { get; set; }
        public string created_datetime { get; set; }
        public string updated_by { get; set; }
        public string updated_datetime { get; set; }
        public string sn_code_a { get; set; }
        public string sn_code_b { get; set; }
        public string sn_code_c { get; set; }
        public string sn_code_d { get; set; }
        public string sn_code_e { get; set; }
        public string incoterm { get; set; }
        //Doc
        //public string rowid { get; set; }
        //public string equip_code { get; set; }
        //public string doc_nodeid { get; set; } 
        //public string created_by { get; set; }
        //public string created_datetime { get; set; }
        //public string updated_by { get; set; }
        //public string updated_datetime { get; set; }

    }
    public class EquipmentDoc
    {
        public string equip_code { get; set; }
        public string doc_nodeid { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public System.DateTime updated_datetime { get; set; }
    }
    public class EquipmentEQGroup
    {
        public string eg_code { get; set; }
        public string equip_code { get; set; }
        public string created_by { get; set; }
        public System.DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public System.DateTime updated_datetime { get; set; }
        
    }
}
