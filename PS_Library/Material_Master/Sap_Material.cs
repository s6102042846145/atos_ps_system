﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Globalization;

namespace PS_Library.Material_Master
{
  

    public static class Sap_Material
    {

        public static string psDB = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();

        private static ps_sap_log sap_log = new ps_sap_log();
        public static string connstr { get; set; }

        public static string cmdstrFull { get; set; }
        public static string cmdstr { get; set; }

        public static string cmdvalue { get; set; }
        public static string condi { get; set; }
        public static string order { get; set; }

        public static string SendToSap(string equip_code)
        {
            string material = "";
            // DataTable dtCNO = new DataTable();
            DataTable dt = new DataTable();
            //equip_code = "T213SQB23099";
            dt = getDataMatToSap(equip_code);
            //dt = getDataStockToSap();
            // genXMLRequestStock(dt);

            material = genXMLRequestMat(dt, "");



            insert_ps_sap_log(sap_log);
            if (material != "")
            {
                update_ps_m_equipcode(equip_code, material);
                
            }

            return material;
            //createRequestPO();
        }


        private static string getFormatDate(DateTime date, string format)
        {
            string strDate = "";

            string strMonth = date.Month.ToString().Length == 1 ? ("0" + date.Month) : date.Month.ToString();
            string strDay = date.Day.ToString().Length == 1 ? ("0" + date.Day) : date.Day.ToString();
            if (format == "YYYY-MM-DD")
            {
                strDate = date.Year + "-" + date.Month + "-" + date.Day;


            }

            return strDate;


        }


        //Material
        #region Material


        public static List<int> insert_ps_sap_log(ps_sap_log sap_log)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_sap_log (";
                cmdstr = cmdstr + getNameForInsert(sap_log) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(sap_log) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }




        public static string getNameForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;
            PropertyInfo[] properties = _class.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!property.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {
                        value = property.Name;
                    }
                    else
                    {
                        value = property.Name + ",";
                    }
                    result = result + value;
                }
                count++;

            }
            return result;
        }

        public static string cutLastString(string item)
        {
            string result = "";
            result = item.Substring(0, item.Length - 1);
            return result;
        }
        public static string getValueForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;

            PropertyInfo[] properties = _class.GetType().GetProperties();


            foreach (var p in properties)
            {
                if (!p.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {

                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {
                            value = "GETDATE()" + "";
                        }
                        else
                        {
                            value = "'" + p.GetValue(_class) + "'";
                        }


                    }
                    else
                    {
                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {

                            value = "GETDATE()" + ",";

                        }
                        else
                        {
                            value = "'" + p.GetValue(_class) + "'" + ",";
                        }

                    }
                    result = result + value;
                }
                count++;
            }


            return result;
        }


        public static DataTable getContractNo()
        {

            string sqlFull = "";
            string sql = @"SELECT [contract_no],[status] FROM [dbo].[ps_t_contract_no] ";
            string condi = " where UPPER([status]) = UPPER('new')";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }


        public static DataTable getDataMatToSap(string equip_code)
        {

            string sqlFull = "";
            string sql = @"SELECT * FROM [dbo].[ps_m_equipcode] ";
            string condi = " where [equip_code] = '" + equip_code + "'";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }

        public static string getPOID()
        {

            string pid = "";
            string result = UpdatePOID();
            // string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            DataTable dtRunno = new DataTable();

            try
            {
                if (result.Equals("success"))
                {
                    cmdstrFull = "";
                    cmdstr = @"select runno from wf_runno ";
                    condi = " WHERE prefix = 'PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + condi;

                    dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);
                    if (checkRowData(dtRunno))
                    {
                        pid = dtRunno.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // result = ex.Message;
            }

            return pid;
        }
        public static string getYearThai()
        {
            string yearThai = "";



            string strdate = DateTime.Now.Year.ToString();

            yearThai = (Convert.ToInt32(strdate) + 543).ToString();
            return yearThai;
        }
        public static string UpdatePOID()
        {
            string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            int runno = 0;
            int runnoNew = 0;
            DataTable dtRunno = new DataTable();
            try
            {
                cmdstrFull = "";
                cmdstr = @"select runno from wf_runno ";
                condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                order = " ";
                cmdstrFull = cmdstr + condi;

                dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);

                if (checkRowData(dtRunno))
                {
                    runno = Convert.ToInt32(dtRunno.Rows[0][0].ToString());
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"update wf_runno ";
                    cmdvalue = " set runno = " + runnoNew;
                    condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }
                else
                {
                    runno = 0;
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"insert into wf_runno ( prefix, runno,year_thai)";
                    cmdvalue = "VALUES  ( select 'PO', '" + runnoNew + "' , '" + yearThai + "' )";
                    condi = " ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        public static string update_ps_m_equipcode(string equip_code,string sap_mat_code)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_m_equipcode ";
                cmdstr = cmdstr + "Set sap_mat_code = '";
                cmdvalue = sap_mat_code + "' " ;
                condi = "where equip_code = '" + equip_code +"'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static string update_ps_t_contract_no(string status, string res, string cNo)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_t_contract_no ";
                cmdstr = cmdstr + "Set status = '";
                cmdvalue = status + "' ," + " result_sap = '" + res + "'";
                condi = "where contract_no = '" + cNo + "' and status = 'New'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static bool checkRowData(DataTable dt)
        {
            bool check = false;
            if (dt.Rows.Count > 0)
            {
                check = true;
            }
            else
            {
                check = false;
            }
            return check;
        }
     

        
        public static List<Materialclass> addMaterialclass(DataTable dt)
        {
            List<Materialclass> listMaterialclassItems = new List<Materialclass>();
            Materialclass MaterialclassItems = new Materialclass();



            foreach (DataRow dr in dt.Rows)
            {
                MaterialclassItems = new Materialclass();

                MaterialclassItems.OldMatNo = dr["equip_code"].ToString();
                MaterialclassItems.Classnum = dr["sap_batch_class"].ToString();
                MaterialclassItems.ValueChar = dr["equip_code"].ToString();
                //PoitemItems.Material = dr["Material"].ToString();
                //PoitemItems.Plant = dr["Plant"].ToString();
                //PoitemItems.Quantity = dr["Quantity"] == null ? 0 : dr["Quantity"].ToString() == "" ? 0 : Convert.ToDecimal(dr["Quantity"].ToString());
                //PoitemItems.PoUnit = dr["PoUnit"].ToString();
                //PoitemItems.NetPrice = dr["NetPrice"] == null ? 0 : dr["NetPrice"].ToString() == "" ? 0 : Convert.ToDecimal(dr["NetPrice"].ToString());
                //PoitemItems.PriceUnit = dr["PriceUnit"] == null ? 0 : dr["PriceUnit"].ToString() == "" ? 0 : Convert.ToDecimal(dr["PriceUnit"].ToString());
                //PoitemItems.InfoUpd = dr["InfoUpd"].ToString();
                //PoitemItems.Acctasscat = dr["Acctasscat"].ToString();
                //PoitemItems.GrInd = dr["GrInd"].ToString();
                //PoitemItems.IrInd = dr["IrInd"].ToString();
                //PoitemItems.GrBasediv = dr["GrBasediv"].ToString();
                //PoitemItems.AcknowlNo = dr["AcknowlNo"].ToString();
                //PoitemItems.Incoterms1 = dr["Incoterms1"].ToString();
                //PoitemItems.VendPart = dr["VendPart"].ToString();
                //PoitemItems.PreqName = dr["PreqName"].ToString();
                //PoitemItems.Vendrbatch = dr["Vendrbatch"].ToString();
                //PoitemItems.Itemno = dr["Itemno"].ToString();
                //PoitemItems.PoEcm = dr["PoEcm"].ToString();

                listMaterialclassItems.Add(MaterialclassItems);
            }


            return listMaterialclassItems;
        }
        public static List<Materialdata> addMaterialdata(DataTable dt)
        {
            List<Materialdata> listMaterialdataItems = new List<Materialdata>();
            Materialdata MaterialdataItems = new Materialdata();

           
            foreach (DataRow dr in dt.Rows)
            {
                MaterialdataItems = new Materialdata();

                MaterialdataItems.OldMatNo = dr["equip_code"].ToString();
                MaterialdataItems.MatlGroup = dr["sap_material_group"].ToString();
                MaterialdataItems.MatlType = dr["sap_material_type"].ToString();
                MaterialdataItems.MatlDesc = dr["equip_desc_short"].ToString();
                MaterialdataItems.Plant = dr["sap_plant"].ToString();
                MaterialdataItems.MrpType = dr["sap_mrp_type"].ToString();
                MaterialdataItems.Availcheck = dr["sap_availability_check"].ToString();
                MaterialdataItems.ValClass = dr["sap_value_class"].ToString();
                MaterialdataItems.PriceCtrl = dr["sap_price_control"].ToString();
                MaterialdataItems.TextLine = dr["equip_desc_full"].ToString();


                listMaterialdataItems.Add(MaterialdataItems);
            }
            return listMaterialdataItems;
        }
        



        public static string genXMLRequestMat(DataTable dt, string cNo)
        {
            Console.WriteLine("##########  Send To SAP ###########");
            var soapNs = @"http://schemas.xmlsoap.org/soap/envelope/";
            var soapURN = @"urn:sap-com:document:sap:soap:functions:mc-style";
            var bodyNs = @"";

            XmlDocument doc = new XmlDocument();

            //// Create an XML declaration.
            //XmlDeclaration xmldecl;
            //xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            //xmldecl.Encoding = "UTF-8";

            //// Add the new node to the document.
            //XmlElement rootHead = doc.DocumentElement;
            //doc.InsertBefore(xmldecl, rootHead);



            XmlElement root = doc.CreateElement("soapenv", "Envelope", soapNs);
            root.SetAttribute("xmlns:soapenv", soapNs);
            root.SetAttribute("xmlns:urn", soapURN);
            doc.AppendChild(root);

            var header = root.AppendChild(doc.CreateElement("soapenv", "Header", soapNs));
            var body = root.AppendChild(doc.CreateElement("soapenv", "Body", soapNs));
            var ZbapiEcmMaterial = body.AppendChild(doc.CreateElement("urn:ZbapiEcmMaterial", soapURN));
           

            string className = "";

            #region Materialclass
            // Materialclass
            Materialclass MaterialclassItems = new Materialclass();
            PropertyInfo[] properties = MaterialclassItems.GetType().GetProperties();
            className = MaterialclassItems.GetType().Name;
            var nodeMaterialclass = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", className, bodyNs));



            foreach (var item in addMaterialclass(dt))
            {
                doc = addData(doc, nodeMaterialclass, item);
            }
            #endregion

            #region Materialdata
            // Materialdata
            Materialdata MaterialdataItems = new Materialdata();
            properties = MaterialdataItems.GetType().GetProperties();
            className = MaterialdataItems.GetType().Name;
            var nodeMaterialdata = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in addMaterialdata(dt))
            {
                doc = addData(doc, nodeMaterialdata, item);
            }
            #endregion


            //#region Return
            //// Return
            //ReturnMaterial ReturnItems = new ReturnMaterial();
            //properties = ReturnItems.GetType().GetProperties();
            //className = ReturnItems.GetType().Name.Substring(0, ReturnItems.GetType().Name.Length - 2);
            //var nodeReturn = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addReturnMaterial())
            //{
            //    doc = addData(doc, nodeReturn, item);
            //}
            //#endregion

            //var Testrun = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", "Testrun", bodyNs));
            var mode = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", "Mode", bodyNs)).InnerText = "01";

            //ReservationItemsChg resItemsChg = new ReservationItemsChg();
            //doc = addData(doc, ZbapiEcmReservation, resItemsChg);

            //ReservationItemsNew resItemsNew = new ReservationItemsNew();
            //doc = addData(doc, ZbapiEcmReservation, resItemsNew);

            //Reservationheader resheader = new Reservationheader();
            //doc = addData(doc, ZbapiEcmReservation, resheader);

            //doc.Save("C:\\testXML\\MaterialtoSAP.xml");
            //XmlDocument doc3 = new XmlDocument();
            //doc3.Load("C:\\testXML\\PO.xml");
            string data = XmlSerialize(doc);



            var url = ConfigurationManager.AppSettings["url_qa_mat"].ToString();
            var username = ConfigurationManager.AppSettings["username"].ToString();
            var password = ConfigurationManager.AppSettings["password"].ToString();

            //var url = "http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/rfc/sap/zecm_po/300/zecm_po/zecm_po";// ConfigurationManager.AppSettings["wbs_url"].ToString();
            //var username = "ECMCONNECT";//ConfigurationManager.AppSettings["wbs_username"].ToString();
            //var password = "egat2020";// ConfigurationManager.AppSettings["wbs_password"].ToString();


            /*var res =*/
            string res = DoHttpWebRequestMat(url, "POST", data, username, password);
            sap_log = new ps_sap_log();
            sap_log.send_xlm_text = data;
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml(res);
            // string FileName = "Result" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // FileName = FileName + ".xml";
            //// package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));
            // doc2.Save("C:\\" + FileName);
            //doc2.Save("C:\\testXML\\returnMat.xml");
           // doc2.Load("C:\\testXML\\returnMat.xml");
            string error = "";
            string sap_mat_code = "";

            string sap_status = "";

            string typeItem = "";

            sap_status = "";
            List<string> lsitError = new List<string>();

            for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes.Count; i++)
            {
                if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[1].InnerText == "E")
                {
                    error = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[4].InnerText;
                    lsitError.Add(error);
                }
                if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[1].InnerText == "S")
                {
                    sap_status = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[1].InnerText;
                    break;
                }
            }
            if (lsitError.Count >0)
            {
                sap_mat_code = lsitError[0];
            }


            sap_mat_code = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].InnerText;


            CultureInfo th = new CultureInfo("th-TH");


            sap_log.api_name = "Material";
            sap_log.return_message = res;
            sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
            sap_log.error_msg_othr = error;
            insert_ps_sap_log(sap_log);

            
            //UpdatePOID();
            //if (!PoNumber.Equals(""))
            //{

            //    update_ps_t_contract_no("S",res, cNo);
            //}else
            //{
            //    //List<string> listMessage = new List<string>();
            //    //listMessage = getListError(doc2, "Message");


            //    //int i = 0;
            //    //ConsoleUtility.WriteProgress(i);
            //    //foreach (var item in listMessage)
            //    //{
            //    //    error += item + ",";
            //    //    ConsoleUtility.WriteProgress(i, true);
            //    //    Thread.Sleep(50);
            //    //    i++;
            //    //}

            //    //error = doc2.GetElementsByTagName("Return").Item(0).ChildNodes[5].ChildNodes[4].InnerText;
            //    error = doc2.GetElementsByTagName("Return").Item(0).InnerText;

            //    update_ps_t_contract_no("E", error, cNo);
            //}

            //List<string[]> listPoNumber = new List<string[]>();
            //listPoNumber = getListPoNumber(doc2);

            //List<string> listType = new List<string>();
            //listType = getListError(doc2, "Type");

            //List<string> listMessage = new List<string>();
            //listMessage = getListError(doc2, "Message");

            return sap_mat_code;


        }
        public static List<string[]> getListPoNumber(XmlDocument doc)
        {
            List<string[]> listPoNumber = new List<string[]>();
            string[] numPO = new string[2];
            int count = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes.Count;
            //var x = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[0].InnerText;
            //var x2 = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[1].InnerText;
            for (int i = 0; i < count; i++)
            {
                numPO = new string[2];
                numPO[0] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[0].InnerText;
                numPO[1] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[1].InnerText;

            }
            listPoNumber.Add(numPO);
            return listPoNumber;



        }



     

        private static string DoHttpWebRequestMat(string url, string method, string data, string username, string password)
        {
           // SAP_Material.ZbapiEcmMaterial MatResponse = new SAP_Material.ZbapiEcmMaterial();
           
            WrtieLogWbs("Request " + data);

            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.KeepAlive = false;
            req.ContentType = "text/xml";
            req.Method = method;
            req.Credentials = new NetworkCredential(username, password);

            byte[] buffer = Encoding.UTF8.GetBytes(data);
            Stream PostData = req.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            var result = "";
            try
            {
                //Console.WriteLine("Connecting to SAP.......");
                using (WebResponse webResponse = req.GetResponse())
                {
                    //ConsoleUtility.WriteProgressBar(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgressBar(i, true);
                    //    Thread.Sleep(30);
                    //}
                    //Console.WriteLine("");
                    //Console.WriteLine("Connected");

                    //ConsoleUtility.WriteProgress(0);
                    ////var getResponse = req.GetResponse();
                    //// Stream newStream = getResponse.GetResponseStream();
                    Stream newStream = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(newStream);
                    result = sr.ReadToEnd();

                    WrtieLogWbs("Result " + result);

                    //ConsoleUtility.WriteProgress(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgress(i, true);
                    //    Thread.Sleep(50);
                    //}
                }
            }
            catch(Exception ex)
            {
                CultureInfo th = new CultureInfo("th-TH");
                sap_log = new ps_sap_log();

                sap_log.api_name = "Material";
                sap_log.send_xlm_text = data;
                sap_log.return_message = ex.Message;
                sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                sap_log.error_msg_othr = ex.Message;
                insert_ps_sap_log(sap_log);
            }


            //var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
            //return settings;

            return result;


        }


        public static List<ReturnMaterial> addReturnMaterial()
        {
            List<ReturnMaterial> listReturnItems = new List<ReturnMaterial>();
            ReturnMaterial ReturnItems = new ReturnMaterial();
            listReturnItems.Add(ReturnItems);
            return listReturnItems;
        }

        public static XmlDocument addData(XmlDocument doc, XmlNode xnode, object _class)
        {
            var bodyNs = @"";
            PropertyInfo[] properties = _class.GetType().GetProperties();
            //if (className.Contains("Item"))
            //{
            var item = xnode.AppendChild(doc.CreateElement("", "item", bodyNs));

            foreach (var p in properties)
            {
                if (p.GetValue(_class) != null)
                {
                    item.AppendChild(doc.CreateElement("", p.Name, bodyNs)).InnerText = p.GetValue(_class).ToString();
                }
                else
                {
                    item.AppendChild(doc.CreateElement("", p.Name.Trim(), bodyNs));
                }

            }

            return doc;

        }
        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }




        private static string xmlToJson(string xml)
        {
            //if (fileName.Contains("feedback"))
            //{
            //    return;
            //}

            //string xml = File.ReadAllText(fileName);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            //File.WriteAllText(fileName.Replace(".xml", ".json"), json);
            return json;
        }


        private static string XmlSerialize<T>(T entity) where T : class
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                // removes namespace
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add("q1", "http://schemas.xmlsoap.org/soap/envelope/");
                xmlns.Add("q2", "urn:sap-com:document:sap:soap:functions:mc-style");

                xsSubmit.Serialize(writer, entity, xmlns);
                return sw.ToString(); // Your XML
            }
        }

        private static T DeserializeFromXmlString<T>(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
        //private static ModelReturn.Envelope DoHttpWebRequest(string url, string method, string data, string username, string password)
        //{
        //    WrtieLogWbs("Request " + data);

        //    HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
        //    req.KeepAlive = false;
        //    req.ContentType = "text/xml";
        //    req.Method = method;
        //    req.Credentials = new NetworkCredential(username, password);

        //    byte[] buffer = Encoding.UTF8.GetBytes(data);
        //    Stream PostData = req.GetRequestStream();
        //    PostData.Write(buffer, 0, buffer.Length);
        //    PostData.Close();

        //    var getResponse = req.GetResponse();
        //    Stream newStream = getResponse.GetResponseStream();
        //    StreamReader sr = new StreamReader(newStream);
        //    var result = sr.ReadToEnd();

        //    WrtieLogWbs("Result " + result);

        //    var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
        //    return settings;
        //}


        private static void WrtieLogWbs(string str)
        {
            System.IO.Directory.CreateDirectory(@"c:\logwbs");
            var currDate = DateTime.Now.ToString("yyyy-MM-dd");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\logwbs\" + currDate + ".txt", true))
            {
                file.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + str);
            }
        }

        #endregion
    }
}
