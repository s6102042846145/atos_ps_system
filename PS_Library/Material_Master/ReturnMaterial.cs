﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.Material_Master
{
    public class ReturnMaterial
    {
        public string OldMatNo { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
        public string Number { get; set; }
        public string Message { get; set; }
        public string LogNo { get; set; }
        public string LogMsgNo { get; set; }
        public string MessageV1 { get; set; }
        public string MessageV2 { get; set; }
        public string MessageV3 { get; set; }
        public string MessageV4 { get; set; }
        public string Parameter { get; set; }
        public string Row { get; set; }
        public string Field { get; set; }
        public string System { get; set; }
    }
}
