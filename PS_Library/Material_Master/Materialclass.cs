﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.Material_Master
{
    public class Materialclass
    {
        public string OldMatNo { get; set; }
        public string Object { get; set; }
        public string Classnum { get; set; }
        public string Classtype { get; set; } = "023";
        public string Charact { get; set; } = "RF_NUMBER";
        public string ValueChar { get; set; }
    }
}
