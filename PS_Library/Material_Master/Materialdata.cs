﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.Material_Master
{
    public class Materialdata
    {
        public string OldMatNo { get; set; }
        public string Material { get; set; }
        public string IndSector { get; set; } = "M";
        public string MatlType { get; set; } 
        public string BasicView { get; set; } = "X";
        public string PurchaseView { get; set; } = "X";
        public string MrpView { get; set; } = "X";
        public string ForecastView { get; set; }
        public string WorkSchedView { get; set; }
        public string PrtView { get; set; } 
        public string StorageView { get; set; } = "X";
        public string AccountView { get; set; } = "X";
        public string CostView { get; set; } = "X";
        public string BaseUom { get; set; } = "EA";
        public string MatlGroup { get; set; } 
        public string BatchMgmt { get; set; } = "X";
        public string PurStatusB { get; set; } 
        public string Zunspsc { get; set; }//10102000
        public string MatlDesc { get; set; }
        public string Plant { get; set; }
      //  public string StgeLoc { get; set; }
        public string StgeBin { get; set; }
        public string PurGroup { get; set; } 
        public string AutoPOrd { get; set; }
        public string VarOrdUn { get; set; } = "1";
        public string MrpType { get; set; }
        public string MrpCtrler { get; set; }
        public string PlndDelry { get; set; }
        public string GrPrTime { get; set; }
        public string Lotsizekey { get; set; }
        public string ProcType { get; set; }
        public string Spproctype { get; set; }
        public string ReorderPt { get; set; }
        public string SafetyStk { get; set; }
        public string Minlotsize { get; set; }
        public string Maxlotsize { get; set; }
        public string FixedLot { get; set; }
        public string RoundVal { get; set; }
        public string MaxStock { get; set; }
        public string SmKey { get; set; }
        public string Availcheck { get; set; }
        public string MrpGroup { get; set; }
        public string SernoProf { get; set; }
        public string PurStatusP { get; set; }
        public string ValClass { get; set; }
        public string PriceCtrl { get; set; }
        public string MovingPr { get; set; }
        public string StdPrice { get; set; }
        public string PriceUnit { get; set; } = "1";
        public string QtyStruct { get; set; } = "X";
        public string OrigMat { get; set; } = "X";
        public string TextLine { get; set; }
                
                  
             
              
    }
}
