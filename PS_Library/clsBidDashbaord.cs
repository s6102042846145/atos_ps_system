﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
   public class clsBidDashbaord
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
		private string db_common = ConfigurationManager.AppSettings["db_common"].ToString();
		static DbControllerBase db = new DbControllerBase();
		static oraDBUtil oraDBUtil = new oraDBUtil();
		
		public clsBidDashbaord() { }
		public DataTable getHead(string org4, string org5)
		{
			DataTable dt = new DataTable();

			string strQuery = @"select * from 
(
select distinct
1 as orderby,
't' as default_select,
orgsname5
from m_syn_employee where orgsname4 = '" + org4 + @"' and orgsname5 is not null
union
select distinct
2 as orderby,
't' as default_select,
orgsname4
from m_syn_employee where orgsname4 = '" + org4 + @"'
union
select distinct 
3 as orderby,
case when ROW_NUMBER() OVER (ORDER BY mc_short) = 1 then 't' else 'f' end as default_select,
mc_short 
from m_syn_employee where orgsname3 in (select orgsname3 from m_syn_employee where orgsname4 = '" + org4 + @"' and rownum=1) 
and mc_short like (select 'ช.' || replace(orgsname3,'.','') || '%' from m_syn_employee where orgsname4 = '" + org4 + @"' and rownum=1) --'ช.อวส%'
union
select distinct
4 as orderby,
't' as default_select,
orgsname3
from m_syn_employee where orgsname4 = '" + org4 + @"'
) tb_approve
order by orderby, default_select desc";
			//    string strQuery = @"SELECT 
			//  distinct
			//  ORGSNAME1, --รอง
			//  ORGSNAME2, --ช.รอง
			//   ORGSNAME3, --ฝ่าย
			//  ORGSNAME4, --หัวหน้ากอง
			//  ORGSNAME5 --หัวหน้าแผนก
			//FROM M_SYN_EMPLOYEE 
			//where (orgsname4 = '" + mc_short + "' or orgsname5 = '" + mc_short + "') and orgsname5 is not null";

			dt = oraDBUtil._getDT(strQuery, db_common);

			return dt;
		}
		public DataTable getHead2()
		{
			DataTable dt = new DataTable();

			string strQuery = @"SELECT 
  orgsname
  ,orgname
FROM M_SYN_EMPLOYEE 
where  mc_short like 'หก-%'";

			dt = oraDBUtil._getDT(strQuery, db_common);

			return dt;
		}
		public DataTable getDoc(string bidNo)
        {
			DataTable dt = new DataTable();
			try
            {
				string sql = @"SELECT depart_position_name, ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS posDocid 
  FROM vw_ps_dashboard_doc WHERE bid_no = '"+ bidNo + @"' and bid_revision = 
  (select max(revision) from bid_no where bid_no = '"+ bidNo + "') GROUP BY depart_position_name";
				dt = db.ExecSql_DataTable(sql, zetl4eisdb);
			}
            catch (Exception ex)
            {

				LogHelper.WriteEx(ex);
			}
			return dt;
        }
		public DataTable getDocDetail(string bidNo)
        {
			DataTable dt = new DataTable();
			try
            {
				string sql = @"SELECT distinct [depart_position_name]
	  ,[job_no]
      ,[job_revision]
      ,[bid_no]
      ,[bid_revision]
      ,[schedule_no]
      ,[schedule_name]
      ,[section_position_name]
      ,[doctype_code]
      ,[doctype_name]
      ,[doc_no]
      ,[doc_revision]
      ,[doc_desc]
      ,[doc_sheetno]
      ,[link_doc]
      ,[updated_by]
      ,[updated_datetime]
      FROM vw_ps_dashboard_doc
  WHERE bid_no = '" + bidNo + "' and bid_revision = (select max(revision) from bid_no where bid_no =  '" + bidNo + "')";
				dt = db.ExecSql_DataTable(sql, zetl4eisdb);
			}
            catch (Exception ex)
            {

				LogHelper.WriteEx(ex);
			}
			return dt;
        }
		public void insertActivity(bidActivity objbidActivity)
        {
			try {
				string sql = @"SELECT [bid_no]
      ,[bid_revision]
      ,[depart_position_name]
      ,[section_position_name]
      ,[activity_id]
      ,[req_drawing]
      ,[req_bid_schedulename]
      ,[assignee_drawing]
      ,[req_equip]
      ,[assignee_equip]
      ,[req_tech_doc]
      ,[assignee_tech_doc]
      ,[approval_routing]
      ,[next_activity_id]
      ,[email_noti_id]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime]
	  ,[status]
      ,[is_select]
  FROM [ps_t_bid_dashboard]
  where depart_position_name ='" + objbidActivity.depart_position_name + @"'
  and bid_no ='" + objbidActivity.bid_no + @"'
  and activity_id ='" + objbidActivity.activity_id + "'";
				DataTable dt = db.ExecSql_DataTable(sql, zetl4eisdb);
				string sql2 = "";
				if (dt.Rows.Count > 0)
				{
                    if (objbidActivity.chkDept == "1")
                    {
                        //if (objbidActivity.assignee_drawing != "" || objbidActivity.assignee_equip != "" || objbidActivity.assignee_tech_doc != "")
                        //{
							sql2 = "update ps_t_bid_dashboard set assignee_drawing = '" + objbidActivity.assignee_drawing + "', assignee_equip='" + objbidActivity.assignee_equip + "', assignee_tech_doc='" + objbidActivity.assignee_tech_doc + "' , status ='" + objbidActivity.status + "' , is_select ='" + objbidActivity.is_select + "' , updated_by ='" + objbidActivity.updated_by + "' , created_by='" + objbidActivity.created_by + @"' , updated_datetime = GETDATE(),approval_routing='" + objbidActivity.approval_routing + @"'
where  bid_no = '" + objbidActivity.bid_no + "' and depart_position_name ='" + objbidActivity.depart_position_name + "' and activity_id ='" + objbidActivity.activity_id + "'";
							db.ExecNonQuery(sql2, zetl4eisdb);
						//}
					}
				}
				else
				{
					dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                    if (dt.Rows.Count == 0)
                    {
						sql2 = @"insert into ps_t_bid_dashboard ([bid_no]
      ,[bid_revision]
      ,[depart_position_name]
      ,[section_position_name]
      ,[activity_id]
      ,[req_drawing]
      ,[req_bid_schedulename]
      ,[assignee_drawing]
      ,[req_equip]
      ,[assignee_equip]
      ,[req_tech_doc]
      ,[assignee_tech_doc]
      ,[approval_routing]
      ,[next_activity_id]
      ,[email_noti_id]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime]
	  ,[status]
      ,[is_select])
values('" + objbidActivity.bid_no + "','" + objbidActivity.bid_revision + "','" + objbidActivity.depart_position_name + "'," +
"'" + objbidActivity.section_position_name + "','" + objbidActivity.activity_id + "','" + objbidActivity.req_drawing + "'," +
"'" + objbidActivity.req_bid_schedulename + "','" + objbidActivity.assignee_drawing + "','" + objbidActivity.req_equip + "'," +
"'" + objbidActivity.assignee_equip + "','" + objbidActivity.req_tech_doc + "','" + objbidActivity.assignee_tech_doc + "'," +
"'" + objbidActivity.approval_routing + "','" + objbidActivity.next_activity_id + "'," +
"'" + objbidActivity.email_noti_id + "','" + objbidActivity.created_by + "','" + objbidActivity.created_datetime + "','" + objbidActivity.updated_by + "','" + objbidActivity.updated_datetime + "','" + objbidActivity.status + "','" + objbidActivity.is_select + "')";
					}
					db.ExecNonQuery(sql2, zetl4eisdb);
				}
					

			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}
		}
		public void AssignReqBid(bidActivity objbidActivity)
		{
			DataTable dtchk = new DataTable();
			try
			{
				string sqlchk = @"SELECT [bid_no]
      ,[bid_revision]
      ,[depart_position_name]
      ,[section_position_name]
      ,[activity_id]
      ,[req_drawing]
      ,[req_bid_schedulename]
      ,[assignee_drawing]
      ,[req_equip]
      ,[assignee_equip]
      ,[req_tech_doc]
      ,[assignee_tech_doc]
      ,[approval_routing]
      ,[next_activity_id]
      ,[email_noti_id]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime]
	  ,[status]
      ,[is_select]
  FROM [ps_t_bid_dashboard]
  where depart_position_name ='" + objbidActivity.depart_position_name + @"'
  and bid_no ='" + objbidActivity.bid_no + @"'
  and activity_id ='" + objbidActivity.activity_id + "'";
				 dtchk = db.ExecSql_DataTable(sqlchk, zetl4eisdb);

				string strQuery = "";

				if (dtchk.Rows.Count == 0)
				{
					strQuery = @"insert into ps_t_bid_dashboard ([bid_no]
      ,[bid_revision]
      ,[depart_position_name]
      ,[section_position_name]
      ,[activity_id]
      ,[req_drawing]
      ,[req_bid_schedulename]
      ,[assignee_drawing]
      ,[req_equip]
      ,[assignee_equip]
      ,[req_tech_doc]
      ,[assignee_tech_doc]
      ,[approval_routing]
      ,[next_activity_id]
      ,[email_noti_id]
      ,[created_by]
      ,[created_datetime]
      ,[updated_by]
      ,[updated_datetime]
	  ,[status]
      ,[is_select])
values('" + objbidActivity.bid_no + "','" + objbidActivity.bid_revision + "','" + objbidActivity.depart_position_name + "'," +
"'" + objbidActivity.section_position_name + "','" + objbidActivity.activity_id + "','" + objbidActivity.req_drawing + "'," +
"'" + objbidActivity.req_bid_schedulename + "','" + objbidActivity.assignee_drawing + "','" + objbidActivity.req_equip + "'," +
"'" + objbidActivity.assignee_equip + "','" + objbidActivity.req_tech_doc + "','" + objbidActivity.assignee_tech_doc + "'," +
"'" + objbidActivity.approval_routing + "','" + objbidActivity.next_activity_id + "'," +
"'" + objbidActivity.email_noti_id + "','" + objbidActivity.created_by + "','" + objbidActivity.created_datetime + "','" + objbidActivity.updated_by + "','" + objbidActivity.updated_datetime + "','" + objbidActivity.status + "','" + objbidActivity.is_select + "')";
					db.ExecNonQuery(strQuery, zetl4eisdb);
				}
				else
				{
					if (objbidActivity.chkDept != "")
					{
						if (dtchk.Rows[0]["status"].ToString() != "WF in progress" && dtchk.Rows[0]["status"].ToString() != "FINISHED" && dtchk.Rows[0]["status"].ToString() != "ASSIGNED")
						{
							if (dtchk.Rows[0]["assignee_drawing"].ToString() != "") objbidActivity.assignee_drawing = dtchk.Rows[0]["assignee_drawing"].ToString();
							if (dtchk.Rows[0]["assignee_equip"].ToString() != "") objbidActivity.assignee_equip = dtchk.Rows[0]["assignee_equip"].ToString();
							if (dtchk.Rows[0]["assignee_tech_doc"].ToString() != "") objbidActivity.assignee_tech_doc = dtchk.Rows[0]["assignee_tech_doc"].ToString();

							strQuery = "update ps_t_bid_dashboard set assignee_drawing = '" + objbidActivity.assignee_drawing + "', assignee_equip='" + objbidActivity.assignee_equip + "', assignee_tech_doc='" + objbidActivity.assignee_tech_doc + "' , status ='" + objbidActivity.status + "' , is_select ='" + objbidActivity.is_select + "' , updated_by ='" + objbidActivity.updated_by + "' , created_by='" + objbidActivity.created_by + @"' , updated_datetime = GETDATE(),approval_routing='" + objbidActivity.approval_routing + @"'
where  bid_no = '" + objbidActivity.bid_no + "' and depart_position_name ='" + objbidActivity.depart_position_name + "' and activity_id ='" + objbidActivity.activity_id + "'";
							db.ExecNonQuery(strQuery, zetl4eisdb);
						}
					}
				}

			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

		}
		public DataTable getAttachWAbid(string bidno)
		{
			DataTable dt = new DataTable();
			string sql = "";
			try
			{
				sql = @"select  [bid_no]
							  ,[bid_revision]
							  ,[document_name]
							  ,[upload_datetime]
							  ,[att_url]
                     from vw_ps_bid_dashboard_att where bid_no='" + bidno + "'";

				dt = db.ExecSql_DataTable(sql, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}
		public DataTable GetDetail(string bidNo)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = @"select [bid_no]
							  ,[bid_revision]
							  ,[bid_desc]
							  ,[remark]
							  ,[completion_date]
							  ,[p6_task_id]
							  ,[p6_task_name]
							  ,[p6_plan_start]
							  ,[p6_plan_end]
						from vw_ps_bid_dashboard_info where bid_no='" + bidNo + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);

			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}
		public DataTable GetAct(string bidno, string rev)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = "";
				string sql = "select * from ps_t_bid_dashboard where bid_no = '" + bidno + "' and bid_revision = '" + rev + "'";
				dt = db.ExecSql_DataTable(sql, zetl4eisdb);
				if (dt.Rows.Count > 0)
				{
					strQuery = @"SELECT  ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS rowno,
[depart_position_name]
FROM [vw_ps_bid_dashboard_activity] where bid_no = '" + bidno + "' and bid_revision = '" + rev + "' group by[depart_position_name]";
				}
				else
				{
					strQuery = @"SELECT  ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS rowno,
[depart_position_name]
FROM [vw_ps_bid_activity_default] where bid_no = '" + bidno + "' and bid_revision = '" + rev + "' group by[depart_position_name]";
				}



				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}

		public DataTable GetSummary(string bidNo)
		{
			DataTable dt = new DataTable();
			try
			{

				string strQuery = @"SELECT [Rowno]
			 ,[job_no]
			 ,[job_desc]
			 ,[job_revision]
			 ,[schedule_no]
			 ,[schedule_name]
			 ,[schedule_status]
			 ,[bid_no]
			 ,[bid_revision]
		     ,[wa_url]
			 ,[sub_line_name]
			FROM [vw_ps_bid_dashboard_relate] where bid_no ='" + bidNo + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}
			return dt;
		}
		public DataTable GetActivity(string bidNo, string rev)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = "";
				string sql = "select * from ps_t_bid_dashboard where bid_no = '" + bidNo + "' and bid_revision = '" + rev + "'";
				dt = db.ExecSql_DataTable(sql, zetl4eisdb);
				if (dt.Rows.Count > 0)
				{
					strQuery = @"SELECT DISTINCT [activity_id]
   ,[depart_position_name]
   ,[section_position_name]
   ,[activity_name]
   ,[req_drawing]
   ,[assignee_drawing]
   ,[req_equip]
   ,[assignee_equip]
   ,[req_tech_doc]
   ,[assignee_tech_doc]
   ,[next_activity_id]
   ,[approval_routing]
   ,[email_noti_id]
   ,[email_content]
   ,[status_drawing]
   ,[status_equip]
   ,[status_tech_doc]
   ,[bid_no]
   ,[bid_revision]
   ,[is_select]
   ,[status_activity]
  FROM [vw_ps_bid_dashboard_activity] where bid_no = '" + bidNo + "' and bid_revision = '" + rev + "'";
				}
				else
				{
					strQuery = @"select DISTINCT [activity_id]
         ,[is_select]
         ,[bid_no]
         ,[bid_revision]
         ,[depart_position_name]
         ,[section_position_name]
         ,[activity_name]
         ,[req_drawing]
         ,[assignee_drawing]
         ,[status_drawing]
         ,[req_equip]
         ,[assignee_equip]
         ,[status_equip]
         ,[req_tech_doc]
         ,[assignee_tech_doc]
         ,[status_tech_doc]
         ,[next_activity_id]
         ,[approval_routing]
         ,[status_activity]
         ,[email_noti_id]
         ,[email_content] from vw_ps_bid_activity_default where bid_no = '" + bidNo + "' and bid_revision = '" + rev + "'";

				}

				strQuery += " order by activity_id";
				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}
			return dt;
		}
		public DataTable GetDoc(string bidNo)
        {
			DataTable dt = new DataTable();
			try {
				
				string strQuery = @"  SELECT depart_position_name, ROW_NUMBER() OVER(ORDER BY  depart_position_name) AS posDocid 
  FROM vw_ps_dashboard_doc WHERE bid_no = '"+ bidNo + @"' and job_revision = 
  (select max(revision) from bid_no where bid_no =  '" + bidNo + @"') GROUP BY depart_position_name";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);

			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}		
			return dt;

        }
		public DataTable GetDocOther(string bidNo, string rev)
        {
			DataTable dt = new DataTable();
			try {
				
				string strQuery = @"SELECT [depart_position_name]
	  ,[section_position_name]
	  ,[doctype_name]
	  ,[doc_no]
	  ,[doc_desc]
	  ,[doc_sheetno]
	  ,[doc_revision]
	  ,[job_no]
	  ,[job_revision]
	  ,[bid_no]
	  ,[bid_revision]
	  ,[schedule_no]
	  ,[schedule_name]
	  ,[link_doc]
  FROM [vw_ps_dashboard_doc] where bid_no = '" + bidNo + "' and bid_revision = '" + rev + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}
			
			return dt;
        }
		public void DeleteActivityBid(string bid_no, string bid_revision, string depart_position_name, string section_position_name)
		{
			DataTable dt = new DataTable();
			try
			{
				string sql = "select * from ps_t_bid_dashboard where bid_no = '" + bid_no + "' and  bid_revision = '"+ bid_revision + "' and depart_position_name ='" + depart_position_name + "' and section_position_name ='" + section_position_name + "'  and status = ''";
				string strQuery = "";
				dt = db.ExecSql_DataTable(sql, zetl4eisdb);
				if (dt.Rows.Count > 0)
				{
					strQuery = "delete from ps_t_bid_dashboard where bid_no = '" + bid_no + "' and  bid_revision = '" + bid_revision + "' and depart_position_name ='" + depart_position_name + "' and section_position_name ='" + section_position_name + "'  and status = ''";
					db.ExecNonQuery(strQuery, zetl4eisdb);
				}
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

		}
	
		public DataTable AssignActivityReqBid(string dept, string temp)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = @"select distinct [temp_name]
								  ,[activity_id]
								  ,[activity_name]
								  ,[depart_position_name]
								  ,[section_position_name]
								  ,[req_bid_schedulename]
								  ,[req_drawing]
								  ,[assignee_drawing]
								  ,[p6_taskcode_dwg]
								  ,[req_equip]
								  ,[assignee_equip]
								  ,[p6_taskcode_equip]
								  ,[req_tech_doc]
								  ,[assignee_tech_doc]
								  ,[p6_taskcode_tech_doc]
								  ,[next_activity_id]
								  ,[approval_routing]
								  ,[email_noti_id]
								  ,[remark]
								  ,[created_by]
								  ,[created_datetime]
								  ,[updated_by]
								  ,[updated_datetime]
								  ,[weight_factor]
								  ,[is_active]
								from ps_m_activity_bid where depart_position_name ='" + dept + "' and temp_name ='" + temp + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}
		public DataTable SelectActivityReqBid(string dept)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = @"select distinct [temp_name]  from ps_m_activity_bid where depart_position_name = '" + dept + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}
		public DataTable CheckCountTransBid(string bidno, string rev)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = @"select * from ps_t_bid_dashboard where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}
		public DataTable ViewNextActivity(string bidno, string rev,string acid)
		{
			DataTable dt = new DataTable();
			DataTable dt2 = new DataTable();
			try
			{
				string strQuery = @"select * from vw_ps_bid_dashboard_activity where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "' and activity_id='"+ acid + "'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
				if (dt.Rows.Count > 0)
				{
				    strQuery = @"select * from vw_ps_bid_dashboard_activity where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "' and activity_id in('" + dt.Rows[0]["next_activity_id"].ToString().Replace(",","','") + "')";
					dt2 = db.ExecSql_DataTable(strQuery, zetl4eisdb);
				}
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt2;
		}
		public DataTable ViewBeforeActivity(string bidno, string rev, string acid)
		{
			DataTable dt = new DataTable();
			try
			{
				string strQuery = @"select * from vw_ps_bid_dashboard_activity where bid_no = '" + bidno + "' and  bid_revision ='" + rev + "' and next_activity_id like'%" + acid + "%'";

				dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);

			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dt;
		}
		public DataTable ViewLogActivity(string bidno, string rev, string acid,DataTable dtlog)
		{
			DataTable dt = new DataTable();
			DateTime dttime = new DateTime();
			try
			{
					string strQuery = @"select * from wf_ps_request
								 where bid_no = '" + bidno + "' and bid_revision='" + rev + "' and activity_id='" + acid + "' order by rowid desc";

					dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
				if (dt.Rows.Count > 0)
				{
					strQuery = @"select *
								from wf_ps_request pr
								left
								join wf_ps_request_detail rd on pr.process_id = rd.process_id
								where bid_no = '" + bidno + "' and bid_revision='" + rev + "' and activity_id = '" + acid + "' and pr.rowid = " + dt.Rows[0]["rowid"].ToString() + "";
					dt = db.ExecSql_DataTable(strQuery, zetl4eisdb);
					if (dt.Rows.Count > 0)
					{
						foreach (DataRow dr2 in dt.Rows)
						{
							DataRow dr = dtlog.NewRow();
							if (dr2["emp_apvdate"].ToString() != "")
                            {
								dttime = Convert.ToDateTime(dr2["emp_apvdate"]);
								dr["emp_apvdate"] = dttime.ToString("dd/MM/yyyy");
							}
                            else
                            {
								dr["emp_apvdate"] = dr2["emp_apvdate"];
							}
                           
						
							dr["apv_order"] = dr2["apv_order"];
							dr["emp_id"] = dr2["emp_id"];
							dr["emp_name"] = dr2["emp_name"];
							dr["emp_position"] = dr2["emp_position"];
							dr["apv_status"] = dr2["apv_status"];
							dr["ps_subject"] = dr2["ps_subject"];
							dr["process_id"] = dr2["process_id"];

							dtlog.Rows.Add(dr);
						}
					}
				}
				
			}
			catch (Exception ex)
			{
				LogHelper.WriteEx(ex);
			}

			return dtlog;
		}

	}
	public class bidActivity
	{
		public string bid_no { get; set; }
		public int bid_revision { get; set; }
		public string depart_position_name { get; set; }
		public string section_position_name { get; set; }
		public string activity_id { get; set; }
		public Nullable<bool> req_drawing { get; set; }
		public Nullable<bool> req_bid_schedulename { get; set; }
		public string assignee_drawing { get; set; }
		public Nullable<bool> req_equip { get; set; }
		public string assignee_equip { get; set; }
		public Nullable<bool> req_tech_doc { get; set; }
		public string assignee_tech_doc { get; set; }
		public string approval_routing { get; set; }
		public string next_activity_id { get; set; }
		public Nullable<int> email_noti_id { get; set; }
		public string created_by { get; set; }
		public System.DateTime created_datetime { get; set; }
		public string updated_by { get; set; }
		public System.DateTime updated_datetime { get; set; }
		public string status { get; set; }
		public int is_select { get; set; }
		public string chkDept { get; set; }
	}
}
