﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Globalization;

namespace PS_Library.BID
{
   
    public static class Sap_Bid
    {

        public static string psDB = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();
        private static ps_sap_log sap_log = new ps_sap_log();
        public static string Mode = "";
        public static string connstr { get; set; }

        public static string cmdstrFull { get; set; }
        public static string cmdstr { get; set; }

        public static string cmdvalue { get; set; }
        public static string condi { get; set; }
        public static string order { get; set; }

        public static string Bid_SendToSap(string bidno,string mode)
        {
            string result = "";
            Mode = mode;
            // DataTable dtCNO = new DataTable();
            DataTable dt = new DataTable();
            //equip_code = "T213SQB23099";
            dt = getDataBidToSap(bidno);
            //dt = getDataStockToSap();
            // genXMLRequestStock(dt);

            result = genXMLRequestBid(dt, "");

            //insert_ps_sap_log(sap_log);
            //if (material != "")
            //{
            //    update_ps_m_equipcode(equip_code, material);
            //}

            return result;
            //createRequestPO();
        }

        public static string Bid_SendToSap(DataTable dt, string mode)
        {
            string result = "";
            Mode = mode;
            // DataTable dtCNO = new DataTable();
           // DataTable dt = new DataTable();
            //equip_code = "T213SQB23099";
            //dt = getDataBidToSap(bidno);
            //dt = getDataStockToSap();
            // genXMLRequestStock(dt);

            result = genXMLRequestBid(dt, "");

            //insert_ps_sap_log(sap_log);
            //if (material != "")
            //{
            //    update_ps_m_equipcode(equip_code, material);
            //}

            return result;
            //createRequestPO();
        }




        private static string getFormatDate(DateTime date, string format)
        {
            string strDate = "";

            string strMonth = date.Month.ToString().Length == 1 ? ("0" + date.Month) : date.Month.ToString();
            string strDay = date.Day.ToString().Length == 1 ? ("0" + date.Day) : date.Day.ToString();
            if (format == "YYYY-MM-DD")
            {
                strDate = date.Year + "-" + date.Month + "-" + date.Day;


            }

            return strDate;


        }


        //BID
        #region BID


        //public static List<int> insert_ps_t_contract_po(ps_t_contract_po contract_po)
        //{
        //    string result = "success";
        //    DataTable dtRunno = new DataTable();
        //    List<int> listID = new List<int>();
        //    try
        //    {

        //        cmdstrFull = "";


        //        cmdstr = @"insert into ps_up_excel_transection (";
        //        cmdstr = cmdstr + getNameForInsert(contract_po) + ")";

        //        cmdvalue = "VALUES (";
        //        cmdvalue = cmdvalue + getValueForInsert(contract_po) + ");";

        //        condi = " SELECT SCOPE_IDENTITY();";
        //        cmdstrFull = cmdstr + cmdvalue + condi;

        //        //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
        //        int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

        //        listID.Add(id);


        //    }
        //    catch (Exception ex)
        //    {
        //        result = ex.Message;
        //    }
        //    return listID;
        //}




        public static string getNameForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;
            PropertyInfo[] properties = _class.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!property.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {
                        value = property.Name;
                    }
                    else
                    {
                        value = property.Name + ",";
                    }
                    result = result + value;
                }
                count++;

            }
            return result;
        }

        public static string cutLastString(string item)
        {
            string result = "";
            result = item.Substring(0, item.Length - 1);
            return result;
        }
        public static string getValueForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;

            PropertyInfo[] properties = _class.GetType().GetProperties();


            foreach (var p in properties)
            {
                if (!p.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {

                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {
                            value = "GETDATE()" + "";
                        }
                        else
                        {
                            value = "'" + p.GetValue(_class) + "'";
                        }


                    }
                    else
                    {
                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {

                            value = "GETDATE()" + ",";

                        }
                        else
                        {
                            value = "'" + p.GetValue(_class) + "'" + ",";
                        }

                    }
                    result = result + value;
                }
                count++;
            }


            return result;
        }


        public static DataTable getContractNo()
        {

            string sqlFull = "";
            string sql = @"SELECT [contract_no],[status] FROM [dbo].[ps_t_contract_no] ";
            string condi = " where UPPER([status]) = UPPER('new')";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }


        public static DataTable getDataBidToSap(string bid_no)
        {

            string sqlFull = "";
            string sql = @"select  top 1 * from ps_t_bid_selling_createbid";
            string condi = " where bid_no = '" + bid_no + "'";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }

        public static string getPOID()
        {

            string pid = "";
            string result = UpdatePOID();
            // string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            DataTable dtRunno = new DataTable();

            try
            {
                if (result.Equals("success"))
                {
                    cmdstrFull = "";
                    cmdstr = @"select runno from wf_runno ";
                    condi = " WHERE prefix = 'PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + condi;

                    dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);
                    if (checkRowData(dtRunno))
                    {
                        pid = dtRunno.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // result = ex.Message;
            }

            return pid;
        }
        public static string getYearThai()
        {
            string yearThai = "";



            string strdate = DateTime.Now.Year.ToString();

            yearThai = (Convert.ToInt32(strdate) + 543).ToString();
            return yearThai;
        }
        public static string UpdatePOID()
        {
            string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            int runno = 0;
            int runnoNew = 0;
            DataTable dtRunno = new DataTable();
            try
            {
                cmdstrFull = "";
                cmdstr = @"select runno from wf_runno ";
                condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                order = " ";
                cmdstrFull = cmdstr + condi;

                dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);

                if (checkRowData(dtRunno))
                {
                    runno = Convert.ToInt32(dtRunno.Rows[0][0].ToString());
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"update wf_runno ";
                    cmdvalue = " set runno = " + runnoNew;
                    condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }
                else
                {
                    runno = 0;
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"insert into wf_runno ( prefix, runno,year_thai)";
                    cmdvalue = "VALUES  ( select 'PO', '" + runnoNew + "' , '" + yearThai + "' )";
                    condi = " ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        public static string update_ps_m_equipcode(string equip_code, string sap_mat_code)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_m_equipcode ";
                cmdstr = cmdstr + "Set sap_mat_code = '";
                cmdvalue = sap_mat_code + "' ";
                condi = "where equip_code = '" + equip_code + "'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static List<int> insert_ps_sap_log(ps_sap_log sap_log)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_sap_log (";
                cmdstr = cmdstr + getNameForInsert(sap_log) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(sap_log) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }

        //public static string update_ps_t_contract_no(string status, string res, string cNo)
        //{
        //    string result = "success";
        //    DataTable dtRunno = new DataTable();
        //    // res = "error";
        //    try
        //    {

        //        cmdstrFull = "";

        //        cmdstr = @"update ps_t_contract_no ";
        //        cmdstr = cmdstr + "Set status = '";
        //        cmdvalue = status + "' ," + " result_sap = '" + res + "'";
        //        condi = "where contract_no = '" + cNo + "' and status = 'New'";

        //        cmdstrFull = cmdstr + cmdvalue + condi;

        //        zdbUtil.ExecNonQuery(cmdstrFull, psDB);


        //    }
        //    catch (Exception ex)
        //    {
        //        result = ex.Message;
        //    }
        //    return result;
        //}

        public static bool checkRowData(DataTable dt)
        {
            bool check = false;
            if (dt.Rows.Count > 0)
            {
                check = true;
            }
            else
            {
                check = false;
            }
            return check;
        }


        public static List<Bid> addBid(DataTable dt)
        {
            List<Bid> listBidItems = new List<Bid>();
            Bid BidItems = new Bid();


            foreach (DataRow dr in dt.Rows)
            {
                BidItems = new Bid();

                BidItems.Bidno = dr["bid_no"].ToString();
                BidItems.Bidname = dr["bid_name"].ToString();
                BidItems.Bidname2 = dr["bid_name2"].ToString();
                BidItems.Bidname3 = dr["bid_name3"].ToString();
                BidItems.Bidstat = dr["bid_status"].ToString();
                BidItems.Bidtype = dr["bid_type"].ToString();
                BidItems.Approve = dr["approve_by"].ToString();
                BidItems.Appdate = setDateFormat(dr["approve_date"].ToString(), "");
                BidItems.Remark = dr["remark"].ToString();
                BidItems.PBidissue = setDateFormat(dr["plan_bid_issue"].ToString(), "plan");
                BidItems.PBidcloseN = setDateFormat(dr["plan_open_technical"].ToString(), "plan");
                BidItems.PBidopenc = setDateFormat(dr["plan_open_commercial"].ToString(), "plan");
                BidItems.ABidissue = setDateFormat(dr["actual_bid_issue_date"].ToString(), "");
                BidItems.ABidopent = setDateFormat(dr["actual_open_technical"].ToString(), "");
                BidItems.ABidopenc = setDateFormat(dr["actual_open_commercial"].ToString(), "");


                listBidItems.Add(BidItems);
            }
            return listBidItems;
        }

        private static string setDateFormat(string dateValue,string mode )
        {
            string strDate = "";
            dateValue =  Convert.ToDateTime(dateValue).ToString("dd/MM/yyyy HH:mm:ss", new CultureInfo("en-US"));
            if (mode == "plan")
            {
                strDate = dateValue.Substring(3, 2)  + dateValue.Substring(6, 4);
            }else
            {
                strDate = dateValue.Substring(6, 4) + "-" + dateValue.Substring(3, 2) + "-" +dateValue.Substring(0 ,2);
            }

            return strDate;
        }




        public static string genXMLRequestBid(DataTable dt, string cNo)
        {
            Console.WriteLine("##########  Send To SAP ###########");
            var soapNs = @"http://schemas.xmlsoap.org/soap/envelope/";
            var soapURN = @"urn:sap-com:document:sap:soap:functions:mc-style";
            var bodyNs = @"";

            XmlDocument doc = new XmlDocument();

            //// Create an XML declaration.
            //XmlDeclaration xmldecl;
            //xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            //xmldecl.Encoding = "UTF-8";

            //// Add the new node to the document.
            //XmlElement rootHead = doc.DocumentElement;
            //doc.InsertBefore(xmldecl, rootHead);



            XmlElement root = doc.CreateElement("soapenv", "Envelope", soapNs);
            root.SetAttribute("xmlns:soapenv", soapNs);
            root.SetAttribute("xmlns:urn", soapURN);
            doc.AppendChild(root);

            var header = root.AppendChild(doc.CreateElement("soapenv", "Header", soapNs));
            var body = root.AppendChild(doc.CreateElement("soapenv", "Body", soapNs));
            var ZbapiEcmBid = body.AppendChild(doc.CreateElement("urn:ZbapiEcmBid", soapURN));


            string className = "";

            #region Bid
            // Bid
            Bid BidItems = new Bid();
            PropertyInfo[] properties = BidItems.GetType().GetProperties();
            className = BidItems.GetType().Name;
            var nodeZbapiEcmBid = ZbapiEcmBid.AppendChild(doc.CreateElement("", className, bodyNs));



            foreach (var item in addBid(dt))
            {
                doc = addData(doc, nodeZbapiEcmBid, item);
            }
            #endregion




            //#region Return
            //// Return
            //ReturnMaterial ReturnItems = new ReturnMaterial();
            //properties = ReturnItems.GetType().GetProperties();
            //className = ReturnItems.GetType().Name.Substring(0, ReturnItems.GetType().Name.Length - 2);
            //var nodeReturn = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addReturnMaterial())
            //{
            //    doc = addData(doc, nodeReturn, item);
            //}
            //#endregion

            //var Testrun = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", "Testrun", bodyNs));
            var mode = ZbapiEcmBid.AppendChild(doc.CreateElement("", "Mode", bodyNs)).InnerText = Mode;
            //var Return = ZbapiEcmBid.AppendChild(doc.CreateElement("", "Return", bodyNs));

            //ReservationItemsChg resItemsChg = new ReservationItemsChg();
            //doc = addData(doc, ZbapiEcmReservation, resItemsChg);

            //ReservationItemsNew resItemsNew = new ReservationItemsNew();
            //doc = addData(doc, ZbapiEcmReservation, resItemsNew);

            //Reservationheader resheader = new Reservationheader();
            //doc = addData(doc, ZbapiEcmReservation, resheader);

           // doc.Save("C:\\testXML\\BidSAP.xml");
            //XmlDocument doc3 = new XmlDocument();
            //doc3.Load("C:\\testXML\\PO.xml");
            string data = XmlSerialize(doc);



            var url = ConfigurationManager.AppSettings["url_qa_bid"].ToString();
            var username = ConfigurationManager.AppSettings["username"].ToString();
            var password = ConfigurationManager.AppSettings["password"].ToString();

            //var url = "http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/rfc/sap/zecm_po/300/zecm_po/zecm_po";// ConfigurationManager.AppSettings["wbs_url"].ToString();
            //var username = "ECMCONNECT";//ConfigurationManager.AppSettings["wbs_username"].ToString();
            //var password = "egat2020";// ConfigurationManager.AppSettings["wbs_password"].ToString();

            sap_log = new ps_sap_log();
            sap_log.send_xlm_text = data;
            /*var res =*/
            string res = DoHttpWebRequestMat(url, "POST", data, username, password);
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml(res);
            // string FileName = "Result" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // FileName = FileName + ".xml";
            //// package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));
            // doc2.Save("C:\\" + FileName);
            //doc2.Save("C:\\testXML\\returnBidSAP.xml");
            // doc2.Load("C:\\testXML\\returnMat.xml");
            string error = "";
            string sap_status = "";

            string typeItem = "";

            sap_status = "";

            for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes.Count; i++)
            {
                if(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[i].ChildNodes[1].InnerText == "E")
                {
                    if (error == "")
                    {
                        error = error + doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[i].ChildNodes[4].InnerText;
                    }else
                    {
                        error = error + ","+ doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[i].ChildNodes[4].InnerText;
                    }
                }
                if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[i].ChildNodes[1].InnerText == "S")
                {
                    sap_status = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[i].ChildNodes[1].InnerText;
                    break;
                }
            }
            
            if(error.Length >0)
            {
                sap_status = error;
            }
            else
            {
                sap_status = "Success";
            }
            CultureInfo th = new CultureInfo("th-TH");
          

            sap_log.api_name = "BID";
            sap_log.return_message = res;
            sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
            sap_log.error_msg_othr = error;
            insert_ps_sap_log(sap_log);

            //UpdatePOID();
            //if (!PoNumber.Equals(""))
            //{

            //    update_ps_t_contract_no("S",res, cNo);
            //}else
            //{
            //    //List<string> listMessage = new List<string>();
            //    //listMessage = getListError(doc2, "Message");


            //    //int i = 0;
            //    //ConsoleUtility.WriteProgress(i);
            //    //foreach (var item in listMessage)
            //    //{
            //    //    error += item + ",";
            //    //    ConsoleUtility.WriteProgress(i, true);
            //    //    Thread.Sleep(50);
            //    //    i++;
            //    //}

            //    //error = doc2.GetElementsByTagName("Return").Item(0).ChildNodes[5].ChildNodes[4].InnerText;
            //    error = doc2.GetElementsByTagName("Return").Item(0).InnerText;

            //    update_ps_t_contract_no("E", error, cNo);
            //}

            //List<string[]> listPoNumber = new List<string[]>();
            //listPoNumber = getListPoNumber(doc2);

            //List<string> listType = new List<string>();
            //listType = getListError(doc2, "Type");

            //List<string> listMessage = new List<string>();
            //listMessage = getListError(doc2, "Message");

            return sap_status;


        }
        public static List<string[]> getListPoNumber(XmlDocument doc)
        {
            List<string[]> listPoNumber = new List<string[]>();
            string[] numPO = new string[2];
            int count = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes.Count;
            //var x = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[0].InnerText;
            //var x2 = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[1].InnerText;
            for (int i = 0; i < count; i++)
            {
                numPO = new string[2];
                numPO[0] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[0].InnerText;
                numPO[1] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[1].InnerText;

            }
            listPoNumber.Add(numPO);
            return listPoNumber;



        }





        private static string DoHttpWebRequestMat(string url, string method, string data, string username, string password)
        {
            // SAP_Material.ZbapiEcmMaterial MatResponse = new SAP_Material.ZbapiEcmMaterial();

            WrtieLogWbs("Request " + data);

            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.KeepAlive = false;
            req.ContentType = "text/xml";
            req.Method = method;
            req.Credentials = new NetworkCredential(username, password);

            byte[] buffer = Encoding.UTF8.GetBytes(data);
            Stream PostData = req.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            var result = "";

            try
            {
                //Console.WriteLine("Connecting to SAP.......");
                using (WebResponse webResponse = req.GetResponse())
                {
                    //ConsoleUtility.WriteProgressBar(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgressBar(i, true);
                    //    Thread.Sleep(30);
                    //}
                    //Console.WriteLine("");
                    //Console.WriteLine("Connected");

                    //ConsoleUtility.WriteProgress(0);
                    ////var getResponse = req.GetResponse();
                    //// Stream newStream = getResponse.GetResponseStream();
                    Stream newStream = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(newStream);
                    result = sr.ReadToEnd();

                    WrtieLogWbs("Result " + result);

                    //ConsoleUtility.WriteProgress(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgress(i, true);
                    //    Thread.Sleep(50);
                    //}
                }
            }
            catch(Exception ex)
            {
                CultureInfo th = new CultureInfo("th-TH");
                sap_log = new ps_sap_log();

                sap_log.api_name = "Bid";
                sap_log.send_xlm_text = data;
                sap_log.return_message = ex.Message;
                sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                sap_log.error_msg_othr = ex.Message;
                insert_ps_sap_log(sap_log);
            }


            //var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
            //return settings;

            return result;


        }


        //public static List<ReturnMaterial> addReturnMaterial()
        //{
        //    List<ReturnMaterial> listReturnItems = new List<ReturnMaterial>();
        //    ReturnMaterial ReturnItems = new ReturnMaterial();
        //    listReturnItems.Add(ReturnItems);
        //    return listReturnItems;
        //}

        public static XmlDocument addData(XmlDocument doc, XmlNode xnode, object _class)
        {
            var bodyNs = @"";
            PropertyInfo[] properties = _class.GetType().GetProperties();
            //if (className.Contains("Item"))
            //{
            var item = xnode.AppendChild(doc.CreateElement("", "item", bodyNs));

            foreach (var p in properties)
            {
                if (p.GetValue(_class) != null)
                {
                    item.AppendChild(doc.CreateElement("", p.Name, bodyNs)).InnerText = p.GetValue(_class).ToString();
                }
                else
                {
                    item.AppendChild(doc.CreateElement("", p.Name.Trim(), bodyNs));
                }

            }

            return doc;

        }
        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }




        private static string xmlToJson(string xml)
        {
            //if (fileName.Contains("feedback"))
            //{
            //    return;
            //}

            //string xml = File.ReadAllText(fileName);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            //File.WriteAllText(fileName.Replace(".xml", ".json"), json);
            return json;
        }


        private static string XmlSerialize<T>(T entity) where T : class
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                // removes namespace
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add("q1", "http://schemas.xmlsoap.org/soap/envelope/");
                xmlns.Add("q2", "urn:sap-com:document:sap:soap:functions:mc-style");

                xsSubmit.Serialize(writer, entity, xmlns);
                return sw.ToString(); // Your XML
            }
        }

        private static T DeserializeFromXmlString<T>(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
        //private static ModelReturn.Envelope DoHttpWebRequest(string url, string method, string data, string username, string password)
        //{
        //    WrtieLogWbs("Request " + data);

        //    HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
        //    req.KeepAlive = false;
        //    req.ContentType = "text/xml";
        //    req.Method = method;
        //    req.Credentials = new NetworkCredential(username, password);

        //    byte[] buffer = Encoding.UTF8.GetBytes(data);
        //    Stream PostData = req.GetRequestStream();
        //    PostData.Write(buffer, 0, buffer.Length);
        //    PostData.Close();

        //    var getResponse = req.GetResponse();
        //    Stream newStream = getResponse.GetResponseStream();
        //    StreamReader sr = new StreamReader(newStream);
        //    var result = sr.ReadToEnd();

        //    WrtieLogWbs("Result " + result);

        //    var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
        //    return settings;
        //}


        private static void WrtieLogWbs(string str)
        {
            System.IO.Directory.CreateDirectory(@"c:\logwbs");
            var currDate = DateTime.Now.ToString("yyyy-MM-dd");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\logwbs\" + currDate + ".txt", true))
            {
                file.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + str);
            }
        }

        #endregion
    }
}
