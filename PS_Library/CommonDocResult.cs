﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ECMCreateCommon
{
    public class CommonDocResult
    {
        public DataResult Data { get; set; }
        public string Status { get; set; }
        public string Message { get; set; }

    }

    public class DataResult
    {
        public int Id { get; set; }
        public string DocNo { get; set; }
        public int? DocYear { get; set; }
        public int DocCreateYear { get; set; }
        public string DocDept { get; set; }
        public string DocDeptName { get; set; }
        public int DocType { get; set; }
        public DateTime? DocDate { get; set; }
        public string RefNo { get; set; }
        public long? DocFrom { get; set; }
        public string DocFromName { get; set; }
        public string DocToName { get; set; }
        public string Topic { get; set; }
        public string Description { get; set; }
        public int Priority { get; set; }
        public int Classification { get; set; }
        public string Approver { get; set; }
        public string ApproverName { get; set; }
        public string ApproverPosition { get; set; }
        public int Status { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedByName { get; set; }
        public string CreatedByDept { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? SubmittedDate { get; set; }
        public string SubmittedBy { get; set; }
        public string Reviewer1 { get; set; }
        public string Reviewer2 { get; set; }
        public string Reviewer3 { get; set; }
        public string Reviewer4 { get; set; }
        public string Reviewer5 { get; set; }
        public string Reviewer6 { get; set; }
        public string Reviewer1Name { get; set; }
        public string Reviewer2Name { get; set; }
        public string Reviewer3Name { get; set; }
        public string Reviewer4Name { get; set; }
        public string Reviewer5Name { get; set; }
        public string Reviewer6Name { get; set; }
        public string Tel { get; set; }
        public string Fax { get; set; }
        public string DocOwner { get; set; }
        public string DocOwnerDept { get; set; }
        public string DocOwnerName { get; set; }
        public int DocFromType { get; set; }
        public string DocDeptOwner { get; set; }
        public long? DocRootId { get; set; }
        public DateTime? DueDate { get; set; }
        public bool IsDept { get; set; }
        public string StatusName { get; set; }
        public string DocTypeName { get; set; }
        public string PriorityName { get; set; }
        public string ClassificationName { get; set; }
        public string DocDateName { get; set; }
        public string DocFromFullName { get; set; }
        public string AddRemark { get; set; }
        public string AddRelatedDocNo { get; set; }
        public int? DocPurpose { get; set; }
        public string DocSentPurposeName { get; set; }
        public string Comment { get; set; }
        public string FaxPreview { get; set; }
        public int Mode { get; set; }
        public int ActionId { get; set; }
        public int openType { get; set; }   // 1: Inbox | 2: InternalDocCreate | 3: InternalDocDraft | 4: InternalDocTask | 5: InternalDocFollow | 6: InternalDocSearch
        public bool IsComment { get; set; }
        public bool IsPreview { get; set; }
        public bool IsDeptRequired { get; set; }
        public bool Closed { get; set; }
        public long InboxId { get; set; }
        public int EditAfterApprove { get; set; }
        public int CanEditAfterApprove { get; set; }
        public string CancelBy { get; set; }
        public string CancelByName { get; set; }
        public DateTime? CancelDate { get; set; }
        public string RecallByName { get; set; }
        public int? TransferFrom { get; set; }
        public bool? ShowLogOnPreview { get; set; }
        public long OutBoxId { get; set; }
        public string SendBackByName { get; set; }
        public string RejectdByName { get; set; }
        public List<string> Attachments { get; set; }
        public List<string> SentTos { get; set; }
        public string SentRemark { get; set; }
        public string Url { get; set; }
    }
}
