﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsBom
    {
        static DbControllerBase zdbUtil = new DbControllerBase();
        public string conn = ConfigurationManager.AppSettings["db_ps"].ToString();
        public object gvBid;
        public object gvMyBid;
        public string strBidno;

        public DataTable getEquipInfo(string strEquipId, string strDepartName = "", string strSectionName = "", string strLevel = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = "";
                if (strLevel == "0-SVR")
                {
                    strSQL = @"select equip_gong as 'department_name'
                                        ,equip_pnag as 'section_name'
                                        ,'0-EQI'  as 'bomlevel_code'
                                        ,'Equipment'  as 'bomlevel_name'
                                        ,equip_code as 'item_code'
                                        ,equip_desc_full as 'item_name'
                                        ,equip_desc_short as 'item_name_sh'
                                        ,case when sap_material_type = 'ZNV' then 'Major'
                                              when sap_material_type = 'ZCM' then 'Miscellensous'
                                              when sap_material_type = 'SVR' then 'Service'
                                              else '' end as 'item_type'
                                        ,legend as 'item_legend'
                                        ,'' as 'item_relate'
                                  from ps_m_equipcode
                                  where isactive = 1 and is_service = 1 ";
                }
                else
                {
                    strSQL = @"select equip_gong as 'department_name'
                                        ,equip_pnag as 'section_name'
                                        ,'0-EQI'  as 'bomlevel_code'
                                        ,'Equipment'  as 'bomlevel_name'
                                        ,equip_code as 'item_code'
                                        ,equip_desc_full as 'item_name'
                                        ,equip_desc_short as 'item_name_sh'
                                        ,case when sap_material_type = 'ZNV' then 'Major'
                                              when sap_material_type = 'ZCM' then 'Miscellensous'
                                              when sap_material_type = 'SVR' then 'Service'
                                              else '' end as 'item_type'
                                        ,legend as 'item_legend'
                                        ,'' as 'item_relate'
                                  from ps_m_equipcode
                                  where isactive = 1 and isnull(is_service,0) = 0";
                }

                if (strEquipId != "")
                {
                    strSQL += " and (equip_code LIKE '%" + strEquipId.Trim() + "%' OR equip_desc_full LIKE '%" + strEquipId.Trim() + "%'  OR legend LIKE '%" + strEquipId.Trim() + "%') ";//OR equip_desc_short LIKE '%" + strEquipId.Trim() + "%'
                }

                if (strDepartName != "")
                {
                    if (strDepartName == "กวศ-ส.")
                    {
                        strDepartName = "กวอ-ส.";
                        strSQL += " AND equip_gong = '" + strDepartName + "'";
                    }
                    else
                    {
                        strSQL += " AND equip_gong = '" + strDepartName + "'";
                    }
                  
                }

                //if (strSectionName != "")
                //    strSQL += " AND equip_pnag = '" + strSectionName + "'";

                strSQL += "order by equip_code";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getBOMInfo_Search(string strSearchValue, string strBOMLevel_Code, string strDepartName = "", string strSectionName = "")
        {
            DataTable dt = new DataTable();
            try
            {
                //string strSQL = @"select * from 
                //                    (select distinct bom.depart_position_name as 'department_name'
                //                        ,bom.section_position_name as 'section_name'
                //                        ,bom.bomlevel  as 'bomlevel_code'
                //                        ,case when bom.bomlevel ='0-EQI' then 'Equipment'
                //                              when bom.bomlevel ='1-ZNV' then 'Major'
                //                              when bom.bomlevel ='2-ZCM' then 'Miscellensous'
                //                              when bom.bomlevel ='3-ASM' then 'Assembly'
                //                              when bom.bomlevel ='4-MOD' then 'Module'
                //                              end  as 'bomlevel_name'
                //                        ,bom.bom_code as 'item_code'
                //                        ,bom.bom_name as 'item_name'
                //                        ,'' as 'item_type'
                //                        ,bom.legend as 'item_legend'
                //                        ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                //                                                                      when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                //                                                                      when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                //                                                                      when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                //                                                                      when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                //                                                                      end 
                //                                    FROM ps_m_bom eq_list 
                //                                            left outer join ps_m_equipcode as eq
                //                                            on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                //                                            left outer join ps_m_bom as bom_select
                //                                            on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                //                                    where eq_list.bom_code = bom.bom_code
                //                                    group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                //                                    order by eq_list.equip_type
                //                                FOR XML PATH('')),1,1,''),',','<br/>') as item_relate
                //                        ,bom.equip_unit
                //                        ,bom.equip_buyinglot
                //                        ,bom.equip_reserve
                //                  from ps_m_bom as bom) as tb_search
                //                  where 1=1 ";//           ,bom.equip_qty
                string strSQL = "select * from vw_ps_bom_search where 1=1 ";
                if (strSearchValue != "")
                {
                    strSQL += " and (item_name LIKE '%" + strSearchValue.Trim() + "%' OR item_relate LIKE '%" + strSearchValue.Trim() + "%' OR item_legend LIKE '%" + strSearchValue.Trim() + "%') ";
                }
                if (strBOMLevel_Code != "")
                {
                    strSQL += " and bomlevel_code = '" + strBOMLevel_Code.Trim() + "'";
                }

                if (strDepartName != "")
                    strSQL += " AND department_name = '" + strDepartName + "'";

                //if (strSectionName != "")
                //    strSQL += " AND tb_search.section_name = '" + strSectionName + "'";

                strSQL += "order by item_name";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        private string genBOMCode()
        {
            string xBOMCode = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'BOM'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, conn);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'BOM' ";
                zdbUtil.ExecNonQuery(sql, conn);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('BOM', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                zdbUtil.ExecNonQuery(sql, conn);
            }
            xBOMCode = "BOM-" + xRunno.ToString("0000");
            return xBOMCode;
        }
        public void insertBOM(string strBOMName, string strBOMDesc, string strDepart, string strSection
                              , string strBOMLevel, string strLegend, string strCate_Code, string strMisc_Per_Unit
                              , string strEquipValueData, string strDocData, string strUserLogin, string strBom_Group)
        {
            try
            {
                DataTable dt = new DataTable();
                string strSQL = "";
                string strBOMCode = "";
                if (strBom_Group != "")
                {
                    strSQL = "update ps_m_bom set bom_name='"+ strBOMName.Replace("'", "''").Trim() + "',bom_desc='" + strBOMDesc.Replace("'", "''").Trim() + "' where bom_code ='"+ strBom_Group + "' and depart_position_name='"+ strDepart.Trim() + "' and section_position_name='" + strSection.Trim() + "'";
                    zdbUtil.ExecNonQuery(strSQL, conn);
                }
               
                strBOMCode = genBOMCode();
                string[] arrValues = strEquipValueData.Split('|');
                foreach (string value in arrValues)
                {
                    string[] arrData = value.Split('_');

                    if (strBom_Group != "")
                    {
                        strSQL = "select * from ps_m_bom where bom_code ='"+ strBom_Group + "' and equip_code='" + arrData[0].ToString() + "'";
                        dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
                        if (dt.Rows.Count == 0)
                        {
                            strSQL = @"insert into ps_m_bom(bom_code,bom_name,bom_desc,depart_position_name,section_position_name
                                                           ,bomlevel,legend,cate_code,misc_per_unit
                                                           ,equip_code,equip_type,equip_qty,equip_unit,equip_buyinglot,equip_reserve
                                                           ,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strBom_Group + "'" + @"
                                             ,'" + strBOMName.Replace("'", "''").Trim() + "'" + @"
                                             ,'" + strBOMDesc.Replace("'", "''").Trim() + "'" + @"
                                             ,'" + strDepart.Trim() + "'" + @"
                                             ,'" + strSection.Trim() + "'" + @"
                                             ,'" + strBOMLevel.Trim() + "'" + @"
                                             ,'" + strLegend.Trim() + "'" + @"
                                             ,'" + strCate_Code.Trim() + "'" + @"
                                             ,'" + strMisc_Per_Unit.Trim() + "'" + @"
                                             ,'" + arrData[0].ToString() + "'" + @"
                                             ,'" + arrData[1].ToString() + "'" + @"
                                             ," + arrData[2].ToString() + @"
                                             ,'" + arrData[3].ToString().Trim() + "'" + @"
                                             ," + arrData[4].ToString() + @"
                                             ," + arrData[5].ToString() + @"
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                        }
                        else
                        {
                            strSQL = "update ps_m_bom set bom_name='" + strBOMName.Replace("'", "''").Trim() + "',bom_desc='" + strBOMDesc.Replace("'", "''").Trim() + "',equip_type='" + arrData[1].ToString() + @"' 
    ,equip_qty='" + arrData[2].ToString() + "',equip_unit='" + arrData[3].ToString().Trim() + "',equip_buyinglot='" + arrData[4].ToString() + "',equip_reserve='" + arrData[5].ToString() + @"',updated_datetime = GETDATE() 
    where bom_code ='" + strBom_Group + "' and equip_code='" + arrData[0].ToString() + "' and depart_position_name='" + strDepart.Trim() + "' and section_position_name='" + strSection.Trim() + "'";
                            
                        }
                        zdbUtil.ExecNonQuery(strSQL, conn);
                    }
                    else
                    {
                        strSQL = @"insert into ps_m_bom(bom_code,bom_name,bom_desc,depart_position_name,section_position_name
                                                           ,bomlevel,legend,cate_code,misc_per_unit
                                                           ,equip_code,equip_type,equip_qty,equip_unit,equip_buyinglot,equip_reserve
                                                           ,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strBOMCode + "'" + @"
                                             ,'" + strBOMName.Replace("'", "''").Trim() + "'" + @"
                                             ,'" + strBOMDesc.Replace("'", "''").Trim() + "'" + @"
                                             ,'" + strDepart.Trim() + "'" + @"
                                             ,'" + strSection.Trim() + "'" + @"
                                             ,'" + strBOMLevel.Trim() + "'" + @"
                                             ,'" + strLegend.Trim() + "'" + @"
                                             ,'" + strCate_Code.Trim() + "'" + @"
                                             ,'" + strMisc_Per_Unit.Trim() + "'" + @"
                                             ,'" + arrData[0].ToString() + "'" + @"
                                             ,'" + arrData[1].ToString() + "'" + @"
                                             ," + arrData[2].ToString() + @"
                                             ,'" + arrData[3].ToString().Trim() + "'" + @"
                                             ," + arrData[4].ToString() + @"
                                             ," + arrData[5].ToString() + @"
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";

                        zdbUtil.ExecNonQuery(strSQL, conn);
                    }
                 
                }


                string[] arrDoc = strDocData.Split('|');
                foreach (string doc in arrDoc)
                {
                    if (strBom_Group != "")
                    {
                        strSQL = "select * from ps_m_bom_doc where bom_code='"+ strBom_Group + "' and doc_nodeid = '"+ doc + "'";
                        dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
                        if (dt.Rows.Count == 0)
                        {
                            strSQL = @"insert into ps_m_bom_doc(bom_code,doc_nodeid
                                                           ,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strBom_Group + "','" + doc + @"'
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";
                            zdbUtil.ExecNonQuery(strSQL, conn);
                        }
                        
                    }
                    else
                    {
                        strSQL = @"insert into ps_m_bom_doc(bom_code,doc_nodeid
                                                           ,created_by,created_datetime,updated_by,updated_datetime)
                                      values ('" + strBOMCode + "','" + doc + @"'
                                             ,'" + strUserLogin + "'," + "GETDATE()" + ",'" + strUserLogin + "'," + "GETDATE()" + ")";

                        zdbUtil.ExecNonQuery(strSQL, conn);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public DataTable searchBOM(string Bomname, string strDepartName, string strSectionName,string BomDesc,string BomLegend,string BomLvl)
        {
            DataTable dt = new DataTable();
            try
            {
                //string strSQL = @"SELECT distinct bom.bom_code,bom.bom_name,bom.bom_desc,bom.depart_position_name,bom.section_position_name
                //                            ,bom.bomlevel
                //                            ,case when bom.bomlevel ='0-EQI' then 'Equipment'
                //                                  when bom.bomlevel ='1-ZNV' then 'Major'
                //                                  when bom.bomlevel ='2-ZCM' then 'Miscellensous'
                //                                  when bom.bomlevel ='3-ASM' then 'Assembly'
                //                                  when bom.bomlevel ='4-MOD' then 'Module'
                //                                  end  as 'bomlevel_name'
                //                            ,bom.legend,bom.cate_code,bom.misc_per_unit
                //                            ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                //                                                                      when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                //                                                                      when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                //                                                                      when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                //                                                                      when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                //                                                                      end 
                //                                    FROM ps_m_bom eq_list 
                //                                            left outer join ps_m_equipcode as eq
                //                                            on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                //                                            left outer join ps_m_bom as bom_select
                //                                            on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                //                                    where eq_list.bom_code = bom.bom_code
                //                                    group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                //                                    order by eq_list.equip_type
                //                                FOR XML PATH('')),1,1,''),',','<br/>')  as equip_list

                //                  FROM ps_m_bom as bom
                //                  WHERE (bom.bom_name like '%" + strTextFilter + "%') or (bom.bom_desc like '%" + strTextFilter + @"%')
                //                        and bom.depart_position_name = '" + strDepartName + "' and bom.section_position_name = '" + strSectionName + "'";
                string strSQL = "";
                strSQL = @"SELECT distinct bom.bom_code,bom.bom_name,bom.bom_desc,bom.depart_position_name,bom.section_position_name
                                            ,bom.bomlevel
                                            ,case when bom.bomlevel ='0-EQI' then 'Equipment'
                                                  when bom.bomlevel ='1-ZNV' then 'Major'
                                                  when bom.bomlevel ='2-ZCM' then 'Miscellensous'
                                                  when bom.bomlevel ='3-ASM' then 'Assembly'
                                                  when bom.bomlevel ='4-MOD' then 'Module'
                                                  end  as 'bomlevel_name'
                                            ,bom.legend,bom.cate_code,bom.misc_per_unit
                                            ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                                                                                      when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                                                                                      when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                                                                                      when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                                                                                      when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                                                                                      end 
                                                    FROM ps_m_bom eq_list 
                                                            left outer join ps_m_equipcode as eq
                                                            on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                                                            left outer join ps_m_bom as bom_select
                                                            on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                                                    where eq_list.bom_code = bom.bom_code
                                                    group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                                                    order by eq_list.equip_type
                                                FOR XML PATH('')),1,1,''),',','<br/>')  as equip_list   
                                  FROM ps_m_bom as bom";
                string filter = "";
                if (Bomname != "") filter += " WHERE (bom.bom_name like '%" + Bomname + "%')";
                if (BomDesc != "" && filter != "") filter += " AND (bom.bom_desc like '%" + BomDesc + "%')";
                else if (BomDesc != "" && filter == "") filter += " WHERE (bom.bom_desc like '%" + BomDesc + "%')";
                if (BomLegend != "" && filter != "") filter += " AND legend='" + BomLegend + "'";
                else if(BomLegend != "" && filter == "") filter += " WHERE legend='" + BomLegend + "'";
                if (filter != "") strSQL += filter;
                
                if (BomLvl != "") strSQL += " WHERE bomlevel='" + BomLvl + "'";

                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable selectBOM_EquipList(string strTextFilter, string strDepartName = "", string strSectionName = "")
        {
            DataTable dt = new DataTable();
            try
            {
                #region
                //string strSQL = @"select distinct
                //                         case when bom.equip_type ='0-EQI' then eq_main.equip_gong
                //                              else bom_child.depart_position_name end as 'department_name'
                //                        ,case when bom.equip_type ='0-EQI' then eq_main.equip_pnag
                //                              else bom_child.section_position_name end as 'section_name'
                //                        ,bom.equip_type  as 'bomlevel_code'
                //                        ,case when bom.equip_type ='0-EQI' then 'Equipment'
                //                              when bom.equip_type ='1-ZNV' then 'Major'
                //                              when bom.equip_type ='2-ZCM' then 'Miscellensous'
                //                              when bom.equip_type ='3-ASM' then 'Assembly'
                //                              when bom.equip_type ='4-MOD' then 'Module'
                //                              end  as 'bomlevel_name'
                //                        ,bom.equip_code as 'item_code'
                //                        ,case when bom.equip_type ='0-EQI' then eq_main.equip_desc_short + ' (' + eq_main.equip_code + ')'
                //                              else bom_child.bom_name end as 'item_name'
                //                        ,case when bom.equip_type ='0-EQI' then eq_main.legend
                //                              else bom_child.legend end as 'item_legend'
                //                        ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                //                                                                      when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                //                                                                      when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                //                                                                      when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                //                                                                      when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                //                                                                      end 
                //                                    FROM ps_m_bom eq_list 
                //                                            left outer join ps_m_equipcode as eq
                //                                            on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                //                                            left outer join ps_m_bom as bom_select
                //                                            on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                //                                    where eq_list.bom_code = bom.bom_code
                //                                    group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                //                                    order by eq_list.equip_type
                //                                FOR XML PATH('')),1,1,''),',','<br/>') as item_relate
                //                        ,bom.equip_qty
                //                        ,bom.equip_unit
                //                        ,bom.equip_buyinglot
                //                        ,bom.equip_reserve
                //                  from ps_m_bom as bom
                //                        left outer join ps_m_equipcode as eq_main
                //                        on eq_main.equip_code = bom.equip_code and bom.equip_type = '0-EQI'
                //                        left outer join ps_m_bom as bom_child
                //                        on bom_child.bom_code = bom.equip_code and bom.equip_type <> '0-EQI'
                //                  WHERE (bom.bom_code = '" + strTextFilter + "') ";

                //string strSQL = @"select distinct
                //                               case when bom.equip_type ='0-EQI' then eq_main.equip_gong
                //                                    else bom_child.depart_position_name end as 'bom_childdepartment_name'
                //                              ,case when bom.equip_type ='0-EQI' then eq_main.equip_pnag
                //                                    else bom_child.section_position_name end as 'bom_childsection_name'
                //                              ,bom.equip_type  as 'bomlevel_code'
                //                              ,case when bom.equip_type ='0-EQI' then 'Equipment'
                //                                    when bom.equip_type ='1-ZNV' then 'Major'
                //                                    when bom.equip_type ='2-ZCM' then 'Miscellensous'
                //                                    when bom.equip_type ='3-ASM' then 'Assembly'
                //                                    when bom.equip_type ='4-MOD' then 'Module'
                //                                    end  as 'bomlevel_name'
                //                              ,bom.equip_code as 'item_code'
                //                              ,case when bom.equip_type ='0-EQI' then eq_main.equip_desc_short + ' (' + eq_main.equip_code + ')'
                //                                    else bom_child.bom_name end as 'bom_childitem_name'
                //                              ,case when bom.equip_type ='0-EQI' then eq_main.legend
                //                                    else bom_child.legend end as 'bom_childitem_legend'
                //                              ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                //                                                                            when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                //                                                                            when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                //                                                                            when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                //                                                                            when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                //                                                                            end 
                //                                          FROM ps_m_bom eq_list 
                //                                                  left outer join ps_m_equipcode as eq
                //                                                  on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                //                                                  left outer join ps_m_bom as bom_select
                //                                                  on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                //                                          where eq_list.bom_code = bom.bom_code
                //                                          group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                //                                          order by eq_list.equip_type
                //                                      FOR XML PATH('')),1,1,''),',','<br/>') as item_relate
                //                              ,bom.equip_qty
                //                              ,bom.equip_unit
                //                              ,bom.equip_buyinglot
                //                              ,bom.equip_reserve
                //,bom.depart_position_name as 'department_name'
                //,bom.section_position_name as 'section_name'
                //,bom.bom_name as 'item_name'
                //,bom.legend as 'item_legend'
                //                        from ps_m_bom as bom
                //                              left outer join ps_m_equipcode as eq_main
                //                              on eq_main.equip_code = bom.equip_code and bom.equip_type = '0-EQI'
                //                              left outer join ps_m_bom as bom_child
                //                              on bom_child.bom_code = bom.equip_code and bom.equip_type <> '0-EQI'
                //                        WHERE (bom.bom_code = '" + strTextFilter + "')";
                #endregion
                string strSQL = @"select distinct
                                               case when bom.equip_type ='0-EQI' then eq_main.equip_gong
                                                    else bom_child.depart_position_name end as 'bom_childdepartment_name'
                                              ,case when bom.equip_type ='0-EQI' then eq_main.equip_pnag
                                                    else bom_child.section_position_name end as 'bom_childsection_name'
                                              ,bom.equip_type  as 'bomlevel_code'
                                              ,case when bom.equip_type ='0-EQI' then 'Equipment'
                                                    when bom.equip_type ='1-ZNV' then 'Major'
                                                    when bom.equip_type ='2-ZCM' then 'Miscellensous'
                                                    when bom.equip_type ='3-ASM' then 'Assembly'
                                                    when bom.equip_type ='4-MOD' then 'Module'
                                                    end  as 'bomlevel_name'
                                              ,bom.equip_code as 'item_code'
                                              ,case when bom.equip_type ='0-EQI' then eq_main.equip_desc_short + ' (' + eq_main.equip_code + ')'
                                                    else bom_child.bom_name end as 'bom_childitem_name'
                                              ,case when bom.equip_type ='0-EQI' then eq_main.legend
                                                    else bom_child.legend end as 'bom_childitem_legend'
                                              ,REPLACE(STUFF((SELECT ',- ' + case when eq_list.equip_type ='0-EQI' then 'Equipment: ' + eq.equip_desc_short + ' (' + eq.equip_code + ')'
                                                                                            when eq_list.equip_type ='1-ZNV' then 'Major: ' + bom_select.bom_name 
                                                                                            when eq_list.equip_type ='2-ZCM' then 'Miscellensous: ' + bom_select.bom_name
                                                                                            when eq_list.equip_type ='3-ASM' then 'Assembly: ' + bom_select.bom_name
                                                                                            when eq_list.equip_type ='4-MOD' then 'Module: ' + bom_select.bom_name
                                                                                            end 
                                                          FROM ps_m_bom eq_list 
                                                                  left outer join ps_m_equipcode as eq
                                                                  on eq.equip_code = eq_list.equip_code and eq_list.equip_type = '0-EQI'
                                                                  left outer join ps_m_bom as bom_select
                                                                  on bom_select.bom_code = eq_list.equip_code and eq_list.equip_type <> '0-EQI'
                                                          where eq_list.bom_code = bom.bom_code
                                                          group by eq_list.equip_type, eq.equip_desc_short,eq.equip_code ,bom_select.bom_name 
                                                          order by eq_list.equip_type
                                                      FOR XML PATH('')),1,1,''),',','<br/>') as item_relate
                                              ,bom.equip_qty
                                              ,bom.equip_unit
                                              ,bom.equip_buyinglot
                                              ,bom.equip_reserve
                ,bom.depart_position_name as 'department_name'
                ,bom.section_position_name as 'section_name'
                ,bom.bom_name as 'item_name0'
                ,bom.legend as 'item_legend0'
                ,eq_main.equip_desc_full as 'item_name'
                ,eq_main.legend as 'item_legend'
				,vw.item_name as 'item_name_assem'
				,vw.item_legend as 'item_legend_assem'
                                        from ps_m_bom as bom
                                              left outer join ps_m_equipcode as eq_main
                                              on eq_main.equip_code = bom.equip_code 
                                              left outer join ps_m_bom as bom_child
                                              on bom_child.bom_name = bom.equip_code and bom.equip_type <> '0-EQI'
											   left outer join vw_ps_bom_search as vw
											   on vw.item_code = bom.equip_code 
                                        WHERE (bom.bom_code = '" + strTextFilter + "')";


                if (strDepartName != "")
                    strSQL += " AND bom_child.department_name = '" + strDepartName + "' ";
                //if (strSectionName != "")
                //    strSQL += " AND bom_child.section_name = '" + strSectionName + "' ";

                strSQL += " order by bom.equip_type";
                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable selectBOM_DocList(string strTextFilter, string strDepartName = "", string strSectionName = "")
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"SELECT distinct bom_doc.doc_nodeid,doc_apv.doc_name,doc_apv.doc_revision_display,doc_apv.doc_desc 
                                  FROM ps_m_bom as bom
                                        inner join ps_m_bom_doc as bom_doc
		                                    left outer join ps_m_doc_approve as doc_apv
		                                    on doc_apv.doc_nodeid = bom_doc.doc_nodeid
                                        on bom_doc.bom_code = bom.bom_code
                                  WHERE (bom.bom_code = '" + strTextFilter + "')";

                if (strDepartName != "")
                    strSQL += " AND bom.depart_position_name = '" + strDepartName + "'";
                //if (strSectionName != "")
                //    strSQL += " AND bom.section_position_name = '" + strSectionName + "'";

                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable selectDocByEquipCode(string strEquipCodeList)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"select equip.equip_code,equip.doc_nodeid, doc_apv.doc_name, doc_apv.doc_revision_display, doc_apv.doc_desc
                                    from ps_m_equip_doc as equip 
	                                    left outer join ps_m_doc_approve as doc_apv
	                                    on doc_apv.doc_nodeid = equip.doc_nodeid
                                  WHERE equip.equip_code in (" + strEquipCodeList.Replace(",", "','") + ")";

                dt = zdbUtil.ExecSql_DataTable(strSQL, conn);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable GetAttachBom(string dept, string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string strQuery = @"SELECT [rowid]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[doc_desc]
                              ,[doc_revision_no]
                              ,[doc_revision_display]
                              ,[doc_revision_date]
                              ,[doc_sheetno]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doc_status]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime]
                          FROM ps_m_doc_approve
                where depart_position_name = '" + dept + "' ";//and section_position_name = '" + sect + "'
                dt = zdbUtil.ExecSql_DataTable(strQuery, conn);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLDcoTypeBom()
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = @"SELECT DISTINCT doctype_code,doctype_name FROM ps_m_doctype";
                dt = zdbUtil.ExecSql_DataTable(sql, conn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void delete_EquipListBom(string bom_code, string eq_code, string dept, string sect)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"delete from ps_m_bom where equip_code in(" + eq_code + ") and bom_code = '" + bom_code + "' and depart_position_name='" + dept + "' and section_position_name='" + sect + "'";
                zdbUtil.ExecNonQuery(strSQL, conn);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        public void delete_DocListBom(string bom_code, string doc_id)
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"delete from ps_m_bom_doc where doc_nodeid in(" + doc_id + ") and bom_code = '" + bom_code + "'";
                zdbUtil.ExecNonQuery(strSQL, conn);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
    }
}

