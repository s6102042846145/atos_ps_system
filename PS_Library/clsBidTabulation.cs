﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsBidTabulation
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        CultureInfo th = new CultureInfo("th-TH");
        public clsBidTabulation() { }
        public DataTable getSAPerror(string date)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = " select * from ps_sap_log where api_name = 'PO' and CONVERT(datetime, sent_time,103) > '" + date + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDL(string type)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                if (type != "")  sql = @"select distinct bid_no,bid_revision from ps_xls_template ";
                else sql = @"select * from ps_xls_template where schedule_name <>''";

                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public string getTemplateID(string xbid_no, string xbid_revision, string xschedule_name)
        {
            string xtemplate_id = "";
            string sql = @"   select *
                  from ps_xls_template 
                  where bid_no = '" + xbid_no + "' and bid_revision = '" + xbid_revision + "' and schedule_name = '" + xschedule_name + @"' 
                  order by rowid desc ";
            var ds = db.ExecSql_DataSet(sql, zetl4eisdb);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                xtemplate_id = dr["template_id"].ToString();
            }
            return xtemplate_id;
        }
        public DataTable getDataXls(string search,string tempid)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select *, case when len([description]) > 140 then substring([description],1,140) + '...' else [description] end as desc_short from ps_xls_template xt where template_id in('" + tempid + "')";
                if (search != "") sql += " and " +  search;

                sql += " order by xt.part,xt.item_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getAccXls(string search)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = "select *, case when len(po_item_desc) > 140 then substring(po_item_desc,1,140) + '...' else po_item_desc end as desc_short from ps_t_bid_selling_cmpo_item";
                if (search != "") sql += " where contract_no='"+ search + "'";
                sql += " order by ASCII(item_no), len(item_no),item_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataXls()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = "select distinct schedule_name,bid_no,bid_revision from ps_xls_template xt";
                // sql = @"select *, case when len([description]) > 140 then substring([description],1,140) + '...' else [description] end as desc_short from ps_xls_template xt";
               // sql += " order by xt.part,xt.item_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLVender()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                //sql = @"select distinct (select vendor_name from ps_m_vendor mv where mv.vendor_code=et.vendor_code) as vendor_name
                //        ,(select vendor_type from ps_t_bid_selling_vendor sv where sv.vendor_code=et.vendor_code) as vendor_type
                //        ,et.vendor_code,et.bidder_id from ps_up_excel_transection et";
                sql = @"select distinct (select vendor_name from ps_m_vendor mv where mv.vendor_code=et.vendor_code) as vendor_name,et.bidder_id,et.vendor_code,sv.vendor_type
						from ps_up_excel_transection et
						left join ps_t_bid_selling_vendor sv on  sv.vendor_code=et.vendor_code";
               
                sql += " order by vendor_name";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDDLVender2(string bidid)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct (select vendor_name from ps_m_vendor mv where mv.vendor_code=et.vendor_code) as vendor_name
                        ,et.vendor_code,et.bidder_id from ps_up_excel_transection et";
                if (bidid != "") sql += " where et.bidder_id ='" + bidid + "'";

                sql += " order by vendor_name";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getDataVender(string search,string tempid)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select distinct (select equip_code from ps_m_equipcode te where te.equip_desc_full = xt.description) as equip_code
                        ,(select vendor_name from ps_m_vendor mv where mv.vendor_code=xt.vendor_code) as vendor_name
                        ,xt.part,xt.bidder_id,xt.item_No
						,xt.cif_unit_price,xt.cif_amount,xt.ewp_unit_price,xt.ewp_amount,xt.lci_unit_price,xt.lci_amount
						,((isnull(xt.cif_unit_price,0)*isnull(xt.cif_amount,0)) + (isnull(xt.ewp_unit_price,0)*isnull(xt.ewp_amount,0))+(isnull(xt.lci_unit_price,0)*isnull(xt.lci_amount,0))) as vender_total
						,((isnull(xl.cif_unit_price,0)*isnull(xl.cif_amount,0)) + (isnull(xl.ewp_unit_price,0)*isnull(xl.ewp_amount,0))+(isnull(xl.lci_unit_price,0)*isnull(xl.lci_amount,0))) as xls_total
						,(((isnull(xt.cif_unit_price,0)*isnull(xt.cif_amount,0)) + (isnull(xt.ewp_unit_price,0)*isnull(xt.ewp_amount,0))+(isnull(xt.lci_unit_price,0)*isnull(xt.lci_amount,0)))  -
						((isnull(xl.cif_unit_price,0)*isnull(xl.cif_amount,0)) + (isnull(xl.ewp_unit_price,0)*isnull(xl.ewp_amount,0))+(isnull(xl.lci_unit_price,0)*isnull(xl.lci_amount,0)))) /
						case when(((isnull(xl.cif_unit_price,0)*isnull(xl.cif_amount,0)) + (isnull(xl.ewp_unit_price,0)*isnull(xl.ewp_amount,0))+(isnull(xl.lci_unit_price,0)*isnull(xl.lci_amount,0)) *100)) = 0 then 1 
						else ((isnull(xl.cif_unit_price,0)*isnull(xl.cif_amount,0)) + (isnull(xl.ewp_unit_price,0)*isnull(xl.ewp_amount,0))+(isnull(xl.lci_unit_price,0)*isnull(xl.lci_amount,0)) *100) end as per_dif					
						,xt.vendor_code,xt.vender_file_id
						from ps_up_excel_transection xt
						left join  ps_xls_template xl on xt.bid_no=xl.bid_no"; //xt.item_no = xl.item_no  and 
                if (search != "") sql += " where " + search + " and xl.template_id in('" + tempid + "')";

                sql += " order by part,item_no";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getWhoComment(string login)
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select * from ps_t_bid_selling_committee where emp_id='" + login + "' and committee_type='considered'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getAllComment()
        {
            DataTable dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select (select vendor_name from ps_m_vendor mv where mv.vendor_code=cc.vendor_code) as vendor_name,* from ps_t_bid_selling_committee_comment cc";
                sql += " order by vendor_code";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public void insertComment(string bidno,string rev,string login, string schname,string bidid,string vencode,string tecsc,string prisc,string comment)
        {
            DataTable dt = new DataTable();
            string sql = "";
            string strQuery = "";
            try
            {
                sql = @"select * from ps_t_bid_selling_committee_comment where committee_emp_id='" + login + "' and bidder_id='" + bidid + "' and vendor_code='" + vencode + "' and schedule_name='" + schname + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = @"update ps_t_bid_selling_committee_comment set technical_score='"+ tecsc + "',price_score='"+ prisc + "',comment='"+ comment + @"',comment_datetime=getdate() 
                                where committee_emp_id='" + login + "' and bidder_id='" + bidid + "' and vendor_code='" + vencode + "' and schedule_name='" + schname + "'";
                }
                else
                {
                    strQuery = @"INSERT INTO ps_t_bid_selling_committee_comment
                              ( [bid_no]
                              ,[bid_revision]
                              ,[schedule_name]
                              ,[bidder_id]
                              ,[vendor_code]
                              ,[technical_score]
                              ,[price_score]
                              ,[comment]
                              ,[committee_emp_id]
                              ,[committee_type]
                              ,[comment_datetime])
                            VALUES
                                ('" + bidno + "' " +
                          " , '" + rev + "' " +
                          " , '" + schname + "' " +
                          " , '" + bidid + "' " +
                          " , '" + vencode + "' " +
                          " , '" + tecsc + "' " +
                          " , '" + prisc + "' " +
                          " , '" + comment + "' " +
                          " , '" + login + "' " +
                          " , 'considered' " +
                          " , GETDATE())";
                
                }
                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
           
        }
        public void updateTemplate(string bidno, string rev, string login, string schname,string itmno,string gl,string cc,string wbs,string net,string fund,string rows,string acc_assi,string asse,string inor,string sale)
        {
            DataTable dt = new DataTable();
            string sql = "";
            string strQuery = "";
            try
            {
                sql = @"select * from ps_t_bid_selling_cmpo_item where   bid_no='" + bidno + "' and bid_revision='" + rev + "' and item_no='" + itmno + "' and schedule_name='" + schname + "' and row_id='" + rows + "'";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    strQuery = @"update ps_t_bid_selling_cmpo_item set GL='" + gl + "' ,CostCenter='" + cc + "',WBSNo='" + wbs + "' ,Network='" + net + "' ,Fund='" + fund + "',account_assignment_code='" + acc_assi + "',Asset='" + asse + @"'
                            ,InternalOrder='" + inor + "',SalesOrder='" + sale + @"'
                                where   bid_no='" + bidno + "' and bid_revision='" + rev + "' and item_no='" + itmno + "' and schedule_name='" + schname + "' and row_id = '" + rows + "'";
                }
               
                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void deteleComment(string bidno,string rev,string comby,string rowid)
        {
            try
            {
                string strQuery = "	delete from ps_t_bid_selling_committee_comment where  bid_no='" + bidno + "' and bid_revision='" + rev + "' and committee_emp_id='" + comby + "' and row_id ='" + rowid + "'";
                db.ExecNonQuery(strQuery, zetl4eisdb);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
    }
}
