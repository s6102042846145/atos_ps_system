﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsPriceUpload2
    {
        public string zconnstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        public DbControllerBase zdbUtil = new DbControllerBase();
        public DataTable GetBidno()
        {
            string sql = "select * from bid_no";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetBidScheduleList()
        {
            string sql = @"select bn.bid_id, bn.bid_no, bn.bid_desc
                            from bid_no bn
							left join ps_t_bid_selling_fileupload bsf on bsf.bid_no = bn.bid_no
                            order by bn.bid_no asc";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetBidScheduleList(string bidno)
        {
            string sql = @"select bn.bid_id, bn.bid_no, bn.bid_desc
                            from bid_no bn
							left join ps_t_bid_selling_fileupload bsf on bsf.bid_no = bn.bid_no
                            where bn.bid_no = '" + bidno + "'" +
                            "order by bn.bid_no asc";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetBidScheduleList(int bidid)
        {
            string sql = @"select bn.bid_id, bn.bid_no, bn.bid_desc
                            from bid_no bn
							left join ps_t_bid_selling_fileupload bsf on bsf.bid_no = bn.bid_no
                            where bn.bid_id = '" + bidid + "'" +
                            "order by bn.bid_no asc";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetDocType()
        {
            string sql = "select doctype_code, doctype_name from ps_m_doctype";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetBidSchedule(int bidid)
        {
            string sql = @"select bn.bid_id, bn.bid_no, bn.revision, dbo.udf_StripHTML(bn.bid_desc) as bid_desc, bs.schedule_name
                            from bid_no bn
                            inner join ps_m_bid_schedule bs on (bn.bid_no = bs.bid_no)
                            where bn.bid_id = '" + bidid + "' " +
                            "order by bn.bid_no asc";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetBidVendor(VenderModel model)
        {
            string sql = @"select * from ps_t_bid_selling_vendor
                            where bid_no = '" + model.bid_no + "' and ref_schedule_no = '" + model.ref_schedule_no + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public void InsertBidSellingVendor(VenderModel model)
        {
            string sql = @"
                        insert into ps_t_bid_selling_vendor
                        (
                        bidder_id,
                        bid_no,
                        bid_revision,
                        ref_schedule_no,
                        vendor_code,
                        vendor_name,
                        vendor_type
                        )
                        values
                        (
                        @bidder_id,
                        @bid_no,
                        @bid_revision,
                        @ref_schedule_no,
                        @vendor_code,
                        @vendor_name,
                        @vendor_type
                        )
                        ";
            using (SqlConnection connection = new SqlConnection(zconnstr))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@bidder_id", model.bidder_id);
                command.Parameters.AddWithValue("@bid_no", model.bid_no);
                command.Parameters.AddWithValue("@bid_revision", model.bid_revision);
                command.Parameters.AddWithValue("@ref_schedule_no", string.IsNullOrEmpty(model.ref_schedule_no) ? (object)DBNull.Value : model.ref_schedule_no);
                command.Parameters.AddWithValue("@vendor_code", model.vendor_code);
                command.Parameters.AddWithValue("@vendor_name", model.vendor_name);
                command.Parameters.AddWithValue("@vendor_type", model.vendor_type);

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                connection.Dispose();
            }

        }
        public void DeleteBidSellingVendor(int rowid)
        {
            string sql = @"delete from ps_t_bid_selling_vendor
                            where row_id = " + rowid;
            zdbUtil.ExecNonQuery(sql, zconnstr);
        }
        public DataTable GetVendorFileUpload(string bidno, string scname)
        {
            string sql = @"select * from ps_t_bid_selling_fileupload vf
                            left join ps_m_doctype dt on dt.doctype_code = vf.doc_type
                            where vf.bid_no = '" + bidno + "' and vf.ref_schedule_no = '" + scname + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public void InsertBidSellingFileUpload(FileUploadModel model)
        {
            string sql = @"insert into ps_t_bid_selling_fileupload
                        (
                        bidder_id,
                        bid_no,
                        bid_revision,
                        ref_schedule_no,
                        vendor_code,
                        doc_type,
                        doc_name,
                        doc_version,
                        doc_node_id,
                        status_bid,
                        parent_node_id
                        )
                        values
                        (
                        @bidder_id,
                        @bid_no,
                        @bid_revision,
                        @ref_schedule_no,
                        @vendor_code,
                        @doc_type,
                        @doc_name,
                        @doc_version,
                        @doc_node_id,
                        @status_bid,
                        @parent_node_id
                        )";
            using (SqlConnection connection = new SqlConnection(zconnstr))
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                command.Parameters.AddWithValue("@bidder_id", model.bidder_id);
                command.Parameters.AddWithValue("@bid_no", model.bid_no);
                command.Parameters.AddWithValue("@bid_revision", model.bid_revision);
                command.Parameters.AddWithValue("@ref_schedule_no", model.ref_schedule_no);
                command.Parameters.AddWithValue("@vendor_code", model.vendor_code);
                command.Parameters.AddWithValue("@doc_type", model.doc_type);
                command.Parameters.AddWithValue("@doc_name", model.doc_name);
                command.Parameters.AddWithValue("@doc_version", model.doc_version);
                command.Parameters.AddWithValue("@doc_node_id", model.doc_node_id);
                command.Parameters.AddWithValue("@status_bid", model.status_bid);
                command.Parameters.AddWithValue("@parent_node_id", model.parent_node_id);

                connection.Open();
                command.ExecuteNonQuery();
                connection.Close();
                connection.Dispose();
            }

        }
        public int GetRowIdFileUpload(FileUploadModel model)
        {
            string sql = @"select * from ps_t_bid_selling_fileupload
                            where bid_no = '" + model.bid_no + "' and ref_schedule_no = '" + model.ref_schedule_no + "' and doc_name = '" + model.doc_name + "'" +
                            " and doc_type = '" + model.doc_type + "'";
            int rowid = int.Parse(zdbUtil.ExecSql_DataTable(sql, zconnstr).Rows[0]["row_id"].ToString());
            return rowid;
        }
        public DataTable GetBidSellingFileUpload(string bidno, string scname)
        {
            string sql = @"select bsf.row_id, bsf.doc_node_id, bsf.bidder_id, bsf.parent_node_id, bsv.vendor_name, bsf.doc_name, bsf.doc_type
                            , dt.doctype_name ,bsf.doc_version, bsf.status_bid
							, bn.bid_desc, bn.bid_no
                            from ps_t_bid_selling_fileupload bsf
                            left join ps_t_bid_selling_vendor bsv on bsv.vendor_code = bsf.vendor_code
                            left join ps_m_doctype dt on dt.doctype_code = bsf.doc_type
							left join bid_no bn on bn.bid_no = bsf.bid_no
                            where bsf.bid_no = '" + bidno + "' and bsf.ref_schedule_no = '" + scname + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public void DeleteBisSellingFileUpload(string rowid)
        {
            string sql = @"delete from ps_t_bid_selling_fileupload
                            where row_id = " + rowid;
            zdbUtil.ExecNonQuery(sql, zconnstr);

        }
        public DataTable GetBidder(string bidno)
        {
            string sql = @"select * from ps_t_bid_selling
                            where bid_no = '" + bidno + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetVendor()
        {
            string sql = "select vendor_code code, vendor_name name from ps_m_vendor";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetVendor(string vendorcode)
        {
            string sql = "select vendor_code code, vendor_name name from ps_m_vendor where vendor_code = '" + vendorcode + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetBidderOnBidSelling(string bidno)
        {
            string sql = @"select * from ps_t_bid_selling
                            where bid_no = '" + bidno + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
        public DataTable GetVendorOnBidSellingVendor(string bidno)
        {
            string sql = @"select * from ps_t_bid_selling_vendor
                            where bid_no = '" + bidno + "'";
            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zconnstr);
            return dt;
        }
    }
    public class VenderModel
    {
        public string row_id { get; set; }
        public string bidder_id { get; set; }
        public string bid_no { get; set; }
        public string bid_revision { get; set; }
        public string ref_schedule_no { get; set; }
        public string vendor_code { get; set; }
        public string vendor_name { get; set; }
        public string vendor_type { get; set; }
    }
    public class FileUploadModel
    {
        public string row_id { get; set; }
        public string bidder_id { get; set; }
        public string bid_no { get; set; }
        public string bid_revision { get; set; }
        public string ref_schedule_no { get; set; }
        public string vendor_code { get; set; }
        public string doc_type { get; set; }
        public string doc_name { get; set; }
        public string doc_version { get; set; }
        public string doc_node_id { get; set; }
        public string status_bid { get; set; }
        public string parent_node_id { get; set; }
    }
}
