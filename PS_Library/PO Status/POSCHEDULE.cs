﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class POSCHEDULE
    {
        public string PO_NUMBER { get; set; }//5210000668
        public string PO_ITEM { get; set; }
        public string SCHED_LINE { get; set; }
        public string DELIVERY_DATE { get; set; }
        public decimal QUANTITY { get; set; }

    }
}
