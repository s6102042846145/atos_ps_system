﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class ps_t_sap_postatus_history
    {
		public string po_number { get; set; }
		public string po_item { get; set; }//5210000668
		public string serial_no { get; set; }
		public string doc_year { get; set; }
		public string mat_doc { get; set; }
		public string matdoc_itm { get; set; }
		public string hist_type { get; set; }
		public DateTime pstng_date { get; set; }
		public decimal quantity { get; set; }
		public decimal val_loccur { get; set; }
		public decimal val_forcur { get; set; }
		public string currency { get; set; }
	}
}
