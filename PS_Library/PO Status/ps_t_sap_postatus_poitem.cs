﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class ps_t_sap_postatus_poitem
    {
		public string po_number { get; set; }
		public string po_item { get; set; }
		public string delete_ind { get; set; }
		public string short_text { get; set; }
		public string material { get; set; }
		public string plant { get; set; }
		public string trackingno { get; set; }
		public string matl_group { get; set; }
		public decimal quantity { get; set; }
		public string po_unit { get; set; }
		public decimal net_price { get; set; }
		public string price_unit { get; set; }
		public string item_cat { get; set; }
		public string acctasscat { get; set; }
		public string incoterm1 { get; set; }
		public string vend_part { get; set; }
		public string preq_name { get; set; }
		public decimal deliv_qty { get; set; }
		public decimal iv_qty { get; set; }
	}
}
