﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class ps_t_sap_postatus_hdr
    {
		public string contno { get; set; } 
		public string po_num { get; set; }//5210000668
		public string doc_type { get; set; }
		public string vendor { get; set; }
		public string purch_org { get; set; }
		public string pur_group { get; set; }
		public string currency { get; set; }
		public DateTime doc_date { get; set; }
		public decimal exch_rate { get; set; }
		public string po_rel_ind { get; set; }
		
	}
}
