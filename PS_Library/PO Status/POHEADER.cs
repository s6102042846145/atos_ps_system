﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public  class POHEADER
    {
		public string CONTNO { get; set; } 
		public string PO_NUMBER { get; set; }//5210000668
		public string DOC_TYPE { get; set; }
		public string VENDOR { get; set; }
		public string PURCH_ORG { get; set; }
		public string PUR_GROUP { get; set; }
		public string CURRENCY { get; set; }
		public string EXCH_RATE { get; set; }
		public string DOC_DATE { get; set; }
		public string PO_REL_IND { get; set; }
		public string OldMatNo { get; set; }

       
    }
}
