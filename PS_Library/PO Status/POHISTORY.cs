﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class POHISTORY
    {
		public string PO_NUMBER { get; set; }//5210000668
		public string PO_ITEM { get; set; }
		public string SERIAL_NO { get; set; }
		public string DOC_YEAR { get; set; }
		public string MAT_DOC { get; set; }
		public string MATDOC_ITM { get; set; }
		public string HIST_TYPE { get; set; }
		public string PSTNG_DATE { get; set; }
		public decimal QUANTITY { get; set; }
		public decimal VAL_LOCCUR { get; set; }
        public decimal VAL_FORCUR { get; set; }
        public string CURRENCY { get; set; }

		
    }
}
