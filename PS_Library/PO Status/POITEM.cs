﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class POITEM
    {

        public string PO_NUMBER { get; set; }//5210000668
        public string PO_ITEM { get; set; }
        public string DELETE_IND { get; set; }
        public string SHORT_TEXT { get; set; }
        public string MATERIAL { get; set; }
        public string PLANT { get; set; }
        public string TRACKINGNO { get; set; }
        public string MATL_GROUP { get; set; }
        public decimal QUANTITY { get; set; }
        public string PO_UNIT { get; set; }
        public decimal NET_PRICE { get; set; }
        public int PRICE_UNIT { get; set; }
        public string ITEM_CAT { get; set; }
        public string ACCTASSCAT { get; set; }
        public string INCOTERMS1 { get; set; }
        public string VEND_PART { get; set; }
        public string PREQ_NAME { get; set; }
        public decimal DELIV_QTY { get; set; }
        public decimal IV_QTY { get; set; }

    }
}
