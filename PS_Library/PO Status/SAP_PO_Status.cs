﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Globalization;

namespace PS_Library.PO_Status
{
   

    public static class SAP_PO_Status
    {

        public static string psDB = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();

        private static ps_sap_log sap_log = new ps_sap_log();
        public static string connstr { get; set; }

        public static string cmdstrFull { get; set; }
        public static string cmdstr { get; set; }

        public static string cmdvalue { get; set; }
        public static string condi { get; set; }
        public static string order { get; set; }

        public static string Po_Status_SendToSap(string CONTNO, string PO_NUMBER)
        {
            string result = "";
            // DataTable dtCNO = new DataTable();
            DataTable dt = new DataTable();
            //equip_code = "T213SQB23099";
            if (CONTNO != "")
            {
                dt = getDataContact(CONTNO,"co");
            }
            if (PO_NUMBER != "")
            {
                dt = getDataContact(PO_NUMBER,"po");
            }

            foreach (DataRow dr in dt.Rows)
            {
                result = delete_ps_t_sap_postatus(dr["po_num"].ToString());
            }


            //CONTNO = item;
            result = genXMLRequestPoStatus(CONTNO, PO_NUMBER);
            

            return result;
            //createRequestPO();
        }


        private static string getFormatDate(DateTime date, string format)
        {
            string strDate = "";

            string strMonth = date.Month.ToString().Length == 1 ? ("0" + date.Month) : date.Month.ToString();
            string strDay = date.Day.ToString().Length == 1 ? ("0" + date.Day) : date.Day.ToString();
            if (format == "YYYY-MM-DD")
            {
                strDate = date.Year + "-" + date.Month + "-" + date.Day;


            }

            return strDate;


        }


        //PO_Status
        #region PO_Status





        public static List<int> insert_ps_t_sap_postatus_hdr(ps_t_sap_postatus_hdr ps_t_sap_postatus_hdr)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_t_sap_postatus_hdr (";
                cmdstr = cmdstr + getNameForInsert(ps_t_sap_postatus_hdr) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(ps_t_sap_postatus_hdr) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }
        public static List<int> insert_ps_t_sap_postatus_history(ps_t_sap_postatus_history ps_t_sap_postatus_history)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_t_sap_postatus_history (";
                cmdstr = cmdstr + getNameForInsert(ps_t_sap_postatus_history) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(ps_t_sap_postatus_history) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }

        public static List<int> insert_ps_t_sap_postatus_poitem(ps_t_sap_postatus_poitem ps_t_sap_postatus_poitem)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_t_sap_postatus_poitem (";
                cmdstr = cmdstr + getNameForInsert(ps_t_sap_postatus_poitem) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(ps_t_sap_postatus_poitem) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }

        public static List<int> insert_ps_t_sap_postatus_poschedule(ps_t_sap_postatus_poschedule ps_t_sap_postatus_poschedule)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_t_sap_postatus_poschedule (";
                cmdstr = cmdstr + getNameForInsert(ps_t_sap_postatus_poschedule) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(ps_t_sap_postatus_poschedule) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }





        public static List<int> insert_ps_sap_log(ps_sap_log sap_log)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_sap_log (";
                cmdstr = cmdstr + getNameForInsert(sap_log) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(sap_log) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }




        public static string getNameForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;
            PropertyInfo[] properties = _class.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!property.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {
                        value = property.Name;
                    }
                    else
                    {
                        value = property.Name + ",";
                    }
                    result = result + value;
                }
                count++;

            }
            return result;
        }

        public static string cutLastString(string item)
        {
            string result = "";
            result = item.Substring(0, item.Length - 1);
            return result;
        }
        public static string getValueForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;

            PropertyInfo[] properties = _class.GetType().GetProperties();


            foreach (var p in properties)
            {
                if (!p.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {

                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {
                            value = "GETDATE()" + "";
                        }
                        else
                        {
                            value = "'" + p.GetValue(_class) + "'";
                        }


                    }
                    else
                    {
                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {

                            value = "GETDATE()" + ",";

                        }
                        else
                        {
                            value = "'" + p.GetValue(_class) + "'" + ",";
                        }

                    }
                    result = result + value;
                }
                count++;
            }


            return result;
        }


        public static string delete_ps_t_sap_postatus(string po_no)
        {

            string result = "success";
            DataTable dtRunno = new DataTable();

            try
            {

                cmdstrFull = "";
                cmdstr = "";
                condi = "";

                cmdstr = @"delete ps_t_sap_postatus_poschedule ";

                cmdvalue = "";
                condi = "where po_number = '" + po_no + "' ";
                cmdstrFull = cmdstr + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;

                return result;
            }

            try
            {

                cmdstrFull = "";
                cmdstr = "";
                condi = "";


                cmdstr = @"delete ps_t_sap_postatus_poitem ";

                cmdvalue = "";
                condi = "where po_number = '" + po_no + "' ";
                cmdstrFull = cmdstr + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;

                return result;
            }

            try
            {

                cmdstrFull = "";
                cmdstr = "";
                condi = "";


                cmdstr = @"delete ps_t_sap_postatus_history ";

                cmdvalue = "";
                condi = "where po_number = '" + po_no + "' ";
                cmdstrFull = cmdstr + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;

                return result;
            }

            try
            {

                cmdstrFull = "";
                cmdstr = "";
                condi = "";


                cmdstr = @"delete ps_t_sap_postatus_hdr ";

                cmdvalue = "";
                condi = "where po_num = '" + po_no + "' ";
                cmdstrFull = cmdstr + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;

                return result;
            }



            return result;
        }

        public static DataTable getContractNo()
        {

            string sqlFull = "";
            string sql = @"SELECT [contract_no],[status] FROM [dbo].[ps_t_contract_no] ";
            string condi = " where UPPER([status]) = UPPER('new')";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }


        public static DataTable getDataContact(string value,string type)
        {

            string sqlFull = "";
            string sql = @"SELECT po_num FROM [dbo].[ps_t_sap_postatus_hdr] ";
            string condi = " where [contno] = '" + value + "'";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();

            if(type == "po")
            {
                condi = " where [po_num] = '" + value + "'";
            }
            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }

        public static string getPOID()
        {

            string pid = "";
            string result = UpdatePOID();
            // string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            DataTable dtRunno = new DataTable();

            try
            {
                if (result.Equals("success"))
                {
                    cmdstrFull = "";
                    cmdstr = @"select runno from wf_runno ";
                    condi = " WHERE prefix = 'PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + condi;

                    dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);
                    if (checkRowData(dtRunno))
                    {
                        pid = dtRunno.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // result = ex.Message;
            }

            return pid;
        }
        public static string getYearThai()
        {
            string yearThai = "";



            string strdate = DateTime.Now.Year.ToString();

            yearThai = (Convert.ToInt32(strdate) + 543).ToString();
            return yearThai;
        }
        public static string UpdatePOID()
        {
            string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            int runno = 0;
            int runnoNew = 0;
            DataTable dtRunno = new DataTable();
            try
            {
                cmdstrFull = "";
                cmdstr = @"select runno from wf_runno ";
                condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                order = " ";
                cmdstrFull = cmdstr + condi;

                dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);

                if (checkRowData(dtRunno))
                {
                    runno = Convert.ToInt32(dtRunno.Rows[0][0].ToString());
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"update wf_runno ";
                    cmdvalue = " set runno = " + runnoNew;
                    condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }
                else
                {
                    runno = 0;
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"insert into wf_runno ( prefix, runno,year_thai)";
                    cmdvalue = "VALUES  ( select 'PO', '" + runnoNew + "' , '" + yearThai + "' )";
                    condi = " ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        public static string update_ps_m_equipcode(string equip_code, string sap_mat_code)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_m_equipcode ";
                cmdstr = cmdstr + "Set sap_mat_code = '";
                cmdvalue = sap_mat_code + "' ";
                condi = "where equip_code = '" + equip_code + "'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static string update_ps_t_contract_no(string status, string res, string cNo)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_t_contract_no ";
                cmdstr = cmdstr + "Set status = '";
                cmdvalue = status + "' ," + " result_sap = '" + res + "'";
                condi = "where contract_no = '" + cNo + "' and status = 'New'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static bool checkRowData(DataTable dt)
        {
            bool check = false;
            if (dt.Rows.Count > 0)
            {
                check = true;
            }
            else
            {
                check = false;
            }
            return check;
        }



        public static List<POHEADER> addPOHEADER(string CONTNO,string PO_NUMBER)
        {
            List<POHEADER> listPOHEADERItems = new List<POHEADER>();
            POHEADER POHEADERItems = new POHEADER();


           
                POHEADERItems = new POHEADER();

                POHEADERItems.CONTNO = CONTNO;
                POHEADERItems.PO_NUMBER = PO_NUMBER;


                listPOHEADERItems.Add(POHEADERItems);
            
            return listPOHEADERItems;
        }




        public static string genXMLRequestPoStatus(string CONTNO, string PO_NUMBER)
        {
            Console.WriteLine("##########  Send To SAP ###########");
            var soapNs = @"http://schemas.xmlsoap.org/soap/envelope/";
            var soapURN = @"urn:sap-com:document:sap:rfc:functions";//urn:sap-com:document:sap:soap:functions:mc-style
            var bodyNs = @"";

            XmlDocument doc = new XmlDocument();

            //// Create an XML declaration.
            //XmlDeclaration xmldecl;
            //xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            //xmldecl.Encoding = "UTF-8";

            //// Add the new node to the document.
            //XmlElement rootHead = doc.DocumentElement;
            //doc.InsertBefore(xmldecl, rootHead);



            XmlElement root = doc.CreateElement("soapenv", "Envelope", soapNs);
            root.SetAttribute("xmlns:soapenv", soapNs);
            root.SetAttribute("xmlns:urn", soapURN);
            doc.AppendChild(root);

            var header = root.AppendChild(doc.CreateElement("soapenv", "Header", soapNs));
            var body = root.AppendChild(doc.CreateElement("soapenv", "Body", soapNs));
            var ZBAPI_ECM_PO_STATUS = body.AppendChild(doc.CreateElement("urn:ZBAPI_ECM_PO_STATUS", soapURN));


            string className = "";

            #region POHEADER
            // Materialclass
            POHEADER POHEADERItems = new POHEADER();
            PropertyInfo[] properties = POHEADERItems.GetType().GetProperties();
            className = POHEADERItems.GetType().Name;
            var nodePOHEADER = ZBAPI_ECM_PO_STATUS.AppendChild(doc.CreateElement("", className, bodyNs));



            foreach (var item in addPOHEADER(CONTNO, PO_NUMBER))
            {
                doc = addData(doc, nodePOHEADER, item);
            }
            #endregion



            //#region Return
            //// Return
            //ReturnMaterial ReturnItems = new ReturnMaterial();
            //properties = ReturnItems.GetType().GetProperties();
            //className = ReturnItems.GetType().Name.Substring(0, ReturnItems.GetType().Name.Length - 2);
            //var nodeReturn = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addReturnMaterial())
            //{
            //    doc = addData(doc, nodeReturn, item);
            //}
            //#endregion

            ////var Testrun = ZbapiEcmMaterial.AppendChild(doc.CreateElement("", "Testrun", bodyNs));
            var POHISTORY = ZBAPI_ECM_PO_STATUS.AppendChild(doc.CreateElement("", "POHISTORY", bodyNs));
            var POITEM = ZBAPI_ECM_PO_STATUS.AppendChild(doc.CreateElement("", "POITEM", bodyNs));
            var POSCHEDULE = ZBAPI_ECM_PO_STATUS.AppendChild(doc.CreateElement("", "POSCHEDULE", bodyNs));
            var RETURN = ZBAPI_ECM_PO_STATUS.AppendChild(doc.CreateElement("", "RETURN", bodyNs));

            //ReservationItemsChg resItemsChg = new ReservationItemsChg();
            //doc = addData(doc, ZbapiEcmReservation, resItemsChg);

            //ReservationItemsNew resItemsNew = new ReservationItemsNew();
            //doc = addData(doc, ZbapiEcmReservation, resItemsNew);

            //Reservationheader resheader = new Reservationheader();
            //doc = addData(doc, ZbapiEcmReservation, resheader);

           // doc.Save("C:\\testXML\\SAP_POStatustoSAP.xml");
            //XmlDocument doc3 = new XmlDocument();
            //doc3.Load("C:\\testXML\\PO.xml");
            string data = XmlSerialize(doc);



            var url = ConfigurationManager.AppSettings["url_qa_status"].ToString();
            var username = ConfigurationManager.AppSettings["username"].ToString();
            var password = ConfigurationManager.AppSettings["password"].ToString();

            //var url = "http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/rfc/sap/zecm_po/300/zecm_po/zecm_po";// ConfigurationManager.AppSettings["wbs_url"].ToString();
            //var username = "ECMCONNECT";//ConfigurationManager.AppSettings["wbs_username"].ToString();
            //var password = "egat2020";// ConfigurationManager.AppSettings["wbs_password"].ToString();


            /*var res =*/
            string res = DoHttpWebRequestMat(url, "POST", data, username, password);
            sap_log = new ps_sap_log();
            sap_log.send_xlm_text = data;
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml(res);
            // string FileName = "Result" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // FileName = FileName + ".xml";
            //// package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));
            // doc2.Save("C:\\" + FileName);
            //doc2.Save("C:\\testXML\\returnPOStatus.xml");
            // doc2.Load("C:\\testXML\\returnMat.xml");
            string error = "";
            string sap_mat_code = "";

            //object POCONFIRMATIONData = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].Name; // "doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0]";
            //object POHEADER_EXData = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes; //"doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1]";
            //object POHISTORYData = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].Name; //"doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2]";
            //object POITEMData = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].Name; //"doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3]";
            //object POSCHEDULEData = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].Name; //"doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4]";
            //object ReturnData = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[5].Name; //"doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[5]";


            //getData
            bool status = true;
            string reult = "success";
            CultureInfo provider = CultureInfo.InvariantCulture;
            #region POHEADER

            List<ps_t_sap_postatus_hdr> listPOHEADER = new List<ps_t_sap_postatus_hdr>();
            ps_t_sap_postatus_hdr POHEADERdata = new ps_t_sap_postatus_hdr();

            for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes.Count; i++)
            {

                POHEADERdata = new ps_t_sap_postatus_hdr();
                POHEADERdata.contno = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[0].InnerText;
                POHEADERdata.po_num = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[1].InnerText;
                POHEADERdata.doc_type = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[2].InnerText;
                POHEADERdata.vendor = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[3].InnerText;
                POHEADERdata.purch_org = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[4].InnerText;
                POHEADERdata.pur_group = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[5].InnerText;
                POHEADERdata.currency = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[6].InnerText;
                POHEADERdata.exch_rate = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[7].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[7].InnerText); 
                POHEADERdata.doc_date = DateTime.ParseExact(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[8].InnerText, "yyyy-MM-dd", provider);
                POHEADERdata.po_rel_ind = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[9].InnerText;

                

                listPOHEADER.Add(POHEADERdata);
            }
            foreach (var item in listPOHEADER)
            {
                insert_ps_t_sap_postatus_hdr(item);
            }

            // List<POHEADER> listPOHEADER = new List<POHEADER>();
            // POHEADER POHEADERdata = new POHEADER();

            //for (int i =0;i< doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes.Count;i++)
            // {

            //     POHEADERdata = new POHEADER();
            //     POHEADERdata.CONTNO = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[0].InnerText;
            //     POHEADERdata.PO_NUMBER = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[1].InnerText;
            //     POHEADERdata.DOC_TYPE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[2].InnerText;
            //     POHEADERdata.VENDOR = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[3].InnerText;
            //     POHEADERdata.PURCH_ORG = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[4].InnerText;
            //     POHEADERdata.PUR_GROUP = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[5].InnerText;
            //     POHEADERdata.CURRENCY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[6].InnerText;
            //     POHEADERdata.EXCH_RATE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[7].InnerText;
            //     POHEADERdata.DOC_DATE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[8].InnerText;
            //     POHEADERdata.PO_REL_IND = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[i].ChildNodes[9].InnerText;


            //     listPOHEADER.Add(POHEADERdata);
            // }
            #endregion

            #region POHISTORY
            List<ps_t_sap_postatus_history> listPOHISTORY = new List<ps_t_sap_postatus_history>();
            ps_t_sap_postatus_history POHISTORYdata = new ps_t_sap_postatus_history();

            for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes.Count; i++)
            {

                POHISTORYdata = new ps_t_sap_postatus_history();
                POHISTORYdata.po_number = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[0].InnerText;
                POHISTORYdata.po_item = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[1].InnerText;
                POHISTORYdata.serial_no = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[2].InnerText;
                POHISTORYdata.doc_year = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[3].InnerText;
                POHISTORYdata.mat_doc = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[4].InnerText;
                POHISTORYdata.matdoc_itm = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[5].InnerText;
                POHISTORYdata.hist_type = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[6].InnerText;
                POHISTORYdata.pstng_date = DateTime.ParseExact(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[7].InnerText, "yyyy-MM-dd", provider);
                POHISTORYdata.quantity = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[8].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[8].InnerText);
                POHISTORYdata.val_loccur = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[9].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[9].InnerText);
                POHISTORYdata.val_forcur = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[10].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[10].InnerText);
                POHISTORYdata.currency = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[11].InnerText;


                listPOHISTORY.Add(POHISTORYdata);
            }
            foreach (var item in listPOHISTORY)
            {
                insert_ps_t_sap_postatus_history(item);
            }
            //List<POHISTORY> listPOHISTORY = new List<POHISTORY>();
            //POHISTORY POHISTORYdata = new POHISTORY();

            //for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes.Count; i++)
            //{

            //    POHISTORYdata = new POHISTORY();
            //    POHISTORYdata.PO_NUMBER = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[0].InnerText;
            //    POHISTORYdata.PO_ITEM = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[1].InnerText;
            //    POHISTORYdata.SERIAL_NO = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[2].InnerText;
            //    POHISTORYdata.DOC_YEAR = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[3].InnerText;
            //    POHISTORYdata.MAT_DOC = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[4].InnerText;
            //    POHISTORYdata.MATDOC_ITM = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[5].InnerText;
            //    POHISTORYdata.HIST_TYPE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[6].InnerText;
            //    POHISTORYdata.PSTNG_DATE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[7].InnerText;
            //    POHISTORYdata.QUANTITY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[8].InnerText == "" ? 0:Convert.ToDecimal( doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[8].InnerText);
            //    POHISTORYdata.VAL_LOCCUR = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[9].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[9].InnerText);
            //    POHISTORYdata.VAL_FORCUR = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[10].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[10].InnerText);
            //    POHISTORYdata.CURRENCY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[2].ChildNodes[i].ChildNodes[11].InnerText;


            //    listPOHISTORY.Add(POHISTORYdata);
            //}
            #endregion


            #region POITEM
            List<ps_t_sap_postatus_poitem> listPOITEM = new List<ps_t_sap_postatus_poitem>();
            ps_t_sap_postatus_poitem POITEMdata = new ps_t_sap_postatus_poitem();

            for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes.Count; i++)
            {

                POITEMdata = new ps_t_sap_postatus_poitem();
                POITEMdata.po_number = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[0].InnerText;
                POITEMdata.po_item = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[1].InnerText;
                POITEMdata.delete_ind = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[2].InnerText;
                POITEMdata.short_text = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[3].InnerText;
                POITEMdata.material = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[4].InnerText;
                POITEMdata.plant = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[5].InnerText;
                POITEMdata.trackingno = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[6].InnerText;
                POITEMdata.matl_group = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[7].InnerText;
                POITEMdata.quantity = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[8].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[8].InnerText);
                POITEMdata.po_unit = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[9].InnerText;
                POITEMdata.net_price = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[10].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[10].InnerText);
                POITEMdata.price_unit = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[11].InnerText ;
                POITEMdata.item_cat = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[12].InnerText;
                POITEMdata.acctasscat = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[13].InnerText;
                POITEMdata.incoterm1 = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[14].InnerText;
                POITEMdata.vend_part = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[15].InnerText;
                POITEMdata.preq_name = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[16].InnerText;
                POITEMdata.deliv_qty = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[17].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[17].InnerText);
                POITEMdata.iv_qty = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[18].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[18].InnerText);


                listPOITEM.Add(POITEMdata);
            }

            foreach (var item in listPOITEM)
            {
                insert_ps_t_sap_postatus_poitem(item);
            }


            //List<POITEM> listPOITEM = new List<POITEM>();
            //POITEM POITEMdata = new POITEM();

            //for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes.Count; i++)
            //{

            //    POITEMdata = new POITEM();
            //    POITEMdata.PO_NUMBER = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[0].InnerText;
            //    POITEMdata.PO_ITEM = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[1].InnerText;
            //    POITEMdata.DELETE_IND = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[2].InnerText;
            //    POITEMdata.SHORT_TEXT = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[3].InnerText;
            //    POITEMdata.MATERIAL = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[4].InnerText;
            //    POITEMdata.PLANT = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[5].InnerText;
            //    POITEMdata.TRACKINGNO = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[6].InnerText;
            //    POITEMdata.MATL_GROUP = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[7].InnerText;
            //    POITEMdata.QUANTITY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[8].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[8].InnerText);
            //    POITEMdata.PO_UNIT = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[9].InnerText;
            //    POITEMdata.NET_PRICE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[10].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[10].InnerText);
            //    POITEMdata.PRICE_UNIT = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[11].InnerText == "" ? 0 : Convert.ToInt32(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[11].InnerText);
            //    POITEMdata.ITEM_CAT = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[12].InnerText;
            //    POITEMdata.ACCTASSCAT = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[13].InnerText;
            //    POITEMdata.INCOTERMS1 = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[14].InnerText;
            //    POITEMdata.VEND_PART = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[15].InnerText;
            //    POITEMdata.PREQ_NAME = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[16].InnerText;
            //    POITEMdata.DELIV_QTY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[17].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[17].InnerText);
            //    POITEMdata.IV_QTY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[18].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[3].ChildNodes[i].ChildNodes[18].InnerText);


            //    listPOITEM.Add(POITEMdata);
            //}
            #endregion

            #region POSCHEDULE
            List<ps_t_sap_postatus_poschedule> listPOSCHEDULE = new List<ps_t_sap_postatus_poschedule>();
            ps_t_sap_postatus_poschedule POSCHEDULEdata = new ps_t_sap_postatus_poschedule();

            for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes.Count; i++)
            {

                POSCHEDULEdata = new ps_t_sap_postatus_poschedule();
                POSCHEDULEdata.po_number = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[0].InnerText;
                POSCHEDULEdata.po_item = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[1].InnerText;
                POSCHEDULEdata.sched_line = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[2].InnerText;
                POSCHEDULEdata.delivery_date = DateTime.Parse(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[3].InnerText);
                POSCHEDULEdata.quantity = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[4].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[4].InnerText);

                listPOSCHEDULE.Add(POSCHEDULEdata);
            }

            
            foreach(var item in listPOSCHEDULE)
            {
                insert_ps_t_sap_postatus_poschedule(item);
            }

            //List<POSCHEDULE> listPOSCHEDULE = new List<POSCHEDULE>();
            //POSCHEDULE POSCHEDULEdata = new POSCHEDULE();

            //for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes.Count; i++)
            //{

            //    POSCHEDULEdata = new POSCHEDULE();
            //    POSCHEDULEdata.PO_NUMBER = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[0].InnerText;
            //    POSCHEDULEdata.PO_ITEM = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[1].InnerText;
            //    POSCHEDULEdata.SCHED_LINE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[2].InnerText;
            //    POSCHEDULEdata.DELIVERY_DATE = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[3].InnerText;
            //    POSCHEDULEdata.QUANTITY = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[4].InnerText == "" ? 0 : Convert.ToDecimal(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[4].ChildNodes[i].ChildNodes[4].InnerText);

            //    listPOSCHEDULE.Add(POSCHEDULEdata);
            //}
            #endregion

            //sap_mat_code = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[1].InnerText;

            CultureInfo th = new CultureInfo("th-TH");


            sap_log.api_name = "PO_Status";
            sap_log.return_message = res;
            sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
            sap_log.error_msg_othr = error;
            insert_ps_sap_log(sap_log);

           
            //UpdatePOID();
            //if (!PoNumber.Equals(""))
            //{

            //    update_ps_t_contract_no("S",res, cNo);
            //}else
            //{
            //    //List<string> listMessage = new List<string>();
            //    //listMessage = getListError(doc2, "Message");


            //    //int i = 0;
            //    //ConsoleUtility.WriteProgress(i);
            //    //foreach (var item in listMessage)
            //    //{
            //    //    error += item + ",";
            //    //    ConsoleUtility.WriteProgress(i, true);
            //    //    Thread.Sleep(50);
            //    //    i++;
            //    //}

            //    //error = doc2.GetElementsByTagName("Return").Item(0).ChildNodes[5].ChildNodes[4].InnerText;
            //    error = doc2.GetElementsByTagName("Return").Item(0).InnerText;

            //    update_ps_t_contract_no("E", error, cNo);
            //}

            //List<string[]> listPoNumber = new List<string[]>();
            //listPoNumber = getListPoNumber(doc2);

            //List<string> listType = new List<string>();
            //listType = getListError(doc2, "Type");

            //List<string> listMessage = new List<string>();
            //listMessage = getListError(doc2, "Message");

            return reult;


        }
        public static List<string[]> getListPoNumber(XmlDocument doc)
        {
            List<string[]> listPoNumber = new List<string[]>();
            string[] numPO = new string[2];
            int count = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes.Count;
            //var x = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[0].InnerText;
            //var x2 = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[1].InnerText;
            for (int i = 0; i < count; i++)
            {
                numPO = new string[2];
                numPO[0] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[0].InnerText;
                numPO[1] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[1].InnerText;

            }
            listPoNumber.Add(numPO);
            return listPoNumber;



        }





        private static string DoHttpWebRequestMat(string url, string method, string data, string username, string password)
        {
            //SAP_Material.ZbapiEcmMaterial MatResponse = new SAP_Material.ZbapiEcmMaterial();

            WrtieLogWbs("Request " + data);

            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.KeepAlive = false;
            req.ContentType = "text/xml";
            req.Method = method;
            req.Credentials = new NetworkCredential(username, password);

            byte[] buffer = Encoding.UTF8.GetBytes(data);
            Stream PostData = req.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            var result = "";

            try
            {
                //Console.WriteLine("Connecting to SAP.......");
                using (WebResponse webResponse = req.GetResponse())
                {
                    //ConsoleUtility.WriteProgressBar(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgressBar(i, true);
                    //    Thread.Sleep(30);
                    //}
                    //Console.WriteLine("");
                    //Console.WriteLine("Connected");

                    //ConsoleUtility.WriteProgress(0);
                    ////var getResponse = req.GetResponse();
                    //// Stream newStream = getResponse.GetResponseStream();
                    Stream newStream = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(newStream);
                    result = sr.ReadToEnd();

                    WrtieLogWbs("Result " + result);

                    //ConsoleUtility.WriteProgress(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgress(i, true);
                    //    Thread.Sleep(50);
                    //}
                }
            }
            catch (Exception ex){
                CultureInfo th = new CultureInfo("th-TH");
                sap_log = new ps_sap_log();

                sap_log.api_name = "PO_Status";
                sap_log.send_xlm_text = data;
                sap_log.return_message = ex.Message;
                sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                sap_log.error_msg_othr = ex.Message;
                insert_ps_sap_log(sap_log);
            }


            //var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
            //return settings;

            return result;


        }


        //public static List<ReturnMaterial> addReturnMaterial()
        //{
        //    List<ReturnMaterial> listReturnItems = new List<ReturnMaterial>();
        //    ReturnMaterial ReturnItems = new ReturnMaterial();
        //    listReturnItems.Add(ReturnItems);
        //    return listReturnItems;
        //}

        public static XmlDocument addData(XmlDocument doc, XmlNode xnode, object _class)
        {
            var bodyNs = @"";
            PropertyInfo[] properties = _class.GetType().GetProperties();
            //if (className.Contains("Item"))
            //{
            var item = xnode.AppendChild(doc.CreateElement("", "item", bodyNs));

            foreach (var p in properties)
            {
                if (p.GetValue(_class) != null)
                {
                    item.AppendChild(doc.CreateElement("", p.Name, bodyNs)).InnerText = p.GetValue(_class).ToString();
                }
                else
                {
                    item.AppendChild(doc.CreateElement("", p.Name.Trim(), bodyNs));
                }

            }

            return doc;

        }
        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }




        private static string xmlToJson(string xml)
        {
            //if (fileName.Contains("feedback"))
            //{
            //    return;
            //}

            //string xml = File.ReadAllText(fileName);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            //File.WriteAllText(fileName.Replace(".xml", ".json"), json);
            return json;
        }


        private static string XmlSerialize<T>(T entity) where T : class
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                // removes namespace
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add("q1", "http://schemas.xmlsoap.org/soap/envelope/");
                xmlns.Add("q2", "urn:sap-com:document:sap:rfc:functions");

                xsSubmit.Serialize(writer, entity, xmlns);
                return sw.ToString(); // Your XML
            }
        }

        private static T DeserializeFromXmlString<T>(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
        //private static ModelReturn.Envelope DoHttpWebRequest(string url, string method, string data, string username, string password)
        //{
        //    WrtieLogWbs("Request " + data);

        //    HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
        //    req.KeepAlive = false;
        //    req.ContentType = "text/xml";
        //    req.Method = method;
        //    req.Credentials = new NetworkCredential(username, password);

        //    byte[] buffer = Encoding.UTF8.GetBytes(data);
        //    Stream PostData = req.GetRequestStream();
        //    PostData.Write(buffer, 0, buffer.Length);
        //    PostData.Close();

        //    var getResponse = req.GetResponse();
        //    Stream newStream = getResponse.GetResponseStream();
        //    StreamReader sr = new StreamReader(newStream);
        //    var result = sr.ReadToEnd();

        //    WrtieLogWbs("Result " + result);

        //    var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
        //    return settings;
        //}


        private static void WrtieLogWbs(string str)
        {
            System.IO.Directory.CreateDirectory(@"c:\logwbs");
            var currDate = DateTime.Now.ToString("yyyy-MM-dd");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\logwbs\" + currDate + ".txt", true))
            {
                file.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + str);
            }
        }

        #endregion
    }
}
