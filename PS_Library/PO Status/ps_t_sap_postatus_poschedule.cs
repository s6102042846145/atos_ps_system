﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO_Status
{
    public class ps_t_sap_postatus_poschedule
    {
		public string po_number { get; set; }
		public string po_item { get; set; }
		public string sched_line { get; set; }
		public DateTime delivery_date { get; set; }
		public decimal quantity { get; set; }
		
	
	}
}
