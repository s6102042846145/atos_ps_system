﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.Contract_Master
{
    public class Bid
    {
        public string Contno { get; set; }
        public string Bidno { get; set; }
        public string Desc1 { get; set; }
        public string Desc2 { get; set; } 
        public string Desc3 { get; set; } 
        public string Amount1N { get; set; }
        public string Curr1N { get; set; }
        public string Exchange1N { get; set; }
        public string ALi { get; set; }
        public string ALiconfN { get; set; }
        public string AContract { get; set; }
        public string AComplete { get; set; }
        public string Remark { get; set; }
        public string Status { get; set; }
        public string Type { get; set; }
    }

}
