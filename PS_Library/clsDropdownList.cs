﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsDropdownList
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        DataTable dt;
        public DataTable drpDepartment()
        {
            dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select rowid,depart_code,depart_name from ps_m_department ";
                sql += " order by rowid desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable drpAssign()
        {
            dt = new DataTable();
            string sql = "";
            try
            {
                sql = @"select rowid,depart_code,depart_name from ps_m_department ";
                sql += " order by rowid desc";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
    }
}
