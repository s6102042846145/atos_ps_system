﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using PdfSharp.Pdf;
using PdfSharp.Pdf.IO;
using System.Collections;
using GemBox.Spreadsheet;
using System.Text.RegularExpressions;
using System.Linq;
using System.Drawing;
using OfficeOpenXml.Style;
using System.Configuration;
using System.Reflection;
using System.Security.Cryptography;

namespace PS_Library
{

    public class cExcel
    {
        public string FileName { get; set; }
        public string SheetName { get; set; }
        public string SheetCode { get; set; }

        public static string BidNO = "";


        public string connstr { get; set; }

        public string cmdstrFull { get; set; }
        public string cmdstr { get; set; }

        public string cmdvalue { get; set; }
        public string condi { get; set; }
        public string order { get; set; }

        clsMyTaskJob objJobDb = new clsMyTaskJob();
        List<ps_xls_template> listXls_temp = new List<ps_xls_template>();

        string Dept, Sect, BidNo, BidRev, ScheNo, JobNo, JobRev, partDesc, bidDesc, project, SchName, part, format_input = "";
        string key = "";
        String empID = "";
        public string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();
        static oraDBUtil oraDBUtil = new oraDBUtil();

        public string psDB = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();

        #region gen Drawing
        //Drawing
        public string genExcelDrawingBid(List<AddDocbid> list, String empID)
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(empID);
            string userName = emp.SNAME;
            bool result = false;

            // If you are a commercial business and have
            // purchased commercial licenses use the static property
            // LicenseContext of the ExcelPackage class:
            //ExcelPackage.LicenseContext = LicenseContext.Commercial;

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            int numRow = 3;
            var package = new ExcelPackage();
            //string user = empID;
            string FileName = empID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            string sheetname = "";
            // List<string> listBreak = new List<string>();
            int numSheet = 0;
            var workbook = package.Workbook;
            try
            {
                numSheet++;
                var worksheet = workbook.Worksheets.Add(sheetname + numSheet);
                //worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                worksheet.PrinterSettings.FitToPage = true;
                string strCell = "";

                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;

                if (list.Count > 0)
                {
                    // worksheet.Cells["A1:D1"].Value = "Bid No. "+ list[0];
                    worksheet.Cells["A1:D1"].Value = "Bid No. " + list[0].bid_no;
                }
                else
                {
                    worksheet.Cells["A1:D1"].Value = "";
                }
                worksheet.Cells["A1:D1"].Style.Font.Bold = true;
                worksheet.Cells["A1:D1"].Merge = true;
                worksheet.Cells["A1:D1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //header Colunm

                worksheet.Cells["A2"].Value = "NO.";
                worksheet.Cells["A2"].Style.Font.Bold = true;
                worksheet.Cells["A2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["A2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["A2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["B2"].Value = "Drawing Name";
                worksheet.Cells["B2"].Style.Font.Bold = true;
                worksheet.Cells["B2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["B2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["B2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C2"].Value = "Type";
                worksheet.Cells["C2"].Style.Font.Bold = true;
                worksheet.Cells["C2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["C2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D2"].Value = "Job No.";
                worksheet.Cells["D2"].Style.Font.Bold = true;
                worksheet.Cells["D2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //data
                if (list.Count > 0)
                {

                    foreach (var item in list)
                    {

                        strCell = "A" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = numRow - 2;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "B" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = item.doc_name;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "C" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = item.doc_desc;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "D" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = item.job_no;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;


                        numRow++;
                    }
                }
                FileName = FileName + "_BidDrawing" + ".xlsx";
                package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));


                //load the workbook
                //string strError = "";
                //ExportExcelToPdf(new FileInfo("C:\\Xls\\" + FileName+ "_BidDrawing" + ".xlsx"), "C:\\Xls", ref strError);


                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }



            return FileName;
        }
        public string genExcelDrawingJob(List<AddDoc> list, String empID)
        {

            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(empID);
            string userName = emp.SNAME;
            bool result = false;

            // If you are a commercial business and have
            // purchased commercial licenses use the static property
            // LicenseContext of the ExcelPackage class:
            //ExcelPackage.LicenseContext = LicenseContext.Commercial;

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            int numRow = 3;
            var package = new ExcelPackage();
            //string user = empID;
            string FileName = empID + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            string sheetname = "";
            // List<string> listBreak = new List<string>();
            int numSheet = 0;
            var workbook = package.Workbook;
            try
            {
                numSheet++;
                var worksheet = workbook.Worksheets.Add(sheetname + numSheet);
                // worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                worksheet.PrinterSettings.FitToPage = true;
                string strCell = "";

                worksheet.Column(1).Width = 10;
                worksheet.Column(2).Width = 20;
                worksheet.Column(3).Width = 20;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;


                if (list.Count > 0)
                {
                    // worksheet.Cells["A1:D1"].Value = "Bid No. "+ list[0];
                    worksheet.Cells["A1:D1"].Value = "Job No. " + list[0].job_no;
                }
                else
                {
                    worksheet.Cells["A1:D1"].Value = "";
                }

                worksheet.Cells["A1:D1"].Merge = true;
                //worksheet.Cells["A1:D1"].Value = "";
                worksheet.Cells["A1:D1"].Style.Font.Bold = true;
                worksheet.Cells["A1:D1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //header Colunm

                worksheet.Cells["A2"].Value = "NO.";
                worksheet.Cells["A2"].Style.Font.Bold = true;
                worksheet.Cells["A2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["A2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["A2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["B2"].Value = "Drawing Name";
                worksheet.Cells["B2"].Style.Font.Bold = true;
                worksheet.Cells["B2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["B2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["B2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C2"].Value = "Type";
                worksheet.Cells["C2"].Style.Font.Bold = true;
                worksheet.Cells["C2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["C2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D2"].Value = "Bid No.";
                worksheet.Cells["D2"].Style.Font.Bold = true;
                worksheet.Cells["D2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D2"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D2"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D2"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D2"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D2"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //data
                if (list.Count > 0)
                {

                    foreach (var item in list)
                    {

                        strCell = "A" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = numRow - 2;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "B" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = item.doc_name;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "C" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = item.doc_desc;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "D" + (numRow).ToString();
                        worksheet.Cells[strCell].Value = item.bid_no;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.WrapText = true;


                        numRow++;
                    }
                }

                FileName = FileName + "_JobDrawing" + ".xlsx";
                package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));

                ////load the workbook
                //string strError = "";
                //ExportExcelToPdf(new FileInfo("C:\\Xls\\" + FileName + "_JobDrawing" + ".xlsx"), "C:\\Xls", ref strError);


                result = true;
            }
            catch (Exception ex)
            {
                result = false;
            }



            return FileName;
        }
        #endregion

        #region genExcel Preview New

        private static string GenerateProductKey()
        {
            var pKey = "";
            /* while (pKey.Length < 16)
             {
                 pKey += new Random().Next(0, 10);
             }
             pKey = pKey.Substring(0, 16);*/


            var hmac = new HMACSHA256();
            pKey = Convert.ToBase64String(hmac.Key);
            return pKey;
        }
        public DataTable getBtnPaths(string bidNo, string SchName)
        {
            string sqlFull = "";
            string sql = @"select Convert(decimal(10,2),replace(replace(replace(stuff(part_name, 1, patindex('%[0-9]%', part_name)-1, ''),'_Breakdown','.1'),'_Supervisor','.2'),'_Option','.3')) as orderPart
                                ,replace(replace(replace(Stuff(part_name, PatIndex('%[0-9]%', part_name), 2, ''),'Breakdown',''),'Supervisor','') ,'Option','') as orderPrefix
                                ,part_name,part_description,part_remark,important 
                           from ps_t_mytask_part ";
            string condi = "Where bid_no = '" + bidNo + "'" + " and isnull(schedule_name,'') = '" + SchName + "'" + " and isnull(part_description, '') <> ''" + "  and step ='J' ";
            string order = " order by  orderPrefix,orderPart ";
            bool chk = false;

            //if (!chk)
            //{
            //    sqlFull = sql + order;
            //}
            //else
            //{
            sqlFull = sql + condi + order;
            //}
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            //DataView view = new DataView(dtList);
            //DataTable distinctValues = view.ToTable(true, "part_name", "part_description", "part_remark", "important");
            //LogHelper.Write(sqlFull);
            return dtList;
        }
        public DataTable getBtnPathsSupply(string bidNo, string SchName)
        {
            string sqlFull = "";
            string sql = @"select Convert(decimal(10,2),replace(replace(replace(stuff(part_name, 1, patindex('%[0-9]%', part_name)-1, ''),'_Breakdown','.1'),'_Supervisor','.2'),'_Option','.3')) as orderPart
                                ,replace(replace(replace(Stuff(part_name, PatIndex('%[0-9]%', part_name), 2, ''),'Breakdown',''),'Supervisor','') ,'Option','') as orderPrefix
                                ,part_name,part_description,part_remark,important 
                           from ps_t_mytask_part  ";
            string condi = "Where bid_no = '" + bidNo + "'" + " and isnull(schedule_name,'') = '" + SchName + "' and isnull(part_description,'') <> ''  and step ='B'";
            string order = " order by orderPrefix,orderPart";
            bool chk = false;

            //if (!chk)
            //{
            //    sqlFull = sql + order;
            //}
            //else
            //{
            sqlFull = sql + condi + order;
            //}
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            //DataView view = new DataView(dtList);
            //DataTable distinctValues = view.ToTable(true, "part_name", "part_description", "part_remark", "important");
            //LogHelper.Write(sqlFull);
            return dtList;
        }
        public DataTable getBtnPaths(string bidNo, string SchName, string pathName)
        {
            string sqlFull = "";
            string sql = @"select Convert(decimal(10,2),replace(replace(replace(stuff(part_name, 1, patindex('%[0-9]%', part_name)-1, ''),'_Breakdown','.1'),'_Supervisor','.2'),'_Option','.3')) as orderPart
                                ,replace(replace(replace(Stuff(part_name, PatIndex('%[0-9]%', part_name), 2, ''),'Breakdown',''),'Supervisor','') ,'Option','') as orderPrefix
                                ,part_name,part_description,part_remark,important 
                           from ps_t_mytask_part ";
            string condi = "Where bid_no = '" + bidNo + "'" + " and isnull(schedule_name,'') = '" + SchName + "'   and part_name ='" + pathName + "' and isnull(part_description,'') <> '' and step ='J' ";
            string order = " order by orderPrefix,orderPart ";
            bool chk = false;

            //if (!chk)
            //{
            //    sqlFull = sql + order;
            //}
            //else
            //{
            sqlFull = sql + condi + order;
            //}
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            //DataView view = new DataView(dtList);
            //DataTable distinctValues = view.ToTable(true, "part_name","part_description","part_remark","important");
            //LogHelper.Write(sqlFull);
            return dtList;
        }

        public DataTable getGVSelPart(string dept, string sect, string bidNo, string schName, string part, string jobNo, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (bidNo != "Bid Supply")
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                                    equip_code,equip_add_desc
                                    ,format(equip_qty,'#,##0.00') as equip_qty
                                    ,created_by,created_datetime,updated_by,updated_datetime,equip_incoterm,factory_test,manual_test,routine_test,equip_unit
                                    ,format(equip_bid_qty,'#,##0.00') as equip_bid_qty
                                    ,format(supply_equip_unit_price,'#,##0.00') as supply_equip_unit_price
                                    ,format(supply_equip_amonut,'#,##0.00') as supply_equip_amonut
                                    ,format(local_exwork_unit_price,'#,##0.00') as local_exwork_unit_price
                                    ,format(local_exwork_amonut,'#,##0.00') as local_exwork_amonut
                                    ,format(local_tran_unit_price,'#,##0.00') as local_tran_unit_price
                                    ,format(local_tran_amonut,'#,##0.00') as local_tran_amonut
                                    ,currency,equip_standard_drawing,equip_mas_grp,equip_full_desc + equip_add_desc as equip_full_desc,legend,equip_unit as unit
                                    ,'' as [type],'' as [view],'' as equip_code2,'1' as edit ,path_code as 'Part',equip_item_no 
                                    ,STUFF((select DISTINCT '|' + doc_approve.doc_name 
						                    from ps_m_doc_approve as doc_approve
						                    where doc_approve.doc_nodeid in(select equip_doc.doc_nodeid from ps_m_equip_doc as equip_doc where equip_doc.equip_code =[dbo].[ps_t_mytask_equip].equip_code) 
						                    and doc_approve.doc_status='Active'
						                    FOR XML PATH('')),1,1,'') as eq_design
                            from  ps_t_mytask_equip  
                            where bid_no = '" + bidNo + "' and schedule_name ='" + schName + "'  and path_code='" + part + "' --and job_no='" + jobNo + "' and job_revision='" + rev + @"'
                            Order By equip_item_no";
                }
                else
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                                    equip_code,equip_add_desc
                                    ,format(equip_qty,'#,##0.00') as equip_qty
                                    ,created_by,created_datetime,updated_by,updated_datetime,equip_incoterm,factory_test,manual_test,routine_test,equip_unit
                                    ,format(equip_bid_qty,'#,##0.00') as equip_bid_qty
                                    ,format(supply_equip_unit_price,'#,##0.00') as supply_equip_unit_price
                                    ,format(supply_equip_amonut,'#,##0.00') as supply_equip_amonut
                                    ,format(local_exwork_unit_price,'#,##0.00') as local_exwork_unit_price
                                    ,format(local_exwork_amonut,'#,##0.00') as local_exwork_amonut
                                    ,format(local_tran_unit_price,'#,##0.00') as local_tran_unit_price
                                    ,format(local_tran_amonut,'#,##0.00') as local_tran_amonut
                                    ,currency,equip_standard_drawing,equip_mas_grp,equip_full_desc + equip_add_desc as equip_full_desc,legend,equip_unit as unit
                                    ,'' as [type],'' as [view],'' as equip_code2,'1' as edit ,path_code as 'Part',equip_item_no 
                                    ,STUFF((select DISTINCT '|' + doc_approve.doc_name 
						                    from ps_m_doc_approve as doc_approve
						                    where doc_approve.doc_nodeid in(select equip_doc.doc_nodeid from ps_m_equip_doc as equip_doc where equip_doc.equip_code =[dbo].[ps_t_mytask_equip].equip_code) 
						                    and doc_approve.doc_status='Active'
						                    FOR XML PATH('')),1,1,'') as eq_design
                            from  ps_t_mytask_equip 
                            where  schedule_name ='" + schName + "'  and path_code='" + part + "' --and job_no='" + jobNo + "' and job_revision='" + rev + @"'
                            Order By equip_item_no_b";
                }
                //LogHelper.Write(sql);
                dt = zdbUtil.ExecSql_DataTable(sql, psDB);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        public string genExcel(string FileName, string Dept, string Sect, string BidNo, string ScheNo, string JobNo, string JobRev, string schName, string BidRev, string format_input)
        {
            try
            {
                //string BidRev = "0";
                //Dept = "กวศ–ส.";
                //Sect = "";
                //BidNo = "TS12-S-02";
                //ScheNo = "1";
                //JobNo = "TS12-11-S06";
                //rev = "1";
                //Dept = "กวสส-ส.";
                //Sect = "";
                //BidNo = "TS12-L-02";
                //ScheNo = "2";
                //JobNo = "TS12-07-L01";

                //schName = schName;
                this.key = GenerateProductKey();
                DataTable dtPart = new DataTable();
                if (format_input == "E")
                {
                    dtPart = getBtnPaths(BidNo, schName);
                }
                if (format_input == "S")
                {
                    dtPart = getBtnPathsSupply(BidNo, schName);
                }

                //DataTable dtJob = objJobDb.GetJobRelate(JobNo, BidNo);
                DataTable dtBid = objJobDb.getProject_no(BidNo.Substring(0, 4));
                // FileName = BidNo;
                this.format_input = format_input;
                if (format_input == "E")
                {
                    if (dtPart.Rows.Count > 0)
                    {
                        //if (dtJob.Rows.Count > 0)
                        //{
                        //this.partDesc = dtPart.Rows[0]["part_description"].ToString();

                        //Ton
                        //ต้องเปลั่ยนดึงมาจากหน้า Bid Schedule column "Scope of Work"
                        this.bidDesc = "SUPPLY AND CONSTRUCTION OF " + schName;
                        //}
                        if (dtBid.Rows.Count > 0)
                        {
                            this.project = dtBid.Rows[0]["project_name_en"].ToString();
                        }
                        DataTable dt_eq = new DataTable();
                        //List<DataTable> listEq = new List<DataTable>();
                        this.Dept = Dept;
                        this.Sect = Sect;
                        this.BidNo = BidNo;
                        this.ScheNo = ScheNo;
                        this.JobNo = JobNo;
                        this.JobRev = JobRev;
                        this.SchName = schName;
                        this.BidRev = BidRev;
                        //foreach (DataRow dr in dtPart.Rows)
                        //{
                        //    dt_eq = objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);
                        //    /// listEq.Add(dt_eq);
                        //}

                        FileName = writeExcel(FileName, dtPart);
                    }
                }
                if (format_input == "S")
                {
                    if (dtPart.Rows.Count > 0)
                    {
                        //if (dtJob.Rows.Count > 0)
                        //{
                        //this.partDesc = dtPart.Rows[0]["part_description"].ToString();
                        this.bidDesc = "SUPPLY OF " + schName;
                        //}
                        if (dtBid.Rows.Count > 0)
                        {
                            this.project = dtBid.Rows[0]["project_name_en"].ToString();
                        }
                        DataTable dt_eq = new DataTable();
                        //List<DataTable> listEq = new List<DataTable>();
                        this.Dept = Dept;
                        this.Sect = Sect;
                        this.BidNo = BidNo;
                        this.ScheNo = ScheNo;
                        this.JobNo = JobNo;
                        this.JobRev = JobRev;
                        this.SchName = schName;
                        this.BidRev = BidRev;
                        //foreach (DataRow dr in dtPart.Rows)
                        //{
                        //    dt_eq = objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);
                        //    /// listEq.Add(dt_eq);
                        //}

                        FileName = writeExcel(FileName, dtPart);

                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return FileName;
        }
        public List<DataRow> createDataForPart(DataTable dt)
        {
            DataTable dtReult = new DataTable();
            DataTable dt_eq = new DataTable();
            List<DataRow> listDataPart = new List<DataRow>();
            foreach (DataRow dr in dt.Rows)
            {
                //dt_eq = objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);
                //foreach (DataRow drr in dt_eq.Rows)
                //{

                //}
                listDataPart.Add(dr);
                /// listEq.Add(dt_eq);
            }
            // var a = listDataPart;
            return listDataPart;
        }
        public string writeExcel(string FileName, DataTable dtPart)
        {
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            DataTable dt_eq = new DataTable();
            var package = new ExcelPackage();
            string format = "";
            var workbook = package.Workbook;
            DataTable dttt = new DataTable();

            try
            {
                string type = "Substation";
                string strSQL = @"SELECT bid_no.bid_no,bid_no.bid_revision
		                                , [tx_bid_type].bid_type, [tx_bid_type].is_line, [tx_bid_type].is_sub, [tx_bid_type].is_supply
                                  FROM [dbo].[tx_bid_type] 
		                                inner join bid_no on [tx_bid_type].bid_type = bid_no.bid_type
                                  WHERE bid_no.bid_no='" + this.BidNo +"' and bid_no.bid_revision = " + this.BidRev;
                DataTable dtCheck_BidType = zdbUtil.ExecSql_DataTable(strSQL, psDB);
                if (dtCheck_BidType.Rows.Count > 0)
                { 
                    if (dtCheck_BidType.Rows[0]["is_line"].ToString() == "1")
                        type = "Line";
                    else if (dtCheck_BidType.Rows[0]["is_sub"].ToString() == "1")
                        type = "Substation";
                }

                //DataRow[] resultsSub = dtPart.Select("SUBSTRING(part_name,1,2) like '%AB%' or  SUBSTRING(part_name ,1,1) like '%D%' or SUBSTRING(part_name,1,2) like '%_D%' or  SUBSTRING(part_name ,1,1) like '%C%' and LEN(part_name) > 1 ");
                //if (resultsSub.Count() > 0)
                //{
                //    type = "Substation";
                //}

                //DataRow[] resultsLine = dtPart.Select("( SUBSTRING(part_name,1,1) like '%A%' or  SUBSTRING(part_name ,1,1) like '%I%' or SUBSTRING(part_name,1,1) like '%O%' or " +
                //    "SUBSTRING(part_name ,1,1) like '%B%' or SUBSTRING(part_name,1,1) like '%C%'  or SUBSTRING(part_name,1,1) like '%D%' or SUBSTRING(part_name,1,1) like '%E%'  or " +
                //  "SUBSTRING(part_name,1,1) like '%F%' or SUBSTRING(part_name,1,1) like '%G%' or SUBSTRING(part_name,1,1) like '%H%' or SUBSTRING(part_name,1,1) like '%J%' or " +
                //  "SUBSTRING(part_name,1,1) like '%K%' and LEN(part_name) = 1 or " +
                //  "SUBSTRING(part_name,1,1) like '%N%' or " +
                //  "SUBSTRING(part_name,1,1) like '%Z%' " +
                //  " )and  LEN(part_name) = 1 ");
                //if (resultsLine.Count() > 0)
                //{
                //    type = "Line";
                //}

                if (dtPart.Rows.Count > 0)
                {
                    var worksheetSumAll = workbook.Worksheets.Add("SumAll");

                    worksheetSumAll.PrinterSettings.Orientation = eOrientation.Portrait;
                    worksheetSumAll.PrinterSettings.PaperSize = ePaperSize.A4;
                    worksheetSumAll.PrinterSettings.FitToPage = true;
                    //worksheetSumAll.HeaderFooter.EvenFooter.RightAlignedText = "&[Page]/&[Pages]  &[File]";
                    if (this.format_input == "E")
                    {
                        if (type == "Substation")
                            setSheetSumAll_SubStation(worksheetSumAll, dtPart, type);
                        else if (type == "Line")
                            setSheetSumAll_Line(worksheetSumAll, dtPart, type);
                    }
                    if (this.format_input == "S")
                    {
                        setSheetSumAll_Supply(worksheetSumAll, dtPart, type);
                    }
                }

                ////substation
                if (type == "Substation")
                {
                    try
                    {
                        string NamePart = "";
                        DataRow[] resultsPartAB = dtPart.Select("orderPrefix ='AB'", "orderPrefix asc,orderPart asc");
                        DataRow[] resultsPartD = dtPart.Select("orderPrefix ='D'", "orderPrefix asc,orderPart asc");
                        DataRow[] resultsPart_D = dtPart.Select("orderPrefix ='_D'", "orderPrefix asc,orderPart asc");
                        DataRow[] resultsPartC = dtPart.Select("orderPrefix ='C'", "orderPrefix asc,orderPart asc");

                        //DataRow[] resultsPartAB = dtPart.Select("SUBSTRING(part_name,1,2) like '%AB%'and  LEN(part_name) > 1 ");
                        //DataRow[] resultsPartD = dtPart.Select("SUBSTRING(part_name,1,1) like '%D%' and LEN(part_name) > 1 ");
                        //DataRow[] resultsPart_D = dtPart.Select("SUBSTRING(part_name,1,2) like '%_D%' and  LEN(part_name) > 1 ");
                        //DataRow[] resultsPartC = dtPart.Select("SUBSTRING(part_name,1,1) like '%C%' and  LEN(part_name) > 1 ");

                        DataTable dt = new DataTable();
                        if (resultsPartAB.Count() > 0)
                        {
                            NamePart = "AB";
                            var worksheetSumAB = workbook.Worksheets.Add("Part " + NamePart);
                            worksheetSumAB.PrinterSettings.Orientation = eOrientation.Portrait;
                            worksheetSumAB.PrinterSettings.PaperSize = ePaperSize.A4;
                            worksheetSumAB.PrinterSettings.FitToPage = true;
                            worksheetSumAB.HeaderFooter.EvenFooter.RightAlignedText = "&[Page]/&[Pages]  &[File]";
                            dt = resultsPartAB.CopyToDataTable();

                            setSheetPartSub1(worksheetSumAB, createDataForPart(dt), dtPart, NamePart);
                        }
                        if (resultsPartC.Count() > 0)
                        {
                            NamePart = "C";
                            var worksheetSumC = workbook.Worksheets.Add("Part " + NamePart);
                            worksheetSumC.PrinterSettings.Orientation = eOrientation.Portrait;
                            worksheetSumC.PrinterSettings.PaperSize = ePaperSize.A4;
                            worksheetSumC.PrinterSettings.FitToPage = true;
                            worksheetSumC.HeaderFooter.EvenFooter.RightAlignedText = "&[Page]/&[Pages]  &[File]";
                            dt = resultsPartC.CopyToDataTable();
                            setSheetPartSub2(worksheetSumC, createDataForPart(dt), dtPart, NamePart);
                        }
                        if (resultsPartD.Count() > 0)
                        {
                            NamePart = "D";
                            var worksheetSumD = workbook.Worksheets.Add("Part " + NamePart);
                            worksheetSumD.PrinterSettings.Orientation = eOrientation.Portrait;
                            worksheetSumD.PrinterSettings.PaperSize = ePaperSize.A4;
                            worksheetSumD.PrinterSettings.FitToPage = true;
                            worksheetSumD.HeaderFooter.EvenFooter.RightAlignedText = "&[Page]/&[Pages]  &[File]";
                            dt = resultsPartD.CopyToDataTable();
                            setSheetPartSub1(worksheetSumD, createDataForPart(dt), dtPart, NamePart);
                        }
                        if (resultsPart_D.Count() > 0)
                        {
                            NamePart = "_D";
                            var worksheetSum_D = workbook.Worksheets.Add("Part " + NamePart);
                            worksheetSum_D.PrinterSettings.Orientation = eOrientation.Portrait;
                            worksheetSum_D.PrinterSettings.PaperSize = ePaperSize.A4;
                            worksheetSum_D.PrinterSettings.FitToPage = true;
                            worksheetSum_D.HeaderFooter.EvenFooter.RightAlignedText = "&[Page]/&[Pages]  &[File]";
                            dt = resultsPart_D.CopyToDataTable();
                            setSheetPartSub1(worksheetSum_D, createDataForPart(dt), dtPart, NamePart);
                        }
                    }
                    catch (Exception ex)
                    {
                        LogHelper.WriteEx(ex);
                    }
                }

                //numSheet++;
                DataView dvSort = dtPart.DefaultView;
                dvSort.Sort = "orderPrefix asc,orderPart asc";
                DataTable dtPart_Sort = dvSort.ToTable();
                for (int i = 0; i < dtPart_Sort.Rows.Count; i++)
                {
                    DataRow dr = dtPart_Sort.Rows[i];
                    string part = dr["part_name"] == null ? "" : dr["part_name"].ToString();
                    string part_description = dr["part_description"] == null ? "" : dr["part_description"].ToString();
                    string part_remark = dr["part_remark"] == null ? "" : dr["part_remark"].ToString();
                    string important = dr["important"] == null ? "" : dr["important"].ToString();

                    LogHelper.Write("Part : " + part);

                    var worksheet = workbook.Worksheets.Add(part);
                    worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                    worksheet.PrinterSettings.PaperSize = ePaperSize.A4;
                    worksheet.PrinterSettings.FitToPage = true;
                    //worksheet.HeaderFooter.EvenFooter.RightAlignedText = "&[Page]/&[Pages]  &[File]";
                    string strCell = "";

                    dt_eq = getGVSelPart(Dept, Sect, BidNo, SchName, dr["part_name"].ToString(), JobNo, JobRev);
                    //substation
                    if (part.Length > 1 && (part.Substring(0, 2).ToUpper().Equals("AB")
                        || part.Substring(0, 1).ToUpper().Equals("D") || part.Substring(0, 2).ToUpper().Equals("_D")))
                    {
                        format = "Substation1";
                    }
                    if (part.Length > 1 && part.Substring(0, 1).ToUpper().Equals("C"))
                    {
                        format = "Substation2";
                    }

                    //line
                    if (part.Length == 1 && part.ToUpper().Equals("A"))
                    {
                        format = "Line1";
                    }

                    if (part.Length == 1 && (part.ToUpper().Equals("B")
                        || part.ToUpper().Equals("C") || part.ToUpper().Equals("D")
                        || part.ToUpper().Equals("E") || part.ToUpper().Equals("F")
                        || part.ToUpper().Equals("G") || part.ToUpper().Equals("H")
                        || part.ToUpper().Equals("J") || part.ToUpper().Equals("K")
                        || part.ToUpper().Equals("I") || part.ToUpper().Equals("O")))
                    {
                        format = "Line2";
                    }

                    if (part.Length == 1 && part.ToUpper().Equals("N"))
                    {
                        format = "Line3";
                    }
                    if (part.Length == 1 && part.ToUpper().Equals("Z"))
                    {
                        format = "Line4";
                    }

                    //Supply
                    //if (part.Length > 3 && (part.Substring(0, 3)).ToUpper().Equals("SCH"))
                    //{
                    //    format = "Supply";
                    //}
                    if (this.format_input == "S")
                    {
                        format = "Supply";
                    }



                    //Bind data
                    if (format == "Substation1")
                    {
                        worksheet = setSheetChildSubstationFormat1(worksheet, dt_eq, part, part_description, part_remark, important);
                    }
                    else if (format == "Substation2")
                    {
                        worksheet = setSheetChildSubstationFormat2(worksheet, dt_eq, part, part_description, part_remark, important);
                    }
                    else if (format == "Line1")
                    {
                        worksheet = setSheetChildLineFormat1(worksheet, dt_eq, part, part_description, part_remark, important);

                    }
                    else if (format == "Line2")
                    {
                        if (part != "H")
                        {
                            worksheet = setSheetChildLineFormat2(worksheet, dt_eq, part, part_description, part_remark, important);
                        }
                        else
                        {
                            worksheet = setSheetChildSubstationFormat1(worksheet, dt_eq, part, part_description, part_remark, important);
                        }


                    }
                    else if (format == "Line3")
                    {
                        worksheet = setSheetChildLineFormat3(worksheet, dt_eq, part, part_description, part_remark, important);
                    }
                    else if (format == "Line4")
                    {
                        worksheet = setSheetChildLineFormat4(worksheet, dt_eq, part, part_description, part_remark, important);
                    }
                    else if (format == "Supply")
                    {

                        worksheet = setSheetChildSupplyFormat1(worksheet, dt_eq, part, part_description, part_remark, important);
                    }

                }
                // package.Workbook.Calculate();
                FileName = FileName + ".xlsx";
                package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));

                delete_ps_xls_template();
                insertps_xls_template();
                listXls_temp.Clear();
                // uploadData(1, new FileInfo("C:\\Xls\\" + FileName), empID);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return FileName;
        }

        //set data
        public string getPartName(string strPartcode, string strPartType)
        {
            string strPartName = "";
            try
            {
                string strSQL = @"SELECT [rowid]
                                      ,[parts_code]
                                      ,[parts_name]
                                      ,[parts_name_ln]
                                      ,[parts_name_ss]
                                      ,[parts_name_sp]
                                      ,[isdelete]
                                      ,[created_by]
                                      ,[created_datetime]
                                      ,[updated_by]
                                      ,[updated_datetime]
                                  FROM [dbo].[ps_m_parts]
                                  WHERE parts_code = '" + strPartcode + "'";
                DataTable dt = zdbUtil.ExecSql_DataTable(strSQL, psDB);
                if (dt.Rows.Count > 0)
                {
                    strPartName = dt.Rows[0]["parts_name_" + strPartType].ToString();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return strPartName;
        }

        //set data
        public string getScopeofWork(string strBidNo, string strBidRev, string strScheduleName)
        {
            string strSOW = "";
            try
            {
                string strSQL = @"SELECT [bid_no]
                                      ,[bid_revision]
                                      ,[job_no]
                                      ,[job_revision]
                                      ,[schedule_no]
                                      ,[schedule_name]
                                      ,[sow]
                                      ,[line_length]
                                      ,[pq]
                                      ,[fe]
                                      ,[lc]
                                      ,[construction]
                                      ,[total]
                                      ,[created_by]
                                      ,[created_datetime]
                                      ,[updated_by]
                                      ,[updated_datetime]
                                  FROM [dbo].[ps_m_bid_schedule]
                                  WHERE bid_no = '" + strBidNo + "' and bid_revision = " + strBidRev + " and schedule_name = '" + strScheduleName + "'";
                DataTable dt = zdbUtil.ExecSql_DataTable(strSQL, psDB);
                if (dt.Rows.Count > 0)
                {
                    strSOW = dt.Rows[0]["sow"].ToString();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return strSOW;
        }
        public string getLineLength(string strBidNo, string strBidRev, string strScheduleName)
        {
            string strLineLength = "";
            try
            {
                string strSQL = @"SELECT [bid_no]
                                      ,[bid_revision]
                                      ,[job_no]
                                      ,[job_revision]
                                      ,[schedule_no]
                                      ,[schedule_name]
                                      ,[sow]
                                      ,[line_length]
                                      ,[pq]
                                      ,[fe]
                                      ,[lc]
                                      ,[construction]
                                      ,[total]
                                      ,[created_by]
                                      ,[created_datetime]
                                      ,[updated_by]
                                      ,[updated_datetime]
                                  FROM [dbo].[ps_m_bid_schedule]
                                  WHERE bid_no = '" + strBidNo + "' and bid_revision = " + strBidRev + " and schedule_name = '" + strScheduleName + "'";
                DataTable dt = zdbUtil.ExecSql_DataTable(strSQL, psDB);
                if (dt.Rows.Count > 0)
                {
                    strLineLength = dt.Rows[0]["line_length"].ToString();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return strLineLength;
        }
        public DataTable getCurrency()
        {
            DataTable dt = new DataTable();
            try
            {
                string strSQL = @"SELECT distinct
                                  CURRENCY_CODE
                                FROM P_M_SAP_PR 
                                ORDER BY CURRENCY_CODE ";
                dt = oraDBUtil._getDT(strSQL, commonDB);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }

        #region " Set Sheet SumALL "
        public OfficeOpenXml.ExcelWorksheet setSheetSumAll_SubStation(OfficeOpenXml.ExcelWorksheet wk, DataTable dtPart, string type)
        {
            var worksheet = wk;
            try
            {
                worksheet.Protection.IsProtected = true;
                char cell = 'A';
                decimal totlaAmountCFR = 0;
                string strCell = "";
                int numRow = 11;
                bool havedata = false;

                //sum sheet
                //set Width 
                worksheet.Column(1).Width = 40;
                worksheet.Column(3).Width = 17;
                worksheet.Column(4).Width = 17;
                worksheet.Column(5).Width = 17;
                worksheet.Column(6).Width = 17;
                worksheet.Column(7).Width = 17;

                //set wrap 
                //wrap text in the cells
                //worksheet.Column(5).Style.WrapText = true;
                //worksheet.Column(6).Style.WrapText = true;
                //worksheet.Column(7).Style.WrapText = true;


                //path

                //header

                //childSheet.header_l1_bidno = BidNo;
                //childSheet.header_l2_PartName = partDesc;
                //childSheet.header_l3_biddesc = bidDesc;
                //childSheet.header_l4_project = project;
                XLSPS_SumSheet sumSheet = new XLSPS_SumSheet();
                //BidNO = listSum[0].bidno == null ? "" : listSum[0].bidno.ToString();
                sumSheet.header_l1_bidno = BidNo;
                sumSheet.header_l2_ScheduleName = SchName;
                sumSheet.header_l3_biddesc = bidDesc;
                sumSheet.header_l4_project = project;

                sumSheet.column_1_l1 = "Description";
                sumSheet.column_2_l1 = "Currency";
                sumSheet.column_3_l1 = "Supply of Equipment";
                sumSheet.column_3_l2_c1 = "Foreign Supply";
                sumSheet.column_3_l2_c2 = "Local Supply";
                sumSheet.column_3_l3_c1 = "CIF Thai Port";
                sumSheet.column_3_l3_c2 = "Ex-works Price";
                sumSheet.column_3_l4_c2 = "( excluding VAT )";
                sumSheet.column_3_l5_c2 = "Baht";
                sumSheet.column_3_l6_c1 = "Amount";
                sumSheet.column_3_l6_c2 = "Amount";
                sumSheet.column_4_l1 = "Local Currency";
                sumSheet.column_4_l4 = "( excluding VAT )";
                sumSheet.column_4_l5 = "Baht";
                sumSheet.column_4_l6 = "Amount";
                sumSheet.column_5_l1 = "Local Transportation";
                sumSheet.column_5_l4 = "( excluding VAT )";
                sumSheet.column_5_l5 = "Baht";
                sumSheet.column_5_l6 = "Amount";
                sumSheet.column_6_l1 = "Local Transportation,  Construction and  Installation";
                sumSheet.column_6_l4 = "( excluding VAT )";
                sumSheet.column_6_l5 = "Baht";
                sumSheet.column_6_l6 = "Amount";

                sumSheet.footer_1_l1 = "TOTAL PRICE";
                sumSheet.footer_3_l1_c1 = "Baht";
                sumSheet.footer_3_l1_c2 = "Baht";
                sumSheet.footer_4_l1 = "Baht";
                sumSheet.footer_5_l1 = "Baht";
                sumSheet.footer_6_l1 = "Baht";


                worksheet.Cells["A1:G1"].Merge = true;
                worksheet.Cells["A1:G1"].Value = ("INVITATION TO BID NO. " + sumSheet.header_l1_bidno).ToUpper();
                worksheet.Cells["A1:G1"].Style.Font.Bold = true;
                worksheet.Cells["A1:G1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A2:G2"].Merge = true;
                worksheet.Cells["A2:G2"].Value = ("Schedule " + ScheNo + ": " + sumSheet.header_l2_ScheduleName).ToUpper();
                worksheet.Cells["A2:G2"].Style.Font.Bold = true;
                worksheet.Cells["A2:G2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A3:G3"].Merge = true;
                worksheet.Cells["A3:G3"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// (sumSheet.header_l3_biddesc).ToUpper();
                worksheet.Cells["A3:G3"].Style.Font.Bold = true;
                worksheet.Cells["A3:G3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A4:G4"].Merge = true;
                worksheet.Cells["A4:G4"].Value = (sumSheet.header_l4_project).ToUpper();
                worksheet.Cells["A4:G4"].Style.Font.Bold = true;
                worksheet.Cells["A4:G4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


                //header Colunm

                worksheet.Cells["A5:A10"].Merge = true;
                worksheet.Cells["A5:A10"].Value = sumSheet.column_1_l1;
                //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
                worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["B5:B10"].Merge = true;
                worksheet.Cells["B5:B10"].Value = sumSheet.column_2_l1;
                //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
                worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C5:D5"].Merge = true;
                worksheet.Cells["C5:D5"].Value = sumSheet.column_3_l1;
                worksheet.Cells["C5:D5"].Style.Font.Bold = true;
                worksheet.Cells["C5:D5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["C5:D5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:D5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:D5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:D5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["C6"].Value = sumSheet.column_3_l2_c1;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["C6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["C6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["D6"].Merge = true;
                worksheet.Cells["D6"].Value = sumSheet.column_3_l2_c2;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["D6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C7:C9"].Merge = true;
                worksheet.Cells["C7:C9"].Value = sumSheet.column_3_l3_c1;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["C7:C9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C7:C9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["C7:C9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                // worksheet.Cells["D7"].Merge = true;
                worksheet.Cells["D7"].Value = sumSheet.column_3_l3_c2;
                //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
                worksheet.Cells["D7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;



                //worksheet.Cells["D8"].Merge = true;
                worksheet.Cells["D8"].Value = sumSheet.column_3_l4_c2;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["D8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //worksheet.Cells["D9"].Merge = true;
                worksheet.Cells["D9"].Value = sumSheet.column_3_l5_c2;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["D9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["D9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //worksheet.Cells["C10"].Merge = true;
                worksheet.Cells["C10"].Value = sumSheet.column_3_l6_c1;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                //worksheet.Cells["D10"].Merge = true;
                worksheet.Cells["D10"].Value = sumSheet.column_3_l6_c2;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E5:E7"].Merge = true;
                worksheet.Cells["E5:E7"].Value = sumSheet.column_4_l1;
                worksheet.Cells["E5:E7"].Style.Font.Bold = true;
                worksheet.Cells["E5:E7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E5:E7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["E5:E7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E5:E7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["E5:E7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E5:E7"].Style.WrapText = true;

                //worksheet.Cells["D8"].Merge = true;
                worksheet.Cells["E8"].Value = sumSheet.column_4_l4;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["E8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //worksheet.Cells["D9"].Merge = true;
                worksheet.Cells["E9"].Value = sumSheet.column_4_l5;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["E9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["E9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //worksheet.Cells["C10"].Merge = true;
                worksheet.Cells["E10"].Value = sumSheet.column_4_l6;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["E10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F5:F7"].Merge = true;
                worksheet.Cells["F5:F7"].Value = sumSheet.column_5_l1;
                worksheet.Cells["F5:F7"].Style.Font.Bold = true;
                worksheet.Cells["F5:F7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F5:F7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["F5:F7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5:F7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["F5:F7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5:F7"].Style.WrapText = true;

                //worksheet.Cells["D8"].Merge = true;
                worksheet.Cells["F8"].Value = sumSheet.column_5_l4;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["F8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //worksheet.Cells["D9"].Merge = true;
                worksheet.Cells["F9"].Value = sumSheet.column_5_l5;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["F9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["F9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                //worksheet.Cells["C10"].Merge = true;
                worksheet.Cells["F10"].Value = sumSheet.column_5_l6;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["F10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["G5:H7"].Merge = true;
                worksheet.Cells["G5:H7"].Value = sumSheet.column_6_l1;
                worksheet.Cells["G5:H7"].Style.Font.Bold = true;
                worksheet.Cells["G5:H7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G5:H7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["G5:H7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:H7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:H7"].Style.WrapText = true;

                worksheet.Cells["G8:H8"].Merge = true;
                worksheet.Cells["G8:H8"].Value = sumSheet.column_6_l4;
                worksheet.Cells["G8:H8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G8:H8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["G9:H9"].Merge = true;
                worksheet.Cells["G9:H9"].Value = sumSheet.column_6_l5;
                worksheet.Cells["G9:H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G9:H9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["G9:H9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["G10:H10"].Merge = true;
                worksheet.Cells["G10:H10"].Value = sumSheet.column_6_l6;
                worksheet.Cells["G10:H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G10:H10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["G10:H10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G10:H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G10:H10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                //data
                decimal ForeignAmount = 0;
                decimal LocalAmount = 0;
                decimal LocalCurAmount = 0;
                decimal LocalTranAmount = 0;
                decimal LocalTranConAmount = 0;
                if (dtPart.Rows.Count > 0)
                {
                    DataRow[] resultsPartAB = new DataRow[0];
                    DataRow[] resultsPartD = new DataRow[0];
                    DataRow[] resultsPart_D = new DataRow[0];
                    DataRow[] resultsPartC = new DataRow[0];
                    string curreny = "";
                    string part_name = "";
                    int numRowSumCount = 0;



                    //if (resultsPartAB.Count() > 0)
                    //{
                    //    numRowCount++;
                    //}
                    //if (resultsPartD.Count() > 0)
                    //{
                    //    numRowCount++;
                    //}
                    //if (resultsPart_D.Count() > 0)
                    //{
                    //    numRowCount++;
                    //}
                    //if (resultsPartC.Count() > 0)
                    //{
                    //    numRowCount++;
                    //}
                    List<string> listPart = new List<string>();


                    foreach (DataRow dr in dtPart.Rows)
                    {

                        if (type == "Substation")
                        {

                            // string part_name = dr["part_name"].ToString();


                            DataTable dt_eq = new DataTable();// objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);

                            dt_eq = getGVSelPart(Dept, Sect, BidNo, SchName, dr["part_name"].ToString(), JobNo, JobRev);

                            if (dt_eq.Rows.Count > 0)
                            {
                                // part = dt_eq.Rows[0][""]
                                if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 2).Equals("AB"))
                                {
                                    part_name = "AB";
                                    listPart.Add(part_name);

                                }
                                if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("D"))
                                {
                                    part_name = "D";
                                    listPart.Add(part_name);

                                }
                                if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 2).Equals("_D"))
                                {
                                    part_name = "_D";
                                    listPart.Add(part_name);

                                }
                                if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("C"))
                                {
                                    part_name = "C";
                                    listPart.Add(part_name);

                                }
                            }
                        }
                    }

                    resultsPartAB = dtPart.Select("SUBSTRING(part_name,1,2) like '%AB%'and  LEN(part_name) > 1 ");
                    resultsPartD = dtPart.Select("SUBSTRING(part_name,1,1) like '%D%' and LEN(part_name) > 1 ");
                    resultsPart_D = dtPart.Select("SUBSTRING(part_name,1,2) like '%_d%' and  LEN(part_name) > 1 ");
                    resultsPartC = dtPart.Select("SUBSTRING(part_name,1,1) like '%C%' and  LEN(part_name) > 1 ");

                    var distinctItems = listPart.Distinct().ToList();
                    foreach (var item in distinctItems)
                    {
                        havedata = true;

                        if (item == "AB")
                        {
                            numRowSumCount = resultsPartAB.Count();
                        }
                        if (item == "D")
                        {
                            numRowSumCount = resultsPartD.Count();
                        }
                        if (item == "_D")
                        {
                            numRowSumCount = resultsPart_D.Count();
                        }
                        if (item == "C")
                        {
                            numRowSumCount = resultsPartC.Count();
                        }



                        for (int i = 0; i <= 4; i++)
                        {
                            if (i == 0)
                            {
                                strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 4).ToString();
                                worksheet.Cells[strCell].Merge = true;
                                worksheet.Cells[strCell].Value = "PART " + ScheNo + "" + item + " :" + this.getPartName(item, "ss");
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.WrapText = true;
                            }

                            strCell = "B" + (numRow).ToString();
                            // worksheet.Cells[strCell].Value = Part.Currency;
                            switch (i)
                            {
                                case 0:
                                    worksheet.Cells[strCell].Value = "USD";
                                    break;
                                case 1:
                                    worksheet.Cells[strCell].Value = "EUR";
                                    break;
                                case 2:
                                    worksheet.Cells[strCell].Value = "JPY";
                                    break;
                                case 3:
                                    worksheet.Cells[strCell].Value = "CNY";
                                    break;
                                case 4:
                                    worksheet.Cells[strCell].Value = "THB";
                                    break;
                            }
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            if (i == 4)
                            {
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "C" + (numRow).ToString();
                            if (i != 4)
                            {
                                worksheet.Cells[strCell].Formula = "='" + "Part " + item + "'" + "!C" + ((numRowSumCount * 5) + 11 + i);
                            }
                            //worksheet.Cells[strCell].Value = Part.ForeignAmount == "0" ? "" : Part.ForeignAmount;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            if (i == 4)
                            {
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "D" + (numRow).ToString();
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            if (i == 4)
                            {
                                worksheet.Cells[strCell].Formula = "='" + "Part " + item + "'" + "!D" + ((numRowSumCount * 5) + 12);


                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "E" + (numRow).ToString();
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            if (i == 4)
                            {
                                if (item == "C")
                                {
                                    worksheet.Cells[strCell].Formula = "='" + "Part " + item + "'" + "!B" + ((numRowSumCount * 5) + 12);
                                }
                                //else
                                //{
                                //    worksheet.Cells[strCell].Formula = "='" + "Part " + item + "'" + "!E" + ((numRowSumCount * 5) + 12);
                                //}

                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "F" + (numRow).ToString();
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            if (i == 4)
                            {
                                if (item == "D")
                                {
                                    worksheet.Cells[strCell].Formula = "='" + "Part " + item + "'" + "!E" + ((numRowSumCount * 5) + 12);
                                }
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "G" + (numRow).ToString() + ":H" + (numRow).ToString();
                            worksheet.Cells[strCell].Merge = true;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            if (i == 4)
                            {
                                if ((item != "C") && (item != "D"))
                                {
                                    worksheet.Cells[strCell].Formula = "='" + "Part " + item + "'" + "!E" + ((numRowSumCount * 5) + 12);
                                }
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            }
                            worksheet.Cells[strCell].Style.WrapText = true;

                            numRow++;
                        }
                    }


                    int startRow = 0;
                    int endRow = 0;
                    if (havedata)
                    {
                        startRow = 11;
                    }
                    else
                    {
                        startRow = 10;
                    }
                    endRow = numRow - 1;
                    //footer


                    string spec = " ";
                    string Formula = "";
                    for (int k = 0; k < 5; k++)
                    {

                        if (k == 0)
                        {
                            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 4).ToString();
                            worksheet.Cells[strCell].Merge = true;
                            worksheet.Cells[strCell].Value = sumSheet.footer_1_l1;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                            worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //set border
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }


                        strCell = "B" + (numRow).ToString();
                        if (k == 0)
                        {

                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Value = "USD";
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Value = "EUR";
                        }
                        if (k == 2)
                        {
                            worksheet.Cells[strCell].Value = "JPY";
                        }
                        if (k == 3)
                        {
                            worksheet.Cells[strCell].Value = "CNY";
                        }
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }


                        strCell = "C" + (numRow).ToString();
                        if (k != 4)
                        {

                            Formula = "IF(B" + numRow + "=\"\",\"\",SUMIF($B$" + startRow + ":$B$" + endRow + ",$B" + numRow + ",$C$" + startRow + ":$C$" + endRow + "))";
                            //worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                            worksheet.Cells[strCell].Formula = Formula;
                        }
                        worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                        strCell = "D" + (numRow).ToString();

                        // worksheet.Cells[strCell].Style.Font.Bold = true;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(D" + startRow + ":D" + endRow + ")";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCell = "E" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCell = "F" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(F" + startRow + ":F" + endRow + ")";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCell = "G" + (numRow).ToString() + ":H" + (numRow).ToString();
                        worksheet.Cells[strCell].Merge = true;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        numRow++;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return worksheet;
        }
        public OfficeOpenXml.ExcelWorksheet setSheetSumAll_Line(OfficeOpenXml.ExcelWorksheet wk, DataTable dtPart, string type)
        {
            var worksheet = wk;
            try
            {
                worksheet.Protection.IsProtected = true;
                char cell = 'A';
                decimal totlaAmountCFR = 0;
                string strCell = "";
                int numRow = 11;
                bool havedata = false;

                //sum sheet
                //set Width 
                worksheet.Column(1).Width = 40;
                worksheet.Column(3).Width = 17;
                worksheet.Column(4).Width = 17;
                worksheet.Column(5).Width = 17;
                worksheet.Column(6).Width = 17;
                worksheet.Column(7).Width = 17;

                //set wrap 
                //wrap text in the cells
                //worksheet.Column(5).Style.WrapText = true;
                //worksheet.Column(6).Style.WrapText = true;
                //worksheet.Column(7).Style.WrapText = true;


                //path

                //header

                //childSheet.header_l1_bidno = BidNo;
                //childSheet.header_l2_PartName = partDesc;
                //childSheet.header_l3_biddesc = bidDesc;
                //childSheet.header_l4_project = project;
                XLSPS_SumSheet sumSheet = new XLSPS_SumSheet();
                //BidNO = listSum[0].bidno == null ? "" : listSum[0].bidno.ToString();
                sumSheet.header_l1_bidno = BidNo;
                sumSheet.header_l2_ScheduleName = SchName;
                sumSheet.header_l3_biddesc = bidDesc;
                sumSheet.header_l4_project = project;

                sumSheet.column_1_l1 = "Description";
                sumSheet.column_2_l1 = "Currency";
                sumSheet.column_3_l1 = "Supply of Equipment";
                sumSheet.column_3_l2_c1 = "Foreign Supply";
                sumSheet.column_3_l2_c2 = "Local Supply";
                sumSheet.column_3_l3_c1 = "CIF Thai Port";
                sumSheet.column_3_l3_c2 = "Ex-works Price";
                sumSheet.column_3_l4_c2 = "( excluding VAT )";
                sumSheet.column_3_l5_c2 = "Baht";
                sumSheet.column_3_l6_c1 = "Amount";
                sumSheet.column_3_l6_c2 = "Amount";
                sumSheet.column_4_l1 = "Local Currency";
                sumSheet.column_4_l4 = "( excluding VAT )";
                sumSheet.column_4_l5 = "Baht";
                sumSheet.column_4_l6 = "Amount";
                sumSheet.column_5_l1 = "Local Transportation";
                sumSheet.column_5_l4 = "( excluding VAT )";
                sumSheet.column_5_l5 = "Baht";
                sumSheet.column_5_l6 = "Amount";
                sumSheet.column_6_l1 = "Local Transportation,  Construction and  Installation";
                sumSheet.column_6_l4 = "( excluding VAT )";
                sumSheet.column_6_l5 = "Baht";
                sumSheet.column_6_l6 = "Amount";

                sumSheet.footer_1_l1 = "TOTAL PRICE";
                sumSheet.footer_3_l1_c1 = "Baht";
                sumSheet.footer_3_l1_c2 = "Baht";
                sumSheet.footer_4_l1 = "Baht";
                sumSheet.footer_5_l1 = "Baht";
                sumSheet.footer_6_l1 = "Baht";


                worksheet.Cells["A1:G1"].Merge = true;
                worksheet.Cells["A1:G1"].Value = ("INVITATION TO BID NO. " + sumSheet.header_l1_bidno).ToUpper();
                worksheet.Cells["A1:G1"].Style.Font.Bold = true;
                worksheet.Cells["A1:G1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A2:G2"].Merge = true;
                worksheet.Cells["A2:G2"].Value = "TOTAL PRICE FOR SCHEDULE NO. " + ScheNo;
                worksheet.Cells["A2:G2"].Style.Font.Bold = true;
                worksheet.Cells["A2:G2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A3:G3"].Merge = true;
                worksheet.Cells["A3:G3"].Value = SchName.ToUpper();// "1" + part + " : " + childSheet.header_l3_biddesc;
                worksheet.Cells["A3:G3"].Style.Font.Bold = true;
                worksheet.Cells["A3:G3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A4:G4"].Merge = true;
                worksheet.Cells["A4:G4"].Value = "Line Length " + this.getLineLength(BidNo, BidRev, SchName).ToUpper();// childSheet.header_l4_project;
                worksheet.Cells["A4:G4"].Style.Font.Bold = true;
                worksheet.Cells["A4:G4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                worksheet.Cells["A5:G5"].Merge = true;
                worksheet.Cells["A5:G5"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// childSheet.header_l4_project;
                worksheet.Cells["A5:G5"].Style.Font.Bold = true;
                worksheet.Cells["A5:G5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                worksheet.Cells["A6:G6"].Merge = true;
                worksheet.Cells["A6:G6"].Value = (sumSheet.header_l4_project).ToUpper();
                worksheet.Cells["A6:G6"].Style.Font.Bold = true;
                worksheet.Cells["A6:G6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                //header Colunm
                worksheet.Cells["A7:A12"].Merge = true;
                worksheet.Cells["A7:A12"].Value = sumSheet.column_1_l1;
                worksheet.Cells["A7:A12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["A7:A12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["A7:A12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A7:A12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A7:A12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A7:A12"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["B7:B12"].Merge = true;
                worksheet.Cells["B7:B12"].Value = sumSheet.column_2_l1;
                worksheet.Cells["B7:B12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["B7:B12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["B7:B12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B7:B12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B7:B12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B7:B12"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C7:D7"].Merge = true;
                worksheet.Cells["C7:D7"].Value = sumSheet.column_3_l1;
                worksheet.Cells["C7:D7"].Style.Font.Bold = true;
                worksheet.Cells["C7:D7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C7:D7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C7:D7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C7:D7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C7:D7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C8"].Value = sumSheet.column_3_l2_c1;
                worksheet.Cells["C8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D8"].Merge = true;
                worksheet.Cells["D8"].Value = sumSheet.column_3_l2_c2;
                worksheet.Cells["D8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C9:C11"].Merge = true;
                worksheet.Cells["C9:C11"].Value = sumSheet.column_3_l3_c1;
                worksheet.Cells["C9:C11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C9:C11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["C9:C11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D9"].Value = sumSheet.column_3_l3_c2;
                worksheet.Cells["D9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D10"].Value = sumSheet.column_3_l4_c2;
                worksheet.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D11"].Value = sumSheet.column_3_l5_c2;
                worksheet.Cells["D11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["D1"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C12"].Value = sumSheet.column_3_l6_c1;
                worksheet.Cells["C12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["C12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D12"].Value = sumSheet.column_3_l6_c2;
                worksheet.Cells["D12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["D12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E7:E9"].Merge = true;
                worksheet.Cells["E7:E9"].Value = sumSheet.column_4_l1;
                worksheet.Cells["E7:E9"].Style.Font.Bold = true;
                worksheet.Cells["E7:E9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E7:E9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["E7:E9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7:E9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7:E9"].Style.WrapText = true;

                worksheet.Cells["E10"].Value = sumSheet.column_4_l4;
                worksheet.Cells["E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E11"].Value = sumSheet.column_4_l5;
                worksheet.Cells["E11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["E11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E12"].Value = sumSheet.column_4_l6;
                worksheet.Cells["E12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["E12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F7:F9"].Merge = true;
                worksheet.Cells["F7:F9"].Value = sumSheet.column_5_l1;
                worksheet.Cells["F7:F9"].Style.Font.Bold = true;
                worksheet.Cells["F7:F9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F7:F9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["F7:F9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F7:F9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F7:F9"].Style.WrapText = true;

                worksheet.Cells["F10"].Value = sumSheet.column_5_l4;
                worksheet.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F11"].Value = sumSheet.column_5_l5;
                worksheet.Cells["F11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["F11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F12"].Value = sumSheet.column_5_l6;
                worksheet.Cells["F12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["F12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["G7:H9"].Merge = true;
                worksheet.Cells["G7:H9"].Value = sumSheet.column_6_l1;
                worksheet.Cells["G7:H9"].Style.Font.Bold = true;
                worksheet.Cells["G7:H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G7:H9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["G7:H9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G7:H9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G7:H9"].Style.WrapText = true;

                worksheet.Cells["G10:H10"].Merge = true;
                worksheet.Cells["G10:H10"].Value = sumSheet.column_6_l4;
                worksheet.Cells["G10:H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G10:H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["G11:H11"].Merge = true;
                worksheet.Cells["G11:H11"].Value = sumSheet.column_6_l5;
                worksheet.Cells["G11:H11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G11:H11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["G11:H11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["G12:H12"].Merge = true;
                worksheet.Cells["G12:H12"].Value = sumSheet.column_6_l6;
                worksheet.Cells["G12:H12"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G12:H12"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["G12:H12"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G12:H12"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G12:H12"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                //data
                decimal ForeignAmount = 0;
                decimal LocalAmount = 0;
                decimal LocalCurAmount = 0;
                decimal LocalTranAmount = 0;
                decimal LocalTranConAmount = 0;

                if (dtPart.Rows.Count > 0)
                {

                    string curreny = "";
                    string part_name = "";
                    int numRowSumCount = 0;

                    List<string> listPart = new List<string>();


                    foreach (DataRow dr in dtPart.Rows)
                    {
                        string lineType = "";

                        DataTable dt_eq = new DataTable();// objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);

                        dt_eq = getGVSelPart(Dept, Sect, BidNo, SchName, dr["part_name"].ToString(), JobNo, JobRev);
                        numRowSumCount = dt_eq.Rows.Count;
                        if (dt_eq.Rows.Count > 0)
                        {
                            // part = dt_eq.Rows[0][""]
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("A"))
                            {
                                part_name = "A";
                                listPart.Add(part_name);
                                lineType = "Line1";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("I"))
                            {
                                part_name = "I";
                                listPart.Add(part_name);
                                lineType = "Line1";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("O"))
                            {
                                part_name = "O";
                                listPart.Add(part_name);
                                lineType = "Line1";
                            }
                            //line2
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("B"))
                            {
                                part_name = "B";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("C"))
                            {
                                part_name = "C";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("D"))
                            {
                                part_name = "D";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("E"))
                            {
                                part_name = "E";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("F"))
                            {
                                part_name = "F";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("G"))
                            {
                                part_name = "G";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("J"))
                            {
                                part_name = "J";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("K"))
                            {
                                part_name = "K";
                                listPart.Add(part_name);
                                lineType = "Line2";
                            }
                            if (dt_eq.Rows[0]["Part"].ToString().Substring(0, 1).Equals("H"))
                            {
                                part_name = "H";
                                listPart.Add(part_name);
                                lineType = "Substation";
                            }

                            var distinctItems = listPart.Distinct().ToList();
                            int rowBysheet = 0;
                            if (lineType == "Substation")
                            {
                                rowBysheet = 11;
                            }
                            if (lineType == "Line1")
                            {
                                rowBysheet = 12;
                            }
                            if (lineType == "Line2")
                            {
                                rowBysheet = 10;
                            }
                            havedata = true;

                            strCell = "A" + (numRow).ToString();
                            worksheet.Cells[strCell].Value = "PART " + ScheNo + dr["part_name"].ToString() + " :" + this.getPartName(dr["part_name"].ToString(), "ss");
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "B" + (numRow).ToString();
                            worksheet.Cells[strCell].Value = curreny;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.WrapText = true;

                            strCell = "C" + (numRow).ToString();
                            //worksheet.Cells[strCell].Value = item +"Row"+ numRowSumCount+11;
                            if (lineType == "Line2")
                            {
                                worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!G" + (numRowSumCount + rowBysheet);
                            }
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.WrapText = true;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            // ForeignAmount += Convert.ToDecimal(item.ForeignAmount);

                            strCell = "D" + (numRow).ToString();
                            //worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                            // worksheet.Cells[strCell].Value = item.LocalAmount == "0" ? "" : item.LocalAmount;
                            if (lineType == "Line2")
                            {
                                worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!I" + (numRowSumCount + rowBysheet);
                            }
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.WrapText = true;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //  LocalAmount += Convert.ToDecimal(item.LocalAmount);

                            strCell = "E" + (numRow).ToString();
                            //  worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                            //  worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.WrapText = true;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //  LocalCurAmount += Convert.ToDecimal(item.LocalCurAmount);

                            strCell = "F" + (numRow).ToString();
                            // worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                            //   worksheet.Cells[strCell].Value = item.LocalTranAmount == "0" ? "" : item.LocalTranAmount;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.WrapText = true;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //   LocalTranAmount += Convert.ToDecimal(item.LocalTranAmount);

                            strCell = "G" + (numRow).ToString();
                            // worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                            //  worksheet.Cells[strCell].Value = item.LocalTranConAmount == "0" ? "" : item.LocalTranConAmount;
                            if (lineType == "Line2")
                            {
                                worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!K" + (numRowSumCount + rowBysheet);
                            }
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            worksheet.Cells[strCell].Style.WrapText = true;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                            //   LocalTranConAmount += Convert.ToDecimal(item.LocalTranConAmount);

                            numRow++;
                        }
                    }


                    int startRow = 0;
                    int endRow = 0;
                    //if (lineType == "Line2")
                    //{
                    //}
                    //if (havedata)
                    //{
                    //    startRow = 11;
                    //}
                    //else
                    //{
                    //    startRow = 10;
                    //}
                    endRow = numRow - 1;
                    //footer
                    string spec = " ";
                    strCell = "A" + (numRow).ToString();
                    //worksheet.Cells[strCell].Merge = true;
                    worksheet.Cells[strCell].Value = sumSheet.footer_1_l1;
                    worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //set border
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                    strCell = "C" + (numRow).ToString();
                    //worksheet.Cells[strCell].Style.Font.Bold = true;
                    //worksheet.Cells[strCell].Value = SUM(C11: C12);
                    worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "SUM(D" + startRow + ":D" + endRow + ")";
                    // worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                    //worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    strCell = "F" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                    // worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                    //worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return worksheet;
        }
        public OfficeOpenXml.ExcelWorksheet setSheetSumAll_Supply(OfficeOpenXml.ExcelWorksheet wk, DataTable dtPart, string type)
        {
            var worksheet = wk;
            try
            {
                worksheet.Protection.IsProtected = true;
                char cell = 'A';
                decimal totlaAmountCFR = 0;
                string strCell = "";
                int numRow = 11;
                bool havedata = false;

                //sum sheet
                //set Width 
                worksheet.Column(1).Width = 17;
                worksheet.Column(2).Width = 40;
                worksheet.Column(4).Width = 25;
                worksheet.Column(5).Width = 25;
                worksheet.Column(6).Width = 25;
                worksheet.Column(7).Width = 25;
                worksheet.Column(8).Width = 25;

                //set wrap 
                //wrap text in the cells
                //worksheet.Column(5).Style.WrapText = true;
                //worksheet.Column(6).Style.WrapText = true;
                //worksheet.Column(7).Style.WrapText = true;


                //header

                //childSheet.header_l1_bidno = BidNo;
                //childSheet.header_l2_PartName = partDesc;
                //childSheet.header_l3_biddesc = bidDesc;
                //childSheet.header_l4_project = project;
                XLSPS_SumSheet sumSheet = new XLSPS_SumSheet();
                //BidNO = listSum[0].bidno == null ? "" : listSum[0].bidno.ToString();
                sumSheet.header_l1_bidno = BidNo;
                sumSheet.header_l2_ScheduleName = SchName;
                sumSheet.header_l3_biddesc = "SUPPLY OF " + BidNo;
                sumSheet.header_l4_project = project;

                sumSheet.column_1_l1 = "No.";
                sumSheet.column_2_l1 = "Description";
                sumSheet.column_3_l1 = "Currency";
                sumSheet.column_4_l1 = "Supply of Equipment";
                sumSheet.column_4_l2 = "Foreign Supply";
                sumSheet.column_4_l3 = "CFR Thai Port";
                sumSheet.column_4_l6 = "Amount";
                sumSheet.column_5_l1 = "Supply of Equipment";
                sumSheet.column_5_l2 = "";
                sumSheet.column_5_l3 = "DDP EGAT's Store";
                sumSheet.column_5_l4 = "( excluding VAT )";
                sumSheet.column_5_l5 = "Baht";
                sumSheet.column_5_l6 = "Amount";
                sumSheet.column_6_l1 = "Supply of Equipment";
                sumSheet.column_6_l2 = "Local Supply";
                sumSheet.column_6_l3 = "Ex-works Price";
                sumSheet.column_6_l4 = "( excluding VAT )";
                sumSheet.column_6_l5 = "Baht";
                sumSheet.column_6_l6 = "Amount";
                sumSheet.column_7_l1 = "Foreign Currency";
                sumSheet.column_7_l4 = "";
                sumSheet.column_7_l5 = "";
                sumSheet.column_7_l6 = "Amount";
                sumSheet.column_8_l1 = "Local Currency";
                sumSheet.column_8_l4 = "( excluding VAT )"; ;
                sumSheet.column_8_l5 = "Baht";
                sumSheet.column_8_l6 = "Amount";



                sumSheet.footer_1_l1 = "BID PRICE";
                sumSheet.footer_4_l1 = "Baht";
                sumSheet.footer_5_l1 = "Baht";
                sumSheet.footer_7_l1 = "Baht";

                sumSheet.footer_1_l5 = "VAT";
                sumSheet.footer_4_l5 = "Baht";
                sumSheet.footer_5_l5 = "Baht";
                sumSheet.footer_7_l5 = "Baht";

                sumSheet.footer_1_l9 = "SUMMARY OF BID PRICE";
                sumSheet.footer_4_l9 = "Baht";
                sumSheet.footer_5_l9 = "Baht";
                sumSheet.footer_7_l9 = "Baht";


                worksheet.Cells["A1:H1"].Merge = true;
                worksheet.Cells["A1:H1"].Value = ("INVITATION TO BID NO. " + sumSheet.header_l1_bidno).ToUpper();
                worksheet.Cells["A1:H1"].Style.Font.Bold = true;
                worksheet.Cells["A1:H1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A2:H2"].Merge = true;
                worksheet.Cells["A2:H2"].Value = ("Schedule " + ScheNo + ": " + sumSheet.header_l2_ScheduleName).ToUpper();
                worksheet.Cells["A2:H2"].Style.Font.Bold = true;
                worksheet.Cells["A2:H2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A3:H3"].Merge = true;
                worksheet.Cells["A3:H3"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// sumSheet.header_l3_biddesc;
                worksheet.Cells["A3:H3"].Style.Font.Bold = true;
                worksheet.Cells["A3:G3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A4:H4"].Merge = true;
                worksheet.Cells["A4:H4"].Value = (sumSheet.header_l4_project).ToUpper(); ;
                worksheet.Cells["A4:H4"].Style.Font.Bold = true;
                worksheet.Cells["A4:H4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


                //header Colunm

                worksheet.Cells["A5:A10"].Merge = true;
                worksheet.Cells["A5:A10"].Value = sumSheet.column_1_l1;
                //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
                worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["B5:B10"].Merge = true;
                worksheet.Cells["B5:B10"].Value = sumSheet.column_2_l1;
                //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
                worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C5:C10"].Merge = true;
                worksheet.Cells["C5:C10"].Value = sumSheet.column_3_l1;
                //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
                worksheet.Cells["C5:C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C5:C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["C5:C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["D5"].Value = sumSheet.column_4_l1;
                worksheet.Cells["D5"].Style.Font.Bold = true;
                worksheet.Cells["D5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["D6"].Value = sumSheet.column_4_l2;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["D6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["D7:D9"].Merge = true;
                worksheet.Cells["D7:D9"].Value = sumSheet.column_4_l3;
                //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
                worksheet.Cells["D7:D9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D7:D9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                worksheet.Cells["D7:D9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D7:D9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                // worksheet.Cells["D7"].Merge = true;
                worksheet.Cells["D10"].Value = sumSheet.column_4_l6;
                //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
                worksheet.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["E5"].Value = sumSheet.column_5_l1;
                worksheet.Cells["E5"].Style.Font.Bold = true;
                worksheet.Cells["E5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["E6"].Value = sumSheet.column_5_l2;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["E6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E7"].Value = sumSheet.column_5_l3;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["E7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E8"].Value = sumSheet.column_5_l4;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["E8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E9"].Value = sumSheet.column_5_l5;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["E9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;



                // worksheet.Cells["D7"].Merge = true;
                worksheet.Cells["E10"].Value = sumSheet.column_6_l6;
                //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
                worksheet.Cells["E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["F5"].Value = sumSheet.column_6_l1;
                worksheet.Cells["F5"].Style.Font.Bold = true;
                worksheet.Cells["F5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["F6"].Value = sumSheet.column_6_l2;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["F6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F7"].Value = sumSheet.column_6_l3;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["F7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F8"].Value = sumSheet.column_6_l4;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["F8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["F9"].Value = sumSheet.column_6_l5;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["F9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;



                // worksheet.Cells["D7"].Merge = true;
                worksheet.Cells["F10"].Value = sumSheet.column_7_l6;
                //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
                worksheet.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["G5:G7"].Value = sumSheet.column_7_l1;
                worksheet.Cells["G5:G7"].Style.Font.Bold = true;
                worksheet.Cells["G5:G7"].Merge = true;
                worksheet.Cells["G5:G7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["G5:G7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:G7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:G7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:G7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["G8"].Value = "";
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["G8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["G8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["G9"].Value = "";
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["G9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["G9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                // worksheet.Cells["D7"].Merge = true;
                worksheet.Cells["G10"].Value = sumSheet.column_7_l6;
                //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
                worksheet.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["H5:H7"].Value = sumSheet.column_8_l1;
                worksheet.Cells["H5:H7"].Style.Font.Bold = true;
                worksheet.Cells["H5:H7"].Merge = true;
                worksheet.Cells["H5:H7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["H5:H7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H5:H7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H5:H7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H5:H7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


                worksheet.Cells["H8"].Value = sumSheet.column_8_l4;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["H8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["H8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["H9"].Value = sumSheet.column_8_l5;
                //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
                worksheet.Cells["H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["H9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                // worksheet.Cells["D7"].Merge = true;
                worksheet.Cells["H10"].Value = sumSheet.column_8_l6;
                //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
                worksheet.Cells["H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                //data
                //decimal ForeignAmount = 0;
                //decimal LocalAmount = 0;
                //decimal LocalCurAmount = 0;
                //decimal LocalTranAmount = 0;
                //decimal LocalTranConAmount = 0;


                if (dtPart.Rows.Count > 0)
                {

                    string curreny = "";
                    string part_name = "";
                    int numRowSumCount = 0;

                    List<string> listPart = new List<string>();

                    int count = 1;
                    int No = 1;
                    foreach (DataRow dr in dtPart.Rows)
                    {
                        string lineType = "";

                        DataTable dt_eq = new DataTable();// objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);

                        dt_eq = getGVSelPart(Dept, Sect, BidNo, SchName, dr["part_name"].ToString(), JobNo, JobRev);
                        numRowSumCount = dt_eq.Rows.Count;
                        if (dt_eq.Rows.Count > 0)
                        {

                            int rowBysheet = 0;
                            rowBysheet = 11;
                            havedata = true;
                            ;
                            for (int i = 1; i <= 4; i++)
                            {
                                strCell = "A" + (numRow).ToString();
                                if (i == 1)
                                {
                                    worksheet.Cells[strCell].Value = No.ToString();
                                    No++;
                                }
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                worksheet.Cells[strCell].Style.WrapText = true;


                                strCell = "B" + (numRow).ToString();
                                if (i == 1)
                                {
                                    worksheet.Cells[strCell].Value = "Schedule " + ScheNo + dr["part_name"].ToString() + ":";
                                }

                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.WrapText = true;


                                strCell = "C" + (numRow).ToString();
                                // worksheet.Cells[strCell].Value = Part.Currency;
                                switch (i)
                                {
                                    case 0:
                                        worksheet.Cells[strCell].Value = "USD";
                                        break;
                                    case 1:
                                        worksheet.Cells[strCell].Value = "EUR";
                                        break;
                                    case 2:
                                        worksheet.Cells[strCell].Value = "JPY";
                                        break;
                                    case 3:
                                        worksheet.Cells[strCell].Value = "CNY";
                                        break;
                                    case 4:
                                        worksheet.Cells[strCell].Value = "THB";
                                        break;
                                }
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.WrapText = true;


                                strCell = "D" + (numRow).ToString();
                                if (i != 4)
                                {
                                    worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!J" + ((numRowSumCount * 5) + 11 + i);
                                }
                                //worksheet.Cells[strCell].Value = Part.ForeignAmount == "0" ? "" : Part.ForeignAmount;
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.WrapText = true;


                                //  LocalAmount += Convert.ToDecimal(item.LocalAmount);
                                strCell = "E" + (numRow).ToString();
                                //  worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                                //  worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                                if (i == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!L" + (numRowSumCount + rowBysheet);
                                }
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                worksheet.Cells[strCell].Style.WrapText = true;
                                //  LocalCurAmount += Convert.ToDecimal(item.LocalCurAmount);

                                strCell = "F" + (numRow).ToString();
                                // worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                                //   worksheet.Cells[strCell].Value = item.LocalTranAmount == "0" ? "" : item.LocalTranAmount;
                                if (i == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!N" + (numRowSumCount + rowBysheet);
                                }
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                worksheet.Cells[strCell].Style.WrapText = true;
                                //   LocalTranAmount += Convert.ToDecimal(item.LocalTranAmount);

                                strCell = "G" + (numRow).ToString();
                                // worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                                //  worksheet.Cells[strCell].Value = item.LocalTranConAmount == "0" ? "" : item.LocalTranConAmount;
                                if (i == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!P" + (numRowSumCount + rowBysheet);
                                }
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                worksheet.Cells[strCell].Style.WrapText = true;
                                //   LocalTranConAmount += Convert.ToDecimal(item.LocalTranConAmount);

                                strCell = "H" + (numRow).ToString();
                                // worksheet.Cells[strCell].Value = item + "Row" + numRowSumCount + 11;
                                //  worksheet.Cells[strCell].Value = item.LocalTranConAmount == "0" ? "" : item.LocalTranConAmount;
                                if (i == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "='" + dr["part_name"].ToString() + "'" + "!R" + (numRowSumCount + rowBysheet);
                                }
                                if (i == 4)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin; ;
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                worksheet.Cells[strCell].Style.WrapText = true;
                                //   LocalTranConAmount += Convert.ToDecimal(item.LocalTranConAmount);

                                numRow++;
                            }
                        }
                    }




                    int startRow = 0;
                    int endRow = 0;
                    if (havedata)
                    {
                        startRow = 11;
                    }
                    else
                    {
                        startRow = 10;
                    }
                    endRow = numRow - 1;
                    //footer


                    string spec = " ";
                    string Formula = "";
                    for (int k = 0; k < 5; k++)
                    {

                        if (k == 0)
                        {
                            strCell = "A" + (numRow).ToString() + ":" + "B" + (numRow + 4).ToString();
                            worksheet.Cells[strCell].Merge = true;
                            worksheet.Cells[strCell].Value = sumSheet.footer_1_l1;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                            worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                            //set border
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }


                        strCell = "C" + (numRow).ToString();
                        if (k == 0)
                        {

                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Value = "USD";
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Value = "EUR";
                        }
                        if (k == 2)
                        {
                            worksheet.Cells[strCell].Value = "JPY";
                        }
                        if (k == 3)
                        {
                            worksheet.Cells[strCell].Value = "CNY";
                        }
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }


                        strCell = "D" + (numRow).ToString();
                        if (k != 4)
                        {

                            Formula = "IF(C" + numRow + "=\"\",\"\",SUMIF($C$" + startRow + ":$C$" + endRow + ",$C" + numRow + ",$D$" + startRow + ":$D$" + endRow + "))";
                            //worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                            worksheet.Cells[strCell].Formula = Formula;
                        }
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                        strCell = "E" + (numRow).ToString();

                        // worksheet.Cells[strCell].Style.Font.Bold = true;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCell = "F" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(F" + startRow + ":F" + endRow + ")";
                            //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCell = "G" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")";
                            //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        strCell = "H" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (k == 0)
                        {
                            worksheet.Cells[strCell].Value = "Baht";
                            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                        }
                        if (k == 1)
                        {
                            worksheet.Cells[strCell].Formula = "SUM(H" + startRow + ":H" + endRow + ")";
                            //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }

                        if (k == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }


                        numRow++;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return worksheet;
        }
        #endregion

        #region " Set Sheet Substation Main and Detail Part Type 1"
        public OfficeOpenXml.ExcelWorksheet setSheetPartSub1(OfficeOpenXml.ExcelWorksheet wk, List<DataRow> listPart, DataTable dtPart, string part)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            DataTable dtt = getBtnPaths(BidNo, SchName, part);
            if (dtt.Rows.Count > 0)
            {
                partDesc = dtt.Rows[0]["part_description"].ToString();
            }

            //sum sheet
            //set Width 
            worksheet.Column(1).Width = 40;
            worksheet.Column(3).Width = 17;
            worksheet.Column(4).Width = 17;
            worksheet.Column(5).Width = 17;

            //set wrap 
            //wrap text in the cells
            // worksheet.Column(5).Style.WrapText = true;



            XLSPS_PartSheet partSheet = new XLSPS_PartSheet();
            //header
            partSheet.header_l1_bidno = BidNo;
            partSheet.header_l2_PartName = "SUPPLY AND INSTALLATION OF  SUBSTATION EQUIPMENT";
            partSheet.header_l3_biddesc = bidDesc;
            partSheet.header_l4_project = project;

            partSheet.column_1_l1 = "Description";
            partSheet.column_2_l1 = "Currency";
            partSheet.column_3_l1 = "Supply of Equipment";
            partSheet.column_3_l2_c1 = "Foreign Supply";
            partSheet.column_3_l2_c2 = "Local Supply";
            partSheet.column_3_l3_c1 = "CIF Thai Port";
            partSheet.column_3_l3_c2 = "Ex-works Price";
            partSheet.column_3_l4_c2 = "( excluding VAT )";
            partSheet.column_3_l5_c2 = "Baht";
            partSheet.column_3_l6_c1 = "Amount";
            partSheet.column_3_l6_c2 = "Amount";

            if ((part.ToUpper().StartsWith("D") == true) || (part.ToUpper().StartsWith("_D") == true))
                partSheet.column_4_l1 = "Local Transportation";
            else
                partSheet.column_4_l1 = "Local Transportation,  Construction and  Installation";

            partSheet.column_4_l4 = "( excluding VAT )";
            partSheet.column_4_l5 = "Baht";
            partSheet.column_4_l6 = "Amount";

            partSheet.footer_1_l1 = "PART " + ScheNo + part;
            partSheet.footer_3_l1_c1 = "Baht";
            partSheet.footer_3_l1_c2 = "Baht";
            partSheet.footer_4_l1 = "Baht";
            partSheet.footer_5_l1 = "Baht";

            worksheet.Cells["A1:E1"].Merge = true;
            worksheet.Cells["A1:E1"].Value = ("INVITATION TO BID NO. " + partSheet.header_l1_bidno).ToUpper();
            worksheet.Cells["A1:E1"].Style.Font.Bold = true;
            worksheet.Cells["A1:E1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:E2"].Merge = true;
            worksheet.Cells["A2:E2"].Value = ("PART " + ScheNo + part + " : " + this.getPartName(part, "ss")).ToUpper();//partSheet.header_l2_PartName;
            worksheet.Cells["A2:E2"].Style.Font.Bold = true;
            worksheet.Cells["A2:E2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:E3"].Merge = true;
            worksheet.Cells["A3:E3"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// (partSheet.header_l3_biddesc).ToUpper();
            worksheet.Cells["A3:E3"].Style.Font.Bold = true;
            worksheet.Cells["A3:E3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:E4"].Merge = true;
            worksheet.Cells["A4:E4"].Value = (partSheet.header_l4_project).ToUpper();
            worksheet.Cells["A4:E4"].Style.Font.Bold = true;
            worksheet.Cells["A4:E4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


            //header Colunm

            worksheet.Cells["A5:A10"].Merge = true;
            worksheet.Cells["A5:A10"].Value = partSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B5:B10"].Merge = true;
            worksheet.Cells["B5:B10"].Value = partSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["C5:D5"].Merge = true;
            worksheet.Cells["C5:D5"].Value = partSheet.column_3_l1;
            worksheet.Cells["C5:D5"].Style.Font.Bold = true;
            worksheet.Cells["C5:D5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C5:D5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:D5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:D5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:D5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["C6"].Value = partSheet.column_3_l2_c1;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["C6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["D6"].Merge = true;
            worksheet.Cells["D6"].Value = partSheet.column_3_l2_c2;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["D6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["C7:C9"].Merge = true;
            worksheet.Cells["C7:C9"].Value = partSheet.column_3_l3_c1;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["C7:C9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C7:C9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["C7:C9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            // worksheet.Cells["D7"].Merge = true;
            worksheet.Cells["D7"].Value = partSheet.column_3_l3_c2;
            //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
            worksheet.Cells["D7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;



            //worksheet.Cells["D8"].Merge = true;
            worksheet.Cells["D8"].Value = partSheet.column_3_l4_c2;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["D8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            //worksheet.Cells["D9"].Merge = true;
            worksheet.Cells["D9"].Value = partSheet.column_3_l5_c2;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["D9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["D9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            //worksheet.Cells["C10"].Merge = true;
            worksheet.Cells["C10"].Value = partSheet.column_3_l6_c1;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            //worksheet.Cells["D10"].Merge = true;
            worksheet.Cells["D10"].Value = partSheet.column_3_l6_c2;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            string strCell_LocalTransportation1 = "";
            string strCell_LocalTransportation2 = "";
            string strCell_LocalTransportation3 = "";
            string strCell_LocalTransportation4 = "";
            if ((part.ToUpper().StartsWith("D") == true) || (part.ToUpper().StartsWith("_D") == true))
            {
                strCell_LocalTransportation1 = "E5:E7";
                strCell_LocalTransportation2 = "E8";
                strCell_LocalTransportation3 = "E9";
                strCell_LocalTransportation4 = "E10";
            }
            else
            {
                strCell_LocalTransportation1 = "E5:F7";
                strCell_LocalTransportation2 = "E8:F8";
                strCell_LocalTransportation3 = "E9:F9";
                strCell_LocalTransportation4 = "E10:F10";
            }

            worksheet.Cells[strCell_LocalTransportation1].Merge = true;
            worksheet.Cells[strCell_LocalTransportation1].Value = partSheet.column_4_l1;
            worksheet.Cells[strCell_LocalTransportation1].Style.Font.Bold = true;
            worksheet.Cells[strCell_LocalTransportation1].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation1].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation1].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell_LocalTransportation1].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell_LocalTransportation1].Style.WrapText = true;

            worksheet.Cells[strCell_LocalTransportation2].Merge = true;
            worksheet.Cells[strCell_LocalTransportation2].Value = partSheet.column_4_l4;
            worksheet.Cells[strCell_LocalTransportation2].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation2].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation2].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells[strCell_LocalTransportation3].Merge = true;
            worksheet.Cells[strCell_LocalTransportation3].Value = partSheet.column_4_l5;
            worksheet.Cells[strCell_LocalTransportation3].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation3].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation3].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells[strCell_LocalTransportation4].Merge = true;
            worksheet.Cells[strCell_LocalTransportation4].Value = partSheet.column_4_l6;
            worksheet.Cells[strCell_LocalTransportation4].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation4].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell_LocalTransportation4].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell_LocalTransportation4].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell_LocalTransportation4].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            //data
            DataTable dt_eq = new DataTable();
            //foreach (DataRow dr in dtPart.Rows)
            //{
            //    dt_eq = objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);
            //    /// listEq.Add(dt_eq);
            //}

            //var resultsPartAB = dtPart.Select("SUBSTRING(part_name,1,2) like '%AB%'and  LEN(part_name) > 1 ");
            //var resultsPartD = dtPart.Select("SUBSTRING(part_name,1,1) like '%D%' and LEN(part_name) > 1 ");
            //var resultsPart_D = dtPart.Select("SUBSTRING(part_name,1,2) like '%_d%' and  LEN(part_name) > 1 ");
            //var resultsPartC = dtPart.Select("SUBSTRING(part_name,1,1) like '%C%' and  LEN(part_name) > 1 ");

            decimal ForeignAmount = 0;
            decimal LocalAmount = 0;
            decimal LocalCurAmount = 0;
            decimal LocalTranAmount = 0;
            decimal LocalTranConAmount = 0;
            bool nodata = false;
            if (listPart.Count > 0)
            {

                foreach (var item in listPart)
                {
                    int countRow = 0;
                    dt_eq = getGVSelPart(Dept, Sect, BidNo, SchName, item["part_name"].ToString(), JobNo, JobRev);
                    if (dt_eq != null)
                    {
                        countRow = dt_eq.Rows.Count;
                    }

                    for (int i = 0; i <= 4; i++)
                    {
                        if (i == 0)
                        {
                            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 4).ToString();
                            worksheet.Cells[strCell].Merge = true;
                            worksheet.Cells[strCell].Value = "Schedule " + ScheNo + item["part_name"].ToString() + " : " + getPartName(item["part_name"].ToString(), "ss");
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                            worksheet.Cells[strCell].Style.WrapText = true;
                        }

                        strCell = "B" + (numRow).ToString();
                        // worksheet.Cells[strCell].Value = Part.Currency;
                        switch (i)
                        {
                            case 0:
                                worksheet.Cells[strCell].Value = "USD";
                                break;
                            case 1:
                                worksheet.Cells[strCell].Value = "EUR";
                                break;
                            case 2:
                                worksheet.Cells[strCell].Value = "JPY";
                                break;
                            case 3:
                                worksheet.Cells[strCell].Value = "CNY";
                                break;
                            case 4:
                                worksheet.Cells[strCell].Value = "THB";
                                break;
                        }
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "C" + (numRow).ToString();
                        if (i != 4)
                        {
                            worksheet.Cells[strCell].Formula = "='" + item["part_name"].ToString() + "'" + "!G" + (countRow + 11 + i);
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                        }
                        //worksheet.Cells[strCell].Value = Part.ForeignAmount == "0" ? "" : Part.ForeignAmount;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;
                        //  ForeignAmount += Convert.ToDecimal(Part.ForeignAmount);

                        strCell = "D" + (numRow).ToString();
                        //if (i != 4)
                        //{
                        //    worksheet.Cells[strCell].Formula = "='" + item[0].ToString() + "'" + "!J" + (countRow + 11 + i);
                        //}
                        // worksheet.Cells[strCell].Value = Part.LocalAmount == "0" ? "" : Part.LocalAmount;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Formula = "='" + item["part_name"].ToString() + "'" + "!J" + (countRow + 12);
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        if ((part.ToUpper().StartsWith("D") == true) || (part.ToUpper().StartsWith("_D") == true))
                            strCell = "E" + (numRow).ToString();
                        else
                            strCell = "E" + (numRow).ToString() + ":F" + (numRow).ToString(); ;

                        worksheet.Cells[strCell].Merge = true;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Formula = "='" + item["part_name"].ToString() + "'" + "!L" + (countRow + 12);
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;
                        //  LocalCurAmount += Convert.ToDecimal(Part.LocalCurAmount);

                        numRow++;
                    }

                }
            }
            else
            {
                nodata = true;
            }



            //footer
            string spec = " ";

            //footer
            int startRow = 0;
            int endRow = 0;
            if (nodata)
            {
                startRow = 11;
            }
            else
            {
                startRow = 10;
            }

            endRow = numRow - 1;
            string Formula = "";
            for (int k = 0; k < 5; k++)
            {
                if (k == 0)
                {
                    strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 4).ToString();
                    worksheet.Cells[strCell].Merge = true;
                    worksheet.Cells[strCell].Value = partSheet.footer_1_l1;
                    worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //set border
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "B" + (numRow).ToString();
                if (k == 0)
                {

                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Value = "USD";
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Value = "EUR";
                }
                if (k == 2)
                {
                    worksheet.Cells[strCell].Value = "JPY";
                }
                if (k == 3)
                {
                    worksheet.Cells[strCell].Value = "CNY";
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "C" + (numRow).ToString();
                if (k != 4)
                {

                    Formula = "IF(B" + numRow + "=\"\",\"\",SUMIF($B$" + startRow + ":$B$" + endRow + ",$B" + numRow + ",$C$" + startRow + ":$C$" + endRow + "))";
                    //worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                    worksheet.Cells[strCell].Formula = Formula;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                strCell = "D" + (numRow).ToString();
                // worksheet.Cells[strCell].Value = partSheet.footer_3_l1_c2 + spec + (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                // worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Font.Bold = true;
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(D" + startRow + ":D" + endRow + ")";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }


                if ((part.ToUpper().StartsWith("D") == true) || (part.ToUpper().StartsWith("_D") == true))
                    strCell = "E" + (numRow).ToString();
                else
                    strCell = "E" + (numRow).ToString() + ":F" + (numRow).ToString();

                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Font.Bold = true;
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                numRow++;
            }
            return worksheet;
        }
        public OfficeOpenXml.ExcelWorksheet setSheetChildSubstationFormat1(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            partDesc = part_description;

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 30;
            worksheet.Column(6).Width = 15;
            worksheet.Column(7).Width = 15;
            worksheet.Column(8).Width = 15;
            worksheet.Column(9).Width = 15;
            worksheet.Column(10).Width = 15;
            worksheet.Column(11).Width = 15;
            worksheet.Column(12).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = partDesc;
            childSheet.header_l3_biddesc = bidDesc;
            childSheet.header_l4_project = project;

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Drawing No./ Reference No.";
            childSheet.column_4_l1 = "Qty.";
            childSheet.column_5_l1 = "Unit";
            childSheet.column_6_l1 = "Currency";
            childSheet.column_7_l1 = "Supply of Equipment";
            childSheet.column_7_l2_c1 = "Foreign Supply";
            childSheet.column_7_l2_c2 = "Local Supply";
            childSheet.column_7_l3 = "CIF Thai Port";
            childSheet.column_7_l3_c2 = "Ex-works Price";
            childSheet.column_7_l4_c2 = "( excluding VAT )";
            childSheet.column_7_l5_c2 = "Baht";
            childSheet.column_7_l6_c1_1 = "Unit Price";
            childSheet.column_7_l6_c1_2 = "Amount";
            childSheet.column_7_l6_c2_1 = "Unit Price";
            childSheet.column_7_l6_c2_1 = "Amount";
            //childSheet.column_8_l1 = "Local Transportation, Construction and Installation";

            if ((part.ToUpper().StartsWith("D") == true) || (part.ToUpper().StartsWith("_D") == true))
                childSheet.column_8_l1 = "Local Transportation";
            else
                childSheet.column_8_l1 = "Local Transportation,  Construction and  Installation";

            childSheet.column_8_l4 = "( excluding VAT )";
            childSheet.column_8_l5 = "Baht";
            childSheet.column_8_l6_c1 = "Unit Price";
            childSheet.column_8_l6_c2 = "Amount";


            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_4_l1 = "Baht";
            childSheet.footer_5_l1 = "Baht";



            worksheet.Cells["EE1"].Value = this.key;
            worksheet.Column(135).Hidden = true;

            worksheet.Cells["A1:L1"].Merge = true;
            worksheet.Cells["A1:L1"].Value = ("INVITATION TO BID NO. " + childSheet.header_l1_bidno).ToUpper();
            worksheet.Cells["A1:L1"].Style.Font.Bold = true;
            worksheet.Cells["A1:L1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:L2"].Merge = true;
            worksheet.Cells["A2:L2"].Value = (ScheNo + part + " : " + childSheet.header_l2_PartName).ToUpper();
            worksheet.Cells["A2:L2"].Style.Font.Bold = true;
            worksheet.Cells["A2:L2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:L3"].Merge = true;
            worksheet.Cells["A3:L3"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// childSheet.header_l3_biddesc;
            worksheet.Cells["A3:L3"].Style.Font.Bold = true;
            worksheet.Cells["A3:L3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:L4"].Merge = true;
            worksheet.Cells["A4:L4"].Value = (childSheet.header_l4_project).ToUpper();
            worksheet.Cells["A4:L4"].Style.Font.Bold = true;
            worksheet.Cells["A4:L4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //header Colunm

            worksheet.Cells["A5:A10"].Merge = true;
            worksheet.Cells["A5:A10"].Value = childSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B5:B10"].Merge = true;
            worksheet.Cells["B5:B10"].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["C5:C10"].Merge = true;
            worksheet.Cells["C5:C10"].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["C5:C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C5:C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C5:C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.WrapText = true;

            worksheet.Cells["D5:D10"].Merge = true;
            worksheet.Cells["D5:D10"].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["D5:D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D5:D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["E5:E10"].Merge = true;
            worksheet.Cells["E5:E10"].Value = childSheet.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["E5:E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["E5:E10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["E5:E10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F5:F10"].Merge = true;
            worksheet.Cells["F5:F10"].Value = childSheet.column_6_l1;
            worksheet.Cells["F5:F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F5:F10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F5:F10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G5:J5"].Merge = true;
            worksheet.Cells["G5:J5"].Value = childSheet.column_7_l1;
            worksheet.Cells["G5:J5"].Style.Font.Bold = true;
            worksheet.Cells["G5:J5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G5:J5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G5:J5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:J5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:J5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:J5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G6:H6"].Merge = true;
            worksheet.Cells["G6:H6"].Value = childSheet.column_7_l2_c1;
            // worksheet.Cells["F6:G6"].Style.Font.Bold = true;
            worksheet.Cells["G6:H6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G6:H6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G6:H6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G6:H6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G6:H6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G6:H6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["I6:J6"].Merge = true;
            worksheet.Cells["I6:J6"].Value = childSheet.column_7_l2_c2;
            // worksheet.Cells["H6:I6"].Style.Font.Bold = true;
            worksheet.Cells["I6:J6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I6:J6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["I6:J6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I6:J6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I6:J6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I6:J6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G7:H9"].Merge = true;
            worksheet.Cells["G7:H9"].Value = childSheet.column_7_l3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["G7:H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G7:H9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G7:H9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:H9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:H9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:H9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:H9"].Style.WrapText = true;


            worksheet.Cells["I7:J7"].Merge = true;
            worksheet.Cells["I7:J7"].Value = childSheet.column_7_l3_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I7:J7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I7:J7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["I7:J7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I7:J7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I7:J7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I7:J7"].Style.WrapText = true;

            worksheet.Cells["I8:J8"].Merge = true;
            worksheet.Cells["I8:J8"].Value = childSheet.column_7_l4_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I8:J8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I8:J8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            //  worksheet.Cells["H8:I8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I8:J8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I8:J8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I8:J8"].Style.WrapText = true;

            worksheet.Cells["I9:J9"].Merge = true;
            worksheet.Cells["I9:J9"].Value = childSheet.column_7_l5_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I9:J9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I9:J9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            //worksheet.Cells["H9:I9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I9:J9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I9:J9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I9:J9"].Style.WrapText = true;

            worksheet.Cells["G10"].Value = childSheet.column_7_l6_c1_1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["H10"].Value = childSheet.column_7_l6_c1_2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["H10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["I10"].Value = childSheet.column_7_l6_c1_1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["I10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["J10"].Value = childSheet.column_7_l6_c1_2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["J10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["J10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["K5:L7"].Merge = true;
            worksheet.Cells["K5:L7"].Value = childSheet.column_8_l1;
            worksheet.Cells["K5:L7"].Style.Font.Bold = true;
            worksheet.Cells["K5:L7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K5:L7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K5:L7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K5:L7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K5:L7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K5:L7"].Style.WrapText = true;

            worksheet.Cells["K8:L8"].Merge = true;
            worksheet.Cells["K8:L8"].Value = childSheet.column_8_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K8:L8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K8:L8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K8:L8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K8:L8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K8:L8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K8:L8"].Style.WrapText = true;

            worksheet.Cells["K9:L9"].Merge = true;
            worksheet.Cells["K9:L9"].Value = childSheet.column_8_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K9:L9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K9:L9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.WrapText = true;

            worksheet.Cells["K10"].Value = childSheet.column_8_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["L10"].Value = childSheet.column_8_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["L10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["L10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["L10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            //data
            bool haveData = false;
            ps_xls_template xls_temp = new ps_xls_template();

            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;

                foreach (DataRow item in dt_eq.Rows)
                {
                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = ScheNo + item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["eq_design"].ToString().Replace("|", Environment.NewLine);
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "F" + (numRow).ToString();
                    //worksheet.Cells[strCell].Value = item["currency"];
                    //worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid; 
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    var unitmeasure = worksheet.DataValidations.AddListValidation(strCell);
                    DataTable dtCurrency = this.getCurrency();
                    foreach (DataRow drCurrency in dtCurrency.Rows)
                    {
                        unitmeasure.Formula.Values.Add(drCurrency["CURRENCY_CODE"].ToString());
                    }

                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "H" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(D" + numRow + ",2)*ROUND(G" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "I" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "J" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(D" + numRow + ",2)*ROUND(I" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "K" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "L" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(D" + numRow + ",2)*ROUND(K" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Substation1";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);

                    numRow++;
                }

            }
            int startRow = 0;
            int endRow = 0;
            if (haveData)
            {
                startRow = 11;
            }
            else
            {
                startRow = 10;
            }

            endRow = numRow - 1;
            //footer
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "E" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1 + " ";// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            string Formula = "";

            for (int k = 0; k <= 5; k++)
            {
                strCell = "F" + (numRow).ToString();
                LogHelper.Write(strCell);
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;

                    //Formula = "IF(UNIQUE(F" + startRow + ":F" + endRow + ") = 0, \"\",UNIQUE(F" + startRow + ":F" + endRow + "))";
                    // =IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12)),"", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12))
                    Formula = "IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + ")),\"\", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + "))";
                    worksheet.Cells[strCell].Formula = Formula;

                    worksheet.Cells[strCell].Calculate();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                else if (k == 1)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;

                    //Formula = "IF(UNIQUE(F" + startRow + ":F" + endRow + ") = 0, \"\",UNIQUE(F" + startRow + ":F" + endRow + "))";
                    // =IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12)),"", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12))
                    string strCell_F_Start = "F" + (numRow - k).ToString();
                    string strCell_F_End = "F" + (numRow - 1).ToString();
                    Formula = "IF(ISERROR(LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + ")),\"\", LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + "))";
                    worksheet.Cells[strCell].Formula = Formula;

                    worksheet.Cells[strCell].Calculate();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                else if (k == 2)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;

                    //Formula = "IF(UNIQUE(F" + startRow + ":F" + endRow + ") = 0, \"\",UNIQUE(F" + startRow + ":F" + endRow + "))";
                    // =IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12)),"", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12))
                    string strCell_F_Start = "F" + (numRow - k).ToString();
                    string strCell_F_End = "F" + (numRow - 1).ToString();
                    Formula = "IF(ISERROR(LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + ")),\"\", LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + "))";
                    worksheet.Cells[strCell].Formula = Formula;

                    worksheet.Cells[strCell].Calculate();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                else if (k == 3)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;

                    //Formula = "IF(UNIQUE(F" + startRow + ":F" + endRow + ") = 0, \"\",UNIQUE(F" + startRow + ":F" + endRow + "))";
                    // =IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12)),"", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12))
                    string strCell_F_Start = "F" + (numRow - k).ToString();
                    string strCell_F_End = "F" + (numRow - 1).ToString();
                    Formula = "IF(ISERROR(LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + ")),\"\", LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + "))";
                    worksheet.Cells[strCell].Formula = Formula;

                    worksheet.Cells[strCell].Calculate();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                else if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;

                    //Formula = "IF(UNIQUE(F" + startRow + ":F" + endRow + ") = 0, \"\",UNIQUE(F" + startRow + ":F" + endRow + "))";
                    // =IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12)),"", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12))
                    string strCell_F_Start = "F" + (numRow - k).ToString();
                    string strCell_F_End = "F" + (numRow - 1).ToString();
                    Formula = "IF(ISERROR(LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + ")),\"\", LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + "))";
                    worksheet.Cells[strCell].Formula = Formula;

                    worksheet.Cells[strCell].Calculate();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                else if (k == 5)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                    //Formula = "IF(UNIQUE(F" + startRow + ":F" + endRow + ") = 0, \"\",UNIQUE(F" + startRow + ":F" + endRow + "))";
                    // =IF(ISERROR(LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12)),"", LOOKUP(2,1/(COUNTIF($F$1:$F1,$F$11:$F$12&"")=0),$F$11:$F$12))
                    string strCell_F_Start = "F" + (numRow - k).ToString();
                    string strCell_F_End = "F" + (numRow - 1).ToString();
                    Formula = "IF(ISERROR(LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + ")),\"\", LOOKUP(2,1/(COUNTIF(" + strCell_F_Start + ":" + strCell_F_End + ",$F$" + startRow + ":$F$" + endRow + ")=0),$F$" + startRow + ":$F$" + endRow + "))";
                    worksheet.Cells[strCell].Formula = Formula;

                    worksheet.Cells[strCell].Calculate();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }

                string strCell_F = "$F" + (numRow).ToString();
                strCell = "G" + (numRow).ToString() + ":" + "H" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;

                //=SUMIFS($C$2:$C$13,$B$2:$B$13,B15)
                Formula = "=SUMIFS($H$" + startRow + ":$H$" + endRow + ",$F$" + startRow + ":$F$" + endRow + "," + strCell_F + ")";
                worksheet.Cells[strCell].Formula = Formula;
                worksheet.Cells[strCell].Calculate();
                worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                }
                else if (k == 1)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                }
                else if (k == 2)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                }
                else if (k == 3)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                }
                else if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                }
                else if (k == 5)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "I" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 5)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "J" + (numRow).ToString();
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(J" + startRow + ":J" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 5)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "K" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (k == 5)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                //if (k == 1)
                //{
                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //}

                strCell = "L" + (numRow).ToString();
                //worksheet.Cells[strCell].Merge = true;
                //worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                // worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(L" + startRow + ":L" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 5)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                numRow++;
            }

            if (part_remark != "")
            {

                numRow++;
                //string remark = "";
                //if(dt_eq.Rows[0]["part_remark"] != null)
                //{
                //    remark = dt_eq.Rows[0]["part_remark"].ToString();
                //}
                strCell = "A" + (numRow).ToString() + ":" + "L" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Value = "Remark : " + part_remark;
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            return worksheet;
        }
        #endregion

        #region " Set Sheet Substation Main and Detail Part Type 2"
        public OfficeOpenXml.ExcelWorksheet setSheetPartSub2(OfficeOpenXml.ExcelWorksheet wk, List<DataRow> listPart, DataTable dtPart, string part)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            DataTable dtt = getBtnPaths(BidNo, SchName, part);
            if (dtt.Rows.Count > 0)
            {
                partDesc = dtt.Rows[0]["part_description"].ToString();
            }

            //sum sheet
            //set Width 
            worksheet.Column(1).Width = 60;
            worksheet.Column(2).Width = 17;

            //set wrap 
            //wrap text in the cells
            // worksheet.Column(5).Style.WrapText = true;



            XLSPS_PartSheet partSheet = new XLSPS_PartSheet();
            //header
            partSheet.header_l1_bidno = BidNo;
            partSheet.header_l2_PartName = "SUPPLY AND INSTALLATION OF  SUBSTATION EQUIPMENT";
            partSheet.header_l3_biddesc = bidDesc;
            partSheet.header_l4_project = project;

            partSheet.column_1_l1 = "Description";
            partSheet.column_2_l1 = "Local Currency";
            partSheet.column_2_l4 = "( excluding VAT )";
            partSheet.column_2_l5 = "Baht";
            partSheet.column_2_l6 = "Amount";

            partSheet.footer_1_l1 = "PART " + ScheNo + part;
            partSheet.footer_3_l1_c1 = "Baht";
            partSheet.footer_3_l1_c2 = "Baht";
            partSheet.footer_4_l1 = "Baht";
            partSheet.footer_5_l1 = "Baht";

            worksheet.Cells["A1:B1"].Merge = true;
            worksheet.Cells["A1:B1"].Value = ("INVITATION TO BID NO. " + partSheet.header_l1_bidno).ToUpper();
            worksheet.Cells["A1:B1"].Style.Font.Bold = true;
            worksheet.Cells["A1:B1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:B2"].Merge = true;
            worksheet.Cells["A2:B2"].Value = ("PART " + ScheNo + part + " : " + partSheet.header_l2_PartName).ToUpper();
            worksheet.Cells["A2:B2"].Style.Font.Bold = true;
            worksheet.Cells["A2:B2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:B3"].Merge = true;
            worksheet.Cells["A3:B3"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// partSheet.header_l3_biddesc;
            worksheet.Cells["A3:B3"].Style.Font.Bold = true;
            worksheet.Cells["A3:B3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:B4"].Merge = true;
            worksheet.Cells["A4:B4"].Value = (partSheet.header_l4_project).ToUpper();
            worksheet.Cells["A4:B4"].Style.Font.Bold = true;
            worksheet.Cells["A4:B4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;


            //header Colunm

            worksheet.Cells["A5:A10"].Merge = true;
            worksheet.Cells["A5:A10"].Value = partSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B5:B7"].Merge = true;
            worksheet.Cells["B5:B7"].Value = partSheet.column_2_l1;
            worksheet.Cells["B5:B7"].Style.Font.Bold = true;
            worksheet.Cells["B5:B7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B5:B7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["B5:B7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["E5:E7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B7"].Style.WrapText = true;

            //worksheet.Cells["D8"].Merge = true;
            worksheet.Cells["B8"].Value = partSheet.column_2_l4;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["B8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["B8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            //worksheet.Cells["D9"].Merge = true;
            worksheet.Cells["B9"].Value = partSheet.column_2_l5;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["B9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["B9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            //worksheet.Cells["C10"].Merge = true;
            worksheet.Cells["B10"].Value = partSheet.column_2_l6;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;





            //data
            DataTable dt_eq = new DataTable();
            //foreach (DataRow dr in dtPart.Rows)
            //{
            //    dt_eq = objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);
            //    /// listEq.Add(dt_eq);
            //}

            //var resultsPartAB = dtPart.Select("SUBSTRING(part_name,1,2) like '%AB%'and  LEN(part_name) > 1 ");
            //var resultsPartD = dtPart.Select("SUBSTRING(part_name,1,1) like '%D%' and LEN(part_name) > 1 ");
            //var resultsPart_D = dtPart.Select("SUBSTRING(part_name,1,2) like '%_d%' and  LEN(part_name) > 1 ");
            //var resultsPartC = dtPart.Select("SUBSTRING(part_name,1,1) like '%C%' and  LEN(part_name) > 1 ");

            decimal ForeignAmount = 0;
            decimal LocalAmount = 0;
            decimal LocalCurAmount = 0;
            decimal LocalTranAmount = 0;
            decimal LocalTranConAmount = 0;
            bool nodata = false;
            if (listPart.Count > 0)
            {

                foreach (var item in listPart)
                {
                    int countRow = 0;
                    dt_eq = getGVSelPart(Dept, Sect, BidNo, SchName, item["part_name"].ToString(), JobNo, JobRev);
                    if (dt_eq != null)
                    {
                        countRow = dt_eq.Rows.Count;
                    }

                    for (int i = 0; i <= 4; i++)
                    {
                        if (i == 0)
                        {
                            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 4).ToString();
                            worksheet.Cells[strCell].Merge = true;
                            worksheet.Cells[strCell].Value = "Schedule " + ScheNo + item["part_name"].ToString() + " : " + getPartName(item["part_name"].ToString(), "ss");
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                            worksheet.Cells[strCell].Style.WrapText = true;
                        }


                        strCell = "B" + (numRow).ToString();
                        //if (i != 4)
                        //{
                        //    worksheet.Cells[strCell].Formula = "='" + item[0].ToString() + "'" + "!J" + (countRow + 11 + i);
                        //}
                        // worksheet.Cells[strCell].Value = Part.LocalAmount == "0" ? "" : Part.LocalAmount;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Formula = "='" + item["part_name"].ToString() + "'" + "!G" + (countRow + 12);
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;



                        numRow++;
                    }

                }
            }
            else
            {
                nodata = true;
            }



            //footer
            string spec = " ";

            //footer
            int startRow = 0;
            int endRow = 0;
            if (nodata)
            {
                startRow = 11;
            }
            else
            {
                startRow = 10;
            }

            endRow = numRow - 1;
            string Formula = "";
            for (int k = 0; k < 5; k++)
            {
                if (k == 0)
                {
                    strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 4).ToString();
                    worksheet.Cells[strCell].Merge = true;
                    worksheet.Cells[strCell].Value = partSheet.footer_1_l1;
                    worksheet.Cells[strCell].Style.Font.Bold = true;
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    //set border
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }


                strCell = "B" + (numRow).ToString();
                // worksheet.Cells[strCell].Value = partSheet.footer_3_l1_c2 + spec + (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                // worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Font.Bold = true;
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(B" + startRow + ":B" + endRow + ")";
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }



                numRow++;

            }
            return worksheet;

        }
        public OfficeOpenXml.ExcelWorksheet setSheetChildSubstationFormat2(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            partDesc = part_description;

            //string ItemPart = nameSheet.Substring(0, 1) == "_" ? "1" + nameSheet : "1" + nameSheet;


            //listChild = listChild.Where(c => c.Name == nameSheet).ToList();
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 30;
            worksheet.Column(6).Width = 15;
            worksheet.Column(7).Width = 15;
            worksheet.Column(8).Width = 15;
            worksheet.Column(9).Width = 15;
            worksheet.Column(10).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = partDesc;
            childSheet.header_l3_biddesc = bidDesc;
            childSheet.header_l4_project = project;

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Drawing No./ Reference No.";
            childSheet.column_4_l1 = "Qty.";
            childSheet.column_5_l1 = "Unit";
            childSheet.column_6_l1 = "Local Currency";
            childSheet.column_6_l4 = "( excluding VAT )";
            childSheet.column_6_l5 = "Baht";
            childSheet.column_6_l6_c1 = "Unit Price";
            childSheet.column_6_l6_c2 = "Amount";


            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_6_l1 = "Baht";



            worksheet.Cells["EE1"].Value = this.key;
            worksheet.Column(135).Hidden = true;

            worksheet.Cells["A1:G1"].Merge = true;
            worksheet.Cells["A1:G1"].Value = ("INVITATION TO BID NO. " + childSheet.header_l1_bidno).ToUpper();
            worksheet.Cells["A1:G1"].Style.Font.Bold = true;
            worksheet.Cells["A1:G1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:G2"].Merge = true;
            worksheet.Cells["A2:G2"].Value = (ScheNo + part + " : " + childSheet.header_l2_PartName).ToUpper();
            worksheet.Cells["A2:G2"].Style.Font.Bold = true;
            worksheet.Cells["A2:G2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:G3"].Merge = true;
            worksheet.Cells["A3:G3"].Value = this.getScopeofWork(BidNo, BidRev, SchName).ToUpper();// childSheet.header_l3_biddesc;
            worksheet.Cells["A3:G3"].Style.Font.Bold = true;
            worksheet.Cells["A3:G3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:G4"].Merge = true;
            worksheet.Cells["A4:G4"].Value = (childSheet.header_l4_project).ToUpper();
            worksheet.Cells["A4:G4"].Style.Font.Bold = true;
            worksheet.Cells["A4:G4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //header Colunm

            worksheet.Cells["A5:A10"].Merge = true;
            worksheet.Cells["A5:A10"].Value = childSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B5:B10"].Merge = true;
            worksheet.Cells["B5:B10"].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["C5:C10"].Merge = true;
            worksheet.Cells["C5:C10"].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["C5:C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C5:C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C5:C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.WrapText = true;

            worksheet.Cells["D5:D10"].Merge = true;
            worksheet.Cells["D5:D10"].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["D5:D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D5:D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["E5:E10"].Merge = true;
            worksheet.Cells["E5:E10"].Value = childSheet.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["E5:E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["E5:E10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["E5:E10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F5:G7"].Merge = true;
            worksheet.Cells["F5:G7"].Value = childSheet.column_6_l1;
            worksheet.Cells["F5:G7"].Style.Font.Bold = true;
            worksheet.Cells["F5:G7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F5:G7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F5:G7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:G7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:G7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:G7"].Style.WrapText = true;

            worksheet.Cells["F8:G8"].Merge = true;
            worksheet.Cells["F8:G8"].Value = childSheet.column_6_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["F8:G8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F8:G8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F8:G8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F8:G8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F8:G8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F8:G8"].Style.WrapText = true;

            worksheet.Cells["F9:G9"].Merge = true;
            worksheet.Cells["F9:G9"].Value = childSheet.column_6_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["F9:G9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F9:G9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:G9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:G9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:G9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:G9"].Style.WrapText = true;

            worksheet.Cells["F10"].Value = childSheet.column_6_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["G10"].Value = childSheet.column_6_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;


            //data

            bool haveData = false;
            ps_xls_template xls_temp = new ps_xls_template();

            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;

                foreach (DataRow item in dt_eq.Rows)
                {

                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = ScheNo + item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["eq_design"].ToString().Replace("|", Environment.NewLine);
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "F" + (numRow).ToString();
                    //worksheet.Cells[strCell].Value = item["currency"].ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);



                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(F" + numRow + ",2)*ROUND(D" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Substation2";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);



                    numRow++;
                }

            }

            int startRow = 0;
            int endRow = 0;
            if (haveData)
            {
                startRow = 11;
            }
            else
            {
                startRow = 10;
            }

            endRow = numRow - 1;

            //footer
            strCell = "A" + (numRow).ToString() + ":" + "E" + (numRow + 1).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1 + " ";// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            for (int k = 0; k < 2; k++)
            {
                strCell = "F" + (numRow).ToString();

                worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = childSheet.footer_6_l1;// "Total Price For Schedule";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                strCell = "G" + (numRow).ToString();
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                numRow++;
            }
            if (part_remark != "")
            {

                numRow++;
                //string remark = "";
                //if (dt_eq.Rows[0]["part_remark"] != null)
                //{
                //    remark = dt_eq.Rows[0]["part_remark"].ToString();
                //}
                strCell = "A" + (numRow).ToString() + ":" + "G" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Value = "Remark : " + part_remark;
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            return worksheet;
        }
        #endregion

        #region " Set Sheet Line Detail Part"
        //Set Format for Part A
        public OfficeOpenXml.ExcelWorksheet setSheetChildLineFormat1(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            string ItemPart = "";
            bool haveIm = false;
            bool haveData = false;
            int startRow = 0;
            int endRow = 0;
            partDesc = part_description;
            //listChild = listChild.Where(c => c.Name == nameSheet).ToList();
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 15;
            worksheet.Column(4).Width = 15;
            worksheet.Column(5).Width = 15;
            worksheet.Column(6).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = "SCHEDULE NO. " + ScheNo;
            childSheet.header_l3_biddesc = ScheNo + part + " : " + this.getPartName(part, "ln"); //partDesc;
            childSheet.header_l4_project = "IMPORTANT :";

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Estimated Qty.";
            childSheet.column_4_l1 = "Unit";
            childSheet.column_5_l1 = "Local Transportation, Construction and Installation";
            childSheet.column_5_l4 = "( excluding VAT )";
            childSheet.column_5_l6_c1 = "Unit Price";
            childSheet.column_5_l6_c2 = "Amount";


            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_5_l1 = "Baht";

            worksheet.Cells["A1:K1"].Merge = true;
            worksheet.Cells["A1:K1"].Value = ("INVITATION TO BID NO. " + BidNo).ToUpper();
            worksheet.Cells["A1:K1"].Style.Font.Bold = true;
            worksheet.Cells["A1:K1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:K2"].Merge = true;
            worksheet.Cells["A2:K2"].Value = ("SCHEDULE NO. " + ScheNo).ToUpper();
            worksheet.Cells["A2:K2"].Style.Font.Bold = true;
            worksheet.Cells["A2:K2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:K3"].Merge = true;
            worksheet.Cells["A3:K3"].Value = (ScheNo + part + " : " + this.getPartName(part, "ln")).ToUpper();
            worksheet.Cells["A3:K3"].Style.Font.Bold = true;
            worksheet.Cells["A3:K3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:K4"].Merge = true;
            worksheet.Cells["A4:K4"].Value = "IMPORTANT :";
            worksheet.Cells["A4:K4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            numRow = 5;

            strCell = "A" + (numRow).ToString() + ":" + "K" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = "      " + important;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[strCell].Style.WrapText = true;

            numRow++;

            //header Colunm
            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "B" + (numRow).ToString() + ":" + "B" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "C" + (numRow).ToString() + ":" + "C" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;


            strCell = "D" + (numRow).ToString() + ":" + "D" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "E" + (numRow).ToString() + ":" + "F" + (numRow + 2).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "E" + (numRow + 3).ToString() + ":" + "F" + (numRow + +3).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "E" + (numRow + 4).ToString() + ":" + "F" + (numRow + +4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "E" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_5_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "F" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_5_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;


            //data
            startRow = numRow + 6;
            ps_xls_template xls_temp = new ps_xls_template();
            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;
                numRow = numRow + 6;
                foreach (DataRow item in dt_eq.Rows)
                {

                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = ScheNo + item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);


                    strCell = "F" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(C" + numRow + ",2)*ROUND(E" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Substation1";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);

                    numRow++;
                }

            }

            //if (!haveData)
            //{
            //    endRow = startRow;
            //}

            endRow = numRow - 1;
            //footer
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "D" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1;// + " " + ItemPart;// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            for (int k = 0; k < 2; k++)
            {
                strCell = "E" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = childSheet.footer_5_l1;// "";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "F" + (numRow).ToString();
                if (k == 0)
                {

                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(F" + startRow + ":F" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                }
            }

            if (part_remark != "")
            {

                numRow++;
                //string remark = "";
                //if (dt_eq.Rows[0]["part_remark"] != null)
                //{
                //    remark = dt_eq.Rows[0]["part_remark"].ToString();
                //}
                strCell = "A" + (numRow).ToString() + ":" + "K" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Value = "Remark : " + part_remark;
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            return worksheet;
        }
        //Set Format for Part B, C, D, E, F, G, H, I, J, K, O
        public OfficeOpenXml.ExcelWorksheet setSheetChildLineFormat2(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            bool haveData = false;
            int coutRowTop = 0;
            int startRow = 0;
            int endRow = 0;
            partDesc = part_description;
            worksheet.Protection.IsProtected = true;
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 15;
            worksheet.Column(4).Width = 15;
            worksheet.Column(5).Width = 15;
            worksheet.Column(6).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = ScheNo + part + " : " + partDesc;
            childSheet.header_l3_biddesc = bidDesc;
            childSheet.header_l4_project = "IMPORTANT :";

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Estimated Qty.";
            childSheet.column_4_l1 = "Unit";
            childSheet.column_5_l1 = "Currency";
            childSheet.column_6_l1 = "Supply of Equipment";
            childSheet.column_6_l2 = "Foreign Supply";
            childSheet.column_6_l3 = "CIF Thai Port";
            childSheet.column_6_l6_c1 = "Unit Price";
            childSheet.column_6_l6_c2 = "Amount";
            childSheet.column_7_l1 = "Supply of Equipment";
            childSheet.column_7_l2 = "Local Supply";
            childSheet.column_7_l3 = "Ex-works Price";
            childSheet.column_7_l4 = "( excluding VAT )";
            childSheet.column_7_l5 = "Baht";
            childSheet.column_7_l6_c1 = "Unit Price";
            childSheet.column_7_l6_c2 = "Amount";
            childSheet.column_8_l1 = "Local Transportation,  Construction and  Installation";
            childSheet.column_8_l4 = "( excluding VAT )";
            childSheet.column_8_l5 = "Baht";
            childSheet.column_8_l6_c1 = "Unit Price";
            childSheet.column_8_l6_c2 = "Amount";



            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_7_l1 = "Baht";
            childSheet.footer_8_l1 = "Baht";
            childSheet.footer_9_l1 = "Baht";

            worksheet.Cells["A1:K1"].Merge = true;
            worksheet.Cells["A1:K1"].Value = (ScheNo + part + " : " + this.getPartName(part, "ln")).ToUpper();
            worksheet.Cells["A1:K1"].Style.Font.Bold = true;
            worksheet.Cells["A1:K1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:K2"].Merge = true;
            worksheet.Cells["A2:K2"].Value = "IMPORTANT :";
            worksheet.Cells["A2:K2"].Style.Font.Bold = true;
            worksheet.Cells["A2:K2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;


            numRow = 3;
            strCell = "A" + (numRow).ToString() + ":" + "K" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = "      " + important;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[strCell].Style.WrapText = true;

            numRow++;

            coutRowTop = numRow;
            //header Colunm
            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "B" + (numRow).ToString() + ":" + "B" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            strCell = "C" + (numRow).ToString() + ":" + "C" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "D" + (numRow).ToString() + ":" + "D" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "E" + (numRow).ToString() + ":" + "E" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            strCell = "F" + (numRow).ToString() + ":" + "G" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_6_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "F" + (numRow + 1).ToString() + ":" + "G" + (numRow + 1).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_6_l2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "F" + (numRow + 2).ToString() + ":" + "G" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_6_l3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "F" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_6_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "G" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_6_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "H" + (numRow).ToString() + ":" + "I" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 1).ToString() + ":" + "I" + (numRow + 1).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 2).ToString() + ":" + "I" + (numRow + 2).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 3).ToString() + ":" + "I" + (numRow + 3).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 4).ToString() + ":" + "I" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_7_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "I" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_7_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "J" + (numRow).ToString() + ":" + "K" + (numRow + 2).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_8_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "J" + (numRow + 3).ToString() + ":" + "K" + (numRow + 3).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_8_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "J" + (numRow + 4).ToString() + ":" + "K" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_8_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "J" + (numRow + 5).ToString();
            // worksheet.Cells["J10"].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_8_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "K" + (numRow + 5).ToString();
            //worksheet.Cells["H8:I8"].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_8_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;


            //data
            ps_xls_template xls_temp = new ps_xls_template();
            startRow = numRow + 6;
            if (dt_eq.Rows.Count > 0)
            {
                numRow = numRow + 6;

                haveData = true;
                foreach (DataRow item in dt_eq.Rows)
                {


                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = ScheNo + item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "F" + (numRow).ToString();
                    //worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(C" + numRow + ",2)*ROUND(F" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "H" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    //worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "I" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(C" + numRow + ",2)*ROUND(H" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "J" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    //worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "K" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(C" + numRow + ",2)*ROUND(J" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    endRow = numRow;

                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Substation1";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);

                    numRow++;

                }

            }


            endRow = numRow - 1;


            //footer
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "E" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1;// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            //for (int k = 0; k < 5; k++)
            //{
            //strCell = "E" + (numRow).ToString();
            ////worksheet.Cells[strCell].Value = childSheet.footer_5_l1;// "";
            //worksheet.Cells[strCell].Style.Font.Bold = true;
            //worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "F" + (numRow).ToString();
            worksheet.Cells[strCell].Value = childSheet.footer_7_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "G" + (numRow).ToString();
            worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")";
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

            strCell = "H" + (numRow).ToString();
            worksheet.Cells[strCell].Value = childSheet.footer_8_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "I" + (numRow).ToString();
            worksheet.Cells[strCell].Formula = "SUM(I" + startRow + ":I" + endRow + ")";
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

            strCell = "J" + (numRow).ToString();
            worksheet.Cells[strCell].Value = childSheet.footer_9_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "K" + (numRow).ToString();
            worksheet.Cells[strCell].Formula = "SUM(K" + startRow + ":K" + endRow + ")";
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
            //}

            if (part_remark != "")
            {

                numRow++;
                //string remark = "";
                //if (dt_eq.Rows[0]["part_remark"] != null)
                //{
                //    remark = dt_eq.Rows[0]["part_remark"].ToString();
                //}
                strCell = "A" + (numRow).ToString() + ":" + "K" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Value = "Remark : " + part_remark;
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }


            return worksheet;
        }
        //Set Format for Part N
        public OfficeOpenXml.ExcelWorksheet setSheetChildLineFormat3(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            bool haveData = false;
            int startRow = 0;
            int endRow = 0;
            partDesc = part_description;
            worksheet.Protection.IsProtected = true;
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 15;
            worksheet.Column(4).Width = 15;
            worksheet.Column(5).Width = 15;
            worksheet.Column(6).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();


            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = ScheNo + part + " : " + partDesc;
            childSheet.header_l3_biddesc = bidDesc;
            childSheet.header_l4_project = "IMPORTANT :";

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Estimated Qty.";
            childSheet.column_4_l1 = "Unit";
            childSheet.column_5_l1 = "Currency";
            childSheet.column_6_l1 = "Foreign Currency";
            childSheet.column_6_l6_c1 = "Unit Price";
            childSheet.column_6_l6_c2 = "Amount";
            childSheet.column_7_l1 = "Foreign Currency";
            childSheet.column_7_l4 = "( excluding VAT )";
            childSheet.column_7_l5 = "Baht";
            childSheet.column_7_l6_c1 = "Unit Price";
            childSheet.column_7_l6_c2 = "Amount";


            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_7_l1 = "Baht";
            childSheet.footer_8_l1 = "Baht";


            worksheet.Cells["A1:K1"].Merge = true;
            worksheet.Cells["A1:K1"].Value = (ScheNo + part + " : " + this.getPartName(part, "ln")).ToUpper();
            worksheet.Cells["A1:K1"].Style.Font.Bold = true;
            worksheet.Cells["A1:K1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:K2"].Merge = true;
            worksheet.Cells["A2:K2"].Value = "IMPORTANT :";
            worksheet.Cells["A2:K2"].Style.Font.Bold = true;
            worksheet.Cells["A2:K2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            numRow = 3;
            strCell = "A" + (numRow).ToString() + ":" + "J" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = important;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[strCell].Style.WrapText = true;

            numRow++;

            //header Colunm
            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_1_l1;
            //worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "B" + (numRow).ToString() + ":" + "B" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "C" + (numRow).ToString() + ":" + "C" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "D" + (numRow).ToString() + ":" + "D" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "E" + (numRow).ToString() + ":" + "E" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "F" + (numRow).ToString() + ":" + "G" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_6_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "F" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_6_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "G" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_6_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "H" + (numRow).ToString() + ":" + "I" + (numRow + 2).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 3).ToString() + ":" + "I" + (numRow + 3).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 4).ToString() + ":" + "I" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_7_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "H" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_7_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "I" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_7_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;



            //data
            ps_xls_template xls_temp = new ps_xls_template();
            startRow = numRow + 6;

            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;
                numRow = numRow + 6;
                foreach (DataRow item in dt_eq.Rows)
                {


                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = ScheNo + item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "F" + (numRow).ToString();
                    //worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(C" + numRow + ",2)*ROUND(F" + numRow +",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "H" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    //worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "I" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "=ROUND(C" + numRow + ",2)*ROUND(H" + numRow + ",2)";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
                    worksheet.Cells[strCell].Style.WrapText = true;

                    //strCell = "J" + (numRow).ToString();
                    //worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    //worksheet.Cells[strCell].Style.Locked = false;
                    ////worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    //worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //worksheet.Cells[strCell].Style.WrapText = true;
                    ////worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    //strCell = "K" + (numRow).ToString();
                    //worksheet.Cells[strCell].Formula = "=C" + numRow + "*J" + numRow;
                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    //worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    //worksheet.Cells[strCell].Style.WrapText = true;

                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Substation1";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);

                    numRow++;
                }

            }
            endRow = numRow - 1;

            //footer
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "E" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1;// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "F" + (numRow).ToString();
            worksheet.Cells[strCell].Value = childSheet.footer_7_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "G" + (numRow).ToString();
            worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")";
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

            strCell = "H" + (numRow).ToString();
            worksheet.Cells[strCell].Value = childSheet.footer_8_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "I" + (numRow).ToString();
            worksheet.Cells[strCell].Formula = "SUM(I" + startRow + ":I" + endRow + ")";
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";

            if (part_remark != "")
            {
                numRow++;
                //string remark = "";
                //if (dt_eq.Rows[0]["part_remark"] != null)
                //{
                //    remark = dt_eq.Rows[0]["part_remark"].ToString();
                //}
                strCell = "A" + (numRow).ToString() + ":" + "J" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Value = "Remark : " + part_remark;
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            return worksheet;
        }
        //Set Format for Part Z
        public OfficeOpenXml.ExcelWorksheet setSheetChildLineFormat4(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            bool haveData = false;
            int startRow = 0;
            int endRow = 0;
            partDesc = part_description;
            worksheet.Protection.IsProtected = true;

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 15;
            worksheet.Column(4).Width = 15;
            worksheet.Column(5).Width = 15;
            worksheet.Column(6).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = ScheNo + part + " : " + partDesc;
            childSheet.header_l3_biddesc = bidDesc;
            childSheet.header_l4_project = "IMPORTANT :";

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Estimated Qty.";
            childSheet.column_4_l1 = "Unit";
            childSheet.column_5_l1 = "Purchase Price Offered for Dismantled Equipment";
            childSheet.column_5_l4 = "( including VAT )";
            childSheet.column_5_l5 = "Baht";
            childSheet.column_5_l6_c1 = "Unit Price";
            childSheet.column_5_l6_c2 = "Amount";


            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_5_l1 = "Baht";


            worksheet.Cells["A1:K1"].Merge = true;
            worksheet.Cells["A1:K1"].Value = (ScheNo + part + " : " + this.getPartName(part, "ln")).ToUpper();
            worksheet.Cells["A1:K1"].Style.Font.Bold = true;
            worksheet.Cells["A1:K1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:K2"].Merge = true;
            worksheet.Cells["A2:K2"].Value = "IMPORTANT :";
            worksheet.Cells["A2:K2"].Style.Font.Bold = true;
            worksheet.Cells["A2:K2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            numRow = 3;

            strCell = "A" + (numRow).ToString() + ":" + "K" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = important;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[strCell].Style.WrapText = true;

            numRow++;

            //header Colunm
            strCell = "A" + (numRow).ToString() + ":" + "A" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_1_l1;
            //worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "B" + (numRow).ToString() + ":" + "B" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "C" + (numRow).ToString() + ":" + "C" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "D" + (numRow).ToString() + ":" + "D" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "E" + (numRow).ToString() + ":" + "F" + (numRow + 2).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l1;
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "E" + (numRow + 3).ToString() + ":" + "F" + (numRow + 3).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "E" + (numRow + 4).ToString() + ":" + "F" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.column_5_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.WrapText = true;

            strCell = "E" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_5_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            strCell = "F" + (numRow + 5).ToString();
            worksheet.Cells[strCell].Value = childSheet.column_5_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;


            //data
            ps_xls_template xls_temp = new ps_xls_template();
            startRow = numRow + 6;

            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;
                numRow = numRow + 6;
                foreach (DataRow item in dt_eq.Rows)
                {
                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Top;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);


                    strCell = "F" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Substation1";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString()); ;
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);

                    numRow++;
                }

            }

            endRow = numRow - 1;

            //footer
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "D" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1;// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "E" + (numRow).ToString();
            worksheet.Cells[strCell].Value = childSheet.footer_5_l1;// "";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "F" + (numRow).ToString();
            worksheet.Cells[strCell].Value = "";// Get Sum Total Equip Service
            worksheet.Cells[strCell].Style.Numberformat.Format = "#,##0.00";
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;

            if (part_remark != "")
            {

                numRow++;
                //string remark = "";
                //if (dt_eq.Rows[0]["part_remark"] != null)
                //{
                //    remark = dt_eq.Rows[0]["part_remark"].ToString();
                //}
                strCell = "A" + (numRow).ToString() + ":" + "K" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                worksheet.Cells[strCell].Value = "Remark : " + part_remark;
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            }

            return worksheet;
        }
        #endregion

        #region " Set Sheet Supply and BreakDown "
        public OfficeOpenXml.ExcelWorksheet setSheetChildSupplyFormat1(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description, string part_remark, string important)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            string ItemPart = "";


            //listChild = listChild.Where(c => c.Name == nameSheet).ToList();
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(3).Width = 15;
            worksheet.Column(6).Width = 15;
            worksheet.Column(7).Width = 15;
            worksheet.Column(8).Width = 15;
            worksheet.Column(9).Width = 15;
            worksheet.Column(10).Width = 15;
            worksheet.Column(11).Width = 15;
            worksheet.Column(12).Width = 15;
            worksheet.Column(13).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = part_description;
            childSheet.header_l3_biddesc = "SUPPlY OF " + BidNo;
            childSheet.header_l4_project = project;

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Drawing No. / Reference No.";
            childSheet.column_4_l1 = "Qty.";
            childSheet.column_5_l1 = "Unit";
            childSheet.column_6_l1 = "Currency";




            childSheet.column_7_l1 = "Supply of Equipment";
            childSheet.column_7_l2 = "Foreign Supply";
            childSheet.column_7_l3_c1 = "FOB Vessel";
            childSheet.column_7_l3_c2 = "Cost of Transportation to Thai Port";
            childSheet.column_7_l3_c3 = "CFR Thai Port";
            childSheet.column_7_l6_c1 = "Unit Price";
            childSheet.column_7_l6_c2 = "Unit Price";
            childSheet.column_7_l6_c3_1 = "Unit Price";
            childSheet.column_7_l6_c3_2 = "Amount";

            childSheet.column_8_l1 = "Supply of Equipment";
            childSheet.column_8_l2 = "";
            childSheet.column_8_l3 = "DDP EGAT's Store";
            childSheet.column_8_l4 = "( excluding VAT )";
            childSheet.column_8_l5 = "Baht";
            childSheet.column_8_l6_c1 = "Unit Price";
            childSheet.column_8_l6_c2 = "Amount";


            childSheet.column_9_l1 = "Supply of Equipment";
            childSheet.column_9_l2 = "Local Supply";
            childSheet.column_9_l3 = "Ex-works Price";
            childSheet.column_9_l4 = "( excluding VAT )";
            childSheet.column_9_l5 = "Baht";
            childSheet.column_9_l6_c1 = "Unit Price";
            childSheet.column_9_l6_c2 = "Amount";

            childSheet.column_10_l1 = "Foreign Currency";
            childSheet.column_10_l4 = "";
            childSheet.column_10_l5 = "";
            childSheet.column_10_l6_c1 = "Unit Price";
            childSheet.column_10_l6_c2 = "Amount";

            childSheet.column_11_l1 = "Local Currency";
            childSheet.column_11_l4 = "( excluding VAT )";
            childSheet.column_11_l5 = "Baht";
            childSheet.column_11_l6_c1 = "Unit Price";
            childSheet.column_11_l6_c2 = "Amount";


            childSheet.column_12_l1 = "Local Transportation";
            childSheet.column_12_l4 = "( excluding VAT )";
            childSheet.column_12_l5 = "Baht";
            childSheet.column_12_l6_c1 = "Unit Price";
            childSheet.column_12_l6_c2 = "Amount";

            childSheet.footer_1_l1 = "Total Price for " + ScheNo + part;
            childSheet.footer_5_l1 = "Baht";


            worksheet.Cells["A1:T1"].Merge = true;
            worksheet.Cells["A1:T1"].Value = "INVITATION TO BID NO. " + childSheet.header_l1_bidno;
            worksheet.Cells["A1:T1"].Style.Font.Bold = true;
            worksheet.Cells["A1:T1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:T2"].Merge = true;
            worksheet.Cells["A2:T2"].Value = part + " : " + childSheet.header_l2_PartName;
            worksheet.Cells["A2:T2"].Style.Font.Bold = true;
            worksheet.Cells["A2:T2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:T3"].Merge = true;
            worksheet.Cells["A3:T3"].Value = childSheet.header_l3_biddesc;
            worksheet.Cells["A3:T3"].Style.Font.Bold = true;
            worksheet.Cells["A3:T3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:T4"].Merge = true;
            worksheet.Cells["A4:T4"].Value = childSheet.header_l4_project;
            worksheet.Cells["A4:T4"].Style.Font.Bold = true;
            worksheet.Cells["A4:T4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //header Colunm

            worksheet.Cells["A5:A10"].Merge = true;
            worksheet.Cells["A5:A10"].Value = childSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B5:B10"].Merge = true;
            worksheet.Cells["B5:B10"].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["C5:C10"].Merge = true;
            worksheet.Cells["C5:C10"].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["C5:C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C5:C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C5:C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.WrapText = true;

            worksheet.Cells["D5:D10"].Merge = true;
            worksheet.Cells["D5:D10"].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["D5:D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D5:D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["E5:E10"].Merge = true;
            worksheet.Cells["E5:E10"].Value = childSheet.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["E5:E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["E5:E10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["E5:E10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F5:F10"].Merge = true;
            worksheet.Cells["F5:F10"].Value = childSheet.column_6_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["F5:F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F5:F10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F5:F10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G5:J5"].Merge = true;
            worksheet.Cells["G5:J5"].Value = childSheet.column_7_l1;
            worksheet.Cells["G5:J5"].Style.Font.Bold = true;
            worksheet.Cells["G5:J5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G5:J5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G5:J5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:J5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:J5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:J5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G6:J6"].Merge = true;
            worksheet.Cells["G6:J6"].Value = childSheet.column_7_l2;
            // worksheet.Cells["F6:G6"].Style.Font.Bold = true;
            worksheet.Cells["G6:J6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G6:J6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G6:J6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G6:J6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G6:J6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G6:J6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;



            worksheet.Cells["G7:G9"].Merge = true;
            worksheet.Cells["G7:G9"].Value = childSheet.column_7_l3_c1;
            // worksheet.Cells["H6:I6"].Style.Font.Bold = true;
            worksheet.Cells["G7:G9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G7:G9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G7:G9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:G9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:G9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G7:G9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["H7:H9"].Merge = true;
            worksheet.Cells["H7:H9"].Value = childSheet.column_7_l3_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["H7:H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["H7:H9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H7:H9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:H9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:H9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:H9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:H9"].Style.WrapText = true;


            worksheet.Cells["I7:J9"].Merge = true;
            worksheet.Cells["I7:J9"].Value = childSheet.column_7_l3_c3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I7:J9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I7:J9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["I7:J9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I7:J9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I7:J9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I7:J9"].Style.WrapText = true;

            worksheet.Cells["G10"].Value = childSheet.column_7_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["H10"].Value = childSheet.column_7_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["H10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["I10"].Value = childSheet.column_7_l6_c3_1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["I10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["J10"].Value = childSheet.column_7_l6_c3_2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["J10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["J10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["K5:L5"].Merge = true;
            worksheet.Cells["K5:L5"].Value = childSheet.column_8_l1;
            worksheet.Cells["K5:L5"].Style.Font.Bold = true;
            worksheet.Cells["K5:L5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K5:L5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K5:L5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K5:L5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K5:L5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K5:L5"].Style.WrapText = true;

            worksheet.Cells["K6:L6"].Merge = true;
            worksheet.Cells["K6:L6"].Value = childSheet.column_8_l2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K6:L6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K6:L6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K6:L6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K6:L6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K6:L6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K6:L6"].Style.WrapText = true;

            worksheet.Cells["K7:L7"].Merge = true;
            worksheet.Cells["K7:L7"].Value = childSheet.column_8_l3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K7:L7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K7:L7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K7:L7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K7:L7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K7:L7"].Style.WrapText = true;

            worksheet.Cells["K8:L8"].Merge = true;
            worksheet.Cells["K8:L8"].Value = childSheet.column_8_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K8:L8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K8:L8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K8:L8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K8:L8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K8:L8"].Style.WrapText = true;

            worksheet.Cells["K9:L9"].Merge = true;
            worksheet.Cells["K9:L9"].Value = childSheet.column_8_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K9:L9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K9:L9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:L9"].Style.WrapText = true;

            worksheet.Cells["K10"].Value = childSheet.column_8_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["L10"].Value = childSheet.column_8_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["L10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["L10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["L10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["M5:N5"].Merge = true;
            worksheet.Cells["M5:N5"].Value = childSheet.column_9_l1;
            worksheet.Cells["M5:N5"].Style.Font.Bold = true;
            worksheet.Cells["M5:N5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["M5:N5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["M5:N5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M5:N5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M5:N5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M5:N5"].Style.WrapText = true;

            worksheet.Cells["M6:N6"].Merge = true;
            worksheet.Cells["M6:N6"].Value = childSheet.column_9_l2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["M6:N6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["M6:N6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["M6:N6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M6:N6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M6:N6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M6:N6"].Style.WrapText = true;

            worksheet.Cells["M7:N7"].Merge = true;
            worksheet.Cells["M7:N7"].Value = childSheet.column_9_l3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["M7:N7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["M7:N7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M7:N7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M7:N7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M7:N7"].Style.WrapText = true;

            worksheet.Cells["M8:N8"].Merge = true;
            worksheet.Cells["M8:N8"].Value = childSheet.column_9_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["M8:N8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["M8:N8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M8:N8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M8:N8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M8:N8"].Style.WrapText = true;

            worksheet.Cells["M9:N9"].Merge = true;
            worksheet.Cells["M9:N9"].Value = childSheet.column_9_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["M9:N9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["M9:N9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M9:N9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M9:N9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M9:N9"].Style.WrapText = true;

            worksheet.Cells["M10"].Value = childSheet.column_9_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["M10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["M10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["M10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["M10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["N10"].Value = childSheet.column_9_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["N10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["N10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["N10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["N10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["N10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["N10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["O5:P7"].Merge = true;
            worksheet.Cells["O5:P7"].Value = childSheet.column_10_l1;
            worksheet.Cells["O5:P7"].Style.Font.Bold = true;
            worksheet.Cells["O5:P7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["O5:P7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["O5:P7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O5:P7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O5:P7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O5:P7"].Style.WrapText = true;

            worksheet.Cells["O8:P8"].Merge = true;
            worksheet.Cells["O8:P8"].Value = childSheet.column_10_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["O8:P8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["O8:P8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O8:P8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O8:P8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O8:P8"].Style.WrapText = true;

            worksheet.Cells["O9:P9"].Merge = true;
            worksheet.Cells["O9:P9"].Value = childSheet.column_10_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["O9:P9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["O9:P9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O9:P9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O9:P9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O9:P9"].Style.WrapText = true;

            worksheet.Cells["O10"].Value = childSheet.column_10_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["O10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["O10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["O10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["O10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["P10"].Value = childSheet.column_10_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["P10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["P10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["P10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["P10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["P10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["P10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["Q5:R7"].Merge = true;
            worksheet.Cells["Q5:R7"].Value = childSheet.column_11_l1;
            worksheet.Cells["Q5:R7"].Style.Font.Bold = true;
            worksheet.Cells["Q5:R7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["Q5:R7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["Q5:R7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q5:R7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q5:R7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q5:R7"].Style.WrapText = true;

            worksheet.Cells["Q8:R8"].Merge = true;
            worksheet.Cells["Q8:R8"].Value = childSheet.column_11_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["Q8:R8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["Q8:R8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q8:R8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q8:R8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q8:R8"].Style.WrapText = true;

            worksheet.Cells["Q9:R9"].Merge = true;
            worksheet.Cells["Q9:R9"].Value = childSheet.column_11_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["Q9:R9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["Q9:R9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q9:R9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q9:R9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q9:R9"].Style.WrapText = true;

            worksheet.Cells["Q10"].Value = childSheet.column_11_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["Q10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["Q10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["Q10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["Q10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["R10"].Value = childSheet.column_11_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["R10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["R10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["R10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["R10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["R10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["R10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["S5:T7"].Merge = true;
            worksheet.Cells["S5:T7"].Value = childSheet.column_12_l1;
            worksheet.Cells["S5:T7"].Style.Font.Bold = true;
            worksheet.Cells["S5:T7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["S5:T7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["S5:T7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S5:T7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S5:T7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S5:T7"].Style.WrapText = true;

            worksheet.Cells["S8:T8"].Merge = true;
            worksheet.Cells["S8:T8"].Value = childSheet.column_12_l4;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["S8:T8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["S8:T8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S8:T8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S8:T8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S8:T8"].Style.WrapText = true;

            worksheet.Cells["S9:T9"].Merge = true;
            worksheet.Cells["S9:T9"].Value = childSheet.column_12_l5;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["S9:T9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["S9:T9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S9:T9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S9:T9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S9:T9"].Style.WrapText = true;

            worksheet.Cells["S10"].Value = childSheet.column_12_l6_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["S10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["S10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["S10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["S10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;

            worksheet.Cells["T10"].Value = childSheet.column_12_l6_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["T10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["T10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["T10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["T10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["T10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["T10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            //data
            bool haveData = false;
            ps_xls_template xls_temp = new ps_xls_template();

            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;
                foreach (DataRow item in dt_eq.Rows)
                {

                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = "";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["unit"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "F" + (numRow).ToString();
                    //worksheet.Cells[strCell].Value = item["currency"].ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    var unitmeasure = worksheet.DataValidations.AddListValidation(strCell);
                    DataTable dtCurrency = this.getCurrency();
                    foreach (DataRow drCurrency in dtCurrency.Rows)
                    {
                        unitmeasure.Formula.Values.Add(drCurrency["CURRENCY_CODE"].ToString());
                    }
                    //unitmeasure.Formula.Values.Add("USD");
                    //unitmeasure.Formula.Values.Add("EUR");
                    //unitmeasure.Formula.Values.Add("JPY");
                    //unitmeasure.Formula.Values.Add("CNY");
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);


                    strCell = "H" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);


                    strCell = "I" + (numRow).ToString();
                    worksheet.Cells[strCell].Formula = "SUM(G" + numRow + ":H" + numRow + ")";
                    //worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "J" + (numRow).ToString();
                    //worksheet.Cells[strCell].Value = item.LocalCurUnit == "0" ? "" : item.LocalCurUnit;
                    worksheet.Cells[strCell].Formula = "=D" + numRow + "*I" + numRow;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "K" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;
                    //worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(157, 225, 224, 1);

                    strCell = "L" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Formula = "=D" + numRow + "*K" + numRow;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "M" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "N" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Formula = "=D" + numRow + "*M" + numRow;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "O" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "P" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Formula = "=D" + numRow + "*O" + numRow;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "Q" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "R" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Formula = "=D" + numRow + "*Q" + numRow;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "S" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 153, 255, 255);
                    worksheet.Cells[strCell].Style.Locked = false;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "T" + (numRow).ToString();
                    // worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
                    worksheet.Cells[strCell].Formula = "=D" + numRow + "*S" + numRow;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    xls_temp = new ps_xls_template();
                    xls_temp.template_id = key;
                    xls_temp.schedule_name = SchName;
                    xls_temp.bid_no = BidNo;
                    xls_temp.bid_revision = BidRev;
                    xls_temp.type = "Supply";
                    xls_temp.item_no = item["equip_item_no"].ToString();
                    xls_temp.description = item["equip_full_desc"].ToString();
                    xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString()); ;
                    xls_temp.unit = item["unit"].ToString();
                    xls_temp.part = part;
                    xls_temp.equip_code = item["equip_code"].ToString();
                    listXls_temp.Add(xls_temp);


                    numRow++;
                }

            }
            //footer





            int startRow = 0;
            int endRow = 0;
            if (haveData)
            {
                startRow = 11;
            }
            else
            {
                startRow = 10;
            }

            endRow = numRow - 1;
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "E" + (numRow + 4).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = childSheet.footer_1_l1 + " " + ItemPart;// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            string Formula = "";
            for (int k = 0; k < 5; k++)
            {
                strCell = "F" + (numRow).ToString();
                if (k == 0)
                {

                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    //Formula = "IF(COUNTA(F" + startRow + ":F" + endRow + ")>0,UNIQUE(SORT(FILTER(F" + startRow + ":F" + endRow + ",F" + startRow + ":F" + endRow + "<>\"\"),1,-1)),\"\"" + ")";
                    //Formula = "IF(COUNTA(F" + startRow + ":F" + endRow + ")>0,\""+"A"+"\",\"\"" + ")";
                    worksheet.Cells[strCell].Value = "USD";
                    //worksheet.Cells[strCell].Calculate();
                }
                if (k == 1)
                {
                    worksheet.Cells[strCell].Value = "EUR";
                }
                if (k == 2)
                {
                    worksheet.Cells[strCell].Value = "JPY";
                }
                if (k == 3)
                {
                    worksheet.Cells[strCell].Value = "CNY";
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }



                strCell = "G" + (numRow).ToString() + ":" + "J" + (numRow).ToString();
                worksheet.Cells[strCell].Merge = true;
                Formula = "IF(F" + numRow + "=\"\",\"\",SUMIF($F$" + startRow + ":$F$" + endRow + ",$F" + numRow + ",$H$" + startRow + ":$H$" + endRow + "))";
                //worksheet.Cells[strCell].Formula = "SUM(H" + startRow + ":H" + endRow + ")";
                worksheet.Cells[strCell].Formula = Formula;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }


                strCell = "K" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "L" + (numRow).ToString();
                //worksheet.Cells[strCell].Merge = true;
                //worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                //worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(L" + startRow + ":L" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "M" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "N" + (numRow).ToString();
                //worksheet.Cells[strCell].Merge = true;
                //worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                //worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(N" + startRow + ":N" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }



                strCell = "O" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "P" + (numRow).ToString();
                //worksheet.Cells[strCell].Merge = true;
                //worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                //worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(P" + startRow + ":P" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "Q" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "R" + (numRow).ToString();
                //worksheet.Cells[strCell].Merge = true;
                //worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                //worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(Q" + startRow + ":R" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "S" + (numRow).ToString();
                if (k == 0)
                {
                    worksheet.Cells[strCell].Value = "Baht";
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Font.Bold = true;
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCell = "T" + (numRow).ToString();
                //worksheet.Cells[strCell].Merge = true;
                //worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
                //worksheet.Cells[strCell].Style.Font.Bold = true;
                if (k == 1)
                {
                    worksheet.Cells[strCell].Formula = "SUM(T" + startRow + ":T" + endRow + ")";
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                if (k == 0)
                {
                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                if (k == 4)
                {
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
                numRow++;
            }


            return worksheet;
        }
        public OfficeOpenXml.ExcelWorksheet setSheetBreakDown(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part)
        {

            var worksheet = wk;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 12;
            XLSPS_BreakdownSheet breakDn = new XLSPS_BreakdownSheet();


            breakDn.header_l1_bidno = BidNo;
            breakDn.header_l2_PartName = "Part F1 : Breaksown Price";
            breakDn.header_l3_biddesc = "for Schedule 1AB35 : Communication  Cable";
            breakDn.header_l4_project = "SUPPLY AND CONSTRUCTION OF 115 KV PHACHUK SUBSTATION";
            breakDn.header_l5_project = "TRANSMISSION SYSTEM FOR PHACHUK HYDROPOWER PROJECT";

            breakDn.column_1_l1 = "Item No.";
            breakDn.column_2_l1 = "Description";
            breakDn.column_3_l1 = "Qty.";
            breakDn.column_4_l1 = "Unit";
            breakDn.column_5_l1 = "Currency";
            breakDn.column_6_l1 = "Supply of Equipment";
            breakDn.column_6_l2_c1 = "Foreign Supply";
            breakDn.column_6_l2_c2 = "Local Supply";
            breakDn.column_6_l3 = "CIF Thai Port";
            breakDn.column_6_l3_c2 = "Ex-works Price";
            breakDn.column_6_l4_c2 = "( excluding VAT )";
            breakDn.column_6_l5_c2 = "Baht";
            breakDn.column_6_l6_c1_1 = "Unit Price";
            breakDn.column_6_l6_c1_2 = "Amount";
            breakDn.column_6_l6_c2_1 = "Unit Price";
            breakDn.column_6_l6_c2_2 = "Amount";
            breakDn.column_7_l1 = "Local Transportation, Construction and";
            breakDn.column_7_l4 = "( excluding VAT )";
            breakDn.column_7_l5 = "Baht";
            breakDn.column_7_l6_c1 = "Unit Price";
            breakDn.column_7_l6_c2 = "Amount";

            breakDn.footer_1_l1 = "";
            breakDn.footer_3_l1_c2 = "";
            breakDn.footer_4_l1 = "";
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(6).Width = 10;
            worksheet.Column(7).Width = 10;
            worksheet.Column(8).Width = 10;
            worksheet.Column(9).Width = 10;
            worksheet.Column(10).Width = 10;
            worksheet.Column(11).Width = 10;

            //set wrap 
            //wrap text in the cells
            // worksheet.Column(9).Style.WrapText = true;

            worksheet.Cells["A1:K1"].Merge = true;
            worksheet.Cells["A1:K1"].Value = "INVITATION TO BID NO. " + breakDn.header_l1_bidno;
            worksheet.Cells["A1:K1"].Style.Font.Bold = true;
            worksheet.Cells["A1:K1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:K2"].Merge = true;
            worksheet.Cells["A2:K2"].Value = "Schedule 2 : " + breakDn.header_l2_PartName;
            worksheet.Cells["A2:K2"].Style.Font.Bold = true;
            worksheet.Cells["A2:K2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:K3"].Merge = true;
            worksheet.Cells["A3:K3"].Value = "for Schedule 1AB33 : " + breakDn.header_l3_biddesc;
            worksheet.Cells["A3:K3"].Style.Font.Bold = true;
            worksheet.Cells["A3:K3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:K4"].Merge = true;
            worksheet.Cells["A4:K4"].Value = breakDn.header_l4_project;
            worksheet.Cells["A4:K4"].Style.Font.Bold = true;
            worksheet.Cells["A4:K4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A5:K5"].Merge = true;
            worksheet.Cells["A5:K5"].Value = breakDn.header_l5_project;
            worksheet.Cells["A5:K5"].Style.Font.Bold = true;
            worksheet.Cells["A5:K5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //header Colunm

            worksheet.Cells["A6:A11"].Merge = true;
            worksheet.Cells["A6:A11"].Value = breakDn.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A6:A11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A6:A11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A6:A11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A6:A11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A6:A11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A6:A11"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B6:B11"].Merge = true;
            worksheet.Cells["B6:B11"].Value = breakDn.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["B6:B11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B6:B11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;

            //set border
            worksheet.Cells["B6:B11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B6:B11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B6:B11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B6:B11"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["C6:C11"].Merge = true;
            worksheet.Cells["C6:C11"].Value = breakDn.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["C6:C11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C6:C11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C6:C11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C6:C11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C6:C11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C6:C11"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["D6:D11"].Merge = true;
            worksheet.Cells["D6:D11"].Value = breakDn.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["D6:D11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D6:D11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D6:D11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D6:D11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D6:D11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D6:D11"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["E6:E11"].Merge = true;
            worksheet.Cells["E6:E11"].Value = breakDn.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["E6:E11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["E6:E11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["E6:E11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E6:E11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E6:E11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E6:E11"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F6:I6"].Merge = true;
            worksheet.Cells["F6:I6"].Value = breakDn.column_6_l1;
            worksheet.Cells["F6:I6"].Style.Font.Bold = true;
            worksheet.Cells["F6:I6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F6:I6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F6:I6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F6:I6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F6:I6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F7:G7"].Merge = true;
            worksheet.Cells["F7:G7"].Value = breakDn.column_6_l2_c1;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["F7:G7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F7:G7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F7:G7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F7:G7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F7:G7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["H7:I7"].Merge = true;
            worksheet.Cells["H7:I7"].Value = breakDn.column_6_l2_c2;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["H7:I7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H7:I7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:I7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H7:I7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["F8:G10"].Merge = true;
            worksheet.Cells["F8:G10"].Value = breakDn.column_6_l3;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["F8:G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F8:G10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["F8:G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["H8:I8"].Merge = true;
            worksheet.Cells["H8:I8"].Value = breakDn.column_6_l3_c2;
            //worksheet.Cells["J7:K7"].Style.Font.Bold = true;
            worksheet.Cells["H8:I8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H8:I8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["H9:I9"].Merge = true;
            worksheet.Cells["H9:I9"].Value = breakDn.column_6_l4_c2;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["H9:I9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H9:I9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["H10:I10"].Merge = true;
            worksheet.Cells["H10:I10"].Value = breakDn.column_6_l5_c2;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["H10:I10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["H10:I10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["H10:I10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["F11"].Merge = true;
            worksheet.Cells["F11"].Value = breakDn.column_6_l6_c1_1;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["F11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["F11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G11"].Merge = true;
            worksheet.Cells["G11"].Value = breakDn.column_6_l6_c1_2;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["G11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["G11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["H11"].Merge = true;
            worksheet.Cells["H11"].Value = breakDn.column_6_l6_c2_1;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["H11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["H11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["H11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["I11"].Merge = true;
            worksheet.Cells["I11"].Value = breakDn.column_6_l6_c2_2;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["I11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["I11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["J6:K8"].Merge = true;
            worksheet.Cells["J6:K8"].Value = breakDn.column_7_l1;
            worksheet.Cells["J6:K8"].Style.Font.Bold = true;
            worksheet.Cells["J6:K8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J6:K8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["J6:K8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J6:K8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J6:K8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J6:K8"].Style.WrapText = true;

            worksheet.Cells["J9:K9"].Merge = true;
            worksheet.Cells["J9:K9"].Value = breakDn.column_7_l4;
            //worksheet.Cells["E6:F6"].Style.Font.Bold = true;
            worksheet.Cells["J9:K9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["J9:K9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["J10:K10"].Merge = true;
            worksheet.Cells["J10:K10"].Value = breakDn.column_7_l5;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["J10:K10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J10:K10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["J10:K10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["J11"].Merge = true;
            worksheet.Cells["J11"].Value = breakDn.column_7_l6_c1;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["J11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["J11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["K11"].Merge = true;
            worksheet.Cells["K11"].Value = breakDn.column_7_l6_c2;
            //worksheet.Cells["E6:E6"].Style.Font.Bold = true;
            worksheet.Cells["K11"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K11"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["K11"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K11"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K11"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            //data
            decimal ForeignUnit = 0;
            decimal ForeignAmount = 0;
            decimal LocalUnit = 0;
            decimal LocalAmount = 0;
            decimal LocalCurUnit = 0;
            decimal LocalCurAmount = 0;
            decimal LocalTranUnit = 0;
            decimal LocalTranAmount = 0;
            decimal LocalTranConUnit = 0;
            decimal LocalTranConAmount = 0;

            //if (listBD.Count > 0)
            //{
            //    string itemno = "";
            //    if (listBD[0].ItemNo.Length > 0)
            //    {
            //        itemno = listBD[0].ItemNo.Substring(0, listBD[0].ItemNo.IndexOf("-"));
            //    }
            //    breakDn.footer_1_l1 = breakDn.footer_1_l1 + itemno;

            //    foreach (var item in listBD)
            //    {

            //        strCell = "A" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.ItemNo;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.WrapText = true;

            //        strCell = "B" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.Des;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.WrapText = true;

            //        strCell = "C" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.Qty;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.WrapText = true;

            //        strCell = "D" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.Unit;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.WrapText = true;


            //        strCell = "E" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.Currency;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.WrapText = true;

            //        strCell = "F" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.ForeignUnit == "0" ? "" : item.ForeignUnit;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //        worksheet.Cells[strCell].Style.WrapText = true;
            //        if (item.ForeignUnit != "")
            //        {
            //            ForeignUnit += Convert.ToDecimal(item.ForeignUnit);
            //        }

            //        strCell = "G" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.ForeignAmount == "0" ? "" : item.ForeignAmount;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //        worksheet.Cells[strCell].Style.WrapText = true;
            //        if (item.ForeignAmount != "")
            //        {
            //            ForeignAmount += Convert.ToDecimal(item.ForeignAmount);
            //        }

            //        strCell = "H" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.LocalUnit == "0" ? "" : item.LocalUnit;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //        worksheet.Cells[strCell].Style.WrapText = true;
            //        if (item.LocalUnit != "")
            //        {
            //            LocalUnit += Convert.ToDecimal(item.LocalUnit);
            //        }

            //        strCell = "I" + (numRow).ToString();
            //        worksheet.Cells[strCell].Value = item.LocalAmount == "0" ? "" : item.LocalAmount;
            //        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //        worksheet.Cells[strCell].Style.WrapText = true;
            //        if (item.LocalAmount != "")
            //        {
            //            LocalAmount += Convert.ToDecimal(item.LocalAmount);
            //        }

            //        if (item.LocalCurAmount != "0")
            //        {
            //            strCell = "J" + (numRow).ToString();
            //            worksheet.Cells[strCell].Value = item.LocalCurUnit == "0" ? "" : item.LocalCurUnit;
            //            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //            worksheet.Cells[strCell].Style.WrapText = true;
            //            if (item.LocalCurUnit != "")
            //            {
            //                LocalCurUnit += Convert.ToDecimal(item.LocalCurUnit);
            //            }

            //            strCell = "K" + (numRow).ToString();
            //            worksheet.Cells[strCell].Value = item.LocalCurAmount == "0" ? "" : item.LocalCurAmount;
            //            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //            worksheet.Cells[strCell].Style.WrapText = true;
            //            if (item.LocalCurAmount != "")
            //            {
            //                LocalCurAmount += Convert.ToDecimal(item.LocalCurAmount);
            //            }
            //        }
            //        if (item.LocalTranAmount != "0")
            //        {
            //            strCell = "J" + (numRow).ToString();
            //            worksheet.Cells[strCell].Value = item.LocalTranUnit == "0" ? "" : item.LocalTranUnit;
            //            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //            worksheet.Cells[strCell].Style.WrapText = true;
            //            if (item.LocalTranUnit != "")
            //            {
            //                LocalTranUnit += Convert.ToDecimal(item.LocalTranUnit);
            //            }

            //            strCell = "K" + (numRow).ToString();
            //            worksheet.Cells[strCell].Value = item.LocalTranAmount == "0" ? "" : item.LocalTranAmount;
            //            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //            worksheet.Cells[strCell].Style.WrapText = true;
            //            if (item.LocalTranAmount != "")
            //            {
            //                LocalTranAmount += Convert.ToDecimal(item.LocalTranAmount);
            //            }
            //        }
            //        if (item.LocalTranConAmount != "0")
            //        {
            //            strCell = "J" + (numRow).ToString();
            //            worksheet.Cells[strCell].Value = item.LocalTranConUnit == "0" ? "" : item.LocalTranConUnit;
            //            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //            worksheet.Cells[strCell].Style.WrapText = true;
            //            if (item.LocalTranConUnit != "")
            //            {
            //                LocalTranConUnit += Convert.ToDecimal(item.LocalTranConUnit);
            //            }

            //            strCell = "K" + (numRow).ToString();
            //            worksheet.Cells[strCell].Value = item.LocalTranConAmount == "0" ? "" : item.LocalTranConAmount;
            //            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
            //            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            //            worksheet.Cells[strCell].Style.WrapText = true;
            //            if (item.LocalTranConAmount != "")
            //            {
            //                LocalTranConAmount += Convert.ToDecimal(item.LocalTranConAmount);
            //            }

            //            numRow++;
            //        }

            //    }

            //}

            //footer
            string spec = " ";
            strCell = "A" + (numRow).ToString() + ":" + "D" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = breakDn.footer_1_l1;// "Total Price For Schedule";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            //set border
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "E" + (numRow).ToString();
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "F" + (numRow).ToString() + ":" + "G" + (numRow).ToString();
            worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "H" + (numRow).ToString();
            worksheet.Cells[strCell].Value = "Baht";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "I" + (numRow).ToString();
            //worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = (LocalAmount.ToString() == "0" ? "" : LocalAmount.ToString());
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            strCell = "J" + (numRow).ToString();
            worksheet.Cells[strCell].Value = "Baht";
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            decimal lastAmount = 0;

            if (LocalCurAmount.ToString() != "0")
            {
                lastAmount = LocalCurAmount;
            }
            else if (LocalTranAmount.ToString() != "0")
            {
                lastAmount = LocalTranAmount;
            }
            else if (LocalTranConAmount.ToString() != "0")
            {
                lastAmount = LocalTranConAmount;
            }

            strCell = "K" + (numRow).ToString();
            //worksheet.Cells[strCell].Merge = true;
            worksheet.Cells[strCell].Value = (lastAmount.ToString() == "0" ? "" : lastAmount.ToString());
            worksheet.Cells[strCell].Style.Font.Bold = true;
            worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            return worksheet;
        }
        #endregion

        private string delete_ps_xls_template()
        {

            string result = "success";
            DataTable dtRunno = new DataTable();
            string cmdstrFull = "";
            string cmdstr = "";
            string condi = "";
            string cmdvalue = "";
            try
            {

                cmdstrFull = "";
                cmdstr = "";
                condi = "";


                cmdstr = @"delete ps_xls_template ";

                cmdvalue = "";
                condi = "where schedule_name = '" + SchName + "' and bid_no = '" + BidNo + "' and bid_revision ='" + BidRev + "'";
                cmdstrFull = cmdstr + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;

                return result;
            }



            return result;
        }
        private void insertps_xls_template()
        {
            DataTable dt = new DataTable();

            //xls_temp.schedule_name = SchName;
            //xls_temp.bid_no = BidNo;
            //xls_temp.bid_revision = rev;
            //xls_temp.type = "Substation1";
            //xls_temp.item_no = item["equip_item_no"].ToString();
            //xls_temp.description = item["equip_full_desc"].ToString();
            //xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());;
            //xls_temp.unit = item["unit"].ToString();

            foreach (var item in listXls_temp)
            {
                string sql = "";
                try
                {
                    sql = @"INSERT INTO ps_xls_template
           ([template_id]
           ,[schedule_name]
           ,[bid_no]
           ,[bid_revision]
           ,[type]
           ,[item_no]
           ,[description]
           ,[qty]
           ,[unit]
           ,[part]
           ,[equip_code]
           ,[gen_datetime]
           ,[created_by]
           ,[created_datetime]
           ,[updated_by]
           ,[updated_datetime])
     VALUES
           ('" + item.template_id + @"'
           ,'" + item.schedule_name.Replace("'", "''").Trim() + @"'
           ,'" + item.bid_no + @"'
           ,'" + item.bid_revision + @"'
           ,'" + item.type + @"'
           ,'" + item.item_no + @"'
           ,'" + item.description.Replace("'", "''").Trim() + @"'
           ,'" + item.qty + @"'
           ,'" + item.unit + @"'
           ,'" + item.part + @"'
           ,'" + item.equip_code + @"'
           ,GETDATE()
           ,'" + empID + @"'
           ,GETDATE()
           ,'" + empID + @"'
           ,GETDATE());";

                    zdbUtil.ExecNonQuery(sql, psDB);

                }
                catch (Exception ex)
                {

                    LogHelper.WriteEx(ex);
                }
            }

        }
        #endregion

        #region " genExcel SumTotal "
        public string genExcelSumTotal(string BidNo, List<string> listFileName)
        {
            string reult = "";
            // If you are a commercial business and have
            // purchased commercial licenses use the static property
            // LicenseContext of the ExcelPackage class:
            //ExcelPackage.LicenseContext = LicenseContext.Commercial;

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            DataTable dt_eq = new DataTable();
            bool result = false;
            var package = new ExcelPackage();
            FileName = "" + BidNo;
            this.BidNo = BidNo;
            //string user = empID;
            //FileName = FileName + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            string format = "";
            var workbook = package.Workbook;
            var worksheet = workbook.Worksheets.Add("Total Price");
            // worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
            worksheet.PrinterSettings.FitToPage = true;
            DataTable dttt = new DataTable();
            // listFileNmae = new List<string>();
            //listFileName.Add("TS12-S-02 _25640402_161036491.xlsx");
            worksheet.Protection.IsProtected = true;
            try
            {
                worksheet = setSheetTotalPrice(worksheet, listFileName);

                // package.Workbook.Calculate();
                FileName = FileName + ".xlsx";
                package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));

                ////delete_ps_xls_template();
                //insertps_xls_template();
                //listXls_temp.Clear();
                // uploadData(1, new FileInfo("C:\\Xls\\" + FileName), empID);
            }
            catch (Exception ex)
            {

            }
            return reult;
        }
        public OfficeOpenXml.ExcelWorksheet setSheetTotalPrice(OfficeOpenXml.ExcelWorksheet wk, List<string> listFileName)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = false;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            //string ItemPart = nameSheet.Substring(0, 1) == "_" ? "1" + nameSheet : "1" + nameSheet;
            string bidDesc = "";
            string FullPaht = "";
            string format = "";
            string part = "";
            if (listFileName.Count > 0)
            {
                foreach (var itemF in listFileName)
                {
                    FullPaht = "C:\\Xls\\" + itemF + "";
                    using (var package = new ExcelPackage(new FileInfo(FullPaht)))
                    {
                        var workbook = package.Workbook;

                        foreach (var ws in workbook.Worksheets)
                        {
                            string sheetName = ws.Name;

                            if (sheetName.Equals("SumAll"))
                            {
                                //string a = worksheet.Cells["A11"].Value.ToString();
                                for (int r = 1; r < ws.Dimension.Rows; r++)
                                {
                                    strCell = "A" + (r).ToString();
                                    if (r == 2)
                                    {
                                        int index = ws.Cells[strCell].Value.ToString().IndexOf(':');

                                        if (bidDesc == "")
                                        {
                                            bidDesc = "SUPPLY AND CONSTRUCTION OF " + ws.Cells[strCell].Value.ToString().Substring(index + 1, ws.Cells[strCell].Value.ToString().Length - index - 1);
                                        }
                                        else
                                        {
                                            bidDesc = bidDesc + " AND " + ws.Cells[strCell].Value.ToString().Substring(index + 1, ws.Cells[strCell].Value.ToString().Length - index - 1);
                                        }


                                    }
                                    if (r == 4)
                                    {

                                        project = ws.Cells[strCell].Value.ToString();

                                    }
                                    if (r > 4)
                                    {
                                        break;
                                    }

                                }
                            }
                            else
                            {
                                break;
                            }


                        }
                    }
                }

                partDesc = "";


                //listChild = listChild.Where(c => c.Name == nameSheet).ToList();
                //header

                //set Width 
                worksheet.Column(1).Width = 15;
                worksheet.Column(2).Width = 40;
                worksheet.Column(3).Width = 15;
                worksheet.Column(4).Width = 20;
                worksheet.Column(5).Width = 20;
                worksheet.Column(6).Width = 20;
                worksheet.Column(7).Width = 20;
                worksheet.Column(8).Width = 20;

                XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

                childSheet.header_l1_bidno = BidNo;
                childSheet.header_l2_PartName = "SUMMARY OF BID PRICE";
                childSheet.header_l3_biddesc = bidDesc;
                childSheet.header_l4_project = project;

                childSheet.column_1_l1 = "Schedule";
                childSheet.column_2_l1 = "Description";
                childSheet.column_3_l1 = "Currency";
                childSheet.column_4_l1 = "Supply of Equipment";
                childSheet.column_4_l2_c1 = "Foreign Supply";
                childSheet.column_4_l2_c2 = "Local Supply";
                childSheet.column_4_l3_c1 = "CIF Thai Port";
                childSheet.column_4_l3_c2 = "Ex-works Price";
                childSheet.column_4_l4_c2 = "( excluding VAT )";
                childSheet.column_4_l5_c2 = "Baht";
                childSheet.column_4_l6_c1_1 = "Amount";
                childSheet.column_4_l6_c2_1 = "Amount";
                childSheet.column_5_l1 = "Local Currency";
                childSheet.column_5_l4 = "( excluding VAT )";
                childSheet.column_5_l5 = "Baht";
                childSheet.column_5_l6_c1 = "Amount";
                childSheet.column_6_l1 = "Local Transportation";
                childSheet.column_6_l6_c1 = "Amount";
                childSheet.column_6_l4 = "( excluding VAT )";
                childSheet.column_6_l5 = "Baht";
                childSheet.column_7_l6_c1 = "Amount";
                childSheet.column_7_l1 = "Local Transportation, Construction and Installation";
                childSheet.column_7_l4 = "( excluding VAT )";
                childSheet.column_7_l5 = "Baht";
                childSheet.column_7_l6_c1 = "Amount";


                childSheet.footer_1_l1 = "SUMMARY OF BID PRICE";
                childSheet.footer_4_l1 = "Baht";
                childSheet.footer_5_l1 = "Baht";



                //worksheet.Cells["EE1"].Value = this.key;
                //worksheet.Column(135).Hidden = true;

                worksheet.Cells["A1:H1"].Merge = true;
                worksheet.Cells["A1:H1"].Value = "INVITATION TO BID NO. " + childSheet.header_l1_bidno;
                worksheet.Cells["A1:H1"].Style.Font.Bold = true;
                worksheet.Cells["A1:H1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A2:H2"].Merge = true;
                worksheet.Cells["A2:H2"].Value = childSheet.header_l2_PartName;
                worksheet.Cells["A2:H2"].Style.Font.Bold = true;
                worksheet.Cells["A2:H2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A3:H3"].Merge = true;
                worksheet.Cells["A3:H3"].Value = childSheet.header_l3_biddesc;
                worksheet.Cells["A3:H3"].Style.Font.Bold = true;
                worksheet.Cells["A3:H3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                worksheet.Cells["A4:H4"].Merge = true;
                worksheet.Cells["A4:H4"].Value = childSheet.header_l4_project;
                worksheet.Cells["A4:H4"].Style.Font.Bold = true;
                worksheet.Cells["A4:H4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

                //header Colunm

                worksheet.Cells["A5:A10"].Merge = true;
                worksheet.Cells["A5:A10"].Value = childSheet.column_1_l1;
                worksheet.Cells["A5:A10"].Style.Font.Bold = true;
                worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["B5:B10"].Merge = true;
                worksheet.Cells["B5:B10"].Value = childSheet.column_2_l1;
                worksheet.Cells["B5:B10"].Style.Font.Bold = true;
                worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["C5:C10"].Merge = true;
                worksheet.Cells["C5:C10"].Value = childSheet.column_3_l1;
                worksheet.Cells["C5:C10"].Style.Font.Bold = true;
                worksheet.Cells["C5:C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["C5:C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["C5:C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["C5:C10"].Style.WrapText = true;

                worksheet.Cells["D5:E5"].Merge = true;
                worksheet.Cells["D5:E5"].Value = childSheet.column_4_l1;
                worksheet.Cells["D5:E5"].Style.Font.Bold = true;
                worksheet.Cells["D5:E5"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D5:E5"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D5:E5"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D5:E5"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D5:E5"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D5:E5"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D6"].Merge = true;
                worksheet.Cells["D6"].Value = childSheet.column_4_l2_c1;
                worksheet.Cells["D6"].Style.Font.Bold = true;
                worksheet.Cells["D6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["E6"].Merge = true;
                worksheet.Cells["E6"].Value = childSheet.column_4_l2_c2;
                worksheet.Cells["E6"].Style.Font.Bold = true;
                worksheet.Cells["E6"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E6"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E6"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E6"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E6"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E6"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                worksheet.Cells["D7:D9"].Merge = true;
                worksheet.Cells["D7:D9"].Value = childSheet.column_4_l3_c1;
                worksheet.Cells["D7:D9"].Style.Font.Bold = true;
                worksheet.Cells["D7:D9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D7:D9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D7:D9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D7:D9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D7:D9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D7:D9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D7:D9"].Style.WrapText = true;


                worksheet.Cells["E7"].Merge = true;
                worksheet.Cells["E7"].Value = childSheet.column_4_l3_c2;
                worksheet.Cells["E7"].Style.Font.Bold = true;
                worksheet.Cells["E7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E7"].Style.WrapText = true;

                worksheet.Cells["E8"].Merge = true;
                worksheet.Cells["E8"].Value = childSheet.column_4_l4_c2;
                worksheet.Cells["E8"].Style.Font.Bold = true;
                worksheet.Cells["E8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                //  worksheet.Cells["H8:I8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E8"].Style.WrapText = true;

                worksheet.Cells["E9"].Merge = true;
                worksheet.Cells["E9"].Value = childSheet.column_4_l5_c2;
                worksheet.Cells["E9"].Style.Font.Bold = true;
                worksheet.Cells["E9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                //worksheet.Cells["H9:I9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E9"].Style.WrapText = true;

                worksheet.Cells["D10"].Value = childSheet.column_4_l6_c1_1;
                //worksheet.Cells["D10"].Style.Font.Bold = true;
                worksheet.Cells["D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["D10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["F9"].Style.WrapText = true;

                worksheet.Cells["E10"].Value = childSheet.column_4_l6_c2_1;
                // worksheet.Cells["E10"].Style.Font.Bold = true;
                worksheet.Cells["E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["E10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["E10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["E10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["F9"].Style.WrapText = true;


                worksheet.Cells["F5:F7"].Merge = true;
                worksheet.Cells["F5:F7"].Value = childSheet.column_5_l1;
                worksheet.Cells["F5:F7"].Style.Font.Bold = true;
                worksheet.Cells["F5:F7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F5:F7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F5:F7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5:F7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5:F7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F5:F7"].Style.WrapText = true;

                worksheet.Cells["F8"].Merge = true;
                worksheet.Cells["F8"].Value = childSheet.column_5_l4;
                worksheet.Cells["F8"].Style.Font.Bold = true;
                worksheet.Cells["F8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                // worksheet.Cells["F8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F8"].Style.WrapText = true;

                worksheet.Cells["F9"].Merge = true;
                worksheet.Cells["F9"].Value = childSheet.column_5_l5;
                worksheet.Cells["F9"].Style.Font.Bold = true;
                worksheet.Cells["F9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F9"].Style.WrapText = true;

                worksheet.Cells["F10"].Value = childSheet.column_5_l6_c1;
                //worksheet.Cells["F10"].Style.Font.Bold = true;
                worksheet.Cells["F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["F10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["F10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["F10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["F9"].Style.WrapText = true;


                // worksheet.Cells["F9"].Style.WrapText = true;


                worksheet.Cells["G5:G7"].Merge = true;
                worksheet.Cells["G5:G7"].Value = childSheet.column_6_l1;
                worksheet.Cells["G5:G7"].Style.Font.Bold = true;
                worksheet.Cells["G5:G7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G5:G7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["G5:G7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:G7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:G7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G5:G7"].Style.WrapText = true;

                worksheet.Cells["G8"].Merge = true;
                worksheet.Cells["G8"].Value = childSheet.column_6_l4;
                worksheet.Cells["G8"].Style.Font.Bold = true;
                worksheet.Cells["G8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                // worksheet.Cells["G8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G8"].Style.WrapText = true;

                worksheet.Cells["G9"].Merge = true;
                worksheet.Cells["G9"].Value = childSheet.column_6_l5;
                worksheet.Cells["G9"].Style.Font.Bold = true;
                worksheet.Cells["G9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G9"].Style.WrapText = true;

                worksheet.Cells["G10"].Value = childSheet.column_6_l6_c1;
                // worksheet.Cells["G10"].Style.Font.Bold = true;
                worksheet.Cells["G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["G10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["G10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["G10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["F9"].Style.WrapText = true;

                worksheet.Cells["H5:H7"].Merge = true;
                worksheet.Cells["H5:H7"].Value = childSheet.column_7_l1;
                worksheet.Cells["H5:H7"].Style.Font.Bold = true;
                worksheet.Cells["H5:H7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["H5:H7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["H5:H7"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H5:H7"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                //worksheet.Cells["J5:K7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H5:H7"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H5:H7"].Style.WrapText = true;

                worksheet.Cells["H8"].Merge = true;
                worksheet.Cells["H8"].Value = childSheet.column_7_l4;
                worksheet.Cells["H8"].Style.Font.Bold = true;
                worksheet.Cells["H8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["H8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                //worksheet.Cells["H8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["J8:K8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H8"].Style.WrapText = true;

                worksheet.Cells["H9"].Merge = true;
                worksheet.Cells["H9"].Value = childSheet.column_7_l5;
                worksheet.Cells["H9"].Style.Font.Bold = true;
                worksheet.Cells["H9"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["H9"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                // worksheet.Cells["J9:K9"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H9"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H9"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H9"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H9"].Style.WrapText = true;

                worksheet.Cells["H10"].Value = childSheet.column_7_l6_c1;
                // worksheet.Cells["H10"].Style.Font.Bold = true;
                worksheet.Cells["H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                worksheet.Cells["H10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
                //set border
                worksheet.Cells["H10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells["H10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                // worksheet.Cells["F9"].Style.WrapText = true;


                //data

                bool haveData = false;
                ps_xls_template xls_temp = new ps_xls_template();

                //if (dt_eq.Rows.Count > 0)
                //{
                //    haveData = true;
                int numSch = 1;
                //if (format == "Substation1")
                //{

                foreach (var item in listFileName)
                {

                    string foreignUSD = "";
                    string foreignEUR = "";
                    string foreignJPY = "";
                    string foreignCNY = "";
                    string supplyTHB = "";
                    string currencyTHB = "";
                    string tranTHB = "";
                    string tranConTHB = "";
                    string foreignUSDCell = "";
                    string foreignEURCell = "";
                    string foreignJPYCell = "";
                    string foreignCNYCell = "";
                    string supplyTHBCell = "";
                    string currencyTHBCell = "";
                    string tranTHBCell = "";
                    string tranConTHBCell = "";
                    string des = "";
                    FullPaht = "C:\\Xls\\" + item + "";

                    using (var package = new ExcelPackage(new FileInfo(FullPaht)))
                    {
                        var workbook = package.Workbook;

                        foreach (var ws in workbook.Worksheets)
                        {
                            string sheetName = ws.Name;


                            if (sheetName.Equals("SumAll"))
                            {


                                for (int r = 1; r < ws.Dimension.Rows; r++)
                                {
                                    strCell = "A" + (r).ToString();
                                    if (r == 2)
                                    {
                                        int index = ws.Cells[strCell].Value.ToString().IndexOf(':');
                                        des = ws.Cells[strCell].Value.ToString().Substring(index + 1, ws.Cells[strCell].Value.ToString().Length - index - 1);
                                        //ws.Cells[strCell].Formula = "IF";
                                    }
                                    if (r >= 11)
                                    {

                                        if (ws.Cells[strCell].Value.ToString() == "TOTAL PRICE")
                                        {
                                            strCell = "C" + (r).ToString();
                                            // foreignUSD = ws.Cells[strCell].Value.ToString();
                                            foreignUSDCell = strCell;

                                            strCell = "C" + (r + 1).ToString();
                                            //foreignEUR = ws.Cells[strCell].Value.ToString();
                                            foreignEURCell = strCell;

                                            strCell = "C" + (r + 2).ToString();
                                            // foreignJPY = ws.Cells[strCell].Value.ToString();
                                            foreignJPYCell = strCell;

                                            strCell = "C" + (r + 3).ToString();
                                            // foreignCNY = ws.Cells[strCell].Value.ToString();
                                            foreignCNYCell = strCell;

                                            strCell = "D" + (r + 1).ToString();
                                            // supplyTHB = ws.Cells[strCell].Value.ToString();
                                            supplyTHBCell = strCell;

                                            strCell = "E" + (r + 1).ToString();
                                            // currencyTHB = ws.Cells[strCell].Value.ToString();
                                            currencyTHBCell = strCell;

                                            strCell = "F" + (r + 1).ToString();
                                            //tranTHB = ws.Cells[strCell].Value.ToString();
                                            tranTHBCell = strCell;

                                            strCell = "G" + (r + 1).ToString();
                                            //tranConTHB = ws.Cells[strCell].Value.ToString();
                                            tranConTHBCell = strCell;
                                            break;
                                        }
                                    }

                                }
                            }
                            else
                            {
                                break;
                            }


                        }

                    }
                    //worksheet.Cells["B31"].Formula = "LEFT(CELL(\"filename\",A1),FIND(\"[\",CELL(\"filename\",A1))-1)";
                    //worksheet.Cells["B31"].Calculate();
                    //worksheet.Column(135).Hidden = true;

                    //FullPaht = worksheet.Cells["EE2"].Value.ToString()+"\\["+ item + "]";

                    FullPaht = "\\" + "[" + item + "" + "]";
                    for (int i = 0; i <= 4; i++)
                    {
                        strCell = "A" + (numRow).ToString();
                        if (i == 0)
                        {

                            worksheet.Cells[strCell].Value = numSch.ToString();
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.WrapText = true;
                        }
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                        }
                        strCell = "B" + (numRow).ToString();
                        if (i == 0)
                        {

                            //worksheet.Cells[strCell].Value = des;
                            worksheet.Cells[strCell].Formula = "RIGHT('" + FullPaht + "SumAll'!A2, LEN('" + FullPaht + "SumAll'!A2) - SEARCH(\":\", '" + FullPaht + "SumAll'!A2))";
                            worksheet.Cells[strCell].Style.Font.Bold = true;
                            worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                            worksheet.Cells[strCell].Style.WrapText = true;
                        }
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                        }

                        strCell = "C" + (numRow).ToString();
                        // worksheet.Cells[strCell].Value = Part.Currency;
                        switch (i)
                        {
                            case 0:
                                worksheet.Cells[strCell].Value = "USD";
                                break;
                            case 1:
                                worksheet.Cells[strCell].Value = "EUR";
                                break;
                            case 2:
                                worksheet.Cells[strCell].Value = "JPY";
                                break;
                            case 3:
                                worksheet.Cells[strCell].Value = "CNY";
                                break;
                            case 4:
                                worksheet.Cells[strCell].Value = "THB";
                                break;
                        }

                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "D" + (numRow).ToString();
                        switch (i)
                        {
                            case 0:

                                //worksheet.Cells[strCell].Value = Convert.ToDecimal(foreignUSD);
                                worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + foreignUSDCell;

                                break;
                            case 1:

                                //worksheet.Cells[strCell].Value = Convert.ToDecimal(foreignEUR);
                                worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + foreignEURCell;

                                break;
                            case 2:

                                //worksheet.Cells[strCell].Value = Convert.ToDecimal(foreignJPY);
                                worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + foreignJPYCell;

                                break;
                            case 3:

                                //worksheet.Cells[strCell].Value = Convert.ToDecimal(foreignCNY);
                                worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + foreignCNYCell;

                                break;
                        }
                        //worksheet.Cells[strCell].Value = Part.ForeignAmount == "0" ? "" : Part.ForeignAmount;
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "E" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {

                            //worksheet.Cells[strCell].Value = Convert.ToDecimal(supplyTHB);
                            worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + supplyTHBCell;

                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;



                        strCell = "F" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {

                            //worksheet.Cells[strCell].Value = Convert.ToDecimal(currencyTHB);
                            worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + currencyTHBCell;

                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "G" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {

                            //worksheet.Cells[strCell].Value = Convert.ToDecimal(tranTHB);
                            worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + tranTHBCell;

                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        strCell = "H" + (numRow).ToString();
                        worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                        worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                        if (i == 4)
                        {

                            // worksheet.Cells[strCell].Value = Convert.ToDecimal(tranConTHB);
                            worksheet.Cells[strCell].Formula = "'" + FullPaht + "SumAll'!" + tranConTHBCell;
                            worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                        }
                        worksheet.Cells[strCell].Style.WrapText = true;

                        numRow++;
                    }





                    numSch++;

                }

                int startRow = 0;
                int endRow = 0;
                //if (havedata)
                //{
                startRow = 11;
                //}
                //else
                //{
                //    startRow = 10;
                //}
                endRow = numRow - 1;
                //footer


                string spec = " ";
                string Formula = "";


                for (int r = 0; r < 3; r++)
                {
                    for (int k = 0; k < 4; k++)
                    {
                        switch (r)
                        {
                            case 0:

                                if (k == 0)
                                {
                                    strCell = "A" + (numRow).ToString() + ":" + "B" + (numRow + 3).ToString();
                                    worksheet.Cells[strCell].Merge = true;
                                    worksheet.Cells[strCell].Value = "BID PRICE";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    //set border
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }


                                strCell = "C" + (numRow).ToString();
                                if (k == 0)
                                {

                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Value = "USD";
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Value = "EUR";
                                }
                                if (k == 2)
                                {
                                    worksheet.Cells[strCell].Value = "JPY";
                                }
                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Value = "CNY";
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}


                                strCell = "D" + (numRow).ToString();
                                if (k != 4)
                                {

                                    Formula = "IF(C" + numRow + "=\"\",\"\",SUMIF($C$" + startRow + ":$C$" + endRow + ",$C" + numRow + ",$D$" + startRow + ":$D$" + endRow + "))";
                                    //worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                                    worksheet.Cells[strCell].Formula = Formula;
                                }
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                                strCell = "E" + (numRow).ToString();

                                // worksheet.Cells[strCell].Style.Font.Bold = true;
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}

                                strCell = "F" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(F" + startRow + ":F" + endRow + ")";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}

                                strCell = "G" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}

                                strCell = "H" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(H" + startRow + ":H" + endRow + ")";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}
                                break;
                            case 1:

                                if (k == 0)
                                {
                                    strCell = "A" + (numRow).ToString() + ":" + "B" + (numRow).ToString();
                                    worksheet.Cells[strCell].Merge = true;
                                    worksheet.Cells[strCell].Value = "";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    //set border
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    // worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                if (k == 1)
                                {

                                    strCell = "A" + (numRow).ToString() + ":" + "B" + (numRow).ToString();
                                    worksheet.Cells[strCell].Merge = true;
                                    worksheet.Cells[strCell].Value = "Vat";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    //set border
                                    //worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    // worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                if (k == 2)
                                {

                                    strCell = "A" + (numRow).ToString();
                                    worksheet.Cells[strCell].Value = "";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                                    strCell = "B" + (numRow).ToString();
                                    worksheet.Cells[strCell].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                    worksheet.Cells[strCell].Style.Fill.BackgroundColor.SetColor(1, 128, 128, 128);
                                    worksheet.Cells[strCell].Value = "7%";
                                    worksheet.Cells[strCell].Style.Font.Color.SetColor(Color.White);
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    //set border
                                    // worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    // worksheet.Cells[strCell].Style.Border.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                if (k == 3)
                                {
                                    strCell = "A" + (numRow).ToString();
                                    worksheet.Cells[strCell].Value = "";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                                    strCell = "B" + (numRow).ToString();
                                    worksheet.Cells[strCell].Value = "";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }


                                strCell = "C" + (numRow).ToString();
                                if (k == 0)
                                {

                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    //worksheet.Cells[strCell].Value = "USD";
                                }
                                if (k == 1)
                                {
                                    // worksheet.Cells[strCell].Value = "EUR";
                                }
                                if (k == 2)
                                {
                                    //worksheet.Cells[strCell].Value = "JPY";
                                }
                                if (k == 3)
                                {
                                    // worksheet.Cells[strCell].Value = "CNY";
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}


                                strCell = "D" + (numRow).ToString();
                                if (k != 4)
                                {

                                    // Formula = "IF(C" + numRow + "=\"\",\"\",SUMIF($C$" + startRow + ":$C$" + endRow + ",$C" + numRow + ",$D$" + startRow + ":$D$" + endRow + "))";
                                    //worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                                    // worksheet.Cells[strCell].Formula = Formula;
                                }
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                                strCell = "E" + (numRow).ToString();

                                // worksheet.Cells[strCell].Style.Font.Bold = true;
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(E" + startRow + ":E" + endRow + ")*7%";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}

                                strCell = "F" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(F" + startRow + ":F" + endRow + ")*7%";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}

                                strCell = "G" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(G" + startRow + ":G" + endRow + ")*7%";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}

                                strCell = "H" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(H" + startRow + ":H" + endRow + ")*7%";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                //if (k == 4)
                                //{
                                //    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                //}
                                break;
                            case 2:

                                if (k == 0)
                                {
                                    strCell = "A" + (numRow).ToString() + ":" + "B" + (numRow + 3).ToString();
                                    worksheet.Cells[strCell].Merge = true;
                                    worksheet.Cells[strCell].Value = "SUMMARY OF BID PRICE";
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                                    //set border
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }


                                strCell = "C" + (numRow).ToString();
                                if (k == 0)
                                {

                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Value = "USD";
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Value = "EUR";
                                }
                                if (k == 2)
                                {
                                    worksheet.Cells[strCell].Value = "JPY";
                                }
                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Value = "CNY";
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }


                                strCell = "D" + (numRow).ToString();
                                if (k != 4)
                                {

                                    Formula = "IF(C" + numRow + "=\"\",\"\",SUMIF($C$" + startRow + ":$C$" + endRow + ",$C" + numRow + ",$D$" + startRow + ":$D$" + endRow + "))";
                                    //worksheet.Cells[strCell].Formula = "SUM(C" + startRow + ":C" + endRow + ")";
                                    worksheet.Cells[strCell].Formula = Formula;
                                }
                                worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Dotted;
                                worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

                                strCell = "E" + (numRow).ToString();

                                // worksheet.Cells[strCell].Style.Font.Bold = true;
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(E" + (endRow + 2) + ":E" + (endRow + 6) + ")";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                strCell = "F" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(F" + (endRow + 2) + ":F" + (endRow + 6) + ")";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                strCell = "G" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(G" + (endRow + 2) + ":G" + (endRow + 6) + ")";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                strCell = "H" + (numRow).ToString();
                                worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                if (k == 0)
                                {
                                    worksheet.Cells[strCell].Value = "Baht";
                                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                                    worksheet.Cells[strCell].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                    worksheet.Cells[strCell].Style.Font.Bold = true;
                                }
                                if (k == 1)
                                {
                                    worksheet.Cells[strCell].Formula = "SUM(H" + (endRow + 2) + ":H" + (endRow + 6) + ")";
                                    //worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Right;
                                    //worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }

                                if (k == 3)
                                {
                                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                                }
                                break;

                        }

                        numRow++;
                    }
                }


            }
            return worksheet;
        }
        #endregion

        #region " genExcel Supply Delivery "
        public DataTable getGVSelPartDelivery(string dept, string sect, string bidNo, string schName, string part, string jobNo, string rev)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (bidNo != "Bid Supply")
                {
                    sql = @"select distinct  rowid,a.job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_mas_grp,equip_full_desc + equip_add_desc as equip_full_desc,legend,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit ,part_code_b as 'Part',equip_item_no_b as 'equip_item_no',
				j.sub_line_abbr as 'Substation',4 as 'Delivery_Required','Q' as 'Account_assign', 'J-'+a.job_no+'-40000' as 'Cost_Object','I430000' as 'Fund Center'
                from  ps_t_mytask_equip  a
				left join job_no j
				on a.job_no = j.job_no 
                where bid_no = '" + bidNo + "' and schedule_name ='" + schName + "'  and part_code_b='" + part + "' and j.status ='1Active'";
                }
                else
                {
                    sql = @"select rowid,job_no,job_revision,bid_no,bid_revision,schedule_no,depart_position_name,section_position_name,
                equip_code,equip_add_desc,equip_qty,created_by,created_datetime,updated_by,updated_datetime,equip_incoterm,factory_test,manual_test,routine_test,
                equip_unit,equip_bid_qty,
                supply_equip_unit_price,supply_equip_amonut,local_exwork_unit_price,
                local_exwork_amonut,local_tran_unit_price,local_tran_amonut,currency,equip_standard_drawing,equip_mas_grp,equip_full_desc + equip_add_desc as equip_full_desc,legend,equip_unit as unit,'' as [type],'' as [view],'' as equip_code2,'1' as edit  ,part_code_b as 'Part',equip_item_no_b as 'equip_item_no'
                from  ps_t_mytask_equip 
                where  schedule_name ='" + schName + "'  and path_code='" + part + "' and job_no='" + jobNo + "' and job_revision='" + rev + "'";
                }
                // and section_position_name='" + sect + "'and depart_position_name ='" + dept + "'

                dt = zdbUtil.ExecSql_DataTable(sql, psDB);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public string genExcelDelivery(string FileName, string Dept, string BidNo, string BidRev, string format_input)
        {
            //string BidRev = "0";
            //Dept = "กวศ–ส.";
            //Sect = "";
            //BidNo = "TS12-S-02";
            //ScheNo = "1";
            //JobNo = "TS12-11-S06";
            //rev = "1";
            //Dept = "กวสส-ส.";
            //Sect = "";
            //BidNo = "TS12-L-02";
            //ScheNo = "2";
            //JobNo = "TS12-07-L01";

            //schName = schName;
            this.key = GenerateProductKey();
            DataTable dtPart = new DataTable();
            //if (format_input == "E")
            //{


            //    dtPart = getBtnPaths(BidNo, schName);
            //}
            if (format_input == "S")
            {


                dtPart = getBtnPathsSupply(BidNo, "");
            }


            //DataTable dtJob = objJobDb.GetJobRelate(JobNo, BidNo);
            DataTable dtBid = objJobDb.getProject_no(BidNo.Substring(0, 4));
            // FileName = BidNo;
            this.format_input = format_input;
            if (format_input == "S")
            {
                if (dtPart.Rows.Count > 0)
                {
                    //if (dtJob.Rows.Count > 0)
                    //{
                    //this.partDesc = dtPart.Rows[0]["part_description"].ToString();
                    this.bidDesc = "SUPPLY OF " + "";
                    //}
                    if (dtBid.Rows.Count > 0)
                    {
                        this.project = dtBid.Rows[0]["project_name_en"].ToString();
                    }
                    DataTable dt_eq = new DataTable();
                    //List<DataTable> listEq = new List<DataTable>();
                    this.Dept = Dept;
                    //this.Sect = Sect;
                    this.BidNo = BidNo;
                    this.ScheNo = ScheNo;
                    this.JobNo = JobNo;
                    this.JobRev = JobRev;
                    this.SchName = "";
                    this.BidRev = BidRev;
                    //foreach (DataRow dr in dtPart.Rows)
                    //{
                    //    dt_eq = objJobDb.getGVSelPart(Dept, Sect, BidNo, ScheNo, dr["part_name"].ToString(), JobNo, rev);
                    //    /// listEq.Add(dt_eq);
                    //}

                    FileName = writeExcelDelivery(FileName, dtPart);

                }
            }
            return FileName;
        }
        public string writeExcelDelivery(string FileName, DataTable dtPart)
        {
            // If you are a commercial business and have
            // purchased commercial licenses use the static property
            // LicenseContext of the ExcelPackage class:
            //ExcelPackage.LicenseContext = LicenseContext.Commercial;

            // If you use EPPlus in a noncommercial context
            // according to the Polyform Noncommercial license:
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            DataTable dt_eq = new DataTable();
            bool result = false;
            var package = new ExcelPackage();
            //string user = empID;
            FileName = "Dist_list_" + FileName;
            string format = "";
            var workbook = package.Workbook;
            DataTable dttt = new DataTable();


            try
            {


                //numSheet++;
                foreach (DataRow dr in dtPart.Rows)
                {
                    string part = dr["part_name"] == null ? "" : dr["part_name"].ToString();
                    string part_description = dr["part_description"] == null ? "" : dr["part_description"].ToString();
                    string part_remark = dr["part_remark"] == null ? "" : dr["part_remark"].ToString();
                    string important = dr["important"] == null ? "" : dr["important"].ToString();

                    var worksheet = workbook.Worksheets.Add(part);
                    // worksheet.PrinterSettings.Orientation = eOrientation.Landscape;
                    worksheet.PrinterSettings.FitToPage = true;
                    string strCell = "";

                    dt_eq = getGVSelPartDelivery(Dept, Sect, BidNo, SchName, dr["part_name"].ToString(), JobNo, JobRev);


                    //Supply
                    //if (part.Length > 3 && (part.Substring(0, 3)).ToUpper().Equals("SCH"))
                    //{
                    //    format = "Supply";
                    //}
                    if (this.format_input == "S")
                    {
                        format = "Supply";
                    }


                    worksheet = setSheetChildSupplyDelivery(worksheet, dt_eq, part, part_description);


                }
                // package.Workbook.Calculate();
                FileName = FileName + ".xlsx";
                package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));

                //delete_ps_xls_template();
                //insertps_xls_template();
                //listXls_temp.Clear();
                // uploadData(1, new FileInfo("C:\\Xls\\" + FileName), empID);
            }
            catch (Exception ex) { }

            return FileName;
        }
        public OfficeOpenXml.ExcelWorksheet setSheetChildSupplyDelivery(OfficeOpenXml.ExcelWorksheet wk, DataTable dt_eq, string part, string part_description)
        {
            var worksheet = wk;
            worksheet.Protection.IsProtected = true;
            char cell = 'A';
            decimal totlaAmountCFR = 0;
            string strCell = "";
            int numRow = 11;
            string ItemPart = "";


            //listChild = listChild.Where(c => c.Name == nameSheet).ToList();
            //header

            //set Width 
            worksheet.Column(2).Width = 40;
            worksheet.Column(4).Width = 20;
            worksheet.Column(6).Width = 60;
            //worksheet.Column(3).Width = 15;
            //worksheet.Column(6).Width = 15;
            //worksheet.Column(7).Width = 15;
            //worksheet.Column(8).Width = 15;
            //worksheet.Column(9).Width = 15;
            worksheet.Column(10).Width = 15;
            worksheet.Column(11).Width = 25;
            worksheet.Column(12).Width = 17;
            //worksheet.Column(13).Width = 15;

            XLSPS_ChildSheet childSheet = new XLSPS_ChildSheet();

            childSheet.header_l1_bidno = BidNo;
            childSheet.header_l2_PartName = "Delivery Schedule and Distribution List";
            childSheet.header_l4_project = project;
            childSheet.header_l3_biddesc = "Schedule " + part + " " + part_description;

            childSheet.column_1_l1 = "Item No.";
            childSheet.column_2_l1 = "Description";
            childSheet.column_3_l1 = "Qty.";
            childSheet.column_4_l1 = "Job no.";
            childSheet.column_5_l1 = "Substation";


            childSheet.column_6_l1 = "Delivery Required by EGAT" +
                                    "(within months after receipt of Award of Contract)";
            childSheet.column_6_l2 = "CFR Thai Port / Ex-works / DDP  EGAT's Store ";

            childSheet.column_7_l1 = "";
            childSheet.column_7_l2_c1 = "DDP or";
            childSheet.column_7_l3_c1 = "EGAT's Store";
            childSheet.column_7_l2_c2 = "Ex-works";


            childSheet.column_8_l1 = "Completion Date";

            childSheet.column_9_l1 = "Supply";
            childSheet.column_9_l2_c1 = "Account assign.";
            childSheet.column_9_l2_c2 = "Cost Object ";
            childSheet.column_9_l2_c3 = "Fund Center";


            worksheet.Cells["A1:F1"].Merge = true;
            worksheet.Cells["A1:F1"].Value = "INVITATION TO BID NO. " + childSheet.header_l1_bidno;
            worksheet.Cells["A1:F1"].Style.Font.Bold = true;
            worksheet.Cells["A1:F1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A2:F2"].Merge = true;
            worksheet.Cells["A2:F2"].Value = childSheet.header_l2_PartName;
            worksheet.Cells["A2:F2"].Style.Font.Bold = true;
            worksheet.Cells["A2:F2"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A3:F3"].Merge = true;
            worksheet.Cells["A3:F3"].Value = childSheet.header_l4_project;
            worksheet.Cells["A3:F3"].Style.Font.Bold = true;
            worksheet.Cells["A3:F3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            worksheet.Cells["A4:F4"].Merge = true;
            worksheet.Cells["A4:F4"].Value = childSheet.header_l3_biddesc;
            worksheet.Cells["A4:F4"].Style.Font.Bold = true;
            worksheet.Cells["A4:F4"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;

            //header Colunm
            //setcolor 
            worksheet.Cells["A5:I10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A5:I10"].Style.Fill.BackgroundColor.SetColor(1, 204, 255, 204);

            worksheet.Cells["J5:L8"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["J5:L8"].Style.Fill.BackgroundColor.SetColor(1, 255, 255, 0);

            worksheet.Cells["J9:K10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["J9:K10"].Style.Fill.BackgroundColor.SetColor(1, 255, 255, 0);

            worksheet.Cells["L9:L10"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["L9:L10"].Style.Fill.BackgroundColor.SetColor(1, 255, 255, 153);


            worksheet.Cells["A5:A10"].Merge = true;
            worksheet.Cells["A5:A10"].Value = childSheet.column_1_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["A5:A10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A5:A10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["A5:A10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A5:A10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["B5:B10"].Merge = true;
            worksheet.Cells["B5:B10"].Value = childSheet.column_2_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["B5:B10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["B5:B10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["B5:B10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["B5:B10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["C5:C10"].Merge = true;
            worksheet.Cells["C5:C10"].Value = childSheet.column_3_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["C5:C10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["C5:C10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["C5:C10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["C5:C10"].Style.WrapText = true;

            worksheet.Cells["D5:D10"].Merge = true;
            worksheet.Cells["D5:D10"].Value = childSheet.column_4_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["D5:D10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["D5:D10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["D5:D10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["D5:D10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["E5:E10"].Merge = true;
            worksheet.Cells["E5:E10"].Value = childSheet.column_5_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["E5:E10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["E5:E10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["E5:E10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["E5:E10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F5:F7"].Merge = true;
            worksheet.Cells["F5:F7"].Value = childSheet.column_6_l1;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["F5:F7"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F5:F7"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F5:F8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F5:F8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["F9:F10"].Merge = true;
            worksheet.Cells["F9:F10"].Value = childSheet.column_6_l2;
            //worksheet.Cells["A5:A10"].Style.Font.Bold = true;
            worksheet.Cells["F9:F10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["F9:F10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["F9:F10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:F10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:F10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["F9:F10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G5:H8"].Merge = true;
            worksheet.Cells["G5:H8"].Value = "";
            worksheet.Cells["G5:H8"].Style.Font.Bold = true;
            worksheet.Cells["G5:H8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G5:H8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G5:H8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:H8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:H8"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G5:H8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["G9:G10"].Merge = true;
            worksheet.Cells["G9:G10"].Value = childSheet.column_7_l2_c1;
            // worksheet.Cells["F6:G6"].Style.Font.Bold = true;
            worksheet.Cells["G9:G10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["G9:G10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["G9:G10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G9:G10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G9:G10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["G9:G10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["H9:H10"].Merge = true;
            worksheet.Cells["H9:H10"].Value = childSheet.column_7_l2_c2;
            // worksheet.Cells["H6:I6"].Style.Font.Bold = true;
            worksheet.Cells["H9:H10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["H9:H10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["H9:H10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H9:H10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H9:H10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["H9:H10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            worksheet.Cells["I5:I10"].Merge = true;
            worksheet.Cells["I5:I10"].Value = childSheet.column_8_l1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["I5:I10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["I5:I10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["I5:I10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I5:I10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I5:I10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I5:I10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["I5:I10"].Style.WrapText = true;


            worksheet.Cells["J5:L8"].Merge = true;
            worksheet.Cells["J5:L8"].Value = childSheet.column_9_l1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["J5:L8"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J5:L8"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["J5:L8"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J5:L8"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //worksheet.Cells["H7:I7"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J5:L8"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J5:L8"].Style.WrapText = true;

            worksheet.Cells["J9:J10"].Merge = true;
            worksheet.Cells["J9:J10"].Value = childSheet.column_9_l2_c1;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["J9:J10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["J9:J10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["J9:J10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J9:J10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J9:J10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["J9:J10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            // worksheet.Cells["F9"].Style.WrapText = true;


            worksheet.Cells["K9:K10"].Merge = true;
            worksheet.Cells["K9:K10"].Value = childSheet.column_9_l2_c2;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["K9:K10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["K9:K10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["K9:K10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:K10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:K10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["K9:K10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;


            worksheet.Cells["L9:L10"].Merge = true;
            worksheet.Cells["L9:L10"].Value = childSheet.column_9_l2_c3;
            //worksheet.Cells["F7:G8"].Style.Font.Bold = true;
            worksheet.Cells["L9:L10"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["L9:L10"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            //set border
            worksheet.Cells["L9:L10"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L9:L10"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L9:L10"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["L9:L10"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;

            //data
            bool haveData = false;
            ps_xls_template xls_temp = new ps_xls_template();

            if (dt_eq.Rows.Count > 0)
            {
                haveData = true;
                foreach (DataRow item in dt_eq.Rows)
                {

                    strCell = "A" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_item_no"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "B" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_full_desc"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "C" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["equip_qty"].ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "D" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["job_no"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "E" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["Substation"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "F" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["Delivery_Required"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "G" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "H" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "I" + (numRow).ToString();
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;

                    strCell = "J" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["Account_assign"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "K" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = item["Cost_Object"].ToString();
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;


                    strCell = "L" + (numRow).ToString();
                    worksheet.Cells[strCell].Value = "I430000";
                    worksheet.Cells[strCell].Style.VerticalAlignment = ExcelVerticalAlignment.Center;
                    worksheet.Cells[strCell].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                    worksheet.Cells[strCell].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                    worksheet.Cells[strCell].Style.WrapText = true;



                    //xls_temp = new ps_xls_template();
                    //xls_temp.template_id = key;
                    //xls_temp.schedule_name = SchName;
                    //xls_temp.bid_no = BidNo;
                    //xls_temp.bid_revision = BidRev;
                    //xls_temp.type = "Supply";
                    //xls_temp.item_no = item["equip_item_no"].ToString();
                    //xls_temp.description = item["equip_full_desc"].ToString();
                    //xls_temp.qty = item["equip_qty"] == null ? 0 : item["equip_qty"].ToString() == "" ? 0 : Convert.ToDecimal(item["equip_qty"].ToString());;
                    //xls_temp.unit = item["unit"].ToString();
                    //xls_temp.part = part;
                    //xls_temp.equip_code = item["equip_code"].ToString();
                    //listXls_temp.Add(xls_temp);


                    numRow++;
                }

            }

            //footer


            return worksheet;
        }
        #endregion

        #region " For File PDF "
        public string ExportExcelToPdf(FileInfo excelFile, string output_PDF_Path, ref string strErrorMessage)
        {
            ArrayList arrExportFilePath = new ArrayList();

            string[] fileSplit = SplitExcelLimit5Tab(excelFile.FullName);

            foreach (string strFileName in fileSplit)
            {
                FileInfo excelSplitFile = new FileInfo(strFileName);

                string exportFilePath = "";
                exportFilePath = output_PDF_Path + "\\" + excelSplitFile.Name.Replace(excelSplitFile.Extension, ".pdf");

                // Set license key to use GemBox.Document in a Free mode. 
                SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");

                // Continue to use the component in a Trial mode when free limit is reached. 
                SpreadsheetInfo.FreeLimitReached += (sender, e) => e.FreeLimitReachedAction = FreeLimitReachedAction.ContinueAsTrial;

                ExcelFile ef = ExcelFile.Load(excelSplitFile.FullName);

                PdfSaveOptions opt = new PdfSaveOptions();
                opt.SelectionType = SelectionType.EntireFile;

                ef.Save(exportFilePath, opt);

                arrExportFilePath.Add(exportFilePath);
            }


            string exportPDF_FilePath = "";
            exportPDF_FilePath = output_PDF_Path + "\\" + excelFile.Name.Replace(excelFile.Extension, ".pdf");

            string[] arrPDFFile = (string[])arrExportFilePath.ToArray(typeof(string));
            MergeMultiplePDFIntoSinglePDF(exportPDF_FilePath, arrPDFFile, ref strErrorMessage);

            return exportPDF_FilePath;
        }
        public string[] SplitExcelToPDF(FileInfo fileInfo, string strPDF_Path, ref string strError)
        {
            string strResult = string.Empty;
            string[] arrFileList = null;
            try
            {
                string strPDF_FileName = ExportExcelToPdf(fileInfo, strPDF_Path, ref strError);
                if (strPDF_FileName != "")
                {
                    FileInfo pdfFile = new FileInfo(strPDF_FileName);
                    arrFileList = SplitPagePDF(pdfFile, strPDF_Path, ref strError);

                    pdfFile.Delete();
                    pdfFile = null;
                }
            }
            catch (System.Exception ex)
            {
                strError += ex.Message;
            }
            return arrFileList;
        }
        public static void MergeMultiplePDFIntoSinglePDF(string outputFilePath, string[] pdfFiles, ref string strErrorMessage)
        {
            PdfSharp.Pdf.PdfDocument outputPDFDocument = new PdfSharp.Pdf.PdfDocument();
            try
            {
                foreach (string pdfFile in pdfFiles)
                {
                    PdfSharp.Pdf.PdfDocument inputPDFDocument = PdfReader.Open(pdfFile, PdfDocumentOpenMode.Import);
                    outputPDFDocument.Version = inputPDFDocument.Version;
                    foreach (PdfPage page in inputPDFDocument.Pages)
                    {
                        outputPDFDocument.AddPage(page);
                    }
                }
                outputPDFDocument.Save(outputFilePath);
            }
            catch (System.Exception ex)
            {
                strErrorMessage += ex.Message;
            }
            finally
            {
                outputPDFDocument.Close();
            }
        }
        public static string[] SplitPagePDF(FileInfo input_PDF_File, string output_PDF_Path, ref string strErrorMessage)
        {
            PdfSharp.Pdf.PdfDocument doc = new PdfSharp.Pdf.PdfDocument();
            try
            {
                doc = PdfReader.Open(input_PDF_File.FullName, PdfDocumentOpenMode.Import);
            }
            catch (System.Exception ex)
            {
                //workaround if pdfsharp doesn't support this pdf
                MemoryStream sourceStream = new MemoryStream(File.ReadAllBytes(input_PDF_File.FullName));
                MemoryStream outputStream = new MemoryStream();

                sourceStream.Position = 0;

                iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(sourceStream);
                iTextSharp.text.pdf.PdfStamper pdfStamper = new iTextSharp.text.pdf.PdfStamper(reader, outputStream);
                if (outputStream != null)
                {
                    pdfStamper.FormFlattening = true;
                    pdfStamper.Writer.SetPdfVersion(iTextSharp.text.pdf.PdfWriter.PDF_VERSION_1_4);
                    pdfStamper.Writer.CloseStream = false;
                    pdfStamper.Close();

                    doc = PdfReader.Open(outputStream, PdfDocumentOpenMode.Import);
                }
                else
                    strErrorMessage = ex.Message;
            }

            int iPageCount = 1;
            try
            {
                iPageCount = doc.PageCount;
            }
            catch (System.Exception ex)
            {
                string satrError = ex.Message;
                //workaround if pdfsharp doesn't support this pdf
                MemoryStream sourceStream = new MemoryStream(File.ReadAllBytes(input_PDF_File.FullName));
                MemoryStream outputStream = new MemoryStream();

                sourceStream.Position = 0;

                iTextSharp.text.pdf.PdfReader reader = new iTextSharp.text.pdf.PdfReader(sourceStream);
                iTextSharp.text.pdf.PdfStamper pdfStamper = new iTextSharp.text.pdf.PdfStamper(reader, outputStream);
                if (outputStream != null)
                {
                    pdfStamper.FormFlattening = true;
                    pdfStamper.Writer.SetPdfVersion(iTextSharp.text.pdf.PdfWriter.PDF_VERSION_1_4);
                    pdfStamper.Writer.CloseStream = false;
                    pdfStamper.Close();

                    doc = PdfReader.Open(outputStream, PdfDocumentOpenMode.Import);
                }
            }

            iPageCount = doc.PageCount;
            string[] arrFileList = new string[iPageCount];
            try
            {
                for (int index = 0; index < iPageCount; index++)
                {
                    PdfSharp.Pdf.PdfDocument docSplit = new PdfSharp.Pdf.PdfDocument();
                    docSplit.AddPage(doc.Pages[index]);

                    string strFileSave = output_PDF_Path + "\\" + input_PDF_File.Name.Replace(".PDF", ".pdf").Replace(".pdf", "_p_" + (index + 1).ToString("000") + ".pdf");
                    docSplit.Save(strFileSave);
                    docSplit.Close();

                    arrFileList.SetValue(strFileSave, index);
                }
            }
            catch (System.Exception ex)
            {
                strErrorMessage += ex.Message;
            }
            //finally
            //{
            //    doc.Close();
            //}
            return arrFileList;
        }
        #endregion

        #region " Upload excel "
        //upload
        public bool validateFile(string bid_no, string bid_revision, string schedule_name, Stream stream)
        {
            bool result = false;

            string format = "";
            string part = "";
            using (var package = new ExcelPackage(stream))
            {
                var workbook = package.Workbook;

                foreach (var worksheet in workbook.Worksheets)
                {
                    string sheetName = worksheet.Name;

                    if (sheetName.Length > 4)
                    {
                        string cname = sheetName.Substring(0, 4);
                        if ((!(sheetName.Equals("SumAll")) && cname != "Part"))
                        {
                            //string a = worksheet.Cells["A11"].Value.ToString();
                            format = checkFormatExcel(sheetName);
                            part = getPartOfExcel(sheetName);
                            //setDataFromExcel(worksheet, format, rowid, part);
                        }


                    }
                    else
                    {
                        format = checkFormatExcel(sheetName);
                        part = getPartOfExcel(sheetName);
                        // setDataFromExcel(worksheet, format, rowid, part);

                    }
                }
                //var worksheet = workbook.Worksheets.First();

                //string sheetName = worksheet.Name;
                ////string 
                //var ThatList = worksheet.Tables.First().ConvertTableToObjects<ExcelData>();
                //foreach (var data in ThatList)
                //{
                //    Console.WriteLine(data.Id + data.Name + data.Gender);
                //}

                //package.Save();
            }


            return result;
        }
        public string uploadData(int rowid, string bidder_id, string vendor_code, string bid_no, int bid_revision, string schedule_name, int version, Stream stream, string empID)
        {
            //            vender_file_id(Node id)
            //bidder_id
            //vendor_code
            //version
            //bid_no
            //bid_revision
            //schedule_name
            // File Steam

            string result = "";
            //var package = new ExcelPackage();
            //package.Load(stream);
            this.empID = empID;
            string format = "";
            string part = "";
            using (var package = new ExcelPackage(stream))
            {
                var workbook = package.Workbook;

                foreach (var worksheet in workbook.Worksheets)
                {
                    string sheetName = worksheet.Name;

                    if (sheetName.Length > 4)
                    {
                        string cname = sheetName.Substring(0, 4);
                        if ((!(sheetName.Equals("SumAll")) && cname != "Part"))
                        {
                            //string a = worksheet.Cells["A11"].Value.ToString();
                            format = checkFormatExcel(sheetName);
                            part = getPartOfExcel(sheetName);
                            setDataFromExcel(worksheet, format, rowid, part, bidder_id, vendor_code, bid_no, bid_revision, schedule_name, version, sheetName);
                        }


                    }
                    else
                    {
                        format = checkFormatExcel(sheetName);
                        part = getPartOfExcel(sheetName);
                        setDataFromExcel(worksheet, format, rowid, part, bidder_id, vendor_code, bid_no, bid_revision, schedule_name, version, sheetName);

                    }
                }
                //var worksheet = workbook.Worksheets.First();

                //string sheetName = worksheet.Name;
                ////string 
                //var ThatList = worksheet.Tables.First().ConvertTableToObjects<ExcelData>();
                //foreach (var data in ThatList)
                //{
                //    Console.WriteLine(data.Id + data.Name + data.Gender);
                //}

                //package.Save();
            }

            return result;

        }
        public string uploadData(int rowid, FileInfo file, string empID)
        {
            string result = "";
            //var package = new ExcelPackage();
            //package.Load(stream);
            this.empID = empID;
            string format = "";
            string part = "";
            using (var package = new ExcelPackage(file))
            {
                var workbook = package.Workbook;

                foreach (var worksheet in workbook.Worksheets)
                {
                    string sheetName = worksheet.Name;

                    if (sheetName.Length > 4)
                    {
                        string cname = sheetName.Substring(0, 4);
                        if ((!(sheetName.Equals("SumAll")) && cname != "Part"))
                        {
                            //string a = worksheet.Cells["A11"].Value.ToString();
                            format = checkFormatExcel(sheetName);
                            part = getPartOfExcel(sheetName);
                            setDataFromExcel(worksheet, format, rowid, part, "", "", "TS12-S-02", 0, "", 0, sheetName);
                        }


                    }
                    else
                    {
                        format = checkFormatExcel(sheetName);
                        part = getPartOfExcel(sheetName);
                        setDataFromExcel(worksheet, format, rowid, part, "", "", "TS12-S-02", 0, "", 0, sheetName);

                    }
                }
                //var worksheet = workbook.Worksheets.First();

                //string sheetName = worksheet.Name;
                ////string 
                //var ThatList = worksheet.Tables.First().ConvertTableToObjects<ExcelData>();
                //foreach (var data in ThatList)
                //{
                //    Console.WriteLine(data.Id + data.Name + data.Gender);
                //}

                //package.Save();
            }

            return result;
            //string result = "";
            ////var package = new ExcelPackage();
            ////package.Load(stream);
            //this.empID = empID;
            //string format = "";
            //string part = "";
            //using (var package = new ExcelPackage(file))
            //{
            //    var workbook = package.Workbook;

            //    foreach (var worksheet in workbook.Worksheets)
            //    {
            //        string sheetName = worksheet.Name;

            //        if (sheetName.Length > 4)
            //        {
            //            string cname = sheetName.Substring(0, 4);
            //            if ((!(sheetName.Equals("SumAll")) && cname != "Part"))
            //            {
            //                //string a = worksheet.Cells["A11"].Value.ToString();
            //                format = checkFormatExcel(sheetName);
            //                part = getPartOfExcel(sheetName);
            //                setDataFromExcel(worksheet, format, rowid, part);
            //            }


            //        }
            //        else
            //        {
            //            format = checkFormatExcel(sheetName);
            //            part = getPartOfExcel(sheetName);
            //            setDataFromExcel(worksheet, format, rowid, part);

            //        }
            //    }
            //    //var worksheet = workbook.Worksheets.First();

            //    //string sheetName = worksheet.Name;
            //    ////string 
            //    //var ThatList = worksheet.Tables.First().ConvertTableToObjects<ExcelData>();
            //    //foreach (var data in ThatList)
            //    //{
            //    //    Console.WriteLine(data.Id + data.Name + data.Gender);
            //    //}

            //    //package.Save();
            //}

            //return result;
        }
        public string checkFormatExcel(string part)
        {
            string format = "";
            //sub
            if (part.Length > 1 && (part.Substring(0, 2).ToUpper().Equals("AB")
                          || part.Substring(0, 1).ToUpper().Equals("D") || part.Substring(0, 2).ToUpper().Equals("_D")))
            {
                format = "Substation1";
            }
            if (part.Length > 1 && part.Substring(0, 1).ToUpper().Equals("C"))
            {
                format = "Substation2";
            }
            //line

            if (part.Length == 1 && (part.ToUpper().Equals("A")
           || part.ToUpper().Equals("I") || part.ToUpper().Equals("O")))
            {
                format = "Line1";
            }

            if (part.Length == 1 && (part.ToUpper().Equals("B")
        || part.ToUpper().Equals("C") || part.ToUpper().Equals("D")
        || part.ToUpper().Equals("E") || part.ToUpper().Equals("F")
        || part.ToUpper().Equals("G") || part.ToUpper().Equals("H")
        || part.ToUpper().Equals("J") || part.ToUpper().Equals("K")))
            {
                format = "Line2";
            }

            if (part.Length == 1 && part.ToUpper().Equals("N"))
            {
                format = "Line3";
            }
            if (part.Length == 1 && part.ToUpper().Equals("Z"))
            {
                format = "Line4";
            }

            //Supply
            if (format_input == "S")
            {
                format = "Supply";
            }
            return format;
        }
        public string getPartOfExcel(string sheetname)
        {
            string Part = "";

            if (sheetname.Length > 1 && (sheetname.Substring(0, 2).ToUpper().Equals("AB")))
            {
                Part = "AB";
            }
            if (sheetname.Length > 1 && (sheetname.Substring(0, 1).ToUpper().Equals("D")))
            {
                Part = "D";
            }
            if (sheetname.Length > 1 && (sheetname.Substring(0, 2).ToUpper().Equals("_D")))
            {
                Part = "_D";
            }
            if (sheetname.Length > 1 && (sheetname.Substring(0, 1).ToUpper().Equals("C")))
            {
                Part = "C";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("A")))
            {
                Part = "A";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("I")))
            {
                Part = "I";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("O")))
            {
                Part = "O";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("B")))
            {
                Part = "B";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("C")))
            {
                Part = "C";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("D")))
            {
                Part = "D";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("E")))
            {
                Part = "E";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("F")))
            {
                Part = "F";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("G")))
            {
                Part = "G";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("H")))
            {
                Part = "H";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("J")))
            {
                Part = "J";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("K")))
            {
                Part = "K";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("N")))
            {
                Part = "N";
            }
            if (sheetname.Length == 1 && (sheetname.ToUpper().Equals("Z")))
            {
                Part = "Z";
            }


            return Part;
        }
        public bool checkDataFromExcel(OfficeOpenXml.ExcelWorksheet worksheet, string format, string part, string bid_no, string bid_revision, string schedule_name)
        {
            ps_up_excel_transection excelTran = new ps_up_excel_transection();
            List<ps_up_excel_transection> listexcelTran = new List<ps_up_excel_transection>();
            bool result = false;
            int startRow = 0;
            int endRow = 0;

            string strCell = "";
            string type = "";
            string bidno = "";
            string schDuleName = "";
            int version = 0;
            // strCell = "F" + (numRow).ToString() + ":" + "G" + (numRow).ToString();

            //DataTable dtUpload = get_ps_t_vendor_fileupload_byID(rowid);

            //if (dtUpload.Rows.Count > 0)
            //{
            //    bidno = dtUpload.Rows[0]["bid_no"].ToString();
            //    schDuleName = dtUpload.Rows[0]["ref_schedule_no"].ToString();
            //    version = dtUpload.Rows[0]["doc_version"] == null ? 0 : dtUpload.Rows[0]["doc_version"].ToString() == "" ? 0 : Convert.ToInt32(dtUpload.Rows[0]["doc_version"].ToString());
            //}
            DataTable dtTem = new DataTable();
            dtTem = get_ps_xls_template(bid_no, bid_revision, schedule_name);
            int rowCout = 0;
            if (dtTem.Rows.Count > 0)
            {
                switch (format)
                {
                    case "Substation1":
                        startRow = 11;
                        //check row
                        rowCout = dtTem.Select("part = '" + part + "'").Count();

                        if (rowCout == startRow - worksheet.Dimension.End.Row)
                        {

                            for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                            {
                                excelTran = new ps_up_excel_transection();
                                strCell = "A" + (i).ToString();
                                if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                                {
                                    break;
                                }
                                else
                                {

                                    strCell = "A" + (i).ToString();
                                    excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                    strCell = "B" + (i).ToString();
                                    excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                    strCell = "D" + (i).ToString();
                                    excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                    strCell = "E" + (i).ToString();
                                    excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                    strCell = "F" + (i).ToString();
                                    excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                    strCell = "G" + (i).ToString();
                                    excelTran.cif_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                    strCell = "H" + (i).ToString();
                                    excelTran.cif_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;

                                    strCell = "I" + (i).ToString();
                                    excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                    strCell = "J" + (i).ToString();
                                    excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;

                                    strCell = "K" + (i).ToString();
                                    excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                    strCell = "L" + (i).ToString();
                                    excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;
                                    listexcelTran.Add(excelTran);

                                }
                            }

                        }
                        else
                        {
                            result = false;

                        }

                        break;
                    case "Substation2":
                        startRow = 11;
                        for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                        {
                            excelTran = new ps_up_excel_transection();
                            strCell = "A" + (i).ToString();
                            if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                            {
                                break;
                            }
                            else
                            {
                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "D" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "E" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "F" + (i).ToString();
                                excelTran.lc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "G" + (i).ToString();
                                excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());



                                listexcelTran.Add(excelTran);

                            }
                        }



                        break;
                    case "Line1":
                        startRow = 12;
                        for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                        {
                            excelTran = new ps_up_excel_transection();
                            strCell = "A" + (i).ToString();
                            if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                            {
                                break;
                            }
                            else
                            {

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "C" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "D" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();


                                strCell = "E" + (i).ToString();
                                excelTran.lci_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "F" + (i).ToString();
                                excelTran.lci_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());


                            }
                        }


                        break;

                    case "Line2":
                        startRow = 10;
                        for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                        {
                            excelTran = new ps_up_excel_transection();
                            strCell = "A" + (i).ToString();
                            if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                            {
                                break;
                            }
                            else
                            {

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "C" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "D" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "E" + (i).ToString();
                                excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "F" + (i).ToString();
                                excelTran.cif_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "G" + (i).ToString();
                                excelTran.cif_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "H" + (i).ToString();
                                excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "I" + (i).ToString();
                                excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "J" + (i).ToString();
                                excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "K" + (i).ToString();
                                excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());
                                listexcelTran.Add(excelTran);

                            }
                        }


                        break;

                    case "Line3":
                        startRow = 10;
                        for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                        {
                            excelTran = new ps_up_excel_transection();
                            strCell = "A" + (i).ToString();
                            if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                            {
                                break;
                            }
                            else
                            {

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "C" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "D" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "E" + (i).ToString();
                                excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "F" + (i).ToString();
                                excelTran.fc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "G" + (i).ToString();
                                excelTran.fc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "H" + (i).ToString();
                                excelTran.lc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "I" + (i).ToString();
                                excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());


                                listexcelTran.Add(excelTran);

                            }
                        }


                        break;

                    case "Line4":


                        startRow = 10;
                        for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                        {
                            excelTran = new ps_up_excel_transection();
                            strCell = "A" + (i).ToString();
                            if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                            {
                                break;
                            }
                            else
                            {

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "C" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "D" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "E" + (i).ToString();
                                excelTran.pod_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "F" + (i).ToString();
                                excelTran.pod_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                listexcelTran.Add(excelTran);

                            }
                        }



                        break;

                    case "Supply":

                        startRow = 11;
                        for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                        {
                            excelTran = new ps_up_excel_transection();
                            strCell = "A" + (i).ToString();
                            if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                            {
                                break;
                            }
                            else
                            {

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "D" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "E" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "F" + (i).ToString();
                                excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "G" + (i).ToString();
                                excelTran.fob_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "H" + (i).ToString();
                                excelTran.cottp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "I" + (i).ToString();
                                excelTran.cfr_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "J" + (i).ToString();
                                excelTran.cfr_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                //strCell = "K" + (i).ToString();
                                //excelTran.lci_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                //strCell = "L" + (i).ToString();
                                //excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;
                                //listexcelTran.Add(excelTran);

                                strCell = "M" + (i).ToString();
                                excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "N" + (i).ToString();
                                excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "O" + (i).ToString();
                                excelTran.fc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "P" + (i).ToString();
                                excelTran.fc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "Q" + (i).ToString();
                                excelTran.lc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "R" + (i).ToString();
                                excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                //strCell = "S" + (i).ToString();
                                //excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                //strCell = "T" + (i).ToString();
                                //excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            }
                        }



                        break;

                }

            }

            return result;
        }
        public string setDataFromExcel(OfficeOpenXml.ExcelWorksheet worksheet, string format, int rowid, string part, string bidder_id, string vendor_code, string bid_no, int bid_revision, string schedule_name, int version, string sheetName)
        {
            ps_up_excel_transection excelTran = new ps_up_excel_transection();
            List<ps_up_excel_transection> listexcelTran = new List<ps_up_excel_transection>();
            string result = "";
            int startRow = 0;
            int endRow = 0;

            string strCell = "";
            string type = "";
            string bidno = "";
            string schDuleName = "";
            //int version = 0;
            // strCell = "F" + (numRow).ToString() + ":" + "G" + (numRow).ToString();

            //DataTable dtUpload = get_ps_t_vendor_fileupload_byID(rowid);

            //if(dtUpload.Rows.Count >0)
            //{
            //    bidno = dtUpload.Rows[0]["bid_no"].ToString();
            //    //schDuleName = dtUpload.Rows[0]["ref_schedule_no"].ToString();
            //   // version = dtUpload.Rows[0]["doc_version"] == null ? 0 : dtUpload.Rows[0]["doc_version"].ToString() == "" ? 0 : Convert.ToInt32(dtUpload.Rows[0]["doc_version"].ToString());
            //}

            switch (format)
            {
                case "Substation1":
                    startRow = 11;
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {
                            excelTran.vender_file_id = rowid;
                            excelTran.created_by = empID;
                            excelTran.bid_no = bid_no;
                            excelTran.schedule_name = schedule_name;
                            excelTran.type = format;
                            excelTran.version = version;
                            excelTran.part = sheetName;
                            excelTran.bidder_id = bidder_id;
                            excelTran.vendor_code = vendor_code;
                            excelTran.bid_revision = bid_revision;

                            strCell = "EE" + (1).ToString();
                            excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "A" + (i).ToString();
                            excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "B" + (i).ToString();
                            excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "D" + (i).ToString();
                            excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "E" + (i).ToString();
                            excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "F" + (i).ToString();
                            excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "G" + (i).ToString();
                            excelTran.cif_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "H" + (i).ToString();
                            excelTran.cif_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;

                            strCell = "I" + (i).ToString();
                            excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "J" + (i).ToString();
                            excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;

                            strCell = "K" + (i).ToString();
                            excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "L" + (i).ToString();
                            excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;
                            listexcelTran.Add(excelTran);

                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }

                    break;
                case "Substation2":
                    startRow = 11;
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {
                            excelTran.vender_file_id = rowid;
                            excelTran.created_by = empID;
                            excelTran.bid_no = bid_no;
                            excelTran.schedule_name = schedule_name;
                            excelTran.type = format;
                            excelTran.version = version;
                            excelTran.part = sheetName;
                            excelTran.bidder_id = bidder_id;
                            excelTran.vendor_code = vendor_code;
                            excelTran.bid_revision = bid_revision;

                            strCell = "EE" + (1).ToString();
                            excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "A" + (i).ToString();
                            excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "B" + (i).ToString();
                            excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "D" + (i).ToString();
                            excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "E" + (i).ToString();
                            excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "F" + (i).ToString();
                            excelTran.lc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "G" + (i).ToString();
                            excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());



                            listexcelTran.Add(excelTran);

                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }

                    break;
                case "Line1":
                    startRow = 12;
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {


                            excelTran.vender_file_id = rowid;
                            excelTran.created_by = empID;
                            excelTran.bid_no = bid_no;
                            excelTran.schedule_name = schedule_name;
                            excelTran.type = format;
                            excelTran.version = version;
                            excelTran.part = sheetName;
                            excelTran.bidder_id = bidder_id;
                            excelTran.vendor_code = vendor_code;
                            excelTran.bid_revision = bid_revision;

                            strCell = "EE" + (1).ToString();
                            excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "A" + (i).ToString();
                            excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "B" + (i).ToString();
                            excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "C" + (i).ToString();
                            excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "D" + (i).ToString();
                            excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();


                            strCell = "E" + (i).ToString();
                            excelTran.lci_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "F" + (i).ToString();
                            excelTran.lci_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());


                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }
                    break;

                case "Line2":

                    if (part != "H")
                    {
                        startRow = 10;
                    }
                    else
                    {
                        startRow = 11;
                    }
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {
                            if (part != "H")
                            {

                                excelTran.vender_file_id = rowid;
                                excelTran.created_by = empID;
                                excelTran.bid_no = bid_no;
                                excelTran.schedule_name = schedule_name;
                                excelTran.type = format;
                                excelTran.version = version;
                                excelTran.part = sheetName;
                                excelTran.bidder_id = bidder_id;
                                excelTran.vendor_code = vendor_code;
                                excelTran.bid_revision = bid_revision;

                                strCell = "EE" + (1).ToString();
                                excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "C" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "D" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "E" + (i).ToString();
                                excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "F" + (i).ToString();
                                excelTran.cif_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "G" + (i).ToString();
                                excelTran.cif_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "H" + (i).ToString();
                                excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "I" + (i).ToString();
                                excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "J" + (i).ToString();
                                excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "K" + (i).ToString();
                                excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());
                                listexcelTran.Add(excelTran);
                            }
                            else
                            {
                                excelTran.vender_file_id = rowid;
                                excelTran.created_by = empID;
                                excelTran.bid_no = bid_no;
                                excelTran.schedule_name = schedule_name;
                                excelTran.type = format;
                                excelTran.version = version;
                                excelTran.part = sheetName;
                                excelTran.bidder_id = bidder_id;
                                excelTran.vendor_code = vendor_code;
                                excelTran.bid_revision = bid_revision;

                                strCell = "EE" + (1).ToString();
                                excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "A" + (i).ToString();
                                excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "B" + (i).ToString();
                                excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "D" + (i).ToString();
                                excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "E" + (i).ToString();
                                excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "F" + (i).ToString();
                                excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                                strCell = "G" + (i).ToString();
                                excelTran.cif_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "H" + (i).ToString();
                                excelTran.cif_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;

                                strCell = "I" + (i).ToString();
                                excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "J" + (i).ToString();
                                excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;

                                strCell = "K" + (i).ToString();
                                excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                                strCell = "L" + (i).ToString();
                                excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;
                                listexcelTran.Add(excelTran);
                            }

                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }
                    break;

                case "Line3":
                    startRow = 10;
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {


                            excelTran.vender_file_id = rowid;
                            excelTran.created_by = empID;
                            excelTran.bid_no = bid_no;
                            excelTran.schedule_name = schedule_name;
                            excelTran.type = format;
                            excelTran.version = version;
                            excelTran.part = sheetName;
                            excelTran.bidder_id = bidder_id;
                            excelTran.vendor_code = vendor_code;
                            excelTran.bid_revision = bid_revision;

                            strCell = "EE" + (1).ToString();
                            excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "A" + (i).ToString();
                            excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "B" + (i).ToString();
                            excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "C" + (i).ToString();
                            excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "D" + (i).ToString();
                            excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "E" + (i).ToString();
                            excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "F" + (i).ToString();
                            excelTran.fc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "G" + (i).ToString();
                            excelTran.fc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "H" + (i).ToString();
                            excelTran.lc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "I" + (i).ToString();
                            excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());


                            listexcelTran.Add(excelTran);

                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }
                    break;

                case "Line4":


                    startRow = 10;
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {


                            excelTran.vender_file_id = rowid;
                            excelTran.created_by = empID;
                            excelTran.bid_no = bid_no;
                            excelTran.schedule_name = schedule_name;
                            excelTran.type = format;
                            excelTran.version = version;
                            excelTran.part = sheetName;
                            excelTran.bidder_id = bidder_id;
                            excelTran.vendor_code = vendor_code;
                            excelTran.bid_revision = bid_revision;

                            strCell = "EE" + (1).ToString();
                            excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "A" + (i).ToString();
                            excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "B" + (i).ToString();
                            excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "C" + (i).ToString();
                            excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "D" + (i).ToString();
                            excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "E" + (i).ToString();
                            excelTran.pod_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "F" + (i).ToString();
                            excelTran.pod_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            listexcelTran.Add(excelTran);

                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }

                    break;

                case "Supply":

                    startRow = 11;
                    for (int i = startRow; i < worksheet.Dimension.End.Row; i++)
                    {
                        excelTran = new ps_up_excel_transection();
                        strCell = "A" + (i).ToString();
                        if (worksheet.Cells[strCell].Value.ToString().Substring(0, 2) == "To")
                        {
                            break;
                        }
                        else
                        {
                            excelTran.vender_file_id = rowid;
                            excelTran.created_by = empID;
                            excelTran.bid_no = bid_no;
                            excelTran.schedule_name = schedule_name;
                            excelTran.type = format;
                            excelTran.version = version;
                            excelTran.part = sheetName;
                            excelTran.bidder_id = bidder_id;
                            excelTran.vendor_code = vendor_code;
                            excelTran.bid_revision = bid_revision;

                            strCell = "EE" + (1).ToString();
                            excelTran.template_id = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();
                            strCell = "A" + (i).ToString();
                            excelTran.item_no = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "B" + (i).ToString();
                            excelTran.description = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "D" + (i).ToString();
                            excelTran.qty = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "E" + (i).ToString();
                            excelTran.unit = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "F" + (i).ToString();
                            excelTran.currency = worksheet.Cells[strCell].Value == null ? "" : worksheet.Cells[strCell].Value.ToString();

                            strCell = "G" + (i).ToString();
                            excelTran.fob_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "H" + (i).ToString();
                            excelTran.cottp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "I" + (i).ToString();
                            excelTran.cfr_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "J" + (i).ToString();
                            excelTran.cfr_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            //strCell = "K" + (i).ToString();
                            //excelTran.lci_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            //strCell = "L" + (i).ToString();
                            //excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString()); ;
                            //listexcelTran.Add(excelTran);

                            strCell = "M" + (i).ToString();
                            excelTran.ewp_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "N" + (i).ToString();
                            excelTran.ewp_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "O" + (i).ToString();
                            excelTran.fc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "P" + (i).ToString();
                            excelTran.fc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "Q" + (i).ToString();
                            excelTran.lc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            strCell = "R" + (i).ToString();
                            excelTran.lc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            //strCell = "S" + (i).ToString();
                            //excelTran.ltc_unit_price = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                            //strCell = "T" + (i).ToString();
                            //excelTran.ltc_amount = worksheet.Cells[strCell].Value == null ? 0 : worksheet.Cells[strCell].Value.ToString() == "" ? 0 : Convert.ToDecimal(worksheet.Cells[strCell].Value.ToString());

                        }
                    }

                    foreach (var item in listexcelTran)
                    {
                        insert_ps_up_excel_transection(item);
                    }

                    break;

            }



            return result;
        }
        public DataTable get_ps_t_vendor_fileupload_byID(int rowid)
        {
            string sqlFull = "";
            string sql = @"SELECT * FROM [dbo].[ps_t_bid_selling_fileupload] ";
            string condi = "Where ";
            string order = " ";
            bool chk = false;

            condi = condi + "row_id = " + rowid;
            //if (!chk)
            //{
            //    sqlFull = sql + order;
            //}
            //else
            //{
            sqlFull = sql + condi + order;
            //}
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            return dtList;
        }
        public DataTable get_ps_xls_template(string bid_no, string bid_revision, string schedule_name)
        {
            string sqlFull = "";
            string sql = @"SELECT * FROM [dbo].[ps_xls_template] ";
            string condi = "Where ";
            string order = " ";
            bool chk = false;

            condi = condi + "bid_no ='" + bid_no + "' and bid_revision = '" + bid_revision + "' and schedule_name ='" + schedule_name + "'";
            //if (!chk)
            //{
            //    sqlFull = sql + order;
            //}
            //else
            //{
            sqlFull = sql + condi + order;
            //}
            DataTable dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            return dtList;
        }
        public List<int> insert_ps_up_excel_transection(ps_up_excel_transection excel_transection)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_up_excel_transection (";
                cmdstr = cmdstr + getNameForInsert(excel_transection) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(excel_transection) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }
        #endregion

        #region " Check data excel "
        //public bool setDataFromExcel( string bidNO, string schedule_name,int bid_revision )
        //{
        //    bool result = true;
        //    //,
        //    DataTable dttemp = new DataTable();
        //    DataTable dttran = new DataTable();

        //    if(dttemp.Rows.Count != dttran.Rows.Count)
        //    {
        //        result = false;
        //    }
        //    else
        //    {
        //        foreach (DataRow drTem in dttemp.Rows)
        //        {
        //            foreach (DataRow drTran in dttran.Rows)
        //            {
        //                if (drTem["item_no"].ToString() == drTran["item_no"].ToString())
        //                {
        //                    if (drTem["description"].ToString() != drTran["description"].ToString())
        //                    {
        //                        result = false;
        //                        break;
        //                    }
        //                    if (drTem["qty"].ToString() != drTran["qty"].ToString())
        //                    {
        //                        result = false;
        //                        break;
        //                    }
        //                    if (drTem["unit"].ToString() != drTran["unit"].ToString())
        //                    {
        //                        result = false;
        //                        break;
        //                    }
        //                }
        //            }
        //        }

        //    }



        //    return result;
        //}
        #endregion

        public static string[] SplitExcelLimit5Tab(string FileName)
        {
            ArrayList arrResult = new ArrayList();
            FileInfo theFile = new FileInfo(FileName);
            using (ExcelPackage xlPackage = new ExcelPackage(theFile))
            {
                var workbook = xlPackage.Workbook;
                if (workbook != null)
                {
                    int iCountSheet = workbook.Worksheets.Count / 5;
                    if ((workbook.Worksheets.Count % 5) > 0)
                        iCountSheet += 1;

                    for (int iSheet = 1; iSheet <= iCountSheet; iSheet++)
                    {
                        FileInfo newFile = new FileInfo(theFile.FullName.Replace(theFile.Extension, "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + "_" + iSheet + theFile.Extension));
                        ExcelPackage newExcel = new ExcelPackage(newFile);

                        int iStart = 1;
                        int iEnd = 5;

                        iStart = (5 * (iSheet - 1)) + 1;
                        iEnd = 5 * iSheet;
                        if (iEnd > workbook.Worksheets.Count)
                            iEnd = workbook.Worksheets.Count;

                        for (int j = iStart - 1; j <= iEnd - 1; j++)
                        {
                            newExcel.Workbook.Worksheets.Add(workbook.Worksheets[j].Name, workbook.Worksheets[j]);
                        }

                        newExcel.Save();
                        arrResult.Add(newFile.FullName);
                    }
                }
            }
            return (string[])arrResult.ToArray(typeof(string));
        }
        public string getNameForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;
            PropertyInfo[] properties = _class.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!property.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {
                        value = property.Name;
                    }
                    else
                    {
                        value = property.Name + ",";
                    }
                    result = result + value;
                }
                count++;

            }
            return result;
        }
        public string cutLastString(string item)
        {
            string result = "";
            result = item.Substring(0, item.Length - 1);
            return result;
        }
        public string getValueForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;

            PropertyInfo[] properties = _class.GetType().GetProperties();


            foreach (var p in properties)
            {
                if (!p.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {

                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {
                            value = "GETDATE()" + "";
                        }
                        else
                        {
                            object a = p.PropertyType;
                            if (p.PropertyType.Name == "String")
                            {
                                value = "'" + p.GetValue(_class) + "'";
                            }
                            else
                            {
                                value = p.GetValue(_class) + "";
                            }
                        }


                    }
                    else
                    {
                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {

                            value = "GETDATE()" + ",";

                        }
                        else
                        {

                            if (p.PropertyType.Name == "String")
                            {
                                value = "'" + p.GetValue(_class) + "'" + ",";
                            }
                            else
                            {
                                value = p.GetValue(_class) + ",";
                            }
                        }

                    }
                    result = result + value;
                }
                count++;
            }


            return result;
        }
    }

    #region " Data Class "
    public class ps_xls_template
    {

        public int rowid { get; set; }
        public string template_id { get; set; }

        public string schedule_name { get; set; }
        public string bid_no { get; set; }
        public string bid_revision { get; set; }

        public DateTime gen_datetime { get; set; }
        public string type { get; set; }
        public string item_no { get; set; }
        public string description { get; set; }
        public decimal qty { get; set; }
        public string unit { get; set; }
        public decimal cif_unit_price { get; set; }
        public decimal lci_unit_price { get; set; }
        public decimal lc_unit_price { get; set; }
        public decimal ltc_unit_price { get; set; }
        public decimal fc_unit_price { get; set; }
        public decimal pod_unit_price { get; set; }
        public decimal fob_unit_price { get; set; }
        public decimal cottp_unit_price { get; set; }
        public decimal cfr_unit_price { get; set; }
        public decimal ewp_unit_price { get; set; }
        public decimal cif_amount { get; set; }
        public decimal lci_amount { get; set; }
        public decimal lc_amount { get; set; }
        public decimal ltc_amount { get; set; }
        public decimal fc_amount { get; set; }
        public decimal pod_amount { get; set; }
        public decimal fob_amount { get; set; }
        public decimal cottp_amount { get; set; }
        public decimal cfr_amount { get; set; }
        public decimal ewp_amount { get; set; }
        public decimal oc_amount { get; set; }
        public int version { get; set; }
        public string currency { get; set; }
        public decimal CostCenter { get; set; }
        public string WBSNo { get; set; }
        public string part { get; set; }

        public string equip_code { get; set; }
        public decimal std_price { get; set; }
        public string created_by { get; set; }
        public DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public DateTime updated_datetime { get; set; }

    }
    public class ps_up_excel_transection
    {

        //       [rowid] [int] IDENTITY(1,1) NOT NULL,

        //   [vender_file_id] [int] NULL,
        //[schedule_name] [nvarchar] (255) NULL,
        //[bid_no] [nvarchar] (50) NULL,
        //[type] [nvarchar] (50) NULL,
        //[item_no] [nvarchar] (50) NULL,
        //[description] [nvarchar] (250) NULL,
        //[qty] [int] NULL,
        //[unit] [nvarchar] (50) NULL,
        //[unit_price1] [decimal] NULL,
        //[unit_price2] [decimal] NULL,
        //[unit_price3] [decimal] NULL,
        //[unit_price4] [decimal] NULL,
        //[amount1] [decimal] NULL,
        //[amount2] [decimal] NULL,
        //[amount3] [decimal] NULL,
        //[amount4] [decimal] NULL,
        //[version] [int] NULL,
        //[currency] [nvarchar] (50) NULL,
        //[created_by] [nvarchar] (250) NOT NULL,

        //    [created_datetime] [datetime]
        //       NOT NULL,

        //    [updated_by] [nvarchar] (250) NOT NULL,

        //     [updated_datetime] [datetime]
        //       NOT NULL
        public int rowid { get; set; }
        public int vender_file_id { get; set; }
        public string bidder_id { get; set; }
        public string vendor_code { get; set; }
        public string type { get; set; }
        public string template_id { get; set; }
        public string bid_no { get; set; }
        public int bid_revision { get; set; }
        public string schedule_name { get; set; }
        public string item_no { get; set; }
        public string description { get; set; }
        public decimal qty { get; set; } = 0;
        public string unit { get; set; }
        public decimal cif_unit_price { get; set; } = 0;
        public decimal lci_unit_price { get; set; } = 0;
        public decimal lc_unit_price { get; set; } = 0;
        public decimal ltc_unit_price { get; set; } = 0;
        public decimal fc_unit_price { get; set; } = 0;
        public decimal pod_unit_price { get; set; } = 0;
        public decimal fob_unit_price { get; set; } = 0;
        public decimal cottp_unit_price { get; set; } = 0;
        public decimal cfr_unit_price { get; set; } = 0;
        public decimal ewp_unit_price { get; set; } = 0;
        public decimal cif_amount { get; set; } = 0;
        public decimal lci_amount { get; set; } = 0;
        public decimal lc_amount { get; set; } = 0;
        public decimal ltc_amount { get; set; } = 0;
        public decimal fc_amount { get; set; } = 0;
        public decimal pod_amount { get; set; } = 0;
        public decimal fob_amount { get; set; } = 0;
        public decimal cottp_amount { get; set; } = 0;
        public decimal cfr_amount { get; set; } = 0;
        public decimal ewp_amount { get; set; } = 0;
        public decimal oc_amount { get; set; } = 0;
        public int version { get; set; } = 0;
        public string currency { get; set; }
        // public decimal CostCenter { get; set; } = 0;
        public string created_by { get; set; }
        public DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public DateTime updated_datetime { get; set; }
        public string part { get; set; }

    }
    public class XLSPS_SumSheet
    {

        //headder
        public string header_l1_bidno { get; set; }
        public string header_l2_ScheduleName { get; set; }
        public string header_l3_biddesc { get; set; }
        public string header_l4_project { get; set; }

        //column
        public string column_1_l1 { get; set; }
        public string column_2_l1 { get; set; }
        public string column_3_l1 { get; set; }
        public string column_3_l2_c1 { get; set; }
        public string column_3_l3_c1 { get; set; }
        public string column_3_l6_c1 { get; set; }
        public string column_3_l2_c2 { get; set; }
        public string column_3_l3_c2 { get; set; }
        public string column_3_l4_c2 { get; set; }
        public string column_3_l5_c2 { get; set; }
        public string column_3_l6_c2 { get; set; }
        public string column_4_l1 { get; set; }
        public string column_4_l2 { get; set; }
        public string column_4_l3 { get; set; }
        public string column_4_l4 { get; set; }
        public string column_4_l5 { get; set; }
        public string column_4_l6 { get; set; }
        public string column_5_l1 { get; set; }
        public string column_5_l2 { get; set; }
        public string column_5_l3 { get; set; }
        public string column_5_l4 { get; set; }
        public string column_5_l5 { get; set; }
        public string column_5_l6 { get; set; }
        public string column_6_l1 { get; set; }
        public string column_6_l2 { get; set; }
        public string column_6_l3 { get; set; }
        public string column_6_l4 { get; set; }
        public string column_6_l5 { get; set; }
        public string column_6_l6 { get; set; }

        public string column_7_l1 { get; set; }
        public string column_7_l4 { get; set; }
        public string column_7_l5 { get; set; }
        public string column_7_l6 { get; set; }

        public string column_8_l1 { get; set; }
        public string column_8_l4 { get; set; }
        public string column_8_l5 { get; set; }
        public string column_8_l6 { get; set; }




        //footer 
        //1
        public string footer_1_l1 { get; set; }
        public string footer_1_l2 { get; set; }
        public string footer_1_l3 { get; set; }
        public string footer_1_l4 { get; set; }
        public string footer_4_l1 { get; set; }
        public string footer_4_l2 { get; set; }
        public string footer_4_l3 { get; set; }
        public string footer_4_l4 { get; set; }
        public string footer_5_l1 { get; set; }
        public string footer_5_l2 { get; set; }
        public string footer_5_l3 { get; set; }
        public string footer_5_l4 { get; set; }
        public string footer_7_l1 { get; set; }
        public string footer_7_l2 { get; set; }
        public string footer_7_l3 { get; set; }
        public string footer_7_l4 { get; set; }

        //2
        public string footer_1_l5 { get; set; }
        public string footer_1_l6 { get; set; }
        public string footer_1_l7 { get; set; }
        public string footer_1_l8 { get; set; }
        public string footer_4_l5 { get; set; }
        public string footer_4_l6 { get; set; }
        public string footer_4_l7 { get; set; }
        public string footer_4_l8 { get; set; }
        public string footer_5_l5 { get; set; }
        public string footer_5_l6 { get; set; }
        public string footer_5_l7 { get; set; }
        public string footer_5_l8 { get; set; }
        public string footer_7_l5 { get; set; }
        public string footer_7_l6 { get; set; }
        public string footer_7_l7 { get; set; }
        public string footer_7_l8 { get; set; }

        //3
        public string footer_1_l9 { get; set; }
        public string footer_1_l10 { get; set; }
        public string footer_1_l11 { get; set; }
        public string footer_1_l12 { get; set; }
        public string footer_4_l9 { get; set; }
        public string footer_4_l10 { get; set; }
        public string footer_4_l11 { get; set; }
        public string footer_4_l12 { get; set; }
        public string footer_5_l9 { get; set; }
        public string footer_5_l10 { get; set; }
        public string footer_5_l11 { get; set; }
        public string footer_5_l12 { get; set; }
        public string footer_7_l9 { get; set; }
        public string footer_7_l10 { get; set; }
        public string footer_7_l11 { get; set; }
        public string footer_7_l12 { get; set; }

        public string footer_3_l1_c1 { get; set; }
        public string footer_3_l1_c2 { get; set; }
        public string footer_6_l1 { get; set; }

    }
    public class XLSPS_ChildSheet
    {
        public string header_l1_bidno { get; set; }
        public string header_l2_PartName { get; set; }
        public string header_l3_biddesc { get; set; }
        public string header_l4_project { get; set; }

        //column
        public string column_1_l1 { get; set; }
        public string column_2_l1 { get; set; }
        public string column_3_l1 { get; set; }
        public string column_4_l1 { get; set; }

        public string column_4_l2_c1 { get; set; }
        public string column_4_l2_c2 { get; set; }
        public string column_4_l3_c1 { get; set; }
        public string column_4_l3_c2 { get; set; }
        public string column_4_l4_c2 { get; set; }
        public string column_4_l5_c2 { get; set; }
        public string column_4_l6_c1_1 { get; set; }
        public string column_4_l6_c2_1 { get; set; }
        public string column_5_l1 { get; set; }
        public string column_5_l4 { get; set; }
        public string column_5_l5 { get; set; }
        public string column_5_l6_c1 { get; set; }
        public string column_5_l6_c2 { get; set; }
        public string column_6_l1 { get; set; }
        public string column_6_l2 { get; set; }
        public string column_6_l3_c1 { get; set; }
        public string column_6_l3_c2 { get; set; }
        public string column_6_l3_c3 { get; set; }
        public string column_6_l4_c1 { get; set; }
        public string column_6_l4_c2 { get; set; }
        public string column_6_l4_c3_1 { get; set; }
        public string column_6_l4_c3_2 { get; set; }

        public string column_6_l4 { get; set; }
        public string column_6_l5 { get; set; }
        public string column_6_l6_c1 { get; set; }
        public string column_6_l6_c2 { get; set; }
        public string column_6_l2_c1 { get; set; }
        public string column_6_l2_c2 { get; set; }
        public string column_6_l3 { get; set; }
        public string column_6_l5_c2 { get; set; }
        public string column_6_l6_c1_1 { get; set; }
        public string column_6_l6_c1_2 { get; set; }
        public string column_6_l6_c2_1 { get; set; }
        public string column_6_l6_c2_2 { get; set; }

        public string column_6_l6_c3_1 { get; set; }
        public string column_6_l6_c3_2 { get; set; }

        public string column_7_l1 { get; set; }
        public string column_7_l2_c1 { get; set; }
        public string column_7_l2_c2 { get; set; }
        public string column_7_l3_c2 { get; set; }
        public string column_7_l4_c2 { get; set; }
        public string column_7_l5_c2 { get; set; }
        public string column_7_l6_c1_1 { get; set; }
        public string column_7_l6_c1_2 { get; set; }
        public string column_7_l6_c2_1 { get; set; }
        public string column_7_l6_c2_2 { get; set; }
        public string column_7_l6_c3_1 { get; set; }
        public string column_7_l6_c3_2 { get; set; }
        public string column_7_l2 { get; set; }
        public string column_7_l3 { get; set; }
        public string column_7_l3_c1 { get; set; }
        public string column_7_l3_c3 { get; set; }
        public string column_7_l4 { get; set; }
        public string column_7_l5 { get; set; }
        public string column_7_l6_c1 { get; set; }
        public string column_7_l6_c2 { get; set; }
        public string column_8_l1 { get; set; }
        public string column_8_l2 { get; set; }
        public string column_8_l3 { get; set; }
        public string column_8_l4 { get; set; }
        public string column_8_l5 { get; set; }
        public string column_8_l6 { get; set; }
        public string column_8_l6_c1 { get; set; }
        public string column_8_l6_c2 { get; set; }
        public string column_9_l1 { get; set; }
        public string column_9_l2 { get; set; }
        public string column_9_l2_c1 { get; set; }
        public string column_9_l2_c2 { get; set; }
        public string column_9_l2_c3 { get; set; }
        public string column_9_l3 { get; set; }
        public string column_9_l4 { get; set; }
        public string column_9_l5 { get; set; }
        public string column_9_l6 { get; set; }
        public string column_9_l6_c1 { get; set; }
        public string column_9_l6_c2 { get; set; }

        public string column_10_l1 { get; set; }
        public string column_10_l4 { get; set; }
        public string column_10_l5 { get; set; }
        public string column_10_l6_c1 { get; set; }
        public string column_10_l6_c2 { get; set; }

        public string column_11_l1 { get; set; }
        public string column_11_l4 { get; set; }
        public string column_11_l5 { get; set; }
        public string column_11_l6_c1 { get; set; }
        public string column_11_l6_c2 { get; set; }

        public string column_12_l1 { get; set; }
        public string column_12_l4 { get; set; }
        public string column_12_l5 { get; set; }
        public string column_12_l6_c1 { get; set; }
        public string column_12_l6_c2 { get; set; }


        //footer 
        public string footer_1_l1 { get; set; }
        public string footer_4_l1 { get; set; }
        public string footer_5_l1 { get; set; }
        public string footer_6_l1 { get; set; }

        public string footer_7_l1 { get; set; }
        public string footer_8_l1 { get; set; }
        public string footer_9_l1 { get; set; }
        public string footer_10_l1 { get; set; }
        public string footer_11_l1 { get; set; }
        public string footer_12_l1 { get; set; }
        public string footer_13_l1 { get; set; }





    }
    public class XLSPS_PartSheet
    {
        public string header_l1_bidno { get; set; }
        public string header_l2_PartName { get; set; }
        public string header_l3_biddesc { get; set; }
        public string header_l4_project { get; set; }

        //column
        public string column_1_l1 { get; set; }
        public string column_2_l1 { get; set; }
        public string column_2_l2 { get; set; }
        public string column_2_l3 { get; set; }
        public string column_2_l4 { get; set; }
        public string column_2_l5 { get; set; }
        public string column_2_l6 { get; set; }

        public string column_3_l1 { get; set; }

        public string column_3_l2_c1 { get; set; }
        public string column_3_l3_c1 { get; set; }
        public string column_3_l4 { get; set; }
        public string column_3_l5 { get; set; }
        public string column_3_l6 { get; set; }
        public string column_3_l6_c1 { get; set; }
        public string column_3_l2_c2 { get; set; }
        public string column_3_l3_c2 { get; set; }
        public string column_3_l4_c2 { get; set; }
        public string column_3_l5_c2 { get; set; }
        public string column_3_l6_c2 { get; set; }
        public string column_4_l1 { get; set; }
        public string column_4_l4 { get; set; }
        public string column_4_l5 { get; set; }
        public string column_4_l6 { get; set; }

        //footer 
        public string footer_1_l1 { get; set; }
        public string footer_3_l1_c1 { get; set; }
        public string footer_3_l1_c2 { get; set; }
        public string footer_4_l1 { get; set; }
        public string footer_5_l1 { get; set; }

        public string footer_6_l1 { get; set; }

    }
    public class XLSPS_BreakdownSheet
    {
        public string header_l1_bidno { get; set; }
        public string header_l2_PartName { get; set; }
        public string header_l3_biddesc { get; set; }
        public string header_l4_project { get; set; }
        public string header_l5_project { get; set; }

        //column
        public string column_1_l1 { get; set; }
        public string column_2_l1 { get; set; }
        public string column_3_l1 { get; set; }
        public string column_4_l1 { get; set; }
        public string column_5_l1 { get; set; }
        public string column_6_l1 { get; set; }
        public string column_6_l2_c1 { get; set; }
        public string column_6_l2_c2 { get; set; }
        public string column_6_l3 { get; set; }
        public string column_6_l3_c2 { get; set; }
        public string column_6_l4_c2 { get; set; }
        public string column_6_l5_c2 { get; set; }
        public string column_6_l6_c1_1 { get; set; }
        public string column_6_l6_c1_2 { get; set; }
        public string column_6_l6_c2_1 { get; set; }
        public string column_6_l6_c2_2 { get; set; }
        public string column_7_l1 { get; set; }
        public string column_7_l2 { get; set; }
        public string column_7_l3 { get; set; }
        public string column_7_l4 { get; set; }
        public string column_7_l5 { get; set; }
        public string column_7_l6_c1 { get; set; }
        public string column_7_l6_c2 { get; set; }
        //footer 
        public string footer_1_l1 { get; set; }
        public string footer_3_l1_c2 { get; set; }
        public string footer_4_l1 { get; set; }
    }
    public class cChildPart
    {
        public string projecName { get; set; }
        public string bidno { get; set; }
        public string Name { get; set; }
        public string ScheduleName { get; set; }

        public bool isBreakDown { get; set; }
        public string ItemNo { get; set; }
        public string Des { get; set; }
        public string Qty { get; set; }
        public string Unit { get; set; }
        public string Currency { get; set; }
        public string ForeignUnit { get; set; }
        public string ForeignAmount { get; set; }
        public string LocalUnit { get; set; }
        public string LocalAmount { get; set; }
        public string LocalCurUnit { get; set; }
        public string LocalCurAmount { get; set; }
        public string LocalTranUnit { get; set; }
        public string LocalTranAmount { get; set; }
        public string LocalTranConUnit { get; set; }
        public string LocalTranConAmount { get; set; }


    }
    public class cPart
    {
        public string bidno { get; set; }
        public string Name { get; set; }
        public string ScheduleName { get; set; }
        public string Des { get; set; }
        public string Currency { get; set; }
        public string ForeignAmount { get; set; }
        public string LocalUnit { get; set; }
        public string LocalAmount { get; set; }
        public string LocalCurUnit { get; set; }
        public string LocalCurAmount { get; set; }
        public string LocalTranUnit { get; set; }
        public string LocalTranAmount { get; set; }
        public string LocalTranConUnit { get; set; }
        public string LocalTranConAmount { get; set; }
    }
    public class cSumPart
    {
        public string ItemNo { get; set; }
        public string bidno { get; set; }
        public string projecName { get; set; }
        public string Name { get; set; }
        public string ScheduleName { get; set; }
        public string Des { get; set; }
        public string Currency { get; set; }
        public string ForeignUnit { get; set; }
        public string ForeignAmount { get; set; }
        public string LocalUnit { get; set; }
        public string LocalAmount { get; set; }
        public string LocalCurUnit { get; set; }
        public string LocalCurAmount { get; set; }
        public string LocalTranUnit { get; set; }
        public string LocalTranAmount { get; set; }
        public string LocalTranConUnit { get; set; }
        public string LocalTranConAmount { get; set; }
    }
    public class cBreakdown
    {
        public string bidno { get; set; }
        public string Name { get; set; }
        public string ScheduleName { get; set; }
        public bool isBreakDown { get; set; }
        public string ItemNo { get; set; }
        public string Des { get; set; }
        public string Qty { get; set; }
        public string Unit { get; set; }
        public string Currency { get; set; }
        public string ForeignUnit { get; set; }
        public string ForeignAmount { get; set; }
        public string LocalUnit { get; set; }
        public string LocalAmount { get; set; }
        public string LocalCurUnit { get; set; }
        public string LocalCurAmount { get; set; }
        public string LocalTranUnit { get; set; }
        public string LocalTranAmount { get; set; }
        public string LocalTranConUnit { get; set; }
        public string LocalTranConAmount { get; set; }
    }
    #endregion
}