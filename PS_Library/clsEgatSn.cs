﻿using OfficeOpenXml;
using OfficeOpenXml.Style;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class clsEgatSn
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase db = new DbControllerBase();
        public clsEgatSn() { }

        public DataTable GetContractListByStatus(string strSnStatus,string strDept)
        {

            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct contract_no
                                    FROM ps_t_contract_po c
                                    LEFT JOIN ps_m_equipcode e on c.equip_code = e.equip_code
                                    where e.equip_gong = '"+ strDept + "' and e.sn_code_a is not null ";

            //string strSql_BK = @"SELECT DISTINCT contract_no
            //                  FROM ps_t_contract_po ";
            if (strSnStatus == "g")
            {
                strSql += " and is_gen_sn = 'x'";
            }
            else
            {
                strSql += " and is_gen_sn is null";
            }
            strSql += "   order by contract_no asc";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }
         
        public DataTable GetGenSnByContractNo(string strContractNo,string strDept)
        {
            DataTable dt = new DataTable();
            string strSql = @"select po.contract_no,po.schedule_name,po.item_no,e.equip_desc_full as equip_name,e.equip_code,po.qty,sn.from_sn as sn_from ,sn.to_sn as sn_to,LEFT(sn.from_sn,5) as abcde
                                from ps_t_contract_po po
                                left join ps_t_sn sn on po.rowid = sn.contract_po_row_id
                                left join ps_m_equipcode e on e.equip_code = po.equip_code
                                where po.is_gen_sn = 'x'
                                and contract_no ='" + strContractNo + @"'
                                and e.equip_gong = '"+ strDept + @"' and  e.sn_code_a is not null
                                order by po.schedule_name asc,po.item_no asc ,e.equip_desc_full asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;

        }

        public DataTable GetPoDataByContractNo(string strContractNo,string strDept)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT rowid
                                      ,po.contract_no
	                                  ,po.schedule_name
                                      ,po.po_no
                                      ,po.po_item
                                      ,po.item_no
                                      ,po.equip_code 
	                                  ,e.sn_code_a
	                                  ,e.sn_code_b 
	                                  ,e.sn_code_c
	                                  ,e.sn_code_d
	                                  ,e.sn_code_e
                                      ,CONCAT(e.sn_code_a,e.sn_code_b,e.sn_code_c,e.sn_code_d,e.sn_code_e) as abcde   
	                                  ,e.equip_desc_full as equip_name
                                      ,po.qty
	                                  ,po.total_price
                                      ,po.is_gen_sn
                              FROM ps_t_contract_po po
                             left join ps_m_equipcode e on e.equip_code = po.equip_code
                             where contract_no = '" + strContractNo + "' and e.equip_gong = '" + strDept + @"' and e.sn_code_a is not null ";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public void UpdateMaxGhi(string strGhi, string strAbcde)
        {
            string strSql = @"BEGIN
                                  IF EXISTS (select ghi_max from ps_m_sn_maxghi where abcde = '" + strAbcde + @"')
                                     BEGIN
                                          UPDATE ps_m_sn_maxghi 
		                                  SET ghi_max = " + strGhi + @"
		                                     ,updated_datetime = GETDATE()
			                                 ,updated_by = 'Admin'
		                                  where abcde = '" + strAbcde + @"'
                                     END
                                  ELSE
	                                 BEGIN
		                                  INSERT INTO ps_m_sn_maxghi (abcde,ghi_max,created_by,created_datetime,updated_by,updated_datetime)
			                                 values('" + strAbcde + "'," + strGhi + @",'Admin',GETDATE(),'Admin',GETDATE()) 
	                                 END
                                 END";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void UpdateFlagPoGenSnByContractNo_BK(string strContractNo)
        {
            string strSql = @"update ps_t_contract_po
                                set  is_gen_sn = 'x'
                                where contract_no = '"+ strContractNo + "'";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void UpdateFlagPoGenSnByContractPoRowIds(string strConcatContractPoRowIds)
        {
            string strSql = @"update ps_t_contract_po
                                set  is_gen_sn = 'x'
                                where rowid in(" + strConcatContractPoRowIds + ")";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void InsertPsTSn(DataTable dtBind)
        {
            string strValues = string.Empty;
            string strSql = @"INSERT INTO ps_t_sn
                               (contract_po_row_id
                               ,abcde
                               ,ghi
                               ,from_sn
                               ,to_sn
                               ,created_by
                               ,created_datetime
                               ,updated_by
                               ,updated_datetime)
                         VALUES";
            foreach (DataRow dr in dtBind.Rows)
            {
                if (strValues != string.Empty)
                {
                    strValues += ",";
                }
                strValues += "(" + dr["contract_po_row_id"].ToString() +
                               ",'" + dr["abcde"].ToString() + "'" +
                               ",'" + dr["ghi"].ToString() + "'" +
                               ",'" + dr["sn_from"].ToString() + "'" +
                               ",'" + dr["sn_to"].ToString() + "'" +
                               ",'Admin'" +
                               ",GETDATE()" +
                               ",'Admin'" +
                               ",GETDATE())";
            }
            strSql = strSql + strValues;
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public int GetMaxGhiByAbcde(string strAbcde)
        {
            int iMaxGhi = 0;
            DataTable dt = new DataTable();
            string strSql = @"select ghi_max from ps_m_sn_maxghi where abcde = '" + strAbcde + "';";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            if (dt.Rows.Count > 0)
            {
                iMaxGhi = int.Parse(dt.Rows[0]["ghi_max"].ToString());
            }
            return iMaxGhi;
        }

        public byte[] GenExcel(DataTable dtExport)
        {
            string strFileName = "Egat_Serial_No_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + ".xlsx";
            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            var package = new ExcelPackage();
            var workbook = package.Workbook;
            var worksheet = workbook.Worksheets.Add("Sheet1");

            worksheet.Column(1).Width = 10;
            worksheet.Column(2).Width = 20;
            worksheet.Column(3).Width = 10;
            worksheet.Column(4).Width = 40;
            worksheet.Column(5).Width = 20;
            worksheet.Column(6).Width = 10;
            worksheet.Column(7).Width = 20;
            worksheet.Column(8).Width = 20;


            // title
            worksheet.Cells["A1:D1"].Merge = true;
            worksheet.Cells["A1:D1"].Value = "EGAT Serial No. for Contract No. : " + dtExport.Rows[0]["contract_no"].ToString();
            worksheet.Cells["A1:D1"].Style.Font.Bold = true;
            worksheet.Cells["A1:D1"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

            // gv header
            // worksheet.Cells["A3:I3"].Value = "Item No'";
            worksheet.Cells["A3:H3"].Style.Font.Bold = true;
            worksheet.Cells["A3:H3"].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
            worksheet.Cells["A3:H3"].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Center;
            worksheet.Cells["A3:H3"].Style.Fill.PatternType = ExcelFillStyle.Solid;
            worksheet.Cells["A3:H3"].Style.Fill.BackgroundColor.SetColor(1, 198, 198, 198);
            //set border
            worksheet.Cells["A3:H3"].Style.Border.Top.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            worksheet.Cells["A3:H3"].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
            //set header value
            worksheet.Cells["A3"].Value = "No";
            worksheet.Cells["B3"].Value = "Schedule Name";
            worksheet.Cells["C3"].Value = "Item No.";
            worksheet.Cells["D3"].Value = "Equipment";
            worksheet.Cells["E3"].Value = "EQ.(Old mat.)";
            worksheet.Cells["F3"].Value = "Qty";
            worksheet.Cells["G3"].Value = "Egat S/N From";
            worksheet.Cells["H3"].Value = "Egat S/N to";

            int iRow = 4;
            string strCellId = string.Empty;

            for (int i = 0; i < dtExport.Rows.Count; i++)
            { 
                strCellId = "A" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = (i + 1).ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "B" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["schedule_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "C" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["item_no"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "D" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_name"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "E" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["equip_code"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "F" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["qty"].ToString();
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "G" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["sn_from"];
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }

                strCellId = "H" + (iRow + i).ToString();
                worksheet.Cells[strCellId].Value = dtExport.Rows[i]["sn_to"];
                worksheet.Cells[strCellId].Style.Border.Left.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.Border.Right.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                worksheet.Cells[strCellId].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Center;
                if (i == (dtExport.Rows.Count - 1))
                {
                    worksheet.Cells[strCellId].Style.Border.Bottom.Style = OfficeOpenXml.Style.ExcelBorderStyle.Thin;
                }
            }


            return package.GetAsByteArray();

            //package.SaveAs(new FileInfo("C:\\Xls\\" + strFileName));

        }
    }
}
