﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;
using System.Reflection;
using System.Web;
using PS_Library.DocumentManagement;
using PS_Library.WorkflowService;

namespace PS_Library
{
    public class clsUploadDocApp
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private string dbOraConnectionString = ConfigurationManager.AppSettings["db_common"].ToString();
        private OTFunctions zotFunctions = new OTFunctions();
        private DbControllerBase db = new DbControllerBase();
         
        public clsUploadDocApp() { } 

        public DataTable GetDocTypeAll() 
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT doctype_code
                                  ,doctype_name
                                  ,doctype_desc
                              FROM ps_m_doctype
                              WHERE isdelete = 0
                              ORDER BY doctype_name asc ";

            dt = db.ExecSql_DataTable(strSql, dbConnectionString);

            return dt;
        } 

        public DataTable GetBom(string strSearchText)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT distinct bom_code as code 
                              ,bom_name as name
                              ,'B' as type
                              FROM ps_m_bom ";
            strSql += " where UPPER(bom_code) like '%" + strSearchText.ToUpper() + "%'";
            strSql += " order by bom_name asc ,bom_code asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetEquipByBomCode(string strBomCode)
        {
            DataTable dt = new DataTable();
            string strSql = "select bom_code,equip_code from ps_m_bom where bom_code = '" + strBomCode + "'";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetEquipment(string strSearchText)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT equip_code as code ,equip_desc_short as name,'E' as type
                                FROM ps_m_equipcode
                                where isactive = 1 ";
            strSql += " and UPPER(equip_code) like '%" + strSearchText.ToUpper() + "%'";
            strSql += " order by equip_desc_short asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetEquipmentGroup(string strSearchText)
        {
            DataTable dt = new DataTable();
            string strSql = @" select eg_code as code , eg_name as name,'G' as type
                             from ps_m_equip_grp
                             where isdelete = 0";
            strSql += " and UPPER(eg_code) like '%" + strSearchText.ToUpper() + "%'";
            strSql += " order by eg_name asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDocAppList_BK(string strdocType, string strDocDesc, string strDocName, string strDocRevision, string strSheetNo, string strRemark)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT d.doctype_code
                                  ,d.rowid
	                              ,d.doctype_name
                                  ,doc_name
                                  ,doc_nodeid
								  ,case when doc_nodeid is null then 'false' else 'true' end as doc_visible
								  ,doc_std_nodeid
								  ,case when doc_std_nodeid is null then 'false' else 'true' end as doc_std_visible
								  ,std_approved_date
                                  ,doc_desc
                                  ,doc_revision_display
                                  ,doc_revision_no
                                  ,doc_sheetno
                                  ,doc_remark 
                                  ,case when pending_add_ver = 1 then 'false' else 'true' end as add_doc_visible
                              FROM ps_m_doc_approve
                              left join ps_m_doctype d on ps_m_doc_approve.doctype_code = d.doctype_code
                              WHERE doc_status = 'Active'";
            if (!String.IsNullOrEmpty(strdocType))
                strSql += " and d.doctype_code = '" + strdocType + "'";
            if (!String.IsNullOrEmpty(strDocDesc))
                strSql += " and doc_desc like '%" + strDocDesc + "%'";
            if (!String.IsNullOrEmpty(strDocName))
                strSql += " and doc_name like '%" + strDocName + "%'";
            if (!String.IsNullOrEmpty(strDocRevision))
                strSql += " and doc_revision_display like '%" + strDocRevision + "%'";
            if (!String.IsNullOrEmpty(strSheetNo))
                strSql += " and doc_sheetno like '%" + strSheetNo + "%'";
            if (!String.IsNullOrEmpty(strRemark))
                strSql += " and doc_remark like '%" + strRemark + "%'";
            strSql += " ORDER BY doctype_code asc,doc_name asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDocAppList(string strdocType, string strDocDesc, string strDocName, string strDocRevision, string strSheetNo, string strRemark,string strUserLogin)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT d.doctype_code
                                  ,d.rowid
	                              ,d.doctype_name
                                  ,doc_name
                                  ,doc_nodeid
								  ,case when doc_nodeid is not null and au.can_read =1 then 'true' else 'false' end as doc_visible
								  ,doc_std_nodeid
								  ,case when doc_std_nodeid is not null and au.can_read =1 then 'true' else 'false' end as doc_std_visible
								  ,case when au.can_edit =1 then 'true' else 'false' end as doc_edit_relate 
								  ,std_approved_date
                                  ,doc_desc
                                  ,doc_revision_display
                                  ,doc_revision_no
                                  ,doc_sheetno
                                  ,doc_remark 
								  ,au.can_read
								  ,au.can_edit
								  ,au.can_delete
                                  ,case when pending_add_ver = 1 or au.can_edit =0 then 'false' else 'true' end as add_doc_visible
                              FROM ps_m_doc_approve
                              left join ps_m_doctype d on ps_m_doc_approve.doctype_code = d.doctype_code
							  left join ps_m_employee_auth au on au.doctype_code = ps_m_doc_approve.doctype_code and au.empid = '" + strUserLogin + @"'
                              WHERE doc_status = 'Active' and d.doctype_code is not null";
            if (!String.IsNullOrEmpty(strdocType))
                strSql += " and d.doctype_code = '" + strdocType + "'";
            if (!String.IsNullOrEmpty(strDocDesc))
                strSql += " and doc_desc like '%" + strDocDesc + "%'";
            if (!String.IsNullOrEmpty(strDocName))
                strSql += " and doc_name like '%" + strDocName + "%'";
            if (!String.IsNullOrEmpty(strDocRevision))
                strSql += " and doc_revision_display like '%" + strDocRevision + "%'";
            if (!String.IsNullOrEmpty(strSheetNo))
                strSql += " and doc_sheetno like '%" + strSheetNo + "%'";
            if (!String.IsNullOrEmpty(strRemark))
                strSql += " and doc_remark like '%" + strRemark + "%'";
            strSql += " ORDER BY doctype_code asc,doc_name asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDocInfoByNodeId(string strNodeId)
        {
            DataTable dt = new DataTable();
            string strSql = @"select da.doc_desc,da.doc_name,da.doc_remark,da.doc_revision_display,da.doc_revision_no,da.doc_sheetno,dt.doctype_desc,dt.doctype_name,da.doctype_code
                            from ps_m_doc_approve da
                            left join ps_m_doctype dt on dt.doctype_code = da.doctype_code
                            where da.doc_nodeid = " + strNodeId + @"
                            order by da.doc_nodeid asc,da.doc_revision_no desc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetBomListByNodeId(string strNodeId)
        {
            DataTable dt = new DataTable();
            string strSql = @"    select distinct bd.bom_code as code,b.bom_name as name,'B' as type
                                  from ps_m_bom_doc bd
                                  left join ps_m_bom b on b.bom_code = bd.bom_code
                                  where bd.doc_nodeid = " + strNodeId + @"
                                  order by bd.bom_code asc
                                ";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetEquipmentListByNodeId(string strNodeId)
        {
            DataTable dt = new DataTable();
            string strSql = @"select distinct ed.equip_code as code,e.equip_desc_full as name ,'E' as type
                                from ps_m_equip_doc ed
                                left join ps_m_equipcode e on ed.equip_code = e.equip_code
                                where doc_nodeid = " + strNodeId + @"
                                order by ed.equip_code asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetEquipmentGroupListByNodeId(string strNodeId)
        {
            DataTable dt = new DataTable();
            string strSql = @"  select distinct egd.eg_code as code,eg.eg_name as name ,'G' as type
                                from ps_m_equip_grp_doc egd
                                left join ps_m_equip_grp eg on egd.eg_code = eg.eg_code
                                where egd.doc_nodeid = " + strNodeId + @"
                                order by egd.eg_code asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetLastRevisionDocApprove(string strDocName, string strDocTypeCode)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT TOP (1) [doc_name]
                              ,[doc_desc]
                              ,[doc_revision_no]
                              ,[doc_revision_display]
                              ,[doc_sheetno]
                              ,[doc_remark]
                          FROM [dbo].[ps_m_doc_approve]
                          where doc_name = '" + strDocName + @"' 
                          and doctype_code = '" + strDocTypeCode + @"'
                          order by updated_datetime desc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetApproverCommon(string strEmpId, string strDept, string strFullName, string strPosition, string strEmpDept)
        {
            DataTable dt = new DataTable();
            string strFilter = string.Empty;
            string strSql = @" SELECT distinct ID, FULLNAME, POSITION_SHORT,  POSITION_FULLNAME
                            FROM V_EMPLOYEE_SEARCH 
                            WHERE ID IS NOT NULL  ";

            if (!String.IsNullOrEmpty(strEmpId)) strFilter += " AND ID LIKE '%" + strEmpId + "%' ";
            if (!String.IsNullOrEmpty(strFullName)) strFilter += " AND UPPER(FULLNAME) LIKE '%" + strFullName.ToUpper() + "%' ";
            if (!String.IsNullOrEmpty(strDept)) strFilter += " AND UPPER(DEPARTMENT_FULLNAME) LIKE '%" + strDept.ToUpper() + "%' ";
            if (!String.IsNullOrEmpty(strPosition)) strFilter += " AND UPPER(POSITION_FULLNAME) LIKE '%" + strPosition.ToUpper() + "%' ";
            if (!String.IsNullOrEmpty(strEmpDept)) strFilter += " AND UPPER(EMPLOYEE_DEPARTMENT) LIKE '%" + strEmpDept.ToUpper() + "%' ";

            strSql += strFilter + " ORDER BY FULLNAME ASC";
            oraDBUtil ora = new oraDBUtil();
            dt = ora._getDT(strSql, dbOraConnectionString);

            return dt;
        }

        public DataTable GetDADataByWorkId(string strWorkId)
        {
            DataTable dt = new DataTable();

            string strSql = @"SELECT m.process_id
                              ,m.ps_da_no
                              ,ps_subject
                              ,project_id
                              ,note
                              ,form_status
                              ,current_step
                              ,emp_id
                              ,emp_name
                              ,emp_position
                              ,emp_position_full
                              ,next_step
                              ,wf_processid
                              ,wf_subprocessid
                              ,wf_taskid
                              ,doc_node_id
                              ,doc_node_id_prev
                              ,doc_node_id_memo
                              ,memo_content
                              ,memo_subject
                              ,process_id_ref
	                          ,d.apv_id
	                          ,d.apv_name
	                          ,d.apv_order
	                          ,d.apv_position
	                          ,d.apv_position_full
	                          ,d.apv_status
                              ,d.apv_apvdate
                          FROM wf_ps_doc_approve m
                          LEFT JOIN wf_ps_doc_approve_detail d on m.process_id = d.process_id
                          WHERE wf_processid = '" + strWorkId + @"'
                          order by d.apv_order asc";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public DataTable GetDocListByNodeId(string strNodeId)
        {
            DataTable dt = new DataTable();
            string strSql = @"SELECT rowid
                              ,doctype_code
                              ,doc_nodeid
                              ,doc_name
                              ,doc_desc
                              ,doc_revision_no
                              ,doc_revision_display
                              ,doc_revision_date
                              ,doc_sheetno
                              ,depart_position_name
                              ,section_position_name
                              ,doc_remark
                              ,doc_status
                              ,created_by
                              ,created_datetime
                              ,updated_by
                              ,updated_datetime
                              ,doc_std_nodeid
                              ,std_approved_date
                              ,pending_add_ver
                          FROM ps_m_doc_approve
                          where doc_nodeid in (" + strNodeId + ")";
            dt = db.ExecSql_DataTable(strSql, dbConnectionString);
            return dt;
        }

        public void AddDocApproved(DocApprove docApprove, string strUserLogin, bool bNeedApproval)
        {
            string strDocStatus = bNeedApproval ? "WF in process" : "Active";

            string strSql = @"INSERT INTO ps_m_doc_approve (doctype_code,doc_nodeid,doc_name,doc_desc,doc_revision_no,doc_revision_display,doc_revision_date,
                            doc_sheetno,depart_position_name,section_position_name,doc_remark,doc_status,created_by,created_datetime,updated_by,updated_datetime)
                            VALUES
                            ('" + docApprove.strDocType + "',"
                            + docApprove.strNodeId + ",'"
                            + docApprove.strDocName + "','"
                            + docApprove.strDocDesc + "',"
                            + docApprove.strRevision + ",'"
                            + docApprove.strRevisionDis + "',getdate(),'"
                            + docApprove.strDocSheet + "','"
                            + docApprove.strDep + "', '"
                            + docApprove.strSection + "','"
                            + docApprove.strRemark + "','" + strDocStatus + "','" + strUserLogin + @"',getdate(),'" + strUserLogin + @"',getdate())";

            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void AddDocApproved(List<DocApprove> lstDocApprove, string strUserLogin, bool bNeedApproval)
        {
            string strDocStatus = bNeedApproval ? "Wf in process" : "Active";
            string strValue = string.Empty;
            string strSql = @"INSERT INTO ps_m_doc_approve (doctype_code,doc_nodeid,doc_name,doc_desc,doc_revision_no,doc_revision_display,doc_revision_date,
                            doc_sheetno,depart_position_name,section_position_name,doc_remark,doc_status,created_by,created_datetime,updated_by,updated_datetime)
                            VALUES ";

            foreach (DocApprove doc in lstDocApprove)
            {
                if (strValue != string.Empty) strValue += ",";
                strValue += "('" + doc.strDocType + "',"
                           + doc.strNodeId + ",'"
                           + doc.strDocName + "','"
                           + doc.strDocDesc + "',"
                           + doc.strRevision + ",'"
                           + doc.strRevisionDis + "',getdate(),'"
                           + doc.strDocSheet + "','"
                           + doc.strDep + "', '"
                           + doc.strSection + "','"
                           + doc.strRemark + "','" + strDocStatus + "','" + strUserLogin + "',getdate(),'" + strUserLogin + "',getdate())";
            }
            if (strValue != string.Empty)
            {
                strSql += strValue;
                db.ExecNonQuery(strSql, dbConnectionString);
            }
        }

        public void AddDocEquipment(string strEquipCode, string strNodeId, string strUserLogin)
        {
            if (!String.IsNullOrEmpty(strEquipCode) && !String.IsNullOrEmpty(strNodeId))
            {
                string strSql = @"BEGIN
                               IF NOT EXISTS (SELECT * FROM [ps_m_equip_doc] 
                                               WHERE [equip_code] = '" + strEquipCode + @"'
                                               AND [doc_nodeid] = " + strNodeId + @")
                               BEGIN
                                   INSERT INTO [ps_m_equip_doc] ([equip_code], [doc_nodeid],[created_by]
                                  ,[created_datetime]
                                  ,[updated_by]
                                  ,[updated_datetime])
                                   VALUES ('" + strEquipCode + "', " + strNodeId + @",'" + strUserLogin + @"',getdate(),'" + strUserLogin + @"',getdate())
                               END
                            END";
                db.ExecNonQuery(strSql, dbConnectionString);
            }
        }

        public void AddDocEquipmentGroup(string strEquipGrpCode, string strNodeId,string strDocName, string strUserLogin)
        {
            if (!String.IsNullOrEmpty(strEquipGrpCode) && !String.IsNullOrEmpty(strNodeId))
            {
                string strSql = @"BEGIN
                               IF NOT EXISTS (SELECT * FROM [ps_m_equip_grp_doc] 
                                               WHERE [eg_code] = '" + strEquipGrpCode + @"'
                                               AND [doc_nodeid] = " + strNodeId + @")
                               BEGIN
                                   INSERT INTO [ps_m_equip_grp_doc] ([eg_code], [doc_nodeid],[doc_name],[created_by]
                                  ,[created_datetime]
                                  ,[updated_by]
                                  ,[updated_datetime])
                                   VALUES ('" + strEquipGrpCode + "', " + strNodeId + @",'"+ strDocName + "','" + strUserLogin + @"',getdate(),'" + strUserLogin + @"',getdate())
                               END
                            END";
                db.ExecNonQuery(strSql, dbConnectionString);
            }
        }

        public void AddDocBom(string strBomCode, string strNodeId, string strUserLogin)
        {
            if (!String.IsNullOrEmpty(strBomCode) && !String.IsNullOrEmpty(strNodeId))
            {
                string strSql = @"BEGIN
                               IF NOT EXISTS (SELECT * FROM [ps_m_bom_doc] 
                                               WHERE [bom_code] = '" + strBomCode + @"'
                                               AND [doc_nodeid] = " + strNodeId + @")
                               BEGIN
                                   INSERT INTO [ps_m_bom_doc] ([bom_code], [doc_nodeid],[created_by]
                                  ,[created_datetime]
                                  ,[updated_by]
                                  ,[updated_datetime])
                                   VALUES ('" + strBomCode + "', " + strNodeId + @",'" + strUserLogin + @"',getdate(),'" + strUserLogin + @"',getdate())
                               END
                            END";
                db.ExecNonQuery(strSql, dbConnectionString);
            }
        }

        public void UpdateDocApporve(string strDocName, string strDocDesc, string strDocRevision, string strDocRevisionDis, string strDocSheet, string strDocRemark, string strDocStatus, string strNodeId, string strUserLogin)
        {
            string strSql = @"UPDATE ps_m_doc_approve
                           SET [doc_name] = '" + strDocName + @"'
                              ,[doc_desc] = '" + strDocDesc + @"'
                              ,[doc_revision_no] = " + strDocRevision + @"
                              ,[doc_revision_display] = '" + strDocRevisionDis + @"'
                              ,[doc_revision_date] = getdate()
                              ,[doc_sheetno] = '" + strDocSheet + @"'
                              ,[doc_remark] = '" + strDocRemark + @"'
                              ,[doc_status] = '" + strDocStatus + @"'
                              ,[updated_by] = '" + strUserLogin + @"'
                              ,[updated_datetime] = getdate()
                         WHERE [doc_nodeid] = " + strNodeId;

            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void UpdateDocStdByDocDocId(string strDocNodId, string strDocStdNodeId, string strDocStdApprovedDate, string strUserLogin)
        {
            string strSql = @"UPDATE ps_m_doc_approve
                           SET [doc_std_nodeid] = " + strDocStdNodeId + @"
                              ,[std_approved_date] = '" + strDocStdApprovedDate + @"'
                              ,[updated_by] = '" + strUserLogin + @"'
                              ,[updated_datetime] = getdate()
                         WHERE [doc_nodeid] = " + strDocNodId;

            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void DeleteDocApprove(string strNodeId)
        {
            string strSql = @"DELETE FROM ps_m_doc_approve WHERE doc_nodeid = " + strNodeId;
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void DownloadDocCsByNodeId(string strDocNodeId)
        {
            OTFunctions otcs = new OTFunctions();
            Node node = new Node();
            node = otcs.getNodeByID(strDocNodeId);
            string strFileName = node.Name;
            byte[] fileByteDoc = otcs.downloadDoc(strDocNodeId);
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + strFileName + "\"");
            HttpContext.Current.Response.AddHeader("Content-Length", Convert.ToString(fileByteDoc.Length));
            HttpContext.Current.Response.BinaryWrite(fileByteDoc);
            HttpContext.Current.Response.End();
        }

        public void UpdateEquipBomRelated(string strNodeId,string strDocName, DataTable dtSelectdEquipBom, string strUserLogin)
        {
            DeleteBomRelatedByNodeId(strNodeId);
            DeleteEquipRelatedByNodeId(strNodeId);

            //foreach(DataRow dr in dtSelectdEquipBom.Rows)
            for (int i = 0; i < dtSelectdEquipBom.Rows.Count; i++)
            {
                if (dtSelectdEquipBom.Rows[i]["type"].ToString().ToUpper() == "B")
                {
                    AddDocBom(dtSelectdEquipBom.Rows[i]["code"].ToString(), strNodeId, strUserLogin);
                }
                else if (dtSelectdEquipBom.Rows[i]["type"].ToString().ToUpper() == "E")
                {
                    AddDocEquipment(dtSelectdEquipBom.Rows[i]["code"].ToString(), strNodeId, strUserLogin);
                }
                else if (dtSelectdEquipBom.Rows[i]["type"].ToString().ToUpper() == "G")
                {
                    AddDocEquipmentGroup(dtSelectdEquipBom.Rows[i]["code"].ToString(), strNodeId, strDocName, strUserLogin);
                }
            }
        }

        public void DeleteBomRelatedByNodeId(string strNodeId)
        {
            string strSql = @"delete from ps_m_bom_doc where doc_nodeid = " + strNodeId;
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void DeleteEquipRelatedByNodeId(string strNodeId)
        {
            string strSql = @"delete from ps_m_equip_doc where doc_nodeid = " + strNodeId;
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void UpdateDocStatusByNodeId(string strStatus, string strNodeId)
        {
            string strSql = @"update ps_m_doc_approve
                                set doc_status = '" + strStatus + @"'
                                where doc_nodeid = " + strNodeId;
            db.ExecNonQuery(strSql, dbConnectionString);
        } 

        public void SetPendingWfByNodeId(string strNodeId, bool IsPending)
        {
            string strPendingStatus = IsPending ? "1" : "0";
            string strSql = @"update ps_m_doc_approve
                                set pending_add_ver = " + strPendingStatus + @" 
                                where doc_nodeid = " + strNodeId;
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        private string GetProcessID()
        {
            string xPidNo = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'PID-DA' and year_thai = " + (DateTime.Now.Year + 543).ToString();
            DataTable dt = db.ExecSql_DataTable(sql, dbConnectionString);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'PID-DA' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                db.ExecNonQuery(sql, dbConnectionString);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('PID-DA', " + xRunno + ", " + (DateTime.Now.Year + 543).ToString() + ")  ";
                db.ExecNonQuery(sql, dbConnectionString);
            }
            xPidNo = "PID-DA-" + (DateTime.Now.Year + 543).ToString() + xRunno.ToString("000000");
            return xPidNo;
        }

        private string GetDpNo()
        {
            string xWAANo = "";
            int xRunno = 0;

            string sql = " select * from wf_runno where prefix = 'DA' and year_thai = " + (DateTime.Now.Year + 543).ToString();
            DataTable dt = db.ExecSql_DataTable(sql, dbConnectionString);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xRunno = int.Parse(dr["runno"].ToString());
                xRunno++;
                sql = " update wf_runno set runno = " + xRunno + " where  prefix = 'DA' and year_thai = " + (DateTime.Now.Year + 543).ToString();
                db.ExecNonQuery(sql, dbConnectionString);
            }
            else
            {
                xRunno++;
                sql = " insert into  wf_runno ( prefix, runno, year_thai)  values ('DA', " + xRunno + "," + (DateTime.Now.Year + 543).ToString() + ")  ";
                db.ExecNonQuery(sql, dbConnectionString);
            }
            xWAANo = "DA-" + xRunno.ToString("0000") + " /" + (DateTime.Now.Year + 543).ToString();
            return xWAANo;
        }
         
        public void StartWF(ref WfPsDocApprove wfPsDa, string[] arrApvId)
        {
            string strWfMapId = ConfigurationManager.AppSettings["wf_doc_approved_map_id"].ToString();
            Employee thisEmp;
            EmpInfoClass emp = new EmpInfoClass();
            string result = string.Empty;

            if (arrApvId.Length > 0)
            {
                // wf db section
                wfPsDa.process_id = GetProcessID();
                wfPsDa.process_id_ref = wfPsDa.process_id;
                wfPsDa.ps_da_no = GetDpNo();
                wfPsDa.ps_subject = wfPsDa.process_id + " : " + wfPsDa.ps_da_no;
                wfPsDa.current_step = "APPROVER_1";
                wfPsDa.form_status = "PENDING_APPROVED";
                thisEmp = new Employee();
                thisEmp = emp.getInFoByEmpID(wfPsDa.emp_id);
                //wfPsDa.emp_id = thisEmp.PERNR;
                wfPsDa.emp_name = thisEmp.SNAME;
                wfPsDa.emp_position = thisEmp.MC_SHORT; 
                wfPsDa.emp_position_full = thisEmp.POSITION;

                WfPsDocApproveDetails objDetail = new WfPsDocApproveDetails();
                wfPsDa.lstDocApvDetail = new List<WfPsDocApproveDetails>();
                for (int i = 0; i < arrApvId.Length; i++)
                {
                    thisEmp = new Employee();
                    thisEmp = emp.getInFoByEmpID(arrApvId[i]);
                    objDetail = new WfPsDocApproveDetails();
                    objDetail.process_id = wfPsDa.process_id;
                    objDetail.ps_da_no = wfPsDa.ps_da_no;
                    objDetail.apv_order = wfPsDa.lstDocApvDetail.Count + 1;
                    objDetail.apv_id = thisEmp.PERNR;
                    objDetail.apv_name = thisEmp.SNAME;
                    objDetail.apv_position = thisEmp.MC_SHORT;
                    objDetail.apv_position_full = thisEmp.POSITION;
                    wfPsDa.lstDocApvDetail.Add(objDetail);
                }
                // WF section 
                ApplicationData[] appDatas = zotFunctions.getWFStartAppData(strWfMapId, "");
                AttributeData wfAttr = new AttributeData();

                foreach (ApplicationData appData in appDatas)
                {  
                    if (appData.GetType().Name == "AttributeData")
                    {
                        wfAttr = (AttributeData)appData;
                        break;
                    }
                }
                if (wfAttr.Attributes != null)
                {
                    zotFunctions.setwfAttrValue(ref wfAttr, "Process_ID", wfPsDa.process_id);
                    zotFunctions.setwfAttrValue(ref wfAttr, "Workflow_ID", wfPsDa.ps_da_no);
                    zotFunctions.setwfAttrValue(ref wfAttr, "Workflow_Subject", wfPsDa.ps_da_no + " : " + "Document Approval");
                    zotFunctions.setwfAttrValue(ref wfAttr, "Task_Name", "Document Approval");
                    zotFunctions.setwfAttrValue(ref wfAttr, "Requestor", wfPsDa.emp_id);
                    zotFunctions.setwfAttrValue(ref wfAttr, "User_ID", wfPsDa.lstDocApvDetail[0].apv_id);
                    zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Pending Approve");
                    // zotFunctions.setwfAttrValue(ref wfAttr, "isNext", wfPsDa.process_id);
                    // zotFunctions.setwfAttrValue(ref wfAttr, "isFinish", wfPsDa.process_id);
                    var procInst = zotFunctions.startProcess("", strWfMapId, appDatas, wfPsDa.ps_da_no + " - " + DateTime.Now.ToString());
                    result = procInst.ProcessID.ToString();
                    wfPsDa.wf_processid = int.Parse(result);
                    wfPsDa.wf_subprocessid = int.Parse(result);

                    // create memo
                    string strMemoNodeId = UploadMemo(wfPsDa.ps_da_no, wfPsDa.emp_name, wfPsDa.emp_position, wfPsDa.lstDocApvDetail[0].apv_position, wfPsDa.memo_subject, wfPsDa.memo_content);
                    wfPsDa.doc_node_id_memo = strMemoNodeId;
                    InsertWfDocApprove(wfPsDa);
                    //foreach (WfPsDocApproveDetails objdetail in wfPsDa.lstDocApvDetail)
                    //{
                    //    InsertWfDocApproveDetail(objDetail); 
                    //}
                    for(int i = 0; i< wfPsDa.lstDocApvDetail.Count; i++)
                    {
                        InsertWfDocApproveDetail(wfPsDa.lstDocApvDetail[i]);
                    }

                }
            }
        }

        public void StartWF(ref WfPsDocApprove wfPsDa, List<WfPsDocApproveDetails> lstDocApproveDetails)
        {
            string strWfMapId = ConfigurationManager.AppSettings["wf_doc_approved_map_id"].ToString();
            Employee thisEmp;
            EmpInfoClass emp = new EmpInfoClass();
            string result = string.Empty;
            string strCurrentApproverOrder = string.Empty;


            // wf db section
            wfPsDa.process_id = GetProcessID();
            wfPsDa.ps_da_no = GetDpNo();
            for (int i = 0; i < lstDocApproveDetails.Count; i++)
            {
                lstDocApproveDetails[i].process_id = wfPsDa.process_id;
                lstDocApproveDetails[i].ps_da_no = wfPsDa.ps_da_no;
                if (String.IsNullOrEmpty(lstDocApproveDetails[i].apv_status))
                {
                    strCurrentApproverOrder = strCurrentApproverOrder == string.Empty ? lstDocApproveDetails[i].apv_order.ToString() : strCurrentApproverOrder;
                }
            }
            wfPsDa.ps_subject = wfPsDa.process_id + " : " + wfPsDa.ps_da_no;
            wfPsDa.current_step = "APPROVER_" + strCurrentApproverOrder;
            wfPsDa.form_status = "PENDING_APPROVED";
            thisEmp = new Employee();
            thisEmp = emp.getInFoByEmpID(wfPsDa.emp_id);
            wfPsDa.emp_name = thisEmp.SNAME;
            wfPsDa.emp_position = thisEmp.MC_SHORT;
            wfPsDa.emp_position_full = thisEmp.POSITION;

            WfPsDocApproveDetails objDetail = new WfPsDocApproveDetails();
            wfPsDa.lstDocApvDetail = lstDocApproveDetails;

            // WF section 
            ApplicationData[] appDatas = zotFunctions.getWFStartAppData(strWfMapId, "");
            AttributeData wfAttr = new AttributeData();

            foreach (ApplicationData appData in appDatas)
            {
                if (appData.GetType().Name == "AttributeData")
                {
                    wfAttr = (AttributeData)appData;
                    break;
                }
            }
            if (wfAttr.Attributes != null)
            {
                zotFunctions.setwfAttrValue(ref wfAttr, "Process_ID", wfPsDa.process_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "Workflow_ID", wfPsDa.ps_da_no);
                zotFunctions.setwfAttrValue(ref wfAttr, "Workflow_Subject", wfPsDa.ps_da_no + " : " + "Document Approval");
                zotFunctions.setwfAttrValue(ref wfAttr, "Task_Name", "Document Approval");
                zotFunctions.setwfAttrValue(ref wfAttr, "Requestor", wfPsDa.emp_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "User_ID", wfPsDa.lstDocApvDetail[int.Parse(strCurrentApproverOrder)-1].apv_id);
                zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "Pending Approve");
                var procInst = zotFunctions.startProcess("", strWfMapId, appDatas, wfPsDa.ps_da_no + " - " + DateTime.Now.ToString());
                result = procInst.ProcessID.ToString();
                wfPsDa.wf_processid = int.Parse(result);
                wfPsDa.wf_subprocessid = int.Parse(result);

                // create memo
                string strMemoNodeId = UploadMemo(wfPsDa.ps_da_no, wfPsDa.emp_name, wfPsDa.emp_position, wfPsDa.lstDocApvDetail[int.Parse(strCurrentApproverOrder) - 1].apv_position, wfPsDa.memo_subject, wfPsDa.memo_content);
                wfPsDa.doc_node_id_memo = strMemoNodeId;
                InsertWfDocApprove(wfPsDa);
                
                for(int i =0;i< wfPsDa.lstDocApvDetail.Count; i++)
                {
                    InsertWfDocApproveDetail(wfPsDa.lstDocApvDetail[i]);
                }
            }
        }

        public void UpdateWfAppDocStatusDetail(string strProcessId, string strApproverId, string strStatus)
        {
            string strSql = @"UPDATE wf_ps_doc_approve_detail
                               SET apv_apvdate = GETDATE()
                                  ,apv_status = '" + strStatus + @"'
                             WHERE process_id = '" + strProcessId + @"' 
                             AND apv_id = '" + strApproverId + @"'";
            db.ExecNonQuery(strSql, dbConnectionString);

        }

        public void UpdateWfAppDocStatus(string strFormStatus, string strLogin, string strProcessId, string strApproverOrder,string strNote)
        {
            string strSql = @"UPDATE dbo.wf_ps_doc_approve
                               SET form_status = '" + strFormStatus + @"'
                                  ,current_step = 'APPROVER_" + strApproverOrder + @"'
                                  ,note = '" + strNote + @"'
                                  ,updated_by = '" + strLogin + @"'
                                  ,updated_datetime = GETDATE()
                             WHERE process_id='" + strProcessId + "'";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void UpdateMDocApproveStatus(string strStatus, string strUserLogin, string strDocNodeId)
        {
            string strSql = @"UPDATE ps_m_doc_approve
                               SET doc_status = '" + strStatus + @"'
                                  ,updated_by = '" + strUserLogin + @"'
                                  ,updated_datetime = GETDATE()
                                  ,pending_add_ver = 0
                             WHERE doc_nodeid = " + strDocNodeId;
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public void UpdateNoteWf()
        {
            string strSql = @"";
             
        }

        public void UpdateWfFormComplete(string strProcessIdRef,string strUserLogin)
        {
            string strSql = @"UPDATE wf_ps_doc_approve
                               SET form_status = 'COMPLETED'
                                  ,current_step = 'END'
                                  ,updated_by = '"+ strUserLogin + @"'
                                  ,updated_datetime = GETDATE()
                             WHERE process_id_ref = '"+ strProcessIdRef + "'";
            db.ExecNonQuery(strSql, dbConnectionString);
        }

        public string UploadMemo(string strPsDaNo, string strRequestorName, string strRequestorPostion, string strApproverPosition, string strSubject, string strContent)
        {
            string strNodeId = string.Empty;
            string strFileName = strPsDaNo + "_" + DateTime.Now.ToString("yyyy-MM-dd");
            string strFolderAttNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString();


            memoDocClass memo = new memoDocClass();
            byte[] result = memo.createMemoDocApproveByte("", strPsDaNo, DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", strRequestorName,
              strRequestorPostion, "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"), "", "", "", "", strApproverPosition, "", DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"),
              strSubject, strContent);


            OTFunctions ot = new OTFunctions();
            var nodeId = ot.uploadDoc(strFolderAttNodeId, strFileName + ".pdf", strFileName + ".pdf", result, "");
            strNodeId = nodeId.ToString();
            return strNodeId;
        }

        public void InsertWfDocApprove(WfPsDocApprove objWf)
        {
            string strSql, strFields, strValue;
            strSql = strFields = strValue = string.Empty;

            if (!String.IsNullOrEmpty(objWf.process_id))
            {
                strFields += strFields == string.Empty ? "process_id" : ",process_id";
                strValue += strValue == string.Empty ? "'" + objWf.process_id + "'" : ",'" + objWf.process_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.process_id_ref))
            {
                strFields += strFields == string.Empty ? "process_id_ref" : ",process_id_ref";
                strValue += strValue == string.Empty ? "'" + objWf.process_id_ref + "'" : ",'" + objWf.process_id_ref + "'";
            }
            if (!String.IsNullOrEmpty(objWf.ps_da_no))
            {
                strFields += strFields == string.Empty ? "ps_da_no" : ",ps_da_no";
                strValue += strValue == string.Empty ? "'" + objWf.ps_da_no + "'" : ",'" + objWf.ps_da_no + "'";
            }
            if (!String.IsNullOrEmpty(objWf.ps_subject))
            {
                strFields += strFields == string.Empty ? "ps_subject" : ",ps_subject";
                strValue += strValue == string.Empty ? "'" + objWf.ps_subject + "'" : ",'" + objWf.ps_subject + "'";
            }
            if (!String.IsNullOrEmpty(objWf.project_id))
            {
                strFields += strFields == string.Empty ? "project_id" : ",project_id";
                strValue += strValue == string.Empty ? "'" + objWf.project_id + "'" : ",'" + objWf.project_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.note))
            {
                strFields += strFields == string.Empty ? "note" : ",note";
                strValue += strValue == string.Empty ? "'" + objWf.note + "'" : ",'" + objWf.note + "'";
            }
            if (!String.IsNullOrEmpty(objWf.form_status))
            {
                strFields += strFields == string.Empty ? "form_status" : ",form_status";
                strValue += strValue == string.Empty ? "'" + objWf.form_status + "'" : ",'" + objWf.form_status + "'";
            }
            if (!String.IsNullOrEmpty(objWf.current_step))
            {
                strFields += strFields == string.Empty ? "current_step" : ",current_step";
                strValue += strValue == string.Empty ? "'" + objWf.current_step + "'" : ",'" + objWf.current_step + "'";
            }
            if (!String.IsNullOrEmpty(objWf.emp_id))
            {
                strFields += strFields == string.Empty ? "emp_id" : ",emp_id";
                strValue += strValue == string.Empty ? "'" + objWf.emp_id + "'" : ",'" + objWf.emp_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.emp_name))
            {
                strFields += strFields == string.Empty ? "emp_name" : ",emp_name";
                strValue += strValue == string.Empty ? "'" + objWf.emp_name + "'" : ",'" + objWf.emp_name + "'";
            }
            if (!String.IsNullOrEmpty(objWf.emp_position))
            {
                strFields += strFields == string.Empty ? "emp_position" : ",emp_position";
                strValue += strValue == string.Empty ? "'" + objWf.emp_position + "'" : ",'" + objWf.emp_position + "'";
            }
            if (!String.IsNullOrEmpty(objWf.emp_position_full))
            {
                strFields += strFields == string.Empty ? "emp_position_full" : ",emp_position_full";
                strValue += strValue == string.Empty ? "'" + objWf.emp_position_full + "'" : ",'" + objWf.emp_position_full + "'";
            }
            if (!String.IsNullOrEmpty(objWf.next_step))
            {
                strFields += strFields == string.Empty ? "next_step" : ",next_step";
                strValue += strValue == string.Empty ? "'" + objWf.next_step + "'" : ",'" + objWf.next_step + "'";
            }
            if (!String.IsNullOrEmpty(objWf.created_by))
            {
                strFields += strFields == string.Empty ? "created_by" : ",created_by";
                strValue += strValue == string.Empty ? "'" + objWf.created_by + "'" : ",'" + objWf.created_by + "'";
            }
            if (objWf.created_datetime != null)
            {
                strFields += strFields == string.Empty ? "created_datetime" : ",created_datetime";
                strValue += strValue == string.Empty ? "GETDATE()" : ",GETDATE()";
            }
            if (!String.IsNullOrEmpty(objWf.updated_by))
            {
                strFields += strFields == string.Empty ? "updated_by" : ",updated_by";
                strValue += strValue == string.Empty ? "'" + objWf.updated_by + "'" : ",'" + objWf.updated_by + "'";
            }
            if (objWf.updated_datetime != null)
            {
                strFields += strFields == string.Empty ? "updated_datetime" : ",updated_datetime";
                strValue += strValue == string.Empty ? "GETDATE()" : ",GETDATE()";
            }
            if (objWf.wf_processid.HasValue)
            {
                strFields += strFields == string.Empty ? "wf_processid" : ",wf_processid";
                strValue += strValue == string.Empty ? objWf.wf_processid.ToString() : "," + objWf.wf_processid.ToString();
            }
            if (objWf.wf_subprocessid.HasValue)
            {
                strFields += strFields == string.Empty ? "wf_subprocessid" : ",wf_subprocessid";
                strValue += strValue == string.Empty ? objWf.wf_subprocessid.ToString() : "," + objWf.wf_subprocessid.ToString();
            }
            if (objWf.wf_taskid.HasValue)
            {
                strFields += strFields == string.Empty ? "wf_taskid" : ",wf_taskid";
                strValue += strValue == string.Empty ? objWf.wf_taskid.ToString() : "," + objWf.wf_taskid.ToString();
            }
            if (!String.IsNullOrEmpty(objWf.doc_node_id))
            {
                strFields += strFields == string.Empty ? "doc_node_id" : ",doc_node_id";
                strValue += strValue == string.Empty ? "'" + objWf.doc_node_id + "'" : ",'" + objWf.doc_node_id + "'";
            }
            if (!String.IsNullOrEmpty(objWf.doc_node_id_prev))
            {
                strFields += strFields == string.Empty ? "doc_node_id_prev" : ",doc_node_id_prev";
                strValue += strValue == string.Empty ? "'" + objWf.doc_node_id_prev + "'" : ",'" + objWf.doc_node_id_prev + "'";
            }
            if (!String.IsNullOrEmpty(objWf.doc_node_id_memo))
            {
                strFields += strFields == string.Empty ? "doc_node_id_memo" : ",doc_node_id_memo";
                strValue += strValue == string.Empty ? "'" + objWf.doc_node_id_memo + "'" : ",'" + objWf.doc_node_id_memo + "'";
            }
            if (!String.IsNullOrEmpty(objWf.memo_content))
            {
                strFields += strFields == string.Empty ? "memo_content" : ",memo_content";
                strValue += strValue == string.Empty ? "'" + objWf.memo_content + "'" : ",'" + objWf.memo_content + "'";
            }
            if (!String.IsNullOrEmpty(objWf.memo_subject))
            {
                strFields += strFields == string.Empty ? "memo_subject" : ",memo_subject";
                strValue += strValue == string.Empty ? "'" + objWf.memo_subject + "'" : ",'" + objWf.memo_subject + "'";
            }

            strSql = "INSERT INTO wf_ps_doc_approve ( ";
            strSql += strFields;
            strSql += " ) VALUES ( ";
            strSql += strValue;
            strSql += " ) ";

            db.ExecNonQuery(strSql, dbConnectionString);

        }

        public void InsertWfDocApproveDetail(WfPsDocApproveDetails objWfDetail)
        {
            string strSql, strFields, strValue;
            strSql = strFields = strValue = string.Empty;

            if (!String.IsNullOrEmpty(objWfDetail.process_id))
            {
                strFields += strFields == string.Empty ? "process_id" : ",process_id";
                strValue += strValue == string.Empty ? "'" + objWfDetail.process_id + "'" : ",'" + objWfDetail.process_id + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.ps_da_no))
            {
                strFields += strFields == string.Empty ? "ps_da_no" : ",ps_da_no";
                strValue += strValue == string.Empty ? "'" + objWfDetail.ps_da_no + "'" : ",'" + objWfDetail.ps_da_no + "'";
            }
            if (objWfDetail.apv_order != null)
            {
                strFields += strFields == string.Empty ? "apv_order" : ",apv_order";
                strValue += strValue == string.Empty ? "" + objWfDetail.apv_order.ToString() + "" : "," + objWfDetail.apv_order.ToString() + "";
            }
            if (!String.IsNullOrEmpty(objWfDetail.apv_id))
            {
                strFields += strFields == string.Empty ? "apv_id" : ",apv_id";
                strValue += strValue == string.Empty ? "'" + objWfDetail.apv_id + "'" : ",'" + objWfDetail.apv_id + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.apv_name))
            {
                strFields += strFields == string.Empty ? "apv_name" : ",apv_name";
                strValue += strValue == string.Empty ? "'" + objWfDetail.apv_name + "'" : ",'" + objWfDetail.apv_name + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.apv_position))
            {
                strFields += strFields == string.Empty ? "apv_position" : ",apv_position";
                strValue += strValue == string.Empty ? "'" + objWfDetail.apv_position + "'" : ",'" + objWfDetail.apv_position + "'";
            }
            if (!String.IsNullOrEmpty(objWfDetail.apv_position_full))
            {
                strFields += strFields == string.Empty ? "apv_position_full" : ",apv_position_full";
                strValue += strValue == string.Empty ? "'" + objWfDetail.apv_position_full + "'" : ",'" + objWfDetail.apv_position_full + "'";
            }
            if (objWfDetail.apv_apvdate != null)
            {
                strFields += strFields == string.Empty ? "apv_apvdate" : ",apv_apvdate";
                strValue += strValue == string.Empty ? "GETDATE()" : ",GETDATE()";
            }

            if (!String.IsNullOrEmpty(objWfDetail.apv_status))
            {
                strFields += strFields == string.Empty ? "apv_status" : ",apv_status";
                strValue += strValue == string.Empty ? "'" + objWfDetail.apv_status + "'" : ",'" + objWfDetail.apv_status + "'";
            }

            strSql = "INSERT INTO wf_ps_doc_approve_detail ( ";
            strSql += strFields;
            strSql += " ) VALUES ( ";
            strSql += strValue;
            strSql += " ) ";

            db.ExecNonQuery(strSql, dbConnectionString);

        }

        public bool IsCsUserExist(string strUserId)
        {
            bool isExist = true;
            var empeeee = zotFunctions.getCSUser(strUserId);
            if (empeeee == null) isExist = false;
            return isExist;
        }
    }


    public class DocApprove
    {
        public string strDocType { get; set; }
        public string strNodeId { get; set; }
        public string strDocName { get; set; }
        public string strDocDesc { get; set; }
        public string strRevision { get; set; }
        public string strRevisionDis { get; set; }
        public string strDocSheet { get; set; }
        public string strDep { get; set; }
        public string strSection { get; set; }
        public string strRemark { get; set; }
    }

    public class WfPsDocApprove
    {
        public WfPsDocApprove() { }
        public string process_id { get; set; }
        public string ps_da_no { get; set; }
        public string project_id { get; set; }
        public string current_step { get; set; }
        public string next_step { get; set; }
        public string note { get; set; }
        public string form_status { get; set; }
        public string emp_id { get; set; }
        public string emp_name { get; set; }
        public string emp_position { get; set; }
        public string emp_position_full { get; set; }
        public string created_by { get; set; }
        public DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public DateTime updated_datetime { get; set; }
        public int? wf_processid { get; set; }
        public int? wf_subprocessid { get; set; }
        public int? wf_taskid { get; set; }
        public string doc_node_id { get; set; }
        public string doc_node_id_prev { get; set; }
        public string doc_node_id_memo { get; set; }
        public string ps_subject { get; set; }
        public string memo_subject { get; set; }
        public string memo_content { get; set; }
        public string process_id_ref { get; set; }
        public List<WfPsDocApproveDetails> lstDocApvDetail { get; set; }

    }

    public class WfPsDocApproveDetails
    {
        public WfPsDocApproveDetails() { }

        public string process_id { get; set; }
        public string ps_da_no { get; set; }
        public int? apv_order { get; set; }
        public string apv_id { get; set; }
        public string apv_name { get; set; }
        public string apv_position { get; set; }
        public string apv_position_full { get; set; }
        public DateTime apv_apvdate { get; set; }
        public string apv_status { get; set; }

    }
}
