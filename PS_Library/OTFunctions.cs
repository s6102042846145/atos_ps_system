﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Security.Cryptography;
using System.IO;

namespace PS_Library
{
    public class OTFunctions
    {
        #region " Variable "

        public string zAdminName = ConfigurationManager.AppSettings["csadm"].ToString(); // "Admin";
        public string zAdminPWD = ConfigurationManager.AppSettings["cspwd"].ToString();//"livelink"; // on VMWARE
        public string zCAT_VOLUME_ID = ConfigurationManager.AppSettings["CAT_VOLUME_ID"].ToString();//"livelink"; // on VMWARE

        public string zToken { set; get; }
        public string zImpersonateToken { set; get; }
        public Authentication.OTAuthentication zotAuthen = new Authentication.OTAuthentication();
        public Authentication.AuthenticationClient zAuthen = new Authentication.AuthenticationClient();

        public ContentService.OTAuthentication zcsAuthen = new ContentService.OTAuthentication();
        public ContentService.ContentServiceClient zcsClient = new ContentService.ContentServiceClient();

        public DocumentManagement.OTAuthentication zdocAuthen = new DocumentManagement.OTAuthentication();
        public DocumentManagement.DocumentManagementClient zdocClient = new DocumentManagement.DocumentManagementClient();

        public WorkflowService.OTAuthentication zwfAuthen = new WorkflowService.OTAuthentication();
        public WorkflowService.WorkflowServiceClient zwfClient = new WorkflowService.WorkflowServiceClient();

        public MemberService.OTAuthentication zmemAuthen = new MemberService.OTAuthentication();
        public MemberService.MemberServiceClient zmemClient = new MemberService.MemberServiceClient();
        public OTFunctions()
        {
            zToken = getTokenAdmin();
            zotAuthen = new Authentication.OTAuthentication();
            zotAuthen.AuthenticationToken = zToken;
            zcsAuthen.AuthenticationToken = zToken;
            zdocAuthen.AuthenticationToken = zToken;
            zwfAuthen.AuthenticationToken = zToken;

        }
        public OTFunctions(string zlogin)
        {
            zToken = getTokenImpersonate(zlogin);
            zotAuthen = new Authentication.OTAuthentication();
            zotAuthen.AuthenticationToken = zToken;
            zcsAuthen.AuthenticationToken = zToken;
            zdocAuthen.AuthenticationToken = zToken;
            zwfAuthen.AuthenticationToken = zToken;

        }
        #endregion

        #region Authen
        public string getTokenAdmin()
        {
            string xtoken = "";
            try
            {
                xtoken = zAuthen.AuthenticateUser(zAdminName, zAdminPWD);
            }
            catch (Exception ex)
            {
                xtoken = "";
                LogHelper.WriteEx(ex);
            }
            return xtoken;
        }
        public string getToken(string xusername, string xpassword)
        {
            string xtoken = "";
            try { xtoken = zAuthen.AuthenticateUser(xusername, xpassword); }
            catch (Exception ex)
            {
                xtoken = "";
                LogHelper.WriteEx(ex);
            }
            return xtoken;
        }
        public string getTokenImpersonate(string xusername)
        {
            string xtoken = "";
            try
            {
                Authentication.OTAuthentication otAuthen = new Authentication.OTAuthentication();
                otAuthen.AuthenticationToken = getTokenAdmin();
                xtoken = zAuthen.ImpersonateUser(ref otAuthen, xusername);
            }
            catch (Exception ex)
            {
                xtoken = "";
                LogHelper.WriteEx(ex);
            }
            return xtoken;
        }
        #endregion

        #region Public2
        public string zAttrType_String = "StringAttribute";
        public string zAttrType_MultiLine = "MultiLineAttribute";
        public string zAttrType_User = "UserAttribute";
        public string zAttrType_Boolean = "BooleanAttribute";
        public string zAttrType_Integer = "IntegerAttribute";
        public string zAttrType_Real = "RealAttribute";
        public string zAttrType_Date = "DateAttribute";
        #endregion

        #region CSUserInfo
        public MemberService.Group[] getCSGroup(string xloginname)
        {
            MemberService.Member csmember = null;
            MemberService.Group[] csgroup = null;
            try
            {
                Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
                MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
                otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
                MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();
                csmember = memberClient.GetMemberByLoginName(ref otAdminAuthen, xloginname);
                csgroup = memberClient.ListMemberOf(ref otAdminAuthen, csmember.ID);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return csgroup;
        }
        public MemberService.User getCSUser(string xloginname)
        {
            MemberService.User objUser = null;
            try
            {
                Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
                MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
                otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
                MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();

                objUser = memberClient.GetUserByLoginName(ref otAdminAuthen, xloginname);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return objUser;
        }
        public MemberService.Member getCSMember(string xloginname)
        {
            MemberService.Member csmember = null;
            try
            {
                Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
                MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
                otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
                MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();
                csmember = memberClient.GetMemberByLoginName(ref otAdminAuthen, xloginname);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return csmember;
        }
        public string getCSUserDisplayName(string xloginname)
        {
            string xname = "";
            Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
            MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
            otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
            MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();
            MemberService.User csuser;
            try
            {
                csuser = memberClient.GetUserByLoginName(ref otAdminAuthen, xloginname);
                xname = csuser.DisplayName;
            }
            catch (Exception ex)
            {
                xname = "";
                LogHelper.WriteEx(ex);
            }
            return xname;
        }
        public int getUserID(string xloginname)
        {
            int xid = 0;
            try
            {
                MemberService.User xuser = getCSUser(xloginname);
                xid = int.Parse(xuser.ID.ToString());
            }
            catch
            {
                MemberService.Member xmember = getCSMember(xloginname);
                xid = int.Parse(xmember.ID.ToString());
            }
            return xid;
        }
        public string getEmailAddr(string xgroup_name, string token)
        {
            string email = "";
            try
            {
                MemberService.MemberServiceClient memsrv = new MemberService.MemberServiceClient();
                MemberService.OTAuthentication memAuthen = new MemberService.OTAuthentication();
                memAuthen.AuthenticationToken = getTokenAdmin();
                MemberService.Group xgroup = memsrv.GetGroupByName(ref memAuthen, xgroup_name);
                if (xgroup != null)
                {
                    MemberService.Member[] xmem = memsrv.ListMembers(ref memAuthen, xgroup.ID);
                    if (xmem != null)
                    {
                        for (int i = 0; i < xmem.Length; i++)
                        {
                            if (xmem[i] != null)
                            {
                                MemberService.User user = memsrv.GetUserByLoginName(ref memAuthen, xmem[i].Name);
                                if (user != null)
                                {
                                    email += user.Email + ",";
                                }
                            }
                        }
                    }
                }
                if (email.Length > 0) { email.Substring((email.Length - 1), 1); }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return email;
        }
        public string getEmailAddr_AllGroup(string xgroup_name, string token)
        {
            string xemail = "";
            try
            {
                string[] listgroup = xgroup_name.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                if (listgroup.Length > 0)
                {
                    for (int i = 0; i < listgroup.Length; i++)
                    {
                        xemail += getEmailAddr(listgroup[i], "");
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xemail;
        }
        public string getCSMemberName(int uid)
        {

            string xname = "";
            Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
            MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
            otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
            MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();
            MemberService.Member csmember;
            try
            {
                csmember = memberClient.GetMemberById(ref otAdminAuthen, uid);
                xname = csmember.Name;
            }
            catch (Exception ex)
            {
                xname = "";
                LogHelper.WriteEx(ex);
            }
            return xname;

        }
        public MemberService.User getCSUser(int uid)
        {

            string xname = "";
            Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
            MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
            otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
            MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();
            MemberService.Member csmember;
            MemberService.User csuser = new MemberService.User();
            try
            {
                csmember = memberClient.GetMemberById(ref otAdminAuthen, uid);
                xname = csmember.Name;
                if (csmember.Type.ToUpper() == "USER")
                {
                    csuser = memberClient.GetUserByLoginName(ref otAdminAuthen, xname);
                }
            }
            catch (Exception ex)
            {
                xname = "";
                LogHelper.WriteEx(ex);
            }
            return csuser;

        }
        public int getCSMemberID(string xloginname)
        {
            int x = 0;
            x = getUserID(xloginname);
            return x;
        }
        public DataTable getUserGroupList(int member_id, string token)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id");
            dt.Columns.Add("name");

            try
            {
                Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
                MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
                otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
                MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();


                MemberService.Member[] members = memberClient.ListMembers(ref otAdminAuthen, member_id);
                if (members != null)
                {
                    for (int i = 0; i < members.Length; i++)
                    {
                        if (members[i] != null)
                        {
                            DataRow dr = dt.NewRow();
                            dr[0] = members[i].ID;
                            //dr[1] = members[i].Name;
                            dr[1] = members[i].DisplayName;
                            dt.Rows.Add(dr);
                        }
                    }
                }
                dt.DefaultView.Sort = "name asc ";
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        public DataTable getMembers(string xgroup_name, string token)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("id", typeof(string));
            dt.Columns.Add("name", typeof(string));
            dt.Columns.Add("displayname", typeof(string));
            dt.Columns.Add("firstname", typeof(string));
            dt.Columns.Add("lastname", typeof(string));
            dt.Columns.Add("email", typeof(string));

            try
            {
                Authentication.AuthenticationClient authenClient = new Authentication.AuthenticationClient();
                MemberService.OTAuthentication otAdminAuthen = new MemberService.OTAuthentication();
                otAdminAuthen.AuthenticationToken = authenClient.AuthenticateUser(zAdminName, zAdminPWD);
                MemberService.MemberServiceClient memberClient = new MemberService.MemberServiceClient();

                var xgroup_id = memberClient.GetGroupByName(ref otAdminAuthen, xgroup_name);
                if (xgroup_id != null)
                {
                    var xmembers = memberClient.ListMembers(ref otAdminAuthen, xgroup_id.ID);
                    if (xmembers.Length > 0)
                    {
                        for (int i = 0; i < xmembers.Length; i++)
                        {
                            if (xmembers[i] != null)
                            {
                                if (xgroup_id.LeaderID.HasValue == false)
                                {
                                    DataRow dr = dt.NewRow();
                                    dr[0] = xmembers[i].ID;
                                    dr[1] = xmembers[i].Name;

                                    var xuser = memberClient.GetUserByLoginName(ref otAdminAuthen, xmembers[i].Name);
                                    dr[2] = xuser.FirstName + " " + xuser.LastName;
                                    dr[3] = xuser.FirstName;
                                    dr[4] = xuser.LastName;
                                    dr[5] = getCSUser(xmembers[i].Name).Email;
                                    dt.Rows.Add(dr);
                                }
                                else
                                { 
                                    if (xgroup_id.LeaderID.Value != xmembers[i].ID)
                                    {
                                        DataRow dr = dt.NewRow();
                                        dr[0] = xmembers[i].ID;
                                        dr[1] = xmembers[i].Name;

                                        var xuser = memberClient.GetUserByLoginName(ref otAdminAuthen, xmembers[i].Name);
                                        dr[2] = xuser.FirstName + " " + xuser.LastName;
                                        dr[3] = xuser.FirstName;
                                        dr[4] = xuser.LastName;
                                        dr[5] = getCSUser(xmembers[i].Name).Email;
                                        dt.Rows.Add(dr);
                                    }
                                }
                            }
                        }
                    }
                }

                dt.DefaultView.Sort = "name asc ";
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return dt;
        }
        #endregion

        #region CS Services
        public void applyCategoryInheritance(string xnodeId)
        {

            DocumentManagement.Node xNode;
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                xNode = zdocClient.GetNode(ref zdocAuthen, long.Parse(xnodeId));

                DocumentManagement.CategoryInheritance[] cat = zdocClient.GetCategoryInheritance(ref zdocAuthen, 886811);

                zdocClient.SetCategoryInheritance(ref zdocAuthen, long.Parse(xnodeId), cat);
            }
            catch (Exception ex)
            {
                LogHelper.Write("ERRFUNC: getNodeByID ; Node ID = " + xnodeId);
                LogHelper.WriteEx(ex);

                throw (ex);
            }
        }
        public DocumentManagement.Node getNodeByID(string xnodeId)
        {

            DocumentManagement.Node xNode;
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                xNode = zdocClient.GetNode(ref zdocAuthen, long.Parse(xnodeId));
            }
            catch (Exception ex)
            {
                LogHelper.Write("ERRFUNC: getNodeByID ; Node ID = " + xnodeId);
                LogHelper.WriteEx(ex);

                throw (ex);
            }

            return xNode;
        }
        public String getNodeIDByName(string xparentId, string xSearchName)
        {
            String xnodeid = "";
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                xnodeid = zdocClient.GetNodeByName(ref zdocAuthen, long.Parse(xparentId), xSearchName).ID.ToString();

            }
            catch (Exception ex)
            {
                LogHelper.Write("ERRFUNC: getNodeByName ; Parent ID = " + xparentId + "SearchName = " + xSearchName + " ");
                LogHelper.WriteEx(ex);

                throw;
            }
            return xnodeid;
        }
        public DocumentManagement.Node getNodeByName(string xparentId, string xSearchName)
        {
            DocumentManagement.Node xnodeid;
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                xnodeid = zdocClient.GetNodeByName(ref zdocAuthen, long.Parse(xparentId), xSearchName);
            }
            catch (Exception ex)
            {
                LogHelper.Write("ERRFUNC: getNodeByName ; Parent ID = " + xparentId + "SearchName = " + xSearchName + " ");
                LogHelper.WriteEx(ex);

                throw;
            }
            return xnodeid;
        }
        public DocumentManagement.Node[] getNodesInParentID(string xparentId)
        {
            DocumentManagement.Node[] xnodelist = new DocumentManagement.Node[0];
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                xnodelist = zdocClient.ListNodes(ref zdocAuthen, long.Parse(xparentId), false);
            }
            catch (Exception ex)
            {
                LogHelper.Write("ERRFUNC: getNodesInParentID ; Parent ID = " + xparentId + " not found ");
                LogHelper.WriteEx(ex);

                throw;
            }
            return xnodelist;
        }

        public long UploadDocument(string parentId, string fileName, string fullPath, byte[] content, string title = "")
        {


            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var parentNode = zdocClient.GetNode(ref zdocAuthen, long.Parse(parentId));
                if (parentNode != null)
                {
                    DocumentManagement.Metadata metaData = parentNode.Metadata;
                    DocumentManagement.Attachment attachment = new DocumentManagement.Attachment();
                    attachment.Contents = content;
                    attachment.FileSize = content == null ? 0 : content.Length;
                    attachment.FileName = fileName;
                    attachment.CreatedDate = DateTime.Now;
                    attachment.ModifiedDate = DateTime.Now;
                    var xnode = zdocClient.GetNodeByName(ref zdocAuthen, parentNode.ID, fileName);
                    if (xnode != null)
                    {
                        // Add new version 
                        // string contextID = zdocClient.GetVersionContentsContext(ref zdocAuthen, xnode.ID, 0);
                        // string existingFilePath = WORKPATH + fileName + ".old." + fExt;
                        //System.IO.MemoryStream DocStream = new System.IO.MemoryStream();
                        //System.IO.File.OpenRead(fullPath).CopyTo(DocStream); 
                        //ContentService.FileAtts fileAtts = new ContentService.FileAtts();
                        //fileAtts.CreatedDate = DateTime.Now;
                        //fileAtts.FileName = fileName;
                        //fileAtts.FileSize = content == null ? 0 : content.Length;
                        //fileAtts.ModifiedDate = DateTime.Now;
                        // string contextID = zdocClient.CreateDocumentContext(ref zdocAuthen, parentNode.ID, fileName, "", false, metaData);
                        //  var objectID = zcsClient.UploadContent(ref zcsAuthen, contextID, fileAtts, DocStream);
                        zdocClient.AddVersion(ref zdocAuthen, xnode.ID, metaData, attachment);
                        return xnode.ID;
                    }
                    else
                    {
                        //Add new doc
                        DocumentManagement.Node newDoc = zdocClient.CreateDocument(ref zdocAuthen, long.Parse(parentId), fileName, title, false, metaData, attachment);

                        return newDoc.ID;
                    }
                }
                else { return 0; }

                // DocumentManagement.Node newDoc = DocManClient.CreateDocument(ref OTAuthenDocMan, parentId, fileName, title, false, metaData, attachment);

                // return newDoc.ID;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                return 0;
                //throw;
            }
        }
        public long uploadDoc(string parentId, string docName, string fileName, byte[] content, string title = "")
        {


            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var parentNode = zdocClient.GetNode(ref zdocAuthen, long.Parse(parentId));
                if (parentNode != null)
                {
                    DocumentManagement.Metadata metaData = parentNode.Metadata;
                    DocumentManagement.Attachment attachment = new DocumentManagement.Attachment();
                    attachment.Contents = content;
                    attachment.FileSize = content == null ? 0 : content.Length;
                    attachment.FileName = docName;
                    attachment.CreatedDate = DateTime.Now;
                    attachment.ModifiedDate = DateTime.Now;
                    var xnode = zdocClient.GetNodeByName(ref zdocAuthen, parentNode.ID, fileName);
                    if (xnode != null)
                    {
                        zdocClient.AddVersion(ref zdocAuthen, xnode.ID, metaData, attachment);
                        return xnode.ID;
                    }
                    else
                    {
                        //Add new doc
                        DocumentManagement.Node newDoc = zdocClient.CreateDocument(ref zdocAuthen, long.Parse(parentId), fileName, title, false, metaData, attachment);

                        return newDoc.ID;
                    }
                }
                else { return 0; }


            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                return 0;
                //throw;
            }
        }

        public DocumentManagement.Node checkAndcreateSubFolder(string xparentId, string xFolderlist)
        {
            DocumentManagement.Node xnode = new DocumentManagement.Node();
            try
            {
                xFolderlist = xFolderlist.Replace("/", "|");
                xFolderlist = xFolderlist.Replace("\\", "|");

                string[] subfolderlist = xFolderlist.Split("|".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                DocumentManagement.Node xparent = getNodeByID(xparentId);
                DocumentManagement.Node xparentTemp = new DocumentManagement.Node();
                if (xparent != null)
                {
                    //Create Folders & Sub folder
                    bool skipCheckNode = false;
                    xparentTemp = xparent;
                    foreach (var aFolder in subfolderlist)
                    {
                        if (skipCheckNode == false)
                        {
                            xparentTemp = getNodeByName(xparentTemp.ID.ToString(), aFolder);
                        }
                        if (xparentTemp == null || skipCheckNode == true)
                        {   // create folder
                            if (xparentTemp == null) { xparentTemp = xparent; }
                            xparentTemp = createFolder(xparentTemp.ID.ToString(), aFolder);
                            skipCheckNode = true;
                        }
                    }
                    xnode = xparentTemp;

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xnode;
        }

        public DocumentManagement.Node createFolder(string xparentId, string xFoldername)
        {
            //string strToken = zAuthen.RefreshToken(ref zotAuthen);
            //if (strToken == "")
            string strToken = getTokenAdmin();

            zdocAuthen.AuthenticationToken = strToken;

            DocumentManagement.Node xnode = new DocumentManagement.Node();
            var xparent = zdocClient.GetNode(ref zdocAuthen, long.Parse(xparentId));
            if (xparent != null)
            {
                try
                {
                    var xmetaData = xparent.Metadata;
                    xnode = zdocClient.CreateFolder(ref zdocAuthen, long.Parse(xparentId), xFoldername, "", xmetaData);
                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                    throw (ex);
                }
            }
            return xnode;
        }
        public bool deleteFolder(string xparentId, string xFoldername)
        {
            bool xResult = false;
            try
            {
                var xNode = getNodeByName(xparentId, xFoldername);
                if (xNode != null)
                {
                    //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                    //if (strToken == "")
                    string strToken = getTokenAdmin();

                    zdocAuthen.AuthenticationToken = strToken;

                    zdocClient.DeleteNode(ref zdocAuthen, xNode.ID);
                }
                xResult = true;
            }
            catch (Exception ex)
            {
                xResult = false;
                LogHelper.WriteEx(ex);
            }
            return xResult;
        }
        public bool deleteDoc(string xparentId, string xdocName)
        {
            bool xResult = false;
            try
            {
                var xNode = getNodeByName(xparentId, xdocName);
                if (xNode != null)
                {
                    //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                    //if (strToken == "")
                    string strToken = getTokenAdmin();

                    zdocAuthen.AuthenticationToken = strToken;

                    zdocClient.DeleteNode(ref zdocAuthen, xNode.ID);
                }
                xResult = true;
            }
            catch (Exception ex)
            {
                xResult = false;
                LogHelper.WriteEx(ex);
            }
            return xResult;
        }
        public bool deleteNodeID(string xparentId, string xnodeID)
        {
            bool xResult = false;
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                zdocClient.DeleteNode(ref zdocAuthen, long.Parse(xnodeID));
                xResult = true;
            }
            catch (Exception ex)
            {
                xResult = false;
                LogHelper.WriteEx(ex);
            }
            return xResult;
        }
        public List<NodeInfo> getDocList(string xparentid)
        {

            List<NodeInfo> xlist = new List<NodeInfo>();
            try
            {
                var xparent = getNodeByID(xparentid);
                if (xparent != null)
                {
                    var nodelist = getNodesInParentID(xparentid);
                    if (nodelist != null)
                    {
                        for (int i = 0; i < nodelist.Length; i++)
                        {
                            var xnode = nodelist[i];
                            if (xnode != null)
                            {
                                NodeInfo xNodeInfo = new NodeInfo();
                                xNodeInfo.NodeID = xnode.ID.ToString();
                                xNodeInfo.NodeName = xnode.Name.ToString();
                                xNodeInfo.NodeType = xnode.DisplayType.ToString();
                                xNodeInfo.NodeURL = string.Format("http://{0}/otcs/cs.exe?func=ll&objId={1}&objAction=browse&sort=name&viewType=1"
                                                        , ConfigurationManager.AppSettings["HOST_NAME"].ToString()
                                                        , xnode.ID.ToString());
                                xNodeInfo.NodeParentID = xnode.ParentID.ToString();
                                xNodeInfo.NodeParentName = xparent.Name;
                                xlist.Add(xNodeInfo);

                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xlist;
        }
        public string getLink(string xnodeID)
        {
            string xurl = "";
            try
            {
                var xnode = getNodeByID(xnodeID);
                if (xnode != null)
                {
                    if (xnode.DisplayType.ToUpper().Contains("FOLDER"))
                    {
                        xurl = string.Format("http://{0}/otcs/cs.exe?func=ll&objId={1}&objAction=browse&sort=name&viewType=1"
                                                                , ConfigurationManager.AppSettings["HOST_NAME"].ToString()
                                                            , xnodeID);
                    }
                    else
                    {
                        xurl = string.Format("http://{0}/otcs/cs.exe?func=ll&objaction=overview&objid={1}"
                                                                , ConfigurationManager.AppSettings["HOST_NAME"].ToString()
                                                            , xnodeID);
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xurl;
        }
        public void createShortCut(string xparentid, DocumentManagement.Node xSourceNode, bool inheritCategoryData)
        {
            try
            {
                DocumentManagement.Metadata xMetaData = new DocumentManagement.Metadata();

                if (inheritCategoryData == true)
                {
                    xMetaData = getNodeByID(xparentid).Metadata;
                }
                else
                {
                    xMetaData = xSourceNode.Metadata;
                }
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                zdocClient.CreateShortcut(ref zdocAuthen, long.Parse(xparentid), "ShortCut-" + xSourceNode.Name, "", xSourceNode.ID, xMetaData);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void createShortCutPO(string xparentid, DocumentManagement.Node xSourceNode, bool inheritCategoryData)
        {
            try
            {
                DocumentManagement.Metadata xMetaData = new DocumentManagement.Metadata();

                if (inheritCategoryData == true)
                {
                    xMetaData = getNodeByID(xparentid).Metadata;
                }
                else
                {
                    xMetaData = xSourceNode.Metadata;
                }
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                zdocClient.CreateShortcut(ref zdocAuthen, long.Parse(xparentid), "ShortCut-PO-" + xSourceNode.Name, "", xSourceNode.ID, xMetaData);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }
        public void createShortCutCTNO(string xparentid, DocumentManagement.Node xSourceNode, bool inheritCategoryData)
        {
            try
            {
                DocumentManagement.Metadata xMetaData = new DocumentManagement.Metadata();

                if (inheritCategoryData == true)
                {
                    xMetaData = getNodeByID(xparentid).Metadata;
                }
                else
                {
                    xMetaData = xSourceNode.Metadata;
                }
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                zdocClient.CreateShortcut(ref zdocAuthen, long.Parse(xparentid), "ShortCut-CTNO-" + xSourceNode.Name, "", xSourceNode.ID, xMetaData);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

        }

        public string createShortCutNode(string xparentid, string xSourceNodeID, bool inheritCategoryData)
        {
            string xnodeID = "";
            try
            {
                DocumentManagement.Metadata xMetaData = new DocumentManagement.Metadata();
                var xSourceNode = getNodeByID(xSourceNodeID);
                if (xSourceNode != null)
                {
                    if (inheritCategoryData == true)
                    {
                        xMetaData = getNodeByID(xparentid).Metadata;
                    }
                    else
                    {
                        xMetaData = xSourceNode.Metadata;
                    }

                    //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                    //if (strToken == "")
                    string strToken = getTokenAdmin();

                    zdocAuthen.AuthenticationToken = strToken;


                    var xnode = zdocClient.CreateShortcut(ref zdocAuthen, long.Parse(xparentid), "ShortCut - " + xSourceNode.Name, "", xSourceNode.ID, xMetaData);
                    xnodeID = xnode.ID.ToString();
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                xnodeID = "";
            }
            return xnodeID;
        }
        public string createShortCutNode(string xparentid, string xSourceNodeID, string shortcutname, bool inheritCategoryData)
        {
            string xnodeID = "";
            try
            {
                DocumentManagement.Metadata xMetaData = new DocumentManagement.Metadata();
                var xSourceNode = getNodeByID(xSourceNodeID);
                if (xSourceNode != null)
                {
                    if (inheritCategoryData == true)
                    {
                        xMetaData = getNodeByID(xparentid).Metadata;
                    }
                    else
                    {
                        xMetaData = xSourceNode.Metadata;
                    }
                    //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                    //if (strToken == "")
                    string strToken = getTokenAdmin();

                    zdocAuthen.AuthenticationToken = strToken;
                    var xShortCutNode = zdocClient.GetNodeByName(ref zdocAuthen, long.Parse(xparentid), shortcutname);
                    if (xShortCutNode != null)
                    {
                        xnodeID = xShortCutNode.ID.ToString();
                    }
                    else
                    {
                        var xnode = zdocClient.CreateShortcut(ref zdocAuthen, long.Parse(xparentid), shortcutname, "", xSourceNode.ID, xMetaData);
                        xnodeID = xnode.ID.ToString();
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                xnodeID = "";
            }
            return xnodeID;
        }
        public DocumentManagement.Node copyNode(string xdoc_nodeid, string xdestinationid)
        {
            DocumentManagement.Node xNode = new DocumentManagement.Node();
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xcopyoption = new DocumentManagement.CopyOptions();

                xcopyoption.AddVersion = true;
                var xdoc = getNodeByID(xdoc_nodeid);
                var xdes = getNodeByID(xdestinationid);
                xNode = zdocClient.CopyNode(ref zdocAuthen, xdoc.ID, xdes.ID, xdoc.Name, xcopyoption);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xNode;
        }
        public void moveNode(string xdoc_nodeid, string xdestinationid)
        {
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xmoveoption = new DocumentManagement.MoveOptions();

                xmoveoption.AddVersion = true;

                var xdoc = getNodeByID(xdoc_nodeid);
                var xdes = getNodeByID(xdestinationid);
                zdocClient.MoveNode(ref zdocAuthen, xdoc.ID, xdes.ID, xdoc.Name, xmoveoption);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void renameNode(string wf_nodeid, string xProcessID, string xDocReqNo)
        {
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xFolder_Req = getNodeByName(wf_nodeid, xDocReqNo);
                if (xFolder_Req != null)
                {
                    var xdoc = getNodeByName(xFolder_Req.ID.ToString(), xProcessID + ".pdf");
                    if (xdoc != null)
                        zdocClient.RenameNode(ref zdocAuthen, xdoc.ID, xDocReqNo + ".pdf");
                }
                else
                {
                    var xFolder = getNodeByName(wf_nodeid, xProcessID);
                    if (xFolder != null)
                    {
                        zdocClient.RenameNode(ref zdocAuthen, xFolder.ID, xDocReqNo);

                        var xdoc = getNodeByName(xFolder.ID.ToString(), xProcessID + ".pdf");
                        if (xdoc != null)
                            zdocClient.RenameNode(ref zdocAuthen, xdoc.ID, xDocReqNo + ".pdf");
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public byte[] downloadDoc(string xnodeID)
        {
            byte[] content = new byte[0];
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xnode = zdocClient.GetNode(ref zdocAuthen, long.Parse(xnodeID));
                if (xnode != null)
                {
                    var xversions = xnode.VersionInfo;
                    var xlastverion = xversions.VersionNum;
                    var xattach = zdocClient.GetVersionContents(ref zdocAuthen, xnode.ID, xnode.VersionInfo.VersionNum);
                    content = xattach.Contents;
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return content;
        }
        private DocumentManagement.AttributeGroup GetCategoryDef(long CategoryId)
        {
            try
            {
                //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                //if (strToken == "")
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var categoryDef = zdocClient.GetCategoryTemplate(ref zdocAuthen, CategoryId);

                return categoryDef;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                throw;
            }
        }
        private int getCatTemplateID(string xCatTemplateName)
        {
            int xID = 0;
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xnode = zdocClient.GetNodeByName(ref zdocAuthen, long.Parse(zCAT_VOLUME_ID), xCatTemplateName);
                xID = int.Parse(xnode.ID.ToString());

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xID;
        }
        private string getCatAttrValue(DocumentManagement.DataValue xDocCatAttributeValue)
        {
            string xvalue = "";
            try
            {
                switch (xDocCatAttributeValue.GetType().Name)
                {
                    case "StringValue":
                        {
                            DocumentManagement.StringValue xStrValue = (DocumentManagement.StringValue)xDocCatAttributeValue;
                            if (xStrValue.Values == null) { xvalue = ""; } else { xvalue = xStrValue.Values[0].ToString(); }

                        }; break;
                    case "DateValue":
                        {
                            DocumentManagement.DateValue xDateValue = (DocumentManagement.DateValue)xDocCatAttributeValue;
                            if (xDateValue == null) { xvalue = String.Empty; }
                            else
                            { try { xvalue = xDateValue.Values[0].Value.ToString("yyyy-MM-dd"); } catch { }; }


                        }; break;
                    case "BooleanValue":
                        {
                            DocumentManagement.BooleanValue xBoolValue = (DocumentManagement.BooleanValue)xDocCatAttributeValue;
                            try { xvalue = xBoolValue.Values[0].Value.ToString(); }
                            catch { xvalue = ""; }

                        }; break;
                    case "IntegerValue":
                        {
                            DocumentManagement.IntegerValue xIntValue = (DocumentManagement.IntegerValue)xDocCatAttributeValue;
                            try { xvalue = xIntValue.Values[0].Value.ToString(); }
                            catch { xvalue = ""; }
                        }; break;
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xvalue;

        }
        public void updateCategoryData(string xnodeID, string xCatTemplateName, string xAttributeValue)
        // Example xnodeID = Doc or Folder Node ID
        // CatTemplateName = Category Template
        // xAttributeValue = "{[attribute_name1]:[attribute_value1],[attribute_name2]:[attribute_value2],..}"
        {
            try
            {
                xAttributeValue = xAttributeValue.Replace("{", "");
                xAttributeValue = xAttributeValue.Replace("}", "");
                var xAttributeList = xAttributeValue.Split(",".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                string[] xAttributes = new string[xAttributeList.Length];
                string[] xValues = new string[xAttributeList.Length];
                for (int i = 0; i < xAttributeList.Length; i++)
                {
                    try
                    {
                        var tmp = xAttributeList[i].Split(":".ToCharArray(), StringSplitOptions.RemoveEmptyEntries);
                        xAttributes.SetValue(tmp[0].ToString(), i);
                        xValues.SetValue(tmp[1].ToString(), i);
                    }
                    catch
                    {
                        xAttributes.SetValue("", i);
                        xValues.SetValue("", i);

                    }

                }

                var xnode = zdocClient.GetNode(ref zdocAuthen, long.Parse(xnodeID));
                if (xnode != null)
                {
                    var metaData = xnode.Metadata;
                    // ***
                    var xCatID = getCatTemplateID(xCatTemplateName);
                    var new_attributeGroup = GetCategoryDef(long.Parse(xCatID.ToString()));
                    string xvalue = "";
                    for (int a = 0; a < xAttributes.Length; a++)
                    {
                        for (int i = 0; i < new_attributeGroup.Values.Length; i++)
                        {
                            if (xAttributes[a].ToString().Trim().ToUpper() == new_attributeGroup.Values[i].Description.Trim().ToUpper())
                            {
                                var xDocAttrValue = new_attributeGroup.Values[i];
                                xvalue = xValues[a].ToString().Trim();
                                switch (new_attributeGroup.Values[i].GetType().Name)
                                {
                                    case "StringValue":
                                        {
                                            DocumentManagement.StringValue xStrValue = (DocumentManagement.StringValue)new_attributeGroup.Values[i];
                                            if (xStrValue.Values == null) { xStrValue.Values = new string[1]; }
                                            xStrValue.Values.SetValue(xvalue, 0);
                                            new_attributeGroup.Values.SetValue(xStrValue, i);
                                        }; break;
                                    case "DateValue":
                                        {
                                            DocumentManagement.DateValue xDateValue = (DocumentManagement.DateValue)new_attributeGroup.Values[i];
                                            if (xDateValue == null) { xDateValue.Values = new DateTime?[1]; }
                                            xDateValue.Values.SetValue(DateTime.Parse(xvalue), 0);
                                            new_attributeGroup.Values.SetValue(xDateValue, i);

                                        }; break;
                                    case "BooleanValue":
                                        {
                                            DocumentManagement.BooleanValue xBoolValue = (DocumentManagement.BooleanValue)new_attributeGroup.Values[i];
                                            xBoolValue.Values.SetValue(Boolean.Parse(xvalue), 0);
                                            new_attributeGroup.Values.SetValue(xBoolValue, i);
                                        }; break;
                                    case "IntegerValue":
                                        {
                                            DocumentManagement.IntegerValue xIntValue = (DocumentManagement.IntegerValue)new_attributeGroup.Values[i];
                                            xIntValue.Values.SetValue(int.Parse(xvalue), 0);
                                            new_attributeGroup.Values.SetValue(xIntValue, i);
                                        }; break;

                                }
                            }
                        }

                    }
                    if (metaData.AttributeGroups != null)
                    {
                        // Update Category Data
                        // metaData.AttributeGroups = new DocumentManagement.AttributeGroup[] { new_attributeGroup };
                        bool xfoundAttributeGroup = false;
                        var listAttrGroup = metaData.AttributeGroups.ToList<DocumentManagement.AttributeGroup>();
                        var ind = 0;
                        foreach (DocumentManagement.AttributeGroup xAtrrGrp in metaData.AttributeGroups)
                        {
                            if (xAtrrGrp.DisplayName == xCatTemplateName)
                            {
                                metaData.AttributeGroups.SetValue(new_attributeGroup, ind);
                                xfoundAttributeGroup = true;
                            }
                            ind++;
                        }
                        if (xfoundAttributeGroup == false)
                        {
                            listAttrGroup.Add(new_attributeGroup);
                            metaData.AttributeGroups = listAttrGroup.ToArray();
                        }
                    }
                    else
                    {
                        metaData.AttributeGroups = new DocumentManagement.AttributeGroup[] { new_attributeGroup };
                    }
                    xnode.Metadata = metaData;

                    //string strToken = zAuthen.RefreshToken(ref zotAuthen);
                    //if (strToken == "")
                    string strToken = getTokenAdmin();

                    zdocAuthen.AuthenticationToken = strToken;

                    zdocClient.UpdateNode(ref zdocAuthen, xnode);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void resetCategoryData(string xnodeID, string xCatTemplateName)
        {
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xnode = zdocClient.GetNode(ref zdocAuthen, long.Parse(xnodeID));
                if (xnode != null)
                {
                    var metaData = xnode.Metadata;
                    // ***
                    var xCatID = getCatTemplateID(xCatTemplateName);
                    var new_attributeGroup = GetCategoryDef(long.Parse(xCatID.ToString()));
                    if (metaData.AttributeGroups != null)
                    {
                        // Update Category Data
                        // metaData.AttributeGroups = new DocumentManagement.AttributeGroup[] { new_attributeGroup };
                        bool xfoundAttributeGroup = false;
                        var listAttrGroup = metaData.AttributeGroups.ToList<DocumentManagement.AttributeGroup>();
                        var ind = 0;
                        foreach (DocumentManagement.AttributeGroup xAtrrGrp in metaData.AttributeGroups)
                        {
                            if (xAtrrGrp.DisplayName == xCatTemplateName)
                            {
                                metaData.AttributeGroups.SetValue(new_attributeGroup, ind);
                                xfoundAttributeGroup = true;
                            }
                            ind++;
                        }
                        if (xfoundAttributeGroup == false)
                        {
                            listAttrGroup.Add(new_attributeGroup);
                            metaData.AttributeGroups = listAttrGroup.ToArray();
                        }
                    }
                    else
                    {
                        metaData.AttributeGroups = new DocumentManagement.AttributeGroup[] { new_attributeGroup };
                    }
                    xnode.Metadata = metaData;
                    zdocClient.UpdateNode(ref zdocAuthen, xnode);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void removeCategoryData(string xnodeID, string xCatTemplateName)
        {
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                var xnode = zdocClient.GetNode(ref zdocAuthen, long.Parse(xnodeID));
                if (xnode != null)
                {
                    var metaData = xnode.Metadata;
                    var new_metaData = new DocumentManagement.Metadata();
                    // ***
                    var xCatID = getCatTemplateID(xCatTemplateName);
                    var new_attributeGroup = GetCategoryDef(long.Parse(xCatID.ToString()));
                    if (metaData.AttributeGroups != null)
                    {
                        // Update Category Data
                        // metaData.AttributeGroups = new DocumentManagement.AttributeGroup[] { new_attributeGroup };

                        List<DocumentManagement.AttributeGroup> listAttrGroup = new List<DocumentManagement.AttributeGroup>();


                        var ind = 0;
                        foreach (DocumentManagement.AttributeGroup xAtrrGrp in metaData.AttributeGroups)
                        {
                            if (xAtrrGrp.DisplayName != xCatTemplateName)
                            {
                                listAttrGroup.Add(xAtrrGrp);
                            }
                            ind++;
                        }
                        new_metaData.AttributeGroups = listAttrGroup.ToArray();
                    }

                    xnode.Metadata = new_metaData;
                    zdocClient.UpdateNode(ref zdocAuthen, xnode);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void updateNode(DocumentManagement.Node xnode)
        {
            try
            {
                string strToken = getTokenAdmin();

                zdocAuthen.AuthenticationToken = strToken;

                zdocClient.UpdateNode(ref zdocAuthen, xnode);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        #endregion

        #region WF Services
        public WorkflowService.OTAuthentication getWFAuthen(string xLogin)
        {
            WorkflowService.OTAuthentication wsAuthen = new WorkflowService.OTAuthentication();
            try
            {
                OTFunctions ot = new OTFunctions();
                string otToken = ot.getTokenAdmin();
                Authentication.AuthenticationClient authen = new Authentication.AuthenticationClient();
                Authentication.OTAuthentication oTAuthentication = new Authentication.OTAuthentication();
                oTAuthentication.AuthenticationToken = otToken;

                if (xLogin != "")
                {
                    otToken = authen.ImpersonateUser(ref oTAuthentication, xLogin);
                }

                WorkflowService.WorkflowServiceClient ws = new WorkflowService.WorkflowServiceClient();
                wsAuthen.AuthenticationToken = otToken;

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return wsAuthen;
        }
        public WorkflowService.ProcessInstance startProcess(string xLogin, string xMapID, WorkflowService.ApplicationData[] appDatas, string xTitle)
        {
            try
            {
                zwfAuthen = getWFAuthen(xLogin);
                WorkflowService.ProcessStartData startData = new WorkflowService.ProcessStartData();
                startData.Title = xTitle;
                startData.ApplicationData = appDatas;
                startData.ObjectID = long.Parse(xMapID);
                var procInst = zwfClient.StartProcess(ref zwfAuthen, startData, null, null);
                return procInst;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                return null;
            }
        }
        public WorkflowService.ApplicationData[] getWFStartAppData(string xMapID)
        {
            WorkflowService.ApplicationData[] appDatas;
            try
            {
                zwfAuthen = getWFAuthen("");
                var processData = zwfClient.GetProcessStartData(ref zwfAuthen, long.Parse(xMapID));
                appDatas = processData.ApplicationData;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                appDatas = new WorkflowService.ApplicationData[0];
            }
            return appDatas;
        }
        public WorkflowService.ApplicationData[] getWFStartAppData(string xMapID, string xLogin)
        {
            WorkflowService.ApplicationData[] appDatas;
            try
            {
                zwfAuthen = getWFAuthen(xLogin);
                var processData = zwfClient.GetProcessStartData(ref zwfAuthen, long.Parse(xMapID));
                appDatas = processData.ApplicationData;
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                appDatas = new WorkflowService.ApplicationData[0];
            }
            return appDatas;
        }
        public WorkflowService.ApplicationData[] getWFAppDatas(string xWorkID, string xSubWorkID)
        {
            WorkflowService.ApplicationData[] appDatas;
            try
            {
                zwfAuthen = getWFAuthen("");
                appDatas = zwfClient.GetProcessData(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                appDatas = new WorkflowService.ApplicationData[0];
            }
            return appDatas;
        }
        public WorkflowService.ProcessInstance getProcessStatus(string xWorkID, string xSubWorkID)
        {
            WorkflowService.ProcessInstance processInstance;
            try
            {
                zwfAuthen = getWFAuthen("");
                processInstance = zwfClient.GetProcessStatus(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                processInstance = new WorkflowService.ProcessInstance();
            }
            return processInstance;
        }
        public WorkflowService.ApplicationData[] getWFAppDatas(string xWorkID, string xSubWorkID, string xLogin)
        {
            WorkflowService.ApplicationData[] appDatas;
            try
            {
                zwfAuthen = getWFAuthen(xLogin);
                appDatas = zwfClient.GetProcessData(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                appDatas = new WorkflowService.ApplicationData[0];
            }
            return appDatas;
        }
        public WorkflowService.ApplicationData getWFAppData(string xWorkID, string xSubWorkID)
        {
            WorkflowService.ApplicationData wfAppData = new WorkflowService.ApplicationData();
            try
            {
                zwfAuthen = getWFAuthen("");
                var appDatas = zwfClient.GetProcessData(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
                WorkflowService.AttributeData tmp_wfAppData = new WorkflowService.AttributeData();
                int x = 0;
                for (int wf = 0; wf < appDatas.Length; wf++)
                {
                    try
                    {
                        tmp_wfAppData = (WorkflowService.AttributeData)appDatas[wf];
                        x = wf;
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }
                wfAppData = appDatas[x];
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return wfAppData;
        }
        public WorkflowService.ApplicationData getWFAppData(string xWorkID, string xSubWorkID, string xLogin)
        {
            zwfAuthen = getWFAuthen(xLogin);
            var appDatas = zwfClient.GetProcessData(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
            WorkflowService.AttributeData tmp_wfAppData = new WorkflowService.AttributeData();
            int x = 0;
            for (int wf = 0; wf < appDatas.Length; wf++)
            {
                try
                {
                    tmp_wfAppData = (WorkflowService.AttributeData)appDatas[wf];
                    x = wf;
                    break;
                }
                catch (Exception ex)
                {
                    string strError = ex.Message;
                    //LogHelper.WriteEx(ex);
                }
            }
            var wfAppData = appDatas[x];

            return wfAppData;
        }
        public WorkflowService.AttributeData getWFAttributes(string xWorkID, string xSubWorkID)
        {
            zwfAuthen = getWFAuthen("");
            var appDatas = zwfClient.GetProcessData(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
            WorkflowService.AttributeData tmp_wfAppData = new WorkflowService.AttributeData();
            int x = 0;
            for (int wf = 0; wf < appDatas.Length; wf++)
            {
                try
                {
                    tmp_wfAppData = (WorkflowService.AttributeData)appDatas[wf];
                    x = wf;
                    break;
                }
                catch (Exception ex)
                {
                    string strError = ex.Message;
                    //LogHelper.WriteEx(ex);
                }
            }
            var wfAppData = appDatas[x];
            var wfAttr = (WorkflowService.AttributeData)wfAppData;
            return wfAttr;
        }
        public WorkflowService.AttributeData getWFAttributes(string xWorkID, string xSubWorkID, string xLogin)
        {
            zwfAuthen = getWFAuthen(xLogin);
            var appDatas = zwfClient.GetProcessData(ref zwfAuthen, long.Parse(xWorkID), long.Parse(xSubWorkID));
            WorkflowService.AttributeData tmp_wfAppData = new WorkflowService.AttributeData();
            int x = 0;
            for (int wf = 0; wf < appDatas.Length; wf++)
            {
                try
                {
                    tmp_wfAppData = (WorkflowService.AttributeData)appDatas[wf];
                    x = wf;
                    break;
                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                }
            }
            var wfAppData = appDatas[x];
            var wfAttr = (WorkflowService.AttributeData)wfAppData;
            return wfAttr;
        }
        public string getwfAttrValue(ref WorkflowService.AttributeData wfAttr, string keyName)
        {
            string x = "";
            try
            {
                for (int i = 0; i < wfAttr.Attributes.Attributes.Length; i++)
                {
                    if (keyName.ToUpper() == wfAttr.Attributes.Attributes[i].DisplayName.ToUpper())
                    {
                        var xVal = wfAttr.Attributes.Attributes[i];
                        switch (xVal.GetType().Name)
                        {
                            case "StringAttribute":
                                {
                                    var xData = (WorkflowService.StringAttribute)xVal;
                                    if (xData.Values != null)
                                    {
                                        x = xData.Values[0].ToString();
                                    }

                                }; break;
                            case "UserAttribute":
                                {
                                    try
                                    {
                                        var xData = (WorkflowService.UserAttribute)xVal;
                                        if (xData.Values != null)
                                        {
                                            var xUID = xData.Values[0].ToString();
                                            x = getCSUser(int.Parse(xUID)).Name;
                                        }
                                    }
                                    catch { x = ""; }
                                }; break;
                            case "DateAttribute":
                                {
                                    var xData = (WorkflowService.DateAttribute)xVal;
                                    if (xData.Values != null)
                                    {
                                        x = xData.Values[0].ToString();
                                    }

                                }; break;

                        }

                        break;
                    }
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return x;

        }
        public void setwfAttrValue(ref WorkflowService.AttributeData wfAttr, string xKeyName, string xKeyValue)
        {
            try
            {
                for (int i = 0; i < wfAttr.Attributes.Attributes.Length; i++)
                {
                    if (xKeyName.ToUpper() == wfAttr.Attributes.Attributes[i].DisplayName.ToUpper())
                    {
                        var xVal = wfAttr.Attributes.Attributes[i];
                        switch (xVal.GetType().Name)
                        {
                            case "StringAttribute":
                                {
                                    var xData = (WorkflowService.StringAttribute)xVal;
                                    xData.Values = new string[1] { xKeyValue };

                                }; break;
                            case "UserAttribute":
                                {
                                    var xData = (WorkflowService.UserAttribute)xVal;
                                    try
                                    {
                                        var xUID = getCSUser(xKeyValue);
                                        xData.Values[0] = xUID.ID;
                                    }
                                    catch { xData.Values[0] = null; }
                                }; break;
                            case "DateAttribute":
                                {
                                    try
                                    {
                                        var xData = (WorkflowService.DateAttribute)xVal;
                                        var xDate = DateTime.Parse(xKeyValue);
                                        xData.Values[0] = xDate;
                                    }
                                    catch { }


                                }; break;

                        }

                        break;
                    }
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public void completeWorkItem(string xLogin, string xWorkId, string xSubworkId, string xTaskID, WorkflowService.ApplicationData[] appDatas)
        {
            try
            {
                zwfAuthen = getWFAuthen(xLogin);
                zwfClient.UpdateWorkItemData(ref zwfAuthen, long.Parse(xWorkId), long.Parse(xSubworkId), int.Parse(xTaskID), appDatas);
                zwfClient.CompleteWorkItem(ref zwfAuthen, long.Parse(xWorkId), long.Parse(xSubworkId), int.Parse(xTaskID), String.Empty);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        public void stopWorkItem(string xLogin, string xWorkId)
        {
            try
            {
                zwfAuthen = getWFAuthen(xLogin);
                zwfClient.StopProcess(ref zwfAuthen, long.Parse(xWorkId));
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        #endregion
        // Encryption Document
        #region Encryption Document
        public string encryptDocument(string password, string xfilename, byte[] xcontent, string xparent_nodeid)
        {
            string xnodeid = "";
            try
            {
                // Create input and output file streams.
                Stream in_stream = new MemoryStream(xcontent);
                MemoryStream out_stream = new MemoryStream();
                CryptMemoryStream(password, in_stream, ref out_stream, true);

                var xnew_encryptcontent = out_stream.GetBuffer();
                out_stream.Dispose();
                var xnode = uploadDoc(xparent_nodeid, xfilename, xfilename, xnew_encryptcontent, "");

                xnodeid = xnode.ToString();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xnodeid;
        }
        public byte[] decryptDocument(string password, string xnode_id)
        {
            byte[] xnew_decryptcontent;
            try
            {
                // Create input and output file streams.
                var xcontent = downloadDoc(xnode_id);


                Stream in_stream = new MemoryStream(xcontent);
                MemoryStream out_stream = new MemoryStream();
                CryptMemoryStream(password, in_stream, ref out_stream, false);

                xnew_decryptcontent = out_stream.GetBuffer();
                out_stream.Dispose();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                xnew_decryptcontent = new byte[0];
            }
            return xnew_decryptcontent;
        }
        public static byte[] CryptFile(string password, string in_file, string out_file, bool encrypt)
        {
            byte[] output_content = new byte[0];
            try
            {
                // Create input and output file streams.
                using (FileStream in_stream =
                new FileStream(in_file, FileMode.Open, FileAccess.Read))
                {
                    Stream out_stream = new FileStream(out_file, FileMode.Create,
                            FileAccess.Write);

                    // Encrypt/decrypt the input stream into
                    // the output stream.
                    CryptStream(password, in_stream, ref out_stream, encrypt);
                    output_content = convertStreamToBytes(out_stream);
                    out_stream.Dispose();

                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return output_content;
        }
        public static void CryptFile2(string password, Stream in_stream, string out_file, bool encrypt)
        {
            Stream out_stream = new FileStream(out_file, FileMode.Create, FileAccess.Write);
            // Create input and output file streams.

            // Encrypt/decrypt the input stream into
            // the output stream.
            CryptStream(password, in_stream, ref out_stream, encrypt);


        }
        // Encrypt the data in the input stream into the output stream.
        public static void CryptStream(string password, Stream in_stream, ref Stream out_stream, bool encrypt)
        {
            try
            {
                // Make the encryptor or decryptor.
                ICryptoTransform crypto_transform;

                // Make an AES service provider.
                AesCryptoServiceProvider aes_provider = new AesCryptoServiceProvider();

                // Find a valid key size for this provider.
                int key_size_bits = 0;
                for (int i = 1024; i > 1; i--)
                {
                    if (aes_provider.ValidKeySize(i))
                    {
                        key_size_bits = i;
                        break;
                    }
                }
                //Debug.Assert(key_size_bits > 0);
                Console.WriteLine("Key size: " + key_size_bits);

                // Get the block size for this provider.
                int block_size_bits = aes_provider.BlockSize;

                // Generate the key and initialization vector.
                byte[] key = null;
                byte[] iv = null;
                byte[] salt = { 0x0, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0xF1, 0xF0, 0xEE, 0x21, 0x22, 0x45 };
                MakeKeyAndIV(password, salt, key_size_bits, block_size_bits, out key, out iv);

                if (encrypt)
                {
                    crypto_transform = aes_provider.CreateEncryptor(key, iv);
                }
                else
                {
                    crypto_transform = aes_provider.CreateDecryptor(key, iv);
                }

                // Attach a crypto stream to the output stream.
                // Closing crypto_stream sometimes throws an
                // exception if the decryption didn't work
                // (e.g. if we use the wrong password).

                using (CryptoStream crypto_stream = new CryptoStream(out_stream, crypto_transform, CryptoStreamMode.Write))
                {
                    // Encrypt or decrypt the file.
                    const int block_size = 1024;
                    byte[] buffer = new byte[block_size];
                    int bytes_read;
                    while (true)
                    {
                        // Read some bytes.
                        bytes_read = in_stream.Read(buffer, 0, block_size);
                        if (bytes_read == 0) break;

                        // Write the bytes into the CryptoStream.
                        crypto_stream.Write(buffer, 0, bytes_read);
                    }
                } // using crypto_stream 

                crypto_transform.Dispose();

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        public static void CryptMemoryStream(string password, Stream in_stream, ref MemoryStream out_stream, bool encrypt)
        {
            try
            {
                // Make an AES service provider.
                AesCryptoServiceProvider aes_provider = new AesCryptoServiceProvider();

                // Find a valid key size for this provider.
                int key_size_bits = 0;
                for (int i = 1024; i > 1; i--)
                {
                    if (aes_provider.ValidKeySize(i))
                    {
                        key_size_bits = i;
                        break;
                    }
                }
                //Debug.Assert(key_size_bits > 0);
                Console.WriteLine("Key size: " + key_size_bits);

                // Get the block size for this provider.
                int block_size_bits = aes_provider.BlockSize;

                // Generate the key and initialization vector.
                byte[] key = null;
                byte[] iv = null;
                byte[] salt = { 0x0, 0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0xF1, 0xF0, 0xEE, 0x21, 0x22, 0x45 };
                MakeKeyAndIV(password, salt, key_size_bits, block_size_bits, out key, out iv);

                // Make the encryptor or decryptor.
                ICryptoTransform crypto_transform;
                if (encrypt)
                {
                    crypto_transform = aes_provider.CreateEncryptor(key, iv);
                }
                else
                {
                    crypto_transform = aes_provider.CreateDecryptor(key, iv);
                }

                // Attach a crypto stream to the output stream.
                // Closing crypto_stream sometimes throws an
                // exception if the decryption didn't work
                // (e.g. if we use the wrong password).

                using (CryptoStream crypto_stream = new CryptoStream(out_stream, crypto_transform, CryptoStreamMode.Write))
                {
                    // Encrypt or decrypt the file.
                    const int block_size = 1024;
                    byte[] buffer = new byte[block_size];
                    int bytes_read;
                    while (true)
                    {
                        // Read some bytes.
                        bytes_read = in_stream.Read(buffer, 0, block_size);
                        if (bytes_read == 0) break;

                        // Write the bytes into the CryptoStream.
                        crypto_stream.Write(buffer, 0, bytes_read);
                    }
                } // using crypto_stream 
                crypto_transform.Dispose();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        // Use the password to generate key bytes.
        private static void MakeKeyAndIV(string password, byte[] salt, int key_size_bits, int block_size_bits, out byte[] key, out byte[] iv)
        {
            try
            {
                Rfc2898DeriveBytes derive_bytes = new Rfc2898DeriveBytes(password, salt, 1000);

                key = derive_bytes.GetBytes(key_size_bits / 8);
                iv = derive_bytes.GetBytes(block_size_bits / 8);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                key = new byte[0];
                iv = new byte[0];
            }
        }

        public static byte[] convertStreamToBytes(Stream input)
        {
            byte[] result = new byte[0];
            try
            {
                byte[] buffer = new byte[16 * 1024];
                using (MemoryStream ms = new MemoryStream())
                {
                    int read;
                    while ((read = input.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        ms.Write(buffer, 0, read);
                    }
                    result = ms.ToArray();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return result;
        }
        public string getMineType(string xExtention)
        {
            string xvalue = "";
            try
            {
                switch (xExtention)
                {
                    case "doc": xvalue = "application/msword"; break;
                    case "dot": xvalue = "application/msword"; break;
                    case "docx": xvalue = "application/vnd.openxmlformats-officedocument.wordprocessingml.document"; break;
                    case "dotx": xvalue = "application/vnd.openxmlformats-officedocument.wordprocessingml.template"; break;
                    case "docm": xvalue = "application/vnd.ms-word.document.macroEnabled.12"; break;
                    case "dotm": xvalue = "application/vnd.ms-word.template.macroEnabled.12"; break;
                    case "xls": xvalue = "application/vnd.ms-excel"; break;
                    case "xlt": xvalue = "application/vnd.ms-excel"; break;
                    case "xla": xvalue = "application/vnd.ms-excel"; break;
                    case "xlsx": xvalue = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"; break;
                    case "xltx": xvalue = "application/vnd.openxmlformats-officedocument.spreadsheetml.template"; break;
                    case "xlsm": xvalue = "application/vnd.ms-excel.sheet.macroEnabled.12"; break;
                    case "xltm": xvalue = "application/vnd.ms-excel.template.macroEnabled.12"; break;
                    case "xlam": xvalue = "application/vnd.ms-excel.addin.macroEnabled.12"; break;
                    case "xlsb": xvalue = "application/vnd.ms-excel.sheet.binary.macroEnabled.12"; break;
                    case "ppt": xvalue = "application/vnd.ms-powerpoint"; break;
                    case "pot": xvalue = "application/vnd.ms-powerpoint"; break;
                    case "pps": xvalue = "application/vnd.ms-powerpoint"; break;
                    case "ppa": xvalue = "application/vnd.ms-powerpoint"; break;
                    case "pptx": xvalue = "application/vnd.openxmlformats-officedocument.presentationml.presentation"; break;
                    case "potx": xvalue = "application/vnd.openxmlformats-officedocument.presentationml.template"; break;
                    case "ppsx": xvalue = "application/vnd.openxmlformats-officedocument.presentationml.slideshow"; break;
                    case "ppam": xvalue = "application / vnd.ms - powerpoint.addin.macroEnabled.12"; break;
                    case "pptm": xvalue = "application/vnd.ms-powerpoint.presentation.macroEnabled.12"; break;
                    case "potm": xvalue = "application/vnd.ms-powerpoint.presentation.macroEnabled.12"; break;
                    case "ppsm": xvalue = "application/vnd.ms-powerpoint.slideshow.macroEnabled.12"; break;


                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xvalue;
        }
        #endregion

    }
}
