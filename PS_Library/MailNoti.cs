﻿
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Mail;

namespace PS_Library
{
    public class MailNoti
    {
        #region Email
        //Host: mail.egat.co.th
        //User: 950858
        //Pass: egatcem858*
        //From: ecm @egat.co.th
        //https://mail.egat.co.th/owa2016/

        #region Public
        public bool isMailTest = System.Convert.ToBoolean(ConfigurationManager.AppSettings["isMailTest"].ToString());
        public string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();
        #endregion 

        public void SendEmail(string subject, string display, string mailBody, string mailTos)
        {
            SendEmail(subject, display, mailBody, mailTos, string.Empty, string.Empty);
        }
        public void SendEmail(string subject, string display, string mailBody, string mailTos, string mailCCs, string mailBCCs)
        {
            try
            {
                MailMessage mail = new MailMessage();
                string mailHost = ConfigurationManager.AppSettings["MailHost"];
                string mailUser = ConfigurationManager.AppSettings["MailUser"];
                string mailPass = ConfigurationManager.AppSettings["MailPass"];
                string mailFrom = ConfigurationManager.AppSettings["MailFrom"];
                string mailTest = ConfigurationManager.AppSettings["MailTo_Test"];

                mailBCCs = ConfigurationManager.AppSettings["MailBcc"];
                // For Test
                if (!string.IsNullOrEmpty(mailTest))
                {
                    mailTest.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).ToList().ForEach(x => mail.To.Add(x.Trim()));
                }
                else
                {
                    if (!string.IsNullOrEmpty(mailTos)) mailTos.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList().ForEach(x => mail.To.Add(x.Trim()));

                    if (!string.IsNullOrEmpty(mailCCs)) mailCCs.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList().ForEach(x => mail.CC.Add(x.Trim()));

                    if (!string.IsNullOrEmpty(mailBCCs)) mailBCCs.Split(new string[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToList().ForEach(x => mail.Bcc.Add(x.Trim()));
                }

                mail.From = display != string.Empty ? new MailAddress(mailFrom, display) : new MailAddress(mailFrom);

                mail.Subject = subject;
                mail.IsBodyHtml = true;
                mail.Body = "<html><body>" + mailBody + "</body></html>";

                // Mail Sending
                SmtpClient smtpClient = new SmtpClient(mailHost);

                if (!string.IsNullOrEmpty(mailUser))
                {
                    NetworkCredential cred = new NetworkCredential();
                    cred.UserName = mailUser;
                    cred.Password = mailPass;
                    smtpClient.Credentials = cred;
                    //smtpClient.UseDefaultCredentials = false;
                    smtpClient.Port = 25;
                    smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
                }
                smtpClient.Send(mail);

                smtpClient.Dispose();
                smtpClient = null;

                LogHelper.WriteLogSendMail("subject : " + subject + Environment.NewLine +
                                            "display : " + display + Environment.NewLine +
                                            "mailBody : " + mailBody + Environment.NewLine +
                                            "mailTos : " + mailTos + Environment.NewLine +
                                            "mailCCs : " + mailCCs + Environment.NewLine +
                                            "mailBCCs : " + mailBCCs);
            }
            catch (Exception ex)
            {
                //string subject, string display, string mailBody, string mailTos, string mailCCs, string mailBCCs
                LogHelper.Write("ERRFUNC: SendEmail ; subject = " + subject + "mailTos = " + mailTos + " ");
                LogHelper.Write("ERRMSG : " + ex.Message);
            }
        }
        public string GenECMSystemSignature()
        {
            string xSignature = "";
            xSignature += "<p> </p>";
            xSignature += "<p>System Admin (ระบบ ECM)</p>";
            xSignature += "<p> </p>";
            return xSignature;
        }
        public string GenMailBody(string xSubject, string xToName_Position, string xContent, string xSignature)
        {
            string xbody = "";
            xbody += "<table style='font-name:tahoma font-size:10 text-align:left v-align:top'>";

            xbody += "<tr>";
            xbody += "<td colspan=3>";
            xbody += "" + xToName_Position;
            xbody += "</td>";
            xbody += "<tr>";
            //  Content
            xbody += "<tr>";
            xbody += "<td style='width:50'>";
            xbody += "</td>";
            xbody += "<td style='width:650'>";
            xbody += xContent;
            xbody += "</td>";
            xbody += "<td style='width:25'>";
            xbody += "";
            xbody += "</td>";
            // Signature
            if (xSignature == "") { xSignature = GenECMSystemSignature(); }
            xbody += "</tr>";
            xbody += "<tr>";
            xbody += "<td colspan=3>";
            xbody += xSignature;
            xbody += "</td>";
            xbody += "</tr>";
            xbody += "</table>";
            return xbody;
        }
        #endregion



        public void SendMailOut(string xCode, string empid, string type)
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            //var emp = empInfo.getInFoByEmpID(empid);
            //string email = emp.EMAIL;
            string sql = "";
            //string xWorkLink = "http://" + ConfigurationManager.AppSettings["HOST_NAME"].ToString() + @"/tmps/forms/PreviewDocCS?nodeid={0}";
            //string xAttachLink = "http://" + ConfigurationManager.AppSettings["HOST_NAME"].ToString() + @"/tmps/forms/PreviewDocCS?nodeid=";
            string strURL_Dash = "";
            //Create EMAIL

            //TMPS.Class.MailNoti mailNoti = new Class.MailNoti();
            MailNoti mailNoti = new MailNoti();

            string xTo_Email = "";
            string xTo_CC = "";
            string xToName_Position = "เรียน ";

            string type_no = "";
            string subj_type = "";

            if (type == "bid")
            {
                sql = "select distinct bid_no,staff_id from wf_tc_taskassignment where task_code in (" + xCode + ")";

            }
            else if (type == "job")
            {
                sql = "select distinct job_no,staff_id from wf_tc_taskassignment where task_code in (" + xCode + ")";

            }

            var dt2 = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);


            if (dt2.Rows.Count > 0)
            {
                if (isMailTest == true)
                {
                    xTo_Email = ConfigurationManager.AppSettings["MailTo_Test"].ToString();
                    //xToName_Position = emp.SNAME + " (" + emp.MC_SHORT + ")";
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        var emp = empInfo.getInFoByEmpID(dt2.Rows[i]["staff_id"].ToString());
                        string email = emp.EMAIL;
                        xToName_Position = emp.SNAME + " (" + emp.MC_SHORT + ")";
                        string project_name = "";
                        string strProject = " สำหรับ Project ";
                        var dr = dt2.Rows[i];
                        if (type == "bid")
                        {
                            DataTable dt = new DataTable();
                            sql = "select * from wf_tc_taskassignment where task_code in (" + xCode + ") and bid_no ='" + dt2.Rows[i]["bid_no"].ToString() + "' and staff_id ='" + dt2.Rows[i]["staff_id"].ToString() + "'";
                            dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (project_name == "")
                                {
                                    project_name += dt.Rows[j]["project_name"].ToString();
                                }
                                else
                                {
                                    if (project_name.IndexOf(dt.Rows[j]["project_name"].ToString()) > -1) { }
                                    else
                                    {
                                        project_name += ", " + dt.Rows[j]["project_name"].ToString();
                                    }

                                }
                            }
                            strProject += project_name;
                            strURL_Dash = "สามารถ Click Link (" + dr["bid_no"].ToString() + ")<a href= '" + ConfigurationManager.AppSettings["url_MyTaskList"].ToString() + "?zuserlogin=" + emp.PERNR + "&zmode=B'>Open ECM</a> เพื่อดูข้อมูลได้" + "<br>";
                            subj_type = "Bid No.";
                            type_no = dr["bid_no"].ToString();
                            string xSubject = "ท่านมี " + subj_type + " " + type_no + " เข้ามา";

                            // Email Body
                            string xContent = "<br>"
                                                + "ท่านมี " + subj_type + " " + type_no + strProject + " เข้ามา ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                               + "<br>" + strURL_Dash
                                                + "<br><br> ขอแสดงความนับถือ "
                                               + " <br>  ";

                            // Send Email
                            if (isMailTest == true)
                            {
                                xTo_CC = "";
                                xContent += " หมายเหตุ : ขออภัยหากอีเมล์ฉบับบนี้รบกวนท่าน เนื่องจากขณะนี้ระบบอยู่ในระหว่างกำลังทดสอบ";
                            }

                            string xBody = mailNoti.GenMailBody(xSubject, xToName_Position, xContent, "");

                            string xBCC = ConfigurationManager.AppSettings["MailTo_Test"];
                            // For Debug
                            //xTo_Email = "eknawat.choojaisiriroj@atos.net";
                            //xTo_CC = "";
                            mailNoti.SendEmail(xSubject, "System Admin (ECM)", xBody, xTo_Email, xTo_CC, xBCC);

                        }
                        else if (type == "job")
                        {
                            DataTable dt = new DataTable();
                            sql = "select * from wf_tc_taskassignment where task_code in (" + xCode + ") and job_no ='" + dt2.Rows[i]["job_no"].ToString() + "' and staff_id ='" + dt2.Rows[i]["staff_id"].ToString() + "'";
                            dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (project_name == "")
                                {
                                    project_name += dt.Rows[j]["project_name"].ToString();
                                }
                                else
                                {
                                    if (project_name.IndexOf(dt.Rows[j]["project_name"].ToString()) > -1) { }
                                    else
                                    {
                                        project_name += ", " + dt.Rows[j]["project_name"].ToString();
                                    }

                                }
                            }
                            strProject += project_name;
                            strURL_Dash = "สามารถ Click Link (" + dr["job_no"].ToString() + ")<a href= '" + ConfigurationManager.AppSettings["url_MyTaskList"].ToString() + "?zuserlogin=" + emp.PERNR + "&zmode=J'>Open ECM</a> เพื่อดูข้อมูลได้" + "<br>";
                            subj_type = "Job No.";
                            type_no = dr["job_no"].ToString();


                            string xSubject = "ท่านมี " + subj_type + " " + type_no + " เข้ามา";

                            // Email Body
                            string xContent = "<br>"
                                                + "ท่านมี " + subj_type + " " + type_no + strProject + " เข้ามา ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                               //+ "<br><table style='border: 1px solid LightGrey;border-spacing:0;font-family: tahoma;font-size:10pt;width:300px;'>"
                                               //+ "<tr><td style='border: 1px solid LightGrey;border-spacing:0;font-family: tahoma;font-size:10pt;width:200px'>Job No</td><td style='border: 1px solid LightGrey;border-spacing:0;font-family: tahoma;font-size:10pt;width:100px'>Job Revision</td></tr>"
                                               //+ strJobList
                                               // + "</table>"
                                               // + "<br>สามารถ Click  Link " + xWorkLink + " เพื่อดูข้อมูลได้ "
                                               //+ strAttachList
                                               // + "<br>สามารถ Click  Link <a href='" + strURL_Dash + "'>Open ECM</a> "
                                               + "<br>" + strURL_Dash
                                                + "<br><br> ขอแสดงความนับถือ "
                                               + " <br>  ";

                            // Send Email
                            if (isMailTest == true)
                            {
                                xTo_CC = "";
                                xContent += " หมายเหตุ : ขออภัยหากอีเมล์ฉบับบนี้รบกวนท่าน เนื่องจากขณะนี้ระบบอยู่ในระหว่างกำลังทดสอบ";
                            }
                            else
                            {
                                xTo_Email = email;
                            }

                            string xBody = mailNoti.GenMailBody(xSubject, xToName_Position, xContent, "");

                            string xBCC = ConfigurationManager.AppSettings["MailTo_Test"];
                            // For Debug
                            //xTo_Email = "eknawat.choojaisiriroj@atos.net";
                            //xTo_CC = "";
                            mailNoti.SendEmail(xSubject, "System Admin (ECM)", xBody, xTo_Email, xTo_CC, xBCC);
                        }

                    }


                }

            }
        }

        public void SendMailMonitor(string email, string bidno, string type,string content)
        {
            string sql = "select * from vw_ps_bid_monitor bm where bid_no='" + bidno + "'  and job_revision = (select max(job_revision) from vw_ps_bid_monitor bb where bm.job_no = bb.job_no) order by job_no";
            //Create EMAIL

            //TMPS.Class.MailNoti mailNoti = new Class.MailNoti();
            MailNoti mailNoti = new MailNoti();

            string xTo_Email = "";
            string xTo_CC = "";
            string xToName_Position = "เรียน หัวหน้ากองทุกท่าน";
            string strProject = " Job No. ";
            string type_no = "";
            string subj_type = "";
            string project_name = "";
            var dt2 = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);


            if (dt2.Rows.Count > 0)
            {
                if (isMailTest == true)
                {
                    xTo_Email = ConfigurationManager.AppSettings["MailTo_Test"].ToString();
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        var dr = dt2.Rows[i];

                        if (project_name == "")
                        {
                            project_name += dt2.Rows[i]["job_no"].ToString();
                        }
                        else
                        {
                            if (project_name.IndexOf(dt2.Rows[i]["job_no"].ToString()) > -1) { }
                            else
                            {
                                project_name += ", " + dt2.Rows[i]["job_no"].ToString();
                            }

                        }
                        //strURL_Dash = "สามารถ Click Link (" + dr["bid_no"].ToString() + ")<a href= '" + ConfigurationManager.AppSettings["url_MyTaskList"].ToString() + "?zuserlogin=" + emp.PERNR + "&zmode=B'>Open ECM</a> เพื่อดูข้อมูลได้" + "<br>";
                        subj_type = "Bid No.";
                        type_no = dr["bid_no"].ToString();
                    }
                    strProject += project_name;
                    string xSubject = "";
                    string xContent = "";
                    if (type == "start")
                    {
                        xSubject = "ท่านมี " + subj_type + " " + type_no + " Start";
                        xContent = "<br>"
                                           + "ท่านมี " + subj_type + " " + type_no + " Start ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                          + "<br>" + strProject
                                           + "<br><br> ขอแสดงความนับถือ "
                                          + " <br>  ";

                    }
                    else if (type == "req")
                    {
                        xSubject = "ท่านมี " + subj_type + " " + type_no + " รอดำเนินการอยู่";
                        xContent = "<br>"
                                           + "ท่านมี " + subj_type + " " + type_no + " รอดำเนินการอยู่ ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                          + "<br>" + content //strProject
                                           + "<br><br> ขอแสดงความนับถือ "
                                          + " <br>  ";
                    }
                    else if (type == "change")
                    {
                        xSubject = "ท่านมี " + subj_type + " " + type_no + " ปรับเปลี่ยนแผน";
                        xContent = "<br>"
                                           + "ท่านมี " + subj_type + " " + type_no + " ปรับเปลี่ยนแผน ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                          + "<br>" + strProject
                                           + "<br><br> ขอแสดงความนับถือ "
                                          + " <br>  ";
                    }
                    if (isMailTest == true)
                    {
                        xTo_CC = "";
                        xContent += " หมายเหตุ : ขออภัยหากอีเมล์ฉบับบนี้รบกวนท่าน เนื่องจากขณะนี้ระบบอยู่ในระหว่างกำลังทดสอบ";
                    }
                    else
                    {
                        xTo_Email = email;
                    }

                    string xBody = mailNoti.GenMailBody(xSubject, xToName_Position, xContent, "");

                    string xBCC = ConfigurationManager.AppSettings["MailTo_Test"];
                    mailNoti.SendEmail(xSubject, "System Admin (ECM)", xBody, xTo_Email, xTo_CC, xBCC);
                }

            }
        }
        public void SendMailTOR(string login, string bidno, string tor, string sec, string notmain)
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            var emp = empInfo.getInFoByEmpID(login);
            string email = emp.EMAIL;
            string xToName_Position = emp.SNAME + " (" + emp.MC_SHORT + ")";
            MailNoti mailNoti = new MailNoti();

            string xTo_Email = "";
            string xTo_CC = "";
            string xSubject = "";
            string xContent = "";

            xTo_Email = ConfigurationManager.AppSettings["MailTo_Test"].ToString();
           

            xSubject = "ท่านมี Bid No. " + bidno + " Update";
            if (notmain != "")
            {
                xContent = "<br>"
                               + "ท่านมี Bid No. " + bidno + " มีการ Update โปรดตรวจสอบข้อมูล ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                               + "<br><br> ขอแสดงความนับถือ "
                              + " <br>  ";
            }
            else
            {
                xContent = "<br>"
                                  + "ท่านมี Bid No. " + bidno + " Template Name: " + tor + " Section: " + sec + " มีการ Update โปรดตรวจสอบข้อมูล ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                  + "<br><br> ขอแสดงความนับถือ "
                                 + " <br>  ";

            }
            
            xTo_CC = "";
            xContent += " หมายเหตุ : ขออภัยหากอีเมล์ฉบับบนี้รบกวนท่าน เนื่องจากขณะนี้ระบบอยู่ในระหว่างกำลังทดสอบ";
            if (isMailTest == true)
            {
            }
            else
            {
                xTo_Email = email;
            }

            string xBody = mailNoti.GenMailBody(xSubject, xToName_Position, xContent, "");

            string xBCC = ConfigurationManager.AppSettings["MailTo_Test"];
            // For Debug
            //xTo_Email = "eknawat.choojaisiriroj@atos.net";
            //xTo_CC = "";
            mailNoti.SendEmail(xSubject, "System Admin (ECM)", xBody, xTo_Email, xTo_CC, xBCC);
        }
        public void SendMailSupNotice(string empid, string bidno, string sn, string subject,string type)
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            var emp = empInfo.getInFoByEmpID(empid);
            string email = emp.EMAIL;
            string xToName_Position = emp.SNAME + " (" + emp.MC_SHORT + ")";
            MailNoti mailNoti = new MailNoti();

            string xTo_Email = "";
            string xTo_CC = "";
            string xSubject = "";
            string xContent = "";
            string strURL = "";
            xTo_Email = ConfigurationManager.AppSettings["MailTo_Test"].ToString();
            if (type == "submit")
            {

                strURL = "สามารถ Click Link (" + bidno + ")<a href= '" + ConfigurationManager.AppSettings["url_SupNotice"].ToString() + "?zuserlogin=" + emp.PERNR + "&zmode=view&sncode=" + sn + "&bidno=" + bidno + "'>Open Link</a> เพื่อดูข้อมูลได้" + "<br>";

                xSubject = "ท่านมี Bid No. " + bidno + " Subject: " + subject + " เข้ามา";

                xContent = "<br>"
                                  + "ท่านมี Bid No. " + bidno + " S/N No.: " + sn + " Subject: " + subject + " เข้ามา โปรดตรวจสอบข้อมูล ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                  + "<br><br> ขอแสดงความนับถือ "
                                 + " <br>  "
                                 + strURL;
            }
            else if (type == "appr")
            {
                strURL = "สามารถ Click Link (" + bidno + ")<a href= '" + ConfigurationManager.AppSettings["url_SupNotice"].ToString() + "?zuserlogin=" + emp.PERNR + "&zmode=view&sncode=" + sn + "&bidno=" + bidno + "'>Open Link</a> เพื่อดูข้อมูลได้" + "<br>";

                xSubject = "Bid No. " + bidno + " Subject: " + subject + " ถูก Assign เรียบร้อบแล้ว";

                xContent = "<br>"
                                  + "ท่านมี Bid No. " + bidno + " S/N No.: " + sn + " Subject: " + subject + " ถูก Assign เรียบร้อบแล้ว ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                  + "<br><br> ขอแสดงความนับถือ "
                                 + " <br>  "
                                 + strURL;
            }
            else if (type == "fin")
            {
                strURL = "สามารถ Click Link (" + bidno + ")<a href= '" + ConfigurationManager.AppSettings["url_SupNotice"].ToString() + "?zuserlogin=" + emp.PERNR + "&zmode=view&sncode=" + sn + "&bidno=" + bidno + "'>Open Link</a> เพื่อดูข้อมูลได้" + "<br>";

                xSubject = "Bid No. " + bidno + " Subject: " + subject + " ดำเนินการเสร็จสิ้น";

                xContent = "<br>"
                                  + "ท่านมี Bid No. " + bidno + " S/N No.: " + sn + " Subject: " + subject + " ดำเนินการเสร็จสิ้น ณ วันที่ " + DateTime.Now.ToString("dd/MM/yyyy") + " เวลา " + DateTime.Now.ToString("HH:mm")
                                  + "<br><br> ขอแสดงความนับถือ "
                                 + " <br>  "
                                 + strURL;

            }



            

            xTo_CC = "";
            xContent += " หมายเหตุ : ขออภัยหากอีเมล์ฉบับบนี้รบกวนท่าน เนื่องจากขณะนี้ระบบอยู่ในระหว่างกำลังทดสอบ";
            if (isMailTest == true)
            {
            }
            else
            {
                xTo_Email = email;
            }

            string xBody = mailNoti.GenMailBody(xSubject, xToName_Position, xContent, "");

            string xBCC = ConfigurationManager.AppSettings["MailTo_Test"];
            // For Debug
            //xTo_Email = "eknawat.choojaisiriroj@atos.net";
            //xTo_CC = "";
            mailNoti.SendEmail(xSubject, "System Admin (ECM)", xBody, xTo_Email, xTo_CC, xBCC);
        }

    }

}
    

