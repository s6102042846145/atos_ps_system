﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class DropdownList
    {
        public int id { get; set; }
        public string value { get; set; }
        public string name { get; set; }
    }
}
