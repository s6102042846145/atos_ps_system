﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class QC02
    {
        public int row_id { get; set; }
        public string bid_no { get; set; }
        public string bid_revision { get; set; }
        public string schedule_name { get; set; }
        public string contract_no { get; set; }
        public string assign_to { get; set; }


        public string shipment_date { get; set; }
        public string remark { get; set; }

    }
}
