﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class Poaccount
    {
        //item

        public string PoEcm { get; set; }
        public string PoItem { get; set; }
        public string SerialNo { get; set; }
        public string GlAccount { get; set; }
        public string Costcenter { get; set; }
        public string SdDoc { get; set; }
        public string ItmNumber { get; set; }
        public string AssetNo { get; set; }

        public string SubNumber { get; set; }
        public string Orderid { get; set; }
        public string GrRcpt { get; set; }
        public string UnloadPt { get; set; }
        public string CoArea { get; set; }
        public string Costobject { get; set; }
        public string ProfitCtr { get; set; }
        public string WbsElement { get; set; }
        public string Network { get; set; }
        public string FundsCtr { get; set; }
        public string Activity { get; set; }


        //public string PoEcm { get; set; }
        //public string PoItem { get; set; }
        //public string SerialNo { get; set; }
        //public string DeleteInd { get; set; }
        //public string CreatDate { get; set; }
        //public decimal Quantity { get; set; }
        //public decimal DistrPerc { get; set; }
        //public decimal NetValue { get; set; }
        //public string GlAccount { get; set; }
        //public string BusArea { get; set; }
        //public string Costcenter { get; set; }
        //public string SdDoc { get; set; }
        //public string ItmNumber { get; set; }
        //public string SchedLine { get; set; }
        //public string AssetNo { get; set; }
        
        //public string Orderid { get; set; }
        //public string GrRcpt { get; set; }
        //public string UnloadPt { get; set; }
        //public string CoArea { get; set; }
        //public string Costobject { get; set; }
       
        //public string WbsElement { get; set; }
        //public string Network { get; set; }
        //public string RlEstKey { get; set; }
        //public string PartAcct { get; set; }
        //public string CmmtItem { get; set; }
        //public string RecInd { get; set; }
        //public string FundsCtr { get; set; }
        //public string Fund { get; set; }
        //public string FuncArea { get; set; }
        //public string RefDate { get; set; }
        //public string TaxCode { get; set; }
        //public string Taxjurcode { get; set; }
        //public decimal NondItax { get; set; }
        //public string Acttype { get; set; }
        //public string CoBusproc { get; set; }
        //public string ResDoc { get; set; }
        //public string ResItem { get; set; }
        //public string Activity { get; set; }
        //public string GrantNbr { get; set; }
        //public string CmmtItemLong { get; set; }
        //public string FuncAreaLong { get; set; }

    }
}
