﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class Pocond
    {
        //item
        public string PoEcm { get; set; }
        public string ConditionNo { get; set; }
        public string ItmNumber { get; set; }
        public string CondStNo { get; set; }
        public string CondCount { get; set; }
        public string CondType { get; set; }
        public decimal CondValue { get; set; }
        public string Currency { get; set; }
        public string CurrencyIso { get; set; }
        public string CondUnit { get; set; }
        public string CondUnitIso { get; set; }
        public decimal CondPUnt { get; set; }
        public string Applicatio{ get; set; }
        public string Conpricdat { get; set; }
        public string Calctypcon { get; set; }
        public decimal Conbaseval{ get; set; }
        public decimal Numconvert { get; set; }
        public decimal Denominato { get; set; }
        public string Condtype{ get; set; }
        public string StatCon{ get; set; }
        public string Scaletype{ get; set; }
        public string Accruals{ get; set; }
        public string Coninvolst{ get; set; }
        public string Condorigin{ get; set; }
        public string Groupcond{ get; set; }
        public string CondUpdat{ get; set; }
        public string AccessSeq{ get; set; }
        public string Condcount{ get; set; }
        public string Condcntrl{ get; set; }
        public string Condisacti{ get; set; }
        public string Condclass{ get; set; }
        public float Factbasval{ get; set; }
        public string Scalebasin{ get; set; }
        public decimal Scalbasval{ get; set; }
        public string Unitmeasur{ get; set; }
        public string UnitmeasurIso{ get; set; }
        public string Currenckey{ get; set; }
        public string CurrenckeyIso{ get; set; }
        public string Condincomp{ get; set; }
        public string Condconfig{ get; set; }
        public string Condchaman{ get; set; }
        public string CondNo{ get; set; }
        public string ChangeId{ get; set; }
        public string VendorNo{ get; set; }
    }
}
