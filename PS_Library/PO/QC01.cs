﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class QC01
    {
        public string bid_no { get; set; }
        public string bid_revision { get; set; }
        public string item_no { get; set; }
        public string schedule_name { get; set; }
        public string contract_no { get; set; }
        public string description { get; set; }
        public string part { get; set; }
        public string qty { get; set; }
        public string unit { get; set; }
        public string ms { get; set;}
        public string qp { get; set;}
        public string ts { get; set;}
        public string tp { get; set;}
        public string iw { get; set;}
        public string ir { get; set;}
        public string fir { get; set;}
        public string cr { get; set;}
        public string tr { get; set; }
    }
}
