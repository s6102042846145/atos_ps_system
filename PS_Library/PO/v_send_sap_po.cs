﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class v_send_sap_po
    {
      public string PoEcm{ get; set; }
      public string PoItem{ get; set; }
      public string WbsElement{ get; set; }
      public string FundsCtr{ get; set; }
      public string CompCode{ get; set; }
      public string DocType{ get; set; }
      public string Vendor{ get; set; }
      public string PurchOrg{ get; set; }
      public string PurGroup{ get; set; }
      public string Currency{ get; set; }
      public string ExchRate{ get; set; }
      public string DocDate{ get; set; }
      public string QuotDate{ get; set; }
      public string Methcode{ get; set; }
      public string Contno{ get; set; }
      public string ShortText{ get; set; }
      public string Material{ get; set; }
      public string Plant{ get; set; }
      public string Quantity{ get; set; }
      public string PoUnit{ get; set; }
      public string NetPrice{ get; set; }
      public string PriceUnit{ get; set; }
      public string InfoUpd{ get; set; }
      public string Acctasscat{ get; set; }
      public string GrInd{ get; set; }
      public string IrInd{ get; set; }
      public string GrBasediv{ get; set; }
      public string AcknowlNo{ get; set; }
      public string Incoterms1{ get; set; }
      public string VendPart{ get; set; }
      public string PreqName{ get; set; }
      public string Vendrbatch{ get; set; }
      public string Itemno{ get; set; }
      public string DeliveryDate{ get; set; }
      public string TextLine{ get; set; }
      public string TextId{ get; set; }
    }
}
