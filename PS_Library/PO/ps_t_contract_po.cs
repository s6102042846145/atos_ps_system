﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class ps_t_contract_po
    {
        public int rowid { get; set; }
        public string Contract_No { get; set; }
        public string po_no { get; set; }
        public string po_item { get; set; }
        public string bid_no { get; set; }
        public int bid_rev { get; set; }
        public string schedule_name { get; set; }
        public string part_code { get; set; }
        public string item_no { get; set; }
        public string equip_code { get; set; }
        public int qty { get; set; }
        public string created_by { get; set; }
        public DateTime created_datetime { get; set; }
        public string updated_by { get; set; }
        public DateTime updated_datetime { get; set; }
        public string total_price { get; set; }

    }
}
