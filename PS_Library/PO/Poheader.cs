﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class Poheader
    {
        public string PoEcm { get; set; }
        public string PoNumber { get; set; }
        public string CompCode { get; set; }
        public string DocType { get; set; }
        public string Vendor { get; set; }
        public string Langu { get; set; }
        public string PurchOrg { get; set; }
        public string PurGroup { get; set; }
        public string Currency { get; set; }
        public decimal ExchRate { get; set; }
        public string DocDate { get; set; }
        public string Warranty { get; set; }
        public string QuotDate { get; set; }
        public string Methcode { get; set; }
        public string Contno { get; set; }


        //public string PoEcm { get; set; }
        //public string PoNumber { get; set; }
        //public string CompCode { get; set; }
        //public string DocType { get; set; }
        //public string DeleteInd { get; set; }
        //public string Status { get; set; }
        //public string CreatDate { get; set; }
        //public string CreatedBy { get; set; }
        //public string ItemIntvl { get; set; }
        //public string Vendor { get; set; }
        //public string Langu { get; set; }
        //public string LanguIso { get; set; }
        //public string Pmnttrms { get; set; }
        //public decimal Dscnt1To { get; set; }
        //public decimal Dscnt2To { get; set; }
        //public decimal Dscnt3To { get; set; }
        //public decimal DsctPct1 { get; set; }
        //public decimal DsctPct2 { get; set; }
        //public string PurchOrg { get; set; }
        //public string PurGroup { get; set; }
        //public string Currency { get; set; }
        //public string CurrencyIso { get; set; }
        //public decimal ExchRate { get; set; }
        //public string ExRateFx { get; set; }
        //public string DocDate { get; set; }
        //public string VperStart { get; set; }
        //public string VperEnd { get; set; }
        //public string Warranty { get; set; }
        //public string Quotation { get; set; }
        //public string QuotDate { get; set; }
        //public string Ref1 { get; set; }
        //public string SalesPers { get; set; }
        //public string Telephone { get; set; }
        //public string SupplVend { get; set; }
        //public string Customer { get; set; }
        //public string Agreement { get; set; }
        //public string GrMessage { get; set; }
        //public string SupplPlnt { get; set; }
        //public string Incoterms1 { get; set; }
        //public string Incoterms2 { get; set; }
        //public string CollectNo { get; set; }
        //public string DiffInv { get; set; }
        //public string OurRef { get; set; }
        //public string Logsystem { get; set; }
        //public string Subitemint { get; set; }
        //public string PoRelInd { get; set; }
        //public string RelStatus { get; set; }
        //public string VatCntry { get; set; }
        //public string VatCntryIso { get; set; }
        //public string ReasonCancel { get; set; }
        //public string ReasonCode { get; set; }
        //public string Methcode { get; set; }
        //public string Contno { get; set; }
    }
}
