﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class Potextitem
    {
        public string PoEcm { get; set; }
        public string PoNumber { get; set; }
        public string PoItem { get; set; }
        public string TextId { get; set; }
        public string TextForm { get; set; }
        public string TextLine { get; set; }
    }
}
