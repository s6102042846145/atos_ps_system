﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library.PO
{
    public class ResultData
    {
        public string doc_name { get; set; }
        public string doc_desc { get; set; }
        public string doc_sheetno { get; set; }
        public string doc_nodeid { get; set; }
    }
}
