﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using Newtonsoft.Json;
using System.Collections;
using System.Globalization;

namespace PS_Library.PO
{
    public static class Sap_PO
    {

        public static string psDB = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();


        public static string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();
        static oraDBUtil oraDBUtil = new oraDBUtil();


        private static ps_sap_log sap_log = new ps_sap_log();

        private static List<Poaccount> listPoaccountItems = new List<Poaccount>();
        private static List<Pocond> listPocondItems = new List<Pocond>();
        private static List<Poheader> listPoheaderItems = new List<Poheader>();
        private static List<Poitem> listPoitemtItems = new List<Poitem>();
        private static List<Poschedule> listPoscheduleItems = new List<Poschedule>();
        private static List<Potextitem> listPotextitemItems = new List<Potextitem>();
        public static string connstr { get; set; }

        public static string cmdstrFull { get; set; }
        public static string cmdstr { get; set; }

        public static string cmdvalue { get; set; }
        public static string condi { get; set; }
        public static string order { get; set; }

        private static string Mode = "Test Run";
        private static string Batch_no = "";
        public static string SendToSap(string cNO, string mode,string batch_no)
        {
            string poNumber = "";
            // DataTable dtCNO = new DataTable();
            DataTable dt = new DataTable();

            Mode = mode;
            Batch_no = batch_no;



            dt = getDataPOToSap(cNO);
            //dt = getDataStockToSap();
            // genXMLRequestStock(dt);
            setDataToSap(dt, cNO);
            //poNumber = genXMLRequestPO();

            //ps_t_contract_po contract_po = new ps_t_contract_po();
            //List<ps_t_contract_po> list_contract_po = new List<ps_t_contract_po>();
            //if (poNumber != "")
            //{
            //    foreach(DataRow dr in dt.Rows)
            //    {
            //        contract_po.Contract_No = dr["Contno"].ToString();
            //        contract_po.po_no = poNumber;
            //        contract_po.po_item = dr["PoItem"].ToString();
            //        contract_po.bid_no = dr["bid_no"].ToString();
            //        contract_po.bid_rev = bid_rev;
            //        contract_po.schedule_name = dr["schedule_name"].ToString();
            //        contract_po.part_code = dr["Part"].ToString();
            //        contract_po.item_no = dr["Itemno"].ToString();
            //        contract_po.equip_code = dr["Material"].ToString();
            //        contract_po.qty = Convert.ToInt32(dr["NetPrice"].ToString());
            //        //insert_ps_t_contract_po();
            //    }


            //}


            return poNumber;
            //createRequestPO();
        }

        public static string createDataToSap(string cNO, int bid_rev)
        {
            string poNumber = "";
            // DataTable dtCNO = new DataTable();
            DataTable dt = new DataTable();






            dt = getDataPO(cNO);
            //dt = getDataStockToSap();
            // genXMLRequestStock(dt);
            setDataPO(dt, cNO);
            //poNumber = genXMLRequestPO();

            //ps_t_contract_po contract_po = new ps_t_contract_po();
            //List<ps_t_contract_po> list_contract_po = new List<ps_t_contract_po>();
            //if (poNumber != "")
            //{
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        contract_po.Contract_No = dr["Contno"].ToString();
            //        contract_po.po_no = poNumber;
            //        contract_po.po_item = dr["PoItem"].ToString();
            //        contract_po.bid_no = dr["bid_no"].ToString();
            //        contract_po.bid_rev = bid_rev;
            //        contract_po.schedule_name = dr["schedule_name"].ToString();
            //        contract_po.part_code = dr["Part"].ToString();
            //        contract_po.item_no = dr["Itemno"].ToString();
            //        contract_po.equip_code = dr["Material"].ToString();
            //        contract_po.qty = Convert.ToInt32(dr["NetPrice"].ToString());
            //        //insert_ps_t_contract_po();
            //    }


            //}


            return poNumber;
            //createRequestPO();
        }

        public class Map_Data_PO_ToSAP
        {
            public string vendor { get; set; }
            public string shedule_name { get; set; }
            public string part { get; set; }
            public string currency { get; set; }
            public bool mat { get; set; }
            public bool service { get; set; }
            public bool oversea { get; set; }
            public bool local { get; set; }
            public string item_no { get; set; }
            public string sap_doctype { get; set; }

            public bool localTran { get; set; }
        }

        public class ps_t_bid_selling_cmpo_item
        {
            public string bid_no { get; set; }
            public int bid_revision { get; set; }
            public string schedule_name { get; set; }
            public string bidder_id { get; set; }
            public string vendor_code { get; set; }
            public string contract_no { get; set; }
            public string part_no { get; set; }
            public string item_no { get; set; }
            public string equip_code { get; set; }
            public string po_num { get; set; }
            public string po_item_no { get; set; }
            public string po_item_desc { get; set; }
            public decimal po_qty { get; set; }
            public decimal po_unit_price { get; set; }
            public string po_unit { get; set; }
            public decimal po_total_price { get; set; }
            public string po_currency { get; set; }
            public string sap_po_num { get; set; }
            public string sap_po_item { get; set; }
            public string sap_mat_code { get; set; }
            public bool waive_test { get; set; }
            public decimal total_receive_qty { get; set; }
            public string account_assignment_code { get; set; }
            public string SalesOrder { get; set; }
            public string InternalOrder { get; set; }
            public string Asset { get; set; }
            public string GL { get; set; }
            public string CostCenter { get; set; }
            public string WBSNo { get; set; }
            public string Network { get; set; }
            public string Fund { get; set; }
            public string sap_status { get; set; }
            public string sap_doctype { get; set; }
        }


        private static void setDataPO(DataTable dt, string cNO)
        {

            //ZCPO สญ. ซื้อ
            //ZCSV สญ. จ้าง
            //ZOCP สญ. ซื้อ ต่าง ปท.
            //ZOCS สญ. จ้าง ต่าง ปท

            string runnigPo = "";
            string POID = "";
            string PoItem = "";
            List<string> listPoEcm = new List<string>();
            int numPoitem = 10;
            List<string> listPoItem = new List<string>();

            DataTable dtPoaccount = new DataTable();
            DataTable dtPocond = new DataTable();
            DataTable dtPoheader = new DataTable();
            DataTable dtPoitem = new DataTable();
            DataTable dtPoschedule = new DataTable();
            DataTable dtPotextitem = new DataTable();

            Poaccount poaccountItems = new Poaccount();
            Poheader PoheaderItems = new Poheader();
            Poitem PoitemItems = new Poitem();
            Poschedule PoscheduleItems = new Poschedule();
            Potextitem PotextitemItems = new Potextitem();
            //Poaccount poaccountItems = new Poaccount();
            DataTable dtClone = dt.Clone();
            foreach (DataRow dr in dt.Rows)
            {
                dtClone.ImportRow(dr);
            }
            DataTable dtPocm = RemoveDuplicateRows(dtClone, "vendor_code");


            Map_Data_PO_ToSAP mapdata = new Map_Data_PO_ToSAP();
            List<Map_Data_PO_ToSAP> listMap = new List<Map_Data_PO_ToSAP>();


            ps_t_bid_selling_cmpo_item cmpoitem = new ps_t_bid_selling_cmpo_item();
            List<ps_t_bid_selling_cmpo_item> listcmpoitem = new List<ps_t_bid_selling_cmpo_item>();
            //foreach (DataRow drPo in dtPocm.Rows)
            //{
            string equip_code = "";
            string sap_material_group = "";
            foreach (DataRow dr in dt.Rows)
            {
                equip_code = "";
                sap_material_group = "";
                DataTable dtequip_codeItem = new DataTable();
                dtequip_codeItem = get_equip_code(dr["item_no"].ToString());



                if (dtequip_codeItem.Rows.Count > 0)
                {
                    equip_code = dtequip_codeItem.Rows[0]["equip_code"].ToString();
                }
                DataTable dtequip_code = new DataTable();

                dtequip_code = get_ps_m_equipcode(equip_code);
                if (dtequip_code.Rows.Count > 0)
                {
                    sap_material_group = dtequip_code.Rows[0]["sap_material_group"] == null ? "" : dtequip_code.Rows[0]["sap_material_group"].ToString();
                }


                mapdata = new Map_Data_PO_ToSAP();
                mapdata.vendor = dr["vendor_code"].ToString().Trim();
                mapdata.shedule_name = dr["schedule_name"].ToString().Trim();
                mapdata.part = dr["Part"].ToString().Trim();

                if (sap_material_group.Contains("SV"))
                {
                    mapdata.service = true;
                    mapdata.mat = false;
                }
                else
                {
                    mapdata.service = false;
                    mapdata.mat = true;
                }
                mapdata.item_no = dr["item_no"].ToString().Trim();

                if (dr["type"].ToString() == "Substation1")
                {
                    if (dr["cif_amount"].ToString() != "0" && dr["cif_unit_price"].ToString() != "0")
                    {
                        mapdata.oversea = true;
                        mapdata.local = false;
                        mapdata.localTran = false;
                        mapdata.currency = dr["currency"].ToString().Trim();


                        if (equip_code == "")
                        {
                            //ZOCS สญ. จ้าง ต่าง ปท
                            mapdata.sap_doctype = "ZOCS";
                        }
                        else
                        {
                            //ZOCP สญ. ซื้อ ต่าง ปท.
                            mapdata.sap_doctype = "ZOCP";
                        }
                    }
                    if (dr["ewp_amount"].ToString() != "0" && dr["ewp_unit_price"].ToString() != "0")
                    {
                        mapdata.oversea = false;
                        mapdata.local = true;
                        mapdata.localTran = false;


                        if (equip_code == "")
                        {
                            //ZCSV สญ. จ้าง
                            mapdata.sap_doctype = "ZCSV";
                            mapdata.currency = "THB";
                        }
                        else
                        {
                            //ZCPO สญ. ซื้อ
                            mapdata.sap_doctype = "ZCPO";
                            mapdata.currency = "THB";
                        }


                    }
                    listMap.Add(mapdata);

                    if (dr["ltc_amount"].ToString() != "0" && dr["ltc_unit_price"].ToString() != "0")
                    {
                        //doctype = ZCSV
                        Map_Data_PO_ToSAP mapdataTran = new Map_Data_PO_ToSAP();
                        mapdataTran.vendor = mapdata.vendor;
                        mapdataTran.shedule_name = mapdata.shedule_name;
                        mapdataTran.part = mapdata.part;
                        mapdataTran.currency = "THB";
                        mapdataTran.item_no = mapdata.item_no;
                        mapdataTran.local = true;
                        mapdataTran.localTran = true;
                        mapdataTran.sap_doctype = "ZCSV";
                        listMap.Add(mapdataTran);
                    }


                }


                if (dr["type"].ToString() == "Substation2")
                {
                    
                  

                    if (dr["lc_amount"].ToString() != "0" && dr["lc_unit_price"].ToString() != "0")
                    {

                        mapdata.oversea = false;
                        mapdata.local = true;
                        mapdata.localTran = false;


                        if (equip_code == "")
                        {
                            //ZCSV สญ. จ้าง
                            mapdata.sap_doctype = "ZCSV";
                            mapdata.currency = "THB";
                        }
                        else
                        {
                            //ZCPO สญ. ซื้อ
                            mapdata.sap_doctype = "ZCPO";
                            mapdata.currency = "THB";
                        }
                    }
                    listMap.Add(mapdata);
                }
                if (dr["type"].ToString() == "Line1")
                {
                    if (dr["ltc_amount"].ToString() != "0" && dr["ltc_unit_price"].ToString() != "0")
                    {
                        //doctype = ZCSV
                        Map_Data_PO_ToSAP mapdataTran = new Map_Data_PO_ToSAP();
                        mapdataTran.vendor = mapdata.vendor;
                        mapdataTran.shedule_name = mapdata.shedule_name;
                        mapdataTran.part = mapdata.part;
                        mapdataTran.currency = "THB";
                        mapdataTran.item_no = mapdata.item_no;
                        mapdataTran.local = true;
                        mapdataTran.localTran = true;
                        mapdataTran.sap_doctype = "ZCSV";
                        listMap.Add(mapdataTran);
                    }

                   
                }

                if (dr["type"].ToString() == "Line2")
                {
                    if (dr["cif_amount"].ToString() != "0" && dr["cif_unit_price"].ToString() != "0")
                    {
                        mapdata.oversea = true;
                        mapdata.local = false;
                        mapdata.localTran = false;
                        mapdata.currency = dr["currency"].ToString().Trim();


                        if (equip_code == "")
                        {
                            //ZOCS สญ. จ้าง ต่าง ปท
                            mapdata.sap_doctype = "ZOCS";
                        }
                        else
                        {
                            //ZOCP สญ. ซื้อ ต่าง ปท.
                            mapdata.sap_doctype = "ZOCP";
                        }
                    }
                    if (dr["ewp_amount"].ToString() != "0" && dr["ewp_unit_price"].ToString() != "0")
                    {
                        mapdata.oversea = false;
                        mapdata.local = true;
                        mapdata.localTran = false;


                        if (equip_code == "")
                        {
                            //ZCSV สญ. จ้าง
                            mapdata.sap_doctype = "ZCSV";
                            mapdata.currency = "THB";
                        }
                        else
                        {
                            //ZCPO สญ. ซื้อ
                            mapdata.sap_doctype = "ZCPO";
                            mapdata.currency = "THB";
                        }


                    }
                    listMap.Add(mapdata);

                    if (dr["ltc_amount"].ToString() != "0" && dr["ltc_unit_price"].ToString() != "0")
                    {
                        //doctype = ZCSV
                        Map_Data_PO_ToSAP mapdataTran = new Map_Data_PO_ToSAP();
                        mapdataTran.vendor = mapdata.vendor;
                        mapdataTran.shedule_name = mapdata.shedule_name;
                        mapdataTran.part = mapdata.part;
                        mapdataTran.currency = "THB";
                        mapdataTran.item_no = mapdata.item_no;
                        mapdataTran.local = true;
                        mapdataTran.localTran = true;
                        mapdataTran.sap_doctype = "ZCSV";
                        listMap.Add(mapdataTran);
                    }


                }
                if (dr["type"].ToString() == "Line3")
                {
                    if (dr["fc_amount"].ToString() != "0" && dr["fc_unit_price"].ToString() != "0")
                    {
                        mapdata.oversea = true;
                        mapdata.local = false;
                        mapdata.localTran = false;
                        mapdata.currency = dr["currency"].ToString().Trim();


                        if (equip_code == "")
                        {
                            //ZOCS สญ. จ้าง ต่าง ปท
                            mapdata.sap_doctype = "ZOCS";
                        }
                        else
                        {
                            //ZOCP สญ. ซื้อ ต่าง ปท.
                            mapdata.sap_doctype = "ZOCP";
                        }
                    }
                    if (dr["lc_amount"].ToString() != "0" && dr["lc_unit_price"].ToString() != "0")
                    {
                        mapdata.oversea = false;
                        mapdata.local = true;
                        mapdata.localTran = false;


                        if (equip_code == "")
                        {
                            //ZCSV สญ. จ้าง
                            mapdata.sap_doctype = "ZCSV";
                            mapdata.currency = "THB";
                        }
                        else
                        {
                            //ZCPO สญ. ซื้อ
                            mapdata.sap_doctype = "ZCPO";
                            mapdata.currency = "THB";
                        }


                    }
                    listMap.Add(mapdata);

                }


            }


            var listGrupPO = listMap.GroupBy(x => new { x.vendor, x.shedule_name, x.currency, x.service, x.mat, x.oversea, x.local, x.part, x.sap_doctype, x.localTran }).ToList();

            foreach (var item in listGrupPO)
            {
                POID = getPOID();
                runnigPo = "";
                numPoitem = 0;
                for (int i = 0; i < 10 - POID.Length; i++)
                {
                    runnigPo = runnigPo + "0";
                    if (i == 10 - POID.Length - 1)
                    {
                        runnigPo = "PO" + runnigPo + POID;
                    }
                }

                var listItem = listMap.Where(x => x.vendor == item.Key.vendor && x.shedule_name == item.Key.shedule_name
                && x.currency == item.Key.currency && x.service == item.Key.service && x.mat == item.Key.mat
                && x.oversea == item.Key.oversea && x.local == item.Key.local && x.part == item.Key.part && x.sap_doctype == item.Key.sap_doctype && x.localTran == item.Key.localTran).ToList();

                foreach (var itemMap in listItem)
                {
                    //DataRow[] dr = dt.Select("vendor_code = '"+ itemMap.vendor.ToString().Trim() + "' and schedule_name = '" + itemMap.shedule_name.ToString().Trim() + "' and Part = '" + itemMap.part.ToString().Trim()+ "' and item_no ='" + itemMap.item_no.ToString().Trim() +"'");



                    DataView dv = new DataView(dt);
                    dv.RowFilter = "vendor_code = '" + itemMap.vendor.ToString().Trim() + "' and schedule_name = '" + itemMap.shedule_name.ToString().Trim() + "' and Part = '" + itemMap.part.ToString().Trim() + "' and item_no ='" + itemMap.item_no.ToString().Trim() + "'"; // query example = "id = 10"



                    if (dv.Count > 0)
                    {
                        DataTable dtItem = dv.ToTable();

                        numPoitem += 10;
                        cmpoitem = new ps_t_bid_selling_cmpo_item();
                        cmpoitem.bid_no = dtItem.Rows[0]["bid_no"].ToString();
                        cmpoitem.bid_revision = Convert.ToInt32(dtItem.Rows[0]["bid_revision"].ToString());
                        cmpoitem.schedule_name = dtItem.Rows[0]["schedule_name"].ToString();
                        cmpoitem.bidder_id = dtItem.Rows[0]["bidder_id"].ToString();
                        cmpoitem.vendor_code = dtItem.Rows[0]["vendor_code"].ToString();
                        cmpoitem.contract_no = cNO;
                        cmpoitem.part_no = dtItem.Rows[0]["part"].ToString();
                        cmpoitem.item_no = dtItem.Rows[0]["item_no"].ToString();
                        DataTable dtequip_codeItem = new DataTable();
                        dtequip_codeItem = get_equip_code(dtItem.Rows[0]["item_no"].ToString());
                        if (dtequip_codeItem.Rows.Count > 0)
                        {
                            cmpoitem.equip_code = dtequip_codeItem.Rows[0]["equip_code"].ToString();
                            DataTable dtequip_code = new DataTable();
                            dtequip_code = get_ps_m_equipcode(cmpoitem.equip_code);
                            if (dtequip_code.Rows.Count > 0)
                            {
                                cmpoitem.sap_mat_code = dtequip_code.Rows[0]["sap_mat_code"].ToString();
                            }

                        }
                        if (cmpoitem.equip_code == "")
                        {
                            cmpoitem.po_unit = "LUM";
                        }
                        else
                        {
                            cmpoitem.po_unit = dtItem.Rows[0]["unit"].ToString();
                        }
                        cmpoitem.po_num = runnigPo;
                        cmpoitem.po_item_no = numPoitem.ToString();
                        cmpoitem.po_item_desc = dtItem.Rows[0]["description"].ToString();
                        cmpoitem.po_qty = dtItem.Rows[0]["qty"] == null ? 0 : dtItem.Rows[0]["qty"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["qty"].ToString());
                        if (dtItem.Rows[0]["type"].ToString() == "Substation1")
                        {
                            if (!item.Key.localTran)
                            {
                                if (dtItem.Rows[0]["cif_amount"].ToString() != "0" && dtItem.Rows[0]["cif_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["cif_unit_price"] == null ? 0 : dtItem.Rows[0]["cif_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["cif_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["cif_amount"] == null ? 0 : dtItem.Rows[0]["cif_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["cif_amount"].ToString());
                                }
                                if (dtItem.Rows[0]["ewp_amount"].ToString() != "0" && dtItem.Rows[0]["ewp_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["ewp_unit_price"] == null ? 0 : dtItem.Rows[0]["ewp_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ewp_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["ewp_amount"] == null ? 0 : dtItem.Rows[0]["ewp_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ewp_amount"].ToString());
                                }
                            }
                            else
                            {
                                if (dtItem.Rows[0]["ltc_amount"].ToString() != "0" && dtItem.Rows[0]["ltc_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["ltc_unit_price"] == null ? 0 : dtItem.Rows[0]["ltc_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ltc_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["ltc_amount"] == null ? 0 : dtItem.Rows[0]["ltc_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ltc_amount"].ToString());
                                }
                            }




                        }
                        if (dtItem.Rows[0]["type"].ToString() == "Substation2")
                        {
                            if (!item.Key.localTran)
                            {
                                if (dtItem.Rows[0]["lc_amount"].ToString() != "0" && dtItem.Rows[0]["lc_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["lc_unit_price"] == null ? 0 : dtItem.Rows[0]["lc_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["lc_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["lc_amount"] == null ? 0 : dtItem.Rows[0]["lc_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["lc_amount"].ToString());
                                }
                            }
                        }
                        if (dtItem.Rows[0]["type"].ToString() == "Line1")
                        {
                            if (item.Key.localTran)
                            {
                                if (dtItem.Rows[0]["ltc_amount"].ToString() != "0" && dtItem.Rows[0]["ltc_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["ltc_unit_price"] == null ? 0 : dtItem.Rows[0]["ltc_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ltc_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["ltc_amount"] == null ? 0 : dtItem.Rows[0]["ltc_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ltc_amount"].ToString());
                                }
                            }
                        }

                        if (dtItem.Rows[0]["type"].ToString() == "Line2")
                        {
                            if (!item.Key.localTran)
                            {
                                if (dtItem.Rows[0]["cif_amount"].ToString() != "0" && dtItem.Rows[0]["cif_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["cif_unit_price"] == null ? 0 : dtItem.Rows[0]["cif_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["cif_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["cif_amount"] == null ? 0 : dtItem.Rows[0]["cif_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["cif_amount"].ToString());
                                }
                                if (dtItem.Rows[0]["ewp_amount"].ToString() != "0" && dtItem.Rows[0]["ewp_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["ewp_unit_price"] == null ? 0 : dtItem.Rows[0]["ewp_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ewp_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["ewp_amount"] == null ? 0 : dtItem.Rows[0]["ewp_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ewp_amount"].ToString());
                                }
                            }
                            else
                            {
                                if (dtItem.Rows[0]["ltc_amount"].ToString() != "0" && dtItem.Rows[0]["ltc_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["ltc_unit_price"] == null ? 0 : dtItem.Rows[0]["ltc_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ltc_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["ltc_amount"] == null ? 0 : dtItem.Rows[0]["ltc_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["ltc_amount"].ToString());
                                }
                            }




                        }
                        if (dtItem.Rows[0]["type"].ToString() == "Line3")
                        {
                            if (!item.Key.localTran)
                            {
                                if (dtItem.Rows[0]["fc_amount"].ToString() != "0" && dtItem.Rows[0]["fc_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["fc_unit_price"] == null ? 0 : dtItem.Rows[0]["fc_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["fc_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["fc_amount"] == null ? 0 : dtItem.Rows[0]["fc_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["fc_amount"].ToString());
                                }
                                if (dtItem.Rows[0]["lc_amount"].ToString() != "0" && dtItem.Rows[0]["ewp_unit_price"].ToString() != "0")
                                {
                                    cmpoitem.po_unit_price = dtItem.Rows[0]["lc_unit_price"] == null ? 0 : dtItem.Rows[0]["lc_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["lc_unit_price"].ToString());
                                    cmpoitem.po_total_price = dtItem.Rows[0]["lc_amount"] == null ? 0 : dtItem.Rows[0]["lc_amount"].ToString() == "" ? 0 : Convert.ToDecimal(dtItem.Rows[0]["lc_amount"].ToString());
                                }
                            }
                            


                        }
                        //cmpoitem.po_total_price = runnigPo;
                        cmpoitem.po_currency = item.Key.currency;
                        cmpoitem.sap_doctype = item.Key.sap_doctype;
                        listcmpoitem.Add(cmpoitem);
                    }

                }

            }

            foreach (var cmpo in listcmpoitem)
            {
                insert_ps_t_bid_selling_cmpo_item(cmpo);
            }

            string a = "";
            //}


            //foreach (DataRow drPo in dtPocm.Rows)
            //{
            //    POID = getPOID();
            //    runnigPo = "";
            //    numPoitem = 0;
            //    for (int i = 0; i < 10 - POID.Length; i++)
            //    {
            //        runnigPo = runnigPo + "0";
            //        if (i == 10 - POID.Length-1)
            //        {
            //            runnigPo = "PO" + runnigPo + POID;
            //        }
            //    }

            //    listPoEcm.Add(runnigPo);

            //    //Poheader
            //    PoheaderItems = new Poheader();
            //    PoheaderItems.PoEcm = runnigPo;
            //    PoheaderItems.CompCode = "1000";
            //    PoheaderItems.DocType = "ZCPO";
            //    PoheaderItems.Vendor = "0080006345";//drPo["vendor_code"].ToString().Trim();
            //   // PoheaderItems.Langu = "EN";
            //    PoheaderItems.PurchOrg = "2000";
            //    PoheaderItems.PurGroup = "253";
            //    PoheaderItems.Currency = drPo["currency"].ToString();
            //    //PoheaderItems.ExchRate = dr["ExchRate"] == null ? 0 : dr["ExchRate"].ToString() == "" ? 0 : Convert.ToDecimal(dr["ExchRate"].ToString());
            //    PoheaderItems.DocDate = "2021-03-24";
            //    PoheaderItems.QuotDate = "2021-03-24";
            //    PoheaderItems.Methcode = "03";
            //    PoheaderItems.Contno = cNO;
            //    listPoheaderItems.Add(PoheaderItems);

            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        if(drPo["vendor_code"].ToString() == dr["vendor_code"].ToString())
            //        {
            //            PoItem = numPoitem.ToString();
            //            listPoItem.Add(PoItem);
            //            numPoitem += 10;

            //            //Poaccount
            //            poaccountItems = new Poaccount();
            //            poaccountItems.PoEcm = runnigPo;
            //            poaccountItems.PoItem = numPoitem.ToString();
            //            poaccountItems.SerialNo = "01";
            //            //poaccountItems.GlAccount = "5704000";
            //            poaccountItems.WbsElement = "L-TS12-14-S02-40000";
            //            poaccountItems.FundsCtr = "N333000";
            //            listPoaccountItems.Add(poaccountItems);


            //            //Poitem
            //            PoitemItems = new Poitem();
            //            PoitemItems.PoItem = numPoitem.ToString();
            //            DataTable dtequip_code = new DataTable();
            //            dtequip_code = get_equip_code(dr["item_no"].ToString());
            //            if (dtequip_code.Rows.Count >0)
            //            {
            //                PoitemItems.Material =  dtequip_code.Rows[0]["equip_code"].ToString();
            //            }
            //            //PoitemItems.ShortText = dr["schedule_name"].ToString();
            //            PoitemItems.Plant = "HQ31";
            //            PoitemItems.Quantity = dr["qty"] == null ? 0 : dr["qty"].ToString() == "" ? 0 : Convert.ToDecimal(dr["qty"].ToString());
            //            PoitemItems.PoUnit = dr["unit"].ToString() == ""?"EA": dr["unit"].ToString();
            //            PoitemItems.NetPrice = 4500;//dr["cif_unit_price"] == null ? 0 : dr["cif_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dr["cif_unit_price"].ToString());
            //            PoitemItems.PriceUnit = 1;
            //            PoitemItems.InfoUpd = "X";
            //            PoitemItems.Acctasscat = "Q";
            //            PoitemItems.GrInd = "X";
            //            PoitemItems.IrInd = "X";
            //            PoitemItems.GrBasediv = "X"; 
            //            PoitemItems.AcknowlNo = PoitemItems.Material;
            //            PoitemItems.Incoterms1 = "EXW";
            //            PoitemItems.VendPart = dr["Part"].ToString();
            //            PoitemItems.PreqName = "224669";
            //            PoitemItems.Vendrbatch = dr["item_no"].ToString();
            //            PoitemItems.Itemno = dr["item_no"].ToString();
            //            PoitemItems.PoEcm = runnigPo;
            //            listPoitemtItems.Add(PoitemItems);

            //            //Poschedule
            //            PoscheduleItems = new Poschedule();
            //            PoscheduleItems.PoEcm = runnigPo;
            //            PoscheduleItems.PoItem = numPoitem.ToString();
            //            PoscheduleItems.DeliveryDate = "2021-03-24";
            //            listPoscheduleItems.Add(PoscheduleItems);


            //            //Potextitem
            //            PotextitemItems = new Potextitem();
            //            PotextitemItems.PoEcm = runnigPo;
            //            PotextitemItems.PoItem = numPoitem.ToString();
            //            PotextitemItems.TextId = "F03";
            //            PotextitemItems.TextForm = "*";
            //            //PotextitemItems.TextLine = dr["schedule_name"].ToString();
            //            listPotextitemItems.Add(PotextitemItems);


            //        }
            //    }

            //}


        }

        public static DataTable get_sap_p_m_sap_exchangerate(string loa_date, string curency)
        {

            DataTable dt = new DataTable();
            cmdstrFull = "";
            cmdstr = @" select* from
                            (SELECT*
                            FROM p_m_sap_exchangerate
                            where Fromcurrency ='" + curency + "'" +
                            "order by valid_startdate desc)  ";
            condi = "where rownum = 1 ";
            order = " ";

            bool chk = false;


            cmdstrFull = cmdstr + condi + order;
            try
            {

                dt = oraDBUtil._getDT(cmdstrFull, commonDB);
            }
            catch (Exception ex)
            {

            }


            return dt;
        }

        private static void setDataToSap(DataTable dt, string cNO)
        {

            //ZCPO สญ. ซื้อ
            //ZCSV สญ. จ้าง
            //ZOCP สญ. ซื้อ ต่าง ปท.
            //ZOCS สญ. จ้าง ต่าง ปท
            List<ps_t_bid_selling_cmpo_item> listPoNum = new List<ps_t_bid_selling_cmpo_item>();
            ps_t_bid_selling_cmpo_item PoNum = new ps_t_bid_selling_cmpo_item();
            DataTable dtPoNum = new DataTable();
            if (dt.Rows.Count > 0)
            {
                dtPoNum = dt.AsEnumerable()
             .GroupBy(r => new { po_num = r["po_num"] })
             .Select(g => g.OrderBy(r => r["po_num"]).First())
             .CopyToDataTable();
            }

            foreach (DataRow drPo in dtPoNum.Rows)
            {
                PoNum = new ps_t_bid_selling_cmpo_item();
                PoNum.po_num = drPo["po_num"].ToString();
                PoNum.sap_doctype = drPo["sap_doctype"].ToString();
                PoNum.vendor_code = drPo["vendor_code"].ToString();
                PoNum.po_currency = drPo["po_currency"].ToString();
                PoNum.vendor_code = drPo["vendor_code"].ToString();
                PoNum.bid_no = drPo["bid_no"].ToString();
                PoNum.bid_revision = drPo["bid_revision"] == null ? 0 : drPo["bid_revision"].ToString() == "" ? 0 : Convert.ToInt32(drPo["bid_revision"].ToString());
                PoNum.schedule_name = drPo["schedule_name"].ToString();
                listPoNum.Add(PoNum);
            }

            DataTable dtPoaccount = new DataTable();
            DataTable dtPocond = new DataTable();
            DataTable dtPoheader = new DataTable();
            DataTable dtPoitem = new DataTable();
            DataTable dtPoschedule = new DataTable();
            DataTable dtPotextitem = new DataTable();

            Poaccount poaccountItems = new Poaccount();
            Poheader PoheaderItems = new Poheader();
            Pocond PocondItems = new Pocond();
            Poitem PoitemItems = new Poitem();
            Poschedule PoscheduleItems = new Poschedule();
            Potextitem PotextitemItems = new Potextitem();

            foreach (var item in listPoNum)
            {
                if (item.po_num == "PO0000000481")
                {
                    string a = "";
                }

                listPoheaderItems.Clear();
                listPoaccountItems.Clear();
                listPocondItems.Clear();
                listPoitemtItems.Clear();
                listPoscheduleItems.Clear();
                listPotextitemItems.Clear();

                DataTable dtCmpo = getData_bid_selling_cmpo(item.vendor_code, item.bid_no, item.bid_revision.ToString(), item.schedule_name);

                //Poheader
                PoheaderItems = new Poheader();
                PoheaderItems.PoEcm = item.po_num;
                PoheaderItems.CompCode = "1000";
                PoheaderItems.DocType = item.sap_doctype;
                PoheaderItems.Vendor = item.vendor_code.Trim();//drPo["vendor_code"].ToString().Trim();
                                                               // PoheaderItems.Langu = "";   // PoheaderItems.Langu = "EN";
                if (dtCmpo.Rows.Count > 0)
                {
                    PoheaderItems.PurchOrg = dtCmpo.Rows[0]["purchase_org"] == null ? "2000" : dtCmpo.Rows[0]["purchase_org"].ToString() == "" ? "2000" : dtCmpo.Rows[0]["purchase_org"].ToString();//PurchOrg
                    PoheaderItems.PurGroup = dtCmpo.Rows[0]["purchase_group"] == null ? "253" : dtCmpo.Rows[0]["purchase_group"].ToString() == "" ? "253" : dtCmpo.Rows[0]["purchase_group"].ToString(); //PurGroup
                }
                else
                {
                    PoheaderItems.PurchOrg = "2000";//PurchOrg
                    PoheaderItems.PurGroup = "253";//PurGroup
                }
                PoheaderItems.Currency = item.po_currency;
                if (dtCmpo.Rows.Count > 0)
                {
                    DataTable dtexchangerate = get_sap_p_m_sap_exchangerate(dtCmpo.Rows[0]["loa_date"].ToString(), item.po_currency);
                    if (dtexchangerate.Rows.Count > 0)
                    {
                        PoheaderItems.ExchRate = dtexchangerate.Rows[0]["RATE"] == null ? (decimal)30.3313 : Convert.ToDecimal(dtexchangerate.Rows[0]["RATE"].ToString());//เอา loa_confrim_date table pocm ไปดึงจาก orcale p_m_sap_exchangerate on validdate on fromcurrency แล้วเอา rate  เงื่อนไข validdate ลาสุด
                    }
                    PoheaderItems.DocDate = getFormatDate(DateTime.Now, "YYYY-MM-DD");// date.now 2021-03-24
                    PoheaderItems.QuotDate = getFormatDate(Convert.ToDateTime(dtCmpo.Rows[0]["loa_date"].ToString()), "YYYY-MM-DD");//loa_confrim_date table pocm 2021-03-24
                    PoheaderItems.Methcode = dtCmpo.Rows[0]["purchase_method"].ToString(); ;//Purchasing Method
                }
                PoheaderItems.Contno = cNO;
                listPoheaderItems.Add(PoheaderItems);

                DataView dv = new DataView(dt);
                string filler = "po_num = '" + item.po_num.ToString() + "'";
                dv.RowFilter = filler;// query example = "id = 10"
                DataTable dtItem = dv.ToTable();



                foreach (DataRow drItem in dtItem.Rows)
                {


                    //            //Poaccount
                    poaccountItems = new Poaccount();
                    poaccountItems.PoEcm = drItem["po_num"].ToString();
                    poaccountItems.PoItem = drItem["po_item_no"].ToString();
                    poaccountItems.SerialNo = "01";
                    if (drItem["Gl"] != null)
                    {
                        if (drItem["Gl"].ToString() != "")
                        {
                            poaccountItems.GlAccount = drItem["Gl"].ToString();
                        }
                    }
                    if (drItem["CostCenter"] != null)
                    {
                        if (drItem["CostCenter"].ToString() != "")
                        {
                            poaccountItems.Costcenter = drItem["CostCenter"].ToString();
                        }
                    }
                    //if (drItem["SalesOrder"] != null)
                    //{
                    //    poaccountItems.SdDoc = drItem["SalesOrder"].ToString();
                    //}
                    //if (drItem["InternalOrder"] != null)
                    //{
                    //    poaccountItems.ItmNumber = drItem["InternalOrder"].ToString();
                    //}
                    if (drItem["Asset"] != null)
                    {
                        if (drItem["Asset"].ToString() != "")
                        {
                            poaccountItems.AssetNo = drItem["Asset"].ToString();
                            poaccountItems.SubNumber = "0";
                        }
                    }
                    if (drItem["WBSNo"] != null)
                    {
                        if (drItem["WBSNo"].ToString() != "")
                        {
                            poaccountItems.WbsElement = drItem["WBSNo"].ToString();
                        }
                    }
                    if (drItem["Fund"] != null)
                    {
                        if (drItem["Fund"].ToString() != "")
                        {
                            poaccountItems.FundsCtr = drItem["Fund"].ToString();
                        }
                    }
                    if (drItem["Network"] != null)
                    {
                        if (drItem["Network"].ToString() != "")
                        {
                            poaccountItems.Network = drItem["Network"].ToString();
                            poaccountItems.Activity = "40";
                        }
                    }
                    listPoaccountItems.Add(poaccountItems);

                    if (drItem["sap_doctype"].ToString() == "ZOCS" || drItem["sap_doctype"].ToString() == "ZOCP")
                    {
                        //Pocond
                        PocondItems = new Pocond();
                        PocondItems.PoEcm = drItem["po_num"].ToString();
                        PocondItems.ItmNumber = drItem["po_item_no"].ToString();
                        PocondItems.Currency = drItem["po_currency"].ToString();
                        PocondItems.Condtype = "PB00";
                        PocondItems.CondValue = 1;
                        PocondItems.ChangeId = "l";
                        listPocondItems.Add(PocondItems);

                        PocondItems = new Pocond();
                        PocondItems.PoEcm = drItem["po_num"].ToString();
                        PocondItems.ItmNumber = drItem["po_item_no"].ToString();
                        PocondItems.Currency = drItem["po_currency"].ToString();
                        PocondItems.Condtype = "ZOA1";
                        PocondItems.CondValue = 2;
                        PocondItems.ChangeId = "l";
                        listPocondItems.Add(PocondItems);
                        //gen 2 รอบ 1 = PB00 value = inc 
                        //รอบ 2 = ZOA1 value = custom
                    }

                    //Poitem
                    PoitemItems = new Poitem();
                    PoitemItems.PoItem = drItem["po_item_no"].ToString();
                    PoitemItems.ShortText = drItem["schedule_name"].ToString();
                    PoitemItems.Material = drItem["equip_code"] == null ? "" : drItem["equip_code"].ToString();
                    PoitemItems.Plant = "HQ31";//FIX
                    if (PoitemItems.Material == "")
                    {
                        PoitemItems.MatlGroup = "TS-MSC";
                    }
                    PoitemItems.VendMat = "";//vendorcode
                    //PoitemItems.ShortText = dr["schedule_name"].ToString();
                    PoitemItems.Quantity = drItem["po_qty"] == null ? 0 : drItem["po_qty"].ToString() == "" ? 0 : Convert.ToDecimal(drItem["po_qty"].ToString());
                    PoitemItems.PoUnit = drItem["po_unit"].ToString() == "" ? "EA" : drItem["po_unit"].ToString();
                    PoitemItems.NetPrice = drItem["po_total_price"] == null ? 0 : drItem["po_total_price"].ToString() == "" ? 0 : Convert.ToDecimal(drItem["po_total_price"].ToString());//dr["cif_unit_price"] == null ? 0 : dr["cif_unit_price"].ToString() == "" ? 0 : Convert.ToDecimal(dr["cif_unit_price"].ToString());
                    PoitemItems.PriceUnit = 1;
                    PoitemItems.InfoUpd = "X";
                    PoitemItems.Acctasscat = drItem["account_assignment_code"].ToString(); ;

                    PoitemItems.IrInd = "X";

                    //ZCPO สญ. ซื้อ
                    //ZCSV สญ. จ้าง
                    //ZOCP สญ. ซื้อ ต่าง ปท.
                    //ZOCS สญ. จ้าง ต่าง ปท
                    if (drItem["sap_doctype"].ToString() == "ZCPO" || drItem["sap_doctype"].ToString() == "ZOCP")
                    {
                        PoitemItems.GrInd = "A";
                    }
                    else
                    {
                        PoitemItems.GrInd = "X";
                    }

                    if (drItem["sap_doctype"].ToString() == "ZCPO")
                    {
                        PoitemItems.GrBasediv = "X";
                    }
                    if (drItem["sap_doctype"].ToString() == "ZOCP")
                    {
                        PoitemItems.AcknReqd = "X";
                    }
                    PoitemItems.AcknowlNo = PoitemItems.Material;
                    if (drItem["sap_doctype"].ToString() == "ZOCP")
                    {
                        PoitemItems.ConfCtrl = "0004"; //Fix = 0004 กรณี PO  ซื้อสินค้าต่างประเทศ
                    }

                    PoitemItems.Incoterms1 = "CIF"; //IncoTerm
                    PoitemItems.VendPart = drItem["part_no"].ToString();
                    if (dtCmpo.Rows.Count > 0)
                    {
                        PoitemItems.PreqName = dtCmpo.Rows[0]["requisitioner_empid"] == null ? "" : dtCmpo.Rows[0]["requisitioner_empid"].ToString();// "224669";//drItem["requisitioner_emid"].ToString();

                    }
                    PoitemItems.Vendrbatch = drItem["item_no"].ToString();
                    PoitemItems.Itemno = drItem["item_no"].ToString();
                    PoitemItems.PoEcm = drItem["po_num"].ToString(); ;
                    listPoitemtItems.Add(PoitemItems);

                    //Poschedule
                    PoscheduleItems = new Poschedule();
                    PoscheduleItems.PoEcm = drItem["po_num"].ToString();
                    PoscheduleItems.PoItem = drItem["po_item_no"].ToString();
                    if (dtCmpo.Rows.Count > 0)
                    {
                        PoscheduleItems.DeliveryDate = getFormatDate(Convert.ToDateTime(dtCmpo.Rows[0]["loa_date"].ToString()).AddMonths(4), "YYYY-MM-DD"); ; //"2021-03-24";//loa_confrim_date +4 m
                    }
                    listPoscheduleItems.Add(PoscheduleItems);


                    //            //Potextitem
                    int count = drItem["schedule_name"].ToString().Length / 132;
                    string TextLine = drItem["schedule_name"].ToString();
                    if (count > 0)
                    {


                        for (int i = 0; i < count; i++)
                        {

                            PotextitemItems = new Potextitem();
                            PotextitemItems.PoEcm = drItem["po_num"].ToString();
                            PotextitemItems.PoItem = drItem["po_item_no"].ToString();

                            PotextitemItems.TextId = "F03";
                            PotextitemItems.TextForm = "*";
                            if (TextLine.Length <= 132)
                            {
                                PotextitemItems.TextLine = TextLine;
                            }
                            else
                            {
                                TextLine = TextLine.Substring(132 * i + (i == 0 ? 0 : 1), 132 * (i + 1));
                            }
                            PotextitemItems.TextLine = TextLine; //lenth = 132 spit;
                            listPotextitemItems.Add(PotextitemItems);
                        }
                    }
                    else
                    {
                        PotextitemItems = new Potextitem();
                        PotextitemItems.PoEcm = drItem["po_num"].ToString();
                        PotextitemItems.PoItem = drItem["po_item_no"].ToString();
                        PotextitemItems.TextId = "F03";
                        PotextitemItems.TextForm = "*";
                        PotextitemItems.TextLine = TextLine; //lenth = 132 spit;
                        listPotextitemItems.Add(PotextitemItems);
                    }
                }

                string poNumber = genXMLRequestPO(item.po_num);

                //ps_t_contract_po contract_po = new ps_t_contract_po();
                //List<ps_t_contract_po> list_contract_po = new List<ps_t_contract_po>();
                //if (poNumber != "")
                //{
                //    foreach (DataRow dr in dt.Rows)
                //    {
                //        contract_po.Contract_No = dr["Contno"].ToString();
                //        contract_po.po_no = poNumber;
                //        contract_po.po_item = dr["PoItem"].ToString();
                //        contract_po.bid_no = dr["bid_no"].ToString();
                //        contract_po.bid_rev = item.bid_revision;
                //        contract_po.schedule_name = dr["schedule_name"].ToString();
                //        contract_po.part_code = dr["Part"].ToString();
                //        contract_po.item_no = dr["Itemno"].ToString();
                //        contract_po.equip_code = dr["Material"].ToString();
                //        contract_po.qty = Convert.ToInt32(dr["NetPrice"].ToString());
                //        insert_ps_t_contract_po();
                //    }

                //}
            }


        }
        private static DataTable RemoveDuplicateRows(DataTable dTable, string colName)
        {
            Hashtable hTable = new Hashtable();
            ArrayList duplicateList = new ArrayList();

            //Add list of all the unique item value to hashtable, which stores combination of key, value pair.
            //And add duplicate item value in arraylist.
            foreach (DataRow drow in dTable.Rows)
            {
                if (hTable.Contains(drow[colName]))
                    duplicateList.Add(drow);
                else
                    hTable.Add(drow[colName], string.Empty);
            }

            //Removing a list of duplicate items from datatable.
            foreach (DataRow dRow in duplicateList)
                dTable.Rows.Remove(dRow);

            //Datatable which contains unique records will be return as output.
            return dTable;
        }
        public static string SendToSapPO6Digit(string DocDate, string batch_no)
        {
            string Digit = "";
            // DataTable dtCNO = new DataTable();
            DataTable dt = new DataTable();
            Batch_no = batch_no;
            //dt = getDataPOToSap(cNO);
            //dt = getDataStockToSap();
            // genXMLRequestStock(dt);

            Digit = genXMLRequestPO6Digit(DocDate);


            return Digit;
            //createRequestPO();
        }


        private static string getFormatDate(DateTime date, string format)
        {
            string strDate = "";

            string strMonth = date.Month.ToString().Length == 1 ? ("0" + date.Month) : date.Month.ToString();
            string strDay = date.Day.ToString().Length == 1 ? ("0" + date.Day) : date.Day.ToString();
            if (format == "YYYY-MM-DD")
            {
                strDate = date.Year + "-" + strMonth + "-" + strDay;


            }



            return strDate;


        }


        //PO
        #region PO


        public static List<int> insert_ps_t_contract_po(ps_t_contract_po contract_po)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_up_excel_transection (";
                cmdstr = cmdstr + getNameForInsert(contract_po) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(contract_po) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }

        public static List<int> insert_ps_t_bid_selling_cmpo_item(ps_t_bid_selling_cmpo_item cmpo_item)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_t_bid_selling_cmpo_item (";
                cmdstr = cmdstr + getNameForInsert(cmpo_item) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(cmpo_item) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }




        public static string getNameForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;
            PropertyInfo[] properties = _class.GetType().GetProperties();

            foreach (PropertyInfo property in properties)
            {
                if (!property.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {
                        value = property.Name;
                    }
                    else
                    {
                        value = property.Name + ",";
                    }
                    result = result + value;
                }
                count++;

            }
            return result;
        }

        public static string cutLastString(string item)
        {
            string result = "";
            result = item.Substring(0, item.Length - 1);
            return result;
        }
        public static string getValueForInsert(object _class)
        {
            string result = "";
            bool chkLast = false;
            var value = "";
            int count = 1;

            PropertyInfo[] properties = _class.GetType().GetProperties();


            foreach (var p in properties)
            {
                if (!p.Name.Equals("rowid"))
                {
                    if (count == properties.Length)
                    {
                        chkLast = true;
                    }
                    if (chkLast)
                    {

                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {
                            value = "GETDATE()" + "";
                        }
                        else
                        {
                            value = p.GetValue(_class) == null ? "" : "'" + p.GetValue(_class).ToString().Replace("'", "''") + "'";

                        }


                    }
                    else
                    {
                        if (p.Name.Contains("date") && !p.Name.Contains("update") || p.Name.Contains("updated_datetime"))
                        {

                            value = "GETDATE()" + ",";

                        }
                        else
                        {
                            value = p.GetValue(_class) == null ? "null," : "'" + p.GetValue(_class).ToString().Replace("'", "''") + "'" + ",";
                        }

                    }
                    result = result + value;
                }
                count++;
            }


            return result;
        }


        public static DataTable getContractNo()
        {

            string sqlFull = "";
            string sql = @"SELECT [contract_no],[status] FROM [dbo].[ps_t_contract_no] ";
            string condi = " where UPPER([status]) = UPPER('new')";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();


            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }

        public static DataTable get_ps_m_equipcode(string equip_code)
        {
            string sqlFull = "";
            string sql = @" select * from ps_m_equipcode where equip_code = '" + equip_code + "'";
            string order = " ";
            string condi = "";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;
        }

        public static DataTable get_equip_code(string itemNo)
        {

            string sqlFull = "";
            string sql = @"  select top 1 a.equip_code,* from ps_xls_template a left join  ps_up_excel_transection b
on a.schedule_name = b.schedule_name and a.bid_no = b.bid_no and a.bid_revision = b.bid_revision
and a.item_no = b.item_no and a.part = b.part and a.template_id = b.template_id 
where a.item_no = '" + itemNo + "'";
            string order = " ";
            string condi = "";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }



        public static DataTable getDataPOToSap(string cNo)
        {

            string sqlFull = "";
            string sql = @"select * from ps_t_bid_selling_cmpo_item where vendor_code in (select vendor_code from ps_t_bid_selling_cmpo where contract_no = '" + cNo + "' and vendor_type <> 'Consortium') and vendor_code <> '1234567890' ";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }
        public static DataTable getData_bid_selling_cmpo(string vendor_code, string bid_no, string bid_revision, string schedule_name)
        {

            string sqlFull = "";
            string sql = @"select * from ps_t_bid_selling_cmpo where vendor_code = '" + vendor_code +
                        "' and vendor_type <> 'Consortium' and vendor_code <> '1234567890' and schedule_name = '" + schedule_name + "'" +
                        "and bid_no = '" + bid_no + "' and bid_revision ='" + bid_revision + "'";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }
        public static DataTable getDataPO(string cNo)
        {

            string sqlFull = "";
            string sql = @"select distinct * from  ps_up_excel_transection a where vendor_code in (select vendor_code from ps_t_bid_selling_cmpo where contract_no = '" + cNo + "' and vendor_type <> 'Consortium') ";
            string order = " ";
            bool chk = true;
            DataTable dtList = new DataTable();

            // condi = "";
            if (!chk)
            {
                sqlFull = sql + order;
            }
            else
            {
                sqlFull = sql + condi + order;
            }

            try
            {
                dtList = zdbUtil.ExecSql_DataTable(sqlFull, psDB);
            }
            catch (Exception ex)
            {

            }
            return dtList;


        }

        public static string getPOID()
        {

            string pid = "";
            string result = UpdatePOID();
            // string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            DataTable dtRunno = new DataTable();

            try
            {
                if (result.Equals("success"))
                {
                    cmdstrFull = "";
                    cmdstr = @"select runno from wf_runno ";
                    condi = " WHERE prefix = 'PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + condi;

                    dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);
                    if (checkRowData(dtRunno))
                    {
                        pid = dtRunno.Rows[0][0].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                // result = ex.Message;
            }

            return pid;
        }
        public static string getYearThai()
        {
            string yearThai = "";



            string strdate = DateTime.Now.Year.ToString();

            yearThai = (Convert.ToInt32(strdate) + 543).ToString();
            return yearThai;
        }
        public static string UpdatePOID()
        {
            string result = "success";
            string yearThai = "";
            yearThai = getYearThai();
            int runno = 0;
            int runnoNew = 0;
            DataTable dtRunno = new DataTable();
            try
            {
                cmdstrFull = "";
                cmdstr = @"select runno from wf_runno ";
                condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                order = " ";
                cmdstrFull = cmdstr + condi;

                dtRunno = zdbUtil.ExecSql_DataTable(cmdstrFull, psDB);

                if (checkRowData(dtRunno))
                {
                    runno = Convert.ToInt32(dtRunno.Rows[0][0].ToString());
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"update wf_runno ";
                    cmdvalue = " set runno = " + runnoNew;
                    condi = " WHERE prefix ='PO' and year_thai = '" + yearThai + "' ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue + condi;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }
                else
                {
                    runno = 0;
                    runnoNew = runno + 1;

                    cmdstrFull = "";
                    cmdstr = @"insert into wf_runno ( prefix, runno,year_thai)";
                    cmdvalue = "VALUES  ( select 'PO', '" + runnoNew + "' , '" + yearThai + "' )";
                    condi = " ";
                    order = " ";
                    cmdstrFull = cmdstr + cmdvalue;

                    zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                }


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static string update_ps_t_contract_no(string status, string res, string cNo)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_t_contract_no ";
                cmdstr = cmdstr + "Set status = '";
                cmdvalue = status + "' ," + " result_sap = '" + res + "'";
                condi = "where contract_no = '" + cNo + "' and status = 'New'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }

        public static string update_ps_t_bid_selling_cmpo_item(string ponum, string sap_ponum)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            // res = "error";
            try
            {

                cmdstrFull = "";

                cmdstr = @"update ps_t_bid_selling_cmpo_item ";
                cmdstr = cmdstr + "Set sap_po_num = '";
                cmdvalue = sap_ponum + "'";
                condi = "where po_num = '" + ponum + "'";

                cmdstrFull = cmdstr + cmdvalue + condi;

                zdbUtil.ExecNonQuery(cmdstrFull, psDB);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return result;
        }


        public static bool checkRowData(DataTable dt)
        {
            bool check = false;
            if (dt.Rows.Count > 0)
            {
                check = true;
            }
            else
            {
                check = false;
            }
            return check;
        }
        public static void createRequestPO(DataTable dt)
        {
            SAP_PO.ZbapiEcmPo objPO_INput = new SAP_PO.ZbapiEcmPo();

            objPO_INput.Mode = "01";

            #region Poaccount
            List<Poaccount> listPoaccount = new List<Poaccount>();
            listPoaccount = addPoaccount(dt);
            int i = 0;

            foreach (var item in listPoaccount)
            {
                objPO_INput.Poaccount = addDataPoaccount(item, i, listPoaccount.Count);
                i++;
            }
            #endregion

            //#region Pocond
            //Pocond Pocond = new Pocond();
            //SAP_PO.Zecmpocond Zecmpocond = new SAP_PO.Zecmpocond();
            //List<Pocond> listPocond = new List<Pocond>();
            //listPocond = addPocond(dt);
            //i = 0;

            //foreach (var item in listPocond)
            //{
            //    objPO_INput.Pocond = addDataPocond(item, i, listPocond.Count);
            //    i++;
            //}
            //#endregion

            #region Poheader
            Poheader Poheader = new Poheader();
            SAP_PO.Zecmpoheader Zecmpoheader = new SAP_PO.Zecmpoheader();
            List<Poheader> listPoheader = new List<Poheader>();
            listPoheader = addPoheader(dt);
            i = 0;

            foreach (var item in listPoheader)
            {
                objPO_INput.Poheader = addDataPoheader(item, i, listPoheader.Count);
                i++;
            }
            #endregion

            #region Poitem
            Poitem Poitem = new Poitem();
            SAP_PO.Zecmpoitem Zecmpoitem = new SAP_PO.Zecmpoitem();
            List<Poitem> listPoitem = new List<Poitem>();
            listPoitem = addPoitem(dt);
            i = 0;

            foreach (var item in listPoitem)
            {
                objPO_INput.Poitem = addDataPoitem(item, i, listPoitem.Count);
                i++;
            }
            #endregion

            #region Poschedule
            Poschedule Poschedule = new Poschedule();
            SAP_PO.Zecmposchedule Zecmposchedule = new SAP_PO.Zecmposchedule();
            List<Poschedule> listPoschedule = new List<Poschedule>();
            listPoschedule = addPoschedule(dt);
            i = 0;

            foreach (var item in listPoschedule)
            {
                objPO_INput.Poschedule = addDataPoschedule(item, i, listPoschedule.Count);
                i++;
            }
            #endregion

            #region Potextitem
            Potextitem Potextitem = new Potextitem();
            SAP_PO.Zecmpotextitem Zecmpotextitem = new SAP_PO.Zecmpotextitem();
            List<Potextitem> listPotextitem = new List<Potextitem>();
            listPotextitem = addPotextitem(dt);
            i = 0;

            foreach (var item in listPotextitem)
            {
                objPO_INput.Potextitem = addDataPotextitem(item, i, listPotextitem.Count);
                i++;
            }
            #endregion



            //CreatePO(objPO_INput);
            //[v_send_sap]

        }

        public static List<Poaccount> addPoaccount(DataTable dt)
        {
            List<Poaccount> listPoaccountItems = new List<Poaccount>();
            Poaccount poaccountItems = new Poaccount();

            //dt = new DataTable();
            //dt.Columns.Add("PoEcm", typeof(System.String));
            //dt.Columns.Add("PoItem", typeof(System.String));
            //dt.Columns.Add("WbsElement", typeof(System.String));
            //dt.Columns.Add("FundsCtr", typeof(System.String));
            //// dt.Columns.Add("Quantity", typeof(System.Decimal));

            //DataRow drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00010";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00020";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00030";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00040";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00050";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00060";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00070";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00080";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00090";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000101";
            //drr["PoItem"] = "00020";
            //drr["WbsElement"] = "L-TS12-14-S02-40000";
            //drr["FundsCtr"] = "N333000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);



            foreach (DataRow dr in dt.Rows)
            {
                poaccountItems = new Poaccount();
                poaccountItems.PoEcm = dr["PoEcm"].ToString();
                poaccountItems.PoItem = dr["PoItem"].ToString();
                //poaccountItems.SerialNo = "01";
                //poaccountItems.GlAccount = "5704000";
                poaccountItems.WbsElement = dr["WbsElement"].ToString();
                poaccountItems.FundsCtr = dr["FundsCtr"].ToString();
                // poaccountItems.Quantity = dr["Quantity"] == null ? 0 : dr["Quantity"].ToString() == "" ? 0 : Convert.ToDecimal(dr["Quantity"].ToString());

                listPoaccountItems.Add(poaccountItems);
            }


            return listPoaccountItems;
        }

        public static List<Pocond> addPocond(DataTable dt)
        {
            List<Pocond> listPocondItems = new List<Pocond>();
            Pocond PocondItems = new Pocond();
            foreach (DataRow dr in dt.Rows)
            {
                PocondItems = new Pocond();
                //PocondItems.PoEcm = "PO0000000199";
                //PocondItems.ItmNumber = "00010";
                //PocondItems.CondStNo = "1";
                //PocondItems.CondType = "ZOA1";
                //PocondItems.CondValue = 2;
                //PocondItems.ChangeId = "I";

                listPocondItems.Add(PocondItems);
            }
            return listPocondItems;
        }

        public static List<Poheader> addPoheader(DataTable dt)
        {
            List<Poheader> listPoheaderItems = new List<Poheader>();
            Poheader PoheaderItems = new Poheader();



            DataView dv = dt.DefaultView;
            dv.Sort = "PoEcm desc";
            DataTable sortedDT = dv.ToTable();
            string PoEcm = "";
            int count = 0;

            int x = 0;

            foreach (DataRow dr in sortedDT.Rows)
            {

                PoheaderItems.PoEcm = dr["PoEcm"].ToString();
                PoheaderItems.CompCode = dr["CompCode"].ToString();
                PoheaderItems.DocType = dr["DocType"].ToString();
                PoheaderItems.Vendor = dr["Vendor"].ToString();
                PoheaderItems.PurchOrg = dr["PurchOrg"].ToString();
                PoheaderItems.PurGroup = dr["PurGroup"].ToString();
                PoheaderItems.Currency = dr["Currency"].ToString();
                PoheaderItems.ExchRate = dr["ExchRate"] == null ? 0 : dr["ExchRate"].ToString() == "" ? 0 : Convert.ToDecimal(dr["ExchRate"].ToString());
                PoheaderItems.DocDate = dr["DocDate"].ToString();
                PoheaderItems.QuotDate = dr["QuotDate"].ToString();
                PoheaderItems.Methcode = dr["Methcode"].ToString();
                PoheaderItems.Contno = dr["Contno"].ToString();
                //}
                //}

                listPoheaderItems.Add(PoheaderItems);
                //x = 1;

            }
            var distinct = listPoheaderItems.GroupBy(i => i.PoEcm, (key, group) => group.First());
            return distinct.ToList();
        }

        public static List<Poheader> addPoheade6ditgitr(string DocDate)
        {
            List<Poheader> listPoheaderItems = new List<Poheader>();
            Poheader PoheaderItems = new Poheader();



            //DataView dv = dt.DefaultView;
            //dv.Sort = "PoEcm desc";
            //DataTable sortedDT = dv.ToTable();
            //string PoEcm = "";
            int count = 0;

            int x = 0;

            //foreach (DataRow dr in sortedDT.Rows)
            //{

            // PoheaderItems.PoEcm = PoEcm;
            PoheaderItems.CompCode = "1000";
            PoheaderItems.DocType = " ZNBR";
            //PoheaderItems.PurchOrg = PurchOrg;
            // PoheaderItems.PurGroup = PurGroup;
            PoheaderItems.DocDate = DocDate;
            //}
            //}

            listPoheaderItems.Add(PoheaderItems);
            //x = 1;

            //}
            // var distinct = listPoheaderItems.GroupBy(i => i.PoEcm, (key, group) => group.First());
            return listPoheaderItems;
        }

        public static List<Poitem> addPoitem(DataTable dt)
        {
            List<Poitem> listPoitemItems = new List<Poitem>();
            Poitem PoitemItems = new Poitem();

            //dt = new DataTable();
            //dt.Columns.Add("PoItem", typeof(System.String));
            //dt.Columns.Add("DeleteInd", typeof(System.String));
            //dt.Columns.Add("ShortText", typeof(System.String));
            //dt.Columns.Add("Material", typeof(System.String));
            //dt.Columns.Add("Plant", typeof(System.String));
            //dt.Columns.Add("StgeLoc", typeof(System.String));
            //dt.Columns.Add("Trackingno", typeof(System.String));
            //dt.Columns.Add("MatlGroup", typeof(System.String));
            //dt.Columns.Add("Quantity", typeof(System.Decimal));
            //dt.Columns.Add("PoUnit", typeof(System.String));
            //dt.Columns.Add("NetPrice", typeof(System.String));
            //dt.Columns.Add("PriceUnit", typeof(System.Decimal));
            //dt.Columns.Add("InfoUpd", typeof(System.String));
            //dt.Columns.Add("ItemCat", typeof(System.String));
            //dt.Columns.Add("Acctasscat", typeof(System.String));
            //dt.Columns.Add("GrInd", typeof(System.String));
            //dt.Columns.Add("IrInd", typeof(System.String));
            //dt.Columns.Add("GrBasediv", typeof(System.String));
            //dt.Columns.Add("AcknReqd", typeof(System.String));
            //dt.Columns.Add("AcknowlNo", typeof(System.String));
            //dt.Columns.Add("CtrlKey", typeof(System.String));
            //dt.Columns.Add("ConfCtrl", typeof(System.String));
            //dt.Columns.Add("Incoterms1", typeof(System.String));
            //dt.Columns.Add("VendPart", typeof(System.String));
            //dt.Columns.Add("PreqName", typeof(System.String));
            //dt.Columns.Add("Vendrbatch", typeof(System.String));
            //dt.Columns.Add("Itemno", typeof(System.String));
            //dt.Columns.Add("PoEcm", typeof(System.String));
            //// dt.Columns.Add("Quantity", typeof(System.Decimal));



            //DataRow drr = dt.NewRow();
            //drr["PoItem"] = "10";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "200MVA 230/121-22kV 3ph TX8612";
            //drr["Material"] = "TX8612";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "2000";
            //drr["MatlGroup"] = "253";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "38925000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "TX8612";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-1";
            //drr["Itemno"] = "1-1";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00020";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "HV Bushing for 200 MVA (230 kV)";
            //drr["Material"] = "TXBH861X";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "800000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "TXBH861X";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.1A";
            //drr["Itemno"] = "1-2.1A";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00030";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "LV Bushing for 200 MVA  (121 kV)";
            //drr["Material"] = "TXBL861X-7";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "450000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "TXBL861X-7";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.1B";
            //drr["Itemno"] = "1-2.1B";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00040";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "TV Bushing for 200 MVA (22 kV)";
            //drr["Material"] = "TXBT861X-2";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "40000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "TXBT861X-2";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.1C";
            //drr["Itemno"] = "1-2.1C";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00050";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "Neutral Bushing for 200 MVA (150 kV BIL)";
            //drr["Material"] = "TXBT861X-2";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "40000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "TXBN861X-2";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.1D";
            //drr["Itemno"] = "1-2.1D";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);


            //drr = dt.NewRow();
            //drr["PoItem"] = "00060";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "192kV Arrester SA8Y11";
            //drr["Material"] = "SA8Y11";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "120000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "SA8Y11";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.2A";
            //drr["Itemno"] = "1-2.2A";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00070";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "108kV Arrester SA7Y11";
            //drr["Material"] = "SA7Y11";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "85000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "SA7Y11";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.2B";
            //drr["Itemno"] = "1-2.2B";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00080";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "24kV Arrester SA2D01";
            //drr["Material"] = "SA2D01";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "60000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "SA2D01";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.2C";
            //drr["Itemno"] = "1-2.2C";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00090";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "Complete Set of Spare Relay for One TX";
            //drr["Material"] = "TXRL861X";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "EA";
            //drr["NetPrice"] = "60000";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "X";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "Q";
            //drr["GrInd"] = "X";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "TXRL861X";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "EXW";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "224669";
            //drr["Vendrbatch"] = "1-2.3";
            //drr["Itemno"] = "1-2.3";
            //drr["PoEcm"] = "PO0000000100";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoItem"] = "00010";
            //drr["DeleteInd"] = "";
            //drr["ShortText"] = "Cost of installation supervisor for Item";
            //drr["Material"] = "";
            //drr["Plant"] = "HQ31";
            //drr["StgeLoc"] = "";
            //drr["Trackingno"] = "";
            //drr["MatlGroup"] = "SV-MNT-EQ";
            //drr["Quantity"] = 1.000;
            //drr["PoUnit"] = "MDA";
            //drr["NetPrice"] = "7800";
            //drr["PriceUnit"] = 1;
            //drr["InfoUpd"] = "";
            //drr["ItemCat"] = "";
            //drr["Acctasscat"] = "N";
            //drr["GrInd"] = "";
            //drr["IrInd"] = "X";
            //drr["GrBasediv"] = "X";
            //drr["AcknReqd"] = "";
            //drr["AcknowlNo"] = "";
            //drr["CtrlKey"] = "";
            //drr["ConfCtrl"] = "";
            //drr["Incoterms1"] = "ZTC";
            //drr["VendPart"] = "1";
            //drr["PreqName"] = "381357";
            //drr["Vendrbatch"] = "1-3";
            //drr["Itemno"] = "1-3";
            //drr["PoEcm"] = "PO0000000101";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);


            foreach (DataRow dr in dt.Rows)
            {
                PoitemItems = new Poitem();

                PoitemItems.PoItem = dr["PoItem"].ToString();
                PoitemItems.Material = dr["Material"].ToString();
                PoitemItems.Plant = dr["Plant"].ToString();
                PoitemItems.Quantity = dr["Quantity"] == null ? 0 : dr["Quantity"].ToString() == "" ? 0 : Convert.ToDecimal(dr["Quantity"].ToString());
                PoitemItems.PoUnit = dr["PoUnit"].ToString();
                PoitemItems.NetPrice = dr["NetPrice"] == null ? 0 : dr["NetPrice"].ToString() == "" ? 0 : Convert.ToDecimal(dr["NetPrice"].ToString());
                PoitemItems.PriceUnit = dr["PriceUnit"] == null ? 0 : dr["PriceUnit"].ToString() == "" ? 0 : Convert.ToDecimal(dr["PriceUnit"].ToString());
                PoitemItems.InfoUpd = dr["InfoUpd"].ToString();
                PoitemItems.Acctasscat = dr["Acctasscat"].ToString();
                PoitemItems.GrInd = dr["GrInd"].ToString();
                PoitemItems.IrInd = dr["IrInd"].ToString();
                PoitemItems.GrBasediv = dr["GrBasediv"].ToString();
                PoitemItems.AcknowlNo = dr["AcknowlNo"].ToString();
                PoitemItems.Incoterms1 = dr["Incoterms1"].ToString();
                PoitemItems.VendPart = dr["VendPart"].ToString();
                PoitemItems.PreqName = dr["PreqName"].ToString();
                PoitemItems.Vendrbatch = dr["Vendrbatch"].ToString();
                PoitemItems.Itemno = dr["Itemno"].ToString();
                PoitemItems.PoEcm = dr["PoEcm"].ToString();

                listPoitemItems.Add(PoitemItems);
            }


            return listPoitemItems;
        }
        public static List<Poschedule> addPoschedule(DataTable dt)
        {
            List<Poschedule> listPoscheduleItems = new List<Poschedule>();
            Poschedule PoscheduleItems = new Poschedule();

            //dt = new DataTable();
            //dt.Columns.Add("PoEcm", typeof(System.String));
            //dt.Columns.Add("PoItem", typeof(System.String));
            //dt.Columns.Add("SchedLine", typeof(System.String));
            //dt.Columns.Add("DeliveryDate", typeof(System.String));


            //DataRow drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00010";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00020";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00030";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00040";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00050";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00060";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00070";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00080";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoItem"] = "00090";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000101";
            //drr["PoItem"] = "00010";
            //drr["SchedLine"] = "";
            //drr["DeliveryDate"] = "2021-11-08";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            foreach (DataRow dr in dt.Rows)
            {
                PoscheduleItems = new Poschedule();

                PoscheduleItems.PoEcm = dr["PoEcm"].ToString();
                PoscheduleItems.PoItem = dr["PoItem"].ToString();
                PoscheduleItems.DeliveryDate = dr["DeliveryDate"].ToString().Substring(0, 10);
                listPoscheduleItems.Add(PoscheduleItems);
            }
            return listPoscheduleItems;
        }
        public static List<Potextitem> addPotextitem(DataTable dt)
        {
            List<Potextitem> listPotextitemItems = new List<Potextitem>();
            Potextitem PotextitemItems = new Potextitem();
            dt = new DataTable();
            dt.Columns.Add("PoEcm", typeof(System.String));
            dt.Columns.Add("PoNumber", typeof(System.String));
            dt.Columns.Add("PoItem", typeof(System.String));
            dt.Columns.Add("TextId", typeof(System.String));
            dt.Columns.Add("TextForm", typeof(System.String));
            dt.Columns.Add("TextLine", typeof(System.String));


            //DataRow drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "200 MVA, 230/121-22kV autotransformer completed with tank mounted surge arrester,";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "";
            //drr["TextLine"] = "insulating oil and accessories as per Ratings and Features RF TX8612";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "38925000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00020";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "HV Bushing for 200 MVA (230 kV)";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00020";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "800000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00020";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00030";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "LV Bushing for 200 MVA  (121 kV)";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00030";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "450000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00030";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00040";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "TV Bushing for 200 MVA (22 kV)";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00040";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "40000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00040";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00050";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Neutral Bushing for 200 MVA (150 kV BIL)";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00050";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "40000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00050";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00060";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "192 kV Surge Arrester as per Ratings and Features RF SA8Y11";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00060";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "120000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00060";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00070";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "108 kV Surge Arrester as per Ratings and Features RF SA7Y11";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00070";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "85000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00070";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00080";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "24 kV Surge Arrester as per Ratings and Features RF SA2D01";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00080";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "60000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00080";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00090";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Complete Set of one or two units of each type and each size of auxiliary relay (two units are required)";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00090";
            //drr["TextId"] = "F03";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "where five units or more of each type and each size are provided per one transformer)";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00090";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "60000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000100";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00090";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Ex-works Price";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000101";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Cost of installation supervisor for Item No. 1-1";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000101";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F13";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "78000";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);

            //drr = dt.NewRow();
            //drr["PoEcm"] = "PO0000000101";
            //drr["PoNumber"] = "";
            //drr["PoItem"] = "00010";
            //drr["TextId"] = "F15";
            //drr["TextForm"] = "*";
            //drr["TextLine"] = "Local Currency";
            //// drr["Quantity"] = null;
            //dt.Rows.Add(drr);



            foreach (DataRow dr in dt.Rows)
            {
                PotextitemItems = new Potextitem();
                PotextitemItems.PoEcm = dr["PoEcm"].ToString();
                PotextitemItems.PoItem = dr["PoItem"].ToString();
                PotextitemItems.TextId = dr["TextId"].ToString();
                PotextitemItems.TextForm = "*";
                PotextitemItems.TextLine = dr["TextLine"].ToString();

                listPotextitemItems.Add(PotextitemItems);
            }
            return listPotextitemItems;
        }



        public static SAP_PO.Zecmpoaccount[] addDataPoaccount(Poaccount _class, int index, int count)
        {

            SAP_PO.Zecmpoaccount _sapclass = new SAP_PO.Zecmpoaccount();

            SAP_PO.Zecmpoaccount[] Input = new SAP_PO.Zecmpoaccount[count];


            _sapclass.PoEcm = _class.PoEcm;
            _sapclass.PoItem = _class.PoItem;
            _sapclass.SerialNo = _class.SerialNo;
            //_sapclass.DeleteInd = _class.DeleteInd;
            //_sapclass.CreatDate = _class.CreatDate;
            // _sapclass.Quantity = _class.Quantity;
            //_sapclass.DistrPerc = _class.DistrPerc;
            //_sapclass.NetValue = _class.NetValue;
            _sapclass.GlAccount = _class.GlAccount;
            //_sapclass.BusArea = _class.BusArea;
            _sapclass.Costcenter = _class.Costcenter;
            _sapclass.SdDoc = _class.SdDoc;
            _sapclass.ItmNumber = _class.ItmNumber;
            //_sapclass.SchedLine = _class.SchedLine;
            //_sapclass.AssetNo = _class.AssetNo;
            //_sapclass.SubNumber = _class.SubNumber;
            _sapclass.Orderid = _class.Orderid;
            _sapclass.GrRcpt = _class.GrRcpt;
            _sapclass.UnloadPt = _class.UnloadPt;
            _sapclass.CoArea = _class.CoArea;
            _sapclass.Costobject = _class.Costobject;
            _sapclass.ProfitCtr = _class.ProfitCtr;
            _sapclass.WbsElement = _class.WbsElement;
            _sapclass.Network = _class.Network;
            //_sapclass.RlEstKey = _class.RlEstKey;
            //_sapclass.PartAcct = _class.PartAcct;
            //_sapclass.CmmtItem = _class.CmmtItem;
            //_sapclass.RecInd = _class.RecInd;
            _sapclass.FundsCtr = _class.FundsCtr;
            //_sapclass.Fund = _class.Fund;
            //_sapclass.FuncArea = _class.FuncArea;
            //_sapclass.RefDate = _class.RefDate;
            //_sapclass.TaxCode = _class.TaxCode;
            //_sapclass.Taxjurcode = _class.Taxjurcode;
            //_sapclass.NondItax = _class.NondItax;
            //_sapclass.Acttype = _class.Acttype;
            //_sapclass.CoBusproc = _class.CoBusproc;
            //_sapclass.ResDoc = _class.ResDoc;
            //_sapclass.ResItem = _class.ResItem;
            _sapclass.Activity = _class.Activity;
            //_sapclass.GrantNbr = _class.GrantNbr;
            //_sapclass.CmmtItemLong = _class.CmmtItemLong;
            //_sapclass.FuncAreaLong = _class.FuncAreaLong;

            Input[index] = _sapclass;


            return Input;

        }

        public static SAP_PO.Zecmpocond[] addDataPocond(Pocond _class, int index, int count)
        {

            SAP_PO.Zecmpocond _sapclass = new SAP_PO.Zecmpocond();

            SAP_PO.Zecmpocond[] Input = new SAP_PO.Zecmpocond[count];

            _sapclass.PoEcm = _class.PoEcm;
            _sapclass.ConditionNo = _class.ConditionNo;
            _sapclass.ItmNumber = _class.ItmNumber;
            _sapclass.CondStNo = _class.CondStNo;
            _sapclass.CondCount = _class.CondCount;
            _sapclass.CondType = _class.CondType;
            _sapclass.CondValue = _class.CondValue;
            _sapclass.Currency = _class.Currency;
            _sapclass.CurrencyIso = _class.CurrencyIso;
            _sapclass.CondUnit = _class.CondUnit;
            _sapclass.CondUnitIso = _class.CondUnitIso;
            _sapclass.CondPUnt = _class.CondPUnt;
            _sapclass.ItmNumber = _class.ItmNumber;
            _sapclass.Applicatio = _class.Applicatio;
            _sapclass.Conpricdat = _class.Conpricdat;
            _sapclass.Calctypcon = _class.Calctypcon;
            _sapclass.Conbaseval = _class.Conbaseval;
            _sapclass.Numconvert = _class.Numconvert;
            _sapclass.Denominato = _class.Denominato;
            _sapclass.Condtype = _class.Condtype;
            _sapclass.StatCon = _class.StatCon;
            _sapclass.Scaletype = _class.Scaletype;
            _sapclass.Accruals = _class.Accruals;
            _sapclass.Coninvolst = _class.Coninvolst;
            _sapclass.Condorigin = _class.Condorigin;
            _sapclass.Groupcond = _class.Groupcond;
            _sapclass.CondUpdat = _class.CondUpdat;
            _sapclass.AccessSeq = _class.AccessSeq;
            _sapclass.Condcount = _class.Condcount;
            _sapclass.Condcntrl = _class.Condcntrl;
            _sapclass.Condisacti = _class.Condisacti;
            _sapclass.Condclass = _class.Condclass;
            _sapclass.Factbasval = _class.Factbasval;
            _sapclass.Scalebasin = _class.Scalebasin;
            _sapclass.Scalbasval = _class.Scalbasval;
            _sapclass.Unitmeasur = _class.Unitmeasur;
            _sapclass.UnitmeasurIso = _class.UnitmeasurIso;
            _sapclass.Currenckey = _class.Currenckey;
            _sapclass.CurrenckeyIso = _class.CurrenckeyIso;
            _sapclass.Condconfig = _class.Condconfig;
            _sapclass.Condincomp = _class.Condincomp;
            _sapclass.Condchaman = _class.Condchaman;
            _sapclass.CondNo = _class.CondNo;
            _sapclass.ChangeId = _class.ChangeId;
            _sapclass.VendorNo = _class.VendorNo;

            Input[index] = _sapclass;

            return Input;

        }

        public static SAP_PO.Zecmpoheader[] addDataPoheader(Poheader _class, int index, int count)
        {

            SAP_PO.Zecmpoheader _sapclass = new SAP_PO.Zecmpoheader();

            SAP_PO.Zecmpoheader[] Input = new SAP_PO.Zecmpoheader[count];

            _sapclass.PoEcm = _class.PoEcm;
            _sapclass.PoNumber = _class.PoNumber;
            _sapclass.CompCode = _class.CompCode;
            _sapclass.DocType = _class.DocType;
            //_sapclass.DeleteInd = _class.DeleteInd;
            //_sapclass.Status = _class.Status;
            //_sapclass.CreatDate = _class.CreatDate;
            _sapclass.Currency = _class.Currency;
            //_sapclass.CurrencyIso = _class.CurrencyIso;
            //_sapclass.CreatedBy = _class.CreatedBy;
            //_sapclass.ItemIntvl = _class.ItemIntvl;
            _sapclass.Vendor = _class.Vendor;
            _sapclass.Langu = _class.Langu;
            //_sapclass.LanguIso = _class.LanguIso;
            //_sapclass.Pmnttrms = _class.Pmnttrms;
            //_sapclass.Dscnt1To = _class.Dscnt1To;
            //_sapclass.Dscnt2To = _class.Dscnt2To;
            //_sapclass.Dscnt3To = _class.Dscnt3To;
            //_sapclass.DsctPct1 = _class.DsctPct1;
            //_sapclass.DsctPct2 = _class.DsctPct2;
            _sapclass.PurchOrg = _class.PurchOrg;
            _sapclass.PurGroup = _class.PurGroup;
            _sapclass.Currency = _class.Currency;
            //_sapclass.CurrencyIso = _class.CurrencyIso;
            _sapclass.ExchRate = _class.ExchRate;
            //_sapclass.ExRateFx = _class.ExRateFx;
            _sapclass.DocDate = _class.DocDate;
            //_sapclass.VperStart = _class.VperStart;
            //_sapclass.VperEnd = _class.VperEnd;
            _sapclass.Warranty = _class.Warranty;
            //_sapclass.Quotation = _class.Quotation;
            _sapclass.QuotDate = _class.QuotDate;
            //_sapclass.Ref1 = _class.Ref1;
            //_sapclass.SalesPers = _class.SalesPers;
            //_sapclass.Telephone = _class.Telephone;
            //_sapclass.SupplVend = _class.SupplVend;
            //_sapclass.Customer = _class.Customer;
            //_sapclass.Agreement = _class.Agreement;
            //_sapclass.GrMessage = _class.GrMessage;
            //_sapclass.Incoterms1 = _class.Incoterms1;
            //_sapclass.Incoterms2 = _class.Incoterms2;
            //_sapclass.CollectNo = _class.CollectNo;
            //_sapclass.DiffInv = _class.DiffInv;
            //_sapclass.OurRef = _class.OurRef;
            //_sapclass.Subitemint = _class.Subitemint;
            //_sapclass.PoRelInd = _class.PoRelInd;
            //_sapclass.RelStatus = _class.RelStatus;
            //_sapclass.VatCntry = _class.VatCntry;
            //_sapclass.VatCntryIso = _class.VatCntryIso;
            //_sapclass.ReasonCancel = _class.ReasonCancel;
            //_sapclass.ReasonCode = _class.ReasonCode;
            _sapclass.Methcode = _class.Methcode;
            _sapclass.Contno = _class.Contno;


            Input[index] = _sapclass;

            return Input;

        }

        public static SAP_PO.Zecmpoitem[] addDataPoitem(Poitem _class, int index, int count)
        {

            SAP_PO.Zecmpoitem _sapclass = new SAP_PO.Zecmpoitem();

            SAP_PO.Zecmpoitem[] Input = new SAP_PO.Zecmpoitem[count];

            _sapclass.PoItem = _class.PoItem;
            _sapclass.DeleteInd = _class.DeleteInd;
            _sapclass.ShortText = _class.ShortText;
            _sapclass.Material = _class.Material;
            _sapclass.DeleteInd = _class.DeleteInd;
            //_sapclass.MaterialExternal = _class.MaterialExternal;
            //_sapclass.MaterialGuid = _class.MaterialGuid;
            //_sapclass.MaterialVersion = _class.MaterialVersion;
            //_sapclass.Ematerial = _class.Ematerial;
            //_sapclass.EmaterialExternal = _class.EmaterialExternal;
            //_sapclass.EmaterialGuid = _class.EmaterialGuid;
            //_sapclass.EmaterialVersion = _class.EmaterialVersion;
            _sapclass.Plant = _class.Plant;
            _sapclass.StgeLoc = _class.StgeLoc;
            _sapclass.Trackingno = _class.Trackingno;
            _sapclass.MatlGroup = _class.MatlGroup;
            //_sapclass.InfoRec = _class.InfoRec;
            //_sapclass.VendMat = _class.VendMat;
            _sapclass.Quantity = _class.Quantity;
            _sapclass.PoUnit = _class.PoUnit;
            //_sapclass.PoUnitIso = _class.PoUnitIso;
            //_sapclass.OrderprUn = _class.OrderprUn;
            //_sapclass.OrderprUnIso = _class.OrderprUnIso;
            //_sapclass.ConvNum1 = _class.ConvNum1;
            //_sapclass.ConvDen1 = _class.ConvDen1;
            //_sapclass.NetPrice = _class.NetPrice;
            //_sapclass.PriceUnit = _class.PriceUnit;
            //_sapclass.GrPrTime = _class.GrPrTime;
            //_sapclass.TaxCode = _class.TaxCode;
            //_sapclass.BonGrp1 = _class.BonGrp1;
            //_sapclass.QualInsp = _class.QualInsp;
            _sapclass.InfoUpd = _class.InfoUpd;
            //_sapclass.PrntPrice = _class.PrntPrice;
            //_sapclass.EstPrice = _class.EstPrice;
            //_sapclass.Reminder1 = _class.Reminder1;
            //_sapclass.Reminder2 = _class.Reminder2;
            //_sapclass.Reminder3 = _class.Reminder3;
            //_sapclass.OverDlvTol = _class.OverDlvTol;
            //_sapclass.UnlimitedDlv = _class.UnlimitedDlv;
            //_sapclass.UnderDlvTol = _class.UnderDlvTol;
            //_sapclass.ValType = _class.ValType;
            //_sapclass.NoMoreGr = _class.NoMoreGr;
            //_sapclass.FinalInv = _class.FinalInv;
            //_sapclass.ItemCat = _class.ItemCat;
            //_sapclass.Acctasscat = _class.Acctasscat;
            //_sapclass.Distrib = _class.Distrib;
            //_sapclass.PartInv = _class.PartInv;
            _sapclass.GrInd = _class.GrInd;
            // _sapclass.GrNonVal = _class.GrNonVal;
            _sapclass.IrInd = _class.IrInd;
            // _sapclass.FreeItem = _class.FreeItem;
            _sapclass.GrBasediv = _class.GrBasediv;
            _sapclass.AcknReqd = _class.AcknReqd;
            _sapclass.AcknowlNo = _class.AcknowlNo;
            //_sapclass.Agreement = _class.Agreement;
            //_sapclass.AgmtItem = _class.AgmtItem;
            //_sapclass.Shipping = _class.Shipping;
            //_sapclass.Customer = _class.Customer;
            //_sapclass.CondGroup = _class.CondGroup;
            //_sapclass.NoDisct = _class.NoDisct;
            //_sapclass.PlanDel = _class.PlanDel;
            //_sapclass.NetWeight = _class.NetWeight;
            //_sapclass.Weightunit = _class.Weightunit;
            //_sapclass.WeightunitIso = _class.WeightunitIso;
            //_sapclass.Taxjurcode = _class.Taxjurcode;
            _sapclass.CtrlKey = _class.CtrlKey;
            _sapclass.ConfCtrl = _class.ConfCtrl;
            //_sapclass.RevLev = _class.RevLev;
            //_sapclass.Fund = _class.Fund;
            //_sapclass.FundsCtr = _class.FundsCtr;
            //_sapclass.CmmtItem = _class.CmmtItem;
            //_sapclass.Pricedate = _class.Pricedate;
            //_sapclass.PriceDate = _class.PriceDate;
            //_sapclass.GrossWt = _class.GrossWt;
            //_sapclass.Volume = _class.Volume;
            //_sapclass.Volumeunit = _class.Volumeunit;
            //_sapclass.VolumeunitIso = _class.VolumeunitIso;
            //_sapclass.Incoterms1 = _class.Incoterms1;
            //_sapclass.Incoterms2 = _class.Incoterms2;
            //_sapclass.PreVendor = _class.PreVendor;
            //_sapclass.VendPart = _class.VendPart;
            //_sapclass.HlItem = _class.HlItem;
            //_sapclass.GrToDate = _class.GrToDate;
            //_sapclass.SuppVendor = _class.SuppVendor;
            //_sapclass.ScVendor = _class.ScVendor;
            //_sapclass.KanbanInd = _class.KanbanInd;
            //_sapclass.Ers = _class.Ers;
            //_sapclass.RPromo = _class.RPromo;
            //_sapclass.Points = _class.Points;
            //_sapclass.PointUnit = _class.PointUnit;
            //_sapclass.PointUnitIso = _class.PointUnitIso;
            //_sapclass.Season = _class.Season;
            //_sapclass.SeasonYr = _class.SeasonYr;
            //_sapclass.BonGrp2 = _class.BonGrp2;
            //_sapclass.BonGrp3 = _class.BonGrp3;
            //_sapclass.SettItem = _class.SettItem;
            //_sapclass.Minremlife = _class.Minremlife;
            //_sapclass.Minremlife = _class.Minremlife;
            //_sapclass.RfqNo = _class.RfqNo;
            //_sapclass.RfqItem = _class.RfqItem;
            //_sapclass.PreqNo = _class.PreqNo;
            //_sapclass.PreqItem = _class.PreqItem;
            //_sapclass.RefDoc = _class.RefDoc;
            //_sapclass.RefItem = _class.RefItem;
            //_sapclass.SiCat = _class.SiCat;
            //_sapclass.RetItem = _class.RetItem;
            //_sapclass.AtRelev = _class.AtRelev;
            //_sapclass.OrderReason = _class.OrderReason;
            //_sapclass.BrasNbm = _class.BrasNbm;
            //_sapclass.MatlUsage = _class.MatlUsage;
            //_sapclass.MatOrigin = _class.MatOrigin;
            //_sapclass.InHouse = _class.InHouse;
            //_sapclass.Indus3 = _class.Indus3;
            //_sapclass.InfIndex = _class.InfIndex;
            //_sapclass.UntilDate = _class.UntilDate;
            //_sapclass.DelivCompl = _class.DelivCompl;
            //_sapclass.PartDeliv = _class.PartDeliv;
            //_sapclass.ShipBlocked = _class.ShipBlocked;
            _sapclass.PreqName = _class.PreqName;
            //_sapclass.PeriodIndExpirationDate = _class.PeriodIndExpirationDate;
            //_sapclass.IntObjNo = _class.IntObjNo;
            //_sapclass.PckgNo = _class.PckgNo;
            //_sapclass.Batch = _class.Batch;
            _sapclass.Vendrbatch = _class.Vendrbatch;
            //_sapclass.Calctype = _class.Calctype;
            //_sapclass.GrantNbr = _class.GrantNbr;
            //_sapclass.CmmtItemLong = _class.CmmtItemLong;
            //_sapclass.FuncAreaLong = _class.FuncAreaLong;
            //_sapclass.NoRounding = _class.NoRounding;
            //_sapclass.PoPrice = _class.PoPrice;
            //_sapclass.SupplStloc = _class.SupplStloc;
            //_sapclass.SrvBasedIv = _class.SrvBasedIv;
            //_sapclass.FundsRes = _class.FundsRes;
            //_sapclass.ResItem = _class.ResItem;
            //_sapclass.OrigAccept = _class.OrigAccept;
            //_sapclass.AllocTbl = _class.AllocTbl;
            //_sapclass.AllocTblItem = _class.AllocTblItem;
            //_sapclass.SrcStockType = _class.SrcStockType;
            //_sapclass.ReasonRej = _class.ReasonRej;
            //_sapclass.CrmSalesOrderNo = _class.CrmSalesOrderNo;
            //_sapclass.CrmSalesOrderItemNo = _class.CrmSalesOrderItemNo;
            //_sapclass.CrmRefSalesOrderNo = _class.CrmRefSalesOrderNo;
            //_sapclass.CrmRefSoItemNo = _class.CrmRefSoItemNo;
            //_sapclass.PrioUrgency = _class.PrioUrgency;
            //_sapclass.PrioRequirement = _class.PrioRequirement;
            //_sapclass.ReasonCode = _class.ReasonCode;
            _sapclass.Itemno = _class.Itemno;
            _sapclass.PoEcm = _class.PoEcm;



            Input[index] = _sapclass;

            return Input;

        }

        public static SAP_PO.Zecmposchedule[] addDataPoschedule(Poschedule _class, int index, int count)
        {

            SAP_PO.Zecmposchedule _sapclass = new SAP_PO.Zecmposchedule();

            SAP_PO.Zecmposchedule[] Input = new SAP_PO.Zecmposchedule[count];

            _sapclass.PoEcm = _class.PoEcm;
            _sapclass.PoItem = _class.PoItem;
            _sapclass.SchedLine = _class.SchedLine;
            //_sapclass.DelDatcatExt = _class.DelDatcatExt;
            _sapclass.DeliveryDate = _class.DeliveryDate;
            //_sapclass.Quantity = _class.Quantity;
            //_sapclass.DelivTime = _class.DelivTime;
            //_sapclass.StatDate = _class.StatDate;
            //_sapclass.PreqNo = _class.PreqNo;
            //_sapclass.PreqItem = _class.PreqItem;
            //_sapclass.PoDate = _class.PoDate;
            //_sapclass.Routesched = _class.Routesched;
            //_sapclass.MsDate = _class.MsDate;
            //_sapclass.MsTime = _class.MsTime;
            //_sapclass.LoadDate = _class.LoadDate;
            //_sapclass.LoadTime = _class.LoadTime;
            //_sapclass.TpDate = _class.TpDate;
            //_sapclass.TpTime = _class.TpTime;
            //_sapclass.GiDate = _class.GiDate;
            //_sapclass.GiTime = _class.GiTime;
            //_sapclass.DeleteInd = _class.DeleteInd;
            //_sapclass.ReqClosed = _class.ReqClosed;
            //_sapclass.GrEndDate = _class.GrEndDate;
            //_sapclass.GrEndTime = _class.GrEndTime;
            //_sapclass.ComQty = _class.ComQty;
            //_sapclass.ComDate = _class.ComDate;

            Input[index] = _sapclass;

            return Input;

        }

        public static SAP_PO.Zecmpotextitem[] addDataPotextitem(Potextitem _class, int index, int count)
        {

            SAP_PO.Zecmpotextitem _sapclass = new SAP_PO.Zecmpotextitem();

            SAP_PO.Zecmpotextitem[] Input = new SAP_PO.Zecmpotextitem[count];

            _sapclass.PoEcm = _class.PoEcm;
            _sapclass.PoNumber = _class.PoNumber;
            _sapclass.PoItem = _class.PoItem;
            _sapclass.TextId = _class.TextId;
            _sapclass.TextForm = _class.TextForm;
            _sapclass.TextLine = _class.TextLine;
            _sapclass.TextLine = _class.TextLine;


            Input[index] = _sapclass;

            return Input;

        }

        //public static SAP_PO.[] addDataReturn(Return _class, int index, int count)
        //{

        //    SAP_PO.Zecmpotextitem _sapclass = new SAP_PO.Zecmpotextitem();

        //    SAP_PO.Zecmpotextitem[] Input = new SAP_PO.Zecmpotextitem[count];

        //    _sapclass.PoEcm = _class.PoEcm;
        //    _sapclass.PoNumber = _class.PoNumber;
        //    _sapclass.PoItem = _class.PoItem;
        //    _sapclass.TextId = _class.TextId;
        //    _sapclass.TextForm = _class.TextForm;
        //    _sapclass.TextLine = _class.TextLine;
        //    _sapclass.TextLine = _class.TextLine;


        //    Input[index] = _sapclass;

        //    return Input;

        //}



        public static SAP_PO.ZbapiEcmPoResponse CreatePO(SAP_PO.ZbapiEcmPo objPO)
        {
            SAP_PO.ZbapiEcmPoResponse response = new SAP_PO.ZbapiEcmPoResponse();

            try
            {
                //BasicHttpBinding binding = new BasicHttpBinding();
                //binding.Security.Mode = BasicHttpSecurityMode.None;
                //binding.Security.Transport.ClientCredentialType = HttpClientCredentialType.Basic;

                ///// your endpoint
                //EndpointAddress endpoint = new EndpointAddress("http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/wsdl/bndg_5F7178DFF62A0941E10000000AF1D333/wsdl11/allinone/ws_policy/document?sap-client=300");
                //SAP_PO.ZECM_POClient client = new SAP_PO.ZECM_POClient("ZECM_PO", endpoint);

                ///// user / password
                //client.ClientCredentials.UserName.UserName = "ECMCONNECT";
                //client.ClientCredentials.UserName.Password = "egat2020";
                //client.ClientCredentials.UserName.UserName = "SIS_SUPPORT";
                //client.ClientCredentials.UserName.Password = "atos2014";

                /// call response class to return value
                //ZecmServFuncLoc funclog = new ZecmServFuncLoc();
                //funclog.ImKksCode = "40HCB06AT021-K01";
                //SAP_PO.ZecmServFuncLocResponse result = client.ZecmServFuncLoc(funclog);

                //response = client.ZbapiEcmPo(objPO);

                //var url = "http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/rfc/sap/zecm_po/300/zecm_po/zecm_po";// ConfigurationManager.AppSettings["wbs_url"].ToString();
                //var username = "ECMCONNECT";//ConfigurationManager.AppSettings["wbs_username"].ToString();
                //var password = "egat2020";// ConfigurationManager.AppSettings["wbs_password"].ToString();

                //NetworkCredential credential = new NetworkCredential();
                //credential.UserName = username;
                //credential.Password = password;

                //SAP_PO.ZECM_PO zPO_CREATE = new SAP_PO.ZECM_PO();
                //zPO_CREATE.PreAuthenticate = true;
                //zPO_CREATE.Credentials = credential;
                //zPO_CREATE.Url = url;




                //response = zPO_CREATE.ZbapiEcmPo(objPO);

                if (response.Return.Length > 0)
                {
                    var responseType = response.Return[0].Type;
                    var result = response.Return[0].Message;
                }
                else
                {
                    response = null;
                }

            }
            catch (Exception ex)
            {
                response = null;
            }

            return response;
        }


        public static string genXMLRequestPO(string po_num)
        {
            Console.WriteLine("##########  Send To SAP ###########");
            var soapNs = @"http://schemas.xmlsoap.org/soap/envelope/";
            var soapURN = @"urn:sap-com:document:sap:soap:functions:mc-style";
            var bodyNs = @"";

            XmlDocument doc = new XmlDocument();

            //// Create an XML declaration.
            //XmlDeclaration xmldecl;
            //xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            //xmldecl.Encoding = "UTF-8";

            //// Add the new node to the document.
            //XmlElement rootHead = doc.DocumentElement;
            //doc.InsertBefore(xmldecl, rootHead);



            XmlElement root = doc.CreateElement("soapenv", "Envelope", soapNs);
            root.SetAttribute("xmlns:soapenv", soapNs);
            root.SetAttribute("xmlns:urn", soapURN);
            doc.AppendChild(root);

            var header = root.AppendChild(doc.CreateElement("soapenv", "Header", soapNs));
            var body = root.AppendChild(doc.CreateElement("soapenv", "Body", soapNs));
            var ZbapiEcmPo = body.AppendChild(doc.CreateElement("urn:ZbapiEcmPo", soapURN));
            var MemoryComplete = ZbapiEcmPo.AppendChild(doc.CreateElement("", "MemoryComplete", bodyNs));
            var MemoryUncomplete = ZbapiEcmPo.AppendChild(doc.CreateElement("", "MemoryUncomplete", bodyNs));
            var mode = ZbapiEcmPo.AppendChild(doc.CreateElement("", "Mode", bodyNs)).InnerText = "01";

            string className = "";
            DataTable dt = new DataTable();
            #region Poaccount
            // Poaccount
            Poaccount poaccountItems = new Poaccount();
            PropertyInfo[] properties = poaccountItems.GetType().GetProperties();
            className = poaccountItems.GetType().Name;
            var nodePoaccount = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));



            foreach (var item in listPoaccountItems)
            {
                doc = addData(doc, nodePoaccount, item);
            }
            #endregion

            //#region Pocond
            //// Pocond
            //Pocond PocondItems = new Pocond();
            //properties = PocondItems.GetType().GetProperties();
            //className = PocondItems.GetType().Name;
            //var nodePocond = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addPocond(dt))
            //{
            //    doc = addData(doc, nodePocond, item);
            //}
            //#endregion

            #region Poheader
            // Poheader
            Poheader PoheaderItems = new Poheader();
            properties = PoheaderItems.GetType().GetProperties();
            className = PoheaderItems.GetType().Name;
            var nodePoheader = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in listPoheaderItems)
            {
                doc = addData(doc, nodePoheader, item);
            }
            #endregion

            #region Poitem
            // Poitem
            Poitem PoitemItems = new Poitem();
            properties = PoitemItems.GetType().GetProperties();
            className = PoitemItems.GetType().Name;
            var nodePoitem = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in listPoitemtItems)
            {
                doc = addData(doc, nodePoitem, item);
            }
            #endregion

            #region Poschedule
            // Poschedule
            Poschedule PoscheduleItems = new Poschedule();
            properties = PoscheduleItems.GetType().GetProperties();
            className = PoscheduleItems.GetType().Name;
            var nodePoschedule = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in listPoscheduleItems)
            {
                doc = addData(doc, nodePoschedule, item);
            }
            #endregion

            #region Potextitem
            // Potextitem
            Potextitem PotextitemItems = new Potextitem();
            properties = PotextitemItems.GetType().GetProperties();
            className = PotextitemItems.GetType().Name;
            var nodePotextitem = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in listPotextitemItems)
            {
                doc = addData(doc, nodePotextitem, item);
            }
            #endregion

            #region Return
            // Return
            ReturnPO ReturnItems = new ReturnPO();
            properties = ReturnItems.GetType().GetProperties();
            className = ReturnItems.GetType().Name.Substring(0, ReturnItems.GetType().Name.Length - 2);
            var nodeReturn = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in addReturnPO())
            {
                doc = addData(doc, nodeReturn, item);
            }
            #endregion



            if (Mode == "Test Run")
            {
                var Testrun = ZbapiEcmPo.AppendChild(doc.CreateElement("", "Testrun", bodyNs)).InnerText = "X";
            }
            else
            {
                var notTestrun = ZbapiEcmPo.AppendChild(doc.CreateElement("", "Testrun", bodyNs));
            }

            //ReservationItemsChg resItemsChg = new ReservationItemsChg();
            //doc = addData(doc, ZbapiEcmReservation, resItemsChg);

            //ReservationItemsNew resItemsNew = new ReservationItemsNew();
            //doc = addData(doc, ZbapiEcmReservation, resItemsNew);

            //Reservationheader resheader = new Reservationheader();
            //doc = addData(doc, ZbapiEcmReservation, resheader);

            string fileDoc = "POtoSAP" + po_num + ".xml";
            //doc.Save("C:\\testXML\\" + fileDoc);
            //XmlDocument doc3 = new XmlDocument();
            //doc3.Load("C:\\testXML\\PO.xml");
            string data = XmlSerialize(doc);



            var url = ConfigurationManager.AppSettings["url_qa_po"].ToString();
            var username = ConfigurationManager.AppSettings["username"].ToString();
            var password = ConfigurationManager.AppSettings["password"].ToString();

            //var url = "http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/rfc/sap/zecm_po/300/zecm_po/zecm_po";// ConfigurationManager.AppSettings["wbs_url"].ToString();
            //var username = "ECMCONNECT";//ConfigurationManager.AppSettings["wbs_username"].ToString();
            //var password = "egat2020";// ConfigurationManager.AppSettings["wbs_password"].ToString();

            string sapPoNumber = "";
            string sap_status = "";
            /*var res =*/
            try
            {
                sap_log = new ps_sap_log();
                sap_log.send_xlm_text = data;
                string res = DoHttpWebRequestPO(url, "POST", data, username, password);
                XmlDocument doc2 = new XmlDocument();
                doc2.LoadXml(res);
                // string FileName = "Result" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
                // FileName = FileName + ".xml";
                //// package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));
                // doc2.Save("C:\\" + FileName);
                //doc2.Save("C:\\testXML\\return.xml");
                string error = "";
                sapPoNumber = "";
                string PoECM = "";
                sap_status = "";
                List<string> lsitError = new List<string>();
                if (Mode == "Test Run")
                {

                    for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes.Count; i++)
                    {
                        if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText == "E")
                        {
                            //error = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[4].InnerText;
                            lsitError.Add(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[4].InnerText);
                            sap_status = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText;
                        }
                        if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText == "S")
                        {
                            sap_status = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText;
                            break;
                        }
                    }


                    //if (doc2.GetElementsByTagName("Expheader").Item(0).ChildNodes[1].ChildNodes[1] != null)
                    //{
                    //    PoECM = doc2.GetElementsByTagName("Expheader").Item(0).ChildNodes[1].ChildNodes[0].InnerText;
                    //    PoNumber = doc2.GetElementsByTagName("Expheader").Item(0).ChildNodes[1].ChildNodes[1].InnerText;
                    //}
                }
                else
                {
                    for (int i = 0; i < doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes.Count; i++)
                    {
                        if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText == "E")
                        {
                            //error = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[4].InnerText;
                            lsitError.Add(doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[4].InnerText);
                            sap_status = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText;
                        }
                        if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText == "S")
                        {
                            sap_status = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[6].ChildNodes[i].ChildNodes[1].InnerText;
                            //break;
                        }
                    }
                    if (doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].InnerText != "")
                    {
                        PoECM = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[0].InnerText;
                        sapPoNumber = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].InnerText;

                        update_ps_t_bid_selling_cmpo_item(PoECM, sapPoNumber);
                    }
                }

                if (lsitError.Count > 0)
                {
                    int count = 0;
                    foreach (var item in lsitError)
                    {
                        error = error + item + ",";
                        count++;
                        if (count == lsitError.Count)
                        {
                            error = error.Substring(0, error.Length - 1);
                        }

                    }

                }
                CultureInfo th = new CultureInfo("th-TH");

                sap_log.api_name = "PO";
                sap_log.batch_no = Batch_no;
                sap_log.return_message = res;
                sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                sap_log.error_msg_othr = error;
                insert_ps_sap_log(sap_log);

            }
            catch (Exception ex)
            {

            }
            //UpdatePOID();
            //if (!PoNumber.Equals(""))
            //{

            //    update_ps_t_contract_no("S",res, cNo);
            //}else
            //{
            //    //List<string> listMessage = new List<string>();
            //    //listMessage = getListError(doc2, "Message");


            //    //int i = 0;
            //    //ConsoleUtility.WriteProgress(i);
            //    //foreach (var item in listMessage)
            //    //{
            //    //    error += item + ",";
            //    //    ConsoleUtility.WriteProgress(i, true);
            //    //    Thread.Sleep(50);
            //    //    i++;
            //    //}

            //    //error = doc2.GetElementsByTagName("Return").Item(0).ChildNodes[5].ChildNodes[4].InnerText;
            //    error = doc2.GetElementsByTagName("Return").Item(0).InnerText;

            //    update_ps_t_contract_no("E", error, cNo);
            //}

            //List<string[]> listPoNumber = new List<string[]>();
            //listPoNumber = getListPoNumber(doc2);

            //List<string> listType = new List<string>();
            //listType = getListError(doc2, "Type");

            //List<string> listMessage = new List<string>();
            //listMessage = getListError(doc2, "Message");

            return sap_status;


        }

        public static string genXMLRequestPO6Digit(string DocDate)
        {
            Console.WriteLine("##########  Send To SAP ###########");
            var soapNs = @"http://schemas.xmlsoap.org/soap/envelope/";
            var soapURN = @"urn:sap-com:document:sap:soap:functions:mc-style";
            var bodyNs = @"";

            XmlDocument doc = new XmlDocument();

            //// Create an XML declaration.
            //XmlDeclaration xmldecl;
            //xmldecl = doc.CreateXmlDeclaration("1.0", null, null);
            //xmldecl.Encoding = "UTF-8";

            //// Add the new node to the document.
            //XmlElement rootHead = doc.DocumentElement;
            //doc.InsertBefore(xmldecl, rootHead);



            XmlElement root = doc.CreateElement("soapenv", "Envelope", soapNs);
            root.SetAttribute("xmlns:soapenv", soapNs);
            root.SetAttribute("xmlns:urn", soapURN);
            doc.AppendChild(root);

            var header = root.AppendChild(doc.CreateElement("soapenv", "Header", soapNs));
            var body = root.AppendChild(doc.CreateElement("soapenv", "Body", soapNs));
            var ZbapiEcmPo = body.AppendChild(doc.CreateElement("urn:ZbapiEcmPo", soapURN));
            var MemoryComplete = ZbapiEcmPo.AppendChild(doc.CreateElement("", "MemoryComplete", bodyNs));
            var MemoryUncomplete = ZbapiEcmPo.AppendChild(doc.CreateElement("", "MemoryUncomplete", bodyNs)).InnerText = "X";
            var mode = ZbapiEcmPo.AppendChild(doc.CreateElement("", "Mode", bodyNs)).InnerText = "01";

            string className = "";

            #region Poaccount
            // Poaccount
            Poaccount poaccountItems = new Poaccount();
            PropertyInfo[] properties = poaccountItems.GetType().GetProperties();
            //className = poaccountItems.GetType().Name;
            //var nodePoaccount = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));



            //foreach (var item in addPoaccount(dt))
            //{
            //    doc = addData(doc, nodePoaccount, item);
            //}
            #endregion

            //#region Pocond
            //// Pocond
            //Pocond PocondItems = new Pocond();
            //properties = PocondItems.GetType().GetProperties();
            //className = PocondItems.GetType().Name;
            //var nodePocond = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addPocond(dt))
            //{
            //    doc = addData(doc, nodePocond, item);
            //}
            //#endregion
            DataTable dt = new DataTable();
            #region Poheader
            // Poheader
            Poheader PoheaderItems = new Poheader();
            properties = PoheaderItems.GetType().GetProperties();
            className = PoheaderItems.GetType().Name;
            var nodePoheader = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addPoheade6ditgitr( DocDate))
            //{
            doc = addData(doc, nodePoheader, PoheaderItems);
            //}
            #endregion

            #region Poitem
            // Poitem
            Poitem PoitemItems = new Poitem();
            properties = PoitemItems.GetType().GetProperties();
            className = PoitemItems.GetType().Name;
            var nodePoitem = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addPoitem(dt))
            //{
            doc = addData(doc, nodePoitem, PoitemItems);
            //}
            #endregion

            #region Poschedule
            // Poschedule
            Poschedule PoscheduleItems = new Poschedule();
            properties = PoscheduleItems.GetType().GetProperties();
            className = PoscheduleItems.GetType().Name;
            var nodePoschedule = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addPoschedule(dt))
            //{
            doc = addData(doc, nodePoschedule, PoscheduleItems);
            //}
            #endregion

            #region Potextitem
            // Potextitem
            Potextitem PotextitemItems = new Potextitem();
            properties = PotextitemItems.GetType().GetProperties();
            className = PotextitemItems.GetType().Name;
            var nodePotextitem = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            //foreach (var item in addPotextitem(dt))
            //{
            doc = addData(doc, nodePotextitem, PotextitemItems);
            //}
            #endregion

            #region Return
            // Return
            ReturnPO ReturnItems = new ReturnPO();
            properties = ReturnItems.GetType().GetProperties();
            className = ReturnItems.GetType().Name.Substring(0, ReturnItems.GetType().Name.Length - 2);
            var nodeReturn = ZbapiEcmPo.AppendChild(doc.CreateElement("", className, bodyNs));

            foreach (var item in addReturnPO())
            {
                doc = addData(doc, nodeReturn, item);
            }
            #endregion

            var Testrun = ZbapiEcmPo.AppendChild(doc.CreateElement("", "Testrun", bodyNs));


            //ReservationItemsChg resItemsChg = new ReservationItemsChg();
            //doc = addData(doc, ZbapiEcmReservation, resItemsChg);

            //ReservationItemsNew resItemsNew = new ReservationItemsNew();
            //doc = addData(doc, ZbapiEcmReservation, resItemsNew);

            //Reservationheader resheader = new Reservationheader();
            //doc = addData(doc, ZbapiEcmReservation, resheader);

            //doc.Save("C:\\testXML\\PO6DigittoSAP.xml");
            //XmlDocument doc3 = new XmlDocument();
            //doc3.Load("C:\\testXML\\PO.xml");
            string data = XmlSerialize(doc);



            var url = ConfigurationManager.AppSettings["url_qa_po"].ToString();
            var username = ConfigurationManager.AppSettings["username"].ToString();
            var password = ConfigurationManager.AppSettings["password"].ToString();

            //var url = "http://erpdevr3ci.egat.co.th:8000/sap/bc/srt/rfc/sap/zecm_po/300/zecm_po/zecm_po";// ConfigurationManager.AppSettings["wbs_url"].ToString();
            //var username = "ECMCONNECT";//ConfigurationManager.AppSettings["wbs_username"].ToString();
            //var password = "egat2020";// ConfigurationManager.AppSettings["wbs_password"].ToString();


            /*var res =*/
            string res = DoHttpWebRequestPO6Digit(url, "POST", data, username, password);
            sap_log = new ps_sap_log();
            sap_log.send_xlm_text = data;
            XmlDocument doc2 = new XmlDocument();
            doc2.LoadXml(res);
            // string FileName = "Result" + "_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // FileName = FileName + ".xml";
            //// package.SaveAs(new FileInfo("C:\\Xls\\" + FileName));
            // doc2.Save("C:\\" + FileName);
            //doc2.Save("C:\\testXML\\return6digit.xml");
            string error = "";
            string Digit = "";
            string subDigit = "";

            Digit = doc2.ChildNodes[0].ChildNodes[1].ChildNodes[0].ChildNodes[0].ChildNodes[0].ChildNodes[1].InnerText;

            //if (doc2.GetElementsByTagName("Expheader").Item(0).ChildNodes[1].ChildNodes[1] != null)
            //{
            //    Digit = doc2.GetElementsByTagName("Expheader").Item(0).ChildNodes[1].ChildNodes[1].InnerText;
            //}

            if (Digit != "")
            {
                subDigit = Digit.Substring(4, 6);

            }

            CultureInfo th = new CultureInfo("th-TH");

            sap_log.api_name = "PO6Digit";
            sap_log.batch_no = Batch_no;
            sap_log.return_message = res;
            sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
            sap_log.error_msg_othr = error;
            insert_ps_sap_log(sap_log);



            return subDigit;


        }

        public static List<int> insert_ps_sap_log(ps_sap_log sap_log)
        {
            string result = "success";
            DataTable dtRunno = new DataTable();
            List<int> listID = new List<int>();
            try
            {

                cmdstrFull = "";


                cmdstr = @"insert into ps_sap_log (";
                cmdstr = cmdstr + getNameForInsert(sap_log) + ")";

                cmdvalue = "VALUES (";
                cmdvalue = cmdvalue + getValueForInsert(sap_log) + ");";

                condi = " SELECT SCOPE_IDENTITY();";
                cmdstrFull = cmdstr + cmdvalue + condi;

                //zdbUtil.ExecNonQuery(cmdstrFull, psDB);
                int id = zdbUtil.ExecuteScalar(cmdstrFull, psDB);

                listID.Add(id);


            }
            catch (Exception ex)
            {
                result = ex.Message;
            }
            return listID;
        }
        public static List<string[]> getListPoNumber(XmlDocument doc)
        {
            List<string[]> listPoNumber = new List<string[]>();
            string[] numPO = new string[2];
            int count = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes.Count;
            //var x = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[0].InnerText;
            //var x2 = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[0].ChildNodes[1].InnerText;
            for (int i = 0; i < count; i++)
            {
                numPO = new string[2];
                numPO[0] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[0].InnerText;
                numPO[1] = doc.GetElementsByTagName("Expheader").Item(0).ChildNodes[i].ChildNodes[1].InnerText;

            }
            listPoNumber.Add(numPO);
            return listPoNumber;



        }



        public static List<string> getListError(XmlDocument doc, string nameItem)
        {
            List<string> listError = new List<string>();
            int count = doc.GetElementsByTagName("Return").Item(0).ChildNodes.Count;

            Console.WriteLine("Get Reult");
            ConsoleUtility.WriteProgressBar(0);
            for (int i = 0; i < count; i++)
            {
                if (nameItem.Equals("Type"))
                {
                    listError.Add(doc.GetElementsByTagName("Return").Item(0).ChildNodes[i].ChildNodes[1].InnerText);
                }
                if (nameItem.Equals("Message"))
                {
                    listError.Add(doc.GetElementsByTagName("Return").Item(0).ChildNodes[i].ChildNodes[4].InnerText);
                }
                ConsoleUtility.WriteProgressBar(i, true);
                Thread.Sleep(100);
                Console.WriteLine();
            }

            return listError;
        }

        private static string DoHttpWebRequestPO(string url, string method, string data, string username, string password)
        {
            SAP_PO.ZbapiEcmPoResponse PoResponse = new SAP_PO.ZbapiEcmPoResponse();
            WrtieLogWbs("Request " + data);

            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.KeepAlive = false;
            req.ContentType = "text/xml";
            req.Method = method;
            req.Credentials = new NetworkCredential(username, password);

            byte[] buffer = Encoding.UTF8.GetBytes(data);
            Stream PostData = req.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            var result = "";

            //Console.WriteLine("Connecting to SAP.......");
            try
            {
                using (WebResponse webResponse = req.GetResponse())
                {
                    //ConsoleUtility.WriteProgressBar(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgressBar(i, true);
                    //    Thread.Sleep(30);
                    //}
                    //Console.WriteLine("");
                    //Console.WriteLine("Connected");

                    //ConsoleUtility.WriteProgress(0);
                    ////var getResponse = req.GetResponse();
                    //// Stream newStream = getResponse.GetResponseStream();
                    Stream newStream = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(newStream);
                    result = sr.ReadToEnd();

                    WrtieLogWbs("Result " + result);

                    //ConsoleUtility.WriteProgress(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgress(i, true);
                    //    Thread.Sleep(50);
                    //}
                }
            }
            catch (Exception ex)
            {
                CultureInfo th = new CultureInfo("th-TH");
                sap_log = new ps_sap_log();

                sap_log.api_name = "PO";
                sap_log.batch_no = Batch_no;
                sap_log.send_xlm_text = data;
                sap_log.return_message = ex.Message;
                sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                sap_log.error_msg_othr = ex.Message;
                insert_ps_sap_log(sap_log);
            }


            //var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
            //return settings;

            return result;


        }

        private static string DoHttpWebRequestPO6Digit(string url, string method, string data, string username, string password)
        {
            SAP_PO.ZbapiEcmPoResponse PoResponse = new SAP_PO.ZbapiEcmPoResponse();
            WrtieLogWbs("Request " + data);

            HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
            req.KeepAlive = false;
            req.ContentType = "text/xml";
            req.Method = method;
            req.Credentials = new NetworkCredential(username, password);

            byte[] buffer = Encoding.UTF8.GetBytes(data);
            Stream PostData = req.GetRequestStream();
            PostData.Write(buffer, 0, buffer.Length);
            PostData.Close();
            var result = "";

            //Console.WriteLine("Connecting to SAP.......");
            try
            {
                using (WebResponse webResponse = req.GetResponse())
                {
                    //ConsoleUtility.WriteProgressBar(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgressBar(i, true);
                    //    Thread.Sleep(30);
                    //}
                    //Console.WriteLine("");
                    //Console.WriteLine("Connected");

                    //ConsoleUtility.WriteProgress(0);
                    ////var getResponse = req.GetResponse();
                    //// Stream newStream = getResponse.GetResponseStream();
                    Stream newStream = webResponse.GetResponseStream();
                    StreamReader sr = new StreamReader(newStream);
                    result = sr.ReadToEnd();

                    WrtieLogWbs("Result " + result);

                    //ConsoleUtility.WriteProgress(0);
                    //for (var i = 0; i <= 100; ++i)
                    //{
                    //    ConsoleUtility.WriteProgress(i, true);
                    //    Thread.Sleep(50);
                    //}
                }
            }
            catch (Exception ex)
            {
                CultureInfo th = new CultureInfo("th-TH");
                sap_log = new ps_sap_log();

                sap_log.api_name = "PO6Digit";
                sap_log.batch_no = Batch_no;
                sap_log.send_xlm_text = data;
                sap_log.return_message = ex.Message;
                sap_log.sent_time = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                sap_log.error_msg_othr = ex.Message;
                insert_ps_sap_log(sap_log);
            }


            //var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
            //return settings;

            return result;


        }


        public static List<ReturnPO> addReturnPO()
        {
            List<ReturnPO> listReturnItems = new List<ReturnPO>();
            ReturnPO ReturnItems = new ReturnPO();
            listReturnItems.Add(ReturnItems);
            return listReturnItems;
        }

        public static XmlDocument addData(XmlDocument doc, XmlNode xnode, object _class)
        {
            var bodyNs = @"";
            PropertyInfo[] properties = _class.GetType().GetProperties();
            //if (className.Contains("Item"))
            //{
            var item = xnode.AppendChild(doc.CreateElement("", "item", bodyNs));

            foreach (var p in properties)
            {
                if (p.GetValue(_class) != null)
                {
                    item.AppendChild(doc.CreateElement("", p.Name, bodyNs)).InnerText = p.GetValue(_class).ToString();
                }
                else
                {
                    item.AppendChild(doc.CreateElement("", p.Name.Trim(), bodyNs));
                }

            }

            return doc;

        }
        public static string XmlSerializeToString(this object objectInstance)
        {
            var serializer = new XmlSerializer(objectInstance.GetType());
            var sb = new StringBuilder();

            using (TextWriter writer = new StringWriter(sb))
            {
                serializer.Serialize(writer, objectInstance);
            }

            return sb.ToString();
        }

        public static T XmlDeserializeFromString<T>(this string objectData)
        {
            return (T)XmlDeserializeFromString(objectData, typeof(T));
        }

        public static object XmlDeserializeFromString(this string objectData, Type type)
        {
            var serializer = new XmlSerializer(type);
            object result;

            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }

            return result;
        }




        private static string xmlToJson(string xml)
        {
            //if (fileName.Contains("feedback"))
            //{
            //    return;
            //}

            //string xml = File.ReadAllText(fileName);

            XmlDocument doc = new XmlDocument();
            doc.LoadXml(xml);

            string json = JsonConvert.SerializeXmlNode(doc);

            //File.WriteAllText(fileName.Replace(".xml", ".json"), json);
            return json;
        }


        private static string XmlSerialize<T>(T entity) where T : class
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;

            XmlSerializer xsSubmit = new XmlSerializer(typeof(T));
            using (StringWriter sw = new StringWriter())
            using (XmlWriter writer = XmlWriter.Create(sw, settings))
            {
                // removes namespace
                var xmlns = new XmlSerializerNamespaces();
                xmlns.Add("q1", "http://schemas.xmlsoap.org/soap/envelope/");
                xmlns.Add("q2", "urn:sap-com:document:sap:soap:functions:mc-style");

                xsSubmit.Serialize(writer, entity, xmlns);
                return sw.ToString(); // Your XML
            }
        }

        private static T DeserializeFromXmlString<T>(string xmlString)
        {
            var serializer = new XmlSerializer(typeof(T));
            using (TextReader reader = new StringReader(xmlString))
            {
                return (T)serializer.Deserialize(reader);
            }
        }
        //private static ModelReturn.Envelope DoHttpWebRequest(string url, string method, string data, string username, string password)
        //{
        //    WrtieLogWbs("Request " + data);

        //    HttpWebRequest req = WebRequest.Create(url) as HttpWebRequest;
        //    req.KeepAlive = false;
        //    req.ContentType = "text/xml";
        //    req.Method = method;
        //    req.Credentials = new NetworkCredential(username, password);

        //    byte[] buffer = Encoding.UTF8.GetBytes(data);
        //    Stream PostData = req.GetRequestStream();
        //    PostData.Write(buffer, 0, buffer.Length);
        //    PostData.Close();

        //    var getResponse = req.GetResponse();
        //    Stream newStream = getResponse.GetResponseStream();
        //    StreamReader sr = new StreamReader(newStream);
        //    var result = sr.ReadToEnd();

        //    WrtieLogWbs("Result " + result);

        //    var settings = DeserializeFromXmlString<ModelReturn.Envelope>(result);
        //    return settings;
        //}


        private static void WrtieLogWbs(string str)
        {
            System.IO.Directory.CreateDirectory(@"c:\logwbs");
            var currDate = DateTime.Now.ToString("yyyy-MM-dd");
            using (System.IO.StreamWriter file = new System.IO.StreamWriter(@"c:\logwbs\" + currDate + ".txt", true))
            {
                file.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + " " + str);
            }
        }

        #endregion
    }
    static class ConsoleUtility
    {
        const char _block = '■';
        const string _back = "\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b\b";
        const string _twirl = "-\\|/";
        public static void WriteProgressBar(int percent, bool update = false)
        {
            if (update)
                Console.Write(_back);
            Console.Write("[");
            var p = (int)((percent / 10f) + .5f);
            for (var i = 0; i < 10; ++i)
            {
                if (i >= p)
                    Console.Write(' ');
                else
                    Console.Write(_block);
            }
            Console.Write("] {0,3:##0}%", percent);
        }
        public static void WriteProgress(int progress, bool update = false)
        {
            if (update)
                Console.Write("\b");
            Console.Write(_twirl[progress % _twirl.Length]);
        }
    }
}

