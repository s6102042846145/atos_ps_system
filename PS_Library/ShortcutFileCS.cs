﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;

namespace PS_Library
{
    public class ShortcutFileCS
    {
        #region public variable
        private string zproject_nodeid = ConfigurationManager.AppSettings["project_nodeid"].ToString();
        public string zdb_ps = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string xfolder_workplan = ConfigurationManager.AppSettings["xfolder_workplan"].ToString();
        public string xfolder_jobs = ConfigurationManager.AppSettings["xfolder_jobs"].ToString();
        public string xfolder_bids = ConfigurationManager.AppSettings["xfolder_bids"].ToString();
        public string xfolder_scurve = ConfigurationManager.AppSettings["xfolder_scurve"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();
        static OTFunctions zotFunctions = new OTFunctions();

        //string xTopic = "";
        //string xProjectName = "";
        //string xjobno = "";
        //string xReqNo = "";
        #endregion

        public ShortcutFileCS()
        {

        }

        public void shortcutFileFromWA(string req_no, string process_id)
        {
            //bool status = false;
            int nodeid = 0;
            string project = "";
            List<string> list_name = new List<string>();
            List<int> list_revision = new List<int>();
            string sql = @"select wf.project_id, wfd.job_no, wfd.job_revision, wfd.document_nodeid, wfd.document_name from wf_waa_request_details wfd
                            inner join wf_waa_request wf on wf.waa_reqno = wfd.waa_reqno where wfd.waa_reqno = '" + req_no + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            if (ds.Tables[0].Rows.Count > 0)
            {
                nodeid = int.Parse(ds.Tables[0].Rows[0]["document_nodeid"].ToString());
                project = ds.Tables[0].Rows[0]["project_id"].ToString();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list_name.Add(dr["job_no"].ToString());
                    list_revision.Add(int.Parse(dr["job_revision"].ToString()));
                }

                for (int i = 0; i < list_name.Count; i++)
                {
                    try
                    {
                        string shortcutname = "WA_" + list_name[i] + "(R" + list_revision[i] + ")_" + req_no + ".pdf";
                        if (shortcutname.Length > 250)
                        {
                            shortcutname = shortcutname.Substring(0, 250);
                            shortcutname += ".pdf";
                        }

                        var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                        if (xparent == null)
                        {
                            xparent = zotFunctions.createFolder(zproject_nodeid, project);
                        }

                        var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                        if (xfolder == null)
                        {
                            xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                        }

                        var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_jobs);
                        if (xtype == null)
                        {
                            xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_jobs);
                        }

                        var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                        if (xjobbid == null)
                        {
                            xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                        }

                        zotFunctions.createShortCutNode(xjobbid.ID.ToString(), nodeid.ToString(), shortcutname, false);
                    }
                    catch (Exception e)
                    {
                        //status = false;
                        LogHelper.Write(e.ToString());
                    }

                    DataSet dsAtt = getFileFromReq(process_id);
                    List<string> list_att_nodeid = new List<string>();
                    List<string> list_att_name = new List<string>();
                    if (dsAtt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsAtt.Tables[0].Rows)
                        {
                            list_att_name.Add(dr["document_name"].ToString());
                            list_att_nodeid.Add(dr["document_nodeid"].ToString());
                        }

                        for (int j = 0; j < list_att_nodeid.Count; j++)
                        {

                            try
                            {
                                var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                                if (xparent == null)
                                {
                                    xparent = zotFunctions.createFolder(zproject_nodeid, project);
                                }

                                var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                                if (xfolder == null)
                                {
                                    xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                                }

                                var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_jobs);
                                if (xtype == null)
                                {
                                    xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_jobs);
                                }

                                var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                                if (xjobbid == null)
                                {
                                    xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                                }

                                string shortcutname = "WA_" + list_name[i] + "(R" + list_revision[i] + ")_" + req_no + "_Attached_" + list_att_name[j];
                                if (shortcutname.Length > 255)
                                {
                                    shortcutname = shortcutname.Substring(0, 250);
                                }

                                zotFunctions.createShortCutNode(xjobbid.ID.ToString(), list_att_nodeid[j].ToString(), shortcutname, false);
                                //status = true;
                            }
                            catch (Exception e)
                            {
                                //status = false;
                                LogHelper.Write(e.ToString());
                            }


                        }
                    }
                }
            }


        }

        public void shortcutFileFromPS(string req_no, string process_id)
        {
            //bool status = false;
            int nodeid = 0;
            string project = "";
            List<string> list_name = new List<string>();
            List<int> list_revision = new List<int>();
            string sql = @"select wf.project_id, wfd.bid_no, wfd.bid_revision, wfd.document_nodeid, wfd.document_name from wf_pcsa_request_details wfd
                            inner join wf_pcsa_request wf on wf.pcsa_no = wfd.pcsa_no where wfd.pcsa_no = '" + req_no + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            if (ds.Tables[0].Rows.Count > 0)
            {
                nodeid = int.Parse(ds.Tables[0].Rows[0]["document_nodeid"].ToString());
                project = ds.Tables[0].Rows[0]["project_id"].ToString();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list_name.Add(dr["bid_no"].ToString());
                    list_revision.Add(int.Parse(dr["bid_revision"].ToString()));
                }

                for (int i = 0; i < list_name.Count; i++)
                {
                    try
                    {
                        string shortcutname = "PS_" + list_name[i] + "(R" + list_revision[i] + ")_" + req_no + ".pdf";
                        if (shortcutname.Length > 250)
                        {
                            shortcutname = shortcutname.Substring(0, 250);
                            shortcutname += ".pdf";
                        }

                        var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                        if (xparent == null)
                        {
                            xparent = zotFunctions.createFolder(zproject_nodeid, project);
                        }

                        var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                        if (xfolder == null)
                        {
                            xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                        }

                        var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_bids);
                        if (xtype == null)
                        {
                            xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_bids);
                        }

                        var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                        if (xjobbid == null)
                        {
                            xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                        }

                        zotFunctions.createShortCutNode(xjobbid.ID.ToString(), nodeid.ToString(), shortcutname, false);
                    }
                    catch (Exception e)
                    {
                        //status = false;
                        LogHelper.Write(e.ToString());
                    }

                    DataSet dsAtt = getFileFromReq(process_id);
                    List<string> list_att_nodeid = new List<string>();
                    List<string> list_att_name = new List<string>();
                    if (dsAtt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsAtt.Tables[0].Rows)
                        {
                            list_att_name.Add(dr["document_name"].ToString());
                            list_att_nodeid.Add(dr["document_nodeid"].ToString());
                        }

                        for (int j = 0; j < list_att_nodeid.Count; j++)
                        {
                            try
                            {
                                var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                                if (xparent == null)
                                {
                                    xparent = zotFunctions.createFolder(zproject_nodeid, project);
                                }

                                var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                                if (xfolder == null)
                                {
                                    xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                                }

                                var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_bids);
                                if (xtype == null)
                                {
                                    xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_bids);
                                }

                                var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                                if (xjobbid == null)
                                {
                                    xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                                }

                                string shortcutname = "PS_" + list_name[i] + "(R" + list_revision[i] + ")_" + req_no + "_Attached_" + list_att_name[j];
                                if (shortcutname.Length > 255)
                                {
                                    shortcutname = shortcutname.Substring(0, 250);
                                }

                                zotFunctions.createShortCutNode(xjobbid.ID.ToString(), list_att_nodeid[j].ToString(), shortcutname, false);
                                //status = true;
                            }
                            catch (Exception e)
                            {
                                //status = false;
                                LogHelper.Write(e.ToString());
                            }
                        }
                    }
                }
            }


        }

        public void shortcutFileFromSP(string req_no, string process_id)
        {
            //bool status = false;
            int nodeid = 0;
            string project = "";
            List<string> list_name = new List<string>();
            List<string> list_revision = new List<string>();
            List<string> list_subrevision = new List<string>();
            string sql = @"select distinct wf.project_id, wfd.job_no, wfd.job_revision, jsub.subrevision, wfd.document_nodeid, wfd.document_name from wf_spa_request_details wfd
                            inner join wf_spa_request wf on wf.sp_reqno = wfd.sp_reqno
                            inner join job_subrevision jsub on wf.sp_reqno = jsub.sp_no 
                            where wfd.sp_reqno = '" + req_no + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            if (ds.Tables[0].Rows.Count > 0)
            {
                nodeid = int.Parse(ds.Tables[0].Rows[0]["document_nodeid"].ToString());
                project = ds.Tables[0].Rows[0]["project_id"].ToString();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list_name.Add(dr["job_no"].ToString());
                    list_revision.Add(dr["job_revision"].ToString());
                    list_subrevision.Add(dr["subrevision"].ToString());
                }

                for (int i = 0; i < list_name.Count; i++)
                {
                    try
                    {
                        string shortcutname = "SP_" + list_name[i] + "(R" + list_revision[i] + "." + list_subrevision[i] + ")_" + req_no + ".pdf";
                        if (shortcutname.Length > 250)
                        {
                            shortcutname = shortcutname.Substring(0, 250);
                            shortcutname += ".pdf";
                        }

                        var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                        if (xparent == null)
                        {
                            xparent = zotFunctions.createFolder(zproject_nodeid, project);
                        }

                        var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                        if (xfolder == null)
                        {
                            xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                        }

                        var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_jobs);
                        if (xtype == null)
                        {
                            xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_jobs);
                        }

                        var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                        if (xjobbid == null)
                        {
                            xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                        }

                        zotFunctions.createShortCutNode(xjobbid.ID.ToString(), nodeid.ToString(), shortcutname, false);
                    }
                    catch (Exception e)
                    {
                        //status = false;
                        LogHelper.Write(e.ToString());
                    }

                    DataSet dsAtt = getFileFromReq(process_id);
                    List<string> list_att_nodeid = new List<string>();
                    List<string> list_att_name = new List<string>();
                    if (dsAtt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsAtt.Tables[0].Rows)
                        {
                            list_att_name.Add(dr["document_name"].ToString());
                            list_att_nodeid.Add(dr["document_nodeid"].ToString());
                        }

                        for (int j = 0; j < list_att_nodeid.Count; j++)
                        {

                            try
                            {
                                var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                                if (xparent == null)
                                {
                                    xparent = zotFunctions.createFolder(zproject_nodeid, project);
                                }

                                var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                                if (xfolder == null)
                                {
                                    xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                                }

                                var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_jobs);
                                if (xtype == null)
                                {
                                    xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_jobs);
                                }

                                var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                                if (xjobbid == null)
                                {
                                    xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                                }

                                string shortcutname = "SP_" + list_name[i] + "(R" + list_revision[i] + "." + list_subrevision[i] + ")_" + req_no + "_Attached_" + list_att_name[j];
                                if (shortcutname.Length > 255)
                                {
                                    shortcutname = shortcutname.Substring(0, 250);
                                }

                                zotFunctions.createShortCutNode(xjobbid.ID.ToString(), list_att_nodeid[j].ToString(), shortcutname, false);
                                //status = true;
                            }
                            catch (Exception e)
                            {
                                //status = false;
                                LogHelper.Write(e.ToString());
                            }

                        }
                    }
                }
            }


        }

        public void shortcutFileFromDTS(string req_no, string process_id)
        {
            //bool status = false;
            int nodeid = 0;
            string project = "";
            List<string> list_name = new List<string>();
            List<string> list_revision = new List<string>();
            List<string> list_subrevision = new List<string>();
            string sql = @"select distinct wf.project_id, wfd.bid_no, wfd.bid_revision, bsub.subrevision, wfd.document_nodeid, wfd.document_name from wf_dtsa_request_details wfd
                            inner join wf_dtsa_request wf on wf.dts_reqno = wfd.dts_reqno 
                            inner join bid_subrevision bsub on wf.process_id = bsub.dts_no 
                            where wfd.dts_reqno = '" + req_no + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            if (ds.Tables[0].Rows.Count > 0)
            {
                nodeid = int.Parse(ds.Tables[0].Rows[0]["document_nodeid"].ToString());
                project = ds.Tables[0].Rows[0]["project_id"].ToString();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list_name.Add(dr["bid_no"].ToString());
                    list_revision.Add(dr["bid_revision"].ToString());
                    list_subrevision.Add(dr["subrevision"].ToString());
                }

                for (int i = 0; i < list_name.Count; i++)
                {
                    try
                    {
                        string shortcutname = "DTSB_" + list_name[i] + "(R" + list_revision[i] + "." + list_subrevision[i] + ")_" + req_no + ".pdf";
                        if (shortcutname.Length > 250)
                        {
                            shortcutname = shortcutname.Substring(0, 250);
                            shortcutname += ".pdf";
                        }

                        var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                        if (xparent == null)
                        {
                            xparent = zotFunctions.createFolder(zproject_nodeid, project);
                        }

                        var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                        if (xfolder == null)
                        {
                            xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                        }

                        var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_bids);
                        if (xtype == null)
                        {
                            xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_bids);
                        }

                        var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                        if (xjobbid == null)
                        {
                            xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                        }

                        zotFunctions.createShortCutNode(xjobbid.ID.ToString(), nodeid.ToString(), shortcutname, false);
                    }
                    catch (Exception e)
                    {
                        //status = false;
                        LogHelper.Write(e.ToString());
                    }

                    DataSet dsAtt = getFileFromReq(process_id);
                    List<string> list_att_nodeid = new List<string>();
                    List<string> list_att_name = new List<string>();
                    if (dsAtt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsAtt.Tables[0].Rows)
                        {
                            list_att_name.Add(dr["document_name"].ToString());
                            list_att_nodeid.Add(dr["document_nodeid"].ToString());
                        }

                        for (int j = 0; j < list_att_nodeid.Count; j++)
                        {

                            try
                            {
                                var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                                if (xparent == null)
                                {
                                    xparent = zotFunctions.createFolder(zproject_nodeid, project);
                                }

                                var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                                if (xfolder == null)
                                {
                                    xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                                }

                                var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_bids);
                                if (xtype == null)
                                {
                                    xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_bids);
                                }

                                var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                                if (xjobbid == null)
                                {
                                    xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                                }

                                string shortcutname = "DTSB_" + list_name[i] + "(R" + list_revision[i] + "." + list_subrevision[i] + ")_" + req_no + "_Attached_" + list_att_name[j];
                                if (shortcutname.Length > 255)
                                {
                                    shortcutname = shortcutname.Substring(0, 250);
                                }

                                zotFunctions.createShortCutNode(xjobbid.ID.ToString(), list_att_nodeid[j].ToString(), shortcutname, false);
                                //status = true;
                            }
                            catch (Exception e)
                            {
                                //status = false;
                                LogHelper.Write(e.ToString());
                            }

                        }
                    }
                }
            }


        }

        public void shortcutFileFromDTSJ(string req_no, string process_id)
        {
            //bool status = false;
            int nodeid = 0;
            string project = "";
            List<string> list_name = new List<string>();
            List<string> list_revision = new List<string>();
            List<string> list_subrevision = new List<string>();
            string sql = @"select distinct wf.project_id, wfd.job_no, wfd.job_revision, jsub.subrevision, wfd.document_nodeid, wfd.document_name 
                            from wf_dtsj_request_details wfd
                            inner join wf_dtsj_request wf on wf.dtsj_reqno = wfd.dtsj_reqno 
                            inner join job_subrevision jsub on wf.process_id = jsub.sp_no 
                            where wfd.dtsj_reqno = '" + req_no + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            if (ds.Tables[0].Rows.Count > 0)
            {
                nodeid = int.Parse(ds.Tables[0].Rows[0]["document_nodeid"].ToString());
                project = ds.Tables[0].Rows[0]["project_id"].ToString();
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    list_name.Add(dr["job_no"].ToString());
                    list_revision.Add(dr["job_revision"].ToString());
                    list_subrevision.Add(dr["subrevision"].ToString());
                }

                for (int i = 0; i < list_name.Count; i++)
                {
                    try
                    {
                        string shortcutname = "DTSJ_" + list_name[i] + "(R" + list_revision[i] + "." + list_subrevision[i] + ")_" + req_no + ".pdf";
                        if (shortcutname.Length > 250)
                        {
                            shortcutname = shortcutname.Substring(0, 250);
                            shortcutname += ".pdf";
                        }

                        var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                        if (xparent == null)
                        {
                            xparent = zotFunctions.createFolder(zproject_nodeid, project);
                        }

                        var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                        if (xfolder == null)
                        {
                            xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                        }

                        var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_jobs);
                        if (xtype == null)
                        {
                            xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_jobs);
                        }

                        var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                        if (xjobbid == null)
                        {
                            xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                        }

                        zotFunctions.createShortCutNode(xjobbid.ID.ToString(), nodeid.ToString(), shortcutname, false);
                    }
                    catch (Exception e)
                    {
                        //status = false;
                        LogHelper.Write(e.ToString());
                    }

                    DataSet dsAtt = getFileFromReq(process_id);
                    List<string> list_att_nodeid = new List<string>();
                    List<string> list_att_name = new List<string>();
                    if (dsAtt.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsAtt.Tables[0].Rows)
                        {
                            list_att_name.Add(dr["document_name"].ToString());
                            list_att_nodeid.Add(dr["document_nodeid"].ToString());
                        }

                        for (int j = 0; j < list_att_nodeid.Count; j++)
                        {

                            try
                            {
                                var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                                if (xparent == null)
                                {
                                    xparent = zotFunctions.createFolder(zproject_nodeid, project);
                                }

                                var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                                if (xfolder == null)
                                {
                                    xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                                }

                                var xtype = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_jobs);
                                if (xtype == null)
                                {
                                    xtype = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_jobs);
                                }

                                var xjobbid = zotFunctions.getNodeByName(xtype.ID.ToString(), list_name[i]);
                                if (xjobbid == null)
                                {
                                    xjobbid = zotFunctions.createFolder(xtype.ID.ToString(), list_name[i]);
                                }

                                string shortcutname = "DTSJ_" + list_name[i] + "(R" + list_revision[i] + "." + list_subrevision[i] + ")_" + req_no + "_Attached_" + list_att_name[j];
                                if (shortcutname.Length > 255)
                                {
                                    shortcutname = shortcutname.Substring(0, 250);
                                }

                                zotFunctions.createShortCutNode(xjobbid.ID.ToString(), list_att_nodeid[j].ToString(), shortcutname, false);
                                //status = true;
                            }
                            catch (Exception e)
                            {
                                //status = false;
                                LogHelper.Write(e.ToString());
                            }

                        }
                    }
                }
            }
        }

        public void shortcutFileFromSC(string req_no, string process_id)
        {
            //bool status = false;
            int nodeid = 0;
            string project = "";
            string subrevision = "";
            //List<string> list_name = new List<string>();
            //List<int> list_subrevision = new List<int>();
            string sql = @"select distinct wf.project_id, psub.subrevision, wfd.document_nodeid, wfd.document_name from wf_sca_request_details wfd
                            inner join wf_sca_request wf on wf.sca_no = wfd.sca_no 
                            inner join project_scurve_subrevision psub on wf.sca_no = psub.sc_no 
                            where wfd.sca_no = '" + req_no + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            if (ds.Tables[0].Rows.Count > 0)
            {
                nodeid = int.Parse(ds.Tables[0].Rows[0]["document_nodeid"].ToString());
                project = ds.Tables[0].Rows[0]["project_id"].ToString();
                subrevision = ds.Tables[0].Rows[0]["subrevision"].ToString();
                //foreach (DataRow dr in ds.Tables[0].Rows)
                //{
                //    //list_name.Add(dr["job_no"].ToString());
                //    list_subrevision.Add(int.Parse(dr["job_revision"].ToString()));
                //}

                //for (int i = 0; i < list_name.Count; i++)
                //{
                try
                {
                    string shortcutname = "SC_" + project + "(R" + subrevision + ")_" + req_no + ".pdf";
                    if (shortcutname.Length > 255)
                    {
                        shortcutname = shortcutname.Substring(0, 250);
                        shortcutname += ".pdf";
                    }

                    var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                    if (xparent == null)
                    {
                        xparent = zotFunctions.createFolder(zproject_nodeid, project);
                    }

                    var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                    if (xfolder == null)
                    {
                        xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                    }

                    var xjobbid = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_scurve);
                    if (xjobbid == null)
                    {
                        xjobbid = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_scurve);
                    }

                    zotFunctions.createShortCutNode(xjobbid.ID.ToString(), nodeid.ToString(), shortcutname, false);
                }
                catch (Exception e)
                {
                    //status = false;
                    LogHelper.Write(e.ToString());
                }

                DataSet dsAtt = getFileFromReq(process_id);
                List<string> list_att_nodeid = new List<string>();
                List<string> list_att_name = new List<string>();
                if (dsAtt.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsAtt.Tables[0].Rows)
                    {
                        list_att_name.Add(dr["document_name"].ToString());
                        list_att_nodeid.Add(dr["document_nodeid"].ToString());
                    }

                    for (int i = 0; i < list_att_nodeid.Count; i++)
                    {
                        try
                        {
                            var xparent = zotFunctions.getNodeByName(zproject_nodeid, project);
                            if (xparent == null)
                            {
                                xparent = zotFunctions.createFolder(zproject_nodeid, project);
                            }

                            var xfolder = zotFunctions.getNodeByName(xparent.ID.ToString(), xfolder_workplan);
                            if (xfolder == null)
                            {
                                xfolder = zotFunctions.createFolder(xparent.ID.ToString(), xfolder_workplan);
                            }

                            var xjobbid = zotFunctions.getNodeByName(xfolder.ID.ToString(), xfolder_scurve);
                            if (xjobbid == null)
                            {
                                xjobbid = zotFunctions.createFolder(xfolder.ID.ToString(), xfolder_scurve);
                            }

                            string shortcutname = "SC_" + project + "(R" + subrevision + ")_" + req_no + "_Attached_" + list_att_name[i];
                            if (shortcutname.Length > 255)
                            {
                                shortcutname = shortcutname.Substring(0, 250);
                            }

                            zotFunctions.createShortCutNode(xjobbid.ID.ToString(), list_att_nodeid[i].ToString(), shortcutname, false);
                            //status = true;
                        }
                        catch (Exception e)
                        {
                            //status = false;
                            LogHelper.Write(e.ToString());
                        }
                    }
                }
            }


        }

        private DataSet getFileFromReq(string process_id)
        {
            string sql = @"select document_name, document_nodeid from wf_attachment where process_id = '" + process_id + "'";
            DataSet ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            return ds;
        }
    }
}