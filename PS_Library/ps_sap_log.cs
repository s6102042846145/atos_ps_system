﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PS_Library
{
    public class ps_sap_log
    {
        public int rowid { get; set; }
        public string batch_no { get; set; }
        public string api_name { get; set; }
        public string send_xlm_text { get; set; }
        public string return_message { get; set; }
        public string sent_time { get; set; }
        public string error_msg_vendor { get; set; }
        public string error_msg_matcode { get; set; }
        public string error_msg_currency { get; set; }
        public string error_msg_item_qty { get; set; }
        public string error_msg_othr { get; set; }

     
    }
}
