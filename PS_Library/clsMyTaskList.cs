﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Configuration;

namespace PS_Library
{
    public class clsMyTaskList
    {
        private string dbConnectionString = ConfigurationManager.AppSettings["db_ps"].ToString();
        private DbControllerBase db = new DbControllerBase();
        public clsMyTaskList() { }
        public DataTable getDisTask(string login, string mode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (mode == "job")
                {
                    sql = @"select distinct [job_no] from ps_t_p6_task_assign where assign_id='" + login + "' and job_no <> '' ";
                }
                else if (mode == "bid")
                {
                    sql = @"select distinct [bid_no] from ps_t_p6_task_assign where assign_id='" + login + "' and bid_no <> '' ";
                }

                dt = db.ExecSql_DataTable(sql, dbConnectionString);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }
        public DataTable getTaskAssign(string login, string mode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (mode == "job")
                {
                    sql = @"select  [job_no]
                          ,[job_revision]
                          ,[bid_no]
                          ,[bid_revision]
                          ,[p6_task_id]
                          ,[p6_task_name]
                          ,[p6_plan_start]
                          ,[p6_plan_end]
                          ,[assign_id]
                          ,[created_by]
                          ,[created_datetime]
                          ,[updated_by]
                          ,[updated_datetime]
                          from ps_t_p6_task_assign where assign_id='" + login + "' and job_no <> '' ";
                    sql += " order by job_no";
                }
                else if (mode == "bid")
                {
                    sql = @"select  [job_no]
                          ,[job_revision]
                          ,[bid_no]
                          ,[bid_revision]
                          ,[p6_task_id]
                          ,[p6_task_name]
                          ,[p6_plan_start]
                          ,[p6_plan_end]
                          ,[assign_id]
                          ,[created_by]
                          ,[created_datetime]
                          ,[updated_by]
                          ,[updated_datetime]
                          from ps_t_p6_task_assign where assign_id='" + login + "' and bid_no <> '' ";

                    sql += " order by bid_no";
                }

                dt = db.ExecSql_DataTable(sql, dbConnectionString);

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            return dt;
        }

        public DataTable getDDL(string login,string mode)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";
                if (mode == "job")
                {
                    sql = @"select distinct job_no from ps_t_p6_task_assign where assign_id='" + login + "' and job_no <> ''";
                }
                else if (mode == "bid")
                {
                    sql = @"select distinct bid_no from ps_t_p6_task_assign where assign_id='" + login + "' and bid_no <> ''";
                }
                
                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
          
            return dt;
        }
        public DataTable getDDLtaskname(string login)
        {
            DataTable dt = new DataTable();
            try
            {
                string sql = "";

                sql = @"select  distinct p6_task_name from ps_t_p6_task_assign where assign_id='" + login + "' and job_no <> '' and p6_task_name <> ''";

                dt = db.ExecSql_DataTable(sql, dbConnectionString);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
         
            return dt;
        }

    }
}
