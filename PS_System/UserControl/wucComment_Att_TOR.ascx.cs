﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using PS_System;
using PS_Library;
using PS_Library.DocumentManagement;

namespace PS_System.UserControl
{
    public partial class wucComment_Att_TOR : System.Web.UI.UserControl
    {
        #region Public valiables
        public string zdb_ps =  ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase zdbUtil = new DbControllerBase();
        static PS_Library.OTFunctions zotFunctions = new PS_Library.OTFunctions();
        
        //static TMPS.Class.OTFunctions zotFunctions = new Class.OTFunctions();
        //public string zurl_viewdoc = ConfigurationManager.AppSettings["url_viewdoc"].ToString();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                //ini_gv1(); 

            }
        }
        public void BindData(string xwfattach_nodeid, string xprocessid, string xlogin, bool bViewOnly)
        {
            BindData(xwfattach_nodeid, xprocessid, xlogin);
            if (bViewOnly == true)
            {
                this.gv1.Columns[5].Visible = false;
                this.tr_add.Visible = false;
            }
        }
        public void BindData(string xwfattach_nodeid, string xprocessid, string xlogin)
        {
            if (xprocessid != "")
            {
                string sql = "select rowid,process_id,req_no,comment,document_name,document_nodeid,uploaded_by,CONVERT(varchar(20), uploaded_datetime,103) as uploaded_datetime from wf_comment_att where process_id = '" + xprocessid + "' ";
                var dt = zdbUtil.ExecSql_DataTable(sql, zdb_ps);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    lblReqNo.Text = dr["req_no"].ToString();
                }
                lblParentNodeID.Text = xwfattach_nodeid;
                lblProcessID.Text = xprocessid;
                lblLogin.Text = xlogin;
                bind_gv1();
                this.tr_add.Visible = true;
            }
            else
            {
                this.tr_add.Visible = false;
            }
        }
        public void ini_gv1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("rowid", typeof(int));
            dt.Columns.Add("process_id", typeof(string));
            dt.Columns.Add("document_name", typeof(string));
            dt.Columns.Add("document_nodeid", typeof(string));
            dt.Columns.Add("parent_nodeid", typeof(string));
            dt.Columns.Add("parent_name", typeof(string));
            dt.Columns.Add("comment", typeof(string));
            dt.Columns.Add("uploaded_by", typeof(string));
            dt.Columns.Add("uploaded_datetime", typeof(string));
            var dr = dt.NewRow();
            dt.Rows.Add(dr);
            gv1.DataSource = dt;
            gv1.DataBind();

        }
        public void bind_gv1()
        {
            string sql = @" select rowid,process_id,req_no,comment,document_name,document_nodeid,uploaded_by,CONVERT(varchar(20), uploaded_datetime,103) as uploaded_datetime from wf_comment_att where process_id = '" + lblProcessID.Text.Trim() + "' ";
            sql += " order by uploaded_datetime  ";

            var ds = zdbUtil.ExecSql_DataSet(sql, zdb_ps);
            gv1.DataSource = ds;
            gv1.DataBind();
         
            if (gv1.Rows.Count == 0)
                ini_gv1();
        }
        public void updateFolder(string xReqNo)
        {
            if (xReqNo != "")
            {
                lblReqNo.Text = xReqNo;
                //var xDocFolder = zotFunctions.getNodeByName(lblParentNodeID.Text.Trim(), xReqNo);
                //if (xDocFolder == null)
                //{
                //    xDocFolder = zotFunctions.getNodeByName(lblParentNodeID.Text.Trim(), lblProcessID.Text);

                //    if (xDocFolder != null)
                //    {
                //        xDocFolder.Name = xReqNo;
                //        zotFunctions.updateNode(xDocFolder);
                //        string sql = "update wf_attachment set req_no = '" + xReqNo + "' where process_id = '" + lblProcessID.Text + "' ";
                //        zdbUtil._runSQL(sql, zdb_ps);
                //        lblReqNo.Text = xReqNo;
                //    }
                //    else
                //    {
                //        xDocFolder = zotFunctions.createFolder(lblParentNodeID.Text.Trim(), xReqNo);
                //        string sql = "update wf_attachment set req_no = '" + xReqNo + "' where process_id = '" + lblProcessID.Text + "' ";
                //        zdbUtil._runSQL(sql, zdb_ps);
                //        lblReqNo.Text = xReqNo;
                //    }
                //}
            }
        }
        protected void gv1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "removedata")
            {
                if (lblProcessID.Text.Trim() != "" && lblLogin.Text != "")
                {
                    int index = Convert.ToInt32(e.CommandArgument);
                    string x = ((Label)(gv1.Rows[index].FindControl("gv1lblRowID"))).Text;
                    string nodeID = ((Label)(gv1.Rows[index].FindControl("gv1lblNodeID"))).Text;
                    if (x != "")//&& nodeID!=""
                    {
                        //TMPS.Class.EmpInfoClass empInfo = new Class.EmpInfoClass();
                        EmpInfoClass empInfo = new EmpInfoClass();
                        var emp = empInfo.getInFoByEmpID(lblLogin.Text.Trim());
                        string strUploadBy = "";
                        if (emp.PERNR != "")
                        {
                            strUploadBy = emp.SNAME + " (" + emp.MC_SHORT + ")";
                        }
                        else
                        {
                            strUploadBy = "anonymous";
                        }
                       if(nodeID != "") DeleteNode(nodeID);
                        string sql = "delete from wf_comment_att where uploaded_by = '" + strUploadBy + "' and rowid = " + x;
                        zdbUtil.ExecNonQuery(sql, zdb_ps);
                        bind_gv1();
                    }
                }
            }
            else if (e.CommandName == "viewdocument")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string x = ((Label)(gv1.Rows[index].FindControl("gv1lblRowID"))).Text;

                if (x != "")
                {
                    //string xurl = zurl_viewdoc + lblParentNodeID.Text.Trim() + "/";
                    //string xparent_nodeid = "";
                    string sql = "select * from wf_comment_att where rowid = " + x + " ";
                    var dt = zdbUtil.ExecSql_DataTable(sql, zdb_ps);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        //xparent_nodeid = dr["parent_nodeid"].ToString();
                        //var xdoc = zotFunctions.getNodeByID(dr["document_nodeid"].ToString());//xnode.VersionInfo.VersionNum
                        //xurl += xparent_nodeid + "/" + dr["document_name"].ToString() + "?nodeid=" + dr["document_nodeid"].ToString() + "&vernum=" + xdoc.VersionInfo.VersionNum.ToString();

                        string xurl = "http://" + ConfigurationManager.AppSettings["HOST_NAME"].ToString() + @"/tmps/forms/PreviewDocCS?nodeid=" + dr["document_nodeid"].ToString();
                        Response.Write("<script> window.open('" + xurl + "');</script>");
                    }
                }
            }
            else if (e.CommandName == "downloaddoc")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                string x = ((Label)(gv1.Rows[index].FindControl("gv1lblRowID"))).Text;
                if (x != "")
                {
                    string sql = "select * from wf_comment_att where rowid = " + x + " ";
                    var dt = zdbUtil.ExecSql_DataTable(sql, zdb_ps);
                    if (dt.Rows.Count > 0)
                    {
                        DataRow dr = dt.Rows[0];
                        var xcontent = zotFunctions.downloadDoc(dr["document_nodeid"].ToString());
                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "binary/octet-stream";
                        //Response.ContentType = "application/pdf";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + dr["document_name"].ToString().Replace(",", "_"));
                        Response.BinaryWrite(xcontent);
                        Response.Flush();
                        Response.End();
                    }
                }
            }
        }
        public void DeleteNode(string strNodeId)
        {
            OTFunctions ot = new OTFunctions();
            Node thisNode = ot.getNodeByID(strNodeId);
            if (thisNode != null)
            {
                ot.deleteNodeID(thisNode.ParentID.ToString(), strNodeId);
            }
        }

        protected void btnAddFile_Click(object sender, EventArgs e)
        {
            if ( txtRemark.Text!="")//inputFile.FileName.Length > 0 ||
            {
                if (lblProcessID.Text.Trim() != "")
                {
                    var xcontent = inputFile.FileBytes;
                    var xfilename = inputFile.FileName;
                    //DocumentManagement.Node xparent;
                    Node xparent = new Node();
                    string strFolderAttNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString();
                    string xparent_folder_name = "";

                    if (lblReqNo.Text == "")
                    {
                        xparent_folder_name = lblProcessID.Text.Trim();

                    }
                    else
                    {
                        xparent_folder_name = lblReqNo.Text.Trim();
                    }
                    xparent = zotFunctions.getNodeByName(strFolderAttNodeId, xparent_folder_name);

                    if (xparent == null)
                    {
                        xparent = zotFunctions.createFolder(strFolderAttNodeId, xparent_folder_name);
                    }
                    string strParent_NodeID = (xparent == null ? "" : xparent.ID.ToString());
                    var xnodeid = zotFunctions.uploadDoc(strParent_NodeID, xfilename, xfilename, xcontent, "");

                    string strUploadBy = "";
                    //TMPS.Class.EmpInfoClass empInfo = new Class.EmpInfoClass();
                    EmpInfoClass empInfo = new EmpInfoClass();
                    var emp = empInfo.getInFoByEmpID(lblLogin.Text.Trim());
                    if (emp.PERNR != "")
                    {
                        strUploadBy = emp.SNAME + " (" + emp.MC_SHORT + ")";
                    }
                    else
                    {
                        strUploadBy = "anonymous";
                    }

                    string sql = " insert into wf_comment_att " +
                        " (process_id, req_no,comment, document_name, document_nodeid, uploaded_by, uploaded_datetime )  " +
                        " values " +
                        "(" +
                        "'" + lblProcessID.Text.Trim() + "' , " +
                        "'" + lblReqNo.Text.Trim() + "' , " +
                        "'" + txtRemark.Text.Trim().Replace("'", "''") + "' , " +
                        "'" + xfilename + "' , " +
                        " " + xnodeid.ToString() + " , " +
                        "'" + strUploadBy + "' , " +
                        "GETDATE()  " +
                        ") ";
                    zdbUtil.ExecNonQuery(sql, zdb_ps);
                    txtRemark.Text = "";
                    bind_gv1();
                    // wucAttachedFile
                }
            }
            else { Response.Write("<script> alert('Please input comment first.');</script>"); }//file
        }
        public void addCSFile(string xdocid, string xdocname)
        {
            string sql1 = "select * from wf_attachment where process_id = '" + lblProcessID.Text + "' and document_name = '" + xdocname + "' and document_nodeid = '" + xdocid + "' ";
            var dtIs_Existing = zdbUtil.ExecSql_DataTable(sql1, zdb_ps);
            if (dtIs_Existing.Rows.Count == 0)
            {
                //sql1 = "select * from wf_attachment where process_id = '" + lblProcessID.Text + "' ";
                //var dt = zdbUtil._findDT(sql1, zdb_ps);
                //if (dt.Rows.Count > 0)
                //{
                //    DataRow dr = dt.Rows[0];
                //    lblReqNo.Text = dr["req_no"].ToString();
                //}

                //var xdoc = zotFunctions.getNodeByID(xdocid);
                //if (xdoc != null)
                //{
                //    var xparent = zotFunctions.getNodeByID(xdoc.ParentID.ToString());

                //    string strUploadBy = "";
                //    TMPS.Class.EmpInfoClass empInfo = new Class.EmpInfoClass();
                //    var emp = empInfo.getInFoByEmpID(lblLogin.Text.Trim());
                //    if (emp.PERNR != "")
                //    {
                //        strUploadBy = emp.SNAME + " (" + emp.MC_SHORT + ")";
                //    }
                //    else
                //    {
                //        strUploadBy = lblLogin.Text.Trim();
                //    }

                //    string sql = " insert into wf_attachment " +
                //                " (process_id, req_no, document_name, document_nodeid, parent_nodeid, " +
                //                "parent_name, remark, uploaded_by, uploaded_datetime )  " +
                //                " values " +
                //                "(" +
                //                "'" + lblProcessID.Text.Trim() + "' , " +
                //                "'" + lblReqNo.Text.Trim() + "' , " +
                //                "'" + xdocname.Replace(",", " ").Replace("'", " ") + "' , " +
                //                " " + xdocid + " , " +
                //                " " + xparent.ID.ToString() + " , " +
                //                "'" + xparent.Name.Replace(",", " ").Replace("'", " ") + "' , " +
                //                "'" + "" + "' , " +
                //                "'" + strUploadBy + "' , " +
                //                "GETDATE()  " +
                //                ") ";
                //    zdbUtil._runSQL(sql, zdb_ps);
                //}
            }
        }
    }
}