﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="wucComment_Att_TOR.ascx.cs" Inherits="PS_System.UserControl.wucComment_Att_TOR" %>
<style type="text/css">
    .auto-style1 {
        width: 100%;
    }

    .auto-style2 {
        height: 31px;
    }
</style>

<script type="text/javascript">

    function disableKeyEnter(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 13) {
            return false;
        }
        return true;
    }
</script>

<table class="auto-style1">
    <tr id="tr_add" runat="server">
        <td >
            <table style="width:100%">
                <tr>
                    <td style="width:22%">
                        <asp:Label ID="lblRemark" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Comment"></asp:Label>
                        &nbsp;<asp:TextBox ID="txtRemark" onkeypress="return disableKeyEnter(event)" runat="server" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Font-Names="Tahoma" Font-Size="10pt" Width="250px"></asp:TextBox>
                    </td>
                    <td style="width:15%">
                        <asp:FileUpload ID="inputFile" runat="server" />
                    </td>
                    <td >
                        &nbsp;<asp:Button ID="btnAddFile" runat="server" BorderColor="#CCCCCC" BorderStyle="Solid" BorderWidth="1px" Font-Names="tahoma" Font-Size="9pt" OnClick="btnAddFile_Click" Text="Add" Width="100px" />
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td >
            <asp:Panel ID="pData" runat="server" Height="150px" ScrollBars="Auto" >
                <asp:GridView ID="gv1" OnRowCommand="gv1_RowCommand" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" Width="1150px">
                    <Columns>
                        <asp:TemplateField HeaderText="DateTime">
                            <ItemTemplate>
                                <asp:Label ID="gv1lblRowID" Text='<%# Bind("rowid") %>' runat="server" Font-Names="Tahoma" Font-Size="9pt" Visible="False"></asp:Label>

                                <asp:Label ID="gv1lblDateTime" Text='<%# Bind("uploaded_datetime") %>' runat="server" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Comment">
                            <ItemTemplate>
                                <asp:Label ID="gv1lblRemark" Text='<%# Bind("comment") %>' runat="server" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Name">
                            <ItemTemplate>
                                <asp:LinkButton ID="gv1lbtnDocName" Text='<%# Bind("document_name") %>' runat="server" Font-Names="tahoma" Font-Size="10pt" ForeColor="Black" CommandName="viewdocument" CommandArgument='<%# Container.DataItemIndex %>'></asp:LinkButton>
                                <br />
                                <asp:Label ID="gv1lblNodeID" Text='<%# Bind("document_nodeid") %>' runat="server" Visible="False"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="400px" HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Download Doc">
                            <ItemTemplate>
                                <asp:LinkButton ID="gv1btnDownloadFile" runat="server" Text="Download" Font-Names="tahoma" Font-Size="9pt" ForeColor="DimGray" CommandName="downloaddoc" CommandArgument='<%# Container.DataItemIndex %>'></asp:LinkButton>
                            </ItemTemplate>
                            <HeaderStyle Width="120px" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="By">
                            <ItemTemplate>
                                <asp:Label ID="gv1lblUploadedBy" Text='<%# Bind("uploaded_by") %>' runat="server" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                            </ItemTemplate>
                            <HeaderStyle Width="80px" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:LinkButton ID="gv1lbtnRemove" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>' runat="server" Font-Names="tahoma" Font-Size="9pt" ForeColor="DimGray">Remove</asp:LinkButton>
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="80px" />
                        </asp:TemplateField>
                    </Columns>
                    <HeaderStyle BackColor="#507CD1" ForeColor="White" />
                </asp:GridView>
                <br />
            </asp:Panel>
        </td>
    </tr>
</table>
<asp:Label ID="lblProcessID" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblLogin" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblParentNodeID" runat="server" Visible="False"></asp:Label>
<asp:Label ID="lblReqNo" runat="server" Visible="False"></asp:Label>



