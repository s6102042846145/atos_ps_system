﻿<%@ Page Language="C#" MasterPageFile="MainDB.Master" AutoEventWireup="true" CodeBehind="db01.aspx.cs" Inherits="PS_System.form.DB_DeliveryShipment.db01" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div id="dvMain" runat="server" style="width: 100%; padding-top: 10px; ">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">

                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Dashboard Delivery Shipment"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

        <table style="width: 100%; margin: 10px">
            <tr>
                <td colspan="4" style="text-align: left; width: 8%">
                    <asp:Label ID="Label3" runat="server" Text="Contract No: " Width="6%"></asp:Label>
                    <asp:TextBox ID="txtContract" runat="server" Width="200px"/>
                    
                    <br />
                    <asp:Label ID="Label6" runat="server" Text="Bid No.: " Width="6%"></asp:Label>
                    <asp:DropDownList ID="ddlBidno" runat="server" Width="200px"></asp:DropDownList>
                    <br />
                    <asp:Label ID="Label1" runat="server" Text="Schedule Name: " Width="6%"></asp:Label>
                    <asp:DropDownList ID="ddlSch" runat="server" Width="200px"></asp:DropDownList>
                    <br />
                    <asp:Label ID="Label2" runat="server" Text="Part: " Width="6%"></asp:Label>
                    <asp:DropDownList ID="ddlPart" runat="server" Width="200px"></asp:DropDownList>
                    <br />
                    <asp:Button ID="btnFil" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFil_Click" />
                    <asp:Button ID="btnUpdatePO" runat="server" Text="Update SAP PO Status" class="btn btn-primary" OnClick="btnUpdatePO_Click" />
                </td>
            </tr>
        </table>
        <div style="width: 100%; margin: 10px; overflow: scroll; height: 500px;">
            <asp:GridView ID="gvDB01" runat="server"
                DataKeyNames="sap_po_num,sap_po_item,po_item_no"
                OnRowDataBound="gvDB01_OnRowDataBound"
                Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="True">
                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                <SortedDescendingHeaderStyle BackColor="#00547E" />
                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                <Columns>
                    <asp:TemplateField HeaderText="Part">
                        <ItemTemplate>
                            <asp:Label ID="lbPart" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Item No.">
                        <ItemTemplate>
                            <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Equipment Code">
                        <ItemTemplate>
                            <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Description">
                        <ItemTemplate>
                            <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("po_item_desc") %>'></asp:Label>
                        </ItemTemplate>
                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall2" />
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="INV." HeaderStyle-BackColor="#164094" HeaderStyle-ForeColor="white">
                        <ItemTemplate>
                            <div>
                                <asp:GridView
                                    ID="gvINV"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="ChildGrid">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Doc Year">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("doc_year")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mat Doc">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("mat_doc")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MatDoc Item">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("matdoc_itm")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Postting Date">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("pstng_date")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("quantity")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Net Price">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("val_loccur").ToString().Replace("$","")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("currency")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="midnightblue" Height="40px" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="pagnation" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="SHIPMENT." HeaderStyle-BackColor="#164094" HeaderStyle-ForeColor="white">
                        <ItemTemplate>
                            <div>
                                <asp:GridView
                                    ID="gvShipment"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="ChildGrid" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="True">
                                    <Columns>
                                        <asp:TemplateField HeaderText="Delivery Date">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 90px; flex-wrap: wrap"><%#Eval("delivery_date")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("quantity")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="midnightblue" Height="40px" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="pagnation" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField ItemStyle-Width="150px" HeaderText="Goods Receive." HeaderStyle-BackColor="#164094" HeaderStyle-ForeColor="white">
                        <ItemTemplate>
                            <div>
                                <asp:GridView
                                    ID="gvGoodsReceive"
                                    runat="server"
                                    AutoGenerateColumns="false"
                                    CssClass="ChildGrid">
                                    <Columns>

                                        <asp:TemplateField HeaderText="Doc Year">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("doc_year")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Mat Doc">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("mat_doc")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MatDoc Item">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("matdoc_itm")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Postting Date">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("pstng_date")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Quantity">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("quantity")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Net Price">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("val_loccur").ToString().Replace("$","")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                <div style="text-align: center; width: 80px; flex-wrap: wrap"><%#Eval("currency")%></div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <EditRowStyle BackColor="#2461BF" />
                                    <FooterStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="White" />
                                    <HeaderStyle BackColor="#CCCCCC" Font-Bold="True" ForeColor="midnightblue" Height="40px" />
                                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" CssClass="pagnation" />
                                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                    <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                    <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                    <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                    <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>
        </div>
    </div>
    <div style="margin: 10px">
        <%--<asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" />--%>
    </div>
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRev" runat="server" />
    <asp:HiddenField ID="hidJobNo" runat="server" />
    <asp:HiddenField ID="hidJobRev" runat="server" />
    <asp:HiddenField ID="hidLogin" runat="server" />
    <asp:HiddenField ID="hidDept" runat="server" />
    <asp:HiddenField ID="hidSect" runat="server" />
    <asp:HiddenField ID="hidMc" runat="server" />

</asp:Content>
