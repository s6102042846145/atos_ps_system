﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.DB_DeliveryShipment
{
    public partial class db01 : System.Web.UI.Page
    {
        clsDashboard objDB = new clsDashboard();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z594900";

                if (Request.QueryString["bidno"] != null)
                    hidBidNo.Value = Request.QueryString["bidno"];
                else
                    hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";
                //Debug 
                //hidContractNo.Value = "W100379-226M-TS12-S-02";
                //hidBidNo.Value = "TS12-S-02";
                //hidBidRev.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;
                bindDDL();
                bindData();

                //grdViewCustomers.DataSource = SelectData("SELECT top 3 row_id CustomerID, doc_year ContactName, mat_doc City from ps_t_sap_postatus_history a where a.hist_type = 'q'");
                

            }
        }
        private void bindData()
        {
            DataTable dt = new DataTable();
            dt = objDB.getDataDB("");
            gvDB01.DataSource = dt;
            gvDB01.DataBind();
        }
        private void bindDDL()
        {
            ddlBidno.Items.Clear();
            DataTable dt = objDB.getDDL("bidno");
            foreach (DataRow dr in dt.Rows)
            {
                ListItem item1 = new ListItem();
                item1.Text = dr["bid_no"].ToString() + " ( Rev. " + dr["bid_revision"].ToString() + " )";
                item1.Value = dr["bid_no"].ToString();
                ddlBidno.Items.Add(item1);
            }
            ddlBidno.DataBind();

            ddlSch.Items.Clear();
            DataTable dt2 = objDB.getDDL("");
            DataTable dtsch = dt2.DefaultView.ToTable(true, "schedule_name");
            DataTable dtpart = dt2.DefaultView.ToTable(true, "part_no");
            if (dtsch.Rows.Count > 0)
            {
                ddlSch.DataSource = dtsch;
                ddlSch.DataTextField = "schedule_name";
                ddlSch.DataValueField = "schedule_name";
                ddlSch.DataBind();
            }

            if (dtpart.Rows.Count > 0)
            {
                ddlPart.Items.Clear();
                ddlPart.DataSource = dtpart;
                ddlPart.DataTextField = "part_no";
                ddlPart.DataValueField = "part_no";
                ddlPart.DataBind();
            }


            ddlBidno.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlSch.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlPart.Items.Insert(0, new ListItem("<-- Select -->", ""));
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }


        protected void btnFil_Click(object sender, EventArgs e)
        {
            Refreshgrid();

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
        }





        protected void gvDB01_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string bid_no = gvDB01.DataKeys[e.Row.RowIndex].Value.ToString();

                string po_number = gvDB01.DataKeys[e.Row.RowIndex].Values[0].ToString().Replace("\r\n","");
                string po_item = gvDB01.DataKeys[e.Row.RowIndex].Values[1].ToString().Replace("\r\n", "");
                string po_item_no = gvDB01.DataKeys[e.Row.RowIndex].Values[2].ToString().Replace("\r\n", "");


                GridView gvINV = (GridView)e.Row.FindControl("gvINV");
                DataTable dt = new DataTable();
                dt = objDB.getDataINV(po_number, po_item_no, "Q");
                //dt = objDB.getDataINV(po_number, po_item, "Q");
                gvINV.DataSource = dt;
                gvINV.DataBind();

                GridView gvShipment = (GridView)e.Row.FindControl("gvShipment");
                dt = new DataTable();
                dt = objDB.getDataSHIPMENT(po_number, po_item);
                gvShipment.DataSource = dt;
                gvShipment.DataBind();

                GridView gvGoodsReceive = (GridView)e.Row.FindControl("gvGoodsReceive");
                dt = new DataTable();
                dt = objDB.getDataGoodsReceive(po_number, po_item, "E");
                gvGoodsReceive.DataSource = dt;
                gvGoodsReceive.DataBind();



                //grdViewOrdersOfCustomer.DataSource = SelectData("SELECT top 3 row_id OrderID, doc_year OrderDate from ps_t_sap_postatus_history a where a.hist_type = 'q'");

                //grdViewOrdersOfCustomer.DataBind();
            }
        }

        protected void btnUpdatePO_Click(object sender, EventArgs e)
        {
            string contract_no = txtContract.Text.Trim();
            if (contract_no != "")
            {
                string result = PS_Library.PO_Status.SAP_PO_Status.Po_Status_SendToSap(contract_no, "");
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Update Successfully');", true);
            }
            Refreshgrid();
        }
        protected void Refreshgrid()
        {
            string rowfilter = "";
            string contract_no = txtContract.Text.Trim();
            string Bidno = ddlBidno.SelectedValue;
            string Sch = ddlSch.SelectedValue;
            string Part = ddlPart.SelectedValue;


            if (contract_no != "")
            {
                rowfilter = string.Format("(contract_no = '{0}')", contract_no);
            }
            if (Bidno != "")
            {
                // rowfilter = string.Format("(bid_no = '{0}')", Bidno);
                if (rowfilter == "") rowfilter = string.Format("(bid_no = '{0}')", Bidno);
                else rowfilter = string.Format("{0} and bid_no = '{1}'", rowfilter, Bidno);
            }
            if (Sch != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(schedule_name = '{0}')", Sch);
                else rowfilter = string.Format("{0} and schedule_name = '{1}'", rowfilter, Sch);
            }
            if (Part != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(part_no = '{0}')", Part);
                else rowfilter = string.Format("{0} and part_no = '{1}'", rowfilter, Part);
            }

            //if (txtContract.Text.Trim() != "")
            //{
            //    rowfilter = string.Format("(contract_no = '{0}')", txtContract.Text.Trim());
            //}
            //if (ddlBidno.SelectedValue != "")
            //{
            //    rowfilter = string.Format("(bid_no = '{0}')", ddlBidno.SelectedValue);
            //}
            //if (ddlSch.SelectedValue != "")
            //{
            //    if (rowfilter == "") rowfilter = string.Format("(schedule_name = '{0}')", ddlSch.SelectedValue);
            //    else rowfilter = string.Format("{0} and schedule_name = '{1}'", rowfilter, ddlSch.SelectedValue);
            //}
            //if (ddlPart.SelectedValue != "")
            //{
            //    if (rowfilter == "") rowfilter = string.Format("(part_no = '{0}')", ddlPart.SelectedValue);
            //    else rowfilter = string.Format("{0} and part_no = '{1}'", rowfilter, ddlPart.SelectedValue);
            //}



            DataTable dtrowfil = objDB.getDataDB(rowfilter);
            gvDB01.DataSource = dtrowfil;
            gvDB01.DataBind();
        }
    }
}