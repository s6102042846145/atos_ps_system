﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="testddl.aspx.cs" Inherits="PS_System.form.testddl" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

<link href="../Content/bootstrap-multiselect.css" rel="stylesheet" />
<link href="../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
        <script src="../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <script src="../Scripts/bootstrap-3.0.3.min.js"></script>
    <script src="../Scripts/bootstrap-multiselect.js"></script>
    

    <!--
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/css/bootstrap-multiselect.css"/>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"/>
    <script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-multiselect/0.9.13/js/bootstrap-multiselect.js"></script>
        -->
    <script type="text/javascript">
        $(document).ready(function () {

            $('[id*=lstPart]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                disableIfEmpty: true,
                maxHeight: 500,
                length: 500,
                numberDisplayed: 1
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ListBox ID="lstPart" runat="server" Width="80%" Height="40px" SelectionMode="Multiple">
                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                <asp:ListItem Value="5" Text="5"></asp:ListItem>
            </asp:ListBox>
        </div>
    </form>
</body>
</html>
