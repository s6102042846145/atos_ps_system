﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadDocApp.aspx.cs" Inherits="PS_System.form.Master.UploadDocApp" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Document Upload</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <!-- Generic page styles -->
    <style>
        .table table tbody tr td a,
        .table table tbody tr td span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .table table > tbody > tr > td > span {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .table table > tbody > tr > td:first-child > a,
        .table table > tbody > tr > td:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .table > tbody > tr > td {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: middle;
        }

        .table table > tbody > tr > td:last-child > a,
        .table table > tbody > tr > td:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .table table > tbody > tr > td > a:hover,
        .table table > tbody > tr > td > span:hover,
        .table table > tbody > tr > td > a:focus,
        .table table > tbody > tr > td > span:focus {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#cbxNeedApprove').click(function () {
                $('#divNeedApprove').toggle();
            });
        });

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function showPleaseWait() {
            if (document.querySelector("#pleaseWaitDialog") == null) {
                var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
            <div class="modal-dialog">\
                <div class="modal-content">\
                    <div class="modal-header">\
                        <h4 class="modal-title">Please wait...</h4>\
                    </div>\
                    <div class="modal-body">\
                        <div class="progress">\
                          <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                          aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                          </div>\
                        </div>\
                    </div>\
                </div>\
            </div>\
        </div>';
                $(document.body).append(modalLoading);
            }

            $("#pleaseWaitDialog").modal("show");
        }

        /**
         * Hides "Please wait" overlay. See function showPleaseWait().
         */
        function hidePleaseWait() {
            $("#pleaseWaitDialog").modal("hide");
        }
    </script>
    <style>
        .mydropdownlist {
            color: black;
            font-size: 11px;
            padding: 2px 5px;
            border-radius: 2px;
            background-color: white;
            font-weight: normal;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 11px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
</head>
<body>
    <div style="padding-left: 10px; padding-right: 10px; padding-top: 10px; font-family: Tahoma; font-size: 11px">
        <form id="form1" runat="server">
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Document Upload Approval"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table style="font-family: Tahoma">

                    <tr>
                        <td colspan="2"><b>กอง :</b><br />
                            <asp:TextBox ID="tbxDepartment" runat="server" Width="400px" Enabled="false"></asp:TextBox>
                        </td>
                        <td colspan="2"><b>แผนก :</b><br />
                            <asp:TextBox ID="tbxSection" runat="server" Width="400px" Enabled="false"></asp:TextBox>
                            <asp:HiddenField ID="hidLogin" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblDocType" runat="server" Text="Document Type : " Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="mydropdownlist"></asp:DropDownList>

                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblUploadFile" runat="server" Text="Upload File : " Font-Bold="true"></asp:Label>
                        </td>
                        <td>
                            <asp:FileUpload ID="fileUploadDoc" runat="server" AllowMultiple="true" />
                        </td>
                        <td>
                            <asp:ImageButton ID="imgUploadFile" runat="server" ImageUrl="~/images/up.png" Height="26px" Width="36px" OnClientClick="showPleaseWait()" OnClick="imgUploadFile_Click" />

                        </td>
                        <td></td>
                    </tr>
                   
                   
                    <tr>
                        <td colspan="4">
                            <asp:CheckBox ID="cbxNeedApprove" runat="server" Text="Need Approval" OnCheckedChanged="cbxNeedApprove_CheckedChanged" AutoPostBack="true" />

                        </td>
                    </tr>


                    <tr>
                        <td colspan="4">
                            <div id="divApproval" runat="server">
                                <table style="width: 100%">
                                    <tr>
                                        <td>
                                            <table style="width: 100%">
                                                <tr>
                                                    <td style="text-align: center; font-size: larger; vertical-align: top; width: 100%; background-color: antiquewhite;">
                                                        <b>Search Approver</b>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td>
                                                                    <b>Emp ID:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="PopTbxEmpIdSearch" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <b>Full Name:</b>

                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="PopTbxEmpFullNameSearch" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>Department:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="PopTbxDeptSearch" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td>
                                                                    <b>Position:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="PopTbxPositionSearch" runat="server"></asp:TextBox>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <b>สังกัด:</b>
                                                                </td>
                                                                <td>
                                                                    <asp:TextBox ID="PopTbxEmpDeptSearch" runat="server"></asp:TextBox>
                                                                </td>
                                                                <td></td>
                                                                <td></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:Button ID="BtnSearchApprover" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="BtnSearchApprover_Click" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                            <td>
                                                                                <b>Memo Subject : </b><b style="color:red">*</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox ID="tbxSubject" runat="server" Width="500px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td >
                                                                                <b>Memo Content : </b><b style="color:red">*</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox ID="tbxContent" runat="server" Width="500px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                            <tr>
                                                                <td colspan="2" style="text-align: center">
                                                                    <b>Unselected Approver</b>
                                                                </td>
                                                                <td colspan="2" style="text-align: center">
                                                                    <b>Selected Approver</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="vertical-align:top">
                                                                    <asp:GridView ID="gvUnselectApprover" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" HorizontalAlign="Center" Width="400px"
                                                                        Visible="true" ClientIDMode="Static" Font-Size="11px" AllowPaging="true" OnPageIndexChanging="gvUnselectApprover_PageIndexChanging">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Approver Name">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("FULLNAME") %>' Style="word-wrap: normal; word-break: break-all;"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="210px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Approver Position">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblEmpPostion" runat="server" Text='<%# Bind("POSITION_SHORT") %>' Style="word-wrap: normal; word-break: break-all;"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="true" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Select">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="imgAddApprover" runat="server" ImageUrl="~/images/add.png" Height="16px" Width="16px" OnClick="imgAddApprover_Click" CommandArgument='<%# "z"+Eval("id") %>' />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                                <td colspan="2" style="vertical-align: top">
                                                                    <asp:GridView ID="gvSelectedApprover" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" HorizontalAlign="Center" Width="400px"
                                                                        Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10" OnRowDeleting="OnRowGvSelectedApproverDeleting">
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Approver No.">
                                                                                <ItemTemplate>
                                                                                    <%# Container.DataItemIndex + 1 %>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Approver Name">
                                                                                <ItemTemplate>
                                                                                    <asp:HiddenField ID="hdnEmpId" runat="server" Value='<%# Bind("id") %>' />
                                                                                    <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("FULLNAME") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="50%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Approver Position">
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="lblEmpPostion" runat="server" Text='<%# Bind("POSITION_SHORT") %>'></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="30%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="Delete">
                                                                                <ItemTemplate>
                                                                                    <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/bin.png" Height="16px" Width="16px" CommandName="Delete" />
                                                                                </ItemTemplate>
                                                                                <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td></td>

                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="4" style="width: 100%;">
                            <asp:GridView ID="gvDocUpload" runat="server" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDeleting="OnRowGvDocDeleting">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" HorizontalAlign="Center" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" /> 
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                        </ItemTemplate>
                                        <ItemStyle Width="20px" HorizontalAlign="center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbxFileName" TextMode="MultiLine" Rows="3" runat="server" Text='<%# Bind("doc_name") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doc Sheet">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbxDocSheetNo" TextMode="MultiLine" Rows="3" runat="server" Text='<%# Bind("doc_sheetno") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbxDocDesc" TextMode="MultiLine" Rows="3" runat="server" Text='<%# Bind("doc_desc") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbxRevisionDis" Width="90px" TextMode="MultiLine" Rows="3" runat="server" Text='<%# Bind("doc_revision_no") %>'></asp:TextBox>
                                            <asp:HiddenField ID="hdnRevision" Value='<%# Bind("doc_revision_no") %>' runat="server" />
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="90px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:TextBox ID="tbxRemark" runat="server" TextMode="MultiLine" Rows="3" Text='<%# Bind("doc_remark") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NodeId" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNodeID" runat="server" Text='<%# Bind("doc_nodeid") %>'></asp:Label>
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remove">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/bin.png" Height="26px" Width="26px" CommandName="Delete" />
                                        </ItemTemplate>
                                        <HeaderStyle />
                                        <ItemStyle Width="90px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" CssClass="btn btn-primary" OnClientClick="showPleaseWait()" OnClick="btnSubmit_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-danger" OnClick="btnCancel_Click" />
                            &nbsp;
                            <asp:Label ID="lblErrorMsg" ForeColor="Red" Font-Bold="true" Font-Size="Medium" runat="server" Text=""></asp:Label>
                        </td>


                    </tr>
                </table>
            </div>
        </form>
    </div>
</body>
</html>
