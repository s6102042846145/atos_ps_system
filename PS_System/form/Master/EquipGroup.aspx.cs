﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class EquipGroup : System.Web.UI.Page
    {
        public clsEquipGroup objEquipGroup = new clsEquipGroup();
        public string key_Decrypt = (ConfigurationManager.AppSettings["key_Decrypt"].ToString());
        CultureInfo th = new CultureInfo("th-TH");
        //public string strFolderAttEquipNodeId = ConfigurationManager.AppSettings["wf_doc_equip_folder"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                bindDoc_Data();
                bindEG_Data("");
                DDLbinddata();
                Session["dtEqGroupDoc"] = "";
            }
            else
            {

            }
            #region
            //    if (!IsPostBack)
            //    {
            //        hidATOS.Value = key_Decrypt; //String.Join(",", Encoding.UTF8.GetBytes(key_Decrypt)); //
            //        initialDataTable();
            //        BindGridView_Equip();
            //        loadBOMLevel();
            //        loadCategory();

            //        this.lstBOMLevel.SelectedValue = "ZCM";

            //        if (Request.QueryString["zuserlogin"] != null)
            //            this.hidLogin.Value = Request.QueryString["zuserlogin"];
            //        else
            //            this.hidLogin.Value = "z596359";

            //        this.hidIsFirstRun.Value = "t";
            //        this.btnSearchBOM_Click(sender, e);
            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "loadSessionData", "loadSessionData()", true);
            //    }
            //if (this.gvEquip.Rows.Count > 0)
            //{
            //    gvEquip.UseAccessibleHeader = true;
            //    gvEquip.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
            //if (this.gvMyEquip1.Rows.Count > 0)
            //{
            //    gvMyEquip1.UseAccessibleHeader = true;
            //    gvMyEquip1.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
            //if (this.gvBOMList.Rows.Count > 0)
            //{
            //    gvBOMList.UseAccessibleHeader = true;
            //    gvBOMList.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
            #endregion
        }

        #region " Search Equipment List "
        protected void btnSearch1_Click(object sender, EventArgs e)
        {
            this.myModal_Equip.Style.Add("display", "block");
            this.myModal_Doc.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            BindGridView_Equip();
        }
        private void BindGridView_Equip()
        {
            DataTable dtEG = new DataTable();
            dtEG = objEquipGroup.getEquipInfo(this.txtSearchEquip.Text);//, this.txtDepartment.Text, this.txtSection.Text

            this.gvEquip.DataSource = dtEG;
            this.gvEquip.DataBind();
        }
        protected void gvEquip_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvEquip.Rows.Count > 0)
            {
                gvEquip.UseAccessibleHeader = true;
                gvEquip.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvEquip_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.myModal_Equip.Style.Add("display", "block");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            gvEquip.PageIndex = e.NewPageIndex;
            BindGridView_Equip();
        }
        protected void dtEquipDoc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.myModal_Doc.Style.Add("display", "block");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            dtEquipDoc.PageIndex = e.NewPageIndex;
            bindDoc_Data();
        }
        #endregion

        #region " Add New Record "
        private void initialDataTable()
        {
            DataTable dt = new DataTable();
            //dt.Columns.AddRange(new DataColumn[12] {new DataColumn("department_name"), new DataColumn("section_name"), new DataColumn("bomlevel_name")
            //                                        , new DataColumn("bomlevel_code"), new DataColumn("item_code"), new DataColumn("item_name")
            //                                        , new DataColumn("item_legend"), new DataColumn("item_relate"), new DataColumn("equip_qty")
            //                                        , new DataColumn("equip_unit") , new DataColumn("equip_buyinglot") , new DataColumn("equip_reserve") });
            dt.Columns.AddRange(new DataColumn[4] {  new DataColumn("item_code"), new DataColumn("item_name"), new DataColumn("item_type")
                                                        , new DataColumn("item_legend")});
            Session.Add("dtEqGroup", dt);
        }
        private void initialDataTableDoc()
        {
            DataTable dt = new DataTable();

            dt.Columns.AddRange(new DataColumn[4] {  new DataColumn("doc_name"), new DataColumn("doc_revision_display"), new DataColumn("doc_desc")
                                                        , new DataColumn("doc_nodeid")});
            Session.Add("dtEqGroupDoc", dt);
        }
        protected void btnGetSelected_Click(object sender, EventArgs e)
        {
            string strEquipList = "";
            this.myModal_Equip.Style.Add("display", "block");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            if (Session["dtEqGroup"] == null)
                this.initialDataTable();

            DataTable dtEquip = (DataTable)Session["dtEqGroup"];
            if (dtEquip.Columns.Count == 0)
            {
                //dtEquip.Columns.AddRange(new DataColumn[12] {new DataColumn("department_name"), new DataColumn("section_name"), new DataColumn("bomlevel_name")
                //                                        , new DataColumn("bomlevel_code"), new DataColumn("item_code"), new DataColumn("item_name")
                //                                        , new DataColumn("item_legend"), new DataColumn("item_relate"), new DataColumn("equip_qty")
                //                                        , new DataColumn("equip_unit") , new DataColumn("equip_buyinglot") , new DataColumn("equip_reserve") });
                dtEquip.Columns.AddRange(new DataColumn[4] { new DataColumn("item_code"), new DataColumn("item_name"), new DataColumn("item_type")
                                                        , new DataColumn("item_legend")});
            }
            #region
            //foreach (GridViewRow row in gvMyEquip1.Rows)
            //{
            //    if (row.RowType == DataControlRowType.DataRow)
            //    {
            //        Label lblitem_code = (Label)row.FindControl("gvMyEquip_lblitem_code");

            //        TextBox txtQty = (TextBox)row.FindControl("gvMyEquip_txtQty");
            //        TextBox txtUnit = (TextBox)row.FindControl("gvMyEquip_txtUnit");

            //        TextBox txtBuyingLot = (TextBox)row.FindControl("gvMyEquip_txtBuyingLot");
            //        TextBox txtReservasion = (TextBox)row.FindControl("gvMyEquip_txtReservasion");

            //        DataRow[] drSelect = dtEquip.Select("item_code='" + lblitem_code.Text + "'");
            //        if (drSelect.Length > 0)
            //        {
            //            drSelect[0]["equip_qty"] = (txtQty.Text == "" ? "0" : txtQty.Text);
            //            drSelect[0]["equip_unit"] = txtUnit.Text;
            //            drSelect[0]["equip_buyinglot"] = (txtBuyingLot.Text == "" ? "1" : txtBuyingLot.Text);
            //            drSelect[0]["equip_reserve"] = (txtReservasion.Text == "" ? "0" : txtReservasion.Text);
            //        }
            //        dtEquip.AcceptChanges();

            //        strEquipList += (strEquipList == "" ? "" : ",") + lblitem_code.Text;
            //    }
            //}
            #endregion
            string[] selectEquip = this.hidSelectEquip.Value.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string equipData in selectEquip)
            {
                if (equipData != "")
                {
                    string new_equipData = equipData.Replace("|", "");

                    string[] equip = new_equipData.Split('+');
                    //string department = equip[0].ToString().Trim();
                    //string section = equip[1].ToString().Trim();
                    //string bomlevel_name = equip[2].ToString().Trim();
                    //string bomlevel_code = equip[3].ToString().Trim();
                    string item_code = equip[0].ToString().Trim();
                    string item_name = equip[1].ToString().Trim();
                    string item_type = equip[2].ToString().Trim();
                    string item_legend = equip[3].ToString().Trim();
                    //string item_relate = equip[7].ToString().Trim();

                    DataRow[] drSelect = dtEquip.Select("item_code='" + item_code + "'");
                    if (drSelect.Length == 0)
                    {
                        DataRow newRow = dtEquip.NewRow();
                        //newRow["department_name"] = department;
                        //newRow["section_name"] = section;
                        //newRow["bomlevel_name"] = bomlevel_name;
                        //newRow["bomlevel_code"] = bomlevel_code;

                        newRow["item_code"] = item_code;
                        newRow["item_name"] = item_name;
                        newRow["item_type"] = item_type;
                        newRow["item_legend"] = item_legend;
                        //newRow["item_relate"] = item_relate;
                        //newRow["equip_qty"] = 0;
                        //newRow["equip_unit"] = "";
                        //newRow["equip_buyinglot"] = 1;
                        //newRow["equip_reserve"] = 0;
                        dtEquip.Rows.Add(newRow);
                        dtEquip.AcceptChanges();

                        strEquipList += (strEquipList == "" ? "" : ",") + item_code;
                    }
                }
            }

            gvMyEquip1.DataSource = dtEquip;
            gvMyEquip1.DataBind();
            Session["dtEqGroup"] = dtEquip;
            myModal_Doc.Style["display"] = "none";
            // DataTable dtEquip_Doc = objBOM.selectDocByEquipCode(strEquipList);
            // DataTable dtEquip_Doc = new DataTable();//

            //if (this.lstBOMLevel.SelectedValue == "2-ZCM")
            //this.tbMisc.Style.Add("display", "block");
            //else
            //this.tbMisc.Style.Add("display", "none");
        }
        protected void btAttsave_Click(object sender, EventArgs e)
        {
            string strEquipListDoc = "";
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            if (Session["dtEqGroupDoc"] == null)
                this.initialDataTableDoc();
            DataTable dtEquip_Doc = (DataTable)Session["dtEqGroupDoc"];
            if (dtEquip_Doc.Columns.Count == 0)
            {
                dtEquip_Doc.Columns.AddRange(new DataColumn[4] {  new DataColumn("doc_name"), new DataColumn("doc_revision_display"), new DataColumn("doc_desc")
                                                        , new DataColumn("doc_nodeid")});
            }
            string[] selectEquipDoc = this.hidSelectEquipDoc.Value.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string equipData in selectEquipDoc)
            {
                if (equipData != "")
                {
                    string new_equipData = equipData.Replace("|", "");

                    string[] equip = new_equipData.Split('+');
                    string doc_name = equip[0].ToString().Trim();
                    string doc_revision_display = equip[1].ToString().Trim();
                    string doc_desc = equip[2].ToString().Trim();
                    string doc_nodeid = equip[3].ToString().Trim();

                    DataRow[] drSelect = dtEquip_Doc.Select("doc_nodeid='" + doc_nodeid + "'");
                    if (drSelect.Length == 0)
                    {
                        DataRow newRow = dtEquip_Doc.NewRow();
                        newRow["doc_name"] = doc_name;
                        newRow["doc_revision_display"] = doc_revision_display;
                        newRow["doc_desc"] = doc_desc;
                        newRow["doc_nodeid"] = doc_nodeid;
                        dtEquip_Doc.Rows.Add(newRow);
                        dtEquip_Doc.AcceptChanges();

                        strEquipListDoc += (strEquipListDoc == "" ? "" : ",") + doc_nodeid;
                    }
                }
            }

            this.gvDocList.DataSource = dtEquip_Doc;
            this.gvDocList.DataBind();
            myModal_Doc.Style["display"] = "block";
            myModal_Equip.Style["display"] = "none";
           
        }
        protected void gvMyEquip1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvMyEquip1.Rows.Count > 0)
            {
                gvMyEquip1.UseAccessibleHeader = true;
                gvMyEquip1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string item = e.Row.Cells[3].Text;
                foreach (Button button in e.Row.Cells[4].Controls.OfType<Button>())
                {
                    if (button.CommandName == "Delete")
                    {
                        button.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item + "?')){ return false; };";
                    }
                }
            }
        }
        protected void gvMyEquip1_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dtEquip = (DataTable)Session["dtEqGroup"];
            //dtEquip.Rows[index].Delete();
            GridViewRow row = (GridViewRow)gvMyEquip1.Rows[e.RowIndex];
            Label lblitem_code = (Label)row.FindControl("gvMyEquip_lblitem_code");
            if (hidEq_Group.Value != "")
            {
                if (hidEq_Code.Value == "") hidEq_Code.Value = "'" + lblitem_code.Text + "'";
                else hidEq_Code.Value += ",'" + lblitem_code.Text + "'";

                //objEquipGroup.delete_EquipList(hidEq_Group.Value, lblitem_code.Text);
                //DataTable dt = objEquipGroup.select_Equiplist(hidEq_Group.Value); 
                //Session["dtEqGroup"] = dt;
                //gvMyEquip1.DataSource = dt;
                //gvMyEquip1.DataBind();

                dtEquip.Rows.RemoveAt(index);
                Session["dtEqGroup"] = dtEquip;
                gvMyEquip1.DataSource = dtEquip;
                gvMyEquip1.DataBind();
            }
            else
            {
        
                dtEquip.Rows.RemoveAt(index);
                Session["dtEqGroup"] = dtEquip;
                gvMyEquip1.DataSource = dtEquip;
                gvMyEquip1.DataBind();
            }
           
            
            //string strEquipList = (dtBOM.Rows.Count > 0 ? String.Join(",", dtBOM.AsEnumerable().Select(x => x.Field<string>("item_code").ToString()).ToArray()) : "xxx");



            //DataTable dtEquip_Doc = objBOM.selectDocByEquipCode(strEquipList);
            //DataTable dtEquip_Doc = new DataTable();//objBOM.selectDocByEquipCode(strEquipList);
            //this.gvDocList.DataSource = dtEquip_Doc;
            //this.gvDocList.DataBind();
            myModal_Doc.Style["display"] = "none";
            myModal_Equip.Style["display"] = "none";
        }
        protected void gvDocList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dtDoc = (DataTable)Session["dtEqGroupDoc"];
            GridViewRow row = (GridViewRow)gvDocList.Rows[e.RowIndex];
            HiddenField lblnodeid = (HiddenField)row.FindControl("gvDocList_hid_nodeid");
            if (hidEq_Group.Value != "")
            {
               
                if (hidDoc_nodeid.Value == "") hidDoc_nodeid.Value = "'" + lblnodeid.Value + "'";
                else hidDoc_nodeid.Value += ",'" + lblnodeid.Value + "'";

                dtDoc.Rows.RemoveAt(index);
                Session["dtEqGroupDoc"] = dtDoc;
                gvDocList.DataSource = dtDoc;
                gvDocList.DataBind();
                //objEquipGroup.delete_DocList(hidEq_Group.Value, lblnodeid.Value);
                //DataTable dt = objEquipGroup.select_Doclist(hidEq_Group.Value);
                //Session["dtEqGroupDoc"] = dt;
                //gvDocList.DataSource = dt;
                //gvDocList.DataBind();
            }
            else
            {
                dtDoc.Rows.RemoveAt(index);
                Session["dtEqGroupDoc"] = dtDoc;
                gvDocList.DataSource = dtDoc;
                gvDocList.DataBind();
            }
           
            myModal_Doc.Style["display"] = "none";
            myModal_Equip.Style["display"] = "none";
        }
        protected void gvDocList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvDocList.Rows.Count > 0)
            {
               
                gvDocList.UseAccessibleHeader = true;
                gvDocList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void btn_insert_Click(object sender, EventArgs e)
        {
            this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            try
            {
                if (txtEG_Name.Text != "")
                {
                    string strEquipValue = "";
                    foreach (GridViewRow g1 in gvMyEquip1.Rows)
                    {
                        Label lblitem_code = (Label)g1.FindControl("gvMyEquip_lblitem_code");
                        Label lblitem_type = (Label)g1.FindControl("gvMyEquip_lblBOM_Level");
                        Label lblitem_name = (Label)g1.FindControl("gvMyEquip_lblitem_name");
                        Label lbllegend = (Label)g1.FindControl("gvMyEquip_lbllegend");
                        //HiddenField hidBOM_Level_Code = (HiddenField)g1.FindControl("gvEquip_hidBOM_Level_Code");

                        //TextBox txtQty = (TextBox)g1.FindControl("gvMyEquip_txtQty");
                        //TextBox txtUnit = (TextBox)g1.FindControl("gvMyEquip_txtUnit");

                        //TextBox txtBuyingLot = (TextBox)g1.FindControl("gvMyEquip_txtBuyingLot");
                        //TextBox txtReservasion = (TextBox)g1.FindControl("gvMyEquip_txtReservasion");

                        //strEquipValue += (strEquipValue == "" ? "" : "|") + lblitem_code.Text + "_" + hidBOM_Level_Code.Value + "_" + txtQty.Text + "_" + txtUnit.Text + "_" + txtBuyingLot.Text + "_" + txtReservasion.Text;
                        //strEquipValue += (strEquipValue == "" ? "" : "|") + lblitem_code.Text + "_" + lblitem_name.Text + "_" + lblitem_type.Text + "_" + lbllegend.Text;
                      
                        strEquipValue += (strEquipValue == "" ? "" : "|") + lblitem_code.Text;
                    }

                    string strDocValue = "";
                    foreach (GridViewRow gDoc in gvDocList.Rows)
                    {
                        HiddenField hid_nodeid = (HiddenField)gDoc.FindControl("gvDocList_hid_nodeid");
                        Label lbldoc_name = (Label)gDoc.FindControl("gvDocList_lbldoc_name");
                        strDocValue += (strDocValue == "" ? "" : "|") + hid_nodeid.Value + "@" + lbldoc_name.Text;
                    }

                    //objBOM.insertBOM(txtBomName.Text, txtDesc.Text, txtDepartment.Text, txtSection.Text
                    //                 , this.lstBOMLevel.SelectedValue, this.txtLegend.Text.Trim(), this.ddlCat.SelectedValue, this.txtmiscell.Text.Trim()
                    //                 , strEquipValue, strDocValue, hidLogin.Value);
                    objEquipGroup.insertEquipGroup(txtEG_Name.Text, "", strEquipValue, strDocValue, hidLogin.Value, hidEq_Group.Value,DDL_Method.SelectedValue,DDL_Egname.SelectedValue);
                    if (hidEq_Code.Value != "")
                    {
                        objEquipGroup.delete_EquipList(hidEq_Group.Value, hidEq_Code.Value);
                    }
                    if (hidDoc_nodeid.Value != "")
                    {
                        objEquipGroup.delete_DocList(hidEq_Group.Value, hidDoc_nodeid.Value);
                    }
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);

                    //this.bindBOMData("");
                }
                else
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Equipment Group Name.');", true);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            this.myModal_Equip.Style.Add("display", "none");
            this.myModal_Doc.Style.Add("display", "none");
            this.add_data.Style.Add("display", "none");
            this.show_data.Style.Add("display", "block");
            bindEG_Data("");
            txtEG_Name.Text = "";
            hidEq_Group.Value = "";
            hidEq_Code.Value = "";
            hidDoc_nodeid.Value = "";
        }
        #endregion

        #region " View Equip Group Data "
        private void bindEG_Data(string strFilter)
        {
            //DataTable dtData = objBOM.searchBOM(strFilter, this.txtDepartment.Text, this.txtSection.Text);
            DataTable dtData = objEquipGroup.getEquipGroup_Search(strFilter);
            this.gvEGList.DataSource = dtData;
            this.gvEGList.DataBind();
        }
        protected void btnSearchEG_Click(object sender, EventArgs e)
        {
            if (this.hidIsFirstRun.Value == "t")
            {
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);
                //this.txtDepartment.Text = emp.ORGSNAME4;
                //this.txtSection.Text = emp.ORGSNAME5;

                this.hidIsFirstRun.Value = "f";
            }
            myModal_Equip.Style["display"] = "none";
            myModal_Doc.Style["display"] = "none";
            add_data.Style["display"] = "none";
            bindEG_Data(txtSearch.Text.Trim());
            show_data.Style["display"] = "block";
            //this.bindBOMData(this.txtSearch.Text.Trim());
        }
        protected void gvEGList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvEGList.Rows.Count > 0)
            {
                gvEGList.UseAccessibleHeader = true;
                gvEGList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvEGList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "editdata")
            {
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvEGList = (GridView)sender;

                //string xBOMCode = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidBOMCode")).Value;
                //string xBOMDesc = ((Label)gvBOMList.Rows[xrowid].FindControl("gvBOMList_lblBOMDesc")).Text;
                //string xBOMLevel = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidBOMLevel")).Value;
                //string xLegend = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidLegend")).Value;
                //string xCate_Code = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidCate_Code")).Value;
                //string xMisc_Per_Unit = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidMisc_Per_Unit")).Value;

                string lblEG_Code = ((Label)gvEGList.Rows[xrowid].FindControl("gvEGList_lblEG_Code")).Text;

                this.loadEqData(lblEG_Code);
            }
        }
        private void loadEqData(string EG_Code)
        {
            this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");
            
            //this.txtBomName.Text = strBOMName;
            //this.txtDesc.Text = strBOMDesc;
            //this.lstBOMLevel.SelectedValue = strBOMLevel;
            ////if (strBOMLevel  == "2-ZCM")
            ////this.tbMisc.Style.Add("display", "block");
            ////else
            ////this.tbMisc.Style.Add("display", "none");

            //this.txtLegend.Text = strLegend;
            //this.ddlCat.SelectedValue = strCate_Code;
            //this.txtmiscell.Text = strMisc_Per_Unit;

            this.loadEquipList(EG_Code);
        }
        private void loadEquipList(string strEqCode)
        {
            //DataTable dtEquipList = objBOM.selectBOM_EquipList(strBOMCode);
            
            //DataTable dtEquipList = new DataTable();//objBOM.selectBOM_EquipList(strBOMCode);
            DataTable dtEquipList = objEquipGroup.select_Equiplist(strEqCode);
            this.gvMyEquip1.DataSource = dtEquipList;
            this.gvMyEquip1.DataBind();

            // DataTable dtBOM_DocList = objBOM.selectBOM_DocList(strBOMCode);
            //DataTable dtEq_DocList = new DataTable(); //objBOM.selectBOM_DocList(strBOMCode);
            DataTable dtEq_DocList = objEquipGroup.select_Doclist(strEqCode);
            this.gvDocList.DataSource = dtEq_DocList;
            this.gvDocList.DataBind();
            DataTable dtEq_Gruop = objEquipGroup.getEquipGroup_edit(strEqCode);
            if (dtEq_Gruop.Rows.Count > 0)
            {
                txtEG_Name.Text = dtEq_Gruop.Rows[0]["eg_name"].ToString();
            }
            hidEq_Group.Value = strEqCode;
            Session["dtEqGroup"] = dtEquipList;
            Session["dtEqGroupDoc"] = dtEq_DocList;
        }
        #endregion

        protected void btnAddEG_Click(object sender, EventArgs e)
        {
            this.myModal_Equip.Style.Add("display", "none");
            this.myModal_Doc.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");
            txtEG_Name.Text = "";
            this.loadEquipList("");
        }
        private void DDLbinddata()
        {
            DataTable dt = new DataTable();
            dt = objEquipGroup.getDDLDcoTypeEquip();
            DDLDoctype.DataSource = dt;
            DDLDoctype.DataTextField = "doctype_name";
            DDLDoctype.DataValueField = "doctype_code";
            DDLDoctype.DataBind();
            DDLDoctype.Items.Insert(0, new ListItem("<--- All Type --->", ""));

            DataTable dtEgName = objEquipGroup.getEquipName();
            DDL_Egname.DataSource = dtEgName;
            DDL_Egname.DataTextField = "eg_name";
            DDL_Egname.DataValueField = "eg_name";
            DDL_Egname.DataBind();
        }
        protected void DDLDoctype_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtAdddoc = objEquipGroup.GetAttachEquip();
            DataView dv = new DataView(dtAdddoc);
            if (DDLDoctype.SelectedValue != "") dv.RowFilter = "doctype_code ='" + DDLDoctype.SelectedValue + "'";
           
            if (dv.Count > 0)
            {
                dtEquipDoc.DataSource = dv;
                dtEquipDoc.DataBind();
            }
            else
            {
                dtEquipDoc.DataSource = null;
                dtEquipDoc.DataBind();
            }
            myModal_Doc.Style["display"] = "block";
            myModal_Equip.Style["display"] = "none";

        }
        private void bindDoc_Data()
        {
            DataTable dtAtt = objEquipGroup.GetAttachEquip();
            if (dtAtt.Rows.Count > 0)
            {
                dtEquipDoc.DataSource = dtAtt;
                dtEquipDoc.DataBind();
            }
            else
            {
                dtEquipDoc.DataSource = null;
                dtEquipDoc.DataBind();
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

    }
}