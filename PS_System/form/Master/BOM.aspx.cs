﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class Bom : System.Web.UI.Page
    {
        public clsBom objBOM = new clsBom();
        public string key_Decrypt = (ConfigurationManager.AppSettings["key_Decrypt"].ToString());

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidATOS.Value = key_Decrypt; //String.Join(",", Encoding.UTF8.GetBytes(key_Decrypt)); //
                initialDataTable();
                BindGridView_Equip();
                loadBOMLevel();
                //loadCategory();



                this.lstBOMLevel.SelectedValue = "ZCM";

                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z594600";

                this.hidIsFirstRun.Value = "t";

                this.btnSearchBOM_Click(sender, e);
                bindDoc_Data();
                DDLbinddata();
                Session["dtEqGroupDoc"] = "";

                //Page.ClientScript.RegisterStartupScript(this.GetType(), "loadSessionData", "loadSessionData()", true);
            }
            //if (this.gvEquip.Rows.Count > 0)
            //{
            //    gvEquip.UseAccessibleHeader = true;
            //    gvEquip.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
            //if (this.gvMyEquip1.Rows.Count > 0)
            //{
            //    gvMyEquip1.UseAccessibleHeader = true;
            //    gvMyEquip1.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
            //if (this.gvBOMList.Rows.Count > 0)
            //{
            //    gvBOMList.UseAccessibleHeader = true;
            //    gvBOMList.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
        }

        #region " Search Equipment List "
        protected void btnSearch1_Click(object sender, EventArgs e)
        {
            //this.myModal_Equip.Style.Add("display", "block");
            this.myModal_Doc.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");
            dvEquipExp.Style["display"] = "block";
            dvEquip.Style["display"] = "block";
            BindGridView_Equip();
        }
        private void BindGridView_Equip()
        {
            DataTable dtBom = new DataTable();
            if ((this.ddlBOMLevel_Search.SelectedValue == "0-EQI") || (this.ddlBOMLevel_Search.SelectedValue == "2-ZCM"))//0-SVR //eq 2-ZCM
                dtBom = objBOM.getEquipInfo(this.txtSearchEquip.Text, this.txtDepartment.Text, this.txtSection.Text, this.ddlBOMLevel_Search.SelectedValue);
            else
                dtBom = objBOM.getBOMInfo_Search(this.txtSearchEquip.Text, this.ddlBOMLevel_Search.SelectedValue, this.txtDepartment.Text, this.txtSection.Text); //bom
            if (ddlBOMLevel_Search.SelectedValue == "0-EQI") txtSearchEquip.Attributes.Add("placeholder", "Equipment Name or Description");
            else txtSearchEquip.Attributes.Add("placeholder", hidSearch.Value);
            this.gvEquip.DataSource = dtBom;
            this.gvEquip.DataBind();
        }
        protected void gvEquip_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvEquip.Rows.Count > 0)
            {
                gvEquip.UseAccessibleHeader = true;
                gvEquip.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvEquip_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //this.myModal_Equip.Style.Add("display", "block");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            gvEquip.PageIndex = e.NewPageIndex;
            BindGridView_Equip();
        }
        protected void dtEquipDoc_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.myModal_Doc.Style.Add("display", "block");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            dtEquipDoc.PageIndex = e.NewPageIndex;
            bindDoc_Data();
        }
        #endregion

        #region " Add New Record "
        private void loadBOMLevel()
        {
            DataTable dtmat = new DataTable();
            dtmat.Columns.Add("CODE", typeof(string));
            dtmat.Columns.Add("TEXT", typeof(string));
            dtmat.Rows.Add("1-ZNV", "Major");
            //dtmat.Rows.Add("2-ZCM", "Miscellensous");
            dtmat.Rows.Add("3-ASM", "Assembly");
            dtmat.Rows.Add("4-MOD", "Module");
            lstBOMLevel.DataSource = dtmat;
            lstBOMLevel.DataValueField = "CODE";
            lstBOMLevel.DataTextField = "TEXT";
            lstBOMLevel.DataBind();

            ddlBOM.DataSource = dtmat;
            ddlBOM.DataValueField = "CODE";
            ddlBOM.DataTextField = "TEXT";
            ddlBOM.DataBind();
        }

        //private void loadCategory()
        //{
        //    DataTable dtgrp = new DataTable();
        //    dtgrp.Columns.Add("CODE", typeof(string));
        //    dtgrp.Columns.Add("NAME", typeof(string));
        //    dtgrp.Rows.Add("", "");
        //    dtgrp.Rows.Add("500 kV Suspension Insulator", "500 kV Suspension Insulator");
        //    dtgrp.Rows.Add("AL Tube and Miscellaneous Hardware", "AL Tube and Miscellaneous Hardware");
        //    dtgrp.Rows.Add("Bus Fitting", "Bus Fitting");
        //    dtgrp.Rows.Add("Conductor", "Conductor");
        //    dtgrp.Rows.Add("Connector and Miscelleous Hardware for Suspension", "Connector and Miscelleous Hardware for Suspension");
        //    dtgrp.Rows.Add("Grouding Material", "Grouding Material");
        //    dtgrp.Rows.Add("Insulator", "Insulator");
        //    dtgrp.Rows.Add("Rigid Steel Conduit", "Rigid Steel Conduit");
        //    ddlCat.DataSource = dtgrp;
        //    ddlCat.DataValueField = "CODE";
        //    ddlCat.DataTextField = "NAME";
        //    ddlCat.DataBind();
        //}

        private void initialDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[12] {new DataColumn("department_name"), new DataColumn("section_name"), new DataColumn("bomlevel_name")
                                                    , new DataColumn("bomlevel_code"), new DataColumn("item_code"), new DataColumn("item_name")
                                                    , new DataColumn("item_legend"), new DataColumn("item_relate"), new DataColumn("equip_qty")
                                                    , new DataColumn("equip_unit") , new DataColumn("equip_buyinglot") , new DataColumn("equip_reserve") });
            Session.Add("dtBOM", dt);
        }
        private void initialDataTableDoc()
        {
            DataTable dt = new DataTable();

            dt.Columns.AddRange(new DataColumn[4] {  new DataColumn("doc_name"), new DataColumn("doc_revision_display"), new DataColumn("doc_desc")
                                                        , new DataColumn("doc_nodeid")});
            Session.Add("dtEqGroupDoc", dt);
        }
        protected void btnGetSelected_Click(object sender, EventArgs e)
        {
            string strEquipList = "";
            //this.myModal_Equip.Style.Add("display", "block");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            if (Session["dtBOM"] == null)
                this.initialDataTable();

            DataTable dtBOM = (DataTable)Session["dtBOM"];
            if (dtBOM.Columns.Count == 0)
            {
                dtBOM.Columns.AddRange(new DataColumn[12] {new DataColumn("department_name"), new DataColumn("section_name"), new DataColumn("bomlevel_name")
                                                        , new DataColumn("bomlevel_code"), new DataColumn("item_code"), new DataColumn("item_name")
                                                        , new DataColumn("item_legend"), new DataColumn("item_relate"), new DataColumn("equip_qty")
                                                        , new DataColumn("equip_unit") , new DataColumn("equip_buyinglot") , new DataColumn("equip_reserve") });
            }

            foreach (GridViewRow row in gvMyEquip1.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    Label lblitem_code = (Label)row.FindControl("gvMyEquip_lblitem_code");

                    TextBox txtQty = (TextBox)row.FindControl("gvMyEquip_txtQty");
                    TextBox txtUnit = (TextBox)row.FindControl("gvMyEquip_txtUnit");

                    TextBox txtBuyingLot = (TextBox)row.FindControl("gvMyEquip_txtBuyingLot");
                    TextBox txtReservasion = (TextBox)row.FindControl("gvMyEquip_txtReservasion");

                    DataRow[] drSelect = dtBOM.Select("item_code='" + lblitem_code.Text + "'");
                    if (drSelect.Length > 0)
                    {
                        drSelect[0]["equip_qty"] = (txtQty.Text == "" ? "0" : txtQty.Text);
                        drSelect[0]["equip_unit"] = txtUnit.Text;
                        drSelect[0]["equip_buyinglot"] = (txtBuyingLot.Text == "" ? "1" : txtBuyingLot.Text);
                        drSelect[0]["equip_reserve"] = (txtReservasion.Text == "" ? "0" : txtReservasion.Text);
                    }
                    dtBOM.AcceptChanges();

                    strEquipList += (strEquipList == "" ? "" : ",") + lblitem_code.Text;
                }
            }

            string[] selectEquip = this.hidSelectEquip.Value.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);
            foreach (string equipData in selectEquip)
            {
                if (equipData != "")
                {
                    string new_equipData = equipData.Replace("|", "");

                    string[] equip = new_equipData.Split('+');
                    string department = equip[0].ToString().Trim();
                    string section = equip[1].ToString().Trim();
                    //if (ddlBOMLevel_Search.SelectedValue == "3-ASM")  bomlevel_name = "Assembly";
                    //else  bomlevel_name = equip[8].ToString().Trim() == "Major" ? "Equipment" : equip[8].ToString().Trim();

                    string bomlevel_name = ddlBOMLevel_Search.SelectedItem.Text;
                    string bomlevel_code = equip[3].ToString().Trim();
                    string item_code = equip[4].ToString().Trim();
                    string item_name = equip[5].ToString().Trim();
                    string item_legend = equip[6].ToString().Trim();
                    string item_relate = equip[7].ToString().Trim();

                    DataRow[] drSelect = dtBOM.Select("item_code='" + item_code + "'");
                    if (drSelect.Length == 0)
                    {
                        DataRow newRow = dtBOM.NewRow();
                        newRow["department_name"] = department;
                        newRow["section_name"] = section;
                        newRow["bomlevel_name"] = bomlevel_name;
                        newRow["bomlevel_code"] = bomlevel_code;
                        newRow["item_code"] = item_code;
                        if (bomlevel_name == "Assembly")
                        {
                            newRow["item_name_assem"] = item_name;
                            newRow["item_legend_assem"] = item_legend;
                        }
                        else
                        {
                            newRow["item_name"] = item_name;
                            newRow["item_legend"] = item_legend;
                        }


                        newRow["equip_qty"] = 0;
                        newRow["equip_unit"] = "";
                        newRow["equip_buyinglot"] = 1;
                        newRow["equip_reserve"] = 0;
                        dtBOM.Rows.Add(newRow);

                        dtBOM.AcceptChanges();

                        strEquipList += (strEquipList == "" ? "" : ",") + item_code;
                    }
                }
            }

            gvMyEquip1.DataSource = dtBOM;
            gvMyEquip1.DataBind();
            Session["dtBOM"] = dtBOM;
            myModal_Doc.Style["display"] = "none";
            //DataTable dtEquip_Doc = objBOM.selectDocByEquipCode(strEquipList);
            //this.gvDocList.DataSource = dtEquip_Doc;
            //this.gvDocList.DataBind();

            //if (this.lstBOMLevel.SelectedValue == "2-ZCM")
            //this.tbMisc.Style.Add("display", "block");
            //else
            //this.tbMisc.Style.Add("display", "none");
        }
        protected void btAttsave_Click(object sender, EventArgs e)
        {
            string strEquipListDoc = "";
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            if (Session["dtEqGroupDoc"] == null)
                this.initialDataTableDoc();
            DataTable dtEquip_Doc = (DataTable)Session["dtEqGroupDoc"];
            if (dtEquip_Doc.Columns.Count == 0)
            {
                dtEquip_Doc.Columns.AddRange(new DataColumn[4] {  new DataColumn("doc_name"), new DataColumn("doc_revision_display"), new DataColumn("doc_desc")
                                                        , new DataColumn("doc_nodeid")});
            }
            string[] selectEquipDoc = this.hidSelectEquipDoc.Value.Split(new string[] { "||" }, StringSplitOptions.RemoveEmptyEntries);

            foreach (string equipData in selectEquipDoc)
            {
                if (equipData != "")
                {
                    string new_equipData = equipData.Replace("|", "");

                    string[] equip = new_equipData.Split('+');
                    string doc_name = equip[0].ToString().Trim();
                    string doc_revision_display = equip[1].ToString().Trim();
                    string doc_desc = equip[2].ToString().Trim();
                    string doc_nodeid = equip[3].ToString().Trim();

                    DataRow[] drSelect = dtEquip_Doc.Select("doc_nodeid='" + doc_nodeid + "'");
                    if (drSelect.Length == 0)
                    {
                        DataRow newRow = dtEquip_Doc.NewRow();
                        newRow["doc_name"] = doc_name;
                        newRow["doc_revision_display"] = doc_revision_display;
                        newRow["doc_desc"] = doc_desc;
                        newRow["doc_nodeid"] = doc_nodeid;
                        dtEquip_Doc.Rows.Add(newRow);
                        dtEquip_Doc.AcceptChanges();

                        strEquipListDoc += (strEquipListDoc == "" ? "" : ",") + doc_nodeid;
                    }
                }
            }

            this.gvDocList.DataSource = dtEquip_Doc;
            this.gvDocList.DataBind();
            myModal_Doc.Style["display"] = "block";
            //myModal_Equip.Style["display"] = "none";

        }
        protected void gvMyEquip1_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvMyEquip1.Rows.Count > 0)
            {
                gvMyEquip1.UseAccessibleHeader = true;
                gvMyEquip1.HeaderRow.TableSection = TableRowSection.TableHeader;
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label gvMyEquip_lbllegend = (Label)e.Row.FindControl("gvMyEquip_lbllegend");
                Label gvMyEquip_lblitem_name = (Label)e.Row.FindControl("gvMyEquip_lblitem_name");
                if (drv["bomlevel_name"].ToString() == "Assembly")
                {
                    gvMyEquip_lblitem_name.Text = drv["item_name_assem"].ToString();
                    gvMyEquip_lbllegend.Text = drv["item_legend_assem"].ToString();
                }
                else
                {
                    gvMyEquip_lblitem_name.Text = drv["item_name"].ToString();
                    gvMyEquip_lbllegend.Text = drv["item_legend"].ToString();
                }


                string item = e.Row.Cells[3].Text;
                foreach (Button button in e.Row.Cells[11].Controls.OfType<Button>())
                {
                    if (button.CommandName == "Delete")
                    {
                        button.Attributes["onclick"] = "if(!confirm('Do you want to delete " + item + "?')){ return false; };";
                    }
                }
            }
        }
        protected void gvMyEquip1_OnRowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dtBOM = (DataTable)Session["dtBOM"];

            //string strEquipList = "";
            GridViewRow row = (GridViewRow)gvMyEquip1.Rows[e.RowIndex];
            Label lblitem_code = (Label)row.FindControl("gvMyEquip_lblitem_code");

            if (hidBom_Group.Value != "")
            {
                if (hidEq_Code.Value == "") hidEq_Code.Value = "'" + lblitem_code.Text + "'";
                else hidEq_Code.Value += ",'" + lblitem_code.Text + "'";
                dtBOM.Rows.RemoveAt(index);

                Session["dtEqGroup"] = dtBOM;
                gvMyEquip1.DataSource = dtBOM;
                gvMyEquip1.DataBind();
            }
            else
            {

                dtBOM.Rows[index].Delete();
                Session["dtBOM"] = dtBOM;

                //string strEquipList = (dtBOM.Rows.Count > 0 ? String.Join(",", dtBOM.AsEnumerable().Select(x => x.Field<string>("item_code").ToString()).ToArray()) : "xxx");

                gvMyEquip1.DataSource = dtBOM;
                gvMyEquip1.DataBind();
            }

            myModal_Doc.Style["display"] = "none";
            //myModal_Equip.Style["display"] = "none";
            //DataTable dtEquip_Doc = objBOM.selectDocByEquipCode(hidEq_Code.Value);
            //this.gvDocList.DataSource = dtEquip_Doc;
            //this.gvDocList.DataBind();
        }
        protected void gvDocList_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dtDoc = (DataTable)Session["dtEqGroupDoc"];
            GridViewRow row = (GridViewRow)gvDocList.Rows[e.RowIndex];
            HiddenField lblnodeid = (HiddenField)row.FindControl("gvDocList_hid_nodeid");
            if (hidBom_Group.Value != "")
            {

                if (hidDoc_nodeid.Value == "") hidDoc_nodeid.Value = "'" + lblnodeid.Value + "'";
                else hidDoc_nodeid.Value += ",'" + lblnodeid.Value + "'";

                dtDoc.Rows.RemoveAt(index);
                Session["dtEqGroupDoc"] = dtDoc;
                gvDocList.DataSource = dtDoc;
                gvDocList.DataBind();
            }
            else
            {
                dtDoc.Rows.RemoveAt(index);
                Session["dtEqGroupDoc"] = dtDoc;
                gvDocList.DataSource = dtDoc;
                gvDocList.DataBind();
            }

            myModal_Doc.Style["display"] = "none";
            //myModal_Equip.Style["display"] = "none";
        }
        protected void gvDocList_OnRowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvDocList.Rows.Count > 0)
            {
                gvDocList.UseAccessibleHeader = true;
                gvDocList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void btn_insert_Click(object sender, EventArgs e)
        {
            //this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            try
            {
                if (txtBomName.Text != "")
                {
                    string strEquipValue = "";
                    foreach (GridViewRow g1 in gvMyEquip1.Rows)
                    {
                        Label lblitem_code = (Label)g1.FindControl("gvMyEquip_lblitem_code");
                        HiddenField hidBOM_Level_Code = (HiddenField)g1.FindControl("gvEquip_hidBOM_Level_Code");

                        TextBox txtQty = (TextBox)g1.FindControl("gvMyEquip_txtQty");
                        TextBox txtUnit = (TextBox)g1.FindControl("gvMyEquip_txtUnit");

                        TextBox txtBuyingLot = (TextBox)g1.FindControl("gvMyEquip_txtBuyingLot");
                        TextBox txtReservasion = (TextBox)g1.FindControl("gvMyEquip_txtReservasion");

                        strEquipValue += (strEquipValue == "" ? "" : "|") + lblitem_code.Text + "_" + hidBOM_Level_Code.Value + "_" + txtQty.Text + "_" + txtUnit.Text + "_" + txtBuyingLot.Text + "_" + txtReservasion.Text;
                    }

                    string strDocValue = "";
                    foreach (GridViewRow gDoc in gvDocList.Rows)
                    {
                        HiddenField hid_nodeid = (HiddenField)gDoc.FindControl("gvDocList_hid_nodeid");

                        strDocValue += (strDocValue == "" ? "" : "|") + hid_nodeid.Value;
                    }

                    objBOM.insertBOM(txtBomName.Text, txtDesc.Text, txtDepartment.Text, txtSection.Text
                                     , this.lstBOMLevel.SelectedValue, this.txtLegend.Text.Trim(), "", ""
                                     , strEquipValue, strDocValue, hidLogin.Value, hidBom_Group.Value); //this.ddlCat.SelectedValue, this.txtmiscell.Text.Trim()
                    if (hidEq_Code.Value != "")
                    {
                        objBOM.delete_EquipListBom(hidBom_Group.Value, hidEq_Code.Value, txtDepartment.Text, txtSection.Text);
                    }
                    if (hidDoc_nodeid.Value != "")
                    {
                        objBOM.delete_DocListBom(hidBom_Group.Value, hidDoc_nodeid.Value);
                    }
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);

                    this.bindBOMData("", "", "");
                }
                else
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input BOM Name.');", true);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            this.myModal_Doc.Style.Add("display", "none");
            //this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "none");
            this.show_data.Style.Add("display", "block");
            hidBom_Group.Value = "";
            hidEq_Code.Value = "";
            hidDoc_nodeid.Value = "";
        }
        #endregion

        #region " View BOM Data "
        private void bindBOMData(string strFilter, string BomDesc, string BomLegend)
        {
            DataTable dtData = objBOM.searchBOM(strFilter, this.txtDepartment.Text, this.txtSection.Text, BomDesc, BomLegend, "");
            this.gvBOMList.DataSource = dtData;
            this.gvBOMList.DataBind();
        }
        protected void btnSearchBOM_Click(object sender, EventArgs e)
        {
            if (this.hidIsFirstRun.Value == "t")
            {
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);
                this.txtDepartment.Text = emp.ORGSNAME4;
                this.txtSection.Text = emp.ORGSNAME5;
                this.hidIsFirstRun.Value = "f";
            }

            this.myModal_Doc.Style.Add("display", "none");
            //this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "none");
            this.show_data.Style.Add("display", "block");
            this.bindBOMData(this.txtSearch.Text.Trim(), txtBomDesc.Text.Trim(), txtBomLegend.Text.Trim());
        }
        protected void gvBOMList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gvBOMList.Rows.Count > 0)
            {
                gvBOMList.UseAccessibleHeader = true;
                gvBOMList.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
        }
        protected void gvBOMList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "editdata")
            {
                this.ddlBOMLevel_Search.Items.Clear();
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvBOMList = (GridView)sender;

                string xBOMCode = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidBOMCode")).Value;
                string xBOMName = ((Label)gvBOMList.Rows[xrowid].FindControl("gvBOMList_lblBOMName")).Text;
                string xBOMDesc = ((Label)gvBOMList.Rows[xrowid].FindControl("gvBOMList_lblBOMDesc")).Text;
                string xBOMLevel = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidBOMLevel")).Value;
                string xLegend = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidLegend")).Value;
                string xCate_Code = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidCate_Code")).Value;
                string xMisc_Per_Unit = ((HiddenField)gvBOMList.Rows[xrowid].FindControl("gvBOMList_hidMisc_Per_Unit")).Value;

                this.loadBOMData(xBOMCode, xBOMName, xBOMDesc, xBOMLevel, xLegend, xCate_Code, xMisc_Per_Unit);
                dvEquipExp.Style["display"] = "block";
            }
        }
        private void loadBOMData(string strBOMCode, string strBOMName, string strBOMDesc, string strBOMLevel, string strLegend, string strCate_Code, string strMisc_Per_Unit)
        {
            this.myModal_Doc.Style.Add("display", "none");
            //this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");
            this.txtBomCode.Text = strBOMCode;
            this.txtBomName.Text = strBOMName;
            this.txtDesc.Text = strBOMDesc;
            this.lstBOMLevel.SelectedValue = strBOMLevel;
            if (lstBOMLevel.SelectedValue == "1-ZNV")
            {
                ListItem opt = new ListItem();
                opt.Text = "Equipment";
                opt.Value = "0-EQI";
                ddlBOMLevel_Search.Items.Add(opt);

                this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            }
            else if (lstBOMLevel.SelectedValue == "3-ASM")
            {
                ListItem opt = new ListItem();
                opt.Text = "Equipment";
                opt.Value = "0-EQI";
                ddlBOMLevel_Search.Items.Add(opt);

                ListItem optMiscellensous = new ListItem();
                optMiscellensous.Text = "Miscellensous";
                optMiscellensous.Value = "2-ZCM";
                ddlBOMLevel_Search.Items.Add(optMiscellensous);

                this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            }
            else if (lstBOMLevel.SelectedValue == "4-MOD")
            {
                ListItem opt = new ListItem();
                opt.Text = "Equipment";
                opt.Value = "0-EQI";
                ddlBOMLevel_Search.Items.Add(opt);

                ListItem optMiscellensous = new ListItem();
                optMiscellensous.Text = "Miscellensous";
                optMiscellensous.Value = "2-ZCM";
                ddlBOMLevel_Search.Items.Add(optMiscellensous);

                ListItem optAssembly = new ListItem();
                optAssembly.Text = "Assembly";
                optAssembly.Value = "3-ASM";
                ddlBOMLevel_Search.Items.Add(optAssembly);

                this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            }

            //if (strBOMLevel  == "2-ZCM")
            //this.tbMisc.Style.Add("display", "block");
            //else
            //this.tbMisc.Style.Add("display", "none");

            this.txtLegend.Text = strLegend;
            //this.ddlCat.SelectedValue = strCate_Code;
            //this.txtmiscell.Text = strMisc_Per_Unit;

            this.loadEquipList(strBOMCode);
        }
        private void loadEquipList(string strBOMCode)
        {
            DataTable dtEquipList = objBOM.selectBOM_EquipList(strBOMCode);
            this.gvMyEquip1.DataSource = dtEquipList;
            this.gvMyEquip1.DataBind();

            DataTable dtBOM_DocList = objBOM.selectBOM_DocList(strBOMCode);
            this.gvDocList.DataSource = dtBOM_DocList;
            this.gvDocList.DataBind();

            hidBom_Group.Value = strBOMCode;
            Session["dtBOM"] = dtEquipList;
            Session["dtEqGroupDoc"] = dtBOM_DocList;
        }
        #endregion

        protected void btnAddBOM_Click(object sender, EventArgs e)
        {
            this.myModal_Doc.Style.Add("display", "none");
            //this.myModal_Equip.Style.Add("display", "none");
            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");
            this.txtBomCode.Text = "";
            this.txtBomName.Text = "";

            //this.lstBOMLevel.SelectedValue = "2-ZCM";
            lstBOMLevel.SelectedValue = "1-ZNV";
            this.ddlBOMLevel_Search.Items.Clear();
            this.ddlBOMLevel_Search.Items.Add(new ListItem() { Value = "0-EQI", Text = "Equipment" });
            if (ddlBOMLevel_Search.SelectedValue == "0-EQI") txtSearchEquip.Attributes.Add("placeholder", "Equipment Name or Description");
            this.txtSearchEquip.Text = "";
            //this.ddlBOMLevel_Search.Items.Add(new ListItem() { Value = "0-SVR", Text = "Service" });
            //this.ddlBOMLevel_Search.Items.Add(new ListItem() { Value = "0-ZCM", Text = "Miscellensous" });

            //this.tbMisc.Style.Add("display", "block");
            this.txtLegend.Text = "";
            //this.txtmiscell.Text = "";
            //hidSearch.Value = "";
            this.loadEquipList("");
            dvEquipExp.Style["display"]= "block";
        }

        protected void lstBOMLevel_SelectedIndexChanged(object sender, EventArgs e)
        {
            //myModal_Equip.Style["display"] = "none";
            this.ddlBOMLevel_Search.Items.Clear();

            //this.tbMisc.Style.Add("display", "none");
            if (lstBOMLevel.SelectedValue == "1-ZNV")
            {
                ListItem opt = new ListItem();
                opt.Text = "Equipment";
                opt.Value = "0-EQI";
                ddlBOMLevel_Search.Items.Add(opt);

                //ListItem optSVR = new ListItem();
                //optSVR.Text = "Service";
                //optSVR.Value = "0-SVR";
                //ddlBOMLevel_Search.Items.Add(optSVR);

                this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            }
            //else if (lstBOMLevel.SelectedValue == "2-ZCM")
            //{
            //    //this.tbMisc.Style.Add("display", "block");

            //    ListItem opt = new ListItem();
            //    opt.Text = "Equipment";
            //    opt.Value = "0-EQI";
            //    ddlBOMLevel_Search.Items.Add(opt);

            //    ListItem optSVR = new ListItem();
            //    optSVR.Text = "Service";
            //    optSVR.Value = "0-SVR";
            //    ddlBOMLevel_Search.Items.Add(optSVR);

            //    this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            //}
            else if (lstBOMLevel.SelectedValue == "3-ASM")
            {
                ListItem opt = new ListItem();
                opt.Text = "Equipment";
                opt.Value = "0-EQI";
                ddlBOMLevel_Search.Items.Add(opt);

                //ListItem optZCM = new ListItem();
                //optZCM.Text = "Miscellensous";
                //optZCM.Value = "1-ZCM";
                //ddlBOMLevel_Search.Items.Add(optZCM);

                //ListItem optSVR = new ListItem();
                //optSVR.Text = "Service";
                //optSVR.Value = "0-SVR";
                //ddlBOMLevel_Search.Items.Add(optSVR);

                //ListItem optMajor = new ListItem();
                //optMajor.Text = "Major";
                //optMajor.Value = "1-ZNV";
                //ddlBOMLevel_Search.Items.Add(optMajor);

                ListItem optMiscellensous = new ListItem();
                optMiscellensous.Text = "Miscellensous";
                optMiscellensous.Value = "2-ZCM";
                ddlBOMLevel_Search.Items.Add(optMiscellensous);

                this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            }
            else if (lstBOMLevel.SelectedValue == "4-MOD")
            {
                ListItem opt = new ListItem();
                opt.Text = "Equipment";
                opt.Value = "0-EQI";
                ddlBOMLevel_Search.Items.Add(opt);

                //ListItem optZCM = new ListItem();
                //optZCM.Text = "Miscellensous";
                //optZCM.Value = "1-ZCM";
                //ddlBOMLevel_Search.Items.Add(optZCM);

                //ListItem optSVR = new ListItem();
                //optSVR.Text = "Service";
                //optSVR.Value = "0-SVR";
                //ddlBOMLevel_Search.Items.Add(optSVR);

                //ListItem optMajor = new ListItem();
                //optMajor.Text = "Major";
                //optMajor.Value = "1-ZNV";
                //ddlBOMLevel_Search.Items.Add(optMajor);

                ListItem optMiscellensous = new ListItem();
                optMiscellensous.Text = "Miscellensous";
                optMiscellensous.Value = "2-ZCM";
                ddlBOMLevel_Search.Items.Add(optMiscellensous);

                ListItem optAssembly = new ListItem();
                optAssembly.Text = "Assembly";
                optAssembly.Value = "3-ASM";
                ddlBOMLevel_Search.Items.Add(optAssembly);

                this.ddlBOMLevel_Search.SelectedValue = "0-EQI";
            }
            dvEquipExp.Style["display"] = "block";
            dvEquip.Style["display"] = "block";
            BindGridView_Equip();
        }
        private void DDLbinddata()
        {
            DataTable dt = new DataTable();
            dt = objBOM.getDDLDcoTypeBom();
            DDLDoctype.DataSource = dt;
            DDLDoctype.DataTextField = "doctype_name";
            DDLDoctype.DataValueField = "doctype_code";
            DDLDoctype.DataBind();
            DDLDoctype.Items.Insert(0, new ListItem("<--- All Type --->", ""));
        }
        protected void DDLDoctype_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtAdddoc = objBOM.GetAttachBom(txtDepartment.Text, txtSection.Text);
            DataView dv = new DataView(dtAdddoc);
            if (DDLDoctype.SelectedValue != "") dv.RowFilter = "doctype_code ='" + DDLDoctype.SelectedValue + "'";

            if (dv.Count > 0)
            {
                dtEquipDoc.DataSource = dv;
                dtEquipDoc.DataBind();
            }
            else
            {
                dtEquipDoc.DataSource = null;
                dtEquipDoc.DataBind();
            }
            myModal_Doc.Style["display"] = "block";
            // myModal_Equip.Style["display"] = "none";

        }
        private void bindDoc_Data()
        {
            DataTable dtAtt = objBOM.GetAttachBom(txtDepartment.Text, txtSection.Text);
            if (dtAtt.Rows.Count > 0)
            {
                dtEquipDoc.DataSource = dtAtt;
                dtEquipDoc.DataBind();
            }
            else
            {
                dtEquipDoc.DataSource = null;
                dtEquipDoc.DataBind();
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void ddlBOM_SelectedIndexChanged(object sender, EventArgs e)
        {
            myModal_Doc.Style["display"] = "none";
            //myModal_Equip.Style["display"] = "none";
            DataTable dtData = objBOM.searchBOM("", this.txtDepartment.Text, this.txtSection.Text, "", "", ddlBOM.SelectedValue);
            this.gvBOMList.DataSource = dtData;
            this.gvBOMList.DataBind();
            dvEquipExp.Style["display"] = "none";
            dvEquip.Style["display"] = "none";
            this.add_data.Style.Add("display", "none");
            this.show_data.Style.Add("display", "block");
        }
        protected void bt_closeRel_Click(object sender, EventArgs e)
        {
            myModal_Rel.Style["display"] = "none";
        }

        protected void gvEquip_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var xrowid = Convert.ToInt32(e.CommandArgument);
            GridView gvEquip = (GridView)sender;
            string relate = ((Label)gvEquip.Rows[xrowid].FindControl("gvEquip_lblitem_relate2")).Text;
            if (e.CommandName == "viewdata")
            {
                if (relate != "")
                {
                    lb_rel.Text = relate;
                }
                else
                {
                    lb_rel.Text = "-";
                }
                myModal_Rel.Style["display"] = "block";
            }
        }
    }
}