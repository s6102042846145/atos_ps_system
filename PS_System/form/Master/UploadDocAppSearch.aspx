﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadDocAppSearch.aspx.cs" Inherits="PS_System.form.Master.UploadDocAppSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Doc Approval search</title>
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.js"></script>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <style>
        .table table tbody tr td a,
        .table table tbody tr td span {
            position: relative;
            float: left; 
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .table table > tbody > tr > td > span {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .table table > tbody > tr > td:first-child > a,
        .table table > tbody > tr > td:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .table > tbody > tr > td {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: middle;
        }

        .table table > tbody > tr > td:last-child > a,
        .table table > tbody > tr > td:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .table table > tbody > tr > td > a:hover,
        .table table > tbody > tr > td > span:hover,
        .table table > tbody > tr > td > a:focus,
        .table table > tbody > tr > td > span:focus {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }

        .mydropdownlist {
            color: black;
            font-size: 11px;
            font-family: Tahoma;
            padding: 2px 5px;
            border-radius: 2px;
            background-color: white;
            font-weight: normal;
        }

        th {
            text-align: center;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 11px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $("input[type=text]").bind("keypress", function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    return false;
                }
                return true;
            });
        });
        function ShowPopUp(type) {
            if (type == 'R') {
                document.getElementById('divRelatedEquipBom').style.display = 'block';
            }
            else if (type == 'S') {
                document.getElementById('divAddStdDoc').style.display = 'block';
            }
            else if (type == 'V') {
                document.getElementById('divAddVersionDoc').style.display = 'block';
            }
        }


        //function disableKeyEnter(evt) {
        //    evt = (evt) ? evt : window.event;
        //    var charCode = (evt.which) ? evt.which : evt.keyCode;
        //    if (charCode == 13) {
        //        return false;
        //    }
        //    return true;
        //}




        function showPleaseWait() {
            if (document.querySelector("#pleaseWaitDialog") == null) {
                var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
                                        <div class="modal-dialog">\
                                            <div class="modal-content">\
                                                <div class="modal-header">\
                                                    <h4 class="modal-title">Please wait...</h4>\
                                                </div>\
                                                <div class="modal-body">\
                                                    <div class="progress">\
                                                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                                                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                                                      </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>';
                $(document.body).append(modalLoading);
            }

            $("#pleaseWaitDialog").modal("show");
        }

        $(function () {
            $(".picker").datepicker();
        });

        /**
         * Hides "Please wait" overlay. See function showPleaseWait().
         */
        function hidePleaseWait() {
            $("#pleaseWaitDialog").modal("hide");
        }
    </script>
</head>
<body>
    <div style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma; font-size: 11px">
        <form id="form1" runat="server">
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Document Upload Approval"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table style="font-family: Tahoma; width: 100%; padding: 2px 0px 2px 0px;">
                    <tr>
                        <td style="width: 200px; padding: 2px 0px 2px 0px;">
                            <b>Document Type : </b>
                        </td>
                        <td style="width: 500px; padding: 2px 0px 2px 0px;">
                            <%--<asp:TextBox ID="tbxEquipCode" runat="server" Width="500px"></asp:TextBox>--%>
                            <asp:DropDownList ID="ddlDocType" runat="server" CssClass="mydropdownlist"></asp:DropDownList>
                        </td>
                        <td style="width: 200px; padding: 2px 0px 2px 0px;">
                            <b>Document Sheet No : </b>
                        </td>
                        <td style="width: 500px; padding: 2px 0px 2px 0px;">
                            <asp:TextBox ID="tbxSheetNo" runat="server" Width="400px"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="padding: 2px 0px 2px 0px;">
                            <b>Document Description : </b>
                        </td>
                        <td style="padding: 2px 0px 2px 0px;">
                            <asp:TextBox ID="tbxDocDesc" runat="server" Width="400px"></asp:TextBox>
                        </td>
                        <td style="width: 200px">
                            <b>Document Revision No : </b>
                        </td>
                        <td style="padding: 2px 0px 2px 0px;">
                            <asp:TextBox ID="tbxDocRevisionNo" runat="server" Width="400px"></asp:TextBox>
                            <%--    <asp:Button ID="Button2" runat="server" Text="Button" OnClick="Button2_Click" />--%>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="padding: 2px 0px 2px 0px;">
                            <b>Document Name : </b>
                        </td>
                        <td style="padding: 2px 0px 2px 0px;">
                            <asp:TextBox ID="tbxDocName" runat="server" Width="400px"></asp:TextBox>
                        </td>
                        <td style="padding: 2px 0px 2px 0px;">
                            <b>Remark : </b>
                        </td>
                        <td style="padding: 2px 0px 2px 0px;">
                            <asp:TextBox ID="tbxRemark" runat="server" Width="400px"></asp:TextBox>
                        </td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 2px 0px 2px 0px;">
                            <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnCreate" CssClass="btn btn-primary" runat="server" Text="Add Document Approval" OnClick="btnCreate_Click" />
                        </td>
                        <td style="width: 200px"></td>
                        <td style="width: 500px"></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div style="width: 90%">
                <asp:GridView ID="gvDocList" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="100%"
                    Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10"
                    AllowPaging="true" OnPageIndexChanging="gvDocList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Type">
                            <ItemTemplate>
                                <asp:Label ID="lblDocType" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                <asp:HiddenField ID="gvHdnRowId" Value='<%# Bind("rowid") %>' runat="server" />
                            </ItemTemplate>
                            <ItemStyle Width="8%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Document Name">
                            <ItemTemplate>
                                <asp:Label ID="lblDocName" runat="server" Text='<%# System.IO.Path.GetFileNameWithoutExtension(Eval("doc_name").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="D/L Doc Approved">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgDlDoc" ImageUrl="~/images/down.png" CommandName="image" CommandArgument='<%# Eval("doc_nodeid") %>' OnClick="imgDLCilck" Width="15px" Height="15px" Visible='<%# Eval("doc_visible").Equals("true") %>' />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="D/L Doc Standard">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgDlDocStd" ImageUrl="~/images/down.png" CommandName="image" CommandArgument='<%# Eval("doc_std_nodeid") %>' OnClick="imgDLCilck" Width="15px" Height="15px" Visible='<%# Eval("doc_std_visible").Equals("true") %>' ToolTip='<%# "Approved Date : " + Eval("std_approved_date").ToString().Replace(" 12:00:00 AM","") %>' />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Description ">
                            <ItemTemplate>
                                <asp:Label ID="lblDocDesc" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="25%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Sheet No">
                            <ItemTemplate>
                                <asp:Label ID="lblSheetNo" runat="server" Text='<%# Bind("doc_sheetno") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="8%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Revision No">
                            <ItemTemplate>
                                <asp:HiddenField ID="hidRevisionDisplay" Value='<%# Bind("doc_revision_no") %>' runat="server" />
                                <asp:Label ID="lblRevisionNo" runat="server" Text='<%# Bind("doc_revision_display") %>'></asp:Label>

                            </ItemTemplate>
                            <ItemStyle Width="7%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remark">
                            <ItemTemplate>
                                <asp:Label ID="lblRemark" runat="server" Text='<%# Bind("doc_remark") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Edit Related Doc">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgEditEq" ImageUrl="~/images/edit.png" CommandName="image" CommandArgument='<%# Eval("doc_nodeid") %>' OnClick="imgEditEqCilck" Width="20px" Height="20px" Visible='<%# Eval("doc_edit_relate").Equals("true") %>'  />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Upload Standard Doc">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgAddStd" ImageUrl="~/images/Up.png" CommandName="image" CommandArgument='<%# Eval("doc_nodeid") %>' OnClick="imgAddStdCilck" Width="20px" Height="20px" Visible='<%# Eval("doc_edit_relate").Equals("true")? String.IsNullOrEmpty(Eval("doc_std_nodeid").ToString()):false %>' />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Add Version Doc">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgAddVer" ImageUrl="~/images/plus.png" CommandName="image" CommandArgument='<%# Eval("doc_nodeid") %>' OnClick="imgAddVerCilck" Width="20px" Height="20px" Visible='<%# Eval("add_doc_visible").Equals("true") %>' />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <%--<asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <asp:HyperLink ID="linkEdit" runat="server" NavigateUrl='<%# "Equipment.aspx?equipcode=" + Eval("equip_code") %>'>Edit</asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>--%>
                    </Columns>
                </asp:GridView>

            </div>
            <div id="divRelatedEquipBom" class="w3-modal">
                <div class="w3-modal-content">
                    <div class="w3-container">
                        <span onclick="document.getElementById('divRelatedEquipBom').style.display='none'"
                            class="w3-button w3-display-topright">&times;</span>
                        <div>
                            <table style="font-family: Tahoma; width: 100%;">
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblHeader" runat="server" Font-Names="Tahoma" Font-Size="12pt" Font-Bold="true" Text="Document related equipment"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 17%">
                                        <b>Document Type : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblDocType" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Name : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblDocName" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Description : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblDocDesc" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Sheet No : </b>
                                    </td>
                                    <td style="width: 83%">
                                        <asp:Label ID="PoplblSheetNo" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>


                                <tr>
                                    <td>
                                        <b>Document Revision No : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblDocRevisionNo" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>


                                <tr>
                                    <td>
                                        <b>Remark : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblRemark" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                        <div>
                            <table style="width: 100%">
                                <tr>
                                    <td colspan="3">
                                        <asp:DropDownList ID="ddlBomEquip" runat="server" Height="21px">
                                            <asp:ListItem Value="B">Bom</asp:ListItem>
                                            <asp:ListItem Value="E">Equipment</asp:ListItem>
                                            <asp:ListItem Value="G">Equipment Group</asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:TextBox ID="tbxSearchBomEquip" runat="server"></asp:TextBox>
                                        <asp:Button CssClass="btn btn-primary" ID="btnSearchEquipBom" runat="server" Text="Search" OnClientClick="showPleaseWait()" OnClick="btnSearchEquipBom_Click" />
                                        <asp:HiddenField ID="hdnSelectedNodeId" runat="server" />
                                    </td>

                                </tr>
                                <tr>
                                    <td style="text-align: center">
                                        <b>Search Related Result</b>
                                    </td>
                                    <td></td>
                                    <td style="text-align: center">
                                        <b>Selected Bom/Equipment Related</b>

                                    </td>
                                </tr>
                                <tr>
                                    <td style="vertical-align: top; align-content: center; width: 45%;">
                                        <asp:GridView ID="gvUnSelect" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="100%"
                                            Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10"
                                            AllowPaging="true" OnPageIndexChanging="gvUnSelect_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cbxSelect" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="60%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                    <td style="vertical-align: top; width: 10%; text-align: center">
                                        <asp:Button ID="btnSelectingEquipBom" CssClass="btn btn-primary" runat="server" Text="Select" OnClientClick="showPleaseWait()" OnClick="btnSelectingEquipBom_Click" />
                                    </td>
                                    <td style="vertical-align: top; align-content: center; width: 45%;">
                                        <asp:GridView ID="gvSelect" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="100%"
                                            Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10" OnRowDeleting="OnRowGvSelectedDeleting"
                                            AllowPaging="true" OnPageIndexChanging="gvSelect_PageIndexChanging">
                                            <Columns>
                                                <asp:TemplateField HeaderText="Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblCode" runat="server" Text='<%# Bind("code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Bind("name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="60%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Type" Visible="false">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("type") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Delete">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/bin.png" Height="16px" Width="16px" CommandName="Delete" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="btnSaveEquipBomRelated" CssClass="btn btn-primary" runat="server" Text="Save" OnClientClick="return confirm('Are you sure you want to save changed!');" OnClick="btnSaveEquipBomRelated_Click" />
                                        <asp:Button ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-danger" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divAddStdDoc" class="w3-modal">
                <div class="w3-modal-content">
                    <div class="w3-container">
                        <span onclick="document.getElementById('divAddStdDoc').style.display='none'"
                            class="w3-button w3-display-topright">&times;</span>
                        <div>
                            <table style="font-family: Tahoma; width: 100%;">
                                <tr>
                                    <td colspan="5">
                                        <asp:Label ID="lblStdHeader" runat="server" Font-Names="Tahoma" Font-Size="12pt" Font-Bold="true" Text="Add Standard Document"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 17%">
                                        <b>Document Type : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PopStdlblDocType" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Name : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PopStdlblDocName" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Description : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PopStdlblDocDesc" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Sheet No : </b>
                                    </td>
                                    <td style="width: 83%">
                                        <asp:Label ID="PopStdlblSheetNo" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>


                                <tr>
                                    <td>
                                        <b>Document Revision No : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PopStdlblDocRevisionNo" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>


                                <tr>
                                    <td>
                                        <b>Remark : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PopStdlblRemark" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Label ID="PoplblStdInput" runat="server" Font-Names="Tahoma" Font-Size="12pt" Font-Bold="true" Text="Upload Standard Document"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Standard Document : </b>
                                    </td>
                                    <td colspan="2">
                                        <asp:FileUpload ID="FileUploadStdDoc" runat="server" />

                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Approved Date : </b>
                                    </td>  
                                    <td colspan="2">
                                        <%--   <input type="text" id="dateStbid" runat="server" class="picker" autocomplete="off"/> --%>
                                        <asp:TextBox ID="popTbxStdAppDate" CssClass="picker" runat="server" autocomplete="off"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="popBtnUploadStdDoc" CssClass="btn btn-primary" runat="server" Text="Save" OnClientClick="return confirm('Are you sure you want to upload this document.');" OnClick="popBtnUploadStdDoc_Click" />
                                        &nbsp;
                                        <asp:Label ID="popStdErrMsgText" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div id="divAddVersionDoc" class="w3-modal">
                <div class="w3-modal-content">
                    <div class="w3-container">
                        <span onclick="document.getElementById('divAddVersionDoc').style.display='none'"
                            class="w3-button w3-display-topright">&times;</span>
                        <div>
                            <table style="font-family: Tahoma; width: 100%;">
                                <tr>
                                    <td colspan="3">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" style="background-color: antiquewhite; text-align: center;">
                                        <asp:Label ID="PopLblAddHeader" runat="server" Font-Names="Tahoma" Font-Size="12pt" Font-Bold="true" Text="Add Version Document"></asp:Label>
                                        <asp:HiddenField ID="PopHdnRowId" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 150px">
                                        <b>Document Type : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblAddDocType" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Name : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblAddDocName" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Description : </b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="PopTbxAddDocDesc" runat="server"></asp:TextBox>
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>Document Revision No : </b>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="PopTbxAddDocRevisionNo" runat="server"></asp:TextBox>
                                        <asp:HiddenField ID="PopHidRevisionDisplay" runat="server" />

                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Document Sheet No : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblAddSheetNo" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td>
                                        <b>Remark : </b>
                                    </td>
                                    <td>
                                        <asp:Label ID="PoplblAddRemark" runat="server" Text=''></asp:Label>
                                    </td>
                                    <td></td>
                                </tr>

                                <tr>
                                    <td>
                                        <b>Upload New Version File : </b>
                                    </td>
                                    <td>
                                        <asp:FileUpload ID="FileUploadAddVersion" runat="server" />
                                    </td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:CheckBox ID="PopCbxNeedApprover" runat="server" Text="Need Approval" OnCheckedChanged="PopCbxNeedApprover_CheckedChanged" AutoPostBack="true" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <div id="divApproval" runat="server">
                                            <table style="width: 100%">
                                                <tr>
                                                    <td>
                                                        <table style="width: 100%">
                                                            <tr>
                                                                <td style="text-align: center; font-size: larger; vertical-align: top; width: 100%; background-color: antiquewhite;">
                                                                    <b>Search Approver</b>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>&nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table style="width: 100%">
                                                                        <tr>
                                                                            <td>
                                                                                <b>Emp ID:</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="PopTbxEmpIdSearch" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <b>Full Name:</b>

                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="PopTbxEmpFullNameSearch" runat="server"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <b>Department:</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="PopTbxDeptSearch" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <b>Position:</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="PopTbxPositionSearch" runat="server"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <b>สังกัด:</b>
                                                                            </td>
                                                                            <td>
                                                                                <asp:TextBox ID="PopTbxEmpDeptSearch" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td></td>
                                                                            <td></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="4">
                                                                                <asp:Button ID="BtnSearchApprover" runat="server" Text="Search" CssClass="btn btn-primary" OnClick="BtnSearchApprover_Click" />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <b>Subject : </b><b style="color: red">*</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox ID="PopTbxSubject" runat="server" Width="500px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td>
                                                                                <b>Content : </b><b style="color: red">*</b>
                                                                            </td>
                                                                            <td colspan="3">
                                                                                <asp:TextBox ID="PopTbxContent" runat="server" Width="500px"></asp:TextBox>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="text-align: center">
                                                                                <b>Unselected Approver</b>
                                                                            </td>
                                                                            <td colspan="2" style="text-align: center">
                                                                                <b>Selected Approver</b>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <asp:GridView ID="gvUnselectApprover" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" HorizontalAlign="Center" Width="400px"
                                                                                    Visible="true" ClientIDMode="Static" Font-Size="11px" AllowPaging="true" OnPageIndexChanging="gvUnselectApprover_PageIndexChanging">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Approver Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("FULLNAME") %>' Style="word-wrap: normal; word-break: break-all;"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="210px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Approver Position">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblEmpPostion" runat="server" Text='<%# Bind("POSITION_SHORT") %>' Style="word-wrap: normal; word-break: break-all;"></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" Wrap="true" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Select">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="imgAddApprover" runat="server" ImageUrl="~/images/add.png" Height="16px" Width="16px" OnClick="imgAddApprover_Click" CommandArgument='<%# "z"+Eval("id") %>' />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                            <td colspan="2" style="vertical-align: top">
                                                                                <asp:GridView ID="gvSelectedApprover" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" HorizontalAlign="Center" Width="400px"
                                                                                    Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10" OnRowDeleting="OnRowGvSelectedApproverDeleting">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="Approver No.">
                                                                                            <ItemTemplate>
                                                                                                <%# Container.DataItemIndex + 1 %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Approver Name">
                                                                                            <ItemTemplate>
                                                                                                <asp:HiddenField ID="hdnEmpId" runat="server" Value='<%# Bind("id") %>' />
                                                                                                <asp:Label ID="lblEmpName" runat="server" Text='<%# Bind("FULLNAME") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="50%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Approver Position">
                                                                                            <ItemTemplate>
                                                                                                <asp:Label ID="lblEmpPostion" runat="server" Text='<%# Bind("POSITION_SHORT") %>'></asp:Label>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="30%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField HeaderText="Delete">
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/bin.png" Height="16px" Width="16px" CommandName="Delete" />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                    <td></td>

                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        <asp:Button ID="popBtnAddVersion" CssClass="btn btn-primary" runat="server" Text="Add Version" OnClientClick="return confirm('Are you sure you want to upload this document.');" OnClick="popBtnAddVersion_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</body>
</html>
