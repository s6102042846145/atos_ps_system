﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PS_Library;
using System.Data;
using System.Configuration;
using PS_Library.DocumentManagement;
using System.IO;

namespace PS_System.form.Master
{
    public partial class UploadDocAppSearch : System.Web.UI.Page
    {
        clsUploadDocApp objDocApp = new clsUploadDocApp();
        public string strEmpIdLogin = string.Empty; 

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["zuserlogin"] != null)
            {
                strEmpIdLogin = Request.QueryString["zuserlogin"];
            }
            else
            {
                strEmpIdLogin = "z590711";// for test only
            }
            if (!IsPostBack)
            {
                BindDdlDocType();
            }
        }

        private void BindingGv()
        {
            DataTable dt = objDocApp.GetDocAppList(ddlDocType.SelectedValue, tbxDocDesc.Text.Trim(), tbxDocName.Text.Trim(), tbxDocRevisionNo.Text.Trim(), tbxSheetNo.Text.Trim(), tbxRemark.Text.Trim(), strEmpIdLogin);
            gvDocList.DataSource = dt;
            gvDocList.DataBind();
        }

        private void BindDdlDocType()
        {
            ddlDocType.Items.Clear();
            DataTable dtDocType = objDocApp.GetDocTypeAll();
            ddlDocType.Items.Add(new ListItem("Please select document type ", ""));
            foreach (DataRow dr in dtDocType.Rows)
            {
                ddlDocType.Items.Add(new ListItem(dr["doctype_name"].ToString(), dr["doctype_code"].ToString()));
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGv();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("UploadDocApp.aspx?zuserlogin=" + strEmpIdLogin);
            //objDocApp.DownloadDocCsByNodeId("2649978");
        }

        protected void gvDocList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvDocList.PageIndex = e.NewPageIndex;
            this.BindingGv();
        }

        protected void imgDLCilck(object sender, EventArgs e)
        {
            //select the row
            ImageButton imageButton = (ImageButton)sender;
            string strNodeId = imageButton.CommandArgument;
            objDocApp.DownloadDocCsByNodeId(strNodeId);
        }

        public void checkboxclear()
        {
            foreach (GridViewRow row in gvUnSelect.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox chkrow = (CheckBox)row.FindControl("cbxSelect");
                    if (chkrow.Checked)
                        chkrow.Checked = false;
                }
            }
        }

        public long UploadDocToCs(string strDocTypeName, byte[] byteContent, string strDocName, string StrFileName)
        {
            OTFunctions ot = new OTFunctions();
            string strParentIdToUpload = string.Empty;
            long nodeID = 0;
            // get root Folder
            string zFolderTransmittionNodeID = ConfigurationManager.AppSettings["FolderTransmittionSystemNodeID"].ToString();

            // check need approve
            //if (!bNeedApprove)
            //{
            if (!String.IsNullOrEmpty(strDocTypeName))
            {
                // Check folder Engineering Design master isExist
                Node nodeEng = ot.getNodeByName(zFolderTransmittionNodeID, "Engineering Design Master");
                if (nodeEng == null)
                { // if not exist : create
                    nodeEng = ot.createFolder(zFolderTransmittionNodeID, "Engineering Design Master");
                }
                // check folder doc type isExist
                Node nodeDocType = ot.getNodeByName(nodeEng.ID.ToString(), strDocTypeName.Trim());
                if (nodeDocType == null)
                { // if not exist : create
                    nodeDocType = ot.createFolder(nodeEng.ID.ToString(), strDocTypeName.Trim());
                }
                // check doc type is Drawing? 
                if (strDocTypeName.Trim() == "Drawings")
                {
                    Node nodeStdDrawing = ot.getNodeByName(nodeDocType.ID.ToString(), "00 - Standard Drawing");
                    if (nodeStdDrawing == null)
                    {
                        nodeStdDrawing = ot.createFolder(nodeDocType.ID.ToString(), "00 - Standard Drawing");
                    }
                    strParentIdToUpload = nodeStdDrawing.ID.ToString();
                }
                else
                {
                    strParentIdToUpload = nodeDocType.ID.ToString();
                }
                // upload doc to cs 
                NodeInfo nodeInfo = new NodeInfo();
                nodeID = ot.uploadDoc(strParentIdToUpload, strDocName, StrFileName, byteContent, strDocName);
                // get node id
            }

            //}
            //else
            //{
            //    // need more logic about approval structure
            //}
            return nodeID;
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            //Returns just 5 unique rows
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        #region popup related doc

        private void AddDocBomEquipment()
        {
            DataTable dtSelected = (DataTable)ViewState["dtSelect"];
            DataTable dtAddDoc = (DataTable)ViewState["dtAddDoc"];
            DataTable dtTempEquip = new DataTable();
            string strNodeId = string.Empty;
            string strType = string.Empty;
            string strCode = string.Empty;
            if (dtSelected != null && dtAddDoc != null)
            {
                if (dtSelected.Rows.Count > 0 && dtAddDoc.Rows.Count > 0)
                {
                    foreach (DataRow drAd in dtAddDoc.Rows)
                    {
                        strNodeId = drAd["doc_nodeid"].ToString();
                        foreach (DataRow drBeq in dtSelected.Rows)
                        {
                            strType = drBeq["type"].ToString();
                            strCode = drBeq["code"].ToString();
                            if (strType == "E")
                            {
                                //equip
                                objDocApp.AddDocEquipment(strCode, strNodeId, strEmpIdLogin);
                            }
                            else
                            {
                                // bom
                                objDocApp.AddDocBom(strCode, strNodeId, strEmpIdLogin);
                                dtTempEquip = objDocApp.GetEquipByBomCode(strCode);
                                foreach (DataRow drEquip in dtTempEquip.Rows)
                                {
                                    //equip in this bom
                                    objDocApp.AddDocEquipment(drEquip["equip_code"].ToString(), strNodeId, strEmpIdLogin);
                                }
                            }
                        }
                    }
                }
            }
        }

        protected void imgEditEqCilck(object sender, EventArgs e)
        {
            //select the row
            ImageButton imageButton = (ImageButton)sender;
            string strNodeId = imageButton.CommandArgument;
            hdnSelectedNodeId.Value = strNodeId;
            // get Doc Detail
            DataTable dtDocDetail = objDocApp.GetDocInfoByNodeId(strNodeId);
            if (dtDocDetail.Rows.Count > 0)
            {
                PoplblDocName.Text = System.IO.Path.GetFileNameWithoutExtension(dtDocDetail.Rows[0]["doc_name"].ToString());
                PoplblDocDesc.Text = dtDocDetail.Rows[0]["doc_desc"].ToString();
                PoplblDocRevisionNo.Text = dtDocDetail.Rows[0]["doc_revision_display"].ToString();
                PoplblSheetNo.Text = dtDocDetail.Rows[0]["doc_sheetno"].ToString();
                PoplblRemark.Text = dtDocDetail.Rows[0]["doc_remark"].ToString();
                PoplblDocType.Text = dtDocDetail.Rows[0]["doctype_name"].ToString();
            }
            // get Equip detail
            DataTable dtSelectedEquip = objDocApp.GetEquipmentListByNodeId(strNodeId);
            DataTable dtSelectedBom = objDocApp.GetBomListByNodeId(strNodeId);
            DataTable dtSelectedEquipGrp = objDocApp.GetEquipmentGroupListByNodeId(strNodeId);
            DataTable dtSelectedAll = new DataTable();
            if (dtSelectedBom.Rows.Count > 0) dtSelectedAll.Merge(dtSelectedBom);
            if (dtSelectedEquip.Rows.Count > 0) dtSelectedAll.Merge(dtSelectedEquip);
            if (dtSelectedEquipGrp.Rows.Count > 0) dtSelectedAll.Merge(dtSelectedEquipGrp);
            ViewState["dtSelect"] = dtSelectedAll;
            gvSelect.DataSource = dtSelectedAll;
            gvSelect.DataBind();

            //get eqip search
            BindingGvUnselectEquip();

            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('R');", true);

        }

        protected void OnRowGvSelectedDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["dtSelect"] as DataTable;
            dt.Rows[index].Delete();
            dt.AcceptChanges();
            ViewState["dtSelect"] = dt;
            gvSelect.DataSource = dt;
            gvSelect.DataBind();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('R');", true);

        }

        private void BindingGvUnselectEquip()
        {
            DataTable dt = GetBomEquipment(tbxSearchBomEquip.Text);
            gvUnSelect.DataSource = dt;
            gvUnSelect.DataBind();
        }

        protected void gvUnSelect_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUnSelect.PageIndex = e.NewPageIndex;
            BindingGvUnselectEquip();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('R');", true);
        }

        protected void gvSelect_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSelect.PageIndex = e.NewPageIndex;
            //BindingGvUnselectEquip();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('R');", true);
        }

        protected void btnSelectingEquipBom_Click(object sender, EventArgs e)
        {
            DataTable dtSelect = new DataTable();
            DataTable dtPreviousSelect = new DataTable();
            if (ViewState["dtSelect"] != null)
                dtPreviousSelect = (DataTable)ViewState["dtSelect"];
            dtSelect.Columns.Add("code");
            dtSelect.Columns.Add("name");
            dtSelect.Columns.Add("type");
            DataRow dr;
            foreach (GridViewRow row in gvUnSelect.Rows)
            {

                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox cbxSelect = row.FindControl("cbxSelect") as CheckBox;
                    if (cbxSelect.Checked)
                    {
                        dr = dtSelect.NewRow();
                        Label lblCode = row.FindControl("lblCode") as Label;
                        Label lblName = row.FindControl("lblName") as Label;
                        Label lblType = row.FindControl("lblType") as Label;
                        dr["code"] = lblCode.Text;
                        dr["name"] = lblName.Text;
                        dr["type"] = lblType.Text;
                        dtSelect.Rows.Add(dr);
                    }
                }
            }

            if (dtSelect.Rows.Count > 0)
            {
                if (dtPreviousSelect.Rows.Count > 0)
                {
                    if (dtPreviousSelect.Rows[0]["type"].ToString() != string.Empty)
                    {
                        dtSelect.Merge(dtPreviousSelect);
                    }
                }

                dtSelect = RemoveDuplicatesRecords(dtSelect);
                ViewState["dtSelect"] = dtSelect;
                gvSelect.DataSource = dtSelect;
                gvSelect.DataBind();
                checkboxclear();
            }
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('R');", true);
        }

        protected void btnSearchEquipBom_Click(object sender, EventArgs e)
        {

            gvUnSelect.DataSource = null;
            gvUnSelect.DataBind();
            DataTable dtUnselect = GetBomEquipment(tbxSearchBomEquip.Text.Trim());
            ViewState["dtUnselect"] = dtUnselect;
            gvUnSelect.DataSource = dtUnselect;
            gvUnSelect.DataBind();


            if (gvSelect.Rows.Count <= 0)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("code");
                dt.Columns.Add("name");
                dt.Columns.Add("type");

                DataRow dr = dt.NewRow();
                dr["code"] = "";
                dr["name"] = "";
                dr["type"] = "";

                dt.Rows.Add(dr);
                ViewState["dtSelect"] = dt;
                gvSelect.DataSource = dt;
                gvSelect.DataBind();
            }
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('R');", true);

            //GvBomEquipSelected.DataSource = GetBomEquipment(tbxSearchBonEquip.Text);
            //GvBomEquipSelected.DataBind();

            //CheckDivGvs();
        }

        protected void btnSaveEquipBomRelated_Click(object sender, EventArgs e)
        {
            DataTable dtSelected = (DataTable)ViewState["dtSelect"];
            objDocApp.UpdateEquipBomRelated(hdnSelectedNodeId.Value, PoplblDocName.Text, dtSelected, strEmpIdLogin);
        }

        private DataTable GetBomEquipment(string strSearchText)
        {
            DataTable dt = new DataTable();

            if (ddlBomEquip.SelectedValue == "B")
            {
                dt = objDocApp.GetBom(strSearchText);
            }
            else if (ddlBomEquip.SelectedValue == "E")
            {
                dt = objDocApp.GetEquipment(strSearchText);
            }
            else
            {
                dt = objDocApp.GetEquipmentGroup(strSearchText);
            }

            return dt;
        }

        #endregion

        #region popup add standard doc

        protected void imgAddStdCilck(object sender, EventArgs e)
        {
            popTbxStdAppDate.Attributes.Add("readonly", "readonly");
            popStdErrMsgText.Text = string.Empty;
            //select the row
            ImageButton imageButton = (ImageButton)sender;
            string strNodeId = imageButton.CommandArgument;
            hdnSelectedNodeId.Value = strNodeId;
            // get Doc Detail
            DataTable dtDocDetail = objDocApp.GetDocInfoByNodeId(strNodeId);
            if (dtDocDetail.Rows.Count > 0)
            {
                PopStdlblDocName.Text = System.IO.Path.GetFileNameWithoutExtension(dtDocDetail.Rows[0]["doc_name"].ToString());
                PopStdlblDocDesc.Text = dtDocDetail.Rows[0]["doc_desc"].ToString();
                PopStdlblDocRevisionNo.Text = dtDocDetail.Rows[0]["doc_revision_display"].ToString();
                PopStdlblSheetNo.Text = dtDocDetail.Rows[0]["doc_sheetno"].ToString();
                PopStdlblRemark.Text = dtDocDetail.Rows[0]["doc_remark"].ToString();
                PopStdlblDocType.Text = dtDocDetail.Rows[0]["doctype_name"].ToString();
            }
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('S');", true);
        }

        protected void popBtnUploadStdDoc_Click(object sender, EventArgs e)
        {
            popStdErrMsgText.Text = string.Empty;
            if (!String.IsNullOrEmpty(popTbxStdAppDate.Text.Trim()))
            {
                if ((FileUploadStdDoc.PostedFile != null) && (FileUploadStdDoc.PostedFile.ContentLength > 0))
                {
                    HttpPostedFile uploadedFile = FileUploadStdDoc.PostedFile;
                    Stream fs = uploadedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    byte[] bytesUl = br.ReadBytes((Int32)fs.Length);
                    long stdNodeId = UploadDocToCs(PopStdlblDocType.Text, bytesUl, uploadedFile.FileName, uploadedFile.FileName);
                    objDocApp.UpdateDocStdByDocDocId(hdnSelectedNodeId.Value, stdNodeId.ToString(), popTbxStdAppDate.Text, strEmpIdLogin);
                    BindingGv();
                }
                else
                {
                    popStdErrMsgText.Text = "Please browse file.";
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('S');", true);
                }
            }
            else
            {
                popStdErrMsgText.Text = "Please input approve date.";
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('S');", true);
            }
        }

        private void SearchApprover()
        {
            string strEmpId, strFullName, strDept, strPosition, strEmpDept;
            strEmpId = strFullName = strDept = strPosition = strEmpDept = string.Empty;
            strEmpId = PopTbxEmpIdSearch.Text.Trim();
            strFullName = PopTbxEmpFullNameSearch.Text.Trim();
            strDept = PopTbxDeptSearch.Text.Trim();
            strPosition = PopTbxPositionSearch.Text.Trim();
            strEmpDept = PopTbxEmpDeptSearch.Text.Trim();
            DataTable dtEmp = objDocApp.GetApproverCommon(strEmpId, strDept, strFullName, strPosition, strEmpDept);
            gvUnselectApprover.DataSource = dtEmp;
            gvUnselectApprover.DataBind();

        }

        #endregion
         
        #region popup add version doc

        protected void popBtnAddVersion_Click(object sender, EventArgs e)
        {
            if (IsValidateAddVer())
            {
                if ((FileUploadAddVersion.PostedFile != null) && (FileUploadAddVersion.PostedFile.ContentLength > 0))
                { 
                    HttpPostedFile uploadedFile = FileUploadAddVersion.PostedFile;
                    Stream fs = uploadedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    bool isStartWf = PopCbxNeedApprover.Checked;
                    string strExtensionFile =  Path.GetExtension(PoplblAddDocName.Text);

                    string strFileName = PoplblAddDocName.Text.Replace(strExtensionFile,string.Empty) + "_" + PopTbxAddDocRevisionNo.Text.Trim() + strExtensionFile;
                    byte[] bytesUl = br.ReadBytes((Int32)fs.Length);
                    long newNodeId = UploadDocToCs(PoplblAddDocType.Text, bytesUl, strFileName, strFileName);

                    //insert to doc_app with status wf in progress/Active
                    DocApprove objThisDoc = new DocApprove();
                    string strLastVerNodeId = hdnSelectedNodeId.Value;
                    DataTable dtLastVerDoc = objDocApp.GetDocInfoByNodeId(strLastVerNodeId);
                    objThisDoc.strDocDesc = PopTbxAddDocDesc.Text;
                    objThisDoc.strDocName = strFileName;
                    objThisDoc.strDocSheet = PoplblAddSheetNo.Text;
                    objThisDoc.strDocType = dtLastVerDoc.Rows[0]["doctype_code"].ToString();
                    objThisDoc.strRemark = PoplblAddRemark.Text;
                    
                    try
                    {
                        objThisDoc.strRevision = (int.Parse(dtLastVerDoc.Rows[0]["doc_revision_no"].ToString()) + 1).ToString();
                    }
                    catch
                    {
                        objThisDoc.strRevision = "0";
                    }
                    objThisDoc.strRevisionDis = PopTbxAddDocRevisionNo.Text;
                    objThisDoc.strSection = "";
                    objThisDoc.strDep = "";
                    objThisDoc.strNodeId = newNodeId.ToString();
                    objDocApp.AddDocApproved(objThisDoc, strEmpIdLogin, isStartWf);

                    // case start WF
                    if (isStartWf)
                    {
                        //update last ver flag pending approve
                        objDocApp.SetPendingWfByNodeId(strLastVerNodeId, true);
                        //put approver position to array
                        DataTable dtSelectedApprover = (DataTable)ViewState["dtSelectedApprover"];
                        // string[] arrApvPostion = new string[dtSelectedApprover.Rows.Count];
                        string[] arrApvId = new string[dtSelectedApprover.Rows.Count];
                        for (int i = 0; i < dtSelectedApprover.Rows.Count;i++)
                        {
                            //arrApvPostion[i] = dtSelectedApprover.Rows[0]["POSITION_SHORT"].ToString();
                            arrApvId[i] = dtSelectedApprover.Rows[i]["id"].ToString();
                        }
                        //start wf get wf processid
                        WfPsDocApprove wfObj = new WfPsDocApprove();
                        wfObj.doc_node_id = newNodeId.ToString();
                        wfObj.doc_node_id_prev = strLastVerNodeId;
                        wfObj.created_by = strEmpIdLogin;
                        wfObj.updated_by = strEmpIdLogin;
                        wfObj.emp_id = strEmpIdLogin;
                        wfObj.memo_content = PopTbxContent.Text.Trim();
                        wfObj.memo_subject = PopTbxSubject.Text.Trim();
                        objDocApp.StartWF(ref wfObj, arrApvId);

                    }
                    else
                    {
                        // inactive last ver 
                        objDocApp.UpdateDocStatusByNodeId("Inactive", strLastVerNodeId);
                    }
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUpo", "alert('Please input all required field.');", true);
            }
        }

        private bool IsValidateAddVer()
        {
            bool isValidate = true;
            if (PopCbxNeedApprover.Checked)
            {
                if (ViewState["dtSelectedApprover"] != null)
                {
                    DataTable dtSelectedApprover = (DataTable)ViewState["dtSelectedApprover"];
                    if(dtSelectedApprover.Rows.Count<1)
                    isValidate = false;
                }
                else
                {
                    isValidate = false;
                }

                if(PopTbxSubject.Text.Trim()==string.Empty) isValidate = false;
                if(PopTbxContent.Text.Trim()==string.Empty) isValidate = false;
                if(!FileUploadAddVersion.HasFile) isValidate = false;


            }
            return isValidate;
        }

        protected void imgAddVerCilck(object sender, EventArgs e)
        {
            //select the row
            ImageButton imageButton = (ImageButton)sender;
            string strNodeId = imageButton.CommandArgument;
            hdnSelectedNodeId.Value = strNodeId;
            PopCbxNeedApprover.Checked = false;
            divApproval.Visible = false;
            // get Doc Detail
            DataTable dtDocDetail = objDocApp.GetDocInfoByNodeId(strNodeId);
            if (dtDocDetail.Rows.Count > 0)
            {
                PoplblAddDocName.Text = dtDocDetail.Rows[0]["doc_name"].ToString();
                PopTbxAddDocDesc.Text = dtDocDetail.Rows[0]["doc_desc"].ToString();
                try
                {
                    PopTbxAddDocRevisionNo.Text = (int.Parse(dtDocDetail.Rows[0]["doc_revision_no"].ToString())+1).ToString();
                }
                catch
                {
                    PopTbxAddDocRevisionNo.Text = dtDocDetail.Rows[0]["doc_revision_display"].ToString();
                }
                PoplblAddSheetNo.Text = dtDocDetail.Rows[0]["doc_sheetno"].ToString();
                PoplblAddRemark.Text = dtDocDetail.Rows[0]["doc_remark"].ToString();
                PoplblAddDocType.Text = dtDocDetail.Rows[0]["doctype_name"].ToString();
               
            }
            gvUnselectApprover.DataSource = null;
            gvUnselectApprover.DataBind();
            gvSelectedApprover.DataSource = null;
            gvSelectedApprover.DataBind();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        protected void imgAddApprover_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgBtn = sender as ImageButton;
            GridViewRow gvRow = (GridViewRow)imgBtn.NamingContainer;
            Label lblEmpPostion = gvRow.FindControl("lblEmpPostion") as Label;
            Label lblEmpName = gvRow.FindControl("lblEmpName") as Label;

            string strUserId = imgBtn.CommandArgument;
            string strPosition = lblEmpPostion.Text;
            string strUserName = lblEmpName.Text;
            //DeleteUserById(Convert.ToInt32(button.CommandArgument));
            // check user is available in cs
            if (objDocApp.IsCsUserExist(strUserId))
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
                AddUserToSelected(strUserId, strUserName, strPosition);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUpo", "alert(' User : " + strUserName + " does not exist in CS.');", true);
            }
        }

        private void AddUserToSelected(string strUserId, string strUserName, string strPosition)
        {
            bool isDup = false;
            DataTable dtSelectedApprover = new DataTable();
            if (ViewState["dtSelectedApprover"] != null)
                dtSelectedApprover = (DataTable)ViewState["dtSelectedApprover"];
            if (dtSelectedApprover.Rows.Count > 0)
            {
                // check dup
                DataRow[] foundEmp = dtSelectedApprover.Select("id = '" + strUserId + "'");
                if (foundEmp.Length > 0)
                    isDup = true;
            }
            else
            {
                // create col
                try
                {
                    DataColumn dc = new DataColumn("id");
                    dtSelectedApprover.Columns.Add(dc);
                    dc = new DataColumn("FULLNAME");
                    dtSelectedApprover.Columns.Add(dc);
                    dc = new DataColumn("POSITION_SHORT");
                    dtSelectedApprover.Columns.Add(dc);
                }
                catch { }
            }
            if (!isDup)
            {
                DataRow dr = dtSelectedApprover.NewRow();
                dr["id"] = strUserId;
                dr["FULLNAME"] = strUserName;
                dr["POSITION_SHORT"] = strPosition;
                dtSelectedApprover.Rows.Add(dr);
            }
            ViewState["dtSelectedApprover"] = dtSelectedApprover;
            gvSelectedApprover.DataSource = dtSelectedApprover;
            gvSelectedApprover.DataBind();

        }

        protected void OnRowGvSelectedApproverDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["dtSelectedApprover"] as DataTable;
            dt.Rows[index].Delete();
            dt.AcceptChanges();
            ViewState["dtSelectedApprover"] = dt;
            gvSelectedApprover.DataSource = dt;
            gvSelectedApprover.DataBind();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        protected void BtnSearchApprover_Click(object sender, EventArgs e)
        {
            SearchApprover();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        protected void PopCbxNeedApprover_CheckedChanged(object sender, EventArgs e)
        {
            divApproval.Visible = PopCbxNeedApprover.Checked ? true : false;
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        protected void gvUnselectApprover_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUnselectApprover.PageIndex = e.NewPageIndex;
            SearchApprover();
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        #endregion


    }
}