﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class EgatSerialNo : System.Web.UI.Page
    {
        public string strEmpIdLogin = string.Empty;
        clsEgatSn objClsSn = new clsEgatSn();


        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["zuserlogin"] != null)
            {
                strEmpIdLogin = Request.QueryString["zuserlogin"];
            }
            else
            {
                strEmpIdLogin = "z596358";// for test only
            }
            hdnDept.Value = GetDeptByEmpId(strEmpIdLogin); // กวอ
            if (!IsPostBack)
            {
                BindDdlContractNo();
                divExport.Visible = false;
            }
        }

        private string GetDeptByEmpId(string strEmpId)
        {
            string strDept = string.Empty;
            Employee thisEmp = new Employee();
            EmpInfoClass emp = new EmpInfoClass();
            try
            {
                thisEmp = emp.getInFoByEmpID(strEmpId);
                strDept = thisEmp.ORGSNAME4;

            }
            catch { }

            return strDept;
        }

        private void BindingGv()
        {
            DataTable dtBind = null; //
            try
            {
                dtBind = (DataTable)ViewState["dtBinding"];
            }
            catch { }
            if (dtBind != null)
            {
                //dtBind = new DataTable();// get from query
                ViewState["dtBinding"] = dtBind;
            }
            if (dtBind.Rows.Count > 0)
            {
                divExport.Visible = true;
            }
            else
            {
                divExport.Visible = false;
            }
            gvSnList.DataSource = dtBind;
            gvSnList.DataBind();
        }

        private void BindDdlContractNo()
        {
            ddlContractNo.Items.Clear();
            DataTable dtContractList = objClsSn.GetContractListByStatus(ddlSnStatus.SelectedValue, hdnDept.Value);
            foreach (DataRow dr in dtContractList.Rows)
            {
                ddlContractNo.Items.Add(new ListItem(dr["contract_no"].ToString()));
            }
        }

        protected void ddlSnStatus_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDdlContractNo();
            if (ddlSnStatus.SelectedValue == "g")
            {
                btnSearch.Text = "Search";
            }
            else
            {
                btnSearch.Text = "Preview S/N.";
            }
            gvSnList.DataSource = null;
            gvSnList.DataBind();
            //divExport.Visible = false;
            btnExport.Visible = false;
            btnGenerateSn.Visible = false;
            //btnSearch.Visible = true;
            lblSelectedContractNo.Text = string.Empty;
        }

        protected void gvSnList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSnList.PageIndex = e.NewPageIndex;
            this.BindingGv();
        }

        private void ExportExcel()
        {
            string strFileName = "Export_serial_no_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // export grid view to excel
            DataTable dtResult = (DataTable)ViewState["dtBinding"];

            byte[] byte1 = objClsSn.GenExcel(dtResult);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "binary/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
            Response.BinaryWrite(byte1);
            Response.Flush();
            Response.End();
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnSearch_Click_BK(object sender, EventArgs e)
        {
            string strStatus = ddlSnStatus.SelectedValue;
            string strContractNo = ddlContractNo.SelectedValue;
            if (!String.IsNullOrEmpty(strContractNo))
            {
                string strTempAbcde, strTempA, strTempB, strTempC, strTempD, strTempE, strTempGhi;
                strTempAbcde = strTempA = strTempB = strTempC = strTempD = strTempE = strTempGhi = string.Empty;
                int iTempMaxGhi, iNewGhi;
                iTempMaxGhi = iNewGhi = 0;
                DataTable dtBind = new DataTable();
                // for gv
                DataColumn dc = new DataColumn("schedule_name");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("item_no");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("equip_name");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("equip_code");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("qty");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("sn_from");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("sn_to");
                dtBind.Columns.Add(dc);
                // for insert
                dc = new DataColumn("contract_po_row_id");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("abcde");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("ghi");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("contract_no");
                dtBind.Columns.Add(dc);

                if (strStatus == "g")
                {
                    // get data from ps_t_sn
                    // show regen if not all gen
                    dtBind = objClsSn.GetGenSnByContractNo(strContractNo, hdnDept.Value);
                    ViewState["dtBinding"] = dtBind;

                }
                else
                {
                    // get data from ps_t_contract_po
                    DataTable dtPoList = objClsSn.GetPoDataByContractNo(strContractNo, hdnDept.Value);
                    // gen

                    foreach (DataRow drPo in dtPoList.Rows)
                    {
                        DataRow drBind = dtBind.NewRow();
                        drBind["contract_po_row_id"] = drPo["rowid"].ToString();
                        drBind["schedule_name"] = drPo["schedule_name"].ToString();
                        drBind["item_no"] = drPo["item_no"].ToString();
                        drBind["equip_name"] = drPo["equip_name"].ToString();
                        drBind["equip_code"] = drPo["equip_code"].ToString();
                        drBind["qty"] = drPo["qty"].ToString();
                        drBind["contract_no"] = strContractNo;
                        if (!String.IsNullOrEmpty(drPo["sn_code_a"].ToString()))
                        {
                            strTempA = drPo["sn_code_a"].ToString();
                            strTempB = !String.IsNullOrEmpty(drPo["sn_code_b"].ToString()) ? drPo["sn_code_b"].ToString() : "0";
                            strTempC = !String.IsNullOrEmpty(drPo["sn_code_c"].ToString()) ? drPo["sn_code_c"].ToString() : "0";
                            strTempD = !String.IsNullOrEmpty(drPo["sn_code_d"].ToString()) ? drPo["sn_code_d"].ToString() : "0";
                            strTempE = !String.IsNullOrEmpty(drPo["sn_code_e"].ToString()) ? drPo["sn_code_e"].ToString() : "0";
                            strTempAbcde = strTempA + strTempB + strTempC + strTempD + strTempE;
                            iTempMaxGhi = objClsSn.GetMaxGhiByAbcde(strTempAbcde);
                            iNewGhi = iTempMaxGhi + 1;
                            drBind["abcde"] = strTempAbcde;
                            drBind["sn_from"] = strTempAbcde + iNewGhi.ToString().PadLeft(3, '0') + "01";
                            drBind["sn_to"] = strTempAbcde + iNewGhi.ToString().PadLeft(3, '0') + drBind["qty"].ToString().PadLeft(2, '0');

                            // update max ghi
                            objClsSn.UpdateMaxGhi(iNewGhi.ToString(), strTempAbcde);
                        }
                        else
                        {
                            drBind["abcde"] = string.Empty;
                            drBind["ghi"] = string.Empty;
                            drBind["sn_from"] = string.Empty;
                            drBind["sn_to"] = string.Empty;
                        }
                        dtBind.Rows.Add(drBind);

                    }
                    // insert ps_t_sn
                    objClsSn.InsertPsTSn(dtBind);
                    // mark flag gen in ps_t_contract_po

                    string strRowIdConCat = string.Empty;
                    foreach (DataRow dr in dtBind.Rows)
                    {
                        strRowIdConCat += strRowIdConCat == string.Empty ? dr["contract_po_row_id"].ToString() : "," + dr["contract_po_row_id"].ToString();
                    }
                    objClsSn.UpdateFlagPoGenSnByContractPoRowIds(strRowIdConCat);

                    ViewState["dtBinding"] = dtBind;
                }
                BindingGv();
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            string strStatus = ddlSnStatus.SelectedValue;
            string strContractNo = ddlContractNo.SelectedValue;
            hdnSelectedContractNo.Value = strContractNo;
            if (!String.IsNullOrEmpty(strContractNo))
            {
                string strTempAbcde, strTempA, strTempB, strTempC, strTempD, strTempE, strTempGhi;
                strTempAbcde = strTempA = strTempB = strTempC = strTempD = strTempE = strTempGhi = string.Empty;
                int iTempMaxGhi, iNewGhi;
                iTempMaxGhi = iNewGhi = 0;
                DataTable dtBind = new DataTable();
                // for gv
                DataColumn dc = new DataColumn("schedule_name");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("item_no");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("equip_name");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("equip_code");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("qty");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("sn_from");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("sn_to");
                dtBind.Columns.Add(dc);
                // for insert
                dc = new DataColumn("contract_po_row_id");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("abcde");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("ghi");
                dtBind.Columns.Add(dc);
                dc = new DataColumn("contract_no");
                dtBind.Columns.Add(dc);

                if (strStatus == "g")
                {
                    lblSelectedContractNo.Text = "Generated S/N of Contract No : " + strContractNo;
                    // get data from ps_t_sn
                    // show regen if not all gen
                    dtBind = objClsSn.GetGenSnByContractNo(strContractNo, hdnDept.Value);
                    ViewState["dtBinding"] = dtBind;
                    BindingGv();
                    btnGenerateSn.Visible = false;
                    btnExport.Visible = true;

                }
                else
                {
                    lblSelectedContractNo.Text = "Preview S/N of Contract No : " + strContractNo;

                    // get data from ps_t_contract_po
                    DataTable dtPoList = objClsSn.GetPoDataByContractNo(strContractNo, hdnDept.Value);
                    // gen
                    DataTable dtTempMaxGhi = new DataTable();
                    DataColumn dcTempMaxGhi = new DataColumn("abcde");
                    dtTempMaxGhi.Columns.Add(dcTempMaxGhi);
                    dcTempMaxGhi = new DataColumn("ghi_max");
                    dtTempMaxGhi.Columns.Add(dcTempMaxGhi);
                    bool isExistInDt = true;

                    foreach (DataRow drPo in dtPoList.Rows)
                    {
                        DataRow drBind = dtBind.NewRow();
                        drBind["contract_po_row_id"] = drPo["rowid"].ToString();
                        drBind["schedule_name"] = drPo["schedule_name"].ToString();
                        drBind["item_no"] = drPo["item_no"].ToString();
                        drBind["equip_name"] = drPo["equip_name"].ToString();
                        drBind["equip_code"] = drPo["equip_code"].ToString();
                        drBind["qty"] = drPo["qty"].ToString();
                        drBind["contract_no"] = strContractNo;
                        strTempAbcde = drPo["abcde"].ToString();
                        drBind["abcde"] = strTempAbcde;

                        if (!String.IsNullOrEmpty(drPo["sn_code_a"].ToString()))
                        {
                            bool containsX = strTempAbcde.Contains("x");
                            if (!containsX)
                            {
                                // select from dtTempMaxGhi
                                DataRow[] drFoundRow = dtTempMaxGhi.Select("abcde = '" + strTempAbcde + "'");
                                if (drFoundRow.Length > 0)
                                {
                                    // get newGhi = max + 1 then update dt = new
                                    iTempMaxGhi = int.Parse(drFoundRow[0]["ghi_max"].ToString());
                                    isExistInDt = true;
                                }
                                else
                                {
                                    // if 0 row get max from GetMaxGhiByAbcde() then insert to dt
                                    iTempMaxGhi = objClsSn.GetMaxGhiByAbcde(strTempAbcde);
                                    isExistInDt = false;
                                }
                                iNewGhi = iTempMaxGhi + 1;

                                drBind["sn_from"] = strTempAbcde + iNewGhi.ToString().PadLeft(3, '0') + "01";
                                drBind["sn_to"] = strTempAbcde + iNewGhi.ToString().PadLeft(3, '0') + drBind["qty"].ToString().PadLeft(2, '0');

                                // update max ghi
                                if (isExistInDt)
                                {
                                    foreach (DataRow row in dtTempMaxGhi.Rows)
                                    {
                                        if (row["abcde"].ToString() == strTempAbcde)
                                        {
                                            row.SetField("ghi_max", "1");
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    DataRow drMaxGhi = dtTempMaxGhi.NewRow();
                                    drMaxGhi["abcde"] = strTempAbcde;
                                    drMaxGhi["ghi_max"] = iNewGhi;
                                }
                                //objClsSn.UpdateMaxGhi(iNewGhi.ToString(), strTempAbcde);
                            }
                        }
                        else
                        {
                            drBind["abcde"] = string.Empty;
                            drBind["ghi"] = string.Empty;
                            drBind["sn_from"] = string.Empty;
                            drBind["sn_to"] = string.Empty;
                        }
                        dtBind.Rows.Add(drBind);

                    }
                    // insert ps_t_sn
                    //objClsSn.InsertPsTSn(dtBind);
                    // mark flag gen in ps_t_contract_po

                   

                    ViewState["dtBinding"] = dtBind;
                    BindingGv();
                    btnGenerateSn.Visible = true;
                    btnExport.Visible = false;
                }
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            ExportExcel();
        }

        protected void btnGenerateSn_Click(object sender, EventArgs e)
        {
            DataTable dtBindPreview = (DataTable)ViewState["dtBinding"];
            bool containX = false;
            string strErrorMessage = string.Empty;

            foreach(DataRow drFindX in dtBindPreview.Rows)
            {
                if (drFindX["abcde"].ToString().Contains("x"))
                {
                    containX = true;
                    strErrorMessage = "Equip code : " + drFindX["equip_code"].ToString() + " has invalid abcde (" + drFindX["abcde"].ToString() + "). Please fix it before generate serial no.";
                    break;
                }
            }

            if (containX)
            {
                Response.Write("<script> alert('"+ strErrorMessage + "');</script>");
            }
            else
            {
                DataTable dtGen = new DataTable();
                dtGen = dtBindPreview.Clone();
                string strTempAbcde = string.Empty;
                int iTempMaxGhi, iNewGhi;

                foreach (DataRow dr in dtBindPreview.Rows)
                {
                    strTempAbcde = dr["abcde"].ToString();
                    iTempMaxGhi = iNewGhi = 0;

                    DataRow drGen = dtGen.NewRow();
                    drGen["schedule_name"] = dr["schedule_name"].ToString();
                    drGen["item_no"] = dr["item_no"].ToString();
                    drGen["equip_name"] = dr["equip_name"].ToString();
                    drGen["equip_code"] = dr["equip_code"].ToString();
                    drGen["qty"] = dr["qty"].ToString();
                    drGen["abcde"] = strTempAbcde;
                    drGen["contract_no"] = dr["contract_no"].ToString();
                    drGen["contract_po_row_id"] = dr["contract_po_row_id"].ToString();
                    //drGen["sn_from"] = dr["sn_from"].ToString();
                    //drGen["sn_to"] = dr["sn_to"].ToString();

                    iTempMaxGhi = objClsSn.GetMaxGhiByAbcde(strTempAbcde);
                    iNewGhi = iTempMaxGhi + 1;
                    drGen["sn_from"] = strTempAbcde + iNewGhi.ToString().PadLeft(3, '0') + "01";
                    drGen["sn_to"] = strTempAbcde + iNewGhi.ToString().PadLeft(3, '0') + drGen["qty"].ToString().PadLeft(2, '0');

                    // update max ghi
                    objClsSn.UpdateMaxGhi(iNewGhi.ToString(), strTempAbcde);

                    dtGen.Rows.Add(drGen);

                   
                }
                objClsSn.InsertPsTSn(dtGen);
                string strRowIdConCat = string.Empty;
                foreach (DataRow dr2 in dtGen.Rows)
                {
                    strRowIdConCat += strRowIdConCat == string.Empty ? dr2["contract_po_row_id"].ToString() : "," + dr2["contract_po_row_id"].ToString();
                }
                objClsSn.UpdateFlagPoGenSnByContractPoRowIds(strRowIdConCat);

                ViewState["dtBinding"] = dtGen;
                BindingGv();
                lblSelectedContractNo.Text = "Generated S/N of Contract No : " + hdnSelectedContractNo.Value;

                btnExport.Visible = true;
                btnGenerateSn.Visible = false;
                BindDdlContractNo();
               // btnSearch.Visible = false;
            }

        }
    }
}