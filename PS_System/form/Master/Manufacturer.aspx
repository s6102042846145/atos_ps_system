﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Manufacturer.aspx.cs" Inherits="PS_System.form.Master.Manufacturer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
   
    </style>
    
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

       <script>
           $(document).keypress(
               function (event) {
                   if (event.which == '13') {
                       event.preventDefault();
                   }
               });
           function NumOnlyChk(e) {
               var key;
               if (window.event) // IE
               {
                   key = e.keyCode;
               }
               else if (e.which) // Netscape/Firefox/Opera
               {
                   key = e.which;
               }
               var vchar = String.fromCharCode(key);
               if (((vchar >= "0" && vchar <= "9") && (key != 8) || ((vchar == "." || key == 49) && (key != 8)))) {

                   e.returnValue = true;
               }
               else {
                   return false;
               }
               e.returnValue = true;
               return true;
           }


       </script>
</head>
<body>
    <form id="form1" runat="server">
         <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                     
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Manufacturer"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width:100%;padding:10px">
            <table style="width:100%">
                <tr>
                    <td>
                    <button onclick="document.getElementById('myCreate').style.display='block';return false;" class="btn btn-primary"  >Create</button>
                    </td>
                </tr>
                <tr>
                    <td>
                          <asp:GridView ID="gvManu" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  style="margin-right:20px" OnRowCommand="gvManu_RowCommand" >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Width="30px"/>
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066"  Height="30px"/>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <Columns>
                                  <asp:TemplateField HeaderText="Manufacturer Code">
                                        <ItemTemplate>
                                           <asp:Label ID="gvManu_code" runat="server" Text='<%# Bind("manufac_code") %>'></asp:Label>
                                           <asp:Label ID="gvManu_rowid" runat="server" visible="false" Text='<%# Bind("rowid") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  Width="20%"/>
                                    </asp:TemplateField>
                                <asp:TemplateField HeaderText="Manufacturer Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvManu_name" runat="server" Text='<%# Bind("manufac_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  Width="25%"/>
                                    </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Address">
                                        <ItemTemplate>
                                            <asp:Label ID="gvManu_addr" runat="server" Text='<%# Bind("manufac_address") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  Width="30%"/>
                                    </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:Label ID="gvManu_remark" runat="server" Text='<%# Bind("manufac_remark") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  Width="20%"/>
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="btnEdit" ClientIDMode="Static" runat="server" ImageUrl="../../Images/edit.png" CommandName="editdata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Edit" Width="20" ImageAlign="NotSet" />

                                           <asp:ImageButton ID="btnDel" ClientIDMode="Static" runat="server" ImageUrl="../../Images/bin.png" CommandName="deletedata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Edit" Width="20" ImageAlign="NotSet"  />
                         
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
         <div id="myCreate"  runat="server" class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                      <tr>
                           <td style="text-align: left; color: #006699;"><b>Create</b></td>
                     </tr>
                     <tr>
                     <td style="padding-left:15px">
                         <asp:Label ID="Label1" runat="server" Text="Manufacturer Code:"></asp:Label>
                          &nbsp;<asp:TextBox ID="txtCode" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                          &nbsp; &nbsp; <asp:Label ID="Label2" runat="server" Text="Manufacturer Name:"></asp:Label>
                          &nbsp; <asp:TextBox ID="txtName" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                          &nbsp; &nbsp; <asp:Label ID="Label3" runat="server" Text="Address:"></asp:Label>
                          &nbsp; <asp:TextBox ID="txtAddress" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                          &nbsp; &nbsp; <asp:Label ID="Label4" runat="server" Text="Remark:"></asp:Label>
                          &nbsp; <asp:TextBox ID="txtRemark" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px" Rows="2" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    </tr>
                      <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                              <asp:Button ID="btnSaveCre" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSaveCre_Click"  />
                             &nbsp; <asp:Button ID="btnCloseCre" runat="server" Text="Close" class="btn btn-danger" OnClick="btnCloseCre_Click" />
                         </td>
                     </tr>
                     </table>
                </div>
             </div>

         <div id="myEdit"  runat="server" class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                     <tr>
                           <td style="text-align: left; color: #006699;"><b>Edit</b></td>
                     </tr>
                     <tr>
                     <td style="padding-left:15px">
                          <asp:Label ID="Label5" runat="server" Text="Manufacturer Code:"></asp:Label>
                          &nbsp;<asp:TextBox ID="txtEdit_code" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px" ReadOnly="true"></asp:TextBox>
                          &nbsp; &nbsp;  <asp:Label ID="Label6" runat="server" Text="Manufacturer Name:"></asp:Label>
                          &nbsp; <asp:TextBox ID="txtEdit_name" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                          &nbsp; &nbsp; <asp:Label ID="Label7" runat="server" Text="Address:"></asp:Label>
                          &nbsp; <asp:TextBox ID="txtEdit_addr" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="200px"></asp:TextBox>
                          &nbsp; &nbsp; <asp:Label ID="Label8" runat="server" Text="Remark:"></asp:Label>
                          &nbsp; <asp:TextBox ID="txtEdit_remark" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px" Rows="2" TextMode="MultiLine"></asp:TextBox>
                    </td>
                    </tr>
                                 <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                              <asp:Button ID="btnSaveEd" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSaveEd_Click"   />
                             &nbsp; <asp:Button ID="btnCloseEd" runat="server" Text="Close" class="btn btn-danger" OnClick="btnCloseEd_Click"  />
                         </td>
                     </tr>
                     </table>
                </div>
             </div>
        <asp:HiddenField ID="hidLogin" runat="server" />
         <asp:HiddenField ID="hidRowid" runat="server" />
    </form>
</body>
</html>
