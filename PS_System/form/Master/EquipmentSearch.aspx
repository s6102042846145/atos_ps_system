﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EquipmentSearch.aspx.cs" Inherits="PS_System.form.Master.EquipmentSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server"> 
    <title>Equipment Master</title>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.js"></script>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <script type="text/javascript">

        $(document).ready(function () {
            $("input[type=text]").bind("keypress", function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    return false;
                }
                return true;
            });
        });
    </script>
    <style>
        .table table tbody tr td a,
        .table table tbody tr td span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .table table > tbody > tr > td > span {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .table table > tbody > tr > td:first-child > a,
        .table table > tbody > tr > td:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .table table > tbody > tr > td:last-child > a,
        .table table > tbody > tr > td:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .table table > tbody > tr > td > a:hover,
        .table table > tbody > tr > td > span:hover,
        .table table > tbody > tr > td > a:focus,
        .table table > tbody > tr > td > span:focus {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }
    </style>
</head>
<body>
    <div style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px">
        <form id="form1" runat="server">
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Equipment Master"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table style="font-family: Tahoma; width: 100%;">
                    <tr>
                        <td style="width: 200px">
                            <b>Equipment Code : </b>
                        </td>
                        <td style="width: 500px">
                            <asp:TextBox ID="tbxEquipCode" runat="server" Width="500px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Equipment Description : </b>
                        </td>
                        <td>
                            <asp:TextBox ID="tbxEquipDesc" runat="server" Width="500px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Legend : </b>
                        </td>
                        <td>
                            <asp:TextBox ID="tbxLegend" runat="server" Width="500px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                             <tr>
                        <td>
                            <b>Status : </b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlStatus" runat="server" Width="150px">
                                <asp:ListItem Text="<-- Select -->" Value=""></asp:ListItem>
                                <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnCreate" CssClass="btn btn-primary" runat="server" Text="Add Equipment" OnClick="btnCreate_Click" />
                            <asp:Button ID="btnExport" CssClass="btn btn-primary" runat="server" Text="Export" OnClick="btnExport_Click" Visible="false" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
            <div style="width: 90%">
                <asp:GridView ID="gvEquipList" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="90%"
                    Visible="true" ClientIDMode="Static" Font-Size="9pt" PageSize="10" BorderStyle="None"
                    AllowPaging="true" OnPageIndexChanging="gvEquipList_PageIndexChanging">
                        <HeaderStyle  Font-Bold="True" ForeColor="White" Height="30px" />
                    <Columns>
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Equipment Code">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipCode" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Equipment Full Description ">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipFull" runat="server" Text='<%# Bind("equip_desc_full") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="35%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Equipment Short Description">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipShot" runat="server" Text='<%# Bind("equip_desc_short") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />

                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Department">
                            <ItemTemplate>
                                <asp:Label ID="lblDept" runat="server" Text='<%# Bind("equip_gong") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Legend">
                            <ItemTemplate>
                                <asp:Label ID="lblLegend" runat="server" Text='<%# Bind("legend") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Unit">
                            <ItemTemplate>
                                <asp:Label ID="lblUnit" runat="server" Text='<%# Bind("unit") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                               <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("acStatus") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                <%--<asp:HyperLink ID="linkEdit" runat="server" NavigateUrl='<%# "Equipment.aspx?equipcode=" + Eval("equip_code") %>'>Edit</asp:HyperLink>--%>
                                <asp:HyperLink ID="linkEdit" runat="server" NavigateUrl='<%# GetUrl(Eval("equip_code"))%>'>Edit</asp:HyperLink>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
        </form>
    </div>
</body>
</html>
