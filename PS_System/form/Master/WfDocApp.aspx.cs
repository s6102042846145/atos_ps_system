﻿using PS_Library;
using PS_Library.DocumentManagement;
using PS_Library.WorkflowService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class WfDocApp : System.Web.UI.Page
    {
        private string strParentPdfNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString(); //ConfigurationManager.AppSettings["wf_job_att_node_id"].ToString();
        private Node doc_node = new Node();

        public string zisDev = ConfigurationManager.AppSettings["isDev"].ToString();
        public string zmode = "";
        public string zworkid = "";
        public string zsubworkid = "";
        public string ztaskid = "";
        public string zpsdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string zhost_name = ConfigurationManager.AppSettings["HOST_NAME"].ToString();
        public string strWfMapId = ConfigurationManager.AppSettings["wf_doc_approved_map_id"].ToString();

        public EmpInfoClass zempInfo = new EmpInfoClass();
        //clsWfPsAPV01 objWf = new clsWfPsAPV01();
        clsUploadDocApp objWf = new clsUploadDocApp();

        static PS_Library.OTFunctions zotFunctions = new PS_Library.OTFunctions();
        static PS_Library.DbControllerBase zdbUtil = new PS_Library.DbControllerBase();
        static PS_Library.LogHelper zLogHelper = new PS_Library.LogHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

            //TestStartWf();
            //TestStartWfBid();

            if (!IsPostBack)
            {

                try { zmode = Request.QueryString["zmode"].ToString().ToUpper(); }
                catch { zmode = "approver"; }
                try { zworkid = Request.QueryString["workid"].ToString().ToUpper(); }
                catch { zworkid = "2817591"; }
                try { zsubworkid = Request.QueryString["subworkid"].ToString().ToUpper(); }
                catch { zsubworkid = "2817591"; }
                try { ztaskid = Request.QueryString["taskid"].ToString().ToUpper(); }
                catch { ztaskid = "1"; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); }
                catch { hidLogin.Value = "z575798"; }


                hidMode.Value = zmode;
                hidWorkID.Value = zworkid;
                hidSubworkID.Value = zsubworkid;
                hidTaskID.Value = ztaskid;
                LoadData();
            }
        }

        private void LoadData()
        {

            DataTable dtData = objWf.GetDADataByWorkId(hidWorkID.Value); //objWf.GetDPDataByWorkId(hidWorkID.Value);
            ViewState["wf_data"] = dtData;
            if (dtData.Rows.Count > 0)
            {
                lblRequestorVal.Text = dtData.Rows[0]["emp_name"].ToString() + " (" + dtData.Rows[0]["emp_position"].ToString() + ")";
                lblSubjectVal.Text = dtData.Rows[0]["memo_subject"].ToString();
                hidProcessID.Value = dtData.Rows[0]["process_id"].ToString();
                lblPsDaNo.Text = dtData.Rows[0]["ps_da_no"].ToString();
                hidDocNodeId.Value = dtData.Rows[0]["doc_node_id"].ToString();
                hidApproverOrder.Value = dtData.Rows[0]["apv_order"].ToString();
                try { hidDocNodeIdPrev.Value = dtData.Rows[0]["doc_node_id_prev"].ToString(); } catch { }


                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    if (String.IsNullOrEmpty(dtData.Rows[i]["apv_status"].ToString()))
                    {
                        lblToVal.Text = dtData.Rows[i]["apv_name"].ToString() + " (" + dtData.Rows[i]["apv_position"].ToString() + ")";
                        if (i < (dtData.Rows.Count - 1))
                        {
                            hidNextApproverId.Value = dtData.Rows[i + 1]["apv_id"].ToString();
                        }
                        break;
                    }
                }
                txtComment.Text = dtData.Rows[0]["note"].ToString();
                // get Memo
                getMemo(dtData.Rows[0]["doc_node_id_memo"].ToString());
                BindingGvDocList();
            }

        }

        private void BindingGvDocList()
        {
            DataTable dtDocList = objWf.GetDocListByNodeId(hidDocNodeId.Value);
            gvDocAppList.DataSource = dtDocList;
            gvDocAppList.DataBind();
        }

        private void getMemo(string strMemoNodeId)
        {
            string xdoc_url = "https://" + zhost_name + "/tmps/forms/viewCSDoc?docnodeid=" + strMemoNodeId;
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");
            ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            //complete wf
            ApplicationData[] appDatas;
            appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
            AttributeData wfAttr = new AttributeData();
            for (int i = 0; i < appDatas.Length; i++)
            {
                try
                {
                    wfAttr = (AttributeData)appDatas[i];
                    break;
                }
                catch (Exception ex)
                {
                    string strError = ex.Message;
                }
            }
            if (wfAttr != null)
            {
                zotFunctions.setwfAttrValue(ref wfAttr, "isFinish", "True");
                zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "APPROVED");
            }
            zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
            //update apv detail status , date
            objWf.UpdateWfAppDocStatusDetail(hidProcessID.Value, hidLogin.Value, "APPROVED");
            // check next approver?

            if (String.IsNullOrEmpty(hidNextApproverId.Value)) // case last approver
            {
                //update status WF doc approval master
                objWf.UpdateWfAppDocStatus("COMPLETED", hidLogin.Value, hidProcessID.Value, hidApproverOrder.Value, txtComment.Text);
                //update status doc_approve inactive last ver  and active new ver

                string[] arrDocNodeId = hidDocNodeId.Value.Split(',');
                if (arrDocNodeId.Length > 0)
                {
                    foreach (string docNodeId in arrDocNodeId)
                    {
                        objWf.UpdateMDocApproveStatus("Active", hidLogin.Value, docNodeId);
                    }
                }
                else
                {
                    objWf.UpdateMDocApproveStatus("Active", hidLogin.Value, hidDocNodeId.Value);
                }
                if (!String.IsNullOrEmpty(hidDocNodeIdPrev.Value))
                {
                    objWf.UpdateMDocApproveStatus("InActive", hidLogin.Value, hidDocNodeIdPrev.Value);
                }
                //update wf status to be complete
                DataTable dtGetProcess = (DataTable)ViewState["wf_data"];
                objWf.UpdateWfFormComplete(dtGetProcess.Rows[0]["process_id_ref"].ToString(), hidLogin.Value);
            }
            else // more approver 
            {
                //update status WF doc approval master
                objWf.UpdateWfAppDocStatus("PENDING_AAPROVED", hidLogin.Value, hidProcessID.Value, hidApproverOrder.Value, txtComment.Text);
                //start new wf update apv_status and date
                DataTable dtWfData = objWf.GetDADataByWorkId(hidWorkID.Value);//(DataTable)ViewState["wf_data"];
                // master
                WfPsDocApprove objDocApprove = new WfPsDocApprove();
                objDocApprove.created_by = hidLogin.Value;
                objDocApprove.doc_node_id = dtWfData.Rows[0]["doc_node_id"].ToString();
                objDocApprove.doc_node_id_prev = dtWfData.Rows[0]["doc_node_id_prev"].ToString();
                objDocApprove.emp_id = dtWfData.Rows[0]["emp_id"].ToString();
                objDocApprove.emp_name = dtWfData.Rows[0]["emp_name"].ToString();
                objDocApprove.emp_position = dtWfData.Rows[0]["emp_position"].ToString();
                objDocApprove.emp_position_full = dtWfData.Rows[0]["emp_position_full"].ToString();
                objDocApprove.memo_content = dtWfData.Rows[0]["memo_content"].ToString();
                objDocApprove.memo_subject = dtWfData.Rows[0]["memo_subject"].ToString();
                objDocApprove.note = dtWfData.Rows[0]["note"].ToString();
                objDocApprove.process_id_ref = dtWfData.Rows[0]["process_id_ref"].ToString();
                objDocApprove.ps_subject = dtWfData.Rows[0]["ps_subject"].ToString();
                objDocApprove.updated_by = hidLogin.Value;

                // detail
                List<WfPsDocApproveDetails> lstObjDocApproveDetail = new List<WfPsDocApproveDetails>();
                WfPsDocApproveDetails objDocApproveDetail;
                foreach (DataRow dr in dtWfData.Rows)
                {
                    objDocApproveDetail = new WfPsDocApproveDetails();
                    objDocApproveDetail.apv_order = int.Parse(dr["apv_order"].ToString()); 
                    objDocApproveDetail.apv_id = dr["apv_id"].ToString();
                    objDocApproveDetail.apv_name = dr["apv_name"].ToString();
                    objDocApproveDetail.apv_position = dr["apv_position"].ToString();
                    objDocApproveDetail.apv_position_full = dr["apv_position_full"].ToString();
                    try
                    {
                        objDocApproveDetail.apv_status = dr["apv_status"].ToString();
                        objDocApproveDetail.apv_apvdate = DateTime.Parse(dr["apv_apvdate"].ToString());
                    }
                    catch { }
                    lstObjDocApproveDetail.Add(objDocApproveDetail);
                }
                objWf.StartWF(ref objDocApprove, lstObjDocApproveDetail);
            }
            Response.Write("<script> alert('The document has been approved.');</script>");
            string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            ApplicationData[] appDatas;
            appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
            AttributeData wfAttr = new AttributeData();
            for (int i = 0; i < appDatas.Length; i++)
            {
                try
                {
                    wfAttr = (AttributeData)appDatas[i];
                    break;
                }
                catch (Exception ex)
                {
                    string strError = ex.Message;
                }
            }
            if (wfAttr != null)
            {
                zotFunctions.setwfAttrValue(ref wfAttr, "isFinish", "False");
                zotFunctions.setwfAttrValue(ref wfAttr, "WF_Status", "REJECTED");
            }
            zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
            //update apv detail status , date
            objWf.UpdateWfAppDocStatusDetail(hidProcessID.Value, hidLogin.Value, "REJECTED");
            Response.Write("<script> alert('The document has been rejected.');</script>");
            string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
        }
        protected void imgDLCilck(object sender, EventArgs e)
        {
            //select the row
            ImageButton imageButton = (ImageButton)sender;
            string strNodeId = imageButton.CommandArgument;
            objWf.DownloadDocCsByNodeId(strNodeId);
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
         

    }
}