﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EgatSerialNo.aspx.cs" Inherits="PS_System.form.Master.EgatSerialNo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>EGAT Serial No.</title>
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.js"></script>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <style>
        .table table tbody tr td a,
        .table table tbody tr td span {
            position: relative;
            float: left; 
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .table table > tbody > tr > td > span {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .table table > tbody > tr > td:first-child > a,
        .table table > tbody > tr > td:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .table > tbody > tr > td {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: middle;
        }

        .table table > tbody > tr > td:last-child > a,
        .table table > tbody > tr > td:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .table table > tbody > tr > td > a:hover,
        .table table > tbody > tr > td > span:hover,
        .table table > tbody > tr > td > a:focus,
        .table table > tbody > tr > td > span:focus {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }

        .mydropdownlist {
            color: black;
            font-size: 11px;
            font-family: Tahoma;
            padding: 2px 5px;
            border-radius: 2px;
            background-color: white;
            font-weight: normal;
        }

        th {
            text-align: center;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 11px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $("input[type=text]").bind("keypress", function (evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode == 13) {
                    return false;
                }
                return true;
            });
        });
        function ShowPopUp(type) {
            if (type == 'R') {
                document.getElementById('divRelatedEquipBom').style.display = 'block';
            }
            else if (type == 'S') {
                document.getElementById('divAddStdDoc').style.display = 'block';
            }
            else if (type == 'V') {
                document.getElementById('divAddVersionDoc').style.display = 'block';
            }
        }


        //function disableKeyEnter(evt) {
        //    evt = (evt) ? evt : window.event;
        //    var charCode = (evt.which) ? evt.which : evt.keyCode;
        //    if (charCode == 13) {
        //        return false;
        //    }
        //    return true;
        //}




        function showPleaseWait() {
            if (document.querySelector("#pleaseWaitDialog") == null) {
                var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
                                        <div class="modal-dialog">\
                                            <div class="modal-content">\
                                                <div class="modal-header">\
                                                    <h4 class="modal-title">Please wait...</h4>\
                                                </div>\
                                                <div class="modal-body">\
                                                    <div class="progress">\
                                                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                                                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                                                      </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>';
                $(document.body).append(modalLoading);
            }

            $("#pleaseWaitDialog").modal("show");
        }

        $(function () {
            $(".picker").datepicker();
        });

        /**
         * Hides "Please wait" overlay. See function showPleaseWait().
         */
        function hidePleaseWait() {
            $("#pleaseWaitDialog").modal("hide");
        }
    </script>
</head>
<body>
    <div style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma; font-size: 11px">
        <form id="form1" runat="server">
            <div>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - EGAT Serial No."></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table style="font-family: Tahoma; width: 100%; padding: 2px 0px 2px 0px;">
                     <tr>
                        <td style="width: 120px; padding: 2px 0px 2px 0px;">
                            <b>Serial No. Status: </b>
                        </td>
                        <td style="width: 1000px; padding: 2px 0px 2px 0px;">
                            <asp:DropDownList ID="ddlSnStatus" runat="server" CssClass="mydropdownlist" AutoPostBack="true" OnSelectedIndexChanged="ddlSnStatus_SelectedIndexChanged">
                              <asp:ListItem Text="Generated" Value="g" />
                              <asp:ListItem Text="Not Generated" Value="n" />
                            </asp:DropDownList>
                        </td>
                       <td></td>
                    </tr>
                    <tr>
                        <td style="width: 120px; padding: 2px 0px 2px 0px;">
                            <b>Contract No. : </b>
                        </td>
                        <td style="width: 1000px; padding: 2px 0px 2px 0px;">
                            <asp:DropDownList ID="ddlContractNo" runat="server" CssClass="mydropdownlist"></asp:DropDownList>
                            <asp:HiddenField ID="hdnSelectedContractNo" runat="server" />
                            <asp:HiddenField ID="hdnDept" runat="server" />
                        </td>
                       <td></td>
                    </tr>
                    <tr>
                        <td colspan="3" style="padding: 2px 0px 2px 0px;">
                            <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btnSearch_Click"  />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divContractNo" runat="server">
                <asp:Label ID="lblSelectedContractNo" runat="server" Text="" Font-Bold="true" Font-Size="Larger"></asp:Label>
            </div>
            <div id="divSnList" runat="server" style="width: 90%">
                <asp:GridView ID="gvSnList" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="100%"
                    Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10"
                    AllowPaging="true" OnPageIndexChanging="gvSnList_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Schedule Name">
                            <ItemTemplate>
                                <asp:Label ID="lblScheduleName" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="8%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Item No.">
                            <ItemTemplate>
                                <asp:Label ID="lblItemNo" runat="server" Text='<%# Eval("item_no") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="7%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Equipment">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipName" runat="server" Text='<%# Bind("equip_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="35%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="EQ.(Old mat.)">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipCode" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="8%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="ABCDE">
                            <ItemTemplate>
                                <asp:Label ID="lblEquipCode" runat="server" Text='<%# Bind("abcde") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="8%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Qty">
                            <ItemTemplate>
                                <asp:Label ID="lblQty" runat="server" Text='<%# Bind("qty") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="7%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Egat S/N From">
                            <ItemTemplate>
                                <asp:Label ID="lblSnFrom" runat="server" Text='<%# Bind("sn_from") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="9%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Egat S/N to">
                            <ItemTemplate>
                                <asp:Label ID="lblSnTo" runat="server" Text='<%# Bind("sn_to") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="9%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
            <div id="divExport" runat="server">
                <table>
                    <tr>
                        <td colspan="3" style="padding: 2px 0px 2px 0px;">
                            <asp:Button ID="btnGenerateSn" CssClass="btn btn-primary" runat="server" Text="Generate S/N" OnClick="btnGenerateSn_Click"/>
                            <asp:Button ID="btnExport" CssClass="btn btn-primary" runat="server" Text="Export" OnClick="btnExport_Click" />
                        </td>
                    </tr>
                </table>
            </div>
          
        </form>
    </div>
</body>
</html>

