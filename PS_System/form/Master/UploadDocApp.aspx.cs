﻿using PS_Library;
using PS_Library.DocumentManagement;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class UploadDocApp : System.Web.UI.Page
    {

        clsUploadDocApp objUploadDocApp = new clsUploadDocApp();
        public string strEmpIdLogin = string.Empty;
        //private string strUserLogin = "Admin";//strEmpIdLogin

        #region Control Event

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["zuserlogin"] != null)
            {
                strEmpIdLogin = Request.QueryString["zuserlogin"];

            }
            else
            {
                strEmpIdLogin = "z593503";// for test only
            }
            if (!IsPostBack)
            {
                BindPosition(strEmpIdLogin);
                BindDdlDocType();
                divApproval.Visible = cbxNeedApprove.Checked ? true : false;
            }
        }

        protected void imgUploadFile_Click(object sender, ImageClickEventArgs e)
        {
            if (ddlDocType.SelectedValue != string.Empty)
            {
                long nodeId = 0;
                lblErrorMsg.Text = string.Empty;
                DataTable dtLastVer = new DataTable();
                DataTable dtUpload = (DataTable)ViewState["dtAddDoc"];
                if (dtUpload == null)
                {
                    dtUpload = new DataTable();
                    dtUpload.Columns.Add("doc_nodeid");
                    dtUpload.Columns.Add("doc_remark");
                    dtUpload.Columns.Add("doc_revision_no");
                    dtUpload.Columns.Add("doc_desc");
                    dtUpload.Columns.Add("doc_sheetno");
                    dtUpload.Columns.Add("doc_name");
                }
                DataRow dr;

                if ((fileUploadDoc.PostedFile != null) && (fileUploadDoc.PostedFile.ContentLength > 0))
                {
                    foreach (HttpPostedFile uploadedFile in fileUploadDoc.PostedFiles)
                    {
                        string strNewFileName = uploadedFile.FileName;
                        DataRow[] foundFileName = dtUpload.Select("doc_name = '" + strNewFileName + "'");
                        if (foundFileName.Length == 0)
                        {
                            // do something...

                            //if()

                            dtLastVer = new DataTable();
                            //uploadedFile.InputStream
                            Stream fs = uploadedFile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            byte[] bytesUl = br.ReadBytes((Int32)fs.Length);
                            nodeId = UploadDocToCs(false, ddlDocType.SelectedItem.Text, bytesUl, uploadedFile.FileName, uploadedFile.FileName);
                            if (nodeId > 0)
                            {
                                dtLastVer = objUploadDocApp.GetLastRevisionDocApprove(uploadedFile.FileName, ddlDocType.SelectedValue);
                                dr = dtUpload.NewRow();
                                dr["doc_nodeid"] = nodeId.ToString();
                                dr["doc_remark"] = dtLastVer.Rows.Count > 0 ? dtLastVer.Rows[0]["doc_remark"].ToString() : string.Empty;
                                dr["doc_desc"] = dtLastVer.Rows.Count > 0 ? dtLastVer.Rows[0]["doc_desc"].ToString() : string.Empty;
                                dr["doc_sheetno"] = dtLastVer.Rows.Count > 0 ? dtLastVer.Rows[0]["doc_sheetno"].ToString() : string.Empty;
                                dr["doc_name"] = dtLastVer.Rows.Count > 0 ? dtLastVer.Rows[0]["doc_name"].ToString() : uploadedFile.FileName;

                                if (dtLastVer.Rows.Count > 0)
                                {
                                    if (!String.IsNullOrEmpty(dtLastVer.Rows[0]["doc_revision_no"].ToString()))
                                    {
                                        try
                                        {
                                            dr["doc_revision_no"] = (int.Parse(dtLastVer.Rows[0]["doc_revision_no"].ToString()) + 1).ToString();
                                        }
                                        catch
                                        {
                                            dr["doc_revision_no"] = "0";
                                        }
                                    }
                                    else
                                    {
                                        dr["doc_revision_no"] = "0";
                                    }
                                }
                                else
                                {
                                    dr["doc_revision_no"] = "0";
                                }
                                dtUpload.Rows.Add(dr);
                            }
                        }
                    }
                    if (dtUpload.Rows.Count > 0)
                    {
                        ViewState["dtAddDoc"] = dtUpload;
                        gvDocUpload.DataSource = dtUpload;
                        gvDocUpload.DataBind();
                    }

                }
            }
            else
            {
                lblErrorMsg.Text = "Please select document type before uploading.";
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            lblErrorMsg.Text = string.Empty;
            if (ValidateSubmit() == string.Empty)
            {
                AddDocApp();
                if (cbxNeedApprove.Checked) // start wf
                {

                    //put approver position to array
                    DataTable dtSelectedApprover = (DataTable)ViewState["dtSelectedApprover"];
                    string[] arrApvId = new string[dtSelectedApprover.Rows.Count];
                    for (int i = 0; i < dtSelectedApprover.Rows.Count; i++)
                    {
                        //arrApvPostion[i] = dtSelectedApprover.Rows[0]["POSITION_SHORT"].ToString();
                        arrApvId[i] = dtSelectedApprover.Rows[i]["id"].ToString();
                    }
                    //start wf get wf processid
                    WfPsDocApprove wfObj = new WfPsDocApprove();
                    DataTable dtUplaod = (DataTable)ViewState["dtAddDoc"];
                    foreach (DataRow dr in dtUplaod.Rows)
                    {
                        wfObj.doc_node_id += String.IsNullOrEmpty(wfObj.doc_node_id)? dr["doc_nodeid"].ToString() : "," + dr["doc_nodeid"].ToString();
                    }
                    //wfObj.doc_node_id_prev = strLastVerNodeId;
                    wfObj.created_by = strEmpIdLogin;
                    wfObj.updated_by = strEmpIdLogin;
                    wfObj.emp_id = strEmpIdLogin;
                    wfObj.memo_content = tbxContent.Text.Trim();
                    wfObj.memo_subject = tbxSubject.Text.Trim();
                    objUploadDocApp.StartWF(ref wfObj, arrApvId);
                }
                ClearValue();
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
                Response.Redirect("UploadDocAppsearch?zuserlogin=" + strEmpIdLogin);

            }
            else
            {
                lblErrorMsg.Text = ValidateSubmit();
            }
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            // ClearValue();
            Response.Redirect("UploadDocAppsearch?zuserlogin=" + strEmpIdLogin);
        }

        protected void OnRowGvDocDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["dtAddDoc"] as DataTable;
            string strNodeId = dt.Rows[index]["doc_nodeid"].ToString();
            DeleteNode(strNodeId);
            dt.Rows[index].Delete();
            ViewState["dtAddDoc"] = dt;
            gvDocUpload.DataSource = dt;
            gvDocUpload.DataBind();
        }

        private void BindDdlDocType()
        {
            ddlDocType.Items.Clear();
            DataTable dtDocType = objUploadDocApp.GetDocTypeAll();
            ddlDocType.Items.Add(new ListItem("Please select document type ", ""));
            foreach (DataRow dr in dtDocType.Rows)
            {
                ddlDocType.Items.Add(new ListItem(dr["doctype_name"].ToString(), dr["doctype_code"].ToString()));
            }
        }

        private void BindPosition(string strempid)
        {
            Employee emp = new Employee();
            EmpInfoClass empinfo = new EmpInfoClass();
            emp = empinfo.getInFoByEmpID(strempid);

            tbxDepartment.Text = emp.ORGSNAME4;
            tbxSection.Text = emp.ORGSNAME5;
        }


        #endregion

        #region cs function

        public long UploadDocToCs(bool bNeedApprove, string strDocTypeName, byte[] byteContent, string strDocName, string StrFileName)
        {
            OTFunctions ot = new OTFunctions();
            string strParentIdToUpload = string.Empty;
            long nodeID = 0;
            // get root Folder
            string zFolderTransmittionNodeID = ConfigurationManager.AppSettings["FolderTransmittionSystemNodeID"].ToString();

            // check need approve
            if (!bNeedApprove)
            {
                if (!String.IsNullOrEmpty(strDocTypeName))
                {
                    // Check folder Engineering Design master isExist
                    Node nodeEng = ot.getNodeByName(zFolderTransmittionNodeID, "Engineering Design Master");
                    if (nodeEng == null)
                    { // if not exist : create
                        nodeEng = ot.createFolder(zFolderTransmittionNodeID, "Engineering Design Master");
                    }
                    // check folder doc type isExist
                    Node nodeDocType = ot.getNodeByName(nodeEng.ID.ToString(), strDocTypeName.Trim());
                    if (nodeDocType == null)
                    { // if not exist : create
                        nodeDocType = ot.createFolder(nodeEng.ID.ToString(), strDocTypeName.Trim());
                    }
                    // check doc type is Drawing? 
                    if (strDocTypeName.Trim() == "Drawings")
                    {
                        Node nodeStdDrawing = ot.getNodeByName(nodeDocType.ID.ToString(), "00 - Standard Drawing");
                        if (nodeStdDrawing == null)
                        {
                            nodeStdDrawing = ot.createFolder(nodeDocType.ID.ToString(), "00 - Standard Drawing");
                        }
                        strParentIdToUpload = nodeStdDrawing.ID.ToString();
                    }
                    else
                    {
                        strParentIdToUpload = nodeDocType.ID.ToString();
                    }
                    // upload doc to cs 
                    NodeInfo nodeInfo = new NodeInfo();
                    nodeID = ot.uploadDoc(strParentIdToUpload, strDocName, StrFileName, byteContent, strDocName);
                    // get node id

                }

            }
            else
            {
                // need more logic about approval structure
            }
            return nodeID;
        }

        public void DeleteNode(string strNodeId)
        {

            OTFunctions ot = new OTFunctions();
            Node thisNode = ot.getNodeByID(strNodeId);
            if (thisNode != null)
            {
                ot.deleteNodeID(thisNode.ParentID.ToString(), strNodeId);
            }
        }
        #endregion

        #region master data

        private DataTable GetDocType()
        {
            DataTable docT = new DataTable();
            docT = objUploadDocApp.GetDocTypeAll();
            return docT;
        }

        private void AddDocApp()
        {
            List<DocApprove> listDa = new List<DocApprove>();
            DocApprove da;
            foreach (GridViewRow row in gvDocUpload.Rows)
            {
                da = new DocApprove();
                if (row.RowType == DataControlRowType.DataRow)
                {
                    //HyperLink myHyperLink = row.FindControl("myHyperLinkID") as HyperLink;
                    TextBox tbxFileName = row.FindControl("tbxFileName") as TextBox;
                    TextBox tbxDocSheetNo = row.FindControl("tbxDocSheetNo") as TextBox;
                    TextBox tbxDocDesc = row.FindControl("tbxDocDesc") as TextBox;
                    HiddenField hdnRevision = row.FindControl("hdnRevision") as HiddenField;
                    TextBox tbxRevisionDis = row.FindControl("tbxRevisionDis") as TextBox;
                    TextBox tbxRemark = row.FindControl("tbxRemark") as TextBox;
                    Label lblNodeID = row.FindControl("lblNodeID") as Label;

                    string strExtensionFile = Path.GetExtension(tbxFileName.Text);
                    string strFileName = tbxFileName.Text.Replace(strExtensionFile, string.Empty) + "_" + tbxRevisionDis.Text.Trim() + strExtensionFile;

                    da.strDep = tbxDepartment.Text;
                    da.strDocDesc = tbxDocDesc.Text;
                    da.strDocName = strFileName;
                    da.strDocSheet = tbxDocSheetNo.Text;
                    da.strDocType = ddlDocType.SelectedValue;
                    da.strNodeId = lblNodeID.Text;
                    da.strRemark = tbxRemark.Text;
                    da.strRevision = hdnRevision.Value;
                    da.strRevisionDis = tbxRevisionDis.Text;
                    da.strSection = tbxSection.Text;

                    listDa.Add(da);
                }
            }
            if (listDa.Count > 0)
            {
                objUploadDocApp.AddDocApproved(listDa, strEmpIdLogin, cbxNeedApprove.Checked);
            }
        }

        #endregion

        #region Methods helper

        private byte[] FileToByteArray(string filePathName)
        {
            return File.ReadAllBytes(filePathName);
        }

        private void ClearValue()
        {
            ViewState["dtAddDoc"] = null;
            gvDocUpload.DataSource = null;
            gvDocUpload.DataBind();
            BindDdlDocType();
            lblErrorMsg.Text = string.Empty;
        }

        private DataTable RemoveDuplicatesRecords(DataTable dt)
        {
            //Returns just 5 unique rows
            var UniqueRows = dt.AsEnumerable().Distinct(DataRowComparer.Default);
            DataTable dt2 = UniqueRows.CopyToDataTable();
            return dt2;
        }

        private string ValidateSubmit()
        {
            string strErrorMsg = string.Empty;
            if (gvDocUpload.Rows.Count <= 0)
            {
                strErrorMsg = "Please Upload Doc approval.";
            }
            if (cbxNeedApprove.Checked)
            {
                if (tbxSubject.Text.Trim() == string.Empty) strErrorMsg = "Please input subject.";
                if (tbxContent.Text.Trim() == string.Empty) strErrorMsg = "Please input content.";
                try
                {
                    DataTable dtApprover = (DataTable)ViewState["dtSelectedApprover"];
                    if (dtApprover.Rows.Count < 1)
                    {
                        strErrorMsg = "Please select approver.";
                    }
                }
                catch
                {
                    strErrorMsg = "Please select approver.";
                }
            }

            return strErrorMsg;
        }

        #endregion

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void cbxNeedApprove_CheckedChanged(object sender, EventArgs e)
        {
            divApproval.Visible = cbxNeedApprove.Checked ? true : false;
        }

        protected void OnRowGvSelectedApproverDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["dtSelectedApprover"] as DataTable;
            dt.Rows[index].Delete();
            dt.AcceptChanges();
            ViewState["dtSelectedApprover"] = dt;
            gvSelectedApprover.DataSource = dt;
            gvSelectedApprover.DataBind();
            // ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        protected void BtnSearchApprover_Click(object sender, EventArgs e)
        {
            SearchApprover();
            // ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

        private void SearchApprover()
        {
            string strEmpId, strFullName, strDept, strPosition, strEmpDept;
            strEmpId = strFullName = strDept = strPosition = strEmpDept = string.Empty;
            strEmpId = PopTbxEmpIdSearch.Text.Trim();
            strFullName = PopTbxEmpFullNameSearch.Text.Trim();
            strDept = PopTbxDeptSearch.Text.Trim();
            strPosition = PopTbxPositionSearch.Text.Trim();
            strEmpDept = PopTbxEmpDeptSearch.Text.Trim();
            DataTable dtEmp = objUploadDocApp.GetApproverCommon(strEmpId, strDept, strFullName, strPosition, strEmpDept);
            gvUnselectApprover.DataSource = dtEmp;
            gvUnselectApprover.DataBind();

        }

        protected void imgAddApprover_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgBtn = sender as ImageButton;
            GridViewRow gvRow = (GridViewRow)imgBtn.NamingContainer;
            Label lblEmpPostion = gvRow.FindControl("lblEmpPostion") as Label;
            Label lblEmpName = gvRow.FindControl("lblEmpName") as Label;

            string strUserId = imgBtn.CommandArgument;
            string strPosition = lblEmpPostion.Text;
            string strUserName = lblEmpName.Text;
            //DeleteUserById(Convert.ToInt32(button.CommandArgument));
            // check user is available in cs
            if (objUploadDocApp.IsCsUserExist(strUserId))
            {
                AddUserToSelected(strUserId, strUserName, strPosition);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUpo", "alert(' User : " + strUserName + " does not exist in CS.');", true);
            }
        }

        private void AddUserToSelected(string strUserId, string strUserName, string strPosition)
        {
            bool isDup = false;
            DataTable dtSelectedApprover = new DataTable();
            if (ViewState["dtSelectedApprover"] != null)
                dtSelectedApprover = (DataTable)ViewState["dtSelectedApprover"];
            if (dtSelectedApprover.Rows.Count > 0)
            {
                // check dup
                DataRow[] foundEmp = dtSelectedApprover.Select("id = '" + strUserId + "'");
                if (foundEmp.Length > 0)
                    isDup = true;
            }
            else
            {
                // create col
                try
                {
                    DataColumn dc = new DataColumn("id");
                    dtSelectedApprover.Columns.Add(dc);
                    dc = new DataColumn("FULLNAME");
                    dtSelectedApprover.Columns.Add(dc);
                    dc = new DataColumn("POSITION_SHORT");
                    dtSelectedApprover.Columns.Add(dc);
                }
                catch { }
            }
            if (!isDup)
            {
                DataRow dr = dtSelectedApprover.NewRow();
                dr["id"] = strUserId;
                dr["FULLNAME"] = strUserName;
                dr["POSITION_SHORT"] = strPosition;
                dtSelectedApprover.Rows.Add(dr);
            }
            ViewState["dtSelectedApprover"] = dtSelectedApprover;
            gvSelectedApprover.DataSource = dtSelectedApprover;
            gvSelectedApprover.DataBind();

        }

        protected void gvUnselectApprover_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvUnselectApprover.PageIndex = e.NewPageIndex;
            SearchApprover();
            // ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "ShowPopUp('V');", true);
        }

    }



}