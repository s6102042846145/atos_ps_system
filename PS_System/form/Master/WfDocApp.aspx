﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WfDocApp.aspx.cs" Inherits="PS_System.form.Master.WfDocApp" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 138px;
        }

        .auto-style3 {
            width: 600px;
        }

        #docview {
            height: 396px;
            width: 893px;
        }

         .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 11px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server"> 
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel5" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="lblHeader" runat="server" Font-Bold="true" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Document Approval Workflow"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <table class="auto-style1" style="font-family: tahoma; font-size: 10pt; color: #333333">
           <%-- <tr>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Title"></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style3">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel9" runat="server" Width="90px">
                                    <asp:Label ID="lblRequestor" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Requestor :"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" Width="500px">
                                    <asp:Label ID="lblRequestorVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Panel ID="Panel7" runat="server" Width="100px">
                                    <asp:Label ID="lblDaNo" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="DA No.  :"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" Width="200px">
                                    <asp:Label ID="lblPsDaNo" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTo" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="To :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel4" runat="server" Width="500px">
                                    <asp:Label ID="lblToVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>

                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Subject :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" Width="500px">
                                    <asp:Label ID="lblSubjectVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="p_PDF" runat="server" BackColor="#666666" Height="400px" Width="900px">
                        <iframe id="docview" src="about:blank" runat="server"></iframe>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblListDoc" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="List of Document"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width:50%">
                     <asp:GridView ID="gvDocAppList" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="50%"
                    Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10">
                    <Columns>
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Name">
                            <ItemTemplate>
                                <asp:Label ID="lblDocName" runat="server" Text='<%# System.IO.Path.GetFileNameWithoutExtension(Eval("doc_name").ToString()) %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="D/L Doc Approved">
                            <ItemTemplate>
                                <asp:ImageButton runat="server" ID="imgDlDoc" ImageUrl="~/images/down.png" CommandName="image" CommandArgument='<%# Eval("doc_nodeid") %>' OnClick="imgDLCilck" Width="15px" Height="15px" />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Comment"></asp:Label>
                </td>
            </tr>
            <tr>

                <td>
                    <asp:Panel ID="p_Comment" runat="server">
                        <asp:TextBox ID="txtComment" runat="server" Font-Names="tahoma" Font-Size="10pt" Height="80px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style2">
                        <tr>
                            <td>
                                <asp:Button ID="btnApprove" CssClass="btn btn-primary"  runat="server" Font-Names="Tahoma" Font-Size="10pt" OnClick="btnApprove_Click" Text="Approve" Width="100px" />
                            </td>
                            <td>
                                <asp:Button ID="btnReject" CssClass="btn btn-danger"  runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Reject" Width="100px" OnClick="btnReject_Click" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        <asp:HiddenField ID="hidPSDPNo" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidRequestorId" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" />
        <asp:HiddenField ID="hidWorkID" runat="server" />
        <asp:HiddenField ID="hidSubworkID" runat="server" />
        <asp:HiddenField ID="hidTaskID" runat="server" />
        <asp:HiddenField ID="hidDocNodeId" runat="server" />
        <asp:HiddenField ID="hidDocNodeIdPrev" runat="server" />
        <asp:HiddenField ID="hidApproverOrder" runat="server" />
        <asp:HiddenField ID="hidNextApproverId" runat="server" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HiddenField ID="hidProcessID" runat="server" />
    </form>
</body>
</html>
