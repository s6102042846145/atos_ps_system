﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Bom.aspx.cs" Inherits="PS_System.form.Master.Bom" EnableEventValidation="false" MaintainScrollPositionOnPostback="true"  ValidateRequest="false"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BOM</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .ddl-serial{

            padding:6px 12px;
            font-size:14px;
            line-height:1.428571429;
            color:#555555;
            vertical-align:middle;
            background-color:#ffffff;
            border:1px solid #cccccc;
            border-radius:4px;
            box-shadow:inset 0 1px 1px rgb(0 0 0 / 8%);
            transition:border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
    </style>
    <script type="text/javascript">
        var urlParams;
        (window.onpopstate = function () {
            var match,
                pl = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
                query = window.location.search.substring(1);

            urlParams = {};
            while (match = search.exec(query))
                urlParams[decode(match[1])] = decode(match[2]);
        })();

        function loadSessionData() {
            var tk = urlParams["tk"];
            if (tk != "") {
                var atos = document.getElementById("hidATOS").value;
                var atosBytes = aesjs.utils.utf8.toBytes(atos);

                var iv = [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36];
                var encryptedBytes = aesjs.utils.hex.toBytes(tk);

                var aesCbc = new aesjs.ModeOfOperation.cbc(atosBytes, iv);
                var decryptedBytes = aesCbc.decrypt(encryptedBytes);

                var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);

                document.getElementById("hidLogin").value = decryptedText.replace(/,/g, '');
                document.getElementById("hidIsFirstRun").value = "t";
                $('#btnSearchBOM').trigger('click');
            }
        }

        function displayTable_BOMList() {
            $('#gvBOMList').DataTable({
                "destroy": true,
                "searching": true // false to disable search (or any other option)
            });
            $('.dataTables_length').addClass('bs-select');
        }
        function displayTable_AddEquip() {
            $('#gvMyEquip1').DataTable({
                "destroy": true,
                "searching": false // false to disable search (or any other option)
            });
            $('.dataTables_length').addClass('bs-select');

            $('#gvDocList').DataTable({
                "destroy": true,
                "searching": true // false to disable search (or any other option)
            });
            $('.dataTables_length').addClass('bs-select');
        }
        function displayTable_SearchEquip() {
            $('#gvEquip').DataTable({
                "destroy": true,
                "searching": true // false to disable search (or any other option)
            });
            $('.dataTables_length').addClass('bs-select');
        }

        $(function () {
            //displayTable_BOMList();
            //displayTable_AddEquip();
            //displayTable_SearchEquip();
        });

        function disableKeyEnter(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode == 13) {
                return false;
            }
            return true;
        }

        function numeric(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode
            if (charCode > 31 && ((charCode >= 48 && charCode <= 57) || charCode == 46))
                return true;
            else {
                alert('Please Enter Numeric values.');
                return false;
            }
        }

        function openAddRecord(mode) {
            if (mode == "open") {
                document.getElementById("add_data").style.display = "block";
                document.getElementById("show_data").style.display = "none";
                //document.getElementById("myModal_Equip").style.display = "none";
                document.getElementById("myModal_Doc").style.display = "none";
            }
            else {
                document.getElementById("add_data").style.display = "none";
                document.getElementById("show_data").style.display = "block";
                //document.getElementById("myModal_Equip").style.display = "none";
                document.getElementById("myModal_Doc").style.display = "none";
                document.getElementById("hidBom_Group").value = "";
                document.getElementById("hidEq_Code").value = "";
                document.getElementById("hidDoc_nodeid").value = "";
            }
            clearData();
            return true;
        }

        function clearData() {
            $('#txtBomName').val("");
            $('#txtDesc').val("");

            $("#gvMyEquip1").html("");
            $("#gvDocList").html("");
        }

        //function openViewEquip(mode) {
        //    if (mode == "open") {
        //        document.getElementById("myModal_Equip").style.display = "block";
        //        document.getElementById("txtSearchEquip").value = ""; 
        //    }
        //    else {
        //        document.getElementById("myModal_Equip").style.display = "none"; 
        //    }

        //    return true;
        //}
        function openViewDoc(mode) {
            if (mode == "open") {
                document.getElementById("myModal_Doc").style.display = "block";
            }
            else {
                document.getElementById("myModal_Doc").style.display = "none";
            }

            return true;
        }
        function selectEquip(objCHK) {
            var department_name = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblequip_gong")).innerText;
            var section_name = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblequip_pnag")).innerText;
            var bomlevel_name = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblBOM_Level")).innerText;
            var bomlevel_code = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_hidBOM_Level_Code")).value;
            var item_code = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblitem_code")).innerText;
            var item_name = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblitem_name")).innerText;
            var item_legend = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblitem_legend")).innerText;
            var item_relate = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblitem_relate")).innerText;
            var item_type = document.getElementById(objCHK.id.replace("gvEquip_chkRow", "gvEquip_lblitem_type")).innerText; 

            var selectValue = "|" + department_name + "+" + section_name + "+" + bomlevel_name + "+" + bomlevel_code + "+" + item_code + "+" + item_name + "+" + item_legend + "+" + item_relate + "+" + item_type +"|";

            if (objCHK.checked == true) {
                if (document.getElementById("hidSelectEquip").value.includes(selectValue) == false) {
                    document.getElementById("hidSelectEquip").value = document.getElementById("hidSelectEquip").value + selectValue;
                }
            }
            else {
                document.getElementById("hidSelectEquip").value = document.getElementById("hidSelectEquip").value.replace(selectValue, "");
            }
        }
        function selectEquipDoc(objCHK) {
    
            var doc_name = document.getElementById(objCHK.id.replace("dtEquipDoc_chkRow", "dtEquipDoc_lbldoc_name")).innerText;
            var doc_revision_display = document.getElementById(objCHK.id.replace("dtEquipDoc_chkRow", "dtEquipDoc_lbldoc_revision_display")).innerText;
            var doc_desc = document.getElementById(objCHK.id.replace("dtEquipDoc_chkRow", "dtEquipDoc_lbldoc_desc")).innerText;
            var doc_nodeid = document.getElementById(objCHK.id.replace("dtEquipDoc_chkRow", "dtEquipDoc_hid_nodeid")).value;
            var selectValueDoc = "|" + doc_name + "+" + doc_revision_display + "+" + doc_desc + "+" + doc_nodeid + "|";
            if (objCHK.checked == true) {
                if (document.getElementById("hidSelectEquipDoc").value.includes(selectValueDoc) == false) {
                    document.getElementById("hidSelectEquipDoc").value = document.getElementById("hidSelectEquipDoc").value + selectValueDoc;
                }
            }
            else {
                document.getElementById("hidSelectEquipDoc").value = document.getElementById("hidSelectEquipDoc").value.replace(selectValueDoc, "");
            }
        }
        function Searchtxt(search) {
            //ddlBOMLevel_Search
            //var value = document.getElementById("hidSearch").value//txtSearchEquip placeholder="Equipment Name or Description"
            var tb = document.getElementById("txtSearchEquip");
            if (search == "0-EQI") {
                tb.placeholder = "Equipment Code or Full/Short Description";
                document.getElementById("hidSearch").value = "Equipment Code or Full/Short Description";

            } else if (search == "2-ZCM") {
                tb.placeholder = "Equipment Code or Legend or Full/Short Description";
                document.getElementById("hidSearch").value = "Equipment Code or Legend or Full/Short Description";

            } else if (search == "3-ASM") {
                tb.placeholder = "Bom Name or Legend";
                document.getElementById("hidSearch").value = "Bom Name or Legend";
            }
        }
        function openTab(chk, ima) {
            var x = document.getElementById(chk);
            var img = document.getElementById(ima);
            if (x.style.display === "none") {
                x.style.display = "block";
                img.src = '../../images/sort_asc.png';

            } else {
                x.style.display = "none";
                img.src = '../../images/sort_desc.png';
            }
        }
    </script>

</head>
<body>
    <form id="form" runat="server">
        <%--<div id="myModal_Equip" class="modal" runat="server">
            <!-- Modal content class="modal"-->
            <div class="modal-content">
                <table style="width: 100%;">
                    <tr style="vertical-align: central;">
                        <td style="width: 5%; height: 34px; text-align: right;">
                            <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="BOM Level: "></asp:Label>
                        </td>
                        <td style="text-align: left; width: 15%;">
                            <asp:DropDownList ID="ddlBOMLevel_Search" runat="server" ClientIDMode="Static" Class="form-control" Width="90%" onchange="Searchtxt(this.value);">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 5%; height: 34px; text-align: right;">
                            <asp:Label ID="Label31" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Search: "></asp:Label>
                        </td>
                        <td style="width: 75%; height: 34px; text-align: left;">
                            <asp:TextBox ID="txtSearchEquip" onkeypress="return disableKeyEnter(event)" runat="server" BorderColor="Silver" BorderStyle="Solid" 
                                BorderWidth="1px" Font-Names="Tahoma" Font-Size="10pt" Width="250px" ></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnSearch1" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch1_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 1000px; height: 300px;">
                            <asp:GridView ID="gvEquip" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" EmptyDataText="No data found." ForeColor="#333333" GridLines="Vertical"
                                OnPageIndexChanging="gvEquip_PageIndexChanging"
                                PageSize="10" ShowHeaderWhenEmpty="true" Width="100%">
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Names="tahoma" Font-Size="10pt" />
                                <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" PageButtonCount="5" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvEquip_chkRow" runat="server" onclick="selectEquip(this);" />
                                        </ItemTemplate>
                                        <ItemStyle Width="50" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblequip_gong" runat="server" Text='<%# Bind("department_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="100" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblequip_pnag" runat="server" Text='<%# Bind("section_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="100" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Level">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblBOM_Level" runat="server" Text='<%# Bind("bomlevel_name") %>'></asp:Label>
                                            <asp:HiddenField ID="gvEquip_hidBOM_Level_Code" runat="server" Value='<%# Bind("bomlevel_code") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="150" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_code" runat="server" Text='<%# Bind("item_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="150" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_name" runat="server" Text='<%# Bind("item_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Type">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_type" runat="server" Text='<%# Bind("item_type") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Legend">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_legend" runat="server" Text='<%# Bind("item_legend") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Related">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_relate" runat="server" Text='<%# Bind("item_relate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnGetSelected" runat="server" CssClass="btn btn-primary" Text="Select Equipment" OnClick="btnGetSelected_Click" />
                            <asp:Button ID="btnClose_Modal" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="if(openViewEquip('close')) return false;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>--%>
        <div id="myModal_Rel" class="modal" runat="server">
             <div class="modal-content">
                 <table style="width:100%">
                        <tr>
                            <td >
                                <asp:Label ID="lb_rel" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:right">
                                <asp:Button ID="bt_closeRel" runat="server" Text="Close" class="btn btn-danger" OnClick="bt_closeRel_Click"/>
                            </td>
                        </tr>
                    </table>
             </div>
        </div>
         <div id="myModal_Doc" class="modal" runat="server">
            <!-- Modal content class="modal"-->
            <div class="modal-content">
              
                           <table align="center" style="width: 95%;">
                                    <tr>
                                    <td colspan="2" style="text-align: left; color: #006699;">
                                        <asp:Label ID="lbTitle" runat="server"><b>Document</b></asp:Label>
                                    </td>

                                </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label5" runat="server" Text="Document Type:"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DDLDoctype" runat="server" Width="250px" OnSelectedIndexChanged="DDLDoctype_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                   <tr>
                        <td colspan="4" style="width: 1000px; height: 300px;">
                            <asp:GridView ID="dtEquipDoc" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" EmptyDataText="No data found." ShowHeaderWhenEmpty="true" 
                                OnPageIndexChanging="dtEquipDoc_PageIndexChanging" class="table table-striped table-bordered table-sm"
                                PageSize="10" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="dtEquipDoc_chkRow" runat="server" onclick="selectEquipDoc(this);" />
                                        </ItemTemplate>
                                        <ItemStyle Width="50" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Name">
                                        <ItemTemplate>
                                            <asp:Label ID="dtEquipDoc_lbldoc_name" runat="server" Text='<%# Bind("doc_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="150" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:Label ID="dtEquipDoc_lbldoc_revision_display" runat="server" Text='<%# Bind("doc_revision_display") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="dtEquipDoc_lbldoc_desc" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                                            <asp:HiddenField ID="dtEquipDoc_hid_nodeid" runat="server" Value='<%# Bind("doc_nodeid") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                             <asp:Button ID="btAttsave" runat="server" Text="Add" class="btn btn-primary" OnClick="btAttsave_Click"  />
                                                    &nbsp;
                             <asp:Button ID="btAttcencel" ClientIDMode="Static" runat="server" CssClass="btn btn-danger" Text="Close" OnClientClick="if(openViewDoc('close')) return false;" />
                        </td>
                    </tr>
                                </table>
            </div>
        </div>
        <!-----------------------------------------END MODAL-------------------------------------->
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - BOM Purchase Master"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="add_data" style="display: none; padding: 10px 10px 10px 10px;" runat="server">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left; width: 10%">
                        <asp:Label ID="Label2" runat="server" Text="กอง"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 40%">
                        <asp:TextBox ID="txtDepartment" runat="server" Enabled="false" Text="Department" Width="90%"></asp:TextBox>
                        <asp:HiddenField ID="hidSelectEquip" runat="server" />
                         <asp:HiddenField ID="hidSelectEquipDoc" runat="server" />
                        <asp:HiddenField ID="hidBom_Group" runat="server" />
                        <asp:HiddenField ID="hidEq_Code" runat="server" />
                        <asp:HiddenField ID="hidDoc_nodeid" runat="server" />
                        
                    </td>
                    <td style="text-align: left; width: 10%">
                        <asp:Label ID="lblSection" runat="server" Text="แผนก"></asp:Label>
                    </td>
                    <td style="text-align: left; width: 40%">
                        <asp:TextBox ID="txtSection" runat="server" Enabled="false" Text="Section" Width="90%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="Label8" runat="server" Text="BOM Code:"></asp:Label>
                    </td>
                    <td colspan="3" style="text-align: left;">
                        <asp:TextBox ID="txtBomCode" runat="server" Width="40%" ClientIDMode="Static"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="lblBomName" runat="server" Text="BOM Name:"></asp:Label>
                    </td>
                    <td colspan="3" style="text-align: left;">
                        <asp:TextBox ID="txtBomName" runat="server" Width="90%" ClientIDMode="Static"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; vertical-align: top;">
                        <asp:Label ID="lblDes" runat="server" Text="Description:"></asp:Label>
                    </td>
                    <td colspan="3" style="text-align: left;">
                        <asp:TextBox ID="txtDesc" runat="server" TextMode="MultiLine" Rows="3" Width="90%" ClientIDMode="Static"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <table style="width: 100%;">
                            <tr>
                                <td style="text-align: left; width: 10%;">
                                    <asp:Label ID="lblMat" runat="server" Text="Label">BOM Level:</asp:Label>
                                </td>
                                <td style="text-align: left; width: 15%;">
                                    <asp:DropDownList ID="lstBOMLevel" runat="server" Class="form-control" Width="150px" Height="40px" AutoPostBack="True" OnSelectedIndexChanged="lstBOMLevel_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left; width: 75%;">
                                    <table id="tbMisc" runat="server" style="width: 100%;">
                                        <tr>
                                            <td style="text-align: left; width: 10%;">
                                                <asp:Label ID="lblLegend" runat="server" Text="Label">Legend:</asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 90%;">
                                                <asp:TextBox ID="txtLegend" runat="server" Width="20%" Height="40px"></asp:TextBox>
                                            </td>
                                          <%--  <td style="text-align: left; width: 10%;">
                                                <asp:Label ID="lblCat" runat="server" Text="Label">Category*:</asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 25%;">
                                                <asp:DropDownList ID="ddlCat" runat="server" Width="80%" Height="40px"></asp:DropDownList>
                                            </td>--%>
                                           <%-- <td style="text-align: left; width: 15%;">
                                                <asp:Label ID="lblmiscell" runat="server" Text="Label">Miscellenous Per Unit*:</asp:Label>
                                            </td>
                                            <td style="text-align: left; width: 20%;">
                                                <asp:TextBox ID="txtmiscell" runat="server" Width="80%" Height="40px"></asp:TextBox>
                                            </td>--%>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                       <%-- <asp:LinkButton ID="btnViewEquip" runat="server" OnClientClick="if(openViewEquip('open')) return false;">Add Equipment</asp:LinkButton>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/search.png" Height="20px" Width="20px" CssClass="w3-right" />--%>
                        <%-- <asp:Button ID="Button2" runat="server" Text="Add Equip/Misc."  CssClass="btn btn-primary"  OnClientClick="if(openViewEquip('open')) return false;" 
                            style="background-image:url('~/images/search.png'); cursor:pointer;"/>--%>

                         <div id="dvEquipExp" runat="server" onclick="openTab('dvEquip','Imag');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block">
                            <asp:Image ID="Imag" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="Label14" runat="server" Text="Bid 1"><b>Add Equip/Misc.</b></asp:Label>
                        </div>
                    </td>
                </tr>
                <!-------------------------------------------START TABLE EQUIP------------------------------------------>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <div id="dvEquip" runat="server" style="display: none;">
                            <div id="myModal_Equip" runat="server">
                <table style="width: 100%;">
                    <tr style="vertical-align: central;">
                        <td style="width: 5%; height: 34px; text-align: right;">
                            <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="BOM Level: "></asp:Label>
                        </td>
                        <td style="text-align: left; width: 15%;">
                            <asp:DropDownList ID="ddlBOMLevel_Search" runat="server" ClientIDMode="Static" Class="form-control" Width="90%" onchange="Searchtxt(this.value);">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 5%; height: 34px; text-align: right;">
                            <asp:Label ID="Label31" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Search: "></asp:Label>
                        </td>
                        <td style="width: 75%; height: 34px; text-align: left;">
                            <asp:TextBox ID="txtSearchEquip" onkeypress="return disableKeyEnter(event)" runat="server" BorderColor="Silver" BorderStyle="Solid" 
                                BorderWidth="1px" Font-Names="Tahoma" Font-Size="10pt" Width="250px" ></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnSearch1" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" Text="Search" OnClick="btnSearch1_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width: 1000px; height: 300px;">
                            <asp:GridView ID="gvEquip" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" EmptyDataText="No data found." ForeColor="#333333" GridLines="Vertical"
                                OnPageIndexChanging="gvEquip_PageIndexChanging"
                                PageSize="10" ShowHeaderWhenEmpty="true" Width="100%" OnRowCommand="gvEquip_RowCommand">
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Names="tahoma" Font-Size="10pt" />
                                <PagerStyle BackColor="#5D7B9D" ForeColor="White" HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" PageButtonCount="5" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvEquip_chkRow" runat="server" onclick="selectEquip(this);" />
                                        </ItemTemplate>
                                        <ItemStyle Width="50" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblequip_gong" runat="server" Text='<%# Bind("department_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="100" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblequip_pnag" runat="server" Text='<%# Bind("section_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="100" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Level">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblBOM_Level" runat="server" Text='<%# Bind("bomlevel_name") %>'></asp:Label>
                                            <asp:HiddenField ID="gvEquip_hidBOM_Level_Code" runat="server" Value='<%# Bind("bomlevel_code") %>' />
                                        </ItemTemplate>
                                        <ItemStyle Width="150" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Code">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_code" runat="server" Text='<%# Bind("item_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="150" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_name" runat="server" Text='<%# Bind("item_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item Type">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_type" runat="server" Text='<%# Bind("item_type") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Legend">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_legend" runat="server" Text='<%# Bind("item_legend") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Related">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquip_lblitem_relate2" runat="server" Text='<%# Bind("item_relate") %>' Visible="false"></asp:Label>
                                            <asp:HiddenField ID="gvEquip_lblitem_relate" value='<%# Bind("item_relate") %>' runat="server" />
                                              <asp:ImageButton ID="img_view" ClientIDMode="Static" runat="server" ImageUrl="../../Images/view.png" CommandName="viewdata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="View Relate" Width="25" ImageAlign="NotSet" />
                                        </ItemTemplate>
                                        <ItemStyle Width="250" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            
                            <%--<asp:Button ID="btnClose_Modal" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="if(openViewEquip('close')) return false;" />--%>
                        </td>
                    </tr>
                </table>
        </div>
                            
                             
                        </div>
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Button ID="btnGetSelected" runat="server" CssClass="btn btn-primary" Text="Select Equipment" OnClick="btnGetSelected_Click" />
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <div>
                                <asp:GridView ID="gvMyEquip1" runat="server" AutoGenerateColumns="false" Width="100%"
                            Visible="true" ClientIDMode="Static" ShowHeaderWhenEmpty="true" EmptyDataText="No Record."
                            OnRowDeleting="gvMyEquip1_OnRowDeleting" OnRowDataBound="gvMyEquip1_OnRowDataBound"
                            class="table table-striped table-bordered table-sm">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="กอง">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lblequip_gong" runat="server" Text='<%# Bind("department_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="แผนก">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lblequip_pnag" runat="server" Text='<%# Bind("section_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Level">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lblBOM_Level" runat="server" Text='<%# Bind("bomlevel_name") %>'></asp:Label>
                                        <asp:HiddenField ID="gvEquip_hidBOM_Level_Code" runat="server" Value='<%# Bind("bomlevel_code") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="150" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Code">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lblitem_code" runat="server" Text='<%# Bind("item_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item Name">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lblitem_name" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Legend">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lbllegend" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
<%--                                <asp:TemplateField HeaderText="Related">
                                    <ItemTemplate>
                                        <asp:Label ID="gvMyEquip_lblitem_relate" runat="server" Text='<%# Bind("item_relate") %>'></asp:Label>
                                    </ItemTemplate> Text='<%# Bind("item_name") %>' Text='<%# Bind("item_legend") %>'
                                    <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="QTY">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvMyEquip_txtQty" runat="server" Width="95%" onkeypass="return numeric(event);" Text='<%# Bind("equip_qty") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvMyEquip_txtUnit" runat="server" Width="95%" Text='<%# Bind("equip_unit") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Buying Lot">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvMyEquip_txtBuyingLot" runat="server" Width="95%" onkeypass="return numeric(event);" Text='<%# Bind("equip_buyinglot") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reservasion (%)">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvMyEquip_txtReservasion" runat="server" Width="95%" onkeypass="return numeric(event);" Text='<%# Bind("equip_reserve") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="../../images/bin.png" ControlStyle-Width="16px" ItemStyle-Width="30px" ItemStyle-HorizontalAlign="Center"/>
                            </Columns>
                        </asp:GridView>
                            </div>
                    </td>
                </tr>
                <!-------------------------------------------START TABLE Document List------------------------------------------>
                   <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Button ID="Button1" runat="server" Text="Select Document"  CssClass="btn btn-primary"  OnClientClick="if(openViewDoc('open')) return false;" 
                            style="background-image:url('~/images/search.png'); cursor:pointer;"/>
                    </td>
                </tr>
                <tr><td style="height:10px"></td></tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:GridView ID="gvDocList" runat="server" Width="100%" ShowHeaderWhenEmpty="true" EmptyDataText="No Record."
                            AutoGenerateColumns="false" Visible="true" ClientIDMode="Static"
                            OnRowDataBound="gvDocList_OnRowDataBound" class="table table-striped table-bordered table-sm" OnRowDeleting="gvDocList_RowDeleting">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document Name">
                                    <ItemTemplate>
                                        <asp:Label ID="gvDocList_lbldoc_name" runat="server" Text='<%# Bind("doc_name") %>'></asp:Label>
                                        <asp:HiddenField ID="gvDocList_hid_nodeid" runat="server" Value='<%# Bind("doc_nodeid") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="30%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Revision">
                                    <ItemTemplate>
                                        <asp:Label ID="gvDocList_lbldoc_revision" runat="server" Text='<%# Bind("doc_revision_display") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="gvDocList_lbldoc_desc" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="45%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="../../images/bin.png" ControlStyle-Width="16px"  ItemStyle-HorizontalAlign="Center"/>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Button ID="btn_insert" CssClass="btn btn-primary" runat="server"
                            Width="5%" Text="Save" OnClick="btn_insert_Click" />
                        <asp:Button ID="btnClose" CssClass="btn btn-danger" runat="server"
                            Width="5%" Text="Close" OnClientClick="if(openAddRecord('close')) return false;" />
                    </td>
                </tr>
            </table>
        </div>
        <div id="show_data" style="padding: 10px 10px 10px 10px;" runat="server">
            <table style="width: 100%;">
                <tr>
                    <td>
                        <table style="text-align: center; vertical-align: middle; "><%--width: 780px;--%>
                            <tr>
                                <td style="text-align: left; vertical-align: middle;"><%-- width: 100px;--%>
                                    <asp:Label ID="Label4" runat="server" Text="BOM Name:"></asp:Label>
                                </td>
                                <td style="text-align: left; vertical-align: middle; "><%--width: 450px;--%>
                                    <asp:TextBox ID="txtSearch" runat="server" Width="400px" onkeypress="return disableKeyEnter(event)" ></asp:TextBox>
                                    <asp:Label ID="Label7" runat="server" Text="Description:"></asp:Label>
                                    <asp:TextBox ID="txtBomDesc" runat="server" Width="400px" onkeypress="return disableKeyEnter(event)" ></asp:TextBox>
                                    
                                </td>
                                <td style="text-align: left; vertical-align: middle;"> <%--width: 300px;--%>
                                    <asp:Button ID="btnSearchBOM" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                                        Font-Names="tahoma" Font-Size="10pt" Text="Search BOM" OnClick="btnSearchBOM_Click" />
                                    <asp:Button ID="btnAddBOM" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                                        Font-Names="tahoma" Font-Size="10pt" Text="Add BOM" OnClick="btnAddBOM_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; vertical-align: middle;">
                                    <asp:Label ID="Label6" runat="server" Text="Legend:"></asp:Label>
                                </td>
                                <td style="text-align: left; vertical-align: middle;">
                                    <asp:TextBox ID="txtBomLegend" runat="server" Width="400px" onkeypress="return disableKeyEnter(event)" ></asp:TextBox>
                                     <asp:Label ID="Label3" runat="server" Text="Label" Width="67px">BOM Level:</asp:Label>
                                     <asp:DropDownList ID="ddlBOM" runat="server"  Width="150px" CssClass="ddl-serial" AutoPostBack="True" OnSelectedIndexChanged="ddlBOM_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left; vertical-align: middle;">

                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:GridView ID="gvBOMList" runat="server" AutoGenerateColumns="False" Width="100%"
                            Visible="true" ClientIDMode="Static" OnRowCommand="gvBOMList_RowCommand" OnRowDataBound="gvBOMList_RowDataBound"
                            class="table table-striped table-bordered table-sm">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BOM Level">
                                    <ItemTemplate>
                                        <asp:Label ID="gvBOMList_lblBOMLevel" runat="server" Text='<%# Bind("bomlevel_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="BOM Name">
                                    <ItemTemplate>
                                        <asp:Label ID="gvBOMList_lblBOMName" runat="server" Text='<%# Bind("bom_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="gvBOMList_lblBOMDesc" runat="server" Text='<%# Bind("bom_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>
                               <%-- <asp:TemplateField HeaderText="Equipment List">
                                    <ItemTemplate>
                                        <asp:Label ID="gvBOMList_lblEquipList" runat="server" Text='<%# Bind("equip_list") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50%" HorizontalAlign="Left" VerticalAlign="Middle" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvBOMList_btnEdit" ClientIDMode="Static" runat="server" ImageUrl="../../Images/edit.png" CommandName="editdata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Edit" Width="30" ImageAlign="NotSet" OnClientClick="if(openAddRecord('open')) return true;" />
                                        <asp:HiddenField ID="gvBOMList_hidBOMCode" runat="server" Value='<%# Bind("bom_code") %>' />
                                        <asp:HiddenField ID="gvBOMList_hidBOMLevel" runat="server" Value='<%# Bind("bomlevel") %>' />
                                        <asp:HiddenField ID="gvBOMList_hidLegend" runat="server" Value='<%# Bind("legend") %>' />
                                        <asp:HiddenField ID="gvBOMList_hidCate_Code" runat="server" Value='<%# Bind("cate_code") %>' />
                                        <asp:HiddenField ID="gvBOMList_hidMisc_Per_Unit" runat="server" Value='<%# Bind("misc_per_unit") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CCFF" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidIsFirstRun" runat="server" />
        <asp:HiddenField ID="hidATOS" runat="server" />
         <asp:HiddenField ID="hidSearch" runat="server" />
    </form>
</body>
</html>
