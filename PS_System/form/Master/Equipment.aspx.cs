﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class Equipment : Page
    {
        clsEquipmentMasterdb EquipDbClass = new clsEquipmentMasterdb();
        private string strMode = string.Empty;
        private string strEmpId = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            Session.Add("dtSource", null);
            string strEquipCode = string.Empty;
            if (Request.QueryString["zuserlogin"] != null)
                strEmpId = Request.QueryString["zuserlogin"];
            else
                strEmpId = "z570753";//"z575348";//

            if (Request.QueryString["equipcode"] != null)
            {
                strEquipCode = Request.QueryString["equipcode"];
                strMode = "E";
            }
            if (!IsPostBack)
            {

                EmpInfoClass emp = new EmpInfoClass();
                Employee thisEmp = emp.getInFoByEmpID(strEmpId);

                lblDepartment.Text = thisEmp.ORGSNAME4;
                lblSection.Text = thisEmp.ORGSNAME5;

                SetDefaultForm();

                if (!String.IsNullOrEmpty(strEquipCode))
                    BindDataToContol(strEquipCode);
                CheckVisibleCintrolByMat();

                if (strMode == "E") SetControlEditMode();
                else
                {
                    btnCreate.Visible = true;
                    btnUpdadte.Visible = false;

                }
                BindEquipDocList();
            }
        }

        private void SetDefaultForm()
        {
            txtEquip.Text = string.Empty;
            txtLegend.Text = string.Empty;
            txtLongDesc.Text = string.Empty;
            txtMatcd.Text = string.Empty;
            txtmiscell.Text = string.Empty;
            txtRemark.Text = string.Empty;
            txtSBuyLot.Text = string.Empty;
            txtSBuyLotUnit.Text = string.Empty;
            txtSearch.Text = string.Empty;
            txtShtDesc.Text = string.Empty;
            txtSRemark.Text = string.Empty;
            txtSReserv.Text = string.Empty;
            txtSReservUnit.Text = string.Empty;
            txtTBuyLot.Text = string.Empty;
            txtTBuyLotUnit.Text = string.Empty;
            txtTRemark.Text = string.Empty;
            txtTReserv.Text = string.Empty;
            txtTReservUnit.Text = string.Empty;

            //BindFormData();
            //BindListData();
            BindDdlSearchDoc();
            BindSapData();
            BindFixData();
            CheckVisibleCintrolByMat();

            BindDdlSerialNoCodeA();
            BindDdlSerialNoCodeB();
            BindDdlSerialNoCodeC();
            BindDdlSerialNoCodeD();
            BindDdlSerialNoCodeE();

        }

        private void CheckVisibleCintrolByMat()
        {
            DataTable dtvalclass = new DataTable();
            dtvalclass.Columns.Add("CODE", typeof(string));
            dtvalclass.Columns.Add("TEXT", typeof(string));
            lstValClass.Items.Clear();
            if (lstMat.SelectedValue == "ZCM")
            {
                txtLegend.Visible = true;
                ddlCat.Visible = true;
                txtmiscell.Visible = true;
                lblLegend.Visible = true;
                lblCate.Visible = true;
                lblMisc.Visible = true;

                dtvalclass.Rows.Add("1204", "1204 อะไหล่ / อุปกรณ์ Common");
                lstValClass.DataSource = dtvalclass;
                lstValClass.DataValueField = "CODE";
                lstValClass.DataTextField = "TEXT";
                lstValClass.DataBind();
            }
            else if (lstMat.SelectedValue == "ZNV")
            {
                txtLegend.Visible = false;
                ddlCat.Visible = false;
                txtmiscell.Visible = false;
                lblLegend.Visible = false;
                lblCate.Visible = false;
                lblMisc.Visible = false;

                dtvalclass.Rows.Add("1601", "1601 Main Eq.Project stock");
                lstValClass.DataSource = dtvalclass;
                lstValClass.DataValueField = "CODE";
                lstValClass.DataTextField = "TEXT";
                lstValClass.DataBind();
            }
            else if (lstMat.SelectedValue == "SVR")
            {
                txtLegend.Visible = false;
                ddlCat.Visible = false;
                txtmiscell.Visible = false;
                lblLegend.Visible = false;
                lblCate.Visible = false;
                lblMisc.Visible = false;
            }
            DDL_EQgroup.Items.Clear();
            DataTable dteq = EquipDbClass.GetEQGroup();
            if (dteq.Rows.Count > 0)
            {
                DDL_EQgroup.DataSource = dteq;
                DDL_EQgroup.DataValueField = "eg_code";
                DDL_EQgroup.DataTextField = "eg_desc";
                DDL_EQgroup.DataBind();
                DDL_EQgroup.Items.Insert(0, new ListItem("Select EquipmentGroup", ""));
            }

        }
        private DataTable GetDtHeader()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("rowid", typeof(string));
            dt.Columns.Add("equip_code", typeof(string));
            dt.Columns.Add("doctype_code", typeof(string));
            dt.Columns.Add("doctype_name", typeof(string));
            dt.Columns.Add("doc_desc", typeof(string));
            dt.Columns.Add("doc_revision_display", typeof(string));
            dt.Columns.Add("updated_datetime", typeof(string));
            dt.Columns.Add("updated_by", typeof(string));
            dt.Columns.Add("depart_position_name", typeof(string));
            dt.Columns.Add("section_position_name", typeof(string));
            dt.Columns.Add("link_doc", typeof(string));
            dt.Columns.Add("doc_name", typeof(string));
            dt.Columns.Add("doc_nodeid", typeof(string));
            return dt;
        }
        private void BindDdlSearchDoc()
        {
            DataTable dt = EquipDbClass.getDoctype();
            //dt.Columns.Add("Value", typeof(string));
            //dt.Columns.Add("Text", typeof(string));
            //DataRow dr;
            //dr = dt.NewRow();
            //dr["Value"] = string.Empty;
            //dr["Text"] = "Seact Type To Search";
            //dt.Rows.Add(dr);
            //dr = dt.NewRow();
            //dr["Value"] = "doctype_code";
            //dr["Text"] = "Document Type";
            //dt.Rows.Add(dr);
            //dr = dt.NewRow();
            //dr["Value"] = "doctype_name";
            //dr["Text"] = "Document Name";
            //dt.Rows.Add(dr);
            DdlSearch.DataSource = dt;
            DdlSearch.DataValueField = "doctype_code";
            DdlSearch.DataTextField = "doctype_name";
            DdlSearch.DataBind();
            DdlSearch.Items.Insert(0, new ListItem("<-- Select -->", ""));
        }
        private void BindFormData()
        {
            DataTable dt = GetDtHeader();
            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            gvBEquipInfo.DataSource = dt;
            gvBEquipInfo.DataBind();
        }
        private void BindListData()
        {
            DataTable dt = GetDtHeader();
            DataTable dtmaster = EquipDbClass.GetDataList();
            for (int i = 0; i < dtmaster.Rows.Count; i++)
            {
                DataRow dr = dt.NewRow();
                dr["rowid"] = dtmaster.Rows[i]["rowid"];
                dr["doctype_code"] = dtmaster.Rows[i]["doctype_code"];
                dr["doctype_name"] = dtmaster.Rows[i]["doctype_name"];
                dr["doc_desc"] = dtmaster.Rows[i]["doc_desc"];
                dr["doc_revision_display"] = dtmaster.Rows[i]["doc_revision_display"];
                dr["updated_datetime"] = "Not Found Yet";
                dr["updated_by"] = dtmaster.Rows[i]["updated_by"];
                dr["depart_position_name"] = dtmaster.Rows[i]["depart_position_name"];
                dr["section_position_name"] = dtmaster.Rows[i]["section_position_name"];
                dt.Rows.Add(dr);
            }

            gvBidNo.DataSource = dt;
            gvBidNo.DataBind();
        }
        private void BindSapData()
        {
            DataTable dtmatGrp = EquipDbClass.GetSapMaterialGroup();
            DataRow drmatGrp = dtmatGrp.NewRow();
            drmatGrp["NAME"] = "Select MaterialGroup";
            dtmatGrp.Rows.InsertAt(drmatGrp, 0);
            lstMatGrp.DataSource = dtmatGrp;
            lstMatGrp.DataValueField = "CODE";
            lstMatGrp.DataTextField = "NAME";
            lstMatGrp.DataBind();

            DataTable dtunit = EquipDbClass.GetSapUnit();
            DataRow drunit = dtunit.NewRow();
            drunit["DESCRIPTION"] = "Select Unit";
            dtunit.Rows.InsertAt(drunit, 0);
            lstBaseUom.DataSource = dtunit;
            lstBaseUom.DataValueField = "CODE";
            lstBaseUom.DataTextField = "DESCRIPTION";
            lstBaseUom.DataBind();

            DataTable dtplant = EquipDbClass.GetSapPlant();
            //DataRow drplant = dtplant.NewRow();
            //drplant["NAME"] = "Select Plant";
            //dtplant.Rows.InsertAt(drplant, 0);
            lstplant.DataSource = dtplant;
            lstplant.DataValueField = "CODE";
            lstplant.DataTextField = "NAME";
            lstplant.DataBind();

            DataTable dtbc = EquipDbClass.GetSapBatchClass();
            DataRow drbc = dtbc.NewRow();
            drbc["DESCRIPTION"] = "Select BatchClass";
            dtbc.Rows.InsertAt(drbc, 0);
            lstBatchClass.DataSource = dtbc;
            lstBatchClass.DataValueField = "CLASS_NUMBER";
            lstBatchClass.DataTextField = "DESCRIPTION";
            lstBatchClass.DataBind();

            //DataTable dtincoterms = EquipDbClass.GetSapIncoterms();

            DataTable dtincoterms = new DataTable();
            dtincoterms.Columns.Add("CODE", typeof(string));
            dtincoterms.Columns.Add("NAME", typeof(string));
            dtincoterms.Rows.Add("CFR", "CFR : Foreign Supply of Equipment: CFR Thai Port");
            dtincoterms.Rows.Add("CIF", "CIF : Foreign Supply of Equipment: CIF Thai Port");
            dtincoterms.Rows.Add("DDP", "DDP : Foreign Supply of Equipment: DDP EGAT's Store");
            dtincoterms.Rows.Add("ZTF", "ZTF : Foreign Supply of Equipment:  Local Transportation");
            dtincoterms.Rows.Add("ZCC", "ZCC : Foreign Supply of Equipment: Custom Clearance ");
            dtincoterms.Rows.Add("DDP", "DDP : Local Supply of Equipment: DDP EGAT's Store");
            dtincoterms.Rows.Add("EXW", "EXW : Local Supply of Equipment: Ex-Works");
            dtincoterms.Rows.Add("ZTL", "ZTL : Local Supply of Equipment: Local Transportation");
            dtincoterms.Rows.Add("F.C.", "F.C. : Foreign Currency");
            dtincoterms.Rows.Add("L.C.", "L.C. : Local Currency ");
            dtincoterms.Rows.Add("ZTT", "ZTT : Local Transportation to EGAT's Store");
            dtincoterms.Rows.Add("ZCN", "ZCN : Local Transportation, Construction and Installation");
            dtincoterms.Rows.Add("-", "Purchase Price Offered for Dismantled Equipment");
            lstIncoterms.DataSource = dtincoterms;
            lstIncoterms.DataValueField = "CODE";
            lstIncoterms.DataTextField = "NAME";
            lstIncoterms.DataBind();

        }
        private void BindFixData()
        {
            DataTable dtac = new DataTable();
            dtac.Columns.Add("CODE", typeof(string));
            dtac.Columns.Add("TEXT", typeof(string));
            dtac.Rows.Add("02", "02 : Individ.requirements");
            lstAval.DataSource = dtac;
            lstAval.DataValueField = "CODE";
            lstAval.DataTextField = "TEXT";
            lstAval.DataBind();

            DataTable dtmrp = new DataTable();
            dtmrp.Columns.Add("CODE", typeof(string));
            dtmrp.Columns.Add("TEXT", typeof(string));
            dtmrp.Rows.Add("ND", "ND : No planning");
            lstMrp.DataSource = dtmrp;
            lstMrp.DataValueField = "CODE";
            lstMrp.DataTextField = "TEXT";
            lstMrp.DataBind();

            DataTable dtpc = new DataTable();
            dtpc.Columns.Add("CODE", typeof(string));
            dtpc.Columns.Add("TEXT", typeof(string));
            dtpc.Rows.Add("V", "V : Moving average price/periodic unit price");
            dtpc.Rows.Add("S", "S : Standard price");
            lstPrice.DataSource = dtpc;
            lstPrice.DataValueField = "CODE";
            lstPrice.DataTextField = "TEXT";
            lstPrice.DataBind();

            DataTable dtpart = EquipDbClass.GetFixMajorParts();
            //dtpart.Columns.Add("CODE", typeof(string));
            //dtpart.Columns.Add("TEXT", typeof(string));
            //dtpart.Rows.Add("C1", "Foundation Work");
            //dtpart.Rows.Add("C2", "Cable Trench");
            //dtpart.Rows.Add("C3", "Control Buiding");
            //dtpart.Rows.Add("C4", "Earth Work Road");
            //dtpart.Rows.Add("C5", "Water Supply System");
            //dtpart.Rows.Add("C6", "Drainage System");
            //dtpart.Rows.Add("C7", "Special Contruction");
            lstPart.DataSource = dtpart;
            lstPart.DataValueField = "CODE";
            lstPart.DataTextField = "NAME";
            lstPart.DataBind();

            DataTable dtmat = new DataTable();
            dtmat.Columns.Add("CODE", typeof(string));
            dtmat.Columns.Add("TEXT", typeof(string));
            dtmat.Rows.Add("ZNV", "Major");
            dtmat.Rows.Add("ZCM", "Miscellaneous");
            dtmat.Rows.Add("SVR", "Service");
            lstMat.DataSource = dtmat;
            lstMat.DataValueField = "CODE";
            lstMat.DataTextField = "TEXT";
            lstMat.DataBind();

            DataTable dtgrp = new DataTable();
            dtgrp.Columns.Add("CODE", typeof(string));
            dtgrp.Columns.Add("NAME", typeof(string));
            dtgrp.Rows.Add("500 kV Suspension Insulator", "500 kV Suspension Insulator");
            dtgrp.Rows.Add("AL Tube and Miscellaneous Hardware", "AL Tube and Miscellaneous Hardware");
            dtgrp.Rows.Add("Bus Fitting", "Bus Fitting");
            dtgrp.Rows.Add("Conductor", "Conductor");
            dtgrp.Rows.Add("Connector and Miscelleous Hardware for Suspension", "Connector and Miscelleous Hardware for Suspension");
            dtgrp.Rows.Add("Grouding Material", "Grouding Material");
            dtgrp.Rows.Add("Insulator", "Insulator");
            dtgrp.Rows.Add("Rigid Steel Conduit", "Rigid Steel Conduit");
            ddlCat.DataSource = dtgrp;
            ddlCat.DataValueField = "CODE";
            ddlCat.DataTextField = "NAME";
            ddlCat.DataBind();

            DataTable dtvalclass = new DataTable();
            dtvalclass.Columns.Add("CODE", typeof(string));
            dtvalclass.Columns.Add("TEXT", typeof(string));
            dtvalclass.Rows.Add("1204", "1204 อะไหล่ / อุปกรณ์ Common");
            dtvalclass.Rows.Add("1601", "1601 Main Eq.Project stock");
            lstValClass.DataSource = dtvalclass;
            lstValClass.DataValueField = "CODE";
            lstValClass.DataTextField = "TEXT";
            lstValClass.DataBind();
        }
        private void BindDdlSerialNoCodeA()
        {
            ddlCodeA.Items.Clear();
            DataTable dt = EquipDbClass.GetSNCodeA();
            ddlCodeA.Items.Add(new ListItem("Please select", ""));

            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlCodeA.Items.Add(new ListItem(dr["code_a"].ToString() + ":" + dr["code_value"].ToString(), dr["code_a"].ToString()));
                }
            }
        }
        private void BindDdlSerialNoCodeB()
        {
            ddlCodeB.Items.Clear();
            DataTable dt = EquipDbClass.GetSNCodeB(ddlCodeA.SelectedValue);
            ddlCodeB.Items.Add(new ListItem("Please select", ""));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlCodeB.Items.Add(new ListItem(dr["code_b"].ToString() + ":" + dr["code_value"].ToString(), dr["code_b"].ToString()));
                }
            }
        }
        private void BindDdlSerialNoCodeC()
        {
            ddlCodeC.Items.Clear();
            DataTable dt = EquipDbClass.GetSNCodeC(ddlCodeA.SelectedValue, ddlCodeB.SelectedValue); // new DataTable();// get codeC
            ddlCodeC.Items.Add(new ListItem("Please select", ""));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlCodeC.Items.Add(new ListItem(dr["code_c"].ToString() + ":" + dr["code_value"].ToString(), dr["code_c"].ToString()));
                }
            }
        }
        private void BindDdlSerialNoCodeD()
        {
            ddlCodeD.Items.Clear();
            DataTable dt = EquipDbClass.GetSNCodeD();//new DataTable();// get codeD
            ddlCodeD.Items.Add(new ListItem("Please select", ""));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlCodeD.Items.Add(new ListItem(dr["code_d"].ToString() + ":" + dr["code_value"].ToString(), dr["code_d"].ToString()));
                }
            }
        }
        private void BindDdlSerialNoCodeE()
        {
            ddlCodeE.Items.Clear();
            DataTable dt = EquipDbClass.GetSNCodeE(ddlCodeA.SelectedValue, ddlCodeB.SelectedValue);// new DataTable();// get codeE
            ddlCodeE.Items.Add(new ListItem("Please select", ""));
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ddlCodeE.Items.Add(new ListItem(dr["code_e"].ToString() + ":" + dr["code_value"].ToString(), dr["code_e"].ToString()));
                }
            }
        }
        private void SetControlEditMode()
        {
            if (DDL_EQgroup.SelectedValue != "") DDL_EQgroup.Enabled = false;
            if (lstMatGrp.SelectedValue != "") lstMatGrp.Enabled = false;
            if (lstBaseUom.SelectedValue != "") lstBaseUom.Enabled = false;
            if (lstBatchClass.SelectedValue != "") lstBatchClass.Enabled = false;
            if (lstPart.SelectedValue != "") lstPart.Enabled = false;
            txtShtDesc.Enabled = false;
            lstMat.Enabled = false;
            txtEquip.Enabled = false;
            btnCreate.Visible = false;
            btnUpdadte.Visible = true;
        }

        protected void btnEquipUpdate_Click(object sender, EventArgs e)
        {
            EquipmentMasterModel objEquip = new EquipmentMasterModel();
            string strUpdateStatus = string.Empty;
            string mpart = string.Empty;
            foreach (ListItem item in lstPart.Items)
            {
                if (item.Selected)
                {
                    if (string.IsNullOrEmpty(mpart))
                    {
                        mpart = item.Value;
                    }
                    else
                    {
                        mpart += ',' + item.Value;
                    }
                }
            }
            string mincoterm = string.Empty;
            foreach (ListItem item in lstIncoterms.Items)
            {
                if (item.Selected)
                {
                    if (string.IsNullOrEmpty(mincoterm))
                    {
                        mincoterm = item.Value;
                    }
                    else
                    {
                        mincoterm += ',' + item.Value;
                    }
                }
            }
            objEquip.equip_code = txtEquip.Text.Trim().Replace("'", "''");
            objEquip.equip_desc_full = txtLongDesc.Text.Trim().Replace("'", "''");
            objEquip.equip_desc_short = txtShtDesc.Text.Trim().Replace("'", "''");
            objEquip.equip_gong = lblDepartment.Text.Trim().Replace("'", "''");
            objEquip.equip_pnag = lblSection.Text.Trim().Replace("'", "''");
            objEquip.isactive = lstStatus.SelectedValue;
            objEquip.remark = txtRemark.Text.Trim().Replace("'", "''");
            objEquip.unit = string.Empty;
            objEquip.materialgroup = lstMatGrp.SelectedValue;
            objEquip.legend = txtLegend.Text.Trim().Replace("'", "''");
            objEquip.cate_code = ddlCat.SelectedValue;
            objEquip.misc_per_unit = txtmiscell.Text.Trim().Replace("'", "''");
            objEquip.parts = mpart;
            objEquip.sap_material_type = lstMat.SelectedValue;
            objEquip.sap_material_group = lstMatGrp.SelectedValue;
            objEquip.sap_base_unit = lstBaseUom.SelectedValue;
            objEquip.sap_plant = lstplant.Text.Trim().Replace("'", "''");
            objEquip.sap_batch_manage = lstBatch.SelectedValue;
            objEquip.sap_batch_class = lstBatchClass.SelectedValue;
            objEquip.sap_value_class = lstValClass.SelectedValue;
            objEquip.sap_price_control = lstPrice.SelectedValue;
            objEquip.sap_mrp_type = lstMrp.SelectedValue;
            objEquip.sap_availability_check = lstAval.SelectedValue;
            objEquip.turnkey_buying_lot = txtTBuyLot.Text.Trim().Replace("'", "''") == "" ? "1" : txtTBuyLot.Text.Trim().Replace("'", "''");
            objEquip.turnkey_buying_unit = txtTBuyLotUnit.Text.Trim().Replace("'", "''");
            objEquip.turnkey_reservation = txtTReserv.Text.Trim().Replace("'", "''") == "" ? "0" : txtTReserv.Text.Trim().Replace("'", "''");
            objEquip.turnkey_remark = txtTRemark.Text.Trim().Replace("'", "''");
            objEquip.supply_buying_lot = txtSBuyLot.Text.Trim().Replace("'", "''") == "" ? "1" : txtSBuyLot.Text.Trim().Replace("'", "''");
            objEquip.supply_buying_unit = txtSBuyLotUnit.Text.Trim().Replace("'", "''");
            objEquip.supply_reservation = txtSReserv.Text.Trim().Replace("'", "''") == "" ? "0" : txtSReserv.Text.Trim().Replace("'", "''");
            objEquip.supply_remark = txtSRemark.Text.Trim().Replace("'", "''");
            objEquip.updated_by = strEmpId;
            objEquip.sn_code_a = ddlCodeA.SelectedValue;
            objEquip.sn_code_b = ddlCodeB.SelectedValue;
            objEquip.sn_code_c = ddlCodeC.SelectedValue;
            objEquip.sn_code_d = ddlCodeD.SelectedValue;
            objEquip.sn_code_e = ddlCodeE.SelectedValue;

            strUpdateStatus = EquipDbClass.UpdateEquipCode(objEquip);
            // need to update equip doc approve

            EquipmentEQGroup objeq = new EquipmentEQGroup();
            objeq.eg_code = DDL_EQgroup.SelectedValue;
            objeq.equip_code = txtEquip.Text;
            objeq.created_by = strEmpId;
            objeq.created_datetime = DateTime.Now;
            objeq.updated_by = strEmpId;
            objeq.updated_datetime = DateTime.Now;
            if (objeq.eg_code != "")
            {
                EquipDbClass.insertToEQlist(objeq);
            }


            foreach (GridViewRow dgi in gvBEquipInfo.Rows)
            {
                GridView gvSubEquip = (GridView)dgi.FindControl("gvSubEquip");
                foreach (GridViewRow gvr in gvSubEquip.Rows)
                {
                    EquipmentDoc objdoc = new EquipmentDoc();
                    Label lblDocType = (Label)gvr.FindControl("lblDocType");
                    Label lblDocNo = (Label)gvr.FindControl("lblDocNo");
                    Label lblDescription = (Label)gvr.FindControl("lblDescription");
                    Label lblRevision = (Label)gvr.FindControl("lblRevision");
                    Label lblDepartment = (Label)gvr.FindControl("lblDepartment");
                    Label lblSection = (Label)gvr.FindControl("lblSection");
                    Label lblUpdateBy = (Label)gvr.FindControl("lblUpdateBy");
                    Label lblEnqNo = (Label)gvr.FindControl("lblEnqNo");
                    Label lblDocnodeid = (Label)gvr.FindControl("lblDocnodeid");

                    objdoc.equip_code = txtEquip.Text.Trim().Replace("'", "''");
                    objdoc.doc_nodeid = lblDocnodeid.Text;
                    objdoc.created_by = lblUpdateBy.Text;
                    objdoc.created_datetime = DateTime.Now;
                    objdoc.updated_by = strEmpId;
                    objdoc.updated_datetime = DateTime.Now;

                    EquipDbClass.inserDocument(objdoc);
                }
            }

            for (int i = 0; i < gvBidNo.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)gvBidNo.Rows[i].FindControl("gvCheckBox");
                cb.Checked = false;
            }
            if (strUpdateStatus == string.Empty)
            {
                Response.Write("<script>alert('Equipment Code : " + txtEquip.Text + " has been updated.');</script>");
            }

            EquipDbClass.deleteDocument(hidEqcode.Value.Replace(",", "','"), hidDocid.Value.Replace(",", "','"));
            BindEquipDocList();
            hidDocid.Value = "";
            hidEqcode.Value = "";

        }

        protected void btnEquipCreate_Click(object sender, EventArgs e)
        {
            if (txtEquip.Text != "" && txtShtDesc.Text != "" && txtLongDesc.Text != "" && lstPart.SelectedValue != "" && lstBaseUom.SelectedValue != "")
            {

                string strInsertStatus = string.Empty;
                string mpart = string.Empty;
                foreach (ListItem item in lstPart.Items)
                {
                    if (item.Selected)
                    {
                        if (string.IsNullOrEmpty(mpart))
                        {
                            mpart = item.Value;
                        }
                        else
                        {
                            mpart += ',' + item.Value;
                        }
                    }
                }
                string mincoterm = string.Empty;
                foreach (ListItem item in lstIncoterms.Items)
                {
                    if (item.Selected)
                    {
                        if (string.IsNullOrEmpty(mincoterm))
                        {
                            mincoterm = item.Value;
                        }
                        else
                        {
                            mincoterm += ',' + item.Value;
                        }
                    }
                }

                EquipmentMasterModel createmodel = new EquipmentMasterModel
                {

                    equip_id = "0",
                    equip_code = txtEquip.Text,
                    equip_desc_full = txtLongDesc.Text,
                    equip_desc_short = txtShtDesc.Text,
                    equip_gong = lblDepartment.Text,
                    equip_pnag = lblSection.Text,
                    isactive = lstStatus.SelectedValue,
                    is_service = this.lstMat.SelectedValue == "SVR" ? "1" : "0",
                    remark = txtRemark.Text,
                    unit = string.Empty,
                    materialgroup = lstMatGrp.SelectedValue,
                    groupsort = string.Empty,
                    isimport = "0",
                    legend = txtLegend.Text,
                    cate_code = ddlCat.SelectedValue,
                    misc_per_unit = "0",
                    parts = mpart,
                    sap_material_type = lstMat.SelectedValue,
                    sap_material_group = lstMatGrp.SelectedValue,
                    sap_base_unit = lstBaseUom.SelectedValue,
                    sap_plant = lstplant.Text,
                    sap_batch_manage = lstBatch.SelectedValue,
                    sap_batch_class = lstBatchClass.SelectedValue,
                    sap_value_class = lstValClass.SelectedValue,
                    sap_price_control = lstPrice.SelectedValue,
                    sap_mrp_type = lstMrp.SelectedValue,
                    sap_availability_check = lstAval.SelectedValue,
                    turnkey_buying_lot = txtTBuyLot.Text == "" ? "1" : txtTBuyLot.Text,
                    turnkey_buying_unit = txtTBuyLotUnit.Text,
                    turnkey_reservation = txtTReserv.Text == "" ? "0" : txtTReserv.Text,
                    turnkey_remark = txtTRemark.Text,
                    supply_buying_lot = txtSBuyLot.Text == "" ? "1" : txtSBuyLot.Text,
                    supply_buying_unit = txtSBuyLotUnit.Text,
                    supply_reservation = txtSReserv.Text == "" ? "0" : txtSReserv.Text,
                    supply_remark = txtSRemark.Text,
                    created_by = strEmpId,
                    created_datetime = DateTime.Now.ToString(),
                    updated_by = strEmpId,
                    updated_datetime = DateTime.Now.ToString(),
                    sn_code_a = ddlCodeA.SelectedValue,
                    sn_code_b = ddlCodeB.SelectedValue,
                    sn_code_c = ddlCodeC.SelectedValue,
                    sn_code_d = ddlCodeD.SelectedValue,
                    sn_code_e = ddlCodeE.SelectedValue,

                };

                strInsertStatus = EquipDbClass.CreateEquipCode(createmodel);

                EquipmentEQGroup objeq = new EquipmentEQGroup();
                objeq.eg_code = DDL_EQgroup.SelectedValue;
                objeq.equip_code = txtEquip.Text;
                objeq.created_by = strEmpId;
                objeq.created_datetime = DateTime.Now;
                objeq.updated_by = strEmpId;
                objeq.updated_datetime = DateTime.Now;
                if (objeq.eg_code != "")
                {
                    EquipDbClass.insertToEQlist(objeq);
                }


                foreach (GridViewRow dgi in gvBEquipInfo.Rows)
                {
                    GridView gvSubEquip = (GridView)dgi.FindControl("gvSubEquip");
                    foreach (GridViewRow gvr in gvSubEquip.Rows)
                    {
                        EquipmentDoc objdoc = new EquipmentDoc();
                        Label lblDocType = (Label)gvr.FindControl("lblDocType");
                        Label lblDocNo = (Label)gvr.FindControl("lblDocNo");
                        Label lblDescription = (Label)gvr.FindControl("lblDescription");
                        Label lblRevision = (Label)gvr.FindControl("lblRevision");
                        Label lblDepartment = (Label)gvr.FindControl("lblDepartment");
                        Label lblSection = (Label)gvr.FindControl("lblSection");
                        Label lblUpdateBy = (Label)gvr.FindControl("lblUpdateBy");
                        Label lblEnqNo = (Label)gvr.FindControl("lblEnqNo");
                        Label lblDocnodeid = (Label)gvr.FindControl("lblDocnodeid");

                        objdoc.equip_code = txtEquip.Text.Trim().Replace("'", "''");
                        objdoc.doc_nodeid = lblDocnodeid.Text;
                        objdoc.created_by = lblUpdateBy.Text;
                        objdoc.created_datetime = DateTime.Now;
                        objdoc.updated_by = strEmpId;
                        objdoc.updated_datetime = DateTime.Now;

                        EquipDbClass.inserDocument(objdoc);
                    }
                }

                //BindEquipDocList();
                gvBEquipInfo.DataSource = null;
                gvBEquipInfo.DataBind();


                if (strInsertStatus == string.Empty)
                {

                    Response.Write("<script>alert('Equipment Code : " + txtEquip.Text + " has been created.');</script>");
                    SetDefaultForm();
                }
                else
                {
                    Response.Write("<script>alert('" + strInsertStatus + "');</script>");
                }

            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Data is Require(*)');", true);
            }
        }
        protected void btnEquipCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("EquipmentSearch.aspx?zuserlogin=" + strEmpId);
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        protected void modalSearch_click(object sender, ImageClickEventArgs e)
        {

        }
        protected void ImageButtonBin_Click(object sender, ImageClickEventArgs e)
        {

        }
        protected void btnSelect_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            DataTable dt2 = EquipDbClass.GetEquipDocByEquipCode(txtEquip.Text.Trim(), lblDepartment.Text);
            dt = EquipDbClass.getDoctypeDash();
            if (ViewState["dtSource"] != null) dt2 = (DataTable)ViewState["dtSource"];

            //if (dt.Rows.Count > 0)
            //{

            //}
            //else
            //{
            //    dt = GetDtHeader();

            //}

            for (int i = 0; i < gvBidNo.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)gvBidNo.Rows[i].FindControl("gvCheckBox");
                Label node = (Label)gvBidNo.Rows[i].FindControl("gvpoplblDocNode");
                if (cb.Checked)
                {
                    DataRow[] drr = dt2.Select("doc_nodeid = '" + node.Text + "'");
                    if (drr.Length == 0)
                    {
                        DataRow dr = dt2.NewRow();
                        dr["equip_code"] = txtEquip.Text;
                        dr["doctype_code"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocType")).Text;
                        dr["doctype_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocTypeName")).Text;
                        dr["doc_desc"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDescription")).Text;
                        dr["doc_revision_display"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblRevision")).Text;
                        //dr["bidenq_no"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblEnqNo")).Text;
                        dr["updated_by"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblUpdateBy")).Text;
                        dr["depart_position_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDepartment")).Text;
                        dr["section_position_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblSection")).Text;

                        dr["doc_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocName")).Text;
                        dr["doc_nodeid"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocNode")).Text;

                        dt2.Rows.Add(dr);
                    }

                }
            }
            if(dt2.Rows.Count >0) ViewState["dtSource"] = dt2;

            if (dt.Rows.Count > 0)
            {
                //Session.Add("dtSource", dt);
                //ViewState["dtSource"] = dt;
                gvBEquipInfo.DataSource = dt;
                gvBEquipInfo.DataBind();
            }
            else
            {
                //BindFormData();
            }
            searchDoc.Style["display"] = "none";
        }
        protected void btnSearch_click(object sender, EventArgs e)
        {
            searchDoc.Style.Add("display", "block");
            string strType = DdlSearch.SelectedValue;
            string strText = txtSearch.Text;
            DataTable dtSource = EquipDbClass.GetDataList(strType, strText, lblDepartment.Text);

            if (dtSource.Rows.Count > 0)
            {
                gvBidNo.DataSource = dtSource;
                gvBidNo.DataBind();
            }
            else
            {
                gvBidNo.DataSource = GetDtHeader();
                gvBidNo.DataBind();
            }

        }
        protected void gvSubEquip_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "ClearList")
            {
                int rowid = Convert.ToInt32(e.CommandArgument);

                DataTable dtSource = (DataTable)ViewState["dtSource"];
                hidDocid.Value = (hidDocid.Value == "" ? "" : ",") + dtSource.Rows[rowid]["doc_nodeid"].ToString();
                hidEqcode.Value = (hidEqcode.Value == "" ? "" : ",") + dtSource.Rows[rowid]["equip_code"].ToString();
                dtSource.Rows.RemoveAt(rowid);
                ViewState["dtSource"] = dtSource;
                gvBEquipInfo.DataSource = dtSource;
                gvBEquipInfo.DataBind();


                //DataRow[] drselect = dtSource.Select("rowid =" + rowid);
                //dtSource.Rows.Remove(drselect[0]);
                //if (dtSource.Rows.Count > 0)
                //{
                //    gvBEquipInfo.DataSource = dtSource;
                //    gvBEquipInfo.DataBind();
                //}
                //else
                //{
                //    BindFormData();
                //}
            }
        }

        private void BindDataToContol(string strEquipCode)
        {
            EquipmentMasterModel model = EquipDbClass.GetSearchResult(strEquipCode);
            {
                try
                {
                    txtEquip.Text = model.equip_code;
                    txtLongDesc.Text = model.equip_desc_full;
                    txtShtDesc.Text = model.equip_desc_short;
                    //lblSection.Text = model.equip_gong;
                    //lblDepartment.Text = model.equip_pnag;
                    model.isactive = model.isactive;
                    txtRemark.Text = model.remark;
                    model.unit = string.Empty;
                    model.groupsort = string.Empty;
                    model.isimport = "0";
                    txtLegend.Text = model.legend;
                    // model.misc_per_unit = model.misc_per_unit;

                    if (string.IsNullOrEmpty(model.parts))
                        lstPart.Items.Add(model.parts);
                    if (string.IsNullOrEmpty(model.cate_code))
                        ddlCat.Items.Add(model.cate_code);
                    if (string.IsNullOrEmpty(model.sap_material_type))
                        lstMat.Items.Add(model.sap_material_type);

                    if (string.IsNullOrEmpty(model.sap_material_group))
                        lstMatGrp.Items.Add(model.sap_material_group);
                    if (string.IsNullOrEmpty(model.sap_base_unit))
                        lstBaseUom.Items.Add(model.sap_base_unit);
                    if (string.IsNullOrEmpty(model.sap_plant))
                        lstplant.Items.Add(model.sap_plant);
                    if (string.IsNullOrEmpty(model.sap_batch_manage))
                        lstBatch.Items.Add(model.sap_batch_manage);
                    if (string.IsNullOrEmpty(model.sap_batch_class))
                        lstBatchClass.Items.Add(model.sap_batch_class);
                    if (string.IsNullOrEmpty(model.sap_price_control))
                        lstPrice.Items.Add(model.sap_price_control);
                    if (string.IsNullOrEmpty(model.sap_mrp_type))
                        lstMrp.Items.Add(model.sap_mrp_type);
                    if (string.IsNullOrEmpty(model.sap_availability_check))
                        lstAval.Items.Add(model.sap_availability_check);
                    if (string.IsNullOrEmpty(model.sap_value_class))
                        lstValClass.Items.Add(model.sap_value_class);

                    lstPart.ClearSelection();
                    ddlCat.ClearSelection();
                    lstMat.ClearSelection();
                    lstMatGrp.ClearSelection();
                    lstBaseUom.ClearSelection();
                    lstplant.ClearSelection();
                    lstBatch.ClearSelection();
                    lstBatchClass.ClearSelection();
                    lstPrice.ClearSelection();
                    lstMrp.ClearSelection();
                    lstAval.ClearSelection();
                    lstValClass.ClearSelection();
                    if (!String.IsNullOrEmpty(model.parts))
                    {
                        string[] splitPart = model.parts.Split(',');
                        foreach (string strPart in splitPart)
                        {
                            lstPart.Items.FindByValue(strPart).Selected = true;
                        }
                    }
                    ddlCat.Items.FindByText(model.cate_code).Selected = true;
                    lstMat.Items.FindByValue(model.sap_material_type).Selected = true;
                    lstMatGrp.Items.FindByValue(model.sap_material_group).Selected = true;
                    lstBaseUom.Items.FindByValue(model.sap_base_unit).Selected = true;
                    lstplant.Items.FindByValue(model.sap_plant).Selected = true;
                    lstBatch.Items.FindByValue(model.sap_batch_manage).Selected = true;
                    lstBatchClass.Items.FindByValue(model.sap_batch_class).Selected = true;
                    lstPrice.Items.FindByValue(model.sap_price_control).Selected = true;
                    lstMrp.Items.FindByValue(model.sap_mrp_type).Selected = true;
                    lstAval.Items.FindByValue(model.sap_availability_check).Selected = true;
                    lstValClass.Items.FindByValue(model.sap_value_class);
                    lstStatus.Items.FindByValue(model.isactive).Selected = true;

                    txtmiscell.Text = model.misc_per_unit;
                    txtTBuyLot.Text = model.turnkey_buying_lot;
                    txtTBuyLotUnit.Text = model.turnkey_buying_unit;
                    txtTReserv.Text = model.turnkey_reservation;
                    txtTRemark.Text = model.turnkey_remark;
                    txtSBuyLot.Text = model.supply_buying_lot;
                    txtSBuyLotUnit.Text = model.supply_buying_unit;
                    txtSReserv.Text = model.supply_reservation;
                    txtSRemark.Text = model.supply_remark;
                    model.created_by = strEmpId;
                    model.created_datetime = DateTime.Now.ToString();
                    model.updated_by = strEmpId;
                    model.updated_datetime = DateTime.Now.ToString();

                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                }
                //model.equip_id = "0"
            }
        }
        protected void lstMat_SelectedIndexChanged(object sender, EventArgs e)
        {
            CheckVisibleCintrolByMat();
        }

        private void BindEquipDocList()
        {
            try
            {
                if (strMode != "E")
                {
                    gvBEquipInfo.DataSource = null;
                    gvBEquipInfo.DataBind();
                }
                else
                {
                    DataTable dt2 = EquipDbClass.GetEquipDocByEquipCode(txtEquip.Text.Trim(), lblDepartment.Text);
                    if (dt2.Rows.Count > 0)
                    {
                        //Session.Add("dtSource", dt);
                        ViewState["dtSource"] = dt2;
                    }
                    DataTable dt = EquipDbClass.getDoctypeDash();
                    gvBEquipInfo.DataSource = dt;
                    gvBEquipInfo.DataBind();
                  
                }
           
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void ddlCodeA_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDdlSerialNoCodeB();
            BindDdlSerialNoCodeC();
            BindDdlSerialNoCodeE();
        }

        protected void ddlCodeB_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindDdlSerialNoCodeC();
            BindDdlSerialNoCodeE();
        }

        protected void gvBEquipInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                GridView gv = (GridView)e.Row.FindControl("gvSubEquip");
                DataTable dt2 = new DataTable();
               
                if (ViewState["dtSource"] == null)
                    dt2 = EquipDbClass.GetEquipDocByEquipCode(txtEquip.Text.Trim(), lblDepartment.Text);
                else
                     dt2 = (DataTable)ViewState["dtSource"];
                
                DataView dv = new DataView(dt2);
                dv.RowFilter = "doctype_code='" + drv["doctype_code"].ToString() + "'";
                DataTable dt = dv.ToTable();

                for (int i = 0; i < gvBidNo.Rows.Count; i++)
                {
                    CheckBox cb = (CheckBox)gvBidNo.Rows[i].FindControl("gvCheckBox");
                    Label node = (Label)gvBidNo.Rows[i].FindControl("gvpoplblDocNode");
                    string doccode = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocType")).Text;
                    if (cb.Checked)
                    {
                        DataRow[] drr = dt.Select("doc_nodeid = '" + node.Text + "'");
                        if (drr.Length == 0)
                        {
                            if (drv["doctype_code"].ToString() == doccode)
                            {
                                DataRow dr = dt.NewRow();
                                dr["equip_code"] = txtEquip.Text;
                                dr["doctype_code"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocType")).Text;
                                dr["doctype_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocTypeName")).Text;
                                dr["doc_desc"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDescription")).Text;
                                dr["doc_revision_display"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblRevision")).Text;
                                //dr["bidenq_no"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblEnqNo")).Text;
                                dr["updated_by"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblUpdateBy")).Text;
                                dr["depart_position_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDepartment")).Text;
                                dr["section_position_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblSection")).Text;

                                dr["doc_name"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocName")).Text;
                                dr["doc_nodeid"] = ((Label)gvBidNo.Rows[i].FindControl("gvpoplblDocNode")).Text;

                                dt.Rows.Add(dr);
                            }
                        }

                    }
                }
                gv.DataSource = dt;
                gv.DataBind();
            }
        }
        protected void gvSubEquip_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                HyperLink hlnk_DocNo = (HyperLink)e.Row.FindControl("hlnk_DocNo");
                hlnk_DocNo.Text = "<a href = '" + drv["link_doc"].ToString() + "' TARGET='openWindow' > " + drv["doc_name"].ToString() + " </ a >";

            }
        }


        protected void btnColsep_Click(object sender, EventArgs e)
        {
            searchDoc.Style["display"] = "none";
        }

        protected void gvBidNo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            searchDoc.Style["display"] = "block";
            gvBidNo.PageIndex = e.NewPageIndex;

            string strType = DdlSearch.SelectedValue;
            string strText = txtSearch.Text;
            DataTable dtSource = EquipDbClass.GetDataList(strType, strText, lblDepartment.Text);

            if (dtSource.Rows.Count > 0)
            {
                gvBidNo.DataSource = dtSource;
                gvBidNo.DataBind();
            }
        }
        protected void img_del_Click(object sender, ImageClickEventArgs e)
        {
            System.Web.UI.WebControls.Image Img_Del = (System.Web.UI.WebControls.Image)sender;
            GridViewRow gvr = (GridViewRow)Img_Del.NamingContainer;
            GridView gv = (GridView)gvr.NamingContainer;
            string nodeid = ((Label)gvr.FindControl("lblDocnodeid")).Text;
            string doccode = ((Label)gvr.FindControl("lblDocTypesub")).Text;
            DataTable dtrow = (DataTable)ViewState["dtSource"];
            DataView dv = new DataView(dtrow);
            string filter = " doctype_code='" + doccode + "'";// and doc_nodeid='" + nodeid + "'";
            dv.RowFilter = filter;
            DataTable dt2 = dv.ToTable();


            for (int i = dt2.Rows.Count - 1; i >= 0; i--)
            {
                DataRow dr = dt2.Rows[i];
                if (dr["doc_nodeid"].ToString() == nodeid && dr["doctype_code"].ToString() == doccode)
                    dt2.Rows.RemoveAt(i);
                //dr.Delete();

            }
            ViewState["dtSource"] = dt2;
            gv.DataSource = dt2;
            gv.DataBind();

            hidDocid.Value += (hidDocid.Value == "" ? "" : ",") + nodeid;
            hidEqcode.Value = (hidEqcode.Value == "" ? "" : ",") + txtEquip.Text.Trim();


            //dt2.AcceptChanges();
            //dtrow.Rows.Remove(dtrow.Rows["doc_nodeid"].ToString());
            // int rowid = Convert.ToInt32(e.CommandArgument);

            // DataTable dtSource = (DataTable)ViewState["dtSource"];

            //dtSource.Rows.RemoveAt(rowid);
            //ViewState["dtSource"] = dtSource;
            //gvBEquipInfo.DataSource = dtSource;
            //gvBEquipInfo.DataBind();

        }
    }
}