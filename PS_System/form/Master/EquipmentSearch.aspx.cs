﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class EquipmentSearch : System.Web.UI.Page
    {
        clsEquipmentMasterdb objEquip = new clsEquipmentMasterdb();
        public string strEmpIdLogin = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["zuserlogin"] != null)
            {
                strEmpIdLogin = Request.QueryString["zuserlogin"];

            }
            else
            {
                strEmpIdLogin = "z593503";// for test only
            }
            if (!IsPostBack)
            {

            }
        }
         
        private void BindingGv()
        {
            //string strEquipCodeSearch, strEquipDesSearch, strLegendSearch;
            //strEquipCodeSearch = strEquipDesSearch = strLegendSearch = string.Empty;

            DataTable dt = objEquip.GetEquipment(tbxEquipCode.Text.Trim(), tbxEquipDesc.Text.Trim(), tbxLegend.Text.Trim(), ddlStatus.SelectedValue);
            ViewState["dt_equip_search"] = dt;
            if (dt.Rows.Count > 0)
            {
                btnExport.Visible = true;
            }
            else
            {
                btnExport.Visible = false;
            }
            gvEquipList.DataSource = dt;
            gvEquipList.DataBind();

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGv();
        }

        protected void btnCreate_Click(object sender, EventArgs e)
        {
            Response.Redirect("Equipment.aspx?zuserlogin="+ strEmpIdLogin);
        }

        protected void gvEquipList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvEquipList.PageIndex = e.NewPageIndex;
            this.BindingGv();
        }

        public string GetUrl(object equipCode)
        {
            string url = "Equipment.aspx?equipcode=" + equipCode.ToString() + "&zuserlogin=" + strEmpIdLogin;
            return url;
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string strFileName = "Export_Equipment_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // export grid view to excel
            DataTable dtResult = (DataTable)ViewState["dt_equip_search"];

            byte[] byte1 = objEquip.GenExcel(dtResult);// objBasePrice.GenExcel(dtResult);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "binary/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
            Response.BinaryWrite(byte1);
            Response.Flush();
            Response.End();
        }
    }
}