﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Equipment.aspx.cs" Inherits="PS_System.form.Master.Equipment" MaintainScrollPositionOnPostback="true"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Equipment Master</title>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.js"></script>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $('#Panel1').hide();
            $('#txtTBuyLot').blur();
            $('#txtTBuyLotUnit').blur();
            $('#txtSBuyLot').blur();
            $('#txtSBuyLotUnit').blur();
            $('#txtTReserv').blur();
            $('#txtTReservUnit').blur();
            $('#txtSReserv').blur();
            $('#txtSReservUnit').blur();

            $('[id*=lstIncoterms]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                disableIfEmpty: true,
                maxHeight: 500,
                length: 500,
                numberDisplayed: 1
            });

            $('[id*=lstManu]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                length: 500,
                numberDisplayed: 1
            });
            
            $('[id*=lstPart]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                disableIfEmpty: true,
                maxHeight: 500,
                length: 500,
                numberDisplayed: 1
            });

            $('[id*=lstStatus]').multiselect({
                maxHeight: 500
            });
            $('[id*=lstBaseUom]').multiselect({
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: 500
            });
            $('[id*=ddlCat]').multiselect({
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: 500
            });
            $('[id*=DDL_EQgroup]').multiselect({
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: 500
            });
            $('[id*=lstMatGrp]').multiselect({
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: 500
            });
            $('[id*=lstBatchClass]').multiselect({
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: 500
            });

            $('#lstMat').change(function () {
                var matt = $('#lstMat').val();
                var legend = $('#txtLegend');
                var cat = $('#ddlCat');
                var misc = $('#txtmiscell');
                var llegend = $('#lblLegend');
                var lcat = $('#lblCate');
                var lmisc = $('#lblMisc');

                if (matt != 'ZCM') {
                    legend.hide();
                    cat.hide();
                    misc.hide();
                    llegend.hide();
                    lcat.hide();
                    lmisc.hide();

                } else {
                    legend.show();
                    cat.show();
                    misc.show();
                    llegend.show();
                    lcat.show();
                    lmisc.show();
                }
            });
        });


        function searchInput() {
            let searchValue = $("#search-input").val();
            searchValue = Number(searchValue);
            console.log(searchValue);
            getBid(searchValue);//when typing fire getBid
        }

        function modalToggle() {
            console.log("MODAL FIRE");
            $('#search-modal').modal('toggle');
        }
        function getBid(bidID) {

            /*
             * [
             * {
             *  id : "XXX",
             * }
             * 
             * ]
            let getURL = "https://localhost/api/something";
            $.get(getURL, data => {
                let htmlReplace = `                                    
                <tr>
                    <th scope="row">${bidID}</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                </tr >
                `;
                $('#get-modal-search-result').html(htmlReplace);
            });
            */

            let htmlReplace = `                                    
                <tr>
                    <th scope="row">${bidID}</th>
                        <td>Mark</td>
                        <td>Otto</td>
                        <td>@mdo</td>
                </tr >

                `;
            $('#get-modal-search-result').html(htmlReplace);
        }

        $(function () {
            $("[id*=ImageButtonSearch]").click(function () {
                $('#searchDoc').show();
                return false;
            });
        });
        $(function () {
            $("[id*=LbSearch]").click(function () {
                $('#searchDoc').show();
                return false;
            });
        });
        function toggle() {
            $("#Panel1").toggle()
        };
        function closeSearchDoc() {
            $('#searchDoc').hide();
        }


        function chkAll(Checkbox, gv) {
           
            var TargetControl = document.getElementById(gv);
            var cb = document.getElementById(Checkbox);
            var TargetChildControl = "gvCheckBox";
            var Inputs = TargetControl.getElementsByTagName("input");

            for (var n = 0; n < Inputs.length; n++) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false) {
                    Inputs[n].checked = cb.checked;
                }
            }
        }

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
        //var text1 = 1;
        //var text2 = "Unit";
        //var text3 = 0;
        //var text4 = "%";

        //function WaterMark1(txt, evt) {
        //    if (txt.value.length == 0 && evt.type == "blur") {
        //        txt.style.color = "gray";
        //        txt.value = text1;
        //    }
        //    if (txt.value == text1 && evt.type == "focus") {
        //        txt.style.color = "black";
        //        txt.value = "";
        //    }
        //}
        //function WaterMark2(txt, evt) {
        //    if (txt.value.length == 0 && evt.type == "blur") {
        //        txt.style.color = "gray";
        //        txt.value = text2;
        //    }
        //    if (txt.value == text2 && evt.type == "focus") {
        //        txt.style.color = "black";
        //        txt.value = "";
        //    }
        //}
        //function WaterMark3(txt, evt) {
        //    if (txt.value.length == 0 && evt.type == "blur") {
        //        txt.style.color = "gray";
        //        txt.value = text3;
        //    }
        //    if (txt.value == text3 && evt.type == "focus") {
        //        txt.style.color = "black";
        //        txt.value = "";
        //    }
        //}
        //function WaterMark4(txt, evt) {
        //    if (txt.value.length == 0 && evt.type == "blur") {
        //        txt.style.color = "gray";
        //        txt.value = text4;
        //    }
        //    if (txt.value == text4 && evt.type == "focus") {
        //        txt.style.color = "black";
        //        txt.value = "";
        //    }
        //}
    </script>

    <style>
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }

        .auto-style1 {
            width: 25%;
        }

        .auto-style2 {
            width: 100%;
            height: 250px;
            border-style: solid;
            border-width: 1px;
        }

        .auto-style3 {
            width: 88px;
        }

        .auto-style4 {
            width: 200px;
        }

        .auto-style6 {
            width: 70px;
        }

        .auto-style8 {
            width: 25px;
        }

        .auto-style9 {
            width: 25%;
        }

        .auto-style10 {
            width: 25%;
        }

        .auto-style11 {
            width: 25%;
        }

        .form-con {
    width: 100%;
    height: 34px;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.428571429;
    color: #555555;
    vertical-align: middle;
    background-color: #ffffff;
    background-image: none;
    border: 1px solid #cccccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
    -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
    transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
}
    </style>

</head>
<body>
    <form id="form1" runat="server">
        <!-- The Modal -->
        <div id="searchDoc" runat="server"  class="modal">
            <!-- Modal content  -->
            <div class="modal-content">
                <span id="searchlbl">Search Text</span>
                <asp:TextBox ID="txtSearch" runat="server" Width="30%" class="form-con" ></asp:TextBox>
                &nbsp; <asp:DropDownList ID="DdlSearch" runat="server" Width="200px" Class="form-con"></asp:DropDownList>
                &nbsp;&nbsp; <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-primary" OnClick="btnSearch_click" />
                <div  align="center">
                    <br />
                <asp:GridView ID="gvBidNo" HeaderStyle-BackColor="#3AC0F2" HeaderStyle-ForeColor="White" runat="server" AutoGenerateColumns="false"  BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                   Font-Names="tahoma" Font-Size="9pt" AllowPaging="True" OnPageIndexChanging="gvBidNo_PageIndexChanging" PageSize="20" >
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="center" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                    <Columns>
                        <asp:TemplateField HeaderText="Select">
                            <HeaderTemplate>
                                <asp:CheckBox ID="gvCb_all" runat="server" Text="Select All"  onclick="chkAll(this.id,gvBidNo.id)"/>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:CheckBox ID="gvCheckBox" runat="server" />
                            </ItemTemplate>
                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="80px" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                   <%# Container.DataItemIndex + 1 %>
                                <asp:Label ID="gvpoplblNo" runat="server" Text='<%# Bind("rowid") %>'  Visible="false"></asp:Label>
                                        <asp:Label ID="gvpoplblDocName" runat="server" Text='<%# Bind("doc_name") %>' Visible="false"></asp:Label>
                                     <asp:Label ID="gvpoplblDocNode" runat="server" Text='<%# Bind("doc_nodeid") %>' Visible="false"></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Type">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblDocType" runat="server" Text='<%# Bind("doctype_code") %>' Visible="false"></asp:Label>
                                  <asp:Label ID="gvpoplblDocTypeName" runat="server" Text='<%# Bind("doctype_name") %>' ></asp:Label>
                 
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document No.">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblDocNo" runat="server" Text='<%# Bind("doc_name") %>' Visible="false"></asp:Label>
                                <asp:Label ID="lnkdoc_name" runat="server" Text='<%# Bind("doc_namelnk") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="250px" HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblDescription" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Revision">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblRevision" runat="server" Text='<%# Bind("doc_revision_no") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <%--                        <asp:TemplateField HeaderText="Update date">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblEnqNo" runat="server" Text='<%# Bind("bidenq_no") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>--%>
                        <asp:TemplateField HeaderText="Update by">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblUpdateBy" runat="server" Text='<%# Bind("updated_by") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="กอง">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblDepartment" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="แผนก">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblSection" runat="server" Text='<%# Bind("section_position_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                    </div>
                <div class="modal-footer">
                    <asp:Button ID="btnSelect" runat="server" Text="Select" class="btn btn-primary" OnClick="btnSelect_Click" />
                    <asp:Button ID="btnColsep" runat="server" Text="Close" class="btn btn-danger" OnClick="btnColsep_Click"/>
                 <%--   <button onclick="closeSearchDoc();return false;" class="btn btn-danger">Close</button>--%>

                </div>
            </div>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Equipment Master"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 20px">
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left;">
                        <asp:Label ID="lblSection0" runat="server" Font-Bold="True" Font-Size="X-Large">Equipment Master</asp:Label>&nbsp;
                        <%--<asp:TextBox ID="txtSearchEquipCode" runat="server"></asp:TextBox>
                        &nbsp;<asp:Button ID="btnsearcheq" runat="server" Text="Search" OnClick="btnsearcheq_Click" />--%>
                    </td>
                </tr>
                <tr>
                    <td style="padding-bottom: 15px;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="text-align: left;" colspan="2">
                                    <asp:Label ID="Label1" runat="server" Text="กอง :" Font-Bold="True" Font-Size="Large"></asp:Label>&nbsp;<asp:Label ID="lblDepartment" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label><br />
                                </td>
                                <td style="text-align: left;" colspan="2">
                                    <asp:Label ID="Label2" runat="server" Text="แผนก:" Font-Bold="True" Font-Size="Large"></asp:Label>&nbsp;<asp:Label ID="lblSection" runat="server" Font-Bold="True" Font-Size="Large"></asp:Label><br />
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style1">
                                    <asp:Label ID="Label4" runat="server" Text="Label">Material / Item Type:</asp:Label>
                                    <br />
                                    <asp:DropDownList ID="lstMat" runat="server" Class="form-control" Width="80%" Height="40px" AutoPostBack="true" OnSelectedIndexChanged="lstMat_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left;" class="auto-style1">
                                    <asp:Label ID="lblLegend" runat="server" Text="Label">Legend:</asp:Label>
                                    <asp:TextBox ID="txtLegend" runat="server" Class="form-control" Width="80%" Height="40px"></asp:TextBox>
                                    <%--                        <asp:TemplateField HeaderText="Update date">
                            <ItemTemplate>
                                <asp:Label ID="gvpoplblEnqNo" runat="server" Text='<%# Bind("bidenq_no") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                        </asp:TemplateField>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="lblCate" runat="server" Text="Label">Category:</asp:Label>
                                    <asp:DropDownList ID="ddlCat" runat="server" Class="form-control" Width="80%" Height="40px"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="txtLegend" ID="RequiredFieldValidator2" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="lblMisc" runat="server" Text="Label">Miscellaneous Per Unit:</asp:Label>
                                    <asp:TextBox ID="txtmiscell" runat="server" Class="form-control" Width="80%" Height="40px"></asp:TextBox>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="ddlCat" ID="RequiredFieldValidator3" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>

                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style1">
                                    <asp:Label ID="lblequipno" runat="server" Text="Label">Equipment /Old Material No.*:</asp:Label>
                                    <asp:TextBox ID="txtEquip" runat="server" Class="form-control" Width="80%" Height="40px"></asp:TextBox>
                                </td>
                                <td style="text-align: left;" class="auto-style1">&nbsp;</td>
                                <td style="text-align: left;" class="auto-style10">&nbsp;</td>
                                <td style="text-align: left;" class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left;">
                                    <asp:Label ID="Label6" runat="server" Text="Label">Short Description*:</asp:Label><br />
                                    <asp:TextBox ID="txtShtDesc" runat="server" Class="form-control" Width="80%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="txtEquip" ID="RequiredFieldValidator1" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" colspan="3">
                                    <asp:Label ID="Label8" runat="server" Text="Label">Long Description*:</asp:Label>
                                    <asp:TextBox ID="txtLongDesc" runat="server" Class="form-control" Width="80%" Height="70px" TextMode="MultiLine"></asp:TextBox>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="txtShtDesc" ID="RequiredFieldValidator5" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style1">
                                    <asp:Label ID="Label28" runat="server" Text="Major Parts*:"></asp:Label>
                                    <asp:ListBox ID="lstPart" runat="server" Class="form-control" Width="80%" Height="40px" SelectionMode="Multiple"></asp:ListBox>

                                </td>
                                <td style="text-align: left;" class="auto-style1">
                                    <asp:Label ID="Label12" runat="server" Text="Label">Equipment Group:</asp:Label>
                                    <asp:DropDownList ID="DDL_EQgroup" runat="server" Class="form-control" Width="80%" Height="40px"></asp:DropDownList>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label5" runat="server" Text="Label">Material Code (SAP):</asp:Label>
                                    <asp:TextBox ID="txtMatcd" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False"></asp:TextBox>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label7" runat="server" Text="Label">Material Group:</asp:Label>
                                    <asp:DropDownList ID="lstMatGrp" runat="server" Class="form-control" Width="80%" Height="40px"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="txtLongDesc" ID="RequiredFieldValidator6" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style1">
                                    <asp:Label ID="Label9" runat="server" Text="Label">Base Unit of Measure*:</asp:Label>
                                    <asp:DropDownList ID="lstBaseUom" runat="server" Class="form-control" Width="80%" Height="40px"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="lstMatGrp" ID="RequiredFieldValidator7" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style9">
                                    <asp:Label ID="Label11" runat="server" Text="Label">Plant:</asp:Label><br />
                                    <asp:DropDownList ID="lstplant" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="lstBaseUom" ID="RequiredFieldValidator8" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label13" runat="server" Text="Label">Batch Management:</asp:Label>
                                    <asp:DropDownList ID="lstBatch" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False">
                                        <asp:ListItem Selected="True">X</asp:ListItem>
                                    </asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="lstplant" ID="RequiredFieldValidator9" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label15" runat="server" Text="Label">Batch Classification:</asp:Label>
                                    <asp:DropDownList ID="lstBatchClass" runat="server" Class="form-control" Width="80%" Height="40px"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="lstBatch" ID="RequiredFieldValidator10" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label17" runat="server" Text="Label">Valuation Class:</asp:Label>
                                    <asp:DropDownList ID="lstValClass" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="lstBatchClass" ID="RequiredFieldValidator11" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label19" runat="server" Text="Label">Price Control:</asp:Label>
                                    <asp:DropDownList ID="lstPrice" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False"></asp:DropDownList>
                                    <%--                                         <asp:RequiredFieldValidator ControlToValidate="txtVal" ID="RequiredFieldValidator12" runat="server"
                                        Style="color: red" ErrorMessage="RequiredFieldValidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label21" runat="server" Text="Label">MRP Type:</asp:Label>
                                    <br />
                                    <asp:DropDownList ID="lstMrp" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False"></asp:DropDownList>
                                    <%--                                    <asp:RequiredFieldValidator ControlToValidate="lstprice" ID="requiredfieldvalidator13" runat="server"
                                        Style="color: red" ErrorMessage="requiredfieldvalidator"></asp:RequiredFieldValidator>--%>
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label25" runat="server" Text="Label">Availability Check:</asp:Label>
                                    <asp:DropDownList ID="lstAval" runat="server" Class="form-control" Width="80%" Height="40px" Enabled="False"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style10">
                                    <asp:Label ID="Label29" runat="server" Text="Incoterms:"></asp:Label>
                                    <asp:ListBox ID="lstIncoterms" runat="server" Width="80%" Height="40px" SelectionMode="Multiple"></asp:ListBox>
                                    <br />
                                </td>
                                <td style="text-align: left;" class="auto-style10">           
                                    <asp:Label ID="Label14" runat="server" Text="Manufacturer:"></asp:Label>
                                    <asp:ListBox ID="lstManu" runat="server" Width="80%" Height="40px" SelectionMode="Multiple" ></asp:ListBox>
                                    
                                </td>
                                <td style="text-align: left;" class="auto-style10">
                                     <asp:Label ID="Label20" runat="server" Text="Status:"></asp:Label>
                                    <asp:DropDownList ID="lstStatus" runat="server" Width="80%" Height="40px"  >
                                        <asp:ListItem Text="Active" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="InActive" Value="0"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td style="text-align: left;" class="auto-style10">&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div style="width: 100%">
                                        <table style="width: 100%">
                                            <tr>
                                                <td colspan="10">
                                                    <asp:Label ID="Label10" runat="server" Text="EGAT Serial No. :"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 5%">
                                                    <asp:Label ID="lblCodeA" runat="server" Text="Code A:"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:DropDownList ID="ddlCodeA" runat="server" OnSelectedIndexChanged="ddlCodeA_SelectedIndexChanged" AutoPostBack="true" CssClass="form-con"></asp:DropDownList>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:Label ID="lblCodeB" runat="server" Text="Code B:"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:DropDownList ID="ddlCodeB" runat="server" OnSelectedIndexChanged="ddlCodeB_SelectedIndexChanged" AutoPostBack="true" CssClass="form-con"></asp:DropDownList>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:Label ID="lblCodeC" runat="server" Text="Code C:"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:DropDownList ID="ddlCodeC" runat="server" CssClass="form-con"></asp:DropDownList>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:Label ID="lblCodeD" runat="server" Text="Code D:"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:DropDownList ID="ddlCodeD" runat="server" CssClass="form-con"></asp:DropDownList>
                                                </td>
                                                <td style="width: 5%">
                                                    <asp:Label ID="lblCodeE" runat="server" Text="Code E:"></asp:Label>
                                                </td>
                                                <td style="width: 15%">
                                                    <asp:DropDownList ID="ddlCodeE" runat="server" CssClass="form-con"></asp:DropDownList>
                                                </td>
                                            </tr>

                                        </table>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; align-content: center" colspan="4">
                                    <asp:Label ID="Label16" runat="server" Text="Label">Remark:</asp:Label>
                                    <asp:TextBox ID="txtRemark" runat="server" Class="form-control" Width="80%" Height="80px" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <button id="btnAtt" class="btn btn-primary" onclick="toggle();return false;" />
                                    QTY Configuration
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" colspan="3">
                                    <asp:Panel ID="Panel1" runat="server" Height="300px">
                                        <table class="auto-style2">
                                            <tr>
                                                <td colspan="6">
                                                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Size="Medium" ForeColor="#0066FF" Text="Quantity Configuration"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">
                                                    <asp:Label ID="Label26" runat="server" Text="For Bid Turnkey" ForeColor="#0066FF"></asp:Label>
                                                </td>
                                                <td class="auto-style6">&nbsp;</td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td>
                                                    <asp:Label ID="Label27" runat="server" ForeColor="#0066FF" Text="For Bid Supply"></asp:Label>
                                                </td>
                                                <td class="auto-style6">&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">Buying Lot:</td>
                                                <td class="auto-style12">&nbsp;</td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td class="auto-style4">Buying Lot:</td>
                                                <td class="auto-style6">&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtTBuyLot" runat="server" Width="80%" Class="form-control" Height="40px"
                                                        placeholder="1 as Default"></asp:TextBox>
                                                </td>
                                                <td class="auto-style6">
                                                    <asp:TextBox ID="txtTBuyLotUnit" runat="server" Class="form-control" Height="40px" Width="70px"
                                                        placeholder="Unit"></asp:TextBox>
                                                </td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtSBuyLot" runat="server" Class="form-control" Height="40px" Width="80%"
                                                        placeholder="1 as Default" TextMode="Number"></asp:TextBox>
                                                </td>
                                                <td class="auto-style6">
                                                    <asp:TextBox ID="txtSBuyLotUnit" runat="server" Class="form-control" Height="40px" Width="70px"
                                                        placeholder="Unit"></asp:TextBox>
                                                </td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">Reservasion (%);</td>
                                                <td class="auto-style12">&nbsp;</td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td class="auto-style4">Reservasion (%);</td>
                                                <td class="auto-style6">&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtTReserv" runat="server" Width="80%" Class="form-control" Height="40px"
                                                        placeholder="0 as Default"></asp:TextBox>
                                                </td>
                                                <td class="auto-style6">
                                                    <asp:TextBox ID="txtTReservUnit" runat="server" Class="form-control" Height="40px" Width="70px" Visible="False"></asp:TextBox>
                                                </td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtSReserv" runat="server" Class="form-control" Height="40px" Width="80%"
                                                        placeholder="0 as Default"></asp:TextBox>
                                                </td>
                                                <td class="auto-style6">
                                                    <asp:TextBox ID="txtSReservUnit" runat="server" Class="form-control" Height="40px" Width="70px"
                                                        placeholder="1 as Default"></asp:TextBox>
                                                </td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">Remark:</td>
                                                <td class="auto-style12">&nbsp;</td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td class="auto-style4">Remark:</td>
                                                <td class="auto-style6">&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>
                                            <tr>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtTRemark" runat="server" Class="form-control" Width="80%" Height="80px" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td class="auto-style12">&nbsp;</td>
                                                <td class="auto-style8">&nbsp;</td>
                                                <td class="auto-style4">
                                                    <asp:TextBox ID="txtSRemark" runat="server" Class="form-control" Width="80%" Height="80px" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <td class="auto-style6">&nbsp;</td>
                                                <td class="auto-style3">&nbsp;</td>
                                            </tr>


                                        </table>
                                    </asp:Panel>
                                </td>
                                <td style="text-align: left;" class="auto-style11">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" class="auto-style1"><b>Related Documents</b>
                                </td>
                                <td style="text-align: left;" class="auto-style9">&nbsp;</td>
                            </tr>
                            <tr>
                                <td class="auto-style1">
                                    <asp:ImageButton ID="ImageButtonSearch" runat="server" ImageUrl="~/images/search.png" Height="20px" Width="20px" OnClick="modalSearch_click" />
                                    <%--<button onclick="document.getElementById('searchDoc').style.display='block';return false;" class="btn btn-primary">Search Document</button>--%>
                                    <asp:LinkButton ID="LbSearch" runat="server">Search Document</asp:LinkButton>
                                </td>
                                <td class="auto-style9">&nbsp;</td>
                            </tr>
                        </table>
                        <table>
                            <tr>
                                <td colspan="2" style="padding-bottom: 15px;">
                                    <asp:GridView ID="gvBEquipInfo" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvBEquipInfo_RowDataBound" >
                                        <FooterStyle BackColor="White" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Center" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <Columns>
                                           <%-- <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                                <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="No." Visible="false">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDocType" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="300px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                                                       	 <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                            <asp:GridView ID="gvSubEquip" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found."  OnRowDataBound="gvSubEquip_RowDataBound"   >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                 

                                            <asp:TemplateField HeaderText="Document No.">
                                                <ItemTemplate>
                                                    <asp:HyperLink ID="hlnk_DocNo" runat="server" Text='<%# Bind("link_doc") %>'></asp:HyperLink>
                                                    <asp:Label ID="lblDocNo" runat="server" Text='<%# Bind("doc_name") %>' Visible="false"></asp:Label>
                                                     <asp:Label ID="lblDocnodeid" runat="server" Text='<%# Bind("doc_nodeid") %>' Visible="false"></asp:Label>
                                                     <asp:Label ID="lblDocTypesub" runat="server" Text='<%# Bind("doctype_code") %>' Visible="false"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDescription" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Revision">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblRevision" runat="server" Text='<%# Bind("doc_revision_display") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="5%" HorizontalAlign="center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Update date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblEnqNo" runat="server" Text='<%# Bind("updated_datetime") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Update by">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblUpdateBy" runat="server" Text='<%# Bind("updated_by") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="กอง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblDepartment" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="แผนก">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblSection" runat="server" Text='<%# Bind("section_position_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                  <asp:ImageButton ID="img_del" runat="server" src="../../images/bin.png" Width="20px" Height="20px" OnClick="img_del_Click" /> 
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

		                      </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" Width="1300px" VerticalAlign="Top"/>
                                    </asp:TemplateField>



                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="col-md-12">

                        <!--<button onclick="document.getElementById('btnEquipUpdate_Click').style.display='block';return false;" class="w3-button w3-blue">Update Material</button>
                            &nbsp; &nbsp;
                            <button onclick="document.getElementById('btnEquipCreate_Click').style.display='block';return false;" class="w3-button w3-blue">Create Material</button>
                            &nbsp; &nbsp;
                            <button onclick="document.getElementById('btnEquipCancel_Click').style.display='block';return false;" class="w3-button w3-blue">Cancel </button>
                            &nbsp; &nbsp;-->


                        <asp:Button ID="btnCreate" runat="server" Text="Create" CssClass="btn btn-primary" OnClick="btnEquipCreate_Click" />
                        <asp:Button ID="btnUpdadte" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btnEquipUpdate_Click" />
                        <asp:Button ID="btnClear" runat="server" Text="Cancel" CssClass="btn btn-danger" OnClick="btnEquipCancel_Click" />

                    </td>
                </tr>
            </table>
        </div>
                 <asp:HiddenField ID="hidDocid" runat="server" />
                 <asp:HiddenField ID="hidEqcode" runat="server" />
    </form>
</body>
</html>
