﻿using System;
using PS_Library;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Master
{
    public partial class Manufacturer : System.Web.UI.Page
    {
        clsManufacturer objmanu = new clsManufacturer();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z597204";

                bindData();
            }
        }
        protected void bindData()
        {
            DataTable dt = objmanu.getData();
            gvManu.DataSource = dt;
            gvManu.DataBind();

        }
        protected void gvManu_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "editdata")
            {
                myEdit.Style["display"] = "block";
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvManu = (GridView)sender;
                string code = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_code")).Text;
                string name = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_name")).Text;
                string addr = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_addr")).Text;
                string remark = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_remark")).Text;
                string rowid = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_rowid")).Text;
                txtEdit_code.Text = code;
                txtEdit_name.Text = name;
                txtEdit_addr.Text = addr;
                txtEdit_remark.Text = remark;
                hidRowid.Value = rowid;
            }
            if (e.CommandName == "deletedata")
            {
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvManu = (GridView)sender;
                string code = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_code")).Text;
                string name = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_name")).Text;
                string addr = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_addr")).Text;
                string remark = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_remark")).Text;
                string rowid = ((Label)gvManu.Rows[xrowid].FindControl("gvManu_rowid")).Text;
                objmanu.deleteData(code, rowid,hidLogin.Value);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);
                bindData();
            }
        }

        protected void btnCloseCre_Click(object sender, EventArgs e)
        {
            myCreate.Style["display"] = "none";
        }

        protected void btnSaveCre_Click(object sender, EventArgs e)
        {
            if (txtCode.Text == "" && txtName.Text == "" && txtAddress.Text == "" && txtRemark.Text == "")
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Data.');", true);
            }
            else if(txtCode.Text != "")
            {
                objmanu.insertManuCode(txtCode.Text, txtName.Text, txtAddress.Text, txtRemark.Text, hidLogin.Value);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
                bindData();
                txtCode.Text = "";
                txtName.Text = ""; 
                txtAddress.Text = "";  
                txtRemark.Text = "";
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Manufacturer Code.');", true);
            }
          
        }

        protected void btnSaveEd_Click(object sender, EventArgs e)
        {
            objmanu.editData(txtEdit_code.Text, txtEdit_name.Text, txtEdit_addr.Text, txtEdit_remark.Text, hidLogin.Value, hidRowid.Value);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
            bindData();
        }
        protected void btnCloseEd_Click(object sender, EventArgs e)
        {
            myEdit.Style["display"] = "none";
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}