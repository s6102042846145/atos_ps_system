﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PriceScheduleUpload.aspx.cs" Inherits="PS_System.form.PriceScheduleUpload" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Contract Management</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .auto-style17 {
            width: 550px;
        }
        .auto-style18 {
            height: 29px;
        }
        


   

   
        .auto-style19 {
            width: 100%;
            height: 400px;
            overflow: scroll;
        }
        


   

   
        .auto-style20 {
            width: 39%;
        }
        .auto-style21 {
            height: 29px;
            width: 39%;
        }
        


   

   
        .auto-style26 {
            height: 24px;
        }
        .auto-style27 {
            width: 39%;
            height: 24px;
        }
        


   

   
        .auto-style29 {
            width: 163px;
        }
        .auto-style30 {
            width: 10%;
            height: 28px;
        }
        .auto-style32 {
            width: 40%;
            height: 28px;
        }
        .auto-style33 {
            width: 8%;
        }
        .auto-style34 {
            width: 8%;
            height: 28px;
        }
        .auto-style35 {
            width: 12%;
        }
        .auto-style36 {
            width: 12%;
            height: 28px;
        }
        


   

   
        </style>
        <script>



            $(function ()
            {


                $(document).ready(function () {
                    $('#mtJob2').multiselect({
                        includeSelectAllOption: true,
                        enableFiltering: true,
                        enableCaseInsensitiveFiltering: true,
                        numberDisplayed: 3,
                        maxHeight: 300
                    });
               

                   
                    

                });
            });

            function ShowHideDiv(chkWonBid) {
                var dvWonbid = document.getElementById("dvWonbid");

                var btnAddVendor = document.getElementById("btnAddVendor");
                var btnCreate = document.getElementById("btnCreate");
               
                //dvWonbid.style.display = chkWonBid.checked ? "block" : "none";
                if (chkWonBid.checked) {
                    dvWonbid.style.display = "block";
                    btnAddVendor.style.backgroundColor = 'Grey';
                    btnCreate.style.backgroundColor = 'Grey';
                } else {
                    dvWonbid.style.display = "none";
                    btnAddVendor.style.backgroundColor = '#428BCA';
                    btnCreate.style.backgroundColor = '#71C520';
                }
            }



        </script>
</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Price Schedule Upload"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;" >
            <table style="width: 100%;">
                
                <tr>
                    <td style="width: 10%;">
                        <asp:Label ID="Label1" runat="server" Text="Bid No."></asp:Label>
                    </td>
                    <td style="width: 30%;">
                                 <select id="mtContackNo" runat="server" name="D4" class="auto-style17">
                                 </select></td>
                    <td  style="width: 8%;">
                        <asp:Label ID="Label16" runat="server" Text="Revision"></asp:Label>
                    </td>
                    <td style="width:25%;">
                        <asp:TextBox ID="txtStatus0" runat="server" Width="100px" ToolTip="Not Start / Complete"></asp:TextBox>
                        </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label52" runat="server" Text="Bid Description"></asp:Label>
                    </td>
                    <td  colspan="3">
                        <asp:TextBox ID="txtStatus" runat="server" Width="100%" ToolTip="Not Start / Complete"></asp:TextBox>
                    </td>
                  
                </tr>

                 <tr>
                    <td class="auto-style18" >
                        <asp:Label ID="Label2" runat="server" Text="Vendor Code"></asp:Label>
                    </td>
                    <td class="auto-style18" >
                                 <select id="Select1" runat="server" name="D4" style="width: 550px">
                                 </select></td>
                    <td class="auto-style18"  >
                        <asp:Label ID="Label3" runat="server" Text="Bidder No."></asp:Label>
                    </td>
                    <td class="auto-style21" >
                        <asp:TextBox ID="txtStatus1" runat="server" Width="200px" ToolTip="Not Start / Complete"></asp:TextBox>
                        </td>
                </tr>

                 <tr>
                    <td class="auto-style26" >
                        <asp:Label ID="Label4" runat="server" Text="Bidder Type"></asp:Label>
                    </td>
                    <td class="auto-style26">
                                 <select id="Select2" runat="server" name="D4" style="width: 550px">
                                 </select></td>
                    <td class="auto-style26"  >
                        &nbsp;&nbsp;&nbsp;
                                                    
                     </td>
                    <td class="auto-style27" >
                        </td>
                </tr>
              
           
                            </table>
        </div>
     
        <div style="padding: 0px 10px 0px 10px;" >
            <table style="width: 100%;">
               
                 <tr>
                     <td style="width: 10%;">
                        <asp:Label ID="Label5" runat="server" Text="Consortium Name"></asp:Label>
                    </td>
                    <td style="width:90%;" colspan="3">
                        <asp:TextBox ID="txtStatus2" runat="server" Width="550px" ToolTip="Not Start / Complete"></asp:TextBox>
                        </td>
                 
                </tr>
                <tr>
                     <td style="width: 10%;">
                        <asp:Label ID="Label6" runat="server" Text="Consortium File"></asp:Label>
                    </td>
                    <td style="width: 40%;">
                             <asp:FileUpload ID="fileUploadDoc" runat="server" AllowMultiple="true" Width="550px" />
                        </td>
                    <td style="width: 10%;">
                              
                     &nbsp;&nbsp;
                            <asp:ImageButton ID="imgUploadFile" runat="server" ImageUrl="~/images/up.png" Height="26px" Width="36px"   />

                    
                        &nbsp;&nbsp;

                    
                        <asp:Label ID="Label53" runat="server" Text="Revision"></asp:Label>

                    
                    </td>
                    <td style="width: 40%;">
                        <asp:TextBox ID="txtStatus3" runat="server" Width="100px" ToolTip="Not Start / Complete"></asp:TextBox>
                        </td>
                </tr>
            </table>
        </div>
          <div style="padding: 10px 10px 10px 10px;" >
            <table class="nav-justified">
                    <tr>
                    <td >
                        &nbsp;</td>
                    <td>
                                 &nbsp;</td>
                    <td  >
                        &nbsp;&nbsp;&nbsp;
                                                    
                     </td>
                    <td class="auto-style20" >
                      <asp:Button ID="btnSearch0" runat="server" class="btn btn-primary"  Text="Add" />
                              
                      </td>
                </tr>
                </table>

        </div>
         <div style="padding: 10px 10px 10px 10px; " >
           
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="4pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                     <td class="auto-style33">
                        <asp:Label ID="Label7" runat="server" Text="Won bid"></asp:Label>
                    </td>
                    <td class="auto-style35">
                                 <asp:CheckBox ID="chkWonBid" runat="server" onclick="ShowHideDiv(this)"/>
                     </td>
                    <td  style="width: 8%;">
                        &nbsp;</td>
                    <td style="width:25%;">
                        &nbsp;</td>
                </tr>
                 <tr>
                     <td class="auto-style33">
                         &nbsp;</td>
                     <td  colspan="3">
                        <div id="dvWonbid" style="display: none">
                            <asp:Label ID="lbWonbid" runat="server" Text="Please add final revision of documents if changed "></asp:Label>
                             </div>
                       
                    </td>
                </tr>
                <tr>
                     <td  colspan="4">
                        <asp:Button ID="btnAddVendor"  class="btn btn-primary" runat="server" Text="Add Vendor" Width="110px" />


                         &nbsp;<asp:Button ID="btnCreate" runat="server" class="btn btn-primary"  Text="Create New" BackColor="#71C520" />
                              
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div runat="server" id="dvGvstock" visible="false" class="auto-style19">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                     <asp:GridView ID="gvDocUpload" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                    <Columns>
                        
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <asp:Label ID="lbPlant" runat="server" Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                <%--Text='<%# Bind("job_no") %>'--%>
                            </ItemTemplate>
                            <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Vendor No.">
                            <ItemTemplate>
                                <asp:DropDownList ID="ddlEqu" runat="server">
                                     <asp:ListItem Value="0">ทั้งหมด</asp:ListItem>
                                     <asp:ListItem Value="1">Main Equipment</asp:ListItem>
                                     <asp:ListItem Value="2">Auxiliary</asp:ListItem>
                                 </asp:DropDownList>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Vendor No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbVendorNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Schedule No.">
                            <ItemTemplate>
                                <asp:Label ID="lbDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Schedule No." Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lbVendorNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Upload File">
                            <ItemTemplate>
                             <asp:TextBox ID="txtReserveQty" runat="server" Text="" AutoPostBack="true"></asp:TextBox>
                             <asp:FileUpload ID="fileUploadDoc" runat="server" AllowMultiple="true" Width="550px" />
                              <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/images/up.png" Height="26px" Width="36px"  />

                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Revision">
                            <ItemTemplate>
                                <asp:Label ID="lbStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="File Status" >
                            <ItemTemplate >
                                <asp:Label ID="lbStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" BackColor="#71C520"  ForeColor="#ffffff"/>
                        </asp:TemplateField>
                     
                       
              
                    </Columns>
                </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">

                    </td>
                </tr>
                <tr>
                     <td class="auto-style34">
                        <asp:Label ID="Label9" runat="server" Text="Other Documents"></asp:Label>
                    </td>
                    <td class="auto-style36">
                                 <select id="Select3" runat="server" name="D5" class="auto-style29">
                                 </select></td>
                    <td class="auto-style30">
                              
                             <asp:FileUpload ID="fileUploadDoc0" runat="server" AllowMultiple="true" Width="462px" />

                    
                    </td>
                    <td class="auto-style32">
                            <asp:ImageButton ID="ImageButton2" runat="server" ImageUrl="~/images/up.png" Height="26px" Width="36px"   />

                    
                        &nbsp;
                      <asp:Button ID="btnSearch1" runat="server" class="btn btn-primary"  Text="Add More File" />
                              
                    
                        </td>
                </tr>
              
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        <asp:Button ID="btnSave"  class="btn btn-primary" runat="server" Text="Save" Width="110px" />


                        &nbsp;<asp:Button  ID="btnCancel" runat="server" Text="Cancel" CssClass="btn btn-danger" Width="110px"/>
                        
                        &nbsp;</td>
                </tr>
            </table>
            
        </div>
    </form>
</body>
</html>

