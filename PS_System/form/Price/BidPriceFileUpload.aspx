﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidPriceFileUpload.aspx.cs" Inherits="PS_System.form.Price.BidPriceFileUpload" %>

<!DOCTYPE html>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Add Vendor</title>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <link href="../../Content/w3.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <script type="text/javascript">
        $(function () {
            $(document).ready(function () {
                $('#ddlBidno').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                $('#btnAdd').on('click', function () {
                    $('.popup').hide();
                    $(this).find('.popup').show();
                });

                $('.popup').on('click', function (e) {
                    e.stopPropagation();
                    $(this).hide();
                });
            });

        });

        function OpenCreateVendor(){
            window.open('VendorAdd.aspx');
        }

        function ShowPopUp() {
            document.getElementById('divAddvendor').style.display = 'block';
            document.getElementById('txtvendorCode').value = '';
            document.getElementById('txtvendorName').value = '';
            document.getElementById('ddlType').selectedIndex = 0;
            //document.getElementById('txtContractNo').value = '';
            //document.getElementById('txtDesc').value = '';
        }

    </script>

    <style type="text/css">
        
        .auto-style4 {
            width: 223px;
        }

        .auto-style5 {
            width: 20%;
        }

        .popup {
            /*display: block;*/
            position: absolute;
            left: 50%;
            top: 50%;
            width: 500px;
            margin-left: -250px;
            height: 200px;
            margin-top: -200px;
            padding: 25px 10px 30px 10px;
            color: #fff;
            background: rgba(47,47,47,0.8);
            z-index: 1000;
            overflow: hidden;
            cursor: pointer;
        }

        .auto-style7 {
            width: 282px;
        }
        .auto-style8 {
            display: block;
            padding: 6px 12px;
            font-size: 14px;
            line-height: 1.428571429;
            color: #555555;
            vertical-align: middle;
            background-color: #ffffff;
            background-image: none;
            border: 1px solid #cccccc;
            border-radius: 4px;
            -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            -webkit-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
        }
        .auto-style9 {
            width: 399px;
        }

        </style>

</head>
<body>
    <form id="form1" runat="server" style="padding:50px">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Price Schedule Upload"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

        <div>
            <table class="nav-justified">
                <tr>
                    <td colspan="3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label1" runat="server" Text="Bid No :" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        &nbsp;<asp:Label ID="lblBidno" runat="server" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                    </td>
                    <td class="auto-style7">&nbsp;<asp:Label ID="Label59" runat="server" Text="Bidder :" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddlBidder" runat="server" CssClass="auto-style8" Font-Names="Tahoma" Font-Size="9pt" Width="80%">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style9">
                        <asp:Label ID="lblMode" runat="server" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td rowspan="2">
                        <asp:Label ID="Label43" runat="server" Text="Description :" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        <asp:Label ID="lblDesc" runat="server" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                    </td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style9">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style7">
                        <asp:Label ID="Label45" runat="server" Text="Ref Schedule :" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        <asp:DropDownList ID="ddlScRef" runat="server" CssClass="auto-style8" AutoPostBack="true" OnSelectedIndexChanged="ddlScRef_SelectedIndexChanged" Font-Names="Tahoma" Font-Size="9pt" Width="80%"></asp:DropDownList>
                    </td>
                    <td class="auto-style9">&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="gvvendor" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999"
                            OnRowCommand="gvvendor_RowCommand" OnRowDeleting="gvvendor_RowDeleting"
                            BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="75%" Font-Names="Tahoma" Font-Size="9pt">
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                            <Columns>
                                <asp:TemplateField HeaderText="rowid" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblrowid" runat="server" Text='<%# Bind("row_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblItemno" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="vendor Code">
                                    <ItemTemplate>

                                        <asp:Label ID="lblvendorCode" runat="server" Text='<%# Bind("vendor_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="vendor Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblvendorName" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lblType" runat="server" Text='<%# Bind("vendor_type") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="...">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbRemove" CommandName="Delete" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Remove</asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#000065" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr id="tradd" runat="server" style="vertical-align: bottom;">
                    <td class="auto-style4">
                        <asp:Label ID="Label60" runat="server" Text="Select Vendor" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        <asp:DropDownList ID="ddlVendor" runat="server" CssClass="auto-style8" Font-Names="Tahoma" Font-Size="9pt" Width="80%">
                        </asp:DropDownList>

                        </td>
                    <td class="auto-style7" >
                        <asp:Button ID="btnAddToGrid" runat="server" class="btn btn-primary" Text="Add To Schedule" OnClick="btnAddToGrid_Click" Width="120px" Font-Names="Tahoma" Font-Size="9pt" />

                    </td>
                    <td class="auto-style9" >
                        <asp:Button ID="btnAddNew" runat="server" class="btn btn-primary" Text="Add New Vendor" OnClientClick="OpenCreateVendor()"  Width="120px" Font-Names="Tahoma" Font-Size="9pt" />

                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style9">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr style="vertical-align: bottom;">
                    <td class="auto-style4">
                        <asp:Label ID="Label56" runat="server" Text="From vendor" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        &nbsp;<asp:DropDownList ID="ddlFromvendor" class="form-control" runat="server" Width="80%" Font-Names="Tahoma" Font-Size="9pt">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style7">
                        <asp:Label ID="Label57" runat="server" Text="Doc Type" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        <asp:DropDownList ID="ddlDocType" class="form-control" runat="server" Width="80%" Font-Names="Tahoma" Font-Size="9pt">
                        </asp:DropDownList>
                    </td>
                    <td class="auto-style9">
                        <asp:Label ID="Label58" runat="server" Text="File Upload" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                        <asp:FileUpload ID="fileUploadFromvendor" class="form-control" runat="server" Width="80%" Height="40px" />
                    </td>
                    <td style="text: ; vertical-align: bottom;">
                        <asp:Button ID="btnUpload" class="btn-success" runat="server" Text="Upload" Height="40px" Width="100px" OnClick="btnUpload_Click" Font-Names="Tahoma" Font-Size="9pt" />
                    </td>
                </tr>
                <tr>
                    <td class="auto-style4">&nbsp;</td>
                    <td class="auto-style7">&nbsp;</td>
                    <td class="auto-style9">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4">
                        <asp:GridView ID="gvFile" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999" BorderStyle="None"
                            OnRowCommand="gvFile_RowCommand" OnRowDeleting="gvFile_RowDeleting"
                            BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%" Font-Names="Tahoma" Font-Size="9pt">
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#000065" />
                            <Columns>
                                <asp:TemplateField HeaderText="rowid" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblrowid" runat="server" Text='<%# Bind("row_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="docnodeid" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblnodeid" runat="server" Text='<%# Bind("doc_node_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="parentnode" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblparentnode" runat="server" Text='<%# Bind("parent_node_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Bid No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBidno" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lbldesc" runat="server" Text='<%# Bind("bid_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblScname" runat="server" Text='<%# hidscname.Value %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bidder ID">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBidderId" runat="server" Text='<%# Bind("bidder_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Vendor">
                                    <ItemTemplate>
                                        <asp:Label ID="lblvendor" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DocType">
                                    <ItemTemplate>
                                        <%--<asp:DropDownList ID="ddlgvDocType" class="form-control" runat="server" Width="200px"></asp:DropDownList>--%>
                                        <asp:Label ID="lblDocTypeName" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DocTypeCode" Visible="false">
                                    <ItemTemplate>
                                        <%--<asp:DropDownList ID="ddlgvDocType" class="form-control" runat="server" Width="200px"></asp:DropDownList>--%>
                                        <asp:Label ID="lblDocType" runat="server" Text='<%# Bind("doc_type") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DocName">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDocName" runat="server" Text='<%# Bind("doc_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Version">
                                    <ItemTemplate>
                                        <asp:Label ID="lblVersion" runat="server" Text='<%# Bind("doc_version") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text='<%# Bind("status_bid") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="...">
                                    <ItemTemplate>
                                        <%--<asp:LinkButton ID="lbRemove" CommandName="Delete" CommandArgument='<%# Container.DataItemIndex %>' runat="server">Remove</asp:LinkButton>--%>
                                        <asp:Button ID="lbRemove" CommandName="Delete" CommandArgument='<%# Container.DataItemIndex %>' runat="server" Text="Remove" Width="100px" CssClass="btn-danger" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action" Visible="false">
                                    <ItemTemplate>
                                        <asp:Button ID="btnAddFinalFile" CommandName="AddFinal" CommandArgument='<%# Container.DataItemIndex %>' runat="server" Text="AddFinalFile" Width="100px" CssClass="btn-success" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </td>
                </tr>
            </table>
        </div>
        <div id="divAddvendor" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('divAddvendor').style.display='none'"
                        class="w3-button w3-display-topright">&times;</span>
                    <div style="padding: 10px 10px 10px 10px;">
                        <table style="width: 100%;">
                            <tr>
                                <td class="auto-style5">
                                    <asp:Label ID="Label7" runat="server" Text="Vendor Code"></asp:Label>
                                </td>
                                <td class="auto-style39">
                                    <asp:TextBox ID="txtvendorCode" runat="server" Width="300px"></asp:TextBox>
                                </td>
                                <td class="auto-style40">
                                    <asp:Label ID="Label53" runat="server" Text="Bidder No"></asp:Label>
                                </td>
                                <td style="width: 40%;">
                                    <asp:TextBox ID="txtBidderNo" runat="server" Width="100px" ToolTip="Not Start / Complete" Enabled="False"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">
                                    <asp:Label ID="Label55" runat="server" Text="Vendor Name"></asp:Label>
                                </td>
                                <td colspan="3" class="auto-style41">
                                    <asp:TextBox ID="txtvendorName" runat="server" Width="300px"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">
                                    <asp:Label ID="Label54" runat="server" Text="Bidder Type"></asp:Label>
                                </td>
                                <td colspan="3" class="auto-style41">
                                    <div id="dvWonbid" style="display: none">
                                        <asp:Label ID="lbWonbid" runat="server" Text="Please add final revision of documents if changed "></asp:Label>
                                    </div>

                                    <asp:DropDownList ID="ddlType" runat="server">
                                        <asp:ListItem>Select vendor Type</asp:ListItem>
                                        <asp:ListItem>Single Entity</asp:ListItem>
                                        <asp:ListItem>Joint Venture</asp:ListItem>
                                        <asp:ListItem>Consortium</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <%--<asp:DropDownList ID="ddlgvDocType" class="form-control" runat="server" Width="200px"></asp:DropDownList>--%>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td style="width: 40%;">
                                    <asp:Button ID="btnSaveNew" runat="server" class="btn btn-primary" Text="Save" OnClick="btnSaveNew_Click" />

                                </td>
                                <td style="width: 10%;"></td>
                                <td style="width: 40%;">&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <asp:HiddenField ID="hidbidderid" runat="server" />
        <asp:HiddenField ID="hidlogin" runat="server" />
        <asp:HiddenField ID="hidscname" runat="server" />
        <asp:HiddenField ID="hidzmode" runat="server" />
        <asp:HiddenField ID="hidbidid" runat="server" />
    </form>
</body>
</html>
