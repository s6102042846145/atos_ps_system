﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Price
{
    public partial class BasePriceDownload : System.Web.UI.Page
    {
        private string strEmpIdLogin = string.Empty;
        EmpInfoClass empInfo = new EmpInfoClass();
        clsBasePrice objBasePrice = new clsBasePrice();

        protected void Page_Load(object sender, EventArgs e)
        {
            // get login from querystring
            if (Request.QueryString["zuserlogin"] != null)
                strEmpIdLogin = Request.QueryString["zuserlogin"];
            else
                strEmpIdLogin = "z593503";// for test only

            // get department
            BindingPosition(strEmpIdLogin);
            if (!IsPostBack)
            {
                BindingEquipGroupByDept(hdnDepartment.Value);
                //BindingEquipGroupByDept("กวอ-ส.");
                BindingBidNo();
                divExport.Visible = false;
            }
            // load dropdown list equipment group filter by department
            // hide gv, export/upload button
        }

        private void BindingBidNo()
        {
            ddlBidNo.Items.Clear();
            DataTable dtBidNo = objBasePrice.GetBidNo(hdnDepartment.Value);
            foreach (DataRow dr in dtBidNo.Rows)
            {
                if(ddlBidNo.Items.Count == 0)
                {
                    ddlBidNo.Items.Add(new ListItem("Please Select",""));
                }
                ddlBidNo.Items.Add(new ListItem(dr["bid_no"].ToString()));
            }
        }

        private void BindingSchedule()
        {
            ddlSchedule.Items.Clear();
            if (!String.IsNullOrEmpty(ddlBidNo.SelectedValue)){
                DataTable dtScheDule = objBasePrice.GetSchedulByBidNo(ddlBidNo.SelectedValue, hdnDepartment.Value);
                foreach (DataRow dr in dtScheDule.Rows)
                {
                    if (ddlSchedule.Items.Count == 0)
                    {
                        ddlSchedule.Items.Add(new ListItem("Please Select", ""));
                    }
                    ddlSchedule.Items.Add(new ListItem(dr["schedule_name"].ToString()));
                }
            }
        }

        private void BindingPosition(string strempid)
        {
            Employee emp = new Employee();
            emp = empInfo.getInFoByEmpID(strempid);
            hdnDepartment.Value = emp.ORGSNAME4;
            // hdnDepartment.Value = "กวอ-ส.";// test
        }

        private void BindingEquipGroupByDept(string strDeptShortName)
        {
            DataTable dt = objBasePrice.GetEquipGrpByDept(strDeptShortName);
            if (dt.Rows.Count < 1)
            {
                hdnIsNoGrp.Value = "nogroup";
            }
            lstEquipGroup.DataSource = dt;
            lstEquipGroup.DataValueField = "CODE";
            lstEquipGroup.DataTextField = "NAME";
            lstEquipGroup.DataBind();
        }

        protected void gvBasePrice_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBasePrice.PageIndex = e.NewPageIndex;
            this.BindingGv();
        }

        private void BindingGv()
        {
            string strEquipGrp = string.Empty;
            string strEquipCode = string.Empty;
            DataTable dt = new DataTable();
            if (!String.IsNullOrEmpty(ddlBidNo.SelectedValue))
            {
                string strBidNo = ddlBidNo.SelectedValue;
                string strSchdule = ddlSchedule.SelectedValue;
                DataTable dtDistinctEquip = objBasePrice.GetEquipByBidSchedule(strBidNo, strSchdule);
                foreach (DataRow dr in dtDistinctEquip.Rows)
                {
                    strEquipCode += strEquipCode == string.Empty ? "'" + dr["equip_code"].ToString() + "'" : ",'" + dr["equip_code"].ToString() + "'";
                }
                dt = objBasePrice.GetAvgPriceByEquipCodes(strEquipCode,hdnDepartment.Value);
            }
            else
            { 
                foreach (ListItem item in lstEquipGroup.Items)
                {
                    if (item.Selected) strEquipGrp += strEquipGrp == string.Empty ? "'" + item.Value + "'" : ",'" + item.Value + "'";
                }
                dt = objBasePrice.GetAvgPriceByEquipGrps(strEquipGrp, hdnDepartment.Value);//new DataTable();// get 
            }
            ViewState["dt_avg_price"] = dt;
            gvBasePrice.DataSource = dt;
            gvBasePrice.DataBind();
            if (dt.Rows.Count > 0)
            {
                divExport.Visible = true;
            }
            else
            {
                divExport.Visible = false;
            }
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //search avgprice by selected equipgroup
            bool isValidated = false;
            if (hdnIsNoGrp.Value == "nogroup")
            {
                isValidated = true;  
            }
            else
            {
                foreach (ListItem item in lstEquipGroup.Items)
                { 
                    if (item.Selected)
                    {
                        isValidated = true;
                        break;
                    }
                }
                if (isValidated == false)
                {
                    if (!String.IsNullOrEmpty(ddlBidNo.SelectedValue))
                    {
                        isValidated = true;
                    }
                }
            }

            if (isValidated)
            {
                BindingGv();
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please select equipment group or bid no.');", true);
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string strFileName = "Export_base_price_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // export grid view to excel
            DataTable dtResult = (DataTable)ViewState["dt_avg_price"];

            byte[] byte1 = objBasePrice.GenExcel(dtResult);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "binary/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
            Response.BinaryWrite(byte1);
            Response.Flush();
            Response.End();
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            // read excel and insert to table
            if (fileUploadBasePrice.HasFile)
            {
                byte[] strFileName = fileUploadBasePrice.FileBytes;
                MemoryStream ms = new MemoryStream(strFileName);
                objBasePrice.ReadExcel(ms, strEmpIdLogin);

                BindingEquipGroupByDept(hdnDepartment.Value);
                divExport.Visible = false;
                gvBasePrice.DataSource = null;
                gvBasePrice.DataBind();
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Uploaded.');", true);

            }
        }

        protected void ddlBidNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            BindingSchedule();
        }
    }
}