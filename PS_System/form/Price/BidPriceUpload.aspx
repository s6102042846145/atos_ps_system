﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidPriceUpload.aspx.cs" Inherits="PS_System.form.Price.BidPriceUpload" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Price Upload</title>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <script type="text/javascript">
        $(function () {
            $(document).ready(function () {
                $('#ddlBidno').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300,
                });
            });

        });
    </script>
    <style type="text/css">
        .auto-style1 {
            width: 6%;
        }

        .auto-style3 {
            width: 30%;
        }
    </style>

</head>
<body>
    <form id="form1" runat="server" enctype="multipart/form-data">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Price Schedule Upload (Main)"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table class="nav-justified">
                <tr>
                    <td colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label1" runat="server" Text="Search Bid" Font-Names="Tahoma" Font-Size="9pt"></asp:Label>
                    </td>
                    <td class="auto-style3">
                        <asp:DropDownList ID="ddlBidno" runat="server" Font-Names="Tahoma" Font-Size="9pt">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" class="btn btn-primary" Text="Search" OnClick="btnSearch_Click" Font-Names="Tahoma" Font-Size="9pt" />

                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3">
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#999999"
                            BorderStyle="None" BorderWidth="1px" CellPadding="3" GridLines="Vertical" Width="100%" AllowPaging="True"
                            OnPageIndexChanging="GridView1_PageIndexChanging" OnRowDataBound="GridView1_RowDataBound"
                            OnRowCommand="GridView1_RowCommand" Font-Names="Tahoma" Font-Size="9pt">
                            <AlternatingRowStyle BackColor="#DCDCDC" />
                            <Columns>
                                <asp:TemplateField HeaderText="bid_id" Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBidId" runat="server" Text='<%# Bind("bid_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Bid No">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBidno" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="130px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("bid_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="250px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" Text="Selling"></asp:Label>
                                        <%--<asp:Label ID="lblStatus" runat="server" Text='<%# Bind("bidder_status") %>'></asp:Label>--%>
                                    </ItemTemplate>
                                    <ItemStyle Width="130px" HorizontalAlign="left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Bidder Id">
                                    <ItemTemplate>
                                        <asp:BulletedList ID="bllBidderId" runat="server"></asp:BulletedList>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Bidder Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBidderName" runat="server" Text='<%# Bind("bidder_name") %>'></asp:Label>
                                        <asp:BulletedList ID="bllBidderName" runat="server"></asp:BulletedList>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="Contractor">
                                    <ItemTemplate>
                                        <asp:Label ID="lblContractor" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                        <asp:BulletedList ID="bllVenderName" runat="server"></asp:BulletedList>

                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="...">
                                    <ItemTemplate>
                                        <asp:Button ID="btnUpload" CssClass="btn-success" CommandName="Upload" CommandArgument='<%# Container.DataItemIndex %>' runat="server" Text="Upload" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EEEEEE" ForeColor="Black" Height="25px" />
                            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#0000A9" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#000065" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;</td>
                    <td class="auto-style3">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hidlogin" runat="server" />

    </form>
</body>
</html>
