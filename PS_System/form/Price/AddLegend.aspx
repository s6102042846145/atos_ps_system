﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddLegend.aspx.cs" Inherits="PS_System.form.Price.AddLegend" EnableEventValidation="false" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Legend</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
    </style>
    <script type="text/javascript">
        function closeViewEquip() {
            document.getElementById("myModal_Equip").style.display = "none";
            return true;
        }
    </script>

</head>
<body>
    <form id="form" runat="server">
        <div id="myModal_Equip" class="modal" runat="server">
            <!-- Modal content -->
            <div class="modal-content">
                <table style="width: 100%;">
                    <tr>
                        <td style="text-align: right;">
                            <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Equipment Code: "></asp:Label>
                        </td>
                        <td style="text-align: left;">
                            <asp:Label ID="lblEquipCode" runat="server" Font-Names="tahoma" Font-Size="9pt" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Label ID="Label5" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Unit: "></asp:Label>
                        </td>
                        <td style="text-align: left; width: 15%;">
                            <asp:Label ID="lblEquipUnit" runat="server" Font-Names="tahoma" Font-Size="9pt" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: right;">
                            <asp:Label ID="Label4" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Equipment Description: "></asp:Label>
                        </td>
                        <td style="text-align: left; width: 15%;">
                            <asp:Label ID="lblEquipDesc" runat="server" Font-Names="tahoma" Font-Size="9pt" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnClose_Modal" ClientIDMode="Static" runat="server" CssClass="btn btn-primary" Text="Close" OnClientClick="if(closeViewEquip()) return false;" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Add Legend"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="show_data" style="padding: 10px 10px 10px 10px;" runat="server">
            <table style="width: 100%;">
                <tr style="padding-left: 5px;">
                    <td>
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 10%">
                                    <asp:Label ID="Label3" runat="server" Text="Job No.:"></asp:Label>
                                </td>
                                <td style="width: 20%"><%--style="width: 50%"--%>
                                    <asp:TextBox ID="txtJobNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;<asp:Label ID="Label6" runat="server" Text="Rev.:"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtJobRev" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="100px"></asp:TextBox>

                                </td>
                                <td style="width: 9%">
                                    <asp:Label ID="Label7" runat="server" Text="Substation / Line:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSubLine" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="95%"></asp:TextBox>

                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Job Description:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtJobDesc" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Work Assignment:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtJobContent" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="4" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label9" runat="server" Text="Remark:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; color: #006699;"><b>Legend</b></td>
                </tr>
                <tr>
                    <td style="text-align:center;vertical-align:middle;width:100%">
                        <asp:GridView ID="gvLegend" runat="server" AutoGenerateColumns="False" Width="600px"
                            Visible="true" ClientIDMode="Static" OnRowCommand="gvLegend_RowCommand" OnRowDataBound="gvLegend_RowDataBound"
                            class="table table-striped table-bordered table-sm">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Legend">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvLegend_txtLegend" runat="server" Text='<%# Bind("legend") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="QTY">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvLegend_txtQTY" runat="server" Text='<%# Bind("legend_qty") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View Legend">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvLegend_btnView" ClientIDMode="Static" runat="server" ImageUrl="../../Images/view.png" CommandName="viewdata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="View Legend" Width="30" ImageAlign="NotSet" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvLegend_btnAdd" ClientIDMode="Static" runat="server" ImageUrl="../../Images/add.png" CommandName="adddata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                        <asp:ImageButton ID="gvLegend_btnRemove" ClientIDMode="Static" runat="server" ImageUrl="../../Images/remove.png" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CCFF" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; vertical-align: middle; width: 300px;">
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnClose" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Close" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidIsFirstRun" runat="server" />
        <asp:HiddenField ID="hidATOS" runat="server" />
        <asp:HiddenField ID="hidJobNo" runat="server" />
        <asp:HiddenField ID="hidJobRevision" runat="server" />
    </form>
</body>
</html>
