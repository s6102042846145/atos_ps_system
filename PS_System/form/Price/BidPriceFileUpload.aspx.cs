﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Price
{
    public partial class BidPriceFileUpload : System.Web.UI.Page
    {
        clsPriceUpload2 clsPriceUpload = new clsPriceUpload2();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Session.Count > 0)
                {
                    GetSession();
                }
                else
                {
                    GetQueryString();
                }
                try
                {
                    hidzmode.Value = Request.QueryString["zmode"].ToString();
                }
                catch
                {
                    hidzmode.Value = string.Empty;
                }

                //hidbidid.Value = "1480";
                if (!string.IsNullOrEmpty(hidbidid.Value))
                {
                    //hidzmode.Value = "Winner";
                    GetBidData();
                    LoadData();
                    BindData();

                    if (hidzmode.Value == "Winner")
                    {
                        lblMode.Text = "อัพโหลดเอกสารไฟนอล";
                        tradd.Visible = false;
                    }
                    else
                    {
                        lblMode.Text = "ยื่นซอง";

                    }
                }
            }
        }
        private void GetSession()
        {
            try
            {
                hidbidid.Value = Session["bidid"].ToString();
            }
            catch
            {
                hidbidid.Value = string.Empty;
            }
            try
            {
                hidbidderid.Value = Session["bidderid"].ToString();
            }
            catch
            {
                hidbidderid.Value = string.Empty;
            }
            try
            {
                hidlogin.Value = Session["login"].ToString();
            }
            catch
            {
                hidlogin.Value = string.Empty;
            }
            try
            {
                hidscname.Value = Session["scname"].ToString();
            }
            catch
            {
                hidscname.Value = string.Empty;
            }

        }
        private void GetQueryString()
        {
            try
            {
                hidlogin.Value = Request.QueryString["login"].ToString();
            }
            catch
            {
                hidlogin.Value = string.Empty;
            }

            try
            {
                hidbidid.Value = Request.QueryString["bidid"].ToString();
            }
            catch
            {
                hidbidid.Value = string.Empty;
            }
            try
            {
                hidscname.Value = Request.QueryString["scname"].ToString();
            }
            catch
            {
                hidscname.Value = string.Empty;
            }
            try
            {
                hidzmode.Value = Request.QueryString["zmode"].ToString();
            }
            catch
            {
                hidzmode.Value = string.Empty;
            }
        }
        private void BindData()
        {
            DataTable dt = clsPriceUpload.GetDocType();
            DataRow dr = dt.NewRow();
            dr["doctype_name"] = "Select Document Type";
            dt.Rows.InsertAt(dr, 0);
            dt.AcceptChanges();
            ddlDocType.DataSource = dt;
            ddlDocType.DataValueField = "doctype_code";
            ddlDocType.DataTextField = "doctype_name";
            ddlDocType.DataBind();

            DataTable dtsc = clsPriceUpload.GetBidSchedule(int.Parse(hidbidid.Value));
            DataRow drsc = dtsc.NewRow();
            drsc["schedule_name"] = "Select Schedule";
            dtsc.Rows.InsertAt(drsc, 0);
            dtsc.AcceptChanges();
            ddlScRef.DataSource = dtsc;
            ddlScRef.DataValueField = "schedule_name";
            ddlScRef.DataTextField = "schedule_name";
            ddlScRef.DataBind();

            DataTable dtb = clsPriceUpload.GetBidder(lblBidno.Text);
            ddlBidder.DataSource = dtb;
            ddlBidder.DataValueField = "bidder_id";
            ddlBidder.DataTextField = "vendor_name";
            ddlBidder.DataBind();

            DataTable dtv = clsPriceUpload.GetVendor();
            DataRow drv = dtv.NewRow();
            drv["name"] = "Select Vendor";
            dtv.Rows.InsertAt(drv, 0);
            dtv.AcceptChanges();
            ddlVendor.DataSource = dtv;
            ddlVendor.DataValueField = "code";
            ddlVendor.DataTextField = "name";
            ddlVendor.DataBind();

        }
        private void LoadData()
        {
            VenderModel model = new VenderModel();
            model.bid_no = lblBidno.Text;

            if (ddlScRef.Items.Count > 0)
            {
                model.ref_schedule_no = ddlScRef.SelectedItem.Text;
                hidscname.Value = ddlScRef.SelectedItem.Text;
            }
            else
            {
                model.ref_schedule_no = hidscname.Value;
            }

            DataTable dtv = clsPriceUpload.GetBidVendor(model);
            if (dtv.Rows.Count > 0)
            {
                gvvendor.DataSource = dtv;
                gvvendor.DataBind();
            }
            else
            {
                DataRow drv = dtv.NewRow();
                if (ddlBidder.Items.Count > 0)
                {
                    drv["vendor_code"] = ddlBidder.SelectedValue;
                    drv["vendor_name"] = ddlBidder.SelectedItem.Text;
                }
                dtv.Rows.Add(drv);
                gvvendor.DataSource = dtv;
                gvvendor.DataBind();
            }

            DataTable dt = clsPriceUpload.GetBidSellingFileUpload(model.bid_no, model.ref_schedule_no);
            if (dt.Rows.Count > 0)
            {
                gvFile.DataSource = dt;
                gvFile.DataBind();
            }
            else
            {
                DataRow dr = dt.NewRow();
                dt.Rows.Add(dr);
                gvFile.DataSource = dt;
                gvFile.DataBind();
            }

            if (hidzmode.Value == "Winner")
            {
                gvFile.Columns[gvFile.Columns.Count - 1].Visible = true;
            }

            ddlFromvendor.DataSource = dtv;
            ddlFromvendor.DataValueField = "vendor_code";
            ddlFromvendor.DataTextField = "vendor_name";
            ddlFromvendor.DataBind();
        }
        private void GetBidData()
        {
            if (!string.IsNullOrEmpty(hidbidid.Value))
            {
                DataTable dt = clsPriceUpload.GetBidScheduleList(int.Parse(hidbidid.Value));
                if (dt.Rows.Count > 0)
                {
                    lblBidno.Text = dt.Rows[0]["bid_no"].ToString();
                    lblDesc.Text = dt.Rows[0]["bid_desc"].ToString();
                }
            }
        }
        private VenderModel SaveVendor()
        {
            VenderModel model = new VenderModel();
            model.bid_no = lblBidno.Text;
            model.vendor_code = txtvendorCode.Text;
            model.vendor_name = txtvendorName.Text;
            model.vendor_type = ddlType.SelectedItem.Text;
            model.ref_schedule_no = ddlScRef.SelectedValue;
            model.bidder_id = hidbidderid.Value;
            model.bid_revision = "0";
            clsPriceUpload.InsertBidSellingVendor(model);
            return model;
        }
        protected void btnSaveNew_Click(object sender, EventArgs e)
        {
            //Save Data
            VenderModel model = SaveVendor();
            //BindGrid
            gvvendor.DataSource = clsPriceUpload.GetBidVendor(model);
            gvvendor.DataBind();

            //Bidddl
            ddlFromvendor.Items.Add(model.vendor_name);
        }
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUploadModel model = new FileUploadModel();
            if (!string.IsNullOrEmpty(ddlDocType.SelectedValue))
            {
                SaveVendor();
                //Upload File
                if (fileUploadFromvendor.PostedFile.ContentLength > 0)
                {
                    model.vendor_code = ddlFromvendor.SelectedValue;
                    model.bid_no = lblBidno.Text;
                    model.doc_name = Path.GetFileNameWithoutExtension(fileUploadFromvendor.PostedFile.FileName);
                    model.doc_type = ddlDocType.SelectedValue;
                    model.doc_version = "0";
                    model.ref_schedule_no = ddlScRef.SelectedValue;

                    if (hidzmode.Value != "Winner")
                    {
                        model.status_bid = "selling";
                    }
                    else
                    {
                        model.status_bid = "winner";
                    }

                    model.bidder_id = ddlFromvendor.SelectedValue;

                    model.bid_revision = "0";

                    string parentNodeId = ConfigurationManager.AppSettings["WF_UPLOAD_PS"].ToString();
                    OTFunctions oT = new OTFunctions();
                    Stream fs = fileUploadFromvendor.PostedFile.InputStream;
                    BinaryReader br = new BinaryReader(fs);
                    byte[] bytes = br.ReadBytes((Int32)fs.Length);
                    if (bytes.Length > 0)
                    {
                        string bidnonodeid = string.Empty;
                        string scnodeid = string.Empty;
                        string vendornodeid = string.Empty;
                        string desnodeid = string.Empty;
                        string finalnodeid = string.Empty;

                        try
                        {
                            bidnonodeid = oT.getNodeIDByName(parentNodeId, lblBidno.Text);
                        }
                        catch
                        {
                            bidnonodeid = oT.createFolder(parentNodeId, lblBidno.Text).ID.ToString();
                        }
                        try
                        {
                            scnodeid = oT.getNodeIDByName(bidnonodeid, ddlScRef.SelectedValue);
                        }
                        catch
                        {
                            scnodeid = oT.createFolder(bidnonodeid, ddlScRef.SelectedValue).ID.ToString();
                        }
                        try
                        {
                            vendornodeid = oT.getNodeIDByName(scnodeid, ddlFromvendor.SelectedItem.Text.Trim());
                        }
                        catch
                        {
                            vendornodeid = oT.createFolder(scnodeid, ddlFromvendor.SelectedItem.Text.Trim()).ID.ToString();
                        }
                        try
                        {
                            finalnodeid = oT.getNodeIDByName(vendornodeid, "Final Version");
                        }
                        catch
                        {
                            finalnodeid = oT.createFolder(vendornodeid, "Final Version").ID.ToString();
                        }

                        if (hidzmode.Value == "Winner")
                        {
                            model.doc_node_id = oT.uploadDoc(finalnodeid, model.doc_name, model.doc_name, bytes).ToString();
                            model.parent_node_id = finalnodeid;
                        }
                        else
                        {
                            model.doc_node_id = oT.uploadDoc(vendornodeid, model.doc_name, model.doc_name, bytes).ToString();
                            model.parent_node_id = vendornodeid;
                        }

                        clsPriceUpload.InsertBidSellingFileUpload(model);
                        //if (hidzmode.Value == "Winner")
                        //{
                        //    int rowid = clsPriceUpload.GetRowIdFileUpload(model);
                        //    cExcel cExcel = new cExcel();
                        //    cExcel.uploadData(rowid, fs, hidlogin.Value);
                        //}
                    }
                }
            }
            //Bind Gird
            DataTable dt = clsPriceUpload.GetBidSellingFileUpload(model.bid_no, model.ref_schedule_no);
            gvFile.DataSource = dt;
            gvFile.DataBind();

            ddlDocType.SelectedIndex = 0;
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void gvFile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            int gvrowindex = int.Parse(e.CommandArgument.ToString());
            if (e.CommandName == "Delete")
            {
                string nodeid = ((Label)gvFile.Rows[gvrowindex].FindControl("lblnodeid")).Text;
                string parentnodeid = ((Label)gvFile.Rows[gvrowindex].FindControl("lblparentnode")).Text;
                OTFunctions oT = new OTFunctions();
                oT.deleteNodeID(parentnodeid, nodeid);

                string bidid = ((Label)gvFile.Rows[gvrowindex].FindControl("lblrowid")).Text;
                int rowid = int.Parse(bidid);
                clsPriceUpload.DeleteBisSellingFileUpload(rowid.ToString());
            }
            if (e.CommandName == "AddFinal")
            {
                string parentNodeId = ConfigurationManager.AppSettings["WF_UPLOAD_PS"].ToString();
                OTFunctions oT = new OTFunctions();
                Stream fs = fileUploadFromvendor.PostedFile.InputStream;
                BinaryReader br = new BinaryReader(fs);
                byte[] bytes = br.ReadBytes((Int32)fs.Length);
                FileUploadModel model = new FileUploadModel();
                if (bytes.Length > 0)
                {
                    string bidnonodeid = string.Empty;
                    string scnodeid = string.Empty;
                    string vendornodeid = string.Empty;
                    string desnodeid = string.Empty;
                    string finalnodeid = string.Empty;

                    try
                    {
                        bidnonodeid = oT.getNodeIDByName(parentNodeId, lblBidno.Text);
                    }
                    catch
                    {
                        bidnonodeid = oT.createFolder(parentNodeId, lblBidno.Text).ID.ToString();
                    }
                    try
                    {
                        scnodeid = oT.getNodeIDByName(bidnonodeid, ddlScRef.SelectedValue);
                    }
                    catch
                    {
                        scnodeid = oT.createFolder(bidnonodeid, ddlScRef.SelectedValue).ID.ToString();
                    }
                    try
                    {
                        vendornodeid = oT.getNodeIDByName(scnodeid, ddlFromvendor.SelectedItem.Text.Trim());
                    }
                    catch
                    {
                        vendornodeid = oT.createFolder(scnodeid, ddlFromvendor.SelectedItem.Text.Trim()).ID.ToString();
                    }
                    try
                    {
                        finalnodeid = oT.getNodeIDByName(vendornodeid, "Final Version");
                    }
                    catch
                    {
                        finalnodeid = oT.createFolder(vendornodeid, "Final Version").ID.ToString();
                    }
                    model.doc_name = Path.GetFileNameWithoutExtension(fileUploadFromvendor.PostedFile.FileName);
                    model.bid_no = lblBidno.Text;
                    model.ref_schedule_no = ddlScRef.SelectedValue;
                    model.doc_type = ddlDocType.SelectedValue;
                    oT.uploadDoc(vendornodeid, model.doc_name, model.doc_name, bytes);
                    oT.uploadDoc(finalnodeid, model.doc_name, model.doc_name, bytes);
                    int rowid = clsPriceUpload.GetRowIdFileUpload(model);
                    cExcel cExcel = new cExcel();
                    //cExcel.uploadData(rowid, fs, hidlogin.Value);
                }
            }
            LoadData();
        }
        protected void gvvendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "Delete")
            {
                int gvrowindex = int.Parse(e.CommandArgument.ToString());
                Label bidid = (Label)gvvendor.Rows[gvrowindex].FindControl("lblrowid");
                int rowid = int.Parse(bidid.Text);
                clsPriceUpload.DeleteBidSellingVendor(rowid);
                LoadData();
            }
            if (e.CommandName == "Download")
            {
                string nodeid = e.CommandArgument.ToString();
                if (!string.IsNullOrEmpty(nodeid))
                {
                    OTFunctions oT = new OTFunctions();
                    var xcontent = oT.downloadDoc(nodeid);
                    var xnode = oT.getNodeByID(nodeid);
                    Response.Clear();
                    MemoryStream ms = new MemoryStream(xcontent);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + xnode.Name);
                    Response.Buffer = true;
                    ms.WriteTo(Response.OutputStream);
                    Response.End();
                }
            }
        }
        protected void gvvendor_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void gvFile_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {

        }
        protected void ddlScRef_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlScRef.SelectedIndex > 0)
            {
                LoadData();
            }
        }
        //protected void btnAddNew_Click(object sender, EventArgs e)
        //{
        //    //hidbidderid.Value = "1480";
        //    //hidscname.Value = "230 KV MAE MOH 4 - PHAYAO";
        //    //string url = string.Format("~/form/Price/BidPriceFileUpload?bidid={0}&scname={1}", hidbidderid.Value, hidscname.Value);

        //    //string urlgo = string.Format("~/form/Selling/VendorAdd?url={0}", url);
        //    string hostname = ConfigurationManager.AppSettings["HOST_NAME"].ToString();
        //    string urlgo = string.Format("{0}/form/Selling/VendorAdd.aspx",hostname);

        //    Page.ClientScript.RegisterStartupScript(this.GetType(), "OpenWindow", "window.open('"+ urlgo +"');", true);
        //}
        protected void btnAddToGrid_Click(object sender, EventArgs e)
        {
            DataTable dt = clsPriceUpload.GetVendor(ddlVendor.SelectedValue);

            //Save Data
            VenderModel model = new VenderModel();
            model.bid_no = lblBidno.Text;
            model.vendor_code = dt.Rows[0]["code"].ToString();
            model.vendor_name = dt.Rows[0]["name"].ToString();
            model.vendor_type = "";
            model.ref_schedule_no = ddlScRef.SelectedValue;
            model.bidder_id = hidbidderid.Value;
            model.bid_revision = "0";
            clsPriceUpload.InsertBidSellingVendor(model);

            //BindGrid
            gvvendor.DataSource = clsPriceUpload.GetBidVendor(model);
            gvvendor.DataBind();

            //Bidddl
            ddlFromvendor.Items.Add(model.vendor_name);
        }
    }
}