﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PriceSchedule.aspx.cs" Inherits="PS_System.form.Price.PriceSchedule" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Job Dashboard</title>
    <style type="text/css">
        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }

        th {
            text-align: center;
        }
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left;"><b>Job No.:</b>
                        <input id="Txt_Jobno" type="text" />
                        <asp:Label ID="Label1" runat="server" Text="Label"><b>Rev.:</b></asp:Label>
                        <input id="Text1" type="text" /></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Substation:</b><input id="Txt_Sub" type="text" /></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Work Assignment:</b><input id="Txt_Work" type="text" /></td>
                </tr>
                <!-----------------------------------------------------START Schedule------------------------------------------------->
                <tr>
                    <td style="text-align: left;"><b>Schedule</b></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px;">
                        <asp:GridView ID="gvBidInfo" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField HeaderText="Job No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Job Description">
                                    <ItemTemplate>
                                        <asp:Label ID="Label22" runat="server" Text='<%# Bind("job_Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Revision">
                                    <ItemTemplate>
                                        <asp:Label ID="Label32" runat="server" Text='<%# Bind("revision") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remark">
                                    <ItemTemplate>
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("remark") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Master">
                                    <ItemTemplate>
                                        <asp:Label ID="Label52" runat="server" Text='<%# Bind("ps_doc_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bid/Enq. No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("bidenq_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("schedule_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule Name">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Job Progress">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("job_prog") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                    </td>
                </tr>


                <!-----------------------------------------------------START Price Schedule------------------------------------------------->
                <tr>
                    <td style="text-align: left;"><b>Price Schedule</b></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Bid No.:</b>
                        <input id="Txt_bidNo" type="text" />
                        <asp:Label ID="Label3" runat="server" Text="Label"><b>Revision:</b></asp:Label>
                        <input id="Text1" type="text" /></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Bid Description:</b><input id="Txt_bidDes" type="text" /></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Schedule No.:</b><input id="Txt_ScheNo" type="text" />
                        <input id="Txt_ScheNo2" type="text" />
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>1AB5: Current Transformer and Junction Box</b></td>
                </tr>
                <tr>
                    <td>

                        <asp:ImageButton ID="ImageButton5" runat="server" ImageAlign="left" ImageUrl="~/images/plus.png" Height="25px" Width="25px" />
                        <asp:ImageButton ID="ImageButton6" runat="server" ImageAlign="left" ImageUrl="~/images/down.png" Height="25px" Width="25px" />
                        <asp:ImageButton ID="ImageButton7" runat="server" ImageAlign="left" ImageUrl="~/images/up.png" Height="25px" Width="25px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px;">
                        <asp:GridView ID="GridView2" runat="server" Width="100%"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvEquipment_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <tr>
                                            <th rowspan="4">Item No.
                                            </th>
                                            <th rowspan="4">Material group
                                            </th>
                                            <th rowspan="4">Code No.
                                            </th>
                                            <th rowspan="4">Description
                                            </th>
                                            <th rowspan="4">Drawing No./Reference No.
                                            </th>
                                            <th rowspan="4">Qty.
                                            </th>
                                            <th rowspan="4">Unit
                                            </th>
                                            <th rowspan="4">Currency
                                            </th>
                                            <th colspan="4" style="text-align: center">Supply of Equipment
                                            </th>
                                            <th colspan="2" rowspan="3" style="text-align: center">Local Transportation,<br />
                                                Construction and
                                                <br />
                                                Installation
                                            </th>
                                            <th rowspan="4"></th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Foreign Supply
                                            </th>
                                            <th colspan="2">Local Supply
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">CIF Thai Port
                                            </th>
                                            <th colspan="2">Ex-work Price
                                                <br />
                                                (excluding VAT)<br />
                                                Baht
                                            </th>


                                        </tr>
                                        <tr>
                                            <th>Unit Price
                                            </th>
                                            <th>Amount
                                            </th>
                                            <th>Unit Price
                                            </th>
                                            <th>Amount
                                            </th>
                                            <th>Unit Price
                                            </th>
                                            <th>Amount
                                            </th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <!--<td>
                                                    <asp:Label ID="Label72" runat="server" Text='<%# Bind("item") %>'></asp:Label>
                                                </td>-->
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("mat_grp") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Code_no") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("desc") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("draw_ref") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("qty") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" Text='<%# Bind("unit") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" Text='<%# Bind("currency") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Bind("cif_unitpr") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label13" runat="server" Text='<%# Bind("cif_amt") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label14" runat="server" Text='<%# Bind("ex_unitpr") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label15" runat="server" Text='<%# Bind("ex_amt") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label16" runat="server" Text='<%# Bind("local_unitpr") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label17" runat="server" Text='<%# Bind("locat_amt") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="Right" ImageUrl="~/images/edit.png"
                                                Height="20px" Width="20px" />
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/images/bin.png"
                                                Height="20px" Width="20px" />
                                        </td>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <!-----------------------------------------------------START 1C3: Control Building------------------------------------------------->
                <tr>
                    <td style="text-align: left;"><b>1C3: Control Building</b></td>
                </tr>
                <tr>
                    <td>

                        <asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="left" ImageUrl="~/images/plus.png" Height="25px" Width="25px" />
                        <asp:ImageButton ID="ImageButton3" runat="server" ImageAlign="left" ImageUrl="~/images/down.png" Height="25px" Width="25px" />
                        <asp:ImageButton ID="ImageButton4" runat="server" ImageAlign="left" ImageUrl="~/images/up.png" Height="25px" Width="25px" />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px;">
                        <asp:GridView ID="GridView1" runat="server" Width="100%"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvEquipment_RowDataBound">
                            <Columns>
                                <asp:TemplateField>
                                    <HeaderTemplate>
                                        <tr>
                                            <th rowspan="4">Item No.
                                            </th>
                                            <th rowspan="4">Material group
                                            </th>
                                            <th rowspan="4">Code No.
                                            </th>
                                            <th rowspan="4">Description
                                            </th>
                                            <th rowspan="4">Drawing No./Reference No.
                                            </th>
                                            <th rowspan="4">Qty.
                                            </th>
                                            <th rowspan="4">Unit
                                            </th>
                                            <th rowspan="4">Currency
                                            </th>
                                            <th colspan="4" style="text-align: center">Supply of Equipment
                                            </th>
                                            <th colspan="2" rowspan="3" style="text-align: center">Local Transportation,<br />
                                                Construction and
                                                <br />
                                                Installation
                                            </th>
                                            <th rowspan="4"></th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">Foreign Supply
                                            </th>
                                            <th colspan="2">Local Supply
                                            </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2">CIF Thai Port
                                            </th>
                                            <th colspan="2">Ex-work Price
                                                <br />
                                                (excluding VAT)<br />
                                                Baht
                                            </th>


                                        </tr>
                                        <tr>
                                            <th>Unit Price
                                            </th>
                                            <th>Amount
                                            </th>
                                            <th>Unit Price
                                            </th>
                                            <th>Amount
                                            </th>
                                            <th>Unit Price
                                            </th>
                                            <th>Amount
                                            </th>
                                        </tr>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <!--<td>
                                                    <asp:Label ID="Label72" runat="server" Text='<%# Bind("item") %>'></asp:Label>
                                                </td>-->
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("mat_grp") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label5" runat="server" Text='<%# Bind("Code_no") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label6" runat="server" Text='<%# Bind("desc") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label7" runat="server" Text='<%# Bind("draw_ref") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label8" runat="server" Text='<%# Bind("qty") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label9" runat="server" Text='<%# Bind("unit") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label10" runat="server" Text='<%# Bind("currency") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" Text='<%# Bind("cif_unitpr") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label13" runat="server" Text='<%# Bind("cif_amt") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label14" runat="server" Text='<%# Bind("ex_unitpr") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label15" runat="server" Text='<%# Bind("ex_amt") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label16" runat="server" Text='<%# Bind("local_unitpr") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label17" runat="server" Text='<%# Bind("locat_amt") %>'></asp:Label>
                                        </td>
                                        <td>
                                            <asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="Right" ImageUrl="~/images/edit.png"
                                                Height="20px" Width="20px" />
                                            <asp:ImageButton ID="ImageButton1" runat="server" ImageAlign="Right" ImageUrl="~/images/bin.png"
                                                Height="20px" Width="20px" />
                                        </td>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <!----------------------------------------------------------------------------------->
                <tr>
                    <td>
                        <div class="float-right">
                            <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-blue">Save</button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
