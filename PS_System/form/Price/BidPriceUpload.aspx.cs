﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Price
{
    public partial class BidPriceUpload : System.Web.UI.Page
    {
        clsPriceUpload2 clsPriceUpload = new clsPriceUpload2();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetQueryString();
                //hidlogin.Value = "1000";

                BindDdl();

                //BindGridViewTest();
                BindGridView();
            }
        }
        private void GetQueryString()
        {
            try
            {
                hidlogin.Value = Request.QueryString["login"].ToString();
            }
            catch
            {
                hidlogin.Value = string.Empty;
            }
        }
        private void BindDdl()
        {
            DataTable dtBid = clsPriceUpload.GetBidno();
            ddlBidno.DataSource = dtBid;
            DataRow drbid = dtBid.NewRow();
            drbid["bid_no"] = "Select Bid";
            dtBid.Rows.InsertAt(drbid, 0);
            ddlBidno.DataValueField = "bid_id";
            ddlBidno.DataTextField = "bid_no";
            ddlBidno.DataBind();
        }
        private void BindGridViewTest()
        {
            DataTable dt = clsPriceUpload.GetBidScheduleList("TS12-L-02");
            dt.Columns.Add("bidder_status", typeof(string));
            dt.Columns.Add("bidder_id", typeof(string));
            dt.Columns.Add("bidder_name", typeof(string));
            dt.Columns.Add("vendor_name", typeof(string));

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        private void BindGridView()
        {
            DataTable dt = clsPriceUpload.GetBidScheduleList();
            dt.Columns.Add("bidder_status", typeof(string));
            dt.Columns.Add("bidder_id", typeof(string));
            dt.Columns.Add("bidder_name", typeof(string));
            dt.Columns.Add("vendor_name", typeof(string));

            GridView1.DataSource = dt;
            GridView1.DataBind();
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dt = clsPriceUpload.GetBidScheduleList(int.Parse(ddlBidno.SelectedValue));
            dt.Columns.Add("bidder_status", typeof(string));
            dt.Columns.Add("bidder_id", typeof(string));
            dt.Columns.Add("bidder_name", typeof(string));
            dt.Columns.Add("vendor_name", typeof(string));

            GridView1.DataSource = dt;
            GridView1.DataBind();

        }
        protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            GridView theGrid = sender as GridView;
            int newPageIndex = 0;
            if (-2 == e.NewPageIndex)
            {
                TextBox txtNewPageIndex = null;
                GridViewRow pagerRow = theGrid.BottomPagerRow;
                if (null != pagerRow)
                {
                    txtNewPageIndex = pagerRow.FindControl("txtNewPageIndex") as TextBox;
                }

                if (null != txtNewPageIndex)
                {
                    newPageIndex = int.Parse(txtNewPageIndex.Text) - 1;
                }
            }
            else
            {
                newPageIndex = e.NewPageIndex;
            }
            newPageIndex = newPageIndex < 0 ? 0 : newPageIndex;
            newPageIndex = newPageIndex >= theGrid.PageCount ? theGrid.PageCount - 1 : newPageIndex;
            theGrid.PageIndex = newPageIndex;
            GridView1.PageIndex = newPageIndex;
            BindGridView();
        }
        protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "AddNew")
            {
                int rowindex = int.Parse(e.CommandArgument.ToString());
                Label schedulename = (Label)GridView1.Rows[rowindex].FindControl("lblRef");
                Label bidid = (Label)GridView1.Rows[rowindex].FindControl("lblBidId");

                Session["bidid"] = bidid.Text;
                Session["login"] = hidlogin.Value;
                Response.Redirect("~/form/Price/AddContractor");
            }
            if (e.CommandName == "Upload")
            {
                int rowindex = int.Parse(e.CommandArgument.ToString());
                Label bidderid = (Label)GridView1.Rows[rowindex].FindControl("lblBidderId");
                Label bidid = (Label)GridView1.Rows[rowindex].FindControl("lblBidId");

                Session["bidid"] = bidid.Text;
                //Session["bidderid"] = bidderid.Text;
                Session["login"] = hidlogin.Value;
                Response.Redirect("~/form/Price/BidPriceFileUpload");
            }

            if (e.CommandName == "Download")
            {
                string nodeid = e.CommandArgument.ToString();
                if (!string.IsNullOrEmpty(nodeid))
                {
                    OTFunctions oT = new OTFunctions();
                    var xcontent = oT.downloadDoc(nodeid);
                    var xnode = oT.getNodeByID(nodeid);
                    Response.Clear();
                    MemoryStream ms = new MemoryStream(xcontent);
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-disposition", "attachment;filename=" + xnode.Name);
                    Response.Buffer = true;
                    ms.WriteTo(Response.OutputStream);
                    Response.End();
                }
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void GridView1_DataBound(object sender, EventArgs e)
        {
            for (int rowIndex = GridView1.Rows.Count - 2; rowIndex >= 0; rowIndex--)
            {
                GridViewRow gvRow = GridView1.Rows[rowIndex];
                GridViewRow gvPreviousRow = GridView1.Rows[rowIndex + 1];
                for (int cellCount = 0; cellCount < gvRow.Cells.Count; cellCount++)
                {
                    if (gvRow.Cells[cellCount].Text == gvPreviousRow.Cells[cellCount].Text)
                    {
                        if (gvPreviousRow.Cells[cellCount].RowSpan < 2)
                        {
                            gvRow.Cells[cellCount].RowSpan = 2;
                        }
                        else
                        {
                            gvRow.Cells[cellCount].RowSpan =
                                gvPreviousRow.Cells[cellCount].RowSpan + 1;
                        }
                        gvPreviousRow.Cells[cellCount].Visible = false;
                    }
                }
            }
        }
        protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                string bidno = ((Label)e.Row.FindControl("lblBidno")).Text;
                BulletedList bllBidder_id = ((BulletedList)e.Row.FindControl("bllBidderId"));
                BulletedList bllBiddername = ((BulletedList)e.Row.FindControl("bllBidderName"));
                BulletedList bllVenderName = ((BulletedList)e.Row.FindControl("bllVenderName"));

                DataTable dt = clsPriceUpload.GetBidderOnBidSelling(bidno);

                bllBidder_id.DataSource = dt;
                bllBidder_id.DataTextField = "bidder_id";
                bllBidder_id.DataBind();

                bllBiddername.DataSource = dt;
                bllBiddername.DataTextField = "vendor_name";
                bllBiddername.DataBind();

                DataTable dt2 = clsPriceUpload.GetVendorOnBidSellingVendor(bidno);
                bllVenderName.DataSource = dt2;
                bllVenderName.DataTextField = "vendor_name";
                bllVenderName.DataBind();

            }
        }
    }
}