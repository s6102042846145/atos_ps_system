﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Price
{
    public partial class AddLegend : System.Web.UI.Page
    {
        public clsAddLegend objAddLegend = new clsAddLegend();
        public clsJobDashboard objJobDb = new clsJobDashboard();
        public string key_Decrypt = (ConfigurationManager.AppSettings["key_Decrypt"].ToString());

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                hidATOS.Value = key_Decrypt; //String.Join(",", Encoding.UTF8.GetBytes(key_Decrypt)); //

                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z596359";

                if (Request.QueryString["jobno"] != null)
                    this.hidJobNo.Value = Request.QueryString["jobno"];
                else
                    this.hidJobNo.Value = "TIPE-01-S03";

                if (Request.QueryString["jobrevision"] != null)
                    this.hidJobRevision.Value = Request.QueryString["jobrevision"];
                else
                    this.hidJobRevision.Value = "11";

                this.SelectData();
                this.loadLegendData(this.hidJobNo.Value, this.hidJobRevision.Value);
            }
        }
        private void SelectData()
        {
            var jinfo = objJobDb.getJobInfo(hidJobNo.Value);
            txtJobNo.Text = jinfo.job_no;
            txtJobRev.Text = jinfo.revision;
            txtSubLine.Text = jinfo.sub_line_name;
            txtJobDesc.Text = jinfo.job_desc;
            txtRemark.Text = jinfo.remark;
            txtJobContent.Text = jinfo.reference;
        }
        private void loadLegendData(string jobno, string job_revision)
        {
            DataTable dt = objAddLegend.getLegendByJob(jobno, job_revision);

            DataRow dr = dt.NewRow();
            dr["legend"] = "";
            dr["legend_qty"] = "0";
            dt.Rows.Add(dr);

            this.gvLegend.DataSource = dt;
            this.gvLegend.DataBind();

            Session.Add("dtLegend", dt);
        }

        protected void gvLegend_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox txtLegend = (TextBox)e.Row.FindControl("gvLegend_txtLegend");
                ImageButton btnAdd = (ImageButton)e.Row.FindControl("gvLegend_btnAdd");
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("gvLegend_btnRemove");
                if (txtLegend.Text != "")
                {
                    btnAdd.Style.Add("display", "none");
                    btnRemove.Style.Add("display", "block");
                }
                else
                {
                    btnAdd.Style.Add("display", "block");
                    btnRemove.Style.Add("display", "none");
                }
            }
        }
        protected void gvLegend_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var xrowid = Convert.ToInt32(e.CommandArgument);
            GridView gvLegend = (GridView)sender;
            string xLegend = ((TextBox)gvLegend.Rows[xrowid].FindControl("gvLegend_txtLegend")).Text;
            string xLegend_QTY = ((TextBox)gvLegend.Rows[xrowid].FindControl("gvLegend_txtQTY")).Text;

            if (e.CommandName == "adddata")
            {
                DataTable dtLegend = (DataTable)Session["dtLegend"];
                dtLegend.Rows.Clear();
                foreach (GridViewRow g1 in gvLegend.Rows)
                {
                    TextBox txtLegend = (TextBox)g1.FindControl("gvLegend_txtLegend");
                    TextBox txtQTY = (TextBox)g1.FindControl("gvLegend_txtQTY");

                    DataRow drOld = dtLegend.NewRow();
                    drOld["legend"] = txtLegend.Text;
                    drOld["legend_qty"] = txtQTY.Text;
                    dtLegend.Rows.Add(drOld);
                }

                DataRow drNew = dtLegend.NewRow();
                drNew["legend"] = "";
                drNew["legend_qty"] = "0";
                dtLegend.Rows.Add(drNew);

                this.gvLegend.DataSource = dtLegend;
                this.gvLegend.DataBind();

                Session.Add("dtLegend", dtLegend);
            }
            else if (e.CommandName == "viewdata")
            {
                DataTable dt = objAddLegend.viewLegend(xLegend);
                if (dt.Rows.Count > 0)
                {
                    this.lblEquipCode.Text = dt.Rows[0]["equip_code"].ToString();
                    this.lblEquipUnit.Text = dt.Rows[0]["unit"].ToString();
                    this.lblEquipDesc.Text = dt.Rows[0]["equip_desc_full"].ToString();

                    this.myModal_Equip.Style.Add("display", "block");
                }
            }
            else if (e.CommandName == "removedata")
            {
                DataTable dtLegend = (DataTable)Session["dtLegend"];
                DataRow[] drSelect = dtLegend.Select("legend = '" + xLegend + "'");
                if (drSelect.Length > 0)
                    dtLegend.Rows.Remove(drSelect[0]);

                this.gvLegend.DataSource = dtLegend;
                this.gvLegend.DataBind();

                Session.Add("dtLegend", dtLegend);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                string strValue = "";
                foreach (GridViewRow g1 in gvLegend.Rows)
                {
                    TextBox txtLegend = (TextBox)g1.FindControl("gvLegend_txtLegend");
                    TextBox txtQTY = (TextBox)g1.FindControl("gvLegend_txtQTY");

                    if (txtLegend.Text != "")
                        strValue += (strValue == "" ? "" : "|") + txtLegend.Text + "_" + txtQTY.Text;
                }

                objAddLegend.insertLegend(txtJobNo.Text, txtJobRev.Text, strValue, hidLogin.Value);

                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}