﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockReservation02.aspx.cs" Inherits="PS_System.form.Stock.StockReservation02" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }

        .auto-style1 {
            height: 29px;
        }
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="text-align: left;"><b>Job No.:</b>
                        <input id="Txt_Jobno" type="text" />
                        <asp:Label ID="Label1" runat="server" Text="Label"><b>Rev.:</b></asp:Label>
                        <input id="Text1" type="text" /></td>
                </tr>


                <!-- PROGRESS BAR-->
                <!--<tr>
                                 <td style="width: 50%; text-align: right;"><b>Job Progress:</b><td>
                                     <div class="progress">
                        <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:20%">20% 
                        </div>
                    </div>                              
                </tr>-->

                <tr>
                    <td style="text-align: left;"><b>Substation:</b><input id="Txt_Sub" type="text" /></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>Stock Reservation</b></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>1AB12: AC & DC Distribution Board and Termination Box</b></td>
                </tr>

                <tr>
                    <td colspan="2" style="padding-bottom: 15px;">
                        <asp:GridView ID="gvBidInfo" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField HeaderText="Item No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mat Group">
                                    <ItemTemplate>
                                        <asp:Label ID="Label22" runat="server" Text='<%# Bind("mat_grp") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Code">
                                    <ItemTemplate>
                                        <asp:Label ID="Label32" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Required QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label52" runat="server" Text='<%# Bind("required_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reserved QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("reserved_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Confirm Reservation">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("confirm_reserv") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bid QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("bid_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reserve">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("job_prog") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <!------------------------------------------------------------------------------------------------->
                <tr>
                    <td style="text-align: left;"><b>2AB15: Insulator</b></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px;">
                        <asp:GridView ID="GridView2" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField HeaderText="Item No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Mat Group">
                                    <ItemTemplate>
                                        <asp:Label ID="Label22" runat="server" Text='<%# Bind("mat_grp") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Code">
                                    <ItemTemplate>
                                        <asp:Label ID="Label32" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Required QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label52" runat="server" Text='<%# Bind("required_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reserved QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("reserved_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Confirm Reservation">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("confirm_reserv") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bid QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("bid_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reserve">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("job_prog") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="Label62" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <!-----------------------------------------------BUTTON----------------------------------------------------->
                <tr>
                    <td>
                        <div class="float-right">
                            <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-blue">Save Draf</button>
                            &nbsp; &nbsp;
                            <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-blue">Request Reservertion</button>
                            &nbsp; &nbsp;
                            <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-blue">Cancel </button>
                        </div>
                    </td>
                </tr>

                <!----------------------------------------------START Reservation Request------------------------------------------------------------>

                <tr>
                    <td style="text-align: left;"><b>Reservation Request</b></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><b>From:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </b>&nbsp;<input id="Txt_Jobno" type="text" />
                        <asp:Label ID="Label4" runat="server" Text="Label"><b>Requested Date:</b></asp:Label>
                        <input id="Text4" type="text" /></td>
                </tr>
                <tr>
                    <td style="text-align: left;" class="auto-style1"><b>Job No.:&nbsp;&nbsp;&nbsp;&nbsp; </b>&nbsp;<input id="Txt_Jobno" type="text" />
                        <asp:Label ID="Label2" runat="server" Text="Label"><b>Substation:</b></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <input id="Text4" type="text" /></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Label"><b>Substation:</b></asp:Label>
                        <input id="Text4" type="text" />
                    </td>
                </tr>
                <!---------------------------------------------------------------------------------------------------------->
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">
                        <asp:GridView ID="GridView1" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvEquipment_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label211" runat="server" Text='<%# Bind("no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Storage">
                                    <ItemTemplate>
                                        <asp:Label ID="Label233" runat="server" Text='<%# Bind("storage") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Batch">
                                    <ItemTemplate>
                                        <asp:Label ID="Label244" runat="server" Text='<%# Bind("batch") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SOH">
                                    <ItemTemplate>
                                        <asp:Label ID="Label244" runat="server" Text='<%# Bind("soh") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Reserved QTY">
                                    <ItemTemplate>
                                        <asp:Label ID="Label244" runat="server" Text='<%# Bind("reserved_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Value">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("value") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="EGAT Contract No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("contract_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="WBS Element">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("wbs_ele") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Substation">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("substation") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="อายุ ERP">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("erp") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="อายุ EPR (เดือน)">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("erp_mnt") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="อายุ L/I">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("li") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="อายุ L/I">
                                    <ItemTemplate>
                                        <asp:Label ID="Label255" runat="server" Text='<%# Bind("li") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <!-------------------------------------------BOTTUN--------------------------------------------------->
                <tr>
                    <td>
                        <div class="float-right">
                            <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-blue">Confirm Reservation</button>
                            &nbsp; &nbsp;
                            <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-blue">Cancel</button>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
