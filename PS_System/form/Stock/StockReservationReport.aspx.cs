﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PS_Library;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Web.Services;

namespace PS_System.form.Stock
{
    public partial class StockReservationReport : System.Web.UI.Page
    {
       // public string zetl4eisdb = ConfigurationManager.AppSettings["etl4eisdb"].ToString();
        //static DbControllerBase zdbUtil = new DbControllerBase();
        public DataTable dtList = new DataTable();
        public StockReservation stock = new StockReservation();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {

                //reqName = Request.QueryString["name"];
                bindDataPlant();
                bindDataMatGroup();
                bindDataStorageLo();
                bindDataEqCode();

              
            }
            else
            {
                dtList = (DataTable)Session["Datalist"];
            }
        }

        private void bindDataPlant()
        {
            //ddlPlant.Items.Insert(0, new ListItem("---Select---", ""));
            //ddlPlant.Items.Insert(1, new ListItem("REG2", "REG2"));
            //ddlPlant.Items.Insert(2, new ListItem("REG3", "REG3"));
            //ddlPlant.Items.Insert(3, new ListItem("REG4", "REG4"));
            //ddlPlant.DataBind();
            DataTable dt = new DataTable();
            dt = stock.get_sap_material_plant();

            //new SelectList(_buildingService.GetBuildings(), "Id", "Name");
            int i = 0;
            foreach(DataRow dr in dt.Rows)
            {
                Multiselect_Plant.Items.Insert(i, new ListItem(dr["plant_code"].ToString(), dr["plant_code"].ToString()));
                i++;
            }
           
            Multiselect_Plant.DataBind();
        }

        private void bindDataMatGroup()
        {
            //ddlMatGroup.Items.Insert(0, new ListItem("---Select---", ""));
            //ddlMatGroup.Items.Insert(1, new ListItem("TS-LN-AC", "TS-LN-AC"));
            //ddlMatGroup.Items.Insert(2, new ListItem("TS-MSC", "TS-MSC"));
            //ddlMatGroup.Items.Insert(3, new ListItem("TS-SUB-SA", "TS-SUB-SA"));
            //ddlMatGroup.Items.Insert(4, new ListItem("TS-SUB-CF", "TS-SUB-CF")); 
            //ddlMatGroup.DataBind();

            DataTable dt = new DataTable();
            dt = stock.get_sap_materialgroup();

            //new SelectList(_buildingService.GetBuildings(), "Id", "Name");
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                Multiselect_MatGroup.Items.Insert(i, new ListItem("(" + dr["code"].ToString() + ") " + dr["name"].ToString(), dr["code"].ToString()));
                i++;
            }

            Multiselect_MatGroup.DataBind();
        }

        private void bindDataStorageLo()
        {
            //ddlStorageLo.Items.Insert(0, new ListItem("---Select---", ""));
            //ddlStorageLo.Items.Insert(1, new ListItem("30AW", "30AW"));
            //ddlStorageLo.Items.Insert(2, new ListItem("2NAW", "2NAW"));
            //ddlStorageLo.DataBind();

            DataTable dt = new DataTable();
            dt = stock.get_m_sap_storagelocation();

            //new SelectList(_buildingService.GetBuildings(), "Id", "Name");
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                Multiselect_StorageLo.Items.Insert(i, new ListItem("("+ dr["code"].ToString() + ") "+dr["name"].ToString(), dr["code"].ToString()));
                i++;
            }

            Multiselect_StorageLo.DataBind();
        }

        private void bindDataEqCode()
        {
            //ddlEqCode.Items.Insert(0, new ListItem("---Select---", ""));
            //ddlEqCode.Items.Insert(1, new ListItem("3105318214", "3105318214"));
            //ddlEqCode.Items.Insert(2, new ListItem("3105365372", "3105365372"));
            //ddlEqCode.Items.Insert(3, new ListItem("3000042941", "3000042941")); 
            //ddlEqCode.DataBind();

            DataTable dt = new DataTable();
            dt = stock.get_m_sap_material();

            //new SelectList(_buildingService.GetBuildings(), "Id", "Name");
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                Multiselect_EqCode.Items.Insert(i, new ListItem("(" + dr["code_old"].ToString() + ") " + dr["name"].ToString(), dr["code_old"].ToString()));
                i++;
            }

            Multiselect_EqCode.DataBind();
        }

        //[WebMethod]
        //private static void bindData()
        //{

        //  string  sql = @"SELECT * FROM [ECM_DEV].[dbo].[ps_m_stock_reserved]";

        //    //DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
        //    //gvStock.DataSource = dt;
        //    //gvStock.DataBind();

        //}
        private  void bindDataGrid()
        {
            string itemPlant = "";
            string itemMatGroup = "";
            string itemStorageLo = "";
            string itemEqCode = "";
            bool chk = false;
            int count = 1;
            foreach (ListItem item in Multiselect_Plant.Items)
            { 
                if (item.Selected)
                {
                   
                        itemPlant += "'" +item.Value+"'" +",";
                    
                    
                }
                count++;
            }
            count = 1;
            foreach (ListItem item in Multiselect_MatGroup.Items)
            {
                if (item.Selected)
                {
                    
                        itemMatGroup += "'" + item.Value + "'" + ",";
                    

                }
                count++;
            }
            count = 1;
            foreach (ListItem item in Multiselect_StorageLo.Items)
            {
                if (item.Selected)
                {
                    
                    
                        itemStorageLo += "'" + item.Value + "'" + ",";
                    

                }
                count++;
            }
            count = 1;
            foreach (ListItem item in Multiselect_EqCode.Items)
            {
                if (item.Selected)
                {
                    
                        itemEqCode += "'" + item.Value + "'" + ",";
                    

                }
                count++;
            }
            if(itemPlant.Length>0)
            {
                itemPlant = stock.cutLastString(itemPlant);
            }
            if (itemMatGroup.Length > 0)
            {
                itemMatGroup = stock.cutLastString(itemMatGroup);
            }
            if (itemStorageLo.Length > 0)
            {
                itemStorageLo = stock.cutLastString(itemStorageLo);
            }
            if (itemEqCode.Length > 0)
            {
                itemEqCode = stock.cutLastString(itemEqCode);
            }

            dtList = stock.get_sap_stockBalance_report(itemPlant, itemMatGroup,itemStorageLo,itemEqCode );

            // dtList = zdbUtil.ExecSql_DataTable(sqlFull, zetl4eisdb);
            gvStock.DataSource = dtList;
            gvStock.DataBind();

        }
     
        protected void gvStock_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //CheckBox cbAll = (CheckBox)e.Row.FindControl("CbAll");

                //cbAll.Attributes.Add("onclick", "javascript:chkAll(" + cbAll.ClientID + ");");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lblastgrdate = (Label)e.Row.FindControl("lblastgrdate");


                Label lbAgeERP = (Label)e.Row.FindControl("lbAgeERP");
                if (lblastgrdate.Text.Length > 0)
                {
                    string dateERP = stock.calAge(lblastgrdate.Text);
                    lbAgeERP.Text = dateERP;
                }


                //int countBooked = stock.checkBooked();
                //Label lbSOH = (Label)e.Row.FindControl("lbSOH");
                //int countRev = Convert.ToInt32(lbSOH.Text) - countBooked;
                //Label lbNotBooke = (Label)e.Row.FindControl("lbNotBooke");
                //lbNotBooke.Text = countRev.ToString();


                //TextBox txtReserveQty = (TextBox)e.Row.FindControl("txtReserveQty");
                //if (txtReserveQty.Text.Length > 0)
                //{
                //    txtReserveQty.Text = stock.checkReserveQty(countRev, Convert.ToInt32(txtReserveQty.Text)).ToString();
                //}


                // Label lbhidID = (Label)e.Row.FindControl("lbhidID");
                //lbGruop.Text = drv["depart_position_name"].ToString();
                //lbhidID.Text = drv["positionid"].ToString();

                //string kID = gvEquipment.DataKeys[e.Row.RowIndex].Value.ToString();
                // GridView grdViewDetail = (GridView)e.Row.FindControl("grdViewDetail");




            }
        }

        protected void gvStock_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStock.PageIndex = e.NewPageIndex;
            bindDataGrid();
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
         
            bindDataGrid();
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}