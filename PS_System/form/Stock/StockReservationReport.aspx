﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockReservationReport.aspx.cs" Inherits="PS_System.form.Stock.StockReservationReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }

        .ddlstyle {
            width: 200px;
        }

        .auto-style3 {
            height: 75px;
        }

        .auto-style5 {
            height: 86px;
        }
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#Multiselect_Plant').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });
        $(document).ready(function () {
            $('#Multiselect_MatGroup').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });
        $(document).ready(function () {
            $('#Multiselect_StorageLo').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });
        $(document).ready(function () {
            $('#Multiselect_EqCode').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "StockReservationReport.aspx/GetData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var names = response.d;
                    alert(names);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });
        $(function () {

            $('#btnSearc1').on('click', function () {

                alert("xxx");
                $.ajax({
                    type: "POST",
                    url: 'form/Stock/StockReservationReport/bindData',
                    data: "{}",
                    contentType: "application/json",
                    success: function (msg) {
                        msg = msg.hasOwnProperty("d") ? msg.d : msg;
                        alert(msg);
                    }
                });


            });
        });
    </script>

</head>

<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Project Stock Reserved Report"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-left: 10px">
            <div>
                <table>
                    <tr>
                        <td></td>
                    </tr>
                    <tr>
                        <td></td>
                        <td style="text-align: center">
                            <asp:Label ID="lbSearch" runat="server" Text="Search Criteria"></asp:Label></td>
                    </tr>
                    <tr>
                        <td class="auto-style3"></td>
                        <td class="auto-style3"></td>
                        <td style="text-align: left" class="auto-style3">
                            <asp:Label ID="lbPos1" runat="server" Text="Plant"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <select id="Multiselect_Plant" class="multiselect" runat="server" multiple="true" name="D1"></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lbPos2" runat="server" Text="Material Group"></asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <select id="Multiselect_MatGroup" class="multiselect" runat="server" multiple="true" name="D2"></select></td>
                        <td rowspan="2">
                            <div class="float-right">
                            </div>

                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style5"></td>
                        <td class="auto-style5"></td>
                        <td style="text-align: left" class="auto-style5">
                            <asp:Label ID="lbPos3" runat="server" Text="Storage Location"></asp:Label>&nbsp;&nbsp;&nbsp;<select id="Multiselect_StorageLo" class="multiselect" runat="server" multiple="true" name="D3"></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Label ID="lbPos4" runat="server" Text="Equipement Code"></asp:Label>
                            &nbsp;&nbsp;&nbsp;
                            <select id="Multiselect_EqCode" class="multiselect" runat="server" multiple="true" name="D4"></select>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            
                            <asp:Button ID="btnSearch" class="w3-button w3-blue" BackColor="#164094" runat="server" Text="Search" OnClick="btnSearch_Click" />
                        </td>


                    </tr>

                    <tr>
                        <td style="text-align: left; color: #006699;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td></td>
                        <td colspan="6" style="padding-bottom: 15px;">

                            <asp:GridView ID="gvStock" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvStock_RowDataBound" AllowPaging="True" OnPageIndexChanging="gvStock_PageIndexChanging" PageSize="20">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="Black" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>

                                    <asp:TemplateField HeaderText="Plant">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("plant_code") %>'></asp:Label>
                                            <%--Text='<%# Bind("job_no") %>'--%>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="StorageLoc.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbStorageLoc" runat="server" Text='<%# Bind("STORAGELOCATION_CODE") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Equipment Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqCode" runat="server" Text='<%# Bind("code_old") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mat.Group">
                                        <ItemTemplate>
                                            <asp:Label ID="lbMatG" runat="server" Text='<%# Bind("materialgroup_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mat.Code">
                                        <ItemTemplate>
                                            <asp:Label ID="lbMatC" runat="server" Text='<%# Bind("material_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Batch">
                                        <ItemTemplate>
                                            <asp:Label ID="lbBatch" runat="server" Text='<%# Bind("batch_number") %>'></asp:Label>
                                            <%--Text='<%# Bind("schedule_no") %>'--%>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Mat.Desc">
                                        <ItemTemplate>
                                            <asp:Label ID="lbMatD" runat="server" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="SOH">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSOH" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="UOM">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUOM" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Value">
                                        <ItemTemplate>
                                            <asp:Label ID="lbValue" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="EgatContractNo">
                                        <ItemTemplate>
                                            <asp:Label ID="lbEgatContractNo" runat="server" Text='<%# Bind("egatcontractno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="WBS">
                                        <ItemTemplate>
                                            <asp:Label ID="lbWBS" runat="server" Text='<%# Bind("wbs_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="lastgrdate">
                                        <ItemTemplate>
                                            <asp:Label ID="lblastgrdate" runat="server" Text='<%# Bind("lastgrdate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Age ERP (Year/Month)">
                                        <ItemTemplate>
                                            <asp:Label ID="lbAgeERP" runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Age L/I (Year/Month)">
                                        <ItemTemplate>
                                            <asp:Label ID="lbAgeLI" runat="server" Text='<%# Bind("lidate") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                    </asp:TemplateField>


                                </Columns>

                            </asp:GridView>
                        </td>
                    </tr>


                </table>
            </div>
        </div>



    </form>
</body>
</html>
