﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockReqReport.aspx.cs" Inherits="PS_System.form.Stock.StockReqReport" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Stock Request Report</title>
        <style type="text/css">
      

    </style>
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script type="text/javascript">
  
        $(function () {
            $(".picker").datepicker();
        });

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        $(document).ready(function () {

            $('[id*=Multi_reqno]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=Multi_subject]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=Multi_Job]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=Multi_Substation]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });
            $('[id*=Multi_status]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });
        });

        function addReq() {
            window.open('../Stock/StockReservationRequest.aspx', '_blank');
            return false;
        }
        //function selectRows(objCHK) {

        //    var process_id = document.getElementById(objCHK.id.replace("ibtnSection", "lblProcessid")).id;
         
        //    var sp = process_id.split('_');

        //    var rowsindex = "";
        //    if (sp.length > 2) {
        //        rowsindex = sp[2];
        //    }
        //    var selectValue = rowsindex;
        
        //    document.getElementById("hidSelectRows").value = selectValue;
        //}
    </script>
</head>
<body>
  <form id="form1" runat="server">
          <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Stock Request Report"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;">
            <table style="width: 100%;">

                  <tr>
                       <td style="width:5%;">
                        <asp:Label ID="Label1" runat="server" Text="Label">Request No.:</asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="Multi_reqno" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                    <td  style="vertical-align:bottom;width:25%">
                     <p>Request Date:&nbsp;&nbsp;<input type="text" id="Reqdate" runat="server" class="picker" autocomplete="off"> </p>
                    </td>
                </tr>
                <tr><td style="height:10px"></td></tr>
                <tr>
                       <td style="width:5%;">
                        <asp:Label ID="Label4" runat="server" Text="Job No.:"></asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="Multi_Job" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                    <td  style="vertical-align:bottom;width:25%">
                     <p>
                        <asp:Label ID="Label40" runat="server" Text="Substation:"></asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="Multi_Substation" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    &nbsp;</p>
                    </td>
                </tr>
                <tr><td style="height:10px"></td></tr>
                <tr>
                   <td style="width:5%;">
                        <asp:Label ID="Label2" runat="server" Text="Label">Subject:</asp:Label>
                             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ListBox ID="Multi_subject" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                    <td>
                         <asp:Label ID="Label3" runat="server" Text="Label">Status:</asp:Label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:ListBox ID="Multi_status" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                </tr>
                 <tr>
                    <td style="text-align: left;">
                        <br />
                          <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-primary" Width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnsearch_Click" />
                    </td>
                     <td style="text-align: right;">
                                 <br />
                               <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn btn-primary" Width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnAdd_Click" />
                               &nbsp;&nbsp;<asp:Button ID="btnExport" runat="server" Text="Export" CssClass="btn btn-primary" Width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnExport_Click"  />
                     </td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-bottom:15px;">
                          <br /><%--OnRowCommand="gvStockreq_RowCommand" --%>
                        <asp:GridView ID="gvStockreq" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" EmptyDataText="No data found." AllowPaging="True" OnPageIndexChanging="gvStockreq_PageIndexChanging" PageSize="20" OnRowDataBound="gvStockreq_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerSettings Mode="NumericFirstLast" PageButtonCount="6" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White"/>
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField HeaderText="Request No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReqNo" runat="server" Text='<%# Bind("stock_reqno") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Request By">
                                    <ItemTemplate>
                                        <asp:Label ID="lblReqBy" runat="server" Text='<%# Bind("requestor_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Request Date">
                                    <ItemTemplate>
                                      <asp:Label ID="lblReqDate" Text='<%# Eval("requestor_submitdate", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Subject">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSubPro" runat="server" Text='<%# Bind("stock_subject") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                         <%--<asp:ImageButton ID="ibtnSection" CommandName="viewdata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png"  OnClientClick="selectRows(this);"/>--%>
                                         &nbsp;<asp:Label ID="lblStatus" runat="server" Text='<%# Bind("form_status") %>' ></asp:Label>
                                          <asp:Label ID="lblProcessid" runat="server" Text='<%# Bind("process_id") %>' style="display:none;"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                     <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                         <asp:GridView ID="gvStockDetail" runat="server"   AutoGenerateColumns="False" Font-Names="tahoma"  Font-Size="10pt" Width="95%" BackColor="White" BorderColor="#CCCCCC" >
                                            <Columns>
                                                <asp:TemplateField HeaderText="Equipment Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqcode" runat="server" Text='<%# Bind("equipment_code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="400px" />
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="Short Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblShotDes" runat="server"  Text='<%# Bind("equip_desc_short") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="400px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="To Job No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToJobNo" runat="server" Text='<%# Bind("to_job_no") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="400px" />
                                                </asp:TemplateField>
                                                  <asp:TemplateField HeaderText="To Substation">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblToSub" runat="server" Text='<%# Bind("to_substation") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="400px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Reserve Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReserv" runat="server" Text='<%# Bind("reserve_qty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt"  Width="400px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Confirm Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblConf" runat="server" Text='<%# Bind("confirm_qty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt"  Width="400px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                             <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            <%--     <asp:TemplateField HeaderText="Equipment Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqcode1" runat="server" Text='<%# Bind("equipment_code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="70px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Plant">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPlant1" runat="server" Text='<%# Bind("plant") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="70px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Reserve Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReserv1" runat="server" Text='<%# Bind("reserve_qty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Font-Names="tahoma" Font-Size="9pt"  Width="10px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Confirm Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblConf1" runat="server" Text='<%# Bind("confirm_qty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt"  Width="10px" />
                                                </asp:TemplateField>--%>
                            </Columns>
                        
                            <PagerStyle BackColor="#006699" Font-Bold="True" ForeColor="White"  HorizontalAlign="Center"/>
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
          <asp:HiddenField ID="hidLogin" runat="server" />
          <%--<asp:ImageButton ID="ibtnSection" CommandName="viewdata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png"  OnClientClick="selectRows(this);"/>--%>       <%--     <asp:TemplateField HeaderText="Equipment Code">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblEqcode1" runat="server" Text='<%# Bind("equipment_code") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="70px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Plant">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblPlant1" runat="server" Text='<%# Bind("plant") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Font-Names="tahoma" Font-Size="10pt"  Width="70px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Reserve Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblReserv1" runat="server" Text='<%# Bind("reserve_qty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Font-Names="tahoma" Font-Size="9pt"  Width="10px" />
                                                </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Confirm Qty">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblConf1" runat="server" Text='<%# Bind("confirm_qty") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt"  Width="10px" />
                                                </asp:TemplateField>--%><%--<asp:ImageButton ID="ibtnSection" CommandName="viewdata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png"  OnClientClick="selectRows(this);"/>--%>
    </form>
</body>
</html>
