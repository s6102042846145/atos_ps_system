﻿using PS_Library;
using PS_Library.WorkflowService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.Stock
{
    public partial class WfStock : System.Web.UI.Page
    {
        public string zisDev = ConfigurationManager.AppSettings["isDev"].ToString();
        public string zmode = "";
        public string zworkid = "";
        public string zsubworkid = "";
        public string ztaskid = "";
        public string zpsdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string zhost_name = ConfigurationManager.AppSettings["HOST_NAME"].ToString();
        private string strWfMapId = ConfigurationManager.AppSettings["wf_stock_map_id"].ToString();
        public EmpInfoClass zempInfo = new EmpInfoClass();
        clsWfStock objWf = new clsWfStock();


        static PS_Library.OTFunctions zotFunctions = new PS_Library.OTFunctions();
        static PS_Library.DbControllerBase zdbUtil = new PS_Library.DbControllerBase();
        static PS_Library.LogHelper zLogHelper = new PS_Library.LogHelper();


        protected void Page_Load(object sender, EventArgs e)
        {
            EmpInfoClass emp = new EmpInfoClass();
            Employee thisEmp = emp.getInFoByEmpID("z552348");

            if (!IsPostBack)
            {

                try { zmode = Request.QueryString["zmode"].ToString().ToLower(); }
                catch { zmode = "section"; }
                try { zworkid = Request.QueryString["workid"].ToString().ToLower(); }
                catch { zworkid = "2817591"; }
                try { zsubworkid = Request.QueryString["subworkid"].ToString().ToLower(); }
                catch { zsubworkid = "2817591"; }
                try { ztaskid = Request.QueryString["taskid"].ToString().ToLower(); }
                catch { ztaskid = "1"; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); }
                catch { hidLogin.Value = "z575798"; }

                hidMode.Value = zmode;
                hidWorkID.Value = zworkid;
                hidSubworkID.Value = zsubworkid;
                hidTaskID.Value = ztaskid;
                LoadData();
            }
           
        }

        private void LoadData()
        {

            DataTable dtData = objWf.GetWfStockDataByWfProcessId(hidWorkID.Value);
            if (dtData.Rows.Count > 0)
            {
                lblRequestorVal.Text = dtData.Rows[0]["requestor_name"].ToString() + " (" + dtData.Rows[0]["requestor_position"].ToString() + ")";

                if (zmode.ToLower() == "section")
                {
                    lblToVal.Text = dtData.Rows[0]["reviewer_name"].ToString() + " (" + dtData.Rows[0]["reviewer_position"].ToString() + ")";
                }
                else if (zmode.ToLower() == "audit")
                {
                    lblToVal.Text = dtData.Rows[0]["approver_name"].ToString() + " (" + dtData.Rows[0]["approver_position"].ToString() + ")";
                }
                else if (zmode.ToLower() == "dept")
                {
                    lblToVal.Text = dtData.Rows[0]["pm_name"].ToString() + " (" + dtData.Rows[0]["pm_position"].ToString() + ")";
                }
                else
                {
                    lblToVal.Text = dtData.Rows[0]["requestor_name"].ToString() + " (" + dtData.Rows[0]["requestor_position"].ToString() + ")";
                }
                lblSubjectVal.Text = dtData.Rows[0]["stock_subject"].ToString();
                hidProcessID.Value = dtData.Rows[0]["process_id"].ToString();
                try { hidNodeID.Value = dtData.Rows[0]["doc_memo_nodeid"].ToString(); } catch { }
                lblStockReqNo.Text = dtData.Rows[0]["stock_reqno"].ToString();
                txtComment.Text = dtData.Rows[0]["note"].ToString();
                wucComment_Att1.BindData(hidNodeID.Value, hidProcessID.Value, hidLogin.Value, false);

                // get Memo
                getMemo(dtData.Rows[0]["doc_memo_nodeid"].ToString());
            }
        }

        private void getMemo(string strMemoNodeId)
        {
            string xdoc_url = "https://" + zhost_name + "/tmps/forms/viewCSDoc?docnodeid=" + strMemoNodeId + "#toolbar=0";
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");
            ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                string sql = "";
                switch (hidMode.Value.ToLower())
                {
                    case "section"://approver1
                        {
                            sql = " update wf_ps_stock_request "
                            + " set "
                            + " form_status = 'Section Head Approved' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", reviewer_status = 'APPROVED' "
                            + ", reviewer_apvdate = GETDATE() "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";
                           
                        }; break;
                    case "audit"://approver1
                        {
                            sql = " update wf_ps_stock_request "
                            + " set "
                            + " form_status = 'Audit Approved' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", approver_status = 'APPROVED' "
                            + ", approver_apvdate = GETDATE() "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                        }; break;
                    case "dept"://approver1
                        {
                            sql = " update wf_ps_stock_request "
                            + " set "
                            + " form_status = 'Department Head Approved' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", pm_status = 'APPROVED' "
                            + ", pm_apvdate = GETDATE() "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                        }; break;
                }

                zdbUtil.ExecNonQuery(sql, zpsdb);

                // Next WORKFLOW Process
                ApplicationData[] appDatas;
                if (hidWorkID.Value == "")
                {
                    appDatas =  zotFunctions.getWFStartAppData(strWfMapId) ;
                }
                else 
                {
                    appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
                }

                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }

                if (wfAttr != null)
                {
                    if (hidMode.Value.ToLower() == "section") // Review by Dept. Head //approver1
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "Approve1_Status", "approved");
                    }
                    else if (hidMode.Value.ToLower() == "audit") // Review by Section Head//approver2
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "Approve2_Status", "approved");

                    }
                    else if (hidMode.Value.ToLower() == "dept") // Review by Section Head//approver2
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "Approve3_Status", "approved");
                    }
                    zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
                    MailNoti mailN = new MailNoti();

                    if (zotFunctions.getwfAttrValue(ref wfAttr, "Approve3_Status") == "approved")
                    {
                        // mailN.SendEmail("")
                    }

                    Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been approved.');</script>");
                    string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                    string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                //  Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
        }

        protected void btnReject_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtComment.Text.Trim() == "")
                {
                    Response.Write("<script>alert('Please enter Note for Reject');</script>");
                }
                else
                {
                    saveComment();
                    var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                    string sql = "";
                    switch (hidMode.Value.ToLower())
                    {
                        case "section"://approver1
                            {
                                sql = " update wf_ps_stock_request "
                                + " set "
                                + " form_status = 'Section Head Rejected' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", reviewer_status = 'REJECTED' "
                                + ", reviewer_apvdate = GETDATE() "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                            }; break;
                        case "audit"://approver1
                            {
                                sql = " update wf_ps_stock_request "
                                + " set "
                                + " form_status = 'Audit Rejected' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", approver_status = 'REJECTED' "
                                + ", approver_apvdate = GETDATE() "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                            }; break;
                        case "dept"://approver1
                            {
                                sql = " update wf_ps_stock_request "
                                + " set "
                                + " form_status = 'Department Head Rejected' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", pm_status = 'REJECTED' "
                                + ", pm_apvdate = GETDATE() "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                            }; break;
                    }
                    zdbUtil.ExecNonQuery(sql, zpsdb);

                    // Next WORKFLOW Process
                    ApplicationData[] appDatas;
                    if (hidWorkID.Value == "")
                    {
                        // appDatas = zotFunctions.getWFStartAppData(strWfJobMapId.ToString());
                        appDatas = !String.IsNullOrEmpty("") ? zotFunctions.getWFStartAppData("") : zotFunctions.getWFStartAppData("");

                    }
                    else
                    {
                        appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
                    }

                    AttributeData wfAttr = new AttributeData();
                    for (int i = 0; i < appDatas.Length; i++)
                    {
                        try
                        {
                            wfAttr = (AttributeData)appDatas[i];
                            break;
                        }
                        catch (Exception ex)
                        {
                            string strError = ex.Message;
                        }
                    }

                    if (wfAttr != null)
                    {
                        if (hidMode.Value.ToLower() == "section") // Review by Dept. Head //approver1
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "Approve1_Status", "rejected");
                        }
                        else if (hidMode.Value.ToLower() == "audit") // Review by Section Head//approver2
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "Approve2_Status", "rejected");

                        }
                        else if (hidMode.Value.ToLower() == "dept") // Review by Section Head//approver2
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "Approve3_Status", "rejected");
                        }
                        zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);

                        Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been rejected.');</script>");

                        string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                        string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
                    }

                   // Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
                }

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                //Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
        }

        private void saveComment()
        {
            if (txtComment.Text != "")
            {
                var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                string sql = "";
                sql = " insert into wf_comment_att (process_id, req_no, comment, uploaded_by, uploaded_datetime) " +
                    " values " +
                    " ( " +
                    " '" + hidProcessID.Value + "' , " +
                    " '" + lblStockReqNo.Text + "' , " +
                    " '" + txtComment.Text.Trim().Replace("'", "''") + "' , " +

                    " '" + emp.SNAME + " (" + emp.MC_SHORT + ")', " +
                     " GETDATE() " +
                    " ) ";
                zdbUtil.ExecNonQuery(sql, zpsdb);
                wucComment_Att1.BindData(hidNodeID.Value, hidProcessID.Value, hidLogin.Value, false);
                txtComment.Text = "";
            }
        }
    }
}