﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PS_Library;
using System.Configuration;
using System.IO;
using System.Drawing;

namespace PS_System.form.Stock
{
    public partial class StockReqReport : System.Web.UI.Page
    {
        clsStockReqReport objStock = new clsStockReqReport();
        private StockReservation stock = new StockReservation();
       
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z423610";
                Session["page"] = "";
                bind_DDL();
                bind_data();
              
            }
        }
        protected void bind_data()
        {
            if (Session["page"].ToString() != "")
            {
                DataView dv = (DataView)Session["page"];
                gvStockreq.DataSource = dv;
                gvStockreq.DataBind();
            }
            else
            {
                setData();
                //DataTable dt = objStock.getStockReq("");
                //gvStockreq.DataSource = dt;
                //gvStockreq.DataBind();
            }

        }
        protected void bind_DDL()
        {
            DataTable dt = objStock.getStockReq("reqno");
            Multi_reqno.Items.Clear();
            Multi_reqno.DataSource = dt;
            Multi_reqno.DataTextField = "stock_reqno";
            Multi_reqno.DataValueField = "stock_reqno";
            Multi_reqno.DataBind();

            dt = objStock.getStockReq("subject");
            Multi_subject.Items.Clear();
            Multi_subject.DataSource = dt;
            Multi_subject.DataTextField = "stock_subject";
            Multi_subject.DataValueField = "stock_subject";
            Multi_subject.DataBind();

            dt = objStock.getStockReq("status");
            Multi_status.Items.Clear();
            Multi_status.DataSource = dt;
            Multi_status.DataTextField = "form_status";
            Multi_status.DataValueField = "form_status";
            Multi_status.DataBind();


            //krit
            DataTable dt2 = new DataTable();
            dt2 = stock.getJob();


            int i = 0;
            foreach (DataRow dr in dt2.Rows)
            {
                Multi_Job.Items.Insert(i, new ListItem(dr["job_no"].ToString(), dr["job_no"].ToString()));
                i++;
            }

            Multi_Job.DataBind();

            dt2 = stock.getLocation();
             i = 0;
            foreach (DataRow dr in dt2.Rows)
            {
                Multi_Substation.Items.Insert(i, new ListItem(dr["location_name"].ToString(), dr["location_name"].ToString()));
                i++;
            }

            Multi_Substation.DataBind();

            default_Multisel();
        }
        protected void default_Multisel()
        {
            for (int i = 0; i < Multi_reqno.Items.Count; i++)
            {
                Multi_reqno.Items[i].Selected = true;
            }
            for (int i = 0; i < Multi_subject.Items.Count; i++)
            {
                Multi_subject.Items[i].Selected = true;
            }
            for (int i = 0; i < Multi_status.Items.Count; i++)
            {
                Multi_status.Items[i].Selected = true;
            }
            //krit
            for (int i = 0; i < Multi_Job.Items.Count; i++)
            {
                Multi_Job.Items[i].Selected = true;
            }
            for (int i = 0; i < Multi_Substation.Items.Count; i++)
            {
                Multi_Substation.Items[i].Selected = true;
            }
        }
        //protected void gvStockreq_RowCommand(object sender, GridViewCommandEventArgs e)
        //{
        //    if (e.CommandName == "viewdata")
        //    {
        //        int i = 0;
        //            //System.Convert.ToInt32(e.CommandArgument);
        //        if (hidSelectRows.Value != "") i = Convert.ToInt32(hidSelectRows.Value);
        //        string xprocess_id =  ((Label)gvStockreq.Rows[i].FindControl("lblProcessid")).Text;

        //        //string sql = "select *  from ps_m_bid_document where article_id like '" + xarticle_id + "%' order by article_id ";
        //        //DataSet ds = zdb.ExecSql_DataSet(sql, zconn);
        //        DataTable dt = objStock.getStockDetail(xprocess_id);
        //        var gv = ((GridView)gvStockreq.Rows[i].FindControl("gvStockDetail"));
        //        gv.DataSource = dt;// ds;
        //        gv.DataBind();
        //        //if (gv.Visible == false)
        //        //{
        //        //    gv.Visible = true;


        //        //}
        //        //else { gv.Visible = false; }



        //    }
        //}

        protected void gvStockreq_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStockreq.PageIndex = e.NewPageIndex;
            bind_data();
        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            setData();

        }
        protected void setData()
        {
            string filter = "";
            string reqno = "";
            foreach (ListItem item in Multi_reqno.Items)
            {
                if (item.Selected)
                {
                    if (reqno == "")
                    {
                        reqno = item.Value;
                    }
                    else
                    {
                        reqno += "," + item.Value;
                    }
                }
            }
            if (reqno != "")
            {
                filter = string.Format("stock_reqno in ('" + reqno.Replace(",", "','") + "')");
            }
            string subject = "";
            foreach (ListItem item in Multi_subject.Items)
            {
                if (item.Selected)
                {
                    if (subject == "")
                    {
                        subject = item.Value;
                    }
                    else
                    {
                        subject += "," + item.Value;
                    }
                }
            }
            if (subject != "")
            {
                string task_name = "";
                task_name = string.Format("stock_subject in ('" + subject.Replace(",", "','") + "')");
                if (filter != "") filter = string.Format("{0} and ({1})", filter, task_name);
                else filter = string.Format(task_name);
            }
            string status = "";
            foreach (ListItem item in Multi_status.Items)
            {
                if (item.Selected)
                {
                    if (status == "")
                    {
                        status = item.Value;
                    }
                    else
                    {
                        status += "," + item.Value;
                    }
                }
            }
            if (status != "")
            {
                string form_status = "";
                form_status = string.Format("form_status in ('" + status.Replace(",", "','") + "')");
                if (filter != "") filter = string.Format("{0} and ({1})", filter, form_status);
                else filter = string.Format(form_status);
            }
            if (Reqdate.Value != "")
            {
                string req_date = "";
                req_date = string.Format("requestor_submitdate = '{0}'", Reqdate.Value);
                if (filter != "") filter = string.Format("{0} and ({1})", filter, req_date);
                else filter = string.Format(req_date);
            }

            //krit
            //string Jobno = "";
            //foreach (ListItem item in Multi_reqno.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (Jobno == "")
            //        {
            //            Jobno = item.Value;
            //        }
            //        else
            //        {
            //            Jobno += "," + item.Value;
            //        }
            //    }
            //}
            //if (Jobno != "")
            //{
            //    filter = string.Format("stock_reqno in ('" + reqno.Replace(",", "','") + "')");
            //}
            List<string> listTojobNo = new List<string>();
            string toJobNo = "";

            foreach (ListItem item in Multi_Job.Items)
            {
                if (item.Selected)
                {

                    toJobNo = item.Value;

                    listTojobNo.Add(toJobNo);
                }
            }
            List<string> listToSubstation = new List<string>();
            string toSubstation = "";

            foreach (ListItem item in Multi_Substation.Items)
            {
                if (item.Selected)
                {

                    toSubstation = item.Value;

                    listToSubstation.Add(toSubstation);
                }
            }

            DataTable dt = objStock.getStockReqFilter(listTojobNo, listToSubstation);
            DataView dv = new DataView(dt);
            dv.RowFilter = filter;
            Session.Add("page", dv);
            if (dv.Count > 0)
            {
                gvStockreq.DataSource = dv;
                gvStockreq.DataBind();
            }
            else
            {
                gvStockreq.DataSource = null;
                gvStockreq.DataBind();
            }
        }

        protected void gvStockreq_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                List<string> listTojobNo = new List<string>();
               string toJobNo = "";
               
                    foreach (ListItem item in Multi_Job.Items)
                    {
                        if (item.Selected)
                        {

                            toJobNo = item.Value;

                        listTojobNo.Add(toJobNo);
                        }
                    }
                List<string> listToSubstation = new List<string>();
                string toSubstation = "";
               
                    foreach (ListItem item in Multi_Substation.Items)
                    {
                        if (item.Selected)
                        {

                            toSubstation = item.Value;

                        listToSubstation.Add(toSubstation);
                    }
                    }
                

                DataRowView drv = (DataRowView)e.Row.DataItem;
                GridView gv = (GridView)e.Row.FindControl("gvStockDetail");
                DataTable dt = objStock.getStockDetail(drv["process_id"].ToString(), listTojobNo, listToSubstation);
                gv.DataSource = dt;// ds;
                gv.DataBind();
                int index = e.Row.RowIndex;
                //if (dt.Rows.Count == 0)
                //{

                //    DataView dv = (DataView)Session["page"];
                //    dv.Delete(index);
                //    gvStockreq.DataSource = dv;
                //    gvStockreq.DataBind();
                //    //drv.Delete();
                //    Session.Add("page", dv);
                //}
            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            Response.Clear();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment;filename=StockRequestReport_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff") + ".xls");
            Response.Charset = "";
            Response.ContentType = "application/vnd.ms-excel";
            using (StringWriter sw = new StringWriter())
            {
                HtmlTextWriter hw = new HtmlTextWriter(sw);
                //To Export all pages    
                gvStockreq.AllowPaging = false;
                this.bind_data();
                //gvStockreq.HeaderRow.BackColor = Color.White;

                //foreach (TableCell cell in gvStockreq.HeaderRow.Cells)
                //{
                //    //cell.BackColor = Color.White;
                //}
                foreach (GridViewRow row in gvStockreq.Rows)
                {
                    Label lblProcessid = (Label)row.FindControl("lblProcessid");
                    lblProcessid.Visible = false;
                    row.BackColor = Color.White;
                    foreach (TableCell cell in row.Cells)
                    {
                        if (row.RowIndex % 2 == 0)
                        {
                            cell.BackColor = gvStockreq.AlternatingRowStyle.BackColor;
                        }
                        else
                        {
                            cell.BackColor = gvStockreq.RowStyle.BackColor;
                        }
                        cell.CssClass = "textmode";
                    }
                }
                gvStockreq.RenderControl(hw);
                //style to format numbers to string    
                //string style = @"<style> .textmode { } </style>";
                //Response.Write(style);
                Response.Output.Write(sw.ToString());
                Response.Flush();
                Response.End();
            }
        }
        public override void VerifyRenderingInServerForm(Control control)
        {
            //
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            //string strUrl = ConfigurationManager.AppSettings["url_StockReq"].ToString() + "?zuserlogin=" + hidLogin.Value + "&ztype=N";
            //ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_blank')", true);
           // Response.Redirect(ConfigurationManager.AppSettings["url_StockReq"].ToString() + "?zuserlogin=" + hidLogin.Value + "&ztype=N");
            Response.Redirect("StockReservationRequest.aspx?zuserlogin=" + hidLogin.Value + "&ztype=N");

        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}