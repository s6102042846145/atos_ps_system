﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WfStock.aspx.cs" Inherits="PS_System.form.Stock.WfStock" %>

<%@ Register src="../../UserControl/wucComment_Att.ascx" tagname="wucComment_Att" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 138px;
        }

        .auto-style3 {
            width: 800px;
        }

        #docview {
            height: 450px;
            width: 950px;
                /*893px;*/
        }
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 52px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .btnclass{
            
            background:white;
            color:#508bcb;
            border:none;
            height:40px;
            width:80px;
            border-radius:5px
        }
        .btnSel{
            
            background:white;
            color:black;
            height:40px;
            width:80px;
            border-left:solid;
            border-top:solid;
            border-right:solid;
            border-bottom:none;
            border-width:thin;
            border-radius:5px 5px 0px 0px;
        }
        .btnclass:hover {
            background: #eeeeee;
        }
        .btnPartSel{
            color:white;
            border-color:#357ebd;
            background-color:#357ebd;
            border:1px solid transparent;
            border-radius:3px;
        }
            .Colsmall {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
             .Colsmall2 {
            width: 80px;
            max-width: 80px;
            min-width: 80px;
        }
            #gvAllEqui   th {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
      
    </style>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
     <link href="../../Content/w3.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-3.4.1.min.js"></script>
    <script>
      

      
        $(document).ready(function () {
            var width = new Array();
            var table = $("table[id*=gvAllEqui]"); //Pass your gridview id here.
            table.find("th").each(function (i) {
                width[i] = $(this).width();
            });
            headerRow = table.find("tr:first");
            headerRow.find("th").each(function (i) {
                $(this).width(width[i]);
            });
            firstRow = table.find("tr:first").next();
            firstRow.find("td").each(function (i) {
                $(this).width(width[i]);
            });
            var header = table.clone();
            header.empty();
            header.append(headerRow);
            //header.append(firstRow);
            header.css("width", width);
            $("#container").before(header);
            table.find("tr:first td").each(function (i) {
                $(this).width(width[i]);
            });
            $("#container").height(700);
            //$("#container").width(table.width() + 20);

            $("#container").append(table);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel5" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="lblHeader" runat="server" Font-Bold="true" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Stock Reservation Approval Workflow"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <table class="auto-style1" style="font-family: tahoma; font-size: 10pt; color: #333333;">
            <%--<tr>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Title"></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style3">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel9" runat="server" Width="120px">
                                    <asp:Label ID="lblRequestor" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Requestor :"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" Width="500px">
                                    <asp:Label ID="lblRequestorVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Panel ID="Panel7" runat="server" Width="100px">
                                    <asp:Label ID="lblReqNo" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="SR No.  :"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" Width="500px">
                                    <asp:Label ID="lblStockReqNo" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTo" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="To :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel4" runat="server" Width="500px">
                                    <asp:Label ID="lblToVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>

                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="margin-left: 320px">
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Subject :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" Width="500px">
                                    <asp:Label ID="lblSubjectVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>
                    <asp:Panel ID="p_PDF" runat="server" BackColor="#666666" Height="450px" Width="950px">
                        <iframe id="docview" src="about:blank" runat="server"></iframe>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Note"></asp:Label>
                </td>
            </tr>
            <tr>

                <td>
                    <asp:Panel ID="p_Comment" runat="server">
                        <asp:TextBox ID="txtComment" runat="server" Font-Names="tahoma" Font-Size="10pt" Height="80px" TextMode="MultiLine" Width="800px"></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style2">
                        <tr>
                            <td>
                                <asp:Button ID="btnApprove" runat="server" Font-Names="Tahoma" Font-Size="10pt" OnClick="btnApprove_Click" Text="Approve" Width="100px"  class="btn btn-primary"/>
                            </td>
                            <td>
                                <asp:Button ID="btnReject" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Reject" Width="100px" OnClick="btnReject_Click" class="btn btn-danger" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <uc1:wucComment_Att ID="wucComment_Att1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
        <asp:HiddenField ID="hidReqNo" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidRequestorId" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" />
        <asp:HiddenField ID="hidWorkID" runat="server" />
        <asp:HiddenField ID="hidSubworkID" runat="server" />
        <asp:HiddenField ID="hidTaskID" runat="server" />
        <asp:HiddenField ID="hidProcessID" runat="server" />
        <asp:HiddenField ID="hidNodeID" runat="server" />
    </form>
</body>
</html>
 