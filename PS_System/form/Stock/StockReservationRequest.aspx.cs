﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PS_Library; 
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Web.Services;
using System.Web.Script.Serialization;
using PS_Library.WorkflowService;
using Xceed.Document.NET; 
using System.Net;  
//using Spire.Pdf; 
//using DocumentFormat.OpenXml.Wordprocessing;

namespace PS_System.form.Stock
{
    public partial class StockReservationRequest : System.Web.UI.Page
    {
        private oraDBUtil zdb = new oraDBUtil();
        private string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();
        private string Template_Stock = ConfigurationManager.AppSettings["Template_Stock"].ToString();
        private string Preview_Stock = ConfigurationManager.AppSettings["Preview_Stock"].ToString();
        private DataTable dtList = new DataTable();
        private DataTable dtList2 = new DataTable();
        // public DataTable dtListCheck = new DataTable();
        private StockReservation stock = new StockReservation();
        private clsWfStock objWfStock = new clsWfStock();
        List<int> listIDChild = new List<int>();
        private string PID = "";
        private string WFID = "";
        private bool savemaster = false;
        public string zmapid = "1029984";
        private bool normalMode = true;
        private string position = "";
        private  string fillter = "";
        // private OTFunctions zOT = new OTFunctions();
        protected void Page_Load(object sender, EventArgs e)
        {
            //objWfStock.testFindGroup();



            if (!this.IsPostBack)
            {

                 

                //string id =  stock.getWFID();
                //reqName = Request.QueryString["name"];
                //bindDataddlPlant();
                //bindDataddlMatGroup();
                //bindDataddlStorageLo();
                //bindDataddlEqCode();
                //wf_ps_stock_details wf_ps_stock_details = new wf_ps_stock_details();
                //wf_ps_stock_details.reserve_qty = 333;
                //List<wf_ps_stock_details> list_wf_ps_stock_details = new List<wf_ps_stock_details>();
                //list_wf_ps_stock_details.Add(wf_ps_stock_details);
                //stock.insert_wf_ps_stock_details(list_wf_ps_stock_details);
                #region setQueryString
                //zmode=? requestor , reviewer
                //zuserlogin 
                //mapid = 1006104

                //####ati
                //PID
                //PJSTK_REQNO
                //Subject
                //Requestor_id
                //Requested_Date
                //Reviewer_id
                //Reviewer_Date
                //Reviewer_Status
                //Approver_id
                //Appover_Date
                //Approved_Status
                //WF_Status
                try
                {
                    if (Request.QueryString["zuserlogin"] != null)
                    {
                        hidLogin.Value = Request.QueryString["zuserlogin"].ToString();
                    }
                    else
                    {
                        hidLogin.Value = "z591787";

                        //z591511 กวป-ส.
                        //z596796 กวศ-ส.
                        //z593326 กวสส-ส.
                        //z596796 
                    }

                }

                catch
                {
                    hidLogin.Value = "";
                }

                try
                {
                    if (Request.QueryString["ztype"] != null)
                    {
                        hidType.Value = Request.QueryString["ztype"].ToString();
                        if (hidType.Value == "N")
                        {
                            normalMode = true;
                        }
                        if (hidType.Value == "A")
                        {
                            normalMode = false;
                        }
                        Session["normalMode"] = normalMode;
                    }
                    else
                    {
                        hidType.Value = "";
                    }

                }

                catch
                {
                    hidLogin.Value = "";
                }

                try
                {
                    if (Request.QueryString["zmode"] != null)
                    {
                        hidMode.Value = Request.QueryString["zmode"].ToString();
                    }
                    else
                    {
                        hidMode.Value = "";
                    }
                    //if (hidMode.Value == "requestor") 
                    //{ 
                    //    hidMode.Value = "normal";

                    //}
                }

                catch
                {
                    hidMode.Value = "";
                }
                try
                {
                    if (Request.QueryString["zeqcode"] != null)
                    {
                        hidEqCode.Value = Request.QueryString["zeqcode"].ToString();

                    }
                    else
                    {
                        hidEqCode.Value = "";
                    }

                }

                catch
                {
                    hidEqCode.Value = "";
                }

                try
                {
                    if (Request.QueryString["zjob"] != null)
                    {
                        if (normalMode)
                        {
                            txtJob.Text = Request.QueryString["zjob"].ToString();
                        }
                        else
                        {
                            //txtJob2.Text = Request.QueryString["zjob"].ToString();
                        }

                    }
                    else
                    {
                        if (normalMode)
                        {
                            txtJob.Text = "";
                        }
                        else
                        {
                            // txtJob2.Text = "";
                        }
                    }
                }

                catch
                {
                    if (normalMode)
                    {
                        txtJob.Text = "";
                    }
                    else
                    {
                        // txtJob2.Text = "";
                    }
                }

                try
                {
                    if (Request.QueryString["zrefbid"] != null)
                    {
                        txtRefBid.Text = Request.QueryString["zrefbid"].ToString();
                    }
                    else
                    {
                        txtRefBid.Text = "";
                    }
                }

                catch
                {
                    txtRefBid.Text = "";
                }

                try
                {
                    if (Request.QueryString["zrefbidsc"] != null)
                    {
                        txtRefBidSch.Text = Request.QueryString["zrefbidsc"].ToString();
                    }
                    else
                    {
                        txtRefBidSch.Text = "";
                    }
                }

                catch
                {
                    txtRefBidSch.Text = "";
                }


                #endregion
                //Debug
                // hidLogin.Value = "559482";
                // hidMode.Value = "normal";
                // hidMode.Value = "sp";
                ini_object();
                if (!normalMode)
                {
                    bindDataEqCode(hidEqCode.Value);
                }
                else
                {
                    bindMatGroup();
                    bindDataJob();
                    bindStorage_location();
                    bindEu();
                    bindDataLocation();

                   // bindDataStorageLocation();
                }
                checkType();


            }
            else
            {
                dtList = (DataTable)Session["Datalist"];
                dtList2 = (DataTable)Session["Datalist2"];
                if (Session["PID"] != null)
                {
                    PID = Session["PID"].ToString();
                }
                else
                {
                    PID = "";
                }
                if (Session["WFID"] != null)
                {
                    WFID = Session["WFID"].ToString();
                }
                else
                {
                    WFID = "";
                }
                if (Session["savemaster"] != null)
                {
                    savemaster = (bool)Session["savemaster"];
                }
                else
                {
                    savemaster = false;
                }
                if (Session["normalMode"] != null)
                {
                    normalMode = (bool)Session["normalMode"];
                }
                else
                {
                    normalMode = true;
                }
                if (Session["fillter"] != null)
                {
                    fillter = Session["fillter"].ToString();
                }
                else
                {
                    fillter = "";
                }
                if (Session["position"] != null)
                {
                    position = Session["position"].ToString();
                }
                else
                {
                    position = "";
                }
                
                //if(Session["Datalist2"] !=null)
                //{
                //    btnSubmit.Visible = true;
                //}else
                //{
                //    btnSubmit.Visible = false;
                //}
                // dtListCheck = (DataTable)Session["DatalistCheck"];
            }
        }
        private void checkType()
        {
            if (rdoType.SelectedValue == "1")
            {
                divForjob2.Visible = true;
                divSub2.Visible = false;
                divStoLo2.Visible = false;

            }
            else if (rdoType.SelectedValue == "2")
            {
                divForjob2.Visible = false;
                divSub2.Visible = true;
                divStoLo2.Visible = false;
            }
            else if (rdoType.SelectedValue == "3")
            {
                divForjob2.Visible = false;
                divSub2.Visible = false;
                divStoLo2.Visible = true;
            }
        }
        private void bindMatGroup()
        {
            mtMatGroup.Items.Clear();
            DataTable dt = new DataTable();
            
           
            dt = stock.get_sap_materialgroup(ddlEqu.SelectedValue, fillter);


            int i = 1;
            mtMatGroup.Items.Insert(0, new ListItem("ทั้งหมด",""));
            foreach (DataRow dr in dt.Rows)
            {
                mtMatGroup.Items.Insert(i, new ListItem("(" + dr["code"].ToString() + ") " + dr["name"].ToString(), dr["code"].ToString()));
                i++;
            }

            mtMatGroup.DataBind();

        }
        private void bindStorage_location()
        {
            DataTable dt = new DataTable();
            dt = stock.get_sap_storage_location();


            int i = 1;
            mtWarehouse.Items.Insert(0, new ListItem("ทั้งหมด", ""));
            foreach (DataRow dr in dt.Rows)
            {
                mtWarehouse.Items.Insert(i, new ListItem("(" + dr["plant_code"].ToString()+"-"+ dr["code"].ToString() + ") " + dr["name"].ToString(), dr["code"].ToString()));
                i++;
            }

            mtWarehouse.DataBind();

             i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                mtStorageLo2.Items.Insert(i, new ListItem("(" + dr["plant_code"].ToString() + "-" + dr["code"].ToString() + ") " + dr["name"].ToString(), dr["code"].ToString()));
                //mtStorageLo2.Items.Insert(i, new ListItem("(" + dr["code"].ToString() + ") " + dr["plant_code"].ToString(), dr["code"].ToString()));
                i++;
            }

            mtStorageLo2.DataBind();

        }
        private void bindEu()
        {
            ddlEqu.Items.Clear();

            ListItem lst = new ListItem("ทั้งหมด", "0");
            ddlEqu.Items.Insert(0, lst);

             lst = new ListItem("Main Equipment", "1");
            ddlEqu.Items.Insert(1, lst);

            if (position != "กวสส-ส.")
            {
                lst = new ListItem("Auxiliary", "2");
                ddlEqu.Items.Insert(2, lst);
            }

            ddlEqu.DataBind();
        }
        

        private void bindDataJob()
        {
            DataTable dt = new DataTable();
            dt = stock.getJob();


            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                mtJob2.Items.Insert(i, new ListItem(dr["job_no"].ToString(), dr["job_no"].ToString()));
                i++;
            }

            mtJob2.DataBind();


             i = 1;
            mtJobS.Items.Insert(0, new ListItem("ทั้งหมด", ""));
            foreach (DataRow dr in dt.Rows)
            {
                mtJobS.Items.Insert(i, new ListItem(dr["job_no"].ToString(), dr["job_no"].ToString()));
                i++;
            }

            mtJobS.DataBind();

        }
        private void bindDataLocation()
        {
            DataTable dt = new DataTable();
            dt = stock.getLocation();
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                mtSub2.Items.Insert(i, new ListItem(dr["location_name"].ToString(), dr["location_name"].ToString()));
                i++;
            }

            mtSub2.DataBind();
        }


        private void bindDataStorageLocation()
        {
            DataTable dt = new DataTable();
            dt = stock.get_sap_storage_location();
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                mtStorageLo2.Items.Insert(i, new ListItem("(" + dr["code"].ToString() + ") " + dr["plant_code"].ToString(), dr["code"].ToString()));
                i++;
            }

            mtStorageLo2.DataBind();
        }

        private void bindDataEqCode(string code)
        {


            DataTable dt = new DataTable();
            dt = stock.get_equipment_code(code);


            if (stock.checkRowData(dt))
            {
                txtEqCode.Text = dt.Rows[0]["equip_code"].ToString();
                txtDesc.Text = dt.Rows[0]["equip_desc_full"].ToString();
            }
            ////new SelectList(_buildingService.GetBuildings(), "Id", "Name");
            //int i = 0;

            //foreach (DataRow dr in dt.Rows)
            //{
            //    if(i==0)
            //    {
            //        ddlEqCode.Items.Insert(i, new ListItem("-----select-----", "select"));
            //        i++;
            //    }
            //    ddlEqCode.Items.Insert(i, new ListItem("(" + dr["eq_code"].ToString() + ") " + dr["eq_desc"].ToString(), dr["eq_code"].ToString()));
            //    i++;
            //}

            //ddlEqCode.DataBind();
            //ddlEqCode.SelectedIndex = 0;

            //if (hidEqCode.Value != "")
            //{
            //    ddlEqCode.SelectedValue = hidEqCode.Value;
            //}
        }
        private void ini_requestor() 
        {
            EmpInfoClass empinfo = new EmpInfoClass();
            var req = empinfo.getInFoByEmpID(hidLogin.Value);
            txtReqBy.Text = req.SNAME;
            hidDeptAppId.Value = req.MANAGER_ID;
            hidDeptAppName.Value = req.MANAGER_NAME;
            hidDeptAppPosition.Value = req.MANAGER_POSITION;
            hidDeptAppPositionFull.Value = req.MANAGER_POSITION_FULL;
            txtReqDate.Text = System.DateTime.Now.ToString("dd MMM yyyy");
            hidApproverID.Value = req.MANAGER_ID;
            this.position = req.ORGSNAME4;
            hdnReqDept.Value = position;
            Session["position"] = position;
            //var req2 = empinfo.getEmpIDAndNameInDepartment(position);
            //var req3 = empinfo.getEmpIDAndNameSectionHead(position);
            //var req4 = empinfo.getEmpListInDepartment(position);
            //var req5 = empinfo.getEmpListInDepartment(position);
            //var req6 = empinfo.getEmpListInDepartment_WithoutMD(position); 

            if (position == "กวศ-ส." || position == "กวป-ส.")
            {
                fillter = "SUB";
                Session["fillter"] = fillter;
            }
            if (position == "กวสส-ส.")
            {
                fillter = "LN";
                Session["fillter"] = fillter;
            }


        }

        private void ini_gvStock()
        {
            //DataTable dt2 = new DataTable();
            //dt2 = getDataByCheckbox();
            string sql = @" select plant_code, STORAGELOCATION_CODE, b.code_old,b.materialgroup_code,material_code,batch_number,b.DESCRIPTION,QUANTITY,a.unit_code,amount,egatcontractno,wbs_code,lastgrdate,lidate  
                            from p_m_sap_stockbalance a left join p_m_sap_material b on a.material_code = b.code  
                            where ROWNUM <=1
                            order by materialgroup_code,batch_number asc";
            var ds = zdb._getDS(sql, commonDB);
            var dt1 = ds.Tables[0].Clone();
            //var newRow = dt1.NewRow();
            //dt1.Rows.Add(newRow);r
            gvStock.DataSource = dt1;
            gvStock.DataBind();
            gvStock.Visible = false;
        }
        private void ini_gvStock2()
        {
            //DataTable dt2 = new DataTable();
            //dt2 = getDataByCheckbox();
            //string sql = @" select ' ' as QTY,plant_code, STORAGELOCATION_CODE, b.code_old,b.materialgroup_code,material_code,batch_number,b.DESCRIPTION,QUANTITY,a.unit_code,amount,egatcontractno,wbs_code,lastgrdate,lidate 
            //                 ,' ' as tojobno, ' ' as tosubstation, ' ' as tobidno, ' ' as toScheduleName,' ' as  Status
            //                from p_m_sap_stockbalance a left join p_m_sap_material b on a.material_code = b.code  
            //                where ROWNUM <=1
            //                order by materialgroup_code,batch_number asc";

            //var ds = zdb._getDS(sql, commonDB);
            //var dt2 = ds.Tables[0].Clone();
            var dt2 = stock.get_p_m_sap_stockbalance_ExData().Clone();
            dt2.Columns.Add("rowid", typeof(System.Int32));
            var newRow = dt2.NewRow();
            dt2.Rows.Add(newRow);
            gvStock2.DataSource = dt2;
            gvStock2.DataBind();
        }
        private void ini_object()
        {
            ini_requestor();
            pAdding.Visible = false;
            dvGvstock.Visible = false;

            //ini_gvStock();
            ini_gvStock2();
            if (normalMode)
            {
                divSP.Visible = false;
                divUser.Visible = true;
            }
            else
            {
                divSP.Visible = true;
                divUser.Visible = false;
            }

        }
        private void bindDataGridOracle()
        {
            DataTable dt = new DataTable();
           // string condition = txtSearch.Text;
            //dt = stock.get_sap_stockBalance(condition);

            string jobNo ="";
            string matCode = "";
            string stroLo = "";

            foreach (ListItem item in mtJobS.Items)
            {
                if (item.Selected)
                {

                    jobNo = item.Value;


                }
            }
            foreach (ListItem item in mtMatGroup.Items)
            {
                if (item.Selected)
                {

                    matCode = item.Value;


                }
            }
            foreach (ListItem item in mtWarehouse.Items)
            {
                if (item.Selected)
                {

                    stroLo = item.Value;


                }
            }

            string WbsEle = "";

            if(ddlWbsEle.SelectedValue != "0")
            {
                WbsEle = ddlWbsEle.SelectedItem.Text;
            }

            

            dt = stock.get_sap_stockBalance(jobNo, txtMatCode.Text, txtContract.Text, ddlEqu.SelectedValue, matCode, WbsEle, ddlValueType.SelectedValue, stroLo,txtDesS.Text, fillter);
            dt.Columns.Add("Substation");
            DataTable dtJob = stock.getJob_sub(jobNo);
            DataTable dtEqCode = stock.getEquipcode("");
            DataTable dtStock = dt.Clone();
            if (stock.checkRowData(dt))
            {
                
                foreach (DataRow dr in dt.Rows)
                {
                    if (stock.checkRowData(dtJob))
                    {
                        if (dr["WBSElement"].ToString() != "Reserve")
                        {
                            dr["Substation"] = dtJob.Rows[0]["sub_line_name"].ToString();
                        }
                        else
                        {
                            dr["Substation"] = "Reserve";
                        }
                    }
                   


                    if (stock.checkRowData(dtEqCode))
                    {

                        foreach (DataRow drEq in dtEqCode.Rows)
                        {
                            if (dr["code_old"].ToString() == drEq["equip_code"].ToString())
                            {
                                dr["DESCRIPTION"] = drEq["equip_desc_full"].ToString();
                            }


                        }
                    }
                    dtStock.ImportRow(dr);
                    //if (txtDesS.Text != "")
                    //{
                    //    var desSplit = txtDesS.Text.Split('+');

                    //    foreach (var des in desSplit)
                    //    {
                    //        if (dr["DESCRIPTION"].ToString().ToUpper().Contains(des.ToUpper()))
                    //        {
                    //            dtStock.ImportRow(dr);
                    //        }
                    //    }
                    //}
                    //else
                    //{
                    //    dtStock.ImportRow(dr);
                    //}
                }

                
            }

            ///DataRow[] drrr = dt2.Select("DESCRIPTION like '%" + txtDesS.Text + "%'");

            

            DataColumn Col = dtStock.Columns.Add("ID");
            Col.SetOrdinal(0);// to put the column in position 0;
            int i = 1;
            foreach (DataRow dr in dtStock.Rows)
            {
                dr["ID"] = i;
                i++;

            }

            gvStock.DataSource = dtStock;
            gvStock.DataBind();

            Session["Datalist"] = dtStock;
        }


        private DataTable createDataTableStock2(List<string[]> lsitData)
        {

            DataTable dtlistCopy = new DataTable();
            dtlistCopy = dtList.Copy();

            DataTable dt = new DataTable();
            dt.Clear();

            //column
            dt.Columns.Add("plant_code");
            dt.Columns.Add("STORAGELOCATION_CODE");
            dt.Columns.Add("STORAGELOCATION_Name");
            dt.Columns.Add("code_old");
            dt.Columns.Add("materialgroup_code");
            dt.Columns.Add("materialgroup_name");
            dt.Columns.Add("material_code");
            dt.Columns.Add("batch_number");
            dt.Columns.Add("DESCRIPTION");
            dt.Columns.Add("QTY");
            dt.Columns.Add("unit_code");
            dt.Columns.Add("egatcontractno");
            dt.Columns.Add("wbs_code");
            dt.Columns.Add("WBSElement"); 
            dt.Columns.Add("Substation");
            dt.Columns.Add("toJobNo");
            dt.Columns.Add("toSubstation");
            dt.Columns.Add("toStorageLocation");
            dt.Columns.Add("toStorageLocationName");
            dt.Columns.Add("toBidNo");
            dt.Columns.Add("toScheduleName");
            dt.Columns.Add("toEqCode");
            dt.Columns.Add("Status");
            dt.Columns.Add("rowid");



            // Session["DatalistCheck"] = dt;

            if (Session["Datalist2"] != null)
            {
                DataTable dt2 = (DataTable)Session["Datalist2"];

                DataRow _dr = dt.NewRow();

                foreach (DataRow dr2 in dt2.Rows)
                {
                    dt.Rows.Add(dr2.ItemArray);
                }
            }

            foreach (var array in lsitData)
            {

                DataRow _dr = dt.NewRow();
                int i = 0;
                foreach (var item in array)
                {
                    _dr[dt.Columns[i].ColumnName] = item;
                    i++;
                }
                dt.Rows.Add(_dr);
            }

            //DataView dv = dt.DefaultView;
            //dv.Sort = "ID asc";
            // dt = dv.ToTable();

            return dt;
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {
            // bindDataGrid();
            bindDataGridOracle();
            //gvStock2.DataBind();
            //Session["Datalist2"] = null;

            gvStock.Visible = true;
            dvGvstock.Visible = true;
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            DataTable dt2 = new DataTable();

            if (gvStock.Rows.Count > 0)
            {
                if (stock.checkSelectData(gvStock, "chkSelect", "txtReserveQty"))
                {
                    dt2 = getDataByCheckbox();
                    int num = 0;

                    if (!savemaster)
                    {
                        PID = stock.getPID();
                        WFID = stock.getWFID();

                        savemaster = insert_wf_ps_stock_request();

                        Session["PID"] = PID;
                        Session["WFID"] = WFID;
                        Session["savemaster"] = savemaster;
                    }

                    DataTable dtCopy = new DataTable();

                    //int qty = 0;
                    //update QTY
                    DataTable dtFinal = dt2.Clone();
                    for (int i = 0; i < dt2.Rows.Count; i++)
                    {
                        bool isDupe = false;
                        for (int j = 0; j < dtFinal.Rows.Count; j++)
                        {
                            if (!normalMode)
                            {
                                if (dt2.Rows[i]["code_old"].ToString() == dtFinal.Rows[j]["code_old"].ToString()
                                    && dt2.Rows[i]["material_code"].ToString() == dtFinal.Rows[j]["material_code"].ToString()
                                    && dt2.Rows[i]["batch_number"].ToString() == dtFinal.Rows[j]["batch_number"].ToString())
                                {
                                    dtFinal.Rows[j]["QTY"] = decimal.Parse(dtFinal.Rows[j]["QTY"].ToString()) + decimal.Parse(dt2.Rows[i]["QTY"].ToString());
                                    isDupe = true;
                                    break;
                                }
                            }
                            else
                            {
                               
                                    if (dt2.Rows[i]["code_old"].ToString() == dtFinal.Rows[j]["code_old"].ToString()
                                        && dt2.Rows[i]["material_code"].ToString() == dtFinal.Rows[j]["material_code"].ToString()
                                        && dt2.Rows[i]["batch_number"].ToString() == dtFinal.Rows[j]["batch_number"].ToString()
                                        && dt2.Rows[i]["toJobNo"].ToString() == dtFinal.Rows[j]["toJobNo"].ToString() 
                                        && dt2.Rows[i]["toJobNo"].ToString() !="")
                                    {
                                        dtFinal.Rows[j]["QTY"] = decimal.Parse(dtFinal.Rows[j]["QTY"].ToString()) + decimal.Parse(dt2.Rows[i]["QTY"].ToString());
                                        isDupe = true;
                                        break;
                                    }


                                else if(dt2.Rows[i]["code_old"].ToString() == dtFinal.Rows[j]["code_old"].ToString()
                                        && dt2.Rows[i]["material_code"].ToString() == dtFinal.Rows[j]["material_code"].ToString()
                                        && dt2.Rows[i]["batch_number"].ToString() == dtFinal.Rows[j]["batch_number"].ToString()
                                        && dt2.Rows[i]["toSubstation"].ToString() == dtFinal.Rows[j]["toSubstation"].ToString()
                                        && dt2.Rows[i]["toSubstation"].ToString() != "")
                                    {
                                        dtFinal.Rows[j]["QTY"] = decimal.Parse(dtFinal.Rows[j]["QTY"].ToString()) + decimal.Parse(dt2.Rows[i]["QTY"].ToString());
                                        isDupe = true;
                                        break;
                                    }

                                else if(dt2.Rows[i]["code_old"].ToString() == dtFinal.Rows[j]["code_old"].ToString()
                                        && dt2.Rows[i]["material_code"].ToString() == dtFinal.Rows[j]["material_code"].ToString()
                                        && dt2.Rows[i]["batch_number"].ToString() == dtFinal.Rows[j]["batch_number"].ToString()
                                        && dt2.Rows[i]["toStorageLocation"].ToString() == dtFinal.Rows[j]["toStorageLocation"].ToString()
                                        && dt2.Rows[i]["toStorageLocation"].ToString() != "")
                                    {
                                        dtFinal.Rows[j]["QTY"] = decimal.Parse(dtFinal.Rows[j]["QTY"].ToString()) + decimal.Parse(dt2.Rows[i]["QTY"].ToString());
                                        isDupe = true;
                                        break;
                                    }
                                
                            }
                        }

                        if (!isDupe)
                        {
                            dtFinal.ImportRow(dt2.Rows[i]);
                        }
                    }


                    if (savemaster)
                    {
                        if (insert_wf_ps_stock_details(dtFinal))
                        {
                            if (listIDChild.Count > 0)
                            {
                                listIDChild = listIDChild.Distinct().ToList();

                                //dt2 = stock.get_wf_ps_stock_details_byID(listID);

                                foreach (DataRow dr in dtFinal.Rows)
                                {

                                    if (dr["rowid"].ToString().Equals(""))
                                    {
                                        dr["rowid"] = listIDChild[num].ToString();
                                        num++;
                                    }
                                }

                                gvStock2.DataSource = dtFinal;
                                gvStock2.DataBind();
                                Session["Datalist2"] = dtFinal;
                            }
                        }
                    }



                    pAdding.Visible = false;
                    //resetData();
                }else
                {
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('กรุณาติ๊กอุปกรณ์ก่อนกด Add');", true);
                }
            }
        }

        private void resetData()
        {
            // txtSearch.Text = "";
            ddlValueType.SelectedIndex = 0;
            mtWarehouse.SelectedIndex = 0;
            ddlEqu.SelectedIndex = 0;
            mtMatGroup.SelectedIndex = 0;
            txtContract.Text = "";
            txtDesS.Text = "";
            txtMatCode.Text = "";
            dvGvstock.Visible = false;
            mtJobS.SelectedIndex = 0;
            //ini_gvStock();
        }

        private void setDataTableStock()
        {

            DataTable dt = new DataTable();
            dt = dtList.Copy();

            DataTable dt2 = new DataTable();
            List<DataRow> listDR = new List<DataRow>();
            listDR.Clear();

            dt2 = dtList2.Copy();

            foreach (DataRow dr in dt.Rows)
            {
                foreach (DataRow dr2 in dt2.Rows)
                {
                    if (dr[0].ToString().Equals(dr2[0].ToString()))
                    {
                        listDR.Add(dr);
                    }
                }

            }
            if (dt.Rows.Count > 0)
            {
                foreach (var dr in listDR)
                {
                    dt.Rows.Remove(dr);
                }

            }



            gvStock.DataSource = dt;
            gvStock.DataBind();


        }


        private DataTable getDataByCheckbox()
        {
            DataTable dt = new DataTable();
            List<string[]> listData = new List<string[]>();


            foreach (GridViewRow row in gvStock.Rows)
            {
                if (row.RowType == DataControlRowType.DataRow)
                {
                    CheckBox CheckRow = (row.Cells[0].FindControl("chkSelect") as CheckBox);
                    if (CheckRow.Checked)
                    {
                        string[] arrayData = new string[24];
                        Label lbPlant = (row.Cells[1].FindControl("lbPlant") as Label);
                        Label lbStorageLoc = (row.Cells[1].FindControl("lbStorageLoc") as Label);
                        Label lbStorageLocName = (row.Cells[1].FindControl("lbStorageLocName") as Label);
                        //STORAGELOCATION_Name
                        Label lbEqCode = (row.Cells[1].FindControl("lbEqCode") as Label);
                        Label lbMatG = (row.Cells[1].FindControl("lbMatG") as Label);
                        Label lbMatGN = (row.Cells[1].FindControl("lbMatGN") as Label);
                        Label lbMatC = (row.Cells[1].FindControl("lbMatC") as Label);
                        Label lbBatch = (row.Cells[1].FindControl("lbBatch") as Label);
                        Label lbMatD = (row.Cells[1].FindControl("lbMatD") as Label);
                        TextBox txtReserveQty = (row.Cells[1].FindControl("txtReserveQty") as TextBox);
                        Label lbUOM = (row.Cells[1].FindControl("lbUOM") as Label);
                        Label lbEgatContractNo = (row.Cells[1].FindControl("lbEgatContractNo") as Label);
                        Label lbWBS = (row.Cells[1].FindControl("lbWBS") as Label);
                        Label lbSubStation = (row.Cells[1].FindControl("lbSubStation") as Label);
                        Label lbWBSElement = (row.Cells[1].FindControl("lbWBSElement") as Label);
                        
                        string toJobNo = "";
                        string toSubstation = "";
                        string toStorageLocation = "";
                        string toStorageLocationName = "";
                        string toBidNo = "";
                        string toScheduleName = "";
                        string toEqCode = "";
                        string Status = "New";


                        if (normalMode)
                        {
                            toJobNo = "";
                            if (divForjob2.Visible)
                            {
                                foreach (ListItem item in mtJob2.Items)
                                {
                                    if (item.Selected)
                                    {

                                        toJobNo = item.Value;


                                    }
                                }
                            } 
                            toSubstation = "";
                            if (divSub2.Visible)
                            {
                                foreach (ListItem item in mtSub2.Items)
                                {
                                    if (item.Selected)
                                    {

                                        toSubstation = item.Value;


                                    }
                                }
                            }
                            toStorageLocation = "";
                            if (divStoLo2.Visible)
                            {
                                foreach (ListItem item in mtStorageLo2.Items)
                                {
                                    if (item.Selected)
                                    {

                                        toStorageLocation = item.Value;
                                        toStorageLocationName = item.Text;


                                    }
                                }
                            }
                            // toSubstation = ddlSubstation.SelectedItem.Text;
                        }
                        else
                        {

                            toJobNo = txtJob.Text;
                            toBidNo = txtRefBid.Text;
                            toScheduleName = txtRefBidSch.Text;
                            toEqCode = txtEqCode.Text;
                        }



                        arrayData[0] = lbPlant.Text;
                        arrayData[1] = lbStorageLoc.Text;
                        arrayData[2] = lbStorageLocName.Text; 
                        arrayData[3] = lbEqCode.Text;
                        arrayData[4] = lbMatG.Text;
                        arrayData[5] = lbMatGN.Text; 
                        arrayData[6] = lbMatC.Text;
                        arrayData[7] = lbBatch.Text;
                        arrayData[8] = lbMatD.Text;
                        arrayData[9] = txtReserveQty.Text;
                        arrayData[10] = lbUOM.Text;
                        arrayData[11] = lbEgatContractNo.Text;
                        arrayData[12] = lbWBS.Text;
                        arrayData[13] = lbWBSElement.Text; 
                        arrayData[14] = lbSubStation.Text;
                        arrayData[15] = toJobNo;
                        arrayData[16] = toSubstation;
                        arrayData[17] = toStorageLocation;
                        arrayData[18] = toStorageLocationName; 
                        arrayData[19] = toBidNo;
                        arrayData[20] = toScheduleName;
                        arrayData[21] = toEqCode;
                        arrayData[22] = Status;
                        arrayData[23] = "";

                        listData.Add(arrayData);

                    }

                }
            }



            dt = createDataTableStock2(listData);


            return dt;
        }



        protected void btnSubmit_Click(object sender, EventArgs e)
        {

            wf_ps_stock_details wf_ps_stock_details = new wf_ps_stock_details();
            List<wf_ps_stock_details> list_wf_ps_stock_details = new List<wf_ps_stock_details>();
            bool check = false;
            foreach (GridViewRow row in gvStock2.Rows)
            {

                if (row.RowType == DataControlRowType.DataRow)
                {
                    wf_ps_stock_details = new wf_ps_stock_details();
                    Label lbRowID = (row.Cells[1].FindControl("lbRowID") as Label);
                    if (!lbRowID.Text.Trim().Equals(""))
                    {
                        wf_ps_stock_details.rowid = Convert.ToInt32(lbRowID.Text);
                        wf_ps_stock_details.item_status = "Submitted";
                        list_wf_ps_stock_details.Add(wf_ps_stock_details);
                        check = true;
                    }
                }
            }
            if (check)
            {
                string reqNo = stock.getSRNo();
                string strMemoNodeId = UploadMemoToCs();
                stock.update_wf_ps_stock_request_status(hidStockReqID.Value, reqNo, strMemoNodeId);
                stock.update_wf_ps_stock_details_status(list_wf_ps_stock_details, reqNo);
                // add by Ging 

                clsWfStock objWfStock = new clsWfStock();
                objWfStock.StartWf(reqNo, hdnReqDept.Value);
                // add by Ging 

                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('The request was sent successfully.');", true);
            }
        }

        private bool insert_wf_ps_stock_request()
        {
            bool status = false;
            string result = "";
            List<int> listID = new List<int>();

            EmpInfoClass empinfo = new EmpInfoClass();
            var req = empinfo.getInFoByEmpID(hidLogin.Value);
             
            wf_ps_stock_request wf_ps_stock_request = new wf_ps_stock_request();

            wf_ps_stock_request.process_id = PID;
            wf_ps_stock_request.stock_reqno = "";
            wf_ps_stock_request.stock_subject = txtSubject.Text;
            wf_ps_stock_request.project_id = "";
            wf_ps_stock_request.reference = "";
            wf_ps_stock_request.note = txtContent.Text;

            wf_ps_stock_request.requestor_id = hidLogin.Value;
            wf_ps_stock_request.requestor_name = req.SNAME;
            wf_ps_stock_request.requestor_position = req.MC_SHORT;
            wf_ps_stock_request.requestor_position_full = req.POSITION;
            // wf_ps_stock_request.requestor_submitdate = "";
            wf_ps_stock_request.requestor_status = "";

            wf_ps_stock_request.reviewer_id = req.SECTION_HEAD_ID;
            wf_ps_stock_request.reviewer_name = req.SECTION_HEAD_NAME;
            wf_ps_stock_request.reviewer_position = req.SECTION_HEAD_POSITION;
            wf_ps_stock_request.reviewer_position_full = req.SECTION_HEAD_POSITION_FULL;
            // wf_ps_stock_request.reviewer_apvdate = "";
            wf_ps_stock_request.reviewer_status = "";

            wf_ps_stock_request.approver_id = req.AVP_ID;
            wf_ps_stock_request.approver_name = req.AVP_NAME;
            wf_ps_stock_request.approver_position = req.AVP_POSITION;
            wf_ps_stock_request.approver_position_full = req.AVP_POSITION_FULL;
            //wf_ps_stock_request.approver_apvdate = "";
            wf_ps_stock_request.approver_status = "";

            wf_ps_stock_request.pm_id = req.MANAGER_ID;
            wf_ps_stock_request.pm_name = req.MANAGER_NAME;
            wf_ps_stock_request.pm_position = req.MANAGER_POSITION;
            wf_ps_stock_request.pm_position_full = req.MANAGER_POSITION_FULL;
            //wf_ps_stock_request.pm_apvdate = "";
            wf_ps_stock_request.pm_status = "";

            wf_ps_stock_request.assistdirector_id = req.VP_ID;
            wf_ps_stock_request.assistdirector_name = req.VP_NAME;
            wf_ps_stock_request.assistdirector_position = req.VP_POSITION;
            wf_ps_stock_request.assistdirector_position_full = req.VP_POSITION_FULL;
            //wf_ps_stock_request.assistdirector_apvdate = "";
            wf_ps_stock_request.assistdirector_status = "";

            wf_ps_stock_request.director_id = req.DIRECTOR_ID;
            wf_ps_stock_request.director_name = req.DIRECTOR_NAME;
            wf_ps_stock_request.director_position = req.DIRECTOR_POSITION;
            wf_ps_stock_request.director_position_full = req.DIRECTOR_POSITION_FULL;
            //wf_ps_stock_request.director_apvdate = "";
            wf_ps_stock_request.director_status = "new";

            wf_ps_stock_request.form_status = "";
            wf_ps_stock_request.form_to = "";
            wf_ps_stock_request.form_cc = "";
            wf_ps_stock_request.created_by = txtReqBy.Text;
            //wf_ps_stock_request.created_datetime = "";
            wf_ps_stock_request.updated_by = txtReqBy.Text;
            //wf_ps_stock_request.updated_datetime = "";
            wf_ps_stock_request.wf_processid = WFID;
            wf_ps_stock_request.wf_subprocessid = "";
            wf_ps_stock_request.wf_taskid = "";

            listID = stock.insert_wf_ps_stock_request(wf_ps_stock_request);
            hidStockReqID.Value = listID[0].ToString();
            if (listID.Count > 0)
            {
                status = true;
            }
            return status;
        }

        private bool insert_wf_ps_stock_details(DataTable dt)
        {
            bool status = false;
            string result = "";
            wf_ps_stock_details wf_ps_stock_details = new wf_ps_stock_details();
            List<wf_ps_stock_details> list_wf_ps_stock_details = new List<wf_ps_stock_details>();
            List<wf_ps_stock_details> list_wf_ps_stock_details_insert = new List<wf_ps_stock_details>();
            List<wf_ps_stock_details> list_wf_ps_stock_details_update = new List<wf_ps_stock_details>();
            listIDChild = new List<int>();
            foreach (DataRow dr in dt.Rows)
            {
                wf_ps_stock_details = new wf_ps_stock_details();

                //Label lbEqCode = (Label)row.FindControl("lbEqCode");
                //Label lbMatC = (Label)row.FindControl("lbMatC");
                //Label lbqty = (Label)row.FindControl("lbqty");
                //Label lbStatus = (Label)row.FindControl("lbStatus"); 

                if (dr["rowid"].ToString().Equals(""))
                {

                    wf_ps_stock_details.process_id = PID;
                    // wf_ps_stock_details.stock_reqno = "999";
                    
                    wf_ps_stock_details.bid_no = txtRefBid.Text;
                    //wf_ps_stock_details.bid_revision = 12;
                    wf_ps_stock_details.equipment_code = dr["code_old"].ToString();
                    wf_ps_stock_details.material_code = dr["material_code"].ToString();
                    wf_ps_stock_details.plant = dr["plant_code"].ToString();
                    wf_ps_stock_details.stge_loc = dr["STORAGELOCATION_CODE"].ToString();
                    wf_ps_stock_details.batch = dr["batch_number"].ToString();
                    wf_ps_stock_details.reserve_qty = Convert.ToDecimal(dr["QTY"].ToString());
                    wf_ps_stock_details.confirm_qty = 0;
                    wf_ps_stock_details.uom = dr["unit_code"].ToString();
                    wf_ps_stock_details.item_status = dr["Status"].ToString();
                    wf_ps_stock_details.wbs = dr["wbs_code"].ToString();
                    wf_ps_stock_details.to_job_no = dr["toJobNo"].ToString();
                    wf_ps_stock_details.to_substation = dr["toSubstation"].ToString();
                    wf_ps_stock_details.to_storageLocation = dr["toStorageLocation"].ToString();
                    wf_ps_stock_details.to_bid_no = "";
                    wf_ps_stock_details.to_schedule_name = txtRefBidSch.Text;
                    wf_ps_stock_details.created_by = txtReqBy.Text;
                    wf_ps_stock_details.updated_by = txtReqBy.Text;
                    // wf_ps_stock_details.document_nodeid = 12;
                    if (!normalMode)
                    {
                        wf_ps_stock_details.job_revision = 12;
                        wf_ps_stock_details.bid_revision = 12;
                        wf_ps_stock_details.document_nodeid = 12;
                    }

                    list_wf_ps_stock_details.Add(wf_ps_stock_details);
                }
                else
                {
                    wf_ps_stock_details.rowid = Convert.ToInt32(dr["rowid"].ToString());
                    wf_ps_stock_details.reserve_qty = Convert.ToDecimal(dr["QTY"].ToString());
                    list_wf_ps_stock_details.Add(wf_ps_stock_details);
                }

            }

            foreach (var item in list_wf_ps_stock_details)
            {
                //int id = 0;
                // DataTable dtdetail = stock.checkDataDetails(item.process_id, item.equipment_code, item.material_code, item.batch);

                if (item.rowid != 0)
                {
                    //item.rowid = Convert.ToInt32(dtdetail.Rows[0]["rowid"].ToString());
                    //item.reserve_qty =  item.reserve_qty;
                    list_wf_ps_stock_details_update.Add(item);
                }
                else
                {
                    list_wf_ps_stock_details_insert.Add(item);
                }
            }

            //insert
            if (list_wf_ps_stock_details_insert.Count > 0)
            {
                listIDChild = stock.insert_wf_ps_stock_details(list_wf_ps_stock_details_insert);
            }

            //update
            if (list_wf_ps_stock_details_update.Count > 0)
            {
                stock.update_wf_ps_stock_details_reserve(list_wf_ps_stock_details_update);

                foreach (GridViewRow row in gvStock2.Rows)
                {
                    if (row.RowType == DataControlRowType.DataRow)
                    {
                        Label lbRowID = (row.Cells[0].FindControl("lbRowID") as Label);
                        Label lbqty = (row.Cells[0].FindControl("lbqty") as Label);
                        foreach (var item in list_wf_ps_stock_details_update)
                        {
                            if (lbRowID.Text.Equals(item.rowid.ToString()))
                            {
                                lbqty.Text = item.reserve_qty.ToString();
                            }
                        }

                    }
                }



            }
            if (listIDChild.Count > 0)
            {
                status = true;
            }
            return status;

        }



        protected void gvStock_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {
                //CheckBox cbAll = (CheckBox)e.Row.FindControl("CbAll");

                //cbAll.Attributes.Add("onclick", "javascript:chkAll(" + cbAll.ClientID + ");");

              

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //if (e.Row.RowIndex == 0)
                //    e.Row.Style.Add("height", "50px");

                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lblastgrdate = (Label)e.Row.FindControl("lblastgrdate");
                Label lblidate = (Label)e.Row.FindControl("lblidate");

                Label lbEqCode = (Label)e.Row.FindControl("lbEqCode");
                Label lbMatC = (Label)e.Row.FindControl("lbMatC");
                Label lbBatch = (Label)e.Row.FindControl("lbBatch");

                Label lbAgeERP = (Label)e.Row.FindControl("lbAgeERP");
                if (lblastgrdate.Text.Length > 0)
                {
                    string dateERP = stock.calAge(lblastgrdate.Text);
                    lbAgeERP.Text = dateERP;
                }

                Label lbAgeLI = (Label)e.Row.FindControl("lbAgeLI");
                if (lblidate.Text.Length > 0)
                {
                    string dateLI = stock.calAge(lblidate.Text);
                    lbAgeLI.Text = dateLI;
                }


                Decimal countRevenue = stock.checkRevenue(lbEqCode.Text, lbMatC.Text, lbBatch.Text);
                Label lbSOH = (Label)e.Row.FindControl("lbSOH");
                decimal countRev = 0;
                if (lbSOH.Text.Length > 0)
                {
                    countRev = Convert.ToDecimal(lbSOH.Text) - countRevenue;
                }
                Label lbRevenue = (Label)e.Row.FindControl("lbRevenue");

                lbRevenue.Text = countRev.ToString();



                TextBox txtReserveQty = (TextBox)e.Row.FindControl("txtReserveQty");
                if (txtReserveQty.Text.Length > 0)
                {
                    txtReserveQty.Text = checkReserveQty(countRev, Convert.ToInt32(txtReserveQty.Text)).ToString();
                }


                // Label lbhidID = (Label)e.Row.FindControl("lbhidID");
                //lbGruop.Text = drv["depart_position_name"].ToString();
                //lbhidID.Text = drv["positionid"].ToString();

                //string kID = gvEquipment.DataKeys[e.Row.RowIndex].Value.ToString();
                // GridView grdViewDetail = (GridView)e.Row.FindControl("grdViewDetail");




            }
        }


        protected void OnTextChanged(object sender, EventArgs e)
        {
            //Reference the TextBox.
            TextBox textBox = sender as TextBox;

            //Get the ID of the TextBox.
            string id = textBox.ID;

            GridViewRow gvr = (GridViewRow)textBox.Parent.Parent;
            int rowindex = gvr.RowIndex;

            Label lbEqCode = (Label)gvr.FindControl("lbEqCode");
            Label lbMatC = (Label)gvr.FindControl("lbMatC");
            Label lbBatch = (Label)gvr.FindControl("lbBatch");

            Decimal countRevenue = stock.checkRevenue(lbEqCode.Text, lbMatC.Text, lbBatch.Text);

            Label lbSOH = (Label)gvr.FindControl("lbSOH");
            Decimal countRev = Convert.ToDecimal(lbSOH.Text) - countRevenue;
            Label lbRevenue = (Label)gvr.FindControl("lbRevenue");
            lbRevenue.Text = countRev.ToString();

            TextBox txtReserveQty = (TextBox)gvr.FindControl("txtReserveQty");
            if (txtReserveQty.Text.Length > 0)
            {
                //txtReserveQty.Text = stock.checkReserveQty(countRev, Convert.ToInt32(txtReserveQty.Text)).ToString();
                if (!checkReserveQty(countRev, Convert.ToDecimal(txtReserveQty.Text)))
                {
                    // ScriptManager.RegisterStartupScript(this, GetType(), "alertMessage", "alertMessage();", true);
                    //Page.ClientScript.RegisterStartupScript(GetType(), "msgbox", "alert('More');", true);
                    ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('Exceed');", true);
                    txtReserveQty.Text = countRev.ToString();
                }

            }

            // gvStock.DataBind();

        }



        protected void ImageButton_Click(object sender, EventArgs e)
        {
            ImageButton Ib = (ImageButton)sender;
            GridViewRow gvRow = (GridViewRow)Ib.NamingContainer;
            int rowID = gvRow.RowIndex;
            Label lbRowID = (Label)gvRow.FindControl("lbRowID");
            if (Session["Datalist2"] != null)
            {

                DataTable dt = (DataTable)Session["Datalist2"];
                if (dt.Rows.Count > 0)
                {
                    if (gvRow.RowIndex <= dt.Rows.Count - 1)
                    {
                        //Remove the Selected Row data and reset row number  
                        stock.delete_wf_ps_stock_details(Convert.ToInt32(lbRowID.Text));
                        dt.Rows.Remove(dt.Rows[rowID]);

                        // ResetRowID(dt);
                    }
                }

                //Store the current data in session for future reference  
                Session["Datalist2"] = dt;

                //Re bind the GridView for the updated data  
                gvStock2.DataSource = dt;
                gvStock2.DataBind();
                if (!stock.checkRowData(dt))
                {
                    Session["Datalist2"] = null;
                    ini_gvStock2();
                }
                //    if(stock.checkRowData(dt))
                //    {
                //        btnSubmit.Visible = true;
                //    }
                //    else
                //    {
                //        btnSubmit.Visible = false;
                //    }
                //    setDataTableStock();
            }

            //Set Previous Data on Postbacks  
            // SetPreviousData();
        }

        private void ResetRowID(DataTable dt)
        {
            int rowNumber = 1;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow row in dt.Rows)
                {
                    row[0] = rowNumber;
                    rowNumber++;
                }
            }
        }

        public bool checkReserveQty(decimal soh, decimal ReserveQty)
        {
            bool chk = false;

            if (ReserveQty > soh)
            {
                chk = false;
            }
            else
            {
                chk = true;
            }

            return chk;
        }


        protected void btnCancel_Click(object sender, EventArgs e)
        {
            pAdding.Visible = false;
        }

        protected void lkbtnAddReqItem_Click(object sender, EventArgs e)
        {
            pAdding.Visible = true;
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            resetData();
        }

        protected void gvStock2_DataBound(object sender, EventArgs e)
        {
            if (normalMode)
            {
               

                //if(rdoType.SelectedValue=="1")
                //{
                //    this.gvStock2.Columns[12].Visible = true;
                //    this.gvStock2.Columns[13].Visible = false;
                //    this.gvStock2.Columns[14].Visible = false;
                //}
                //if (rdoType.SelectedValue == "2")
                //{
                //    this.gvStock2.Columns[12].Visible = false;
                //    this.gvStock2.Columns[13].Visible = true;
                //    this.gvStock2.Columns[14].Visible = false;
                //}
                //if (rdoType.SelectedValue == "3")
                //{
                //    this.gvStock2.Columns[12].Visible = false;
                //    this.gvStock2.Columns[13].Visible = false;
                //    this.gvStock2.Columns[14].Visible = true;
                //}

                this.gvStock2.Columns[20].Visible = false;
                this.gvStock2.Columns[21].Visible = false;
                this.gvStock2.Columns[22].Visible = false;

            }
            else
            {

                this.gvStock2.Columns[16].Visible = false;
                this.gvStock2.Columns[17].Visible = false;
            }
        }

        protected void btnAddReqItem_Click(object sender, EventArgs e)
        {
            pAdding.Visible = true;
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {

            // replace word
            ReplaceWord();
            // convert to pdf
            // save to cs
            //Response.ContentType = "Application/pdf";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=OutPut.pdf");
            //Response.TransmitFile("C:\\preview\\StockReservationRequestPreviewDocOutPut.docx");
            //Response.End();
        }

        private void ReplaceWord()
        {
            //var egatNo = "S810B4";
            var Title = lbTitle.Text;

            //var subject = tbxSubject.Text;
            var lReqBy = lbReqBy.Text;
            var tReqBy = txtReqBy.Text;
            var lReqNo = lbReqNo.Text;
            var tReqNo = txtReqNo.Text;
            var lReqDate = lbReqDate.Text;
            var tReqDate = txtReqDate.Text;
            var lsub = lbsub.Text;
            var tsub = txtSubject.Text;
            var lContent = lbContent.Text;
            var tContent = txtContent.Text;
            //var contractorRef = "W100295-2387-12/2555-MMRP1";
            string outputName = "";
            string filename = "";
            if (normalMode)
            {
                
                filename = "StockReservationRequestPreviewDoc";
            }
            outputName = filename + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");

            //var templateFile = @"C:\preview\StockReservationRequestPreviewDoc.docx";
            var templateFile = Template_Stock + filename + ".docx";
            var outPutFile = Preview_Stock + outputName + ".docx";

            var outPutFilePdf = Preview_Stock + outputName + ".pdf";
            var outPutPathPdf = Preview_Stock;


            templateFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templateFile);
            outPutFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outPutFile);
            outPutFilePdf = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outPutFilePdf);
            outPutPathPdf = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outPutPathPdf);

            String[] parts = Preview_Stock.Split('\\');

            string srcIfram = parts[parts.Length - 2] + "/" + outputName + ".pdf";


            using (Xceed.Words.NET.DocX myDoc = Xceed.Words.NET.DocX.Load(templateFile))
            {
                //myDoc.ReplaceText("<<EGETNO>>", egatNo); // get from wsa
                myDoc.ReplaceText("<<Title>>", Title); //

                myDoc.ReplaceText("<<lbReqBy>>", lReqBy); //
                myDoc.ReplaceText("<<txtReqBy>>", tReqBy); //
                myDoc.ReplaceText("<<lbReqNo>>", lReqNo); //
                myDoc.ReplaceText("<<txtReqNo>>", tReqNo); //
                myDoc.ReplaceText("<<lbReqDate>>", lReqDate); //
                myDoc.ReplaceText("<<txtReqDate>>", tReqDate); //
                myDoc.ReplaceText("<<lbsub>>", lsub); //
                myDoc.ReplaceText("<<txtSubject>>", tsub); //
                myDoc.ReplaceText("<<lbContent>>", lContent); //
                myDoc.ReplaceText("<<txtContent>>", tContent); //
                myDoc.ReplaceText("<<txtDept>>", hdnReqDept.Value); //
                myDoc.ReplaceText("<<txtDeptName>>", hidDeptAppName.Value); //
                myDoc.ReplaceText("<<txtDeptPosition>>", hidDeptAppPositionFull.Value); //

                var myTable = myDoc.Tables[0];

                DataTable dtAtt = (DataTable)Session["Datalist2"];
                int thisRow = 0;
                if (dtAtt != null)
                {
                    foreach (DataRow dr in dtAtt.Rows)
                    {
                        thisRow++;
                        myTable.InsertRow();
                        myTable.Rows[thisRow].Cells[0].Paragraphs[0].Append(dr["plant_code"].ToString()).Alignment = Alignment.center;
                        myTable.Rows[thisRow].Cells[1].Paragraphs[0].Append(dr["STORAGELOCATION_CODE"].ToString()).Alignment = Alignment.center;
                        myTable.Rows[thisRow].Cells[2].Paragraphs[0].Append(dr["code_old"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[3].Paragraphs[0].Append(dr["materialgroup_code"].ToString()).Alignment = Alignment.left;
                        // myTable.Rows[thisRow].Cells[3].Paragraphs[0].Append(dr["material_code"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[4].Paragraphs[0].Append(dr["batch_number"].ToString()).Alignment = Alignment.center;
                        // myTable.Rows[thisRow].Cells[6].Paragraphs[0].Append(dr["DESCRIPTION"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[5].Paragraphs[0].Append(dr["QTY"].ToString()).Alignment = Alignment.center;
                        myTable.Rows[thisRow].Cells[6].Paragraphs[0].Append(dr["unit_code"].ToString()).Alignment = Alignment.left;
                        //myTable.Rows[thisRow].Cells[7].Paragraphs[0].Append(dr["egatcontractno"].ToString()).Alignment = Alignment.left;
                        //myTable.Rows[thisRow].Cells[8].Paragraphs[0].Append(dr["wbs_code"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[7].Paragraphs[0].Append(dr["toJobNo"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[8].Paragraphs[0].Append(dr["toSubstation"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[9].Paragraphs[0].Append(dr["toStorageLocation"].ToString()).Alignment = Alignment.left;
                        //myTable.Rows[thisRow].Cells[10].Paragraphs[0].Append(dr["Status"].ToString()).Alignment = Alignment.left;

                    }
                }
                myDoc.SaveAs(outPutFile);
                

                var fi1 = new FileInfo(outPutFile);

                //ExportExcelToPdf(new FileInfo("C:\\Xls\\" + FileName + "_BidDrawing" + ".xlsx"), "C:\\Xls", ref strError);
                string strError = "";
                //Spire.Doc.Document document = new Spire.Doc.Document();
                //document.LoadFromFile(outPutFile);
                //document.SaveToFile(outPutFilePdf);
                cPDF pdf = new cPDF();
                pdf.ExportWordToPdf(fi1, outPutPathPdf, ref strError);

                //iframpdf.Src = outPutFilePdf;
                iframpdf.Src = srcIfram;
                WebClient user = new WebClient();
                //Byte[] fileBuffer = user.DownloadData(outPutFilePdf);

                //if(fileBuffer != null)
                //{
                //    Response.ContentType =   "Application/pdf";
                //    Response.AppendHeader("content-length", fileBuffer.Length.ToString());
                //    Response.BinaryWrite(fileBuffer);
                //    Response.Flush();


                //}

                hidDocFile.Value = fi1.ToString();
                hidPdfFile.Value = outPutFilePdf;
                ClientScript.RegisterStartupScript(this.GetType(), "Popup", "ShowPopup('');", true);
               
                //System.IO.File.Delete(fi1.ToString());
            }
        }

        private string UploadMemoToCs()
        {
            //var egatNo = "S810B4";
            string strMemoNodeId = string.Empty;
            var Title = lbTitle.Text;

            //var subject = tbxSubject.Text;
            var lReqBy = lbReqBy.Text;
            var tReqBy = txtReqBy.Text;
            var lReqNo = lbReqNo.Text;
            var tReqNo = txtReqNo.Text;
            var lReqDate = lbReqDate.Text;
            var tReqDate = txtReqDate.Text;
            var lsub = lbsub.Text;
            var tsub = txtSubject.Text;
            var lContent = lbContent.Text;
            var tContent = txtContent.Text;
            //var contractorRef = "W100295-2387-12/2555-MMRP1";
            string outputName = "";
            string filename = "";
            if (normalMode)
            {

                filename = "StockReservationRequestPreviewDoc";
            }
            outputName = filename + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");

            //var templateFile = @"C:\preview\StockReservationRequestPreviewDoc.docx";
            var templateFile = Template_Stock + filename + ".docx";
            var outPutFile = Preview_Stock + outputName + ".docx";

            var outPutFilePdf = Preview_Stock + outputName + ".pdf";
            var outPutPathPdf = Preview_Stock;


            templateFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, templateFile);
            outPutFile = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outPutFile);
            //outPutFilePdf = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outPutFilePdf);
            outPutPathPdf = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, outPutPathPdf);

            String[] parts = Preview_Stock.Split('\\');

            string srcIfram = parts[parts.Length - 2] + "/" + outputName + ".pdf";


            using (Xceed.Words.NET.DocX myDoc = Xceed.Words.NET.DocX.Load(templateFile))
            {
                //myDoc.ReplaceText("<<EGETNO>>", egatNo); // get from wsa
                myDoc.ReplaceText("<<Title>>", Title); //

                myDoc.ReplaceText("<<lbReqBy>>", lReqBy); //
                myDoc.ReplaceText("<<txtReqBy>>", tReqBy); //
                myDoc.ReplaceText("<<lbReqNo>>", lReqNo); //
                myDoc.ReplaceText("<<txtReqNo>>", tReqNo); //
                myDoc.ReplaceText("<<lbReqDate>>", lReqDate); //
                myDoc.ReplaceText("<<txtReqDate>>", tReqDate); //
                myDoc.ReplaceText("<<lbsub>>", lsub); //
                myDoc.ReplaceText("<<txtSubject>>", tsub); //
                myDoc.ReplaceText("<<lbContent>>", lContent); //
                myDoc.ReplaceText("<<txtContent>>", tContent); //
                myDoc.ReplaceText("<<txtDept>>", hdnReqDept.Value); //
                myDoc.ReplaceText("<<txtDeptName>>", hidDeptAppName.Value); //
                myDoc.ReplaceText("<<txtDeptPosition>>", hidDeptAppPositionFull.Value); //
                var myTable = myDoc.Tables[0];

                DataTable dtAtt = (DataTable)Session["Datalist2"];
                int thisRow = 0;
                if (dtAtt != null)
                {
                    foreach (DataRow dr in dtAtt.Rows)
                    {
                        thisRow++;
                        myTable.InsertRow();
                        myTable.Rows[thisRow].Cells[0].Paragraphs[0].Append(dr["plant_code"].ToString()).Alignment = Alignment.center;
                        myTable.Rows[thisRow].Cells[1].Paragraphs[0].Append(dr["STORAGELOCATION_CODE"].ToString()).Alignment = Alignment.center;
                        myTable.Rows[thisRow].Cells[2].Paragraphs[0].Append(dr["code_old"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[3].Paragraphs[0].Append(dr["materialgroup_code"].ToString()).Alignment = Alignment.left;
                        // myTable.Rows[thisRow].Cells[3].Paragraphs[0].Append(dr["material_code"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[4].Paragraphs[0].Append(dr["batch_number"].ToString()).Alignment = Alignment.center;
                        // myTable.Rows[thisRow].Cells[6].Paragraphs[0].Append(dr["DESCRIPTION"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[5].Paragraphs[0].Append(dr["QTY"].ToString()).Alignment = Alignment.center;
                        myTable.Rows[thisRow].Cells[6].Paragraphs[0].Append(dr["unit_code"].ToString()).Alignment = Alignment.left;
                        //myTable.Rows[thisRow].Cells[7].Paragraphs[0].Append(dr["egatcontractno"].ToString()).Alignment = Alignment.left;
                        //myTable.Rows[thisRow].Cells[8].Paragraphs[0].Append(dr["wbs_code"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[7].Paragraphs[0].Append(dr["toJobNo"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[8].Paragraphs[0].Append(dr["toSubstation"].ToString()).Alignment = Alignment.left;
                        myTable.Rows[thisRow].Cells[9].Paragraphs[0].Append(dr["toStorageLocation"].ToString()).Alignment = Alignment.left;
                        //myTable.Rows[thisRow].Cells[10].Paragraphs[0].Append(dr["Status"].ToString()).Alignment = Alignment.left;

                    }
                }
                myDoc.SaveAs(outPutFile);

                var fi1 = new FileInfo(outPutFile);

                //ExportExcelToPdf(new FileInfo("C:\\Xls\\" + FileName + "_BidDrawing" + ".xlsx"), "C:\\Xls", ref strError);
                string strError = "";
                //Spire.Doc.Document document = new Spire.Doc.Document();
                //document.LoadFromFile(outPutFile);
                //document.SaveToFile(outPutFilePdf);
                cPDF pdf = new cPDF();
                // --------------------------------------- new for add memo to cs ------------------------------------------
                string strOutputPdfPath = pdf.ExportWordToPdf(fi1, outPutPathPdf, ref strError);
                try
                {
                   byte [] bytePdfMenmo =   File.ReadAllBytes(strOutputPdfPath);
                    if (bytePdfMenmo != null)
                    {
                        string strDocName = outputName + ".pdf";
                        OTFunctions ot = new OTFunctions();
                        string strParentIdToUpload = ConfigurationManager.AppSettings["wf_att_folder"].ToString();
                        // add file to cs and get node id
                        long nodeID = ot.uploadDoc(strParentIdToUpload, strDocName, strDocName, bytePdfMenmo, strDocName);
                        strMemoNodeId = nodeID.ToString();

                        // delete file  strOutputPdfPath
                        if (File.Exists(strOutputPdfPath))
                        {
                            File.Delete(strOutputPdfPath);
                        }
                    }


                }
                catch (Exception ex)
                {

                }

                // --------------------------------------- new for export------------------------------------------

                return strMemoNodeId;
            }
        }


        protected void rdoType_SelectedIndexChanged(object sender, EventArgs e)
        {
            checkType();
        }

        protected void btnClosePopup_Click(object sender, EventArgs e)
        {
            // delete file
            if(hidDocFile.Value != "")
            {
                System.IO.File.Delete(hidDocFile.Value);
                hidDocFile.Value = "";
            }
            if (hidPdfFile.Value != "")
            {
                System.IO.File.Delete(hidPdfFile.Value);
                hidPdfFile.Value = "";
            }

        }

        protected void gvStock_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvStock.PageIndex = e.NewPageIndex;
            bindDataGridOracle();
        }

        protected void btnCloseRedirect_Click(object sender, EventArgs e)
        {
            //Redirect
            // Check whether the browser remains
            // connected to the server.
            if (Response.IsClientConnected)
            {
                // If still connected, redirect
                // to another page. 
               
                Response.Redirect(ConfigurationManager.AppSettings["url_StockReport"].ToString() + "?zuserlogin=" + hidLogin.Value);
            }
            else
            {
                // If the browser is not connected
                // stop all response processing.
                Response.End();
            }
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void ddlEqu_SelectedIndexChanged(object sender, EventArgs e)
        {
            bindMatGroup();
        }
    }
}