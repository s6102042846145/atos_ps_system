﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="StockReservationRequest.aspx.cs" Inherits="PS_System.form.Stock.StockReservationRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <title></title>
    <style type="text/css">
         /*.FixedHeader {
            position: absolute;*/
            /*font-weight: bold;*/
        /*}*/  
        

        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }


        .ddlstyle {
            width: 200px;
        }

         /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
            transform: translate(0%, 0%);
        }

      

         /*Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width:1200px;
            height:1000px;
            transform: translate(-22%, 0%);
           /* align-content:center;*/
        }

        
        .auto-style1 {
            height: 54px;
        }

        
        .auto-style2 {
            height: 22px;
        }

        
        .auto-style3 {
            height: 27px;
        }

        
        .auto-style9 {
            width: 500px;
            height: 24px;
        }

        
        .auto-style14 {
            height: 27px;
            width: 917px;
        }
                
        
        .auto-style18 {
            height: 27px;
            width: 205px;
        }
        .auto-style22 {
            height: 27px;
            width: 208px;
        }
        .auto-style23 {
            width: 208px;
        }
        .auto-style24 {
            width: 205px;
            height: 24px;
        }
        .auto-style25 {
            width: 205px;
        }
        
        
        .auto-style26 {
            width: 143px;
        }
        
        
        .auto-style27 {
            width: 787px;
        }
        
        
        .auto-style28 {
            width: 59px;
        }
        
        
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <script>

        

        $(function () {


            $(document).ready(function () {
                $('#mtJob2').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                $('#mtSub2').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                $('#mtJobS').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                $('#mtMatGroup').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                $('#mtStorageLo2').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                $('#mtWarehouse').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    numberDisplayed: 3,
                    maxHeight: 300
                });
                
            });
           
            $(document).ready(function () {
                $("input:checkbox").click(function () {
                    if ($(this).is(":checked")) {


                    } else {

                    }
                });
                
            });
            $(document).ready(function () {
                $('#rdoType input').change(function () {
                    // The one that fires the event is always the
                    // checked one; you don't need to test for this
                    if ($(this).val() == "1") {
                    
                    }
                    else if ($(this).val() == "2") {

                    }
                    else if ($(this).val() == "3") {
                       
                    }

                });
            });

            $(document).ready(function () {
                var gridHeader = $('#<%=dvGvstock.ClientID%>').clone(true); // Here Clone Copy of Gridview with style
                 $(gridHeader).find("tr:gt(0)").remove(); // Here remove all rows except first row (header row)
                 $('#<%=dvGvstock.ClientID%> tr th').each(function (i) {
                        // Here Set Width of each th from gridview to new table(clone table) th 
                        $("th:nth-child(" + (i + 1) + ")", gridHeader).css('width', ($(this).width()).toString() + "px");
                    });
                    $("#GHead").append(gridHeader);
                    $('#GHead').css('position', 'absolute');
                 $('#GHead').css('top', $('#<%=dvGvstock.ClientID%>').offset().top);
            });
         
          

        });

       

       


        $(function () {
            $("#btnShowPopup").click(function () {
                var title = "Greetings";
                var body = "Welcome to ASPSnippets.com";

               // $("#MyPopup .modal-title").html(title);
                //$("#MyPopup .modal-body").html(body);
                $("#MyPopup").modal("show");
            });

            $("#btnClosePopup").click(function () {
               
                $("#MyPopup").modal("hide");
            });
        });

        function HodePopUp() {
            $("#MyPopup").modal("hide");

        }

        function ShowPopup() {
            $("#MyPopup").modal("show");
        }

        //Restrict the user to key-in chrectors and other special charectors
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
                return false;
            return true;
        }

       
    </script>
     

</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server">
        </asp:ScriptManager>
   
        <div>
            <table style="width: 100%;"> 
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="lbTitle" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Project Stock Reservation Request"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                </tr>
                <tr>
                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left">
                        <asp:Label ID="lbReqBy" runat="server" Text="Request By"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtReqBy" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox></td>

                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left">
                        <asp:Label ID="lbReqNo" runat="server" Text="Request No"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left">
                        <asp:TextBox ID="txtReqNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox></td>
                </tr>

                <tr>
                    <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: center" class="auto-style1">
                        <asp:Label ID="lbReqDate" runat="server" Text="Request Date"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtReqDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox></td>

                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:Label ID="lbsub" runat="server" Text="Subject"></asp:Label>&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:TextBox ID="txtSubject" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="600px"></asp:TextBox></td>

                </tr>
                <tr>
                    <td class="auto-style1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="text-align: left" class="auto-style1">
                        <asp:Label ID="lbContent" runat="server" Text="Content"></asp:Label>&nbsp;&nbsp;&nbsp; </td>
                    <td style="text-align: left" class="auto-style1" colspan="4">
                        <asp:TextBox ID="txtContent" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="600px" Rows="3" TextMode="MultiLine"></asp:TextBox>

                    </td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
            </table>

            <div style="padding-left: 10px; padding-right: 10px; padding-top: 10px">
                <asp:Panel runat="server" BorderStyle="Solid" ID="pAdding" BackColor="#CCCCCC">
                   

                    <table>
                        <tr>
                           
                            <td style="text-align: left"  colspan="4" class="auto-style2">
                                <asp:Label ID="lbPos12" runat="server" Text="Check Project Stock"></asp:Label></td>
                            
                        </tr>
                         <tr>
                           
                             <td style="text-align: left;column-width:100px;" class="auto-style23"  >
                                <asp:Label ID="Label11" runat="server" Text="WBS Element"></asp:Label>
                                 </td>
                                <td colspan="3" class="auto-style9">
                                 <asp:DropDownList ID="ddlWbsEle" runat="server">
                                     <asp:ListItem Value="0">ทั้งหมด</asp:ListItem>
                                     <asp:ListItem Value="1">Reserve</asp:ListItem>
                                     <asp:ListItem Value="2">Assign</asp:ListItem>
                                 </asp:DropDownList>
                             
                            </td>
                        </tr>
                            <tr>
                           
                             <td style="text-align: left" class="auto-style23" >
                                <asp:Label ID="Label2" runat="server" Text="Stock Value Type"></asp:Label>
                                 </td>
                                <td>

                                 <asp:DropDownList ID="ddlValueType" runat="server">
                                     <asp:ListItem Value="0">ทั้งหมด</asp:ListItem>
                                     <asp:ListItem Value="1"> เคลื่อนไหว &lt;=1 ปี</asp:ListItem>
                                     <asp:ListItem Value="2">ไม่เคลื่อนไหว 1-2 ปี</asp:ListItem>
                                     <asp:ListItem Value="3">ไม่เคลื่อนไหว 2-5 ปี</asp:ListItem>
                                     <asp:ListItem Value="4">ไม่เคลื่อนไหว 5-10 ปี</asp:ListItem>
                                     <asp:ListItem Value="5">ไม่เคลื่อนไหว &gt; 10 ปี</asp:ListItem>
                                 </asp:DropDownList>
                             
                            </td>
                        </tr>
                            <tr>
                           
                             <td style="text-align: left" class="auto-style22" >
                                 <asp:Label ID="Label4" runat="server" Text="คลังพัสดุ"></asp:Label>
                                 </td>
                                <td class="auto-style3">
                                    <select id="mtWarehouse" runat="server" name="D5" style="width: 220px">
                                    </select></td>
                                  <td class="auto-style3">
                                      &nbsp;</td>
                                 <td class="auto-style3">
                                     &nbsp;</td>
                                
                        </tr>
                      <tr>
                            
                             <td style="text-align: left" class="auto-style23" >
                                <asp:Label ID="Label5" runat="server" Text="อุปกรณ์"></asp:Label>
                              </td>
                          <td>
                                 <asp:DropDownList ID="ddlEqu" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEqu_SelectedIndexChanged">
                                     <asp:ListItem Value="0">ทั้งหมด</asp:ListItem>
                                     <asp:ListItem Value="1">Main Equipment</asp:ListItem>
                                     <asp:ListItem Value="2">Auxiliary</asp:ListItem>
                                 </asp:DropDownList>
                             
                            </td>
                        </tr>
                           <tr>
                           
                             <td style="text-align: left" class="auto-style23" >
                                <asp:Label ID="Label6" runat="server" Text="Material Group"></asp:Label>
                              </td>
                               <td>
                                   <select id="mtMatGroup" runat="server" name="D4" style="width: 220px">
                                       
                                   </select></td>
                        </tr>
                         <tr>
                            
                             <td style="text-align: left" class="auto-style23" >
                                <asp:Label ID="Label7" runat="server" Text="Contract No."></asp:Label>
                              </td>
                             <td>
                                  <asp:TextBox ID="txtContract" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="332px" ></asp:TextBox>
                             
                            </td>
                        </tr>
                        <tr>
                            
                             <td style="text-align: left" class="auto-style23">
                                <asp:Label ID="Label8" runat="server" Text="Description"></asp:Label>
                              </td>
                            <td>
                                  <asp:TextBox ID="txtDesS" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="332px" TextMode="MultiLine" ></asp:TextBox>
                             
                            </td>
                        </tr>
                         <tr>
                           
                             <td style="text-align: left" class="auto-style23">
                                <asp:Label ID="Label9" runat="server" Text="Material Code"></asp:Label>
                             </td>
                             <td>
                                  <asp:TextBox ID="txtMatCode" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="332px" ></asp:TextBox>
                             
                            </td>
                        </tr>
                           <tr>
                           
                             <td style="text-align: left" class="auto-style22" >
                                <asp:Label ID="Label10" runat="server" Text="Job No"></asp:Label>
                                </td>
                               <td class="auto-style3">
                                 <select id="mtJobS" runat="server" name="D3" style="width: 220px">
                                 </select></td>
                        </tr>
                        <tr>
                            
                            <td style="text-align: left" colspan="1" class="auto-style23" >
                                <asp:Button ID="btnSearch" runat="server" class="btn btn-primary" OnClick="btnSearch_Click" Text="Search" />
                              </td>
                          <td colspan="2">
                              &nbsp;</td>
                           

                        </tr>
                          </table>
                    <table>
                        <tr>
                          
                            <td style="padding-bottom: 15px;">

                              
                                <div runat="server" id="dvGvstock" visible="false" style="width: 100%; height: 400px; overflow: scroll">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <asp:GridView ID="gvStock" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvStock_RowDataBound" 
                                                AllowPaging="True" OnPageIndexChanging="gvStock_PageIndexChanging" PageSize="20" ShowHeader = "true"  >
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Plant" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("plant_code") %>'></asp:Label>
                                                            <%--Text='<%# Bind("job_no") %>'--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="StorageLoc." Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbStorageLoc" runat="server" Text='<%# Bind("STORAGELOCATION_CODE") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="StorageLoc.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbStorageLocName" runat="server" Text='<%# Bind("STORAGELOCATION_Name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                   
                                                    <asp:TemplateField HeaderText="Equip Code(Old Mat.)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbEqCode" runat="server" Text='<%# Bind("code_old") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Mat.Group">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbMatG" runat="server" Text='<%# Bind("materialgroup_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Mat.Group">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbMatGN" runat="server" Text='<%# Bind("materialgroup_name") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Mat.Code">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbMatC" runat="server" Text='<%# Bind("material_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Batch">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbBatch" runat="server" Text='<%# Bind("batch_number") %>'></asp:Label>
                                                            <%--Text='<%# Bind("schedule_no") %>'--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Full Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbMatD" runat="server" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="SOH">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbSOH" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="50px" HorizontalAlign="center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="UOM">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbUOM" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Value">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbValue" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="EgatContractNo">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbEgatContractNo" runat="server" Text='<%# Bind("egatcontractno") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="WBS">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbWBS" runat="server" Text='<%# Bind("wbs_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reserve/Assing" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbWBSElement" runat="server" Text='<%# Bind("WBSElement") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Substation">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbSubStation" runat="server" Text='<%# Bind("Substation") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="lastgrdate" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblastgrdate" runat="server" Text='<%# Bind("lastgrdate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Age ERP (Year/Month)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbAgeERP" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="lidate" Visible="false">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblidate" runat="server" Text='<%# Bind("lidate") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Age L/I (Year/Month)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbAgeLI" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Revenue">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbRevenue" runat="server"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Reserve Qty">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtReserveQty" runat="server" Text="" OnTextChanged="OnTextChanged" AutoPostBack="true" onkeypress="return onlyNumbers(this);"></asp:TextBox>

                                                        </ItemTemplate>
                                                        <ItemStyle Width="50px" HorizontalAlign="Right" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                </Columns>

                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                   
                            </td>
                        </tr>
                        </table>
                         <div runat="server" id="divSP">
                        <table>
                            <tr>
                                <td class="auto-style25"></td>
                            </tr>
                            <tr>
                                <td style="text-align: left" class="auto-style24">
                                    <asp:Label ID="lbForJob" runat="server" Text="For Job **"></asp:Label>
                                    </td>
                                <td>
                        <asp:TextBox ID="txtJob" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="150px"></asp:TextBox>
                        </td>
                                <td class="auto-style28">
                                <asp:Label ID="lbRefBid" runat="server" Text="Ref. Bid"></asp:Label>
                          </td>
                                <td>
                        <asp:TextBox ID="txtRefBid" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="150px"></asp:TextBox>
                                   </td>
                                  <td class="auto-style26">
                        <asp:Label ID="lbRefBidSch" runat="server" Text="Ref. Bid Schedule"></asp:Label>
                       </td>
                                <td class="auto-style27">
                        <asp:TextBox ID="txtRefBidSch" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="150px"></asp:TextBox>
                                </td>

                            </tr>

                            <tr>
                                <td style="text-align: left" class="auto-style24">
                                    <asp:Label ID="lbEqmCode" runat="server" Text="For Equipement Code"></asp:Label>
                                    </td>
                                <td>
                                    <asp:TextBox ID="txtEqCode" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" Width="150px"></asp:TextBox>
                                </td>


                            </tr>
                            <tr>
                                <td style="text-align: left" class="auto-style18">
                                    <asp:Label ID="lbDes" runat="server" Text="Description"></asp:Label>

                                    </td>
                                <td colspan="5">
                                    <asp:TextBox ID="txtDesc" runat="server" BorderColor="#D8D8D8" BorderStyle="Solid" ReadOnly="true" Rows="3" TextMode="MultiLine" Width="52%" Height="74px"></asp:TextBox>
                                </td>


                            </tr>



                        </table>
                    </div>
                    
                     
                    <div runat="server" id="divUser">
                        
                       

                        <div>
                            <table>

                            <tr>
                                <td style="text-align: left" class="auto-style14"> 
                                  
                                <asp:Label ID="lbType" runat="server" Text="Type "></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:RadioButtonList ID="rdoType"  runat="server" AutoPostBack="true" OnSelectedIndexChanged="rdoType_SelectedIndexChanged" >
                                        <asp:ListItem Selected="true" Value="1">Job</asp:ListItem>
                                        <asp:ListItem Value="2">Substation</asp:ListItem>
                                        <asp:ListItem Value="3">Reassign ลง Job และ Subtation</asp:ListItem>
                                    </asp:RadioButtonList>
                                  </td>
                                
                            </tr>
                         </table>
                        </div>

                        <div runat="server" id="divForjob2">

                        <table>

                            <tr>
                               
                                <td style="text-align: left" class="auto-style24"> 
                                    <asp:Label ID="lbForJob2" runat="server" Text="Reassign To Job No. **"></asp:Label>
                                    </td>
                                <td>
                                    <select id="mtJob2" runat="server"  name="D1" style="width: 220px">
                                    </select>

                                    </td>
                                
                            </tr>
                         </table>
                            </div>

                         <div runat="server" id="divSub2">
                         <table>
                            <tr>
                               
                                <td style="text-align: left" class="auto-style24">
                                    <asp:Label ID="lbSubstation" runat="server" Text="To Substation**"></asp:Label>
                                    </td>
                                <td>
                                    <select id="mtSub2" runat="server" class="multiselect" multiple="false"  name="D2" style="width: 220px">
                                    </select></td>

                            </tr>
                              </table>
                            </div>

                        <div runat="server" id="divStoLo2">
                        <table>
                            <tr>
                                
                                <td style="text-align: left" class="auto-style24">
                                    <asp:Label ID="lbStorageLocation" runat="server" Text="To Storage Location**"></asp:Label></td>
                                <td>
                                    <select id="mtStorageLo2" runat="server" class="multiselect" multiple="false"  name="D2" style="width: 220px">
                                    </select></td>

                            </tr>

                        </table>
                    </div>
                        <table>
                        </table>
                        </div>
                        


                    <div>
                        
                        <table>
                        <tr>
                            
                            <td class="auto-style2">
                                <div class="float-center">

                                    <asp:Button ID="btnAdd" class="btn btn-primary" Style="width: 100px;" runat="server" Text="Add" OnClick="btnAdd_Click" Width="100px" />

                                    &nbsp;&nbsp;
                           <asp:Button ID="btnReset" class="btn btn-primary" Style="width: 100px;" runat="server" Text="Reset" Width="100px" OnClick="btnReset_Click" />
                                    &nbsp;
                           <asp:Button ID="btnCancel" CssClass="w3-button w3-red" Style="width: 100px;" runat="server" Text="Cancel" Width="100px" OnClick="btnCancel_Click" />

                                </div>
                            </td>
                        </tr>
                    </table>
                    </div>
                </asp:Panel>
            </div>
        </div>
        <div style="padding-left: 10px; padding-right: 10px;">
            <div>

                <asp:Button ID="btnAddReqItem" class="btn btn-primary" runat="server" Text="Add Request Item" Width="290px" OnClick="btnAddReqItem_Click" />


                <br />
                <table>

                    <asp:GridView ID="gvStock2" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnDataBound="gvStock2_DataBound">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                        <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        <Columns>

                            <asp:TemplateField HeaderText="ID"  Visible="false" >
                                <ItemTemplate>
                                    <asp:Label ID="lbRowID" runat="server" Text='<%# Bind("rowid") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Plant" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("plant_code") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="StorageLoc." Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbStorageLoc" runat="server" Text='<%# Bind("STORAGELOCATION_CODE") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="StorageLoc.">
                                  <ItemTemplate>
                                    <asp:Label ID="lbStorageLocName" runat="server" Text='<%# Bind("STORAGELOCATION_Name") %>'></asp:Label>
                                   </ItemTemplate>
                                  <ItemStyle Width="80px" HorizontalAlign="Center" VerticalAlign="Top" />
                              </asp:TemplateField>
                            <asp:TemplateField HeaderText="Equip Code(Old Mat.)">
                                <ItemTemplate>
                                    <asp:Label ID="lbEqCode" runat="server" Text='<%# Bind("code_old") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mat.Group" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbMatG" runat="server" Text='<%# Bind("materialgroup_code") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mat.Group">
                                  <ItemTemplate>
                                       <asp:Label ID="lbMatGN" runat="server" Text='<%# Bind("materialgroup_name") %>'></asp:Label>
                                   </ItemTemplate>
                                 <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                             </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mat.Code">
                                <ItemTemplate>
                                    <asp:Label ID="lbMatC" runat="server" Text='<%# Bind("material_code") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Batch">
                                <ItemTemplate>
                                    <asp:Label ID="lbBatch" runat="server" Text='<%# Bind("batch_number") %>'></asp:Label>
                                    <%--Text='<%# Bind("schedule_no") %>'--%>
                                </ItemTemplate>
                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Full Description">
                                <ItemTemplate>
                                    <asp:Label ID="lbMatD" runat="server" Text='<%# Bind("DESCRIPTION") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Req.Qty">
                                <ItemTemplate>
                                    <asp:Label ID="lbqty" runat="server" Text='<%# Bind("QTY") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="UOM">
                                <ItemTemplate>
                                    <asp:Label ID="lbUOM" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="EgatContractNo">
                                <ItemTemplate>
                                    <asp:Label ID="lbEgatContractNo" runat="server" Text='<%# Bind("egatcontractno") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="WBS">
                                <ItemTemplate>
                                    <asp:Label ID="lbWBS" runat="server" Text='<%# Bind("wbs_code") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                          <asp:TemplateField HeaderText="Reserve/Assing" Visible="false">
                               <ItemTemplate>
                                <asp:Label ID="lbWBSElement" runat="server" Text='<%# Bind("WBSElement") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                              <asp:TemplateField HeaderText="Substation">
                                  <ItemTemplate>
                                     <asp:Label ID="lbSubStation" runat="server" Text='<%# Bind("Substation") %>'></asp:Label>
                                         </ItemTemplate>
                                  <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Job No">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoJobNo" runat="server" Text='<%# Bind("toJobNo") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="To Substation">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoSubstation" runat="server" Text='<%# Bind("toSubstation") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="To Storage Location" Visible="false">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoStorageLoc" runat="server" Text='<%# Bind("toStorageLocation") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>
                            
                            <asp:TemplateField HeaderText="To Storage Location">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoStorageLocName" runat="server" Text='<%# Bind("toStorageLocationName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="To Bid No">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoBidNo" runat="server" Text='<%# Bind("toBidNo") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="To Schedule Name">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoScheduleName" runat="server" Text='<%# Bind("toScheduleName") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="To Equipment Code">
                                <ItemTemplate>
                                    <asp:Label ID="lbtoEqCode" runat="server" Text='<%# Bind("toEqCode") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Status">
                                <ItemTemplate>
                                    <asp:Label ID="lbStatus" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                                </ItemTemplate>
                                <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                            </asp:TemplateField>

                            <asp:TemplateField>
                                <ItemTemplate>

                                    <asp:ImageButton ID="ImageButton1" runat="server" OnClick="ImageButton_Click" ImageAlign="Right" ImageUrl="~/images/bin.png" Height="20px" Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="25px" HorizontalAlign="center" VerticalAlign="Top" />
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>


                </table>
            </div>
            <table>
                <tr>
                    <td>&nbsp;&nbsp;
                    </td>
                </tr>
                <tr>

                    <td colspan="5">
                        <div class="float-center">
                            &nbsp;<asp:Button ID="btnSubmit0" Visible="false" class="btn btn-primary" runat="server" Text="Save" Width="110px" />


                            &nbsp;<asp:Button ID="btnSubmit" class="btn btn-primary" runat="server" Text="Submit" OnClick="btnSubmit_Click" Width="110px" />


                        &nbsp;<asp:Button ID="btnPreview" class="btn btn-primary" runat="server" Text="Preview" OnClick="btnPreview_Click" Width="110px" />

                             &nbsp;<asp:Button ID="btnCloseRedirect" CssClass="w3-button w3-red" Style="width: 100px;" runat="server" Text="Close" Width="100px" OnClick="btnCloseRedirect_Click" />

                                </div>
       
                    </td>
                </tr>
            </table>



        </div>

                                    <!-- Modal Popup -->
<div id="MyPopup" class="modal" role="dialog"  >
    <div class="modal-dialog" >
        <!-- Modal content-->
        <div class="modal-content"  >
            <div class="modal-header">
                <%--<emptydatatemplate>No data found</emptydatatemplate>--%><%--Text='<%# Bind("job_no") %>'--%>
                 <asp:Button ID="btnClose"  CssClass="close"  runat="server" Text="x" value="Close" OnClientClick="" OnClick="btnClosePopup_Click"/>
                <h4 class="modal-title">
                </h4>
            </div>
            <div class="modal-body" >
                <iframe id="iframpdf" runat="server"  style="width:1150px;height:900px;"> </iframe>
            </div>
            <div class="modal-footer">
                <%--Text='<%# Bind("schedule_no") %>'--%>
                <asp:Button ID="btnClosePopup"  CssClass="btn btn-danger" runat="server" Text="Close" value="Close" OnClientClick="" OnClick="btnClosePopup_Click"/>
            </div>
        </div>
    </div>
</div>



        <asp:HiddenField ID="hidLogin" runat="server" />



        <asp:HiddenField ID="hidMode" runat="server" />



        <asp:HiddenField ID="hidWorkID" runat="server" />



        <asp:HiddenField ID="hidSubworkID" runat="server" />



        <asp:HiddenField ID="hidDocFile" runat="server" />
        <asp:HiddenField ID="hidPdfFile" runat="server" />



        <br />



        <asp:HiddenField ID="hdnReqDept" runat="server" />
        <asp:HiddenField ID="hidApproverID" runat="server" />
        <asp:HiddenField ID="hidEqCode" runat="server" />
        <asp:HiddenField ID="hidType" runat="server" />
        <asp:HiddenField ID="hidStockReqID" runat="server" />
        <asp:HiddenField ID="hidDeptAppId" runat="server" />
        <asp:HiddenField ID="hidDeptAppName" runat="server" />
        <asp:HiddenField ID="hidDeptAppPosition" runat="server" />
        <asp:HiddenField ID="hidDeptAppPositionFull" runat="server" />



    </form>
</body>
</html>


