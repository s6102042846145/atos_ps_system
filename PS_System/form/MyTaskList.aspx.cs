﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using PS_Library;
using System.Configuration;

namespace PS_System.form
{
    public partial class MyTaskList : System.Web.UI.Page
    {
        clsMyTaskList objMytask = new clsMyTaskList();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z423610";

                if (Request.QueryString["zmode"] != null)
                    this.hidMode.Value = Request.QueryString["zmode"];
                else
                    this.hidMode.Value = "";//JB
                bind_data();
                bind_DDL();
            }
        }
        protected void bind_data()
        {
            if (hidMode.Value == "J")
            {
                dvBid.Style["display"] = "none";
                DataTable dt = objMytask.getDisTask(hidLogin.Value,"job");
                gvMyTask.DataSource = dt;
                gvMyTask.DataBind();
            }
            else if (hidMode.Value == "B")
            {
                dvJob.Style["display"] = "none";
                DataTable dt = objMytask.getDisTask(hidLogin.Value, "bid");
                gvMyTaskBid.DataSource = dt;
                gvMyTaskBid.DataBind();
            }
            else
            {
                dvBid.Style["display"] = "none";
                dvJob.Style["display"] = "none";
            }
      

        }
        protected void bind_DDL()
        {
            if (hidMode.Value == "J")
            {
                DataTable dt = objMytask.getDDL(hidLogin.Value, "job");

                Multi_jobno.Items.Clear();
                Multi_jobno.DataSource = dt;
                Multi_jobno.DataTextField = "job_no";
                Multi_jobno.DataValueField = "job_no";
                Multi_jobno.DataBind();
            }
            else if (hidMode.Value == "B")
            {
                DataTable dt = objMytask.getDDL(hidLogin.Value, "bid");

                Multi_bidno.Items.Clear();
                Multi_bidno.DataSource = dt;
                Multi_bidno.DataTextField = "bid_no";
                Multi_bidno.DataValueField = "bid_no";
                Multi_bidno.DataBind();
            }
             

            //dt = objMytask.getDDLtaskname(hidLogin.Value);
            //Multi_Task.Items.Clear();
            //Multi_Task.DataSource = dt;
            //Multi_Task.DataTextField = "p6_task_name";
            //Multi_Task.DataValueField = "p6_task_name";
            //Multi_Task.DataBind();
            default_Multisel();
        }
        protected void default_Multisel()
        {
            if (hidMode.Value == "J")
            {
                for (int i = 0; i < Multi_jobno.Items.Count; i++)
                {
                    Multi_jobno.Items[i].Selected = true;
                }
            }
            else if (hidMode.Value == "B")
            {
                for (int i = 0; i < Multi_bidno.Items.Count; i++)
                {
                    Multi_bidno.Items[i].Selected = true;
                }
            }

        }
        protected void btnsearch_Click(object sender, EventArgs e)
        {
            string filter = "";
            string jobno = "";
            hidFilter.Value = "";
            foreach (ListItem item in Multi_jobno.Items)
            {
                if (item.Selected)
                {
                    if (jobno == "")
                    {
                        jobno = item.Value;
                    }
                    else
                    {
                        jobno += "," + item.Value;
                    }
                }
            }
            if (jobno != "")
            {
                filter = string.Format("job_no in ('" + jobno.Replace(",", "','") + "')");
            }
            //string taskname = "";
            //foreach (ListItem item in Multi_Task.Items)
            //{
            //    if (item.Selected)
            //    {
            //        if (taskname == "")
            //        {
            //            taskname = item.Value;
            //        }
            //        else
            //        {
            //            taskname += "," + item.Value;
            //        }
            //    }
            //}
            //if (taskname != "")
            //{
            //    string task_name = "";
            //    task_name = string.Format("p6_task_name in ('" + taskname.Replace(",", "','") + "')");
            //    filter = string.Format("({0} or {1})", filter, task_name);
            //}
            if (dateStart.Value != "")
            {
                string start_date = "";
                //DateTime dtt = Convert.ToDateTime(dateStart.Value);
                //string dtStart = dtt.ToString("dd/MM/yyyy");
                start_date = string.Format("p6_plan_start >= '{0}'", dateStart.Value);
                if (hidFilter.Value != "") hidFilter.Value = string.Format("{0} and ({1})", hidFilter.Value, start_date);
                else hidFilter.Value = string.Format(start_date);
            }
            if (dateEnd.Value != "")
            {
                string end_date = "";
                end_date = string.Format("p6_plan_end <= '{0}'", dateEnd.Value);
                if (hidFilter.Value != "") hidFilter.Value = string.Format("{0} and ({1})", hidFilter.Value, end_date);
                else hidFilter.Value = string.Format(end_date);
            }
            DataTable dt = objMytask.getDisTask(hidLogin.Value,"job");
            DataView dv = new DataView(dt);
            dv.RowFilter = filter;
            if (dv.Count > 0)
            {
                gvMyTask.DataSource = dv;
                gvMyTask.DataBind();
            }
            else
            {
                gvMyTask.DataSource = null;
                gvMyTask.DataBind();
            }

        }

        protected void gvMyTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton lnkGoto = (LinkButton)e.Row.FindControl("lnkGoto");
                lnkGoto.Text = "<a href = '" + ConfigurationManager.AppSettings["url_Jobdash"].ToString() + "?jobno=" + drv["job_no"].ToString() + "&zuserlogin=" + hidLogin.Value + "' TARGET='openWindow' > Open </ a >";

                GridView gv = (GridView)e.Row.FindControl("gvSubTask");
                DataTable dt = objMytask.getTaskAssign(hidLogin.Value, "job");
                DataView dv = new DataView(dt);
                if (hidFilter.Value != "")
                {
                    dv.RowFilter = "job_no ='" + drv["job_no"].ToString() + "' and " + hidFilter.Value;
                    gv.DataSource = dv;// ds;
                    gv.DataBind();
                }
                else
                {
                    dv.RowFilter = "job_no ='" + drv["job_no"].ToString() + "'";
                    gv.DataSource = dv;// ds;
                    gv.DataBind();
                }
            }
        }
        protected void gvSubTask_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //LinkButton lnkGoto = (LinkButton)e.Row.FindControl("lnkGoto");
                //lnkGoto.Text = "<a href = '" + ConfigurationManager.AppSettings["url_Jobdash"].ToString() + "?jobno=" + drv["job_no"].ToString() + "&zuserlogin=" + hidLogin.Value + "' TARGET='openWindow' > Open </ a >";


            }
        }

        protected void btnSearchbid_Click(object sender, EventArgs e)
        {
            string filter = "";
            string bidno = "";
            hidFilter.Value = "";
            foreach (ListItem item in Multi_bidno.Items)
            {
                if (item.Selected)
                {
                    if (bidno == "")
                    {
                        bidno = item.Value;
                    }
                    else
                    {
                        bidno += "," + item.Value;
                    }
                }
            }
            if (bidno != "")
            {
                filter = string.Format("bid_no in ('" + bidno.Replace(",", "','") + "')");
            }
          
            if (dateStbid.Value != "")
            {
                string start_date = "";
                start_date = string.Format("p6_plan_start >= '{0}'", dateStbid.Value);
                if (hidFilter.Value != "") hidFilter.Value = string.Format("{0} and ({1})", hidFilter.Value, start_date);
                else hidFilter.Value = string.Format(start_date);
            }
            if (dateEndbid.Value != "")
            {
                string end_date = "";
                end_date = string.Format("p6_plan_end <= '{0}'", dateEndbid.Value);
                if (hidFilter.Value != "") hidFilter.Value = string.Format("{0} and ({1})", hidFilter.Value, end_date);
                else hidFilter.Value = string.Format(end_date);
            }
            DataTable dt = objMytask.getDisTask(hidLogin.Value, "bid");
            DataView dv = new DataView(dt);
            dv.RowFilter = filter;
            if (dv.Count > 0)
            {
                gvMyTaskBid.DataSource = dv;
                gvMyTaskBid.DataBind();
            }
            else
            {
                gvMyTaskBid.DataSource = null;
                gvMyTaskBid.DataBind();
            }

        }

        protected void gvMyTaskBid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DataRowView drv = (DataRowView)e.Row.DataItem;
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton lnkGotoBid = (LinkButton)e.Row.FindControl("lnkGotoBid");
                lnkGotoBid.Text = "<a href = '" + ConfigurationManager.AppSettings["url_Biddash"].ToString() + "?bidno=" + drv["bid_no"].ToString() + "&zuserlogin=" + hidLogin.Value + "' TARGET='openWindow' > Open </ a >";
                GridView gv = (GridView)e.Row.FindControl("gvSubTaskBid");
                DataTable dt = objMytask.getTaskAssign(hidLogin.Value, "bid");
                DataView dv = new DataView(dt);

                if (hidFilter.Value != "")
                {
                    dv.RowFilter = "bid_no ='" + drv["bid_no"].ToString() + "' and " + hidFilter.Value;
                    gv.DataSource = dv;// ds;
                    gv.DataBind();

                }
                else
                {
                    dv.RowFilter = "bid_no ='" + drv["bid_no"].ToString() + "'";
                    gv.DataSource = dv;// ds;
                    gv.DataBind();
                }
            }
        }
        protected void gvSubTaskBid_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DataRowView drv = (DataRowView)e.Row.DataItem;
                //LinkButton lnkGotoBid = (LinkButton)e.Row.FindControl("lnkGotoBid");
                //lnkGotoBid.Text = "<a href = '" + ConfigurationManager.AppSettings["url_Biddash"].ToString() + "?bidno=" + drv["bid_no"].ToString() + "&zuserlogin=" + hidLogin.Value + "' TARGET='openWindow' > Open </ a >";


            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}