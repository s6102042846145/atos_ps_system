﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using System.IO;
using PS_Library;

namespace PS_System.form
{
    public partial class frmTC01 : System.Web.UI.Page
    {
        #region Public valiables
        public string zisDev = ConfigurationManager.AppSettings["isDev"].ToString();
        public string zmode = "";
        public string zworkid = "";
        public string zsubworkid = "";
        public string ztaskid = "";
        public string[] sbid = { };
        public string[] sjob = { };
        public string filterexpressionbid = string.Empty;
        public string filterexpressionjob = string.Empty;
        public string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string zcsAdmin = ConfigurationManager.AppSettings["csadm"].ToString();
        public string zcsPwd = ConfigurationManager.AppSettings["cspwd"].ToString();
        public EmpInfoClass zempInfo = new EmpInfoClass();
        public DataTable dtData;
        static OTFunctions zotFunctions = new OTFunctions();
        static DbControllerBase zdbUtil = new DbControllerBase();
        public string zp6db = ConfigurationManager.AppSettings["p6db"].ToString();
        public oraDBUtil zordbUtil = new oraDBUtil();
        static LogHelper zLogHelper = new LogHelper();
        public string zurl_home = ConfigurationManager.AppSettings["url_home"].ToString();
        public string key_Decrypt = (ConfigurationManager.AppSettings["key_Decrypt"].ToString());
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z575798";//"z544124";
                ViewState["first"] = "1";
                lbstatusbid.Items[0].Selected = true;
                lbstatusbid.Items[1].Selected = true;
                lbstatusjob.Items[0].Selected = true;
                lbstatusjob.Items[1].Selected = true;


                btnAssignBid.Enabled = false;
                btnAssignJob.Enabled = false;
                ini_form();
                CheckMode();
                Picklbbid();
                Picklbjob();
                //Bind_DDL();
                filterP6("bid");
                filterP6("job");
                //checkassigneestatusgv2();
            }
            else
            {
                ViewState["first"] = "";
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void Bind_DDL(string type)
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
            string rowfilter = "";
            rowfilter = string.Format("(Org_Name = '{0}')", emp.ORGSNAME4);
            p6Class p6 = new p6Class();
            if (type == "bid")
            {
                if (hidBidTaskStatus.Value != "")
                {
                    string[] array = { };
                    array = hidBidTaskStatus.Value.Split(',');
                    string status_code = "";
                    for (int k = 0; k < array.Length; k++)
                    {
                        if (string.IsNullOrEmpty(status_code))
                        {
                            status_code = string.Format("status_code = '{0}'", array[k]);
                        }
                        else
                        {
                            status_code += string.Format(" or status_code = '{0}'", array[k]);
                        }
                    }
                    rowfilter = string.Format("{0} and ({1})", rowfilter, status_code);
                }
                DataTable dtbid = p6.getActivityBid("");
                DataView dv = new DataView(dtbid);
                dv.RowFilter = rowfilter;
                DataTable dtfilbid = dv.ToTable();

                //Bind list box
                inilbtaskcodebid(dtfilbid);
                Initasknamebid(dtfilbid);
                if (ViewState["first"].ToString() == "1")
                {
                    default_Multisel("bid");
                }
            }
            if (type == "job")
            {

                if (hidJobTaskStatus.Value != "")
                {
                    string[] array2 = { };
                    array2 = hidJobTaskStatus.Value.Split('|');
                    string status_code = "";
                    for (int k = 0; k < array2.Length; k++)
                    {
                        if (string.IsNullOrEmpty(status_code))
                        {
                            status_code = string.Format("status_code = '{0}'", array2[k]);
                        }
                        else
                        {
                            status_code += string.Format("or status_code = '{0}'", array2[k]);
                        }
                    }
                    rowfilter = string.Format("{0} and ({1})", rowfilter, status_code);
                }
                DataTable dtjob = p6.getActivityJob("");
                DataView dv2 = new DataView(dtjob);
                dv2.RowFilter = rowfilter;
                DataTable dtfiljob = dv2.ToTable();

                inilbtaskcodejob(dtfiljob);
                Initasknamejob(dtfiljob);
                if (ViewState["first"].ToString() == "1")
                {
                    default_Multisel("job");
                }
            }
        }
        private void filterP6(string bidjob)
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
            p6Class p6 = new p6Class();
            DataTable dtbid = new DataTable();
            DataTable dtjob = new DataTable();
            DataTable dtstaff = Session["ssdtStaff"] as DataTable;
            string rowfilterorg = "";
            string rowfilter = "";
            string rowfilterstatus = "";
            rowfilterorg = string.Format("(Org_Name = '{0}')", emp.ORGSNAME4);
            try
            {

                if (bidjob.ToLower() == "bid" || string.IsNullOrEmpty(bidjob))
                {

                    if (hidBidTaskStatus.Value != "")
                    {
                        string[] array = { };
                        array = hidBidTaskStatus.Value.Split(',');
                        string status_code = "";
                        for (int k = 0; k < array.Length; k++)
                        {
                            if (string.IsNullOrEmpty(status_code))
                            {
                                status_code = string.Format("status_code = '{0}'", array[k]);
                            }
                            else
                            {
                                status_code += string.Format(" or status_code = '{0}'", array[k]);
                            }
                        }
                        if (rowfilterstatus == "") rowfilterstatus = " and (" + status_code + ")";
                        //else rowfilterstatus = string.Format("{0} and ({1})", rowfilterstatus, status_code);
                    }

                    dtbid = p6.getActivityBidDis("", rowfilterstatus);
                    dtbid.Columns.Add("bid_desc", typeof(string));
                    dtbid.Columns.Add("bid_doc", typeof(string));
                    dtbid.Columns.Add("chk_selected", typeof(bool));
                    DataView dv = new DataView(dtbid);
                    dv.RowFilter = rowfilterorg;
                    DataTable dt = dv.ToTable();
                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        DataRow dr = dt.Rows[a];
                        dr["chk_selected"] = false;
                        dr["bid_desc"] = getBidDesc(dr["wbs_name"].ToString());
                        dr["bid_doc"] = getBidDocId(dr["wbs_name"].ToString());
                        dt.Rows[a].AcceptChanges();

                    }

                    if (hidsbid.Value != "")
                    {
                        string[] array = { };
                        array = hidsbid.Value.Split('|');
                        string hidbis = "";
                        for (int t = 0; t < array.Length; t++)
                        {
                            if (string.IsNullOrEmpty(hidbis))
                            {
                                hidbis = "'" + array[t] + "'";
                            }
                            else
                            {
                                hidbis += ",'" + array[t] + "'";
                            }
                        }
                        if (rowfilter == "") rowfilter = string.Format("wbs_name in (" + hidbis + ")");
                        else rowfilter = string.Format("{0} and ({1})", rowfilter, hidbis);
                    }
                    DataView dvrowfil = new DataView(dt);
                    dvrowfil.RowFilter = rowfilter;
                    dvrowfil.Sort = "chk_selected desc";
                    gv1.DataSource = dvrowfil;
                    gv1.DataBind();
                    Bind_DDL("bid");
                }

                if (bidjob.ToLower() == "job" || string.IsNullOrEmpty(bidjob))
                {
                    if (hidJobTaskStatus.Value != "")
                    {
                        string[] array2 = { };
                        array2 = hidJobTaskStatus.Value.Split('|');
                        string status_code = "";
                        for (int k = 0; k < array2.Length; k++)
                        {
                            if (string.IsNullOrEmpty(status_code))
                            {
                                status_code = string.Format("status_code = '{0}'", array2[k]);
                            }
                            else
                            {
                                status_code += string.Format("or status_code = '{0}'", array2[k]);
                            }
                        }
                        // rowfilter = string.Format("{0} and ({1})", rowfilter, status_code);
                        if (rowfilterstatus == "") rowfilterstatus = " and (" + status_code + ")";
                    }
                    dtjob = p6.getActivityJobDis("", rowfilterstatus);
                    dtjob.Columns.Add("job_desc", typeof(string));
                    dtjob.Columns.Add("job_doc", typeof(string));
                    dtjob.Columns.Add("chk_selected", typeof(bool));
                    DataView dv = new DataView(dtjob);
                    dv.RowFilter = rowfilterorg;
                    DataTable dt = dv.ToTable();
                    for (int a = 0; a < dt.Rows.Count; a++)
                    {
                        DataRow dr = dt.Rows[a];
                        dr["chk_selected"] = false;
                        dr["job_desc"] = getJobDesc(dr["wbs_name"].ToString());
                        dr["job_doc"] = GetJobDocID(dr["wbs_name"].ToString());
                        dt.Rows[a].AcceptChanges();

                    }

                    //if (hidsjob.Value != "")
                    //{
                    //    string hidjob = "";
                    //    hidjob = string.Format("wbs_name in ('" + hidsjob.Value.Replace(",", "','") + "')");
                    //    if (rowfilter == "") rowfilter = string.Format("wbs_name in ('" + hidsjob.Value.Replace(",", "','") + "')");
                    //    else rowfilter = string.Format("{0} and ({1})", rowfilter, hidjob);
                    //}

                    if (hidsjob.Value != "")
                    {
                        string[] array = { };
                        array = hidsjob.Value.Split('|');
                        string hidjob = "";
                        for (int t = 0; t < array.Length; t++)
                        {
                            if (string.IsNullOrEmpty(hidjob))
                            {
                                hidjob = "'" + array[t] + "'";
                            }
                            else
                            {
                                hidjob += ",'" + array[t] + "'";
                            }
                        }
                        if (rowfilter == "") rowfilter = string.Format("wbs_name in (" + hidjob + ")");
                        else rowfilter = string.Format("{0} and ({1})", rowfilter, hidjob);
                    }
                    DataView dvrowfil = new DataView(dt);
                    dvrowfil.RowFilter = rowfilter;
                    dvrowfil.Sort = "chk_selected desc";
                    gv2.DataSource = dvrowfil;
                    gv2.DataBind();
                    Bind_DDL("job");
                }
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }

        }
        protected void clickRowCommand(int index, string type)
        {

            p6Class p6 = new p6Class();
            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
            if (type == "bid")
            {


                int i = System.Convert.ToInt32(index);
                var lbl = ((Label)gv1.Rows[i].FindControl("gv1wbs_name"));
                var gv = ((GridView)gv1.Rows[i].FindControl("gv1Sub"));

                DataTable dtbid = p6.getActivityBid("");
                dtbid.Columns.Add("bid_doc_id", typeof(string));
                dtbid.Columns.Add("assign_id", typeof(string));
                dtbid.Columns.Add("assign_name", typeof(string));
                dtbid.Columns.Add("assignstatus", typeof(string));

                //Check filter
                filterexpressionbid = string.Format("(Org_Name = '{0}')", emp.ORGSNAME4);
                filterexpressionbid = string.Format("{0} and ({1})", filterexpressionbid, "wbs_name ='" + lbl.Text + "'");

                if (hidBidTaskName.Value != "")
                {
                    string[] array = { };
                    array = hidBidTaskName.Value.Split(',');
                    string task_name = "";
                    for (int q = 0; q < array.Length; q++)
                    {
                        if (string.IsNullOrEmpty(task_name))
                        {
                            task_name = string.Format("task_name = '{0}'", array[q]);
                        }
                        else
                        {
                            task_name += string.Format("or task_name = '{0}'", array[q]);
                        }
                    }
                    filterexpressionbid = string.Format("{0} and ({1})", filterexpressionbid, task_name);
                }

                if (hidCheckedBid.Value != "")
                {
                    string[] array = { };
                    array = hidCheckedBid.Value.Split(',');
                    string task_id = "";
                    for (int t = 0; t < array.Length; t++)
                    {
                        if (string.IsNullOrEmpty(task_id))
                        {
                            task_id = string.Format("task_id = '{0}'", array[t]);
                        }
                        else
                        {
                            task_id += string.Format("or task_id = '{0}'", array[t]);
                        }
                    }
                    filterexpressionbid = string.Format("{0} or ({1})", filterexpressionbid, task_id);
                }
                //Select from datatable
                DataRow[] drlBid = dtbid.Select(filterexpressionbid);
                if (drlBid.Length > 0)
                {
                    dtbid = drlBid.CopyToDataTable();
                }
                else
                {
                    dtbid.Clear();
                }
                string taskid = "";
                string sql = "";
                for (int a = 0; a < dtbid.Rows.Count; a++)
                {
                    DataRow dr = dtbid.Rows[a];
                    dtbid.Rows[a].AcceptChanges();

                    taskid = dr["task_id"].ToString();
                    if (!string.IsNullOrEmpty(taskid))
                    {
                        sql = @"select staff_id, staff_name, assignstatus from wf_tc_taskassignment
                        where task_id = " + taskid + " and assign_type = 'Center' and project_name='" + lbl.Text + "'";
                        DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);

                        if (dt.Rows.Count > 0)
                        {
                            DataRow[] drr = dtbid.Select("task_id = '" + taskid + "'");
                            drr[0]["assign_id"] = dt.Rows[0]["staff_id"].ToString();
                            drr[0]["assign_name"] = dt.Rows[0]["staff_name"].ToString();
                            drr[0]["assignstatus"] = "Assign";
                            dtbid.AcceptChanges();
                        }
                        else
                        {
                            DataRow[] drr = dtbid.Select("task_id = '" + taskid + "'");
                            drr[0]["assignstatus"] = "Not Assign";
                            dtbid.AcceptChanges();
                        }
                    }
                }
                if (dtbid.Rows.Count > 0) { Session.Add("ssdtbid", dtbid); }
                DataView dvSort = dtbid.DefaultView;
                gv.DataSource = dvSort.ToTable();
                gv.DataBind();

                TabBid.Attributes.Add("class", "tab-pane active");
                TabJob.Attributes.Add("class", "tab-pane");
                liTabBid.Attributes.Add("class", "active");
                liTabJob.Attributes.Remove("class");
            }
            if (type == "job")
            {

                int i = System.Convert.ToInt32(index);
                var lbl = ((Label)gv2.Rows[i].FindControl("gv2wbs_name"));
                var gv = ((GridView)gv2.Rows[i].FindControl("gv2Sub"));

                DataTable dtjob = p6.getActivityJob("");
                dtjob.Columns.Add("job_doc_id", typeof(string));
                dtjob.Columns.Add("assign_id", typeof(string));
                dtjob.Columns.Add("assign_name", typeof(string));
                dtjob.Columns.Add("assignstatus", typeof(string));

                //Check filter
                filterexpressionjob = string.Format("(Org_Name = '{0}')", emp.ORGSNAME4);
                filterexpressionjob = string.Format("{0} and ({1})", filterexpressionjob, "wbs_name ='" + lbl.Text + "'");

                if (hidJobTaskName.Value != "")
                {
                    string[] array = { };
                    array = hidJobTaskName.Value.Split(',');
                    string task_name = "";
                    for (int q = 0; q < array.Length; q++)
                    {
                        if (string.IsNullOrEmpty(task_name))
                        {
                            task_name = string.Format("task_name = '{0}'", array[q]);
                        }
                        else
                        {
                            task_name += string.Format("or task_name = '{0}'", array[q]);
                        }
                    }
                    filterexpressionjob = string.Format("{0} and ({1})", filterexpressionjob, task_name);
                }

                if (hidCheckedJob.Value != "")
                {
                    string[] array = { };
                    array = hidCheckedJob.Value.Split(',');
                    string task_id = "";
                    for (int t = 0; t < array.Length; t++)
                    {
                        if (string.IsNullOrEmpty(task_id))
                        {
                            task_id = string.Format("task_id = '{0}'", array[t]);
                        }
                        else
                        {
                            task_id += string.Format("or task_id = '{0}'", array[t]);
                        }
                    }
                    filterexpressionjob = string.Format("{0} or ({1})", filterexpressionjob, task_id);
                }
                //Select from datatable
                DataRow[] drlJob = dtjob.Select(filterexpressionjob);
                if (drlJob.Length > 0)
                {
                    dtjob = drlJob.CopyToDataTable();
                }
                else
                {
                    dtjob.Clear();
                }
                string taskid = "";
                string sql = "";
                for (int a = 0; a < dtjob.Rows.Count; a++)
                {
                    DataRow dr = dtjob.Rows[a];

                    dtjob.Rows[a].AcceptChanges();

                    taskid = dr["task_id"].ToString();
                    if (!string.IsNullOrEmpty(taskid))
                    {
                        sql = @"select staff_id, staff_name, assignstatus from wf_tc_taskassignment
                        where task_id = " + taskid + " and assign_type = 'Center'";
                        DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);

                        if (dt.Rows.Count > 0)
                        {
                            DataRow[] drr = dtjob.Select("task_id = '" + taskid + "'");
                            drr[0]["assign_id"] = dt.Rows[0]["staff_id"].ToString();
                            drr[0]["assign_name"] = dt.Rows[0]["staff_name"].ToString();
                            drr[0]["assignstatus"] = "Assign";
                            dtjob.AcceptChanges();
                        }
                        else
                        {
                            DataRow[] drr = dtjob.Select("task_id = '" + taskid + "'");
                            drr[0]["assignstatus"] = "Not Assign";
                            dtjob.AcceptChanges();
                        }
                    }
                }
                if (dtjob.Rows.Count > 0) { Session.Add("ssdtjob", dtjob); }
                DataView dvSort = dtjob.DefaultView;
                gv.DataSource = dvSort.ToTable(); //dtjob;// ds;
                gv.DataBind();


                TabJob.Attributes.Add("class", "tab-pane active");
                TabBid.Attributes.Add("class", "tab-pane");
                liTabJob.Attributes.Add("class", "active");
                liTabBid.Attributes.Remove("class");

            }
        }
        protected void gv1_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "viewdata")
            {
                p6Class p6 = new p6Class();
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                int i = System.Convert.ToInt32(e.CommandArgument);
                var lbl = ((Label)gv1.Rows[i].FindControl("gv1wbs_name"));
                var gv = ((GridView)gv1.Rows[i].FindControl("gv1Sub"));

                if (gv.Visible == false)
                {
                    gv.Visible = true;

                    DataTable dtbid = p6.getActivityBid("");
                    //DataView dv = new DataView(dtbid);
                    //dv.RowFilter = "wbs_name ='" + lbl.Text + "'";
                    //dtbid.Columns.Add("chk_selected", typeof(bool));
                    //dtbid.Columns.Add("bid_desc", typeof(string));
                    dtbid.Columns.Add("bid_doc_id", typeof(string));
                    dtbid.Columns.Add("assign_id", typeof(string));
                    dtbid.Columns.Add("assign_name", typeof(string));
                    dtbid.Columns.Add("assignstatus", typeof(string));
                    //dtbid.Columns.Add("bid_doc", typeof(string));

                    //Check filter
                    filterexpressionbid = string.Format("(Org_Name = '{0}')", emp.ORGSNAME4);
                    filterexpressionbid = string.Format("{0} and ({1})", filterexpressionbid, "wbs_name ='" + lbl.Text + "'");
                    //filterexpressionbid = string.Format("wbs_name ='" + lbl.Text + "'");

                    if (hidBidTaskName.Value != "")
                    {
                        string[] array = { };
                        array = hidBidTaskName.Value.Split(',');
                        string task_name = "";
                        for (int q = 0; q < array.Length; q++)
                        {
                            if (string.IsNullOrEmpty(task_name))
                            {
                                task_name = string.Format("task_name = '{0}'", array[q]);
                            }
                            else
                            {
                                task_name += string.Format("or task_name = '{0}'", array[q]);
                            }
                        }
                        filterexpressionbid = string.Format("{0} and ({1})", filterexpressionbid, task_name);
                    }

                    if (hidBidTaskStatus.Value != "")
                    {
                        string[] array = { };
                        array = hidBidTaskStatus.Value.Split(',');
                        string status_code = "";
                        for (int k = 0; k < array.Length; k++)
                        {
                            if (string.IsNullOrEmpty(status_code))
                            {
                                status_code = string.Format("status_code = '{0}'", array[k]);
                            }
                            else
                            {
                                status_code += string.Format("or status_code = '{0}'", array[k]);
                            }
                        }
                        //filterexpressionbid = string.Format("{0} and ({1})", filterexpressionbid, status_code);
                    }

                    if (hidCheckedBid.Value != "")
                    {
                        string[] array = { };
                        array = hidCheckedBid.Value.Split(',');
                        string task_id = "";
                        for (int t = 0; t < array.Length; t++)
                        {
                            if (string.IsNullOrEmpty(task_id))
                            {
                                task_id = string.Format("task_id = '{0}'", array[t]);
                            }
                            else
                            {
                                task_id += string.Format("or task_id = '{0}'", array[t]);
                            }
                        }
                        filterexpressionbid = string.Format("{0} or ({1})", filterexpressionbid, task_id);
                    }
                    //Select from datatable
                    DataRow[] drlBid = dtbid.Select(filterexpressionbid);
                    if (drlBid.Length > 0)
                    {
                        dtbid = drlBid.CopyToDataTable();
                    }
                    else
                    {
                        dtbid.Clear();
                    }
                    string taskid = "";
                    string sql = "";
                    for (int a = 0; a < dtbid.Rows.Count; a++)
                    {
                        DataRow dr = dtbid.Rows[a];
                        //dr["chk_selected"] = false;
                        //if (hidCheckedBid.Value != "")
                        //{
                        //    if (hidCheckedBid.Value.Contains(dr["task_id"].ToString()))
                        //        dr["chk_selected"] = true;
                        //}

                        // dtbid.Rows[a].AcceptChanges();

                        taskid = dr["task_id"].ToString();
                        if (!string.IsNullOrEmpty(taskid))
                        {
                            sql = @"select staff_id, staff_name, assignstatus from wf_tc_taskassignment
                        where task_id = " + taskid + " and assign_type = 'Center' and project_name='" + lbl.Text + "'";
                            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);

                            if (dt.Rows.Count > 0)
                            {
                                DataRow[] drr = dtbid.Select("task_id = '" + taskid + "'");
                                drr[0]["assign_id"] = dt.Rows[0]["staff_id"].ToString();
                                drr[0]["assign_name"] = dt.Rows[0]["staff_name"].ToString();
                                drr[0]["assignstatus"] = "Assign";
                                dtbid.AcceptChanges();
                            }
                            else
                            {
                                DataRow[] drr = dtbid.Select("task_id = '" + taskid + "'");
                                drr[0]["assignstatus"] = "Not Assign";
                                dtbid.AcceptChanges();
                            }
                        }
                    }
                    if (dtbid.Rows.Count > 0) { Session.Add("ssdtbid", dtbid); }
                    DataView dvSort = dtbid.DefaultView;
                    //dvSort.Sort = "chk_selected desc";
                    gv.DataSource = dvSort.ToTable(); //dtbid;// ds;
                    gv.DataBind();

                }
                else { gv.Visible = false; }

                TabBid.Attributes.Add("class", "tab-pane active");
                TabJob.Attributes.Add("class", "tab-pane");
                liTabBid.Attributes.Add("class", "active");
                liTabJob.Attributes.Remove("class");
            }

            if (e.CommandName == "assigndata")
            {
                int i = System.Convert.ToInt32(e.CommandArgument);
                var lbl = ((Label)gv1.Rows[i].FindControl("gv1wbs_name"));
                var gv = ((GridView)gv1.Rows[i].FindControl("gv1Assigned"));
                string sql = "";
                string sqldis = "";

                DataTable dt = new DataTable();

                DataTable dtdis = new DataTable();

                try
                {
                    if (gv.Visible == false)
                    {
                        gv.Visible = true;

                        sqldis = "select distinct project_name from wf_tc_taskassignment where project_name ='" + lbl.Text + "' order by project_name";
                        dt = zdbUtil.ExecSql_DataTable(sqldis, zetl4eisdb);

                        for (int f = 0; f < dt.Rows.Count; f++)
                        {
                            string staff = "";
                            sql = @"  select distinct project_name,staff_name from wf_tc_taskassignment where  project_name ='" + lbl.Text + @"'";
                            dtdis = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
                            dt.Columns.Add("staff_name", typeof(string));
                            if (dtdis.Rows.Count > 0)
                            {
                                for (int k = 0; k < dtdis.Rows.Count; k++)
                                {
                                    if (staff == "")
                                    {
                                        staff = dtdis.Rows[k]["staff_name"].ToString();
                                    }
                                    else
                                    {
                                        if (staff.IndexOf(dtdis.Rows[k]["staff_name"].ToString()) > -1) { }
                                        else
                                        {
                                            staff += "," + dtdis.Rows[k]["staff_name"].ToString();
                                        }
                                    }
                                    DataRow dr = dt.Rows[f];
                                    dr["staff_name"] = staff;
                                    dt.Rows[f].AcceptChanges();
                                }
                            }
                        }

                        gv.DataSource = dt; //dtbid;// ds;
                        gv.DataBind();
                    }
                    else { gv.Visible = false; }
                    TabBid.Attributes.Add("class", "tab-pane active");
                    TabJob.Attributes.Add("class", "tab-pane");
                    liTabBid.Attributes.Add("class", "active");
                    liTabJob.Attributes.Remove("class");
                }
                catch (Exception ex)
                {

                    LogHelper.WriteEx(ex);
                }
            }
        }
        protected void gv2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "viewdatajob")
            {
                p6Class p6 = new p6Class();
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                int i = System.Convert.ToInt32(e.CommandArgument);
                var lbl = ((Label)gv2.Rows[i].FindControl("gv2wbs_name"));
                var gv = ((GridView)gv2.Rows[i].FindControl("gv2Sub"));

                if (gv.Visible == false)
                {
                    gv.Visible = true;

                    DataTable dtjob = p6.getActivityJob("");
                    //dtjob.Columns.Add("chk_selected", typeof(bool));
                    dtjob.Columns.Add("job_doc_id", typeof(string));
                    dtjob.Columns.Add("assign_id", typeof(string));
                    dtjob.Columns.Add("assign_name", typeof(string));
                    dtjob.Columns.Add("assignstatus", typeof(string));

                    //Check filter
                    filterexpressionjob = string.Format("(Org_Name = '{0}')", emp.ORGSNAME4);
                    filterexpressionjob = string.Format("{0} and ({1})", filterexpressionjob, "wbs_name ='" + lbl.Text + "'");

                    if (hidJobTaskName.Value != "")
                    {
                        string[] array = { };
                        array = hidJobTaskName.Value.Split(',');
                        string task_name = "";
                        for (int q = 0; q < array.Length; q++)
                        {
                            if (string.IsNullOrEmpty(task_name))
                            {
                                task_name = string.Format("task_name = '{0}'", array[q]);
                            }
                            else
                            {
                                task_name += string.Format("or task_name = '{0}'", array[q]);
                            }
                        }
                        filterexpressionjob = string.Format("{0} and ({1})", filterexpressionjob, task_name);
                    }

                    if (hidJobTaskStatus.Value != "")
                    {
                        string[] array = { };
                        array = hidJobTaskStatus.Value.Split(',');
                        string status_code = "";
                        for (int k = 0; k < array.Length; k++)
                        {
                            if (string.IsNullOrEmpty(status_code))
                            {
                                status_code = string.Format("status_code = '{0}'", array[k]);
                            }
                            else
                            {
                                status_code += string.Format("or status_code = '{0}'", array[k]);
                            }
                        }
                        //filterexpressionjob = string.Format("{0} and ({1})", filterexpressionjob, status_code);
                    }

                    if (hidCheckedJob.Value != "")
                    {
                        string[] array = { };
                        array = hidCheckedJob.Value.Split(',');
                        string task_id = "";
                        for (int t = 0; t < array.Length; t++)
                        {
                            if (string.IsNullOrEmpty(task_id))
                            {
                                task_id = string.Format("task_id = '{0}'", array[t]);
                            }
                            else
                            {
                                task_id += string.Format("or task_id = '{0}'", array[t]);
                            }
                        }
                        filterexpressionjob = string.Format("{0} or ({1})", filterexpressionjob, task_id);
                    }
                    //Select from datatable
                    DataRow[] drlJob = dtjob.Select(filterexpressionjob);
                    if (drlJob.Length > 0)
                    {
                        dtjob = drlJob.CopyToDataTable();
                    }
                    else
                    {
                        dtjob.Clear();
                    }
                    string taskid = "";
                    string sql = "";
                    for (int a = 0; a < dtjob.Rows.Count; a++)
                    {
                        DataRow dr = dtjob.Rows[a];
                        //dr["chk_selected"] = false;
                        //if (hidCheckedJob.Value != "")
                        //{
                        //    if (hidCheckedJob.Value.Contains(dr["task_id"].ToString()))
                        //        dr["chk_selected"] = true;
                        //}
                        //dr["bid_desc"] = getBidDesc(dr["wbs_name"].ToString());
                        //dr["bid_doc"] = getBidDocId(dr["wbs_name"].ToString());
                        //dtjob.Rows[a].AcceptChanges();

                        taskid = dr["task_id"].ToString();
                        if (!string.IsNullOrEmpty(taskid))
                        {
                            sql = @"select staff_id, staff_name, assignstatus from wf_tc_taskassignment
                        where task_id = " + taskid + " and assign_type = 'Center'";
                            DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);

                            if (dt.Rows.Count > 0)
                            {
                                DataRow[] drr = dtjob.Select("task_id = '" + taskid + "'");
                                drr[0]["assign_id"] = dt.Rows[0]["staff_id"].ToString();
                                drr[0]["assign_name"] = dt.Rows[0]["staff_name"].ToString();
                                drr[0]["assignstatus"] = "Assign";
                                dtjob.AcceptChanges();
                            }
                            else
                            {
                                DataRow[] drr = dtjob.Select("task_id = '" + taskid + "'");
                                drr[0]["assignstatus"] = "Not Assign";
                                dtjob.AcceptChanges();
                            }
                        }
                    }
                    if (dtjob.Rows.Count > 0) { Session.Add("ssdtjob", dtjob); }
                    DataView dvSort = dtjob.DefaultView;
                    //dvSort.Sort = "chk_selected desc";
                    gv.DataSource = dvSort.ToTable(); //dtjob;// ds;
                    gv.DataBind();

                }
                else { gv.Visible = false; }

                TabJob.Attributes.Add("class", "tab-pane active");
                TabBid.Attributes.Add("class", "tab-pane");
                liTabJob.Attributes.Add("class", "active");
                liTabBid.Attributes.Remove("class");
            }
            if (e.CommandName == "assigndata")
            {
                int i = System.Convert.ToInt32(e.CommandArgument);
                var lbl = ((Label)gv2.Rows[i].FindControl("gv2wbs_name"));
                var gv = ((GridView)gv2.Rows[i].FindControl("gv2Assigned"));
                string sql = "";
                string sqldis = "";

                DataTable dt = new DataTable();

                DataTable dtdis = new DataTable();

                try
                {
                    if (gv.Visible == false)
                    {
                        gv.Visible = true;
                        sqldis = "select distinct project_name from wf_tc_taskassignment where project_name ='" + lbl.Text + "' order by project_name";
                        //sqldis = "select distinct task_name,project_name,task_id,task_code from wf_tc_taskassignment where project_name ='" + lbl.Text + "' order by project_name";
                        dt = zdbUtil.ExecSql_DataTable(sqldis, zetl4eisdb);
                        dt.Columns.Add("staff_name", typeof(string));
                        for (int f = 0; f < dt.Rows.Count; f++)
                        {
                            string staff = "";
                            sql = @"  select distinct project_name,staff_name from wf_tc_taskassignment where  project_name ='" + lbl.Text + @"'";
                            //    sql = @" select distinct task_name,project_name,task_id,task_code,task_name,staff_name from wf_tc_taskassignment where  project_name ='" + lbl.Text + @"' 
                            //and task_name ='" + dt.Rows[f]["task_name"].ToString() + "'";
                            dtdis = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
                            if (dtdis.Rows.Count > 0)
                            {
                                for (int k = 0; k < dtdis.Rows.Count; k++)
                                {
                                    if (staff == "")
                                    {
                                        staff = dtdis.Rows[k]["staff_name"].ToString();
                                    }
                                    else
                                    {
                                        if (staff.IndexOf(dtdis.Rows[k]["staff_name"].ToString()) > -1) { }
                                        else
                                        {
                                            staff += "," + dtdis.Rows[k]["staff_name"].ToString();
                                        }
                                    }
                                    DataRow dr = dt.Rows[f];
                                    dr["staff_name"] = staff;
                                    dt.Rows[f].AcceptChanges();
                                }
                            }
                        }

                        gv.DataSource = dt; //dtbid;// ds;
                        gv.DataBind();
                    }
                    else { gv.Visible = false; }

                    TabJob.Attributes.Add("class", "tab-pane active");
                    TabBid.Attributes.Add("class", "tab-pane");
                    liTabJob.Attributes.Add("class", "active");
                    liTabBid.Attributes.Remove("class");
                }
                catch (Exception ex)
                {

                    LogHelper.WriteEx(ex);
                }

            }
        }
        protected void gv1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //    GridView gv1Sub = (GridView)gv1.FindControl("gv1Sub");
                //    string xtask_id = ((Label)gv1Sub.FindControl("gv1TaskId")).Text;

                //    CheckBox cb = ((CheckBox)gv1Sub.FindControl("gv1CheckBox"));
            }
        }
        private string getBidDesc(string xBid_No)
        {
            int epcindex;
            string pjname = "";
            string name = "";

            string[] prefixBid = ConfigurationManager.AppSettings["P6_filterBid"].ToString().Split(',');

            if (xBid_No.Contains("(EPC"))
            {
                epcindex = xBid_No.IndexOf("(EPC", 0);
                name = xBid_No.Substring(0, epcindex);
                if (name.Contains("Enq."))
                {
                    pjname = name.Replace("Enq.", string.Empty);
                }
                else
                {
                    //pjname = name.Replace("B-", string.Empty); 
                    foreach (string bid in prefixBid)
                    {
                        pjname = name.Replace(bid, string.Empty);
                    }
                }
            }
            else
            {
                if (xBid_No.Contains("Enq."))
                {
                    pjname = xBid_No.Replace("Enq.", string.Empty);
                }
                else
                {
                    //pjname = xBid_No.Replace("B-", string.Empty);
                    foreach (string bid in prefixBid)
                    {
                        pjname = xBid_No.Replace(bid, string.Empty);
                    }
                }
            }
            string xbid_desc = "";
            string sql = "select top 1 dbo.udf_StripHTML(bid_desc) as biddesc, bid_desc from bid_no where bid_no = '" + pjname + "' order by bid_id desc ";
            var dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xbid_desc = dr["biddesc"].ToString();
            }

            return xbid_desc;

        }
        private string getBidDocId(string wbs_name)
        {
            int epcindex;
            string bid_no = "";
            string name = "";

            string[] prefixBid = ConfigurationManager.AppSettings["P6_filterBid"].ToString().Split(',');

            if (wbs_name.Contains("(EPC"))
            {
                epcindex = wbs_name.IndexOf("(EPC", 0);
                name = wbs_name.Substring(0, epcindex);
                if (name.Contains("Enq."))
                {
                    // pjname = name.Replace("Enq.", string.Empty);
                    bid_no = name;
                }
                else
                {
                    //bid_no = name.Replace("B-", string.Empty);
                    foreach (string bid in prefixBid)
                    {
                        bid_no = name.Replace(bid, string.Empty);
                    }
                }
            }
            else
            {
                if (wbs_name.Contains("Enq."))
                {
                    // pjname = xBid_No.Replace("Enq.", string.Empty);
                    bid_no = wbs_name;
                }
                else
                {
                    //bid_no = wbs_name.Replace("B-", string.Empty);
                    foreach (string bid in prefixBid)
                    {
                        bid_no = wbs_name.Replace(bid, string.Empty);
                    }
                }
            }
            string xbid_doc = "";
            string sql = @"select case when doc_pcsa.document_nodeid is not null then '<a id=" + "\"" + "docAtt_' + convert(nvarchar,doc_pcsa.document_nodeid) + '" + "\"" + "class=" + "\"" + "cls_view_doc" + "\"" + "href=" + "\"" + "PreviewDocCS.aspx?nodeid=' + convert(nvarchar,doc_pcsa.document_nodeid) +'" + "\"" + " target=" + "\"" + "_blank" + "\"" + @">- ' + doc_pcsa.document_name + '</a>' 
                                                                                        + case when doc.document_nodeid is not null then '<br/><a id=" + "\"" + "docAtt_' + convert(nvarchar,doc.document_nodeid) + '" + "\"" + "class=" + "\"" + "cls_view_doc" + "\"" + "href=" + "\"" + "PreviewDocCS.aspx?nodeid=' + convert(nvarchar,doc.document_nodeid) +'" + "\"" + " target=" + "\"" + "_blank" + "\"" + @">- ' + doc.document_name + '</a>'
                                                                                                                                         else '' end
                                         when doc.document_nodeid is not null then '<a id=" + "\"" + "docAtt_' + convert(nvarchar,doc.document_nodeid) + '" + "\"" + "class=" + "\"" + "cls_view_doc" + "\"" + "href=" + "\"" + "PreviewDocCS.aspx?nodeid=' + convert(nvarchar,doc.document_nodeid) +'" + "\"" + " target=" + "\"" + "_blank" + "\"" + @">- ' + doc.document_name + '</a>'
                                         else '' end as bid_doc
                            FROM bid_no bid 
                                left join wf_dtsa_request_details doc on bid.bid_no = doc.bid_no and bid.bid_revision = doc.bid_revision
                                                                            and doc.process_id in (select dtsa.process_id from wf_dtsa_request dtsa where dtsa.wf_status = 'PM APPROVED')
                                left join wf_pcsa_request_details doc_pcsa on bid.bid_no = doc_pcsa.bid_no and bid.bid_revision = doc_pcsa.bid_revision
                                                                            and doc_pcsa.process_id in (select pcsa.process_id from wf_pcsa_request pcsa where pcsa.form_status = 'PM APPROVED')
                            where bid.bid_revision = (SELECT max(bid_revision) from bid_no bid2 where bid.bid_no = bid2.bid_no and LOWER(bid2.status) not in ('new','waiting for approve') ) 
                                  and bid.bid_no = '" + bid_no + "'";

            var dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xbid_doc = dr["bid_doc"].ToString();
            }

            return xbid_doc;

        }
        private string getJobDesc(string xJob_No)
        {
            string[] split = xJob_No.Split('(');
            string name = split[0].ToString();
            string pjname = name.Replace("Job No.", string.Empty);

            string xjob_desc = "";
            string sql = "select top 1 dbo.udf_StripHTML(job_desc) as jobdesc, job_desc from job_no where job_no = '" + pjname + "' order by job_id desc ";
            var dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
            if (dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                xjob_desc = dr["jobdesc"].ToString();
            }
            return xjob_desc;

        }
        private string GetJobDocID(string wbs_name)
        {
            string xjob_doc = "";
            try
            {
                string[] split = wbs_name.Split('(');
                string name = split[0].ToString().Trim();
                string job_no = name.Replace("Job No.", string.Empty);

                string[] prefixJob = ConfigurationManager.AppSettings["P6_filterJob"].ToString().Split(',');
                foreach (string job in prefixJob)
                {
                    job_no = job_no.Replace(job, string.Empty);
                }

                string sql = @"select case when doc_waa.document_nodeid is not null then '<a id=" + "\"" + "docAtt_' + convert(nvarchar,doc_waa.document_nodeid) + '" + "\"" + "class=" + "\"" + "cls_view_doc" + "\"" + "href=" + "\"" + "PreviewDocCS.aspx?nodeid=' + convert(nvarchar,doc_waa.document_nodeid) +'" + "\"" + " target=" + "\"" + "_blank" + "\"" + @">- ' + doc_waa.document_name + '</a>' 
                                                                                        + case when doc_dtsj.document_nodeid is not null then '<br/><a id=" + "\"" + "docAtt_' + convert(nvarchar,doc_dtsj.document_nodeid) + '" + "\"" + "class=" + "\"" + "cls_view_doc" + "\"" + "href=" + "\"" + "PreviewDocCS.aspx?nodeid=' + convert(nvarchar,doc_dtsj.document_nodeid) +'" + "\"" + " target=" + "\"" + "_blank" + "\"" + @">- ' + doc_dtsj.document_name + '</a>'
                                                                                                                                         else '' end
                                         when doc_dtsj.document_nodeid is not null then '<a id=" + "\"" + "docAtt_' + convert(nvarchar,doc_dtsj.document_nodeid) + '" + "\"" + "class=" + "\"" + "cls_view_doc" + "\"" + "href=" + "\"" + "PreviewDocCS.aspx?nodeid=' + convert(nvarchar,doc_dtsj.document_nodeid) +'" + "\"" + " target=" + "\"" + "_blank" + "\"" + @">- ' + doc_dtsj.document_name + '</a>'
                                         else '' end as job_doc
                            FROM job_no job 
                                left join project_no proj on job.project_id = proj.project_id
                                left join wf_dtsj_request_details doc_dtsj on job.job_no = doc_dtsj.job_no and job.revision = doc_dtsj.job_revision
                                                                            and doc_dtsj.process_id in (select dtsj.process_id from wf_dtsj_request dtsj where dtsj.wf_status = 'PM APPROVED')
                                left join wf_waa_request_details doc_waa on job.job_no = doc_waa.job_no and job.revision = doc_waa.job_revision 
                                                                            and doc_waa.process_id in (select waa.process_id from wf_waa_request waa where waa.form_status = 'DIRECTOR APPROVED')
                            where revision = (SELECT max(revision) from job_no job2 where job.job_no = job2.job_no and LOWER(job2.job_status) not in ('new','waiting for approve') ) 
                                  and job.job_no = '" + job_no + "' ";
                var dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
                if (dt.Rows.Count > 0)
                {
                    DataRow dr = dt.Rows[0];
                    xjob_doc = dr["job_doc"].ToString();
                }
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
            return xjob_doc;
        }
        private void inilbtaskcodebid(DataTable dtbid)
        {
            DataTable dt = dtbid.DefaultView.ToTable(true, "wbs_name");
            lbTaskCodeBid.Items.Clear();
            lbTaskCodeBid.DataSource = dt;
            lbTaskCodeBid.DataValueField = dt.Columns[0].ColumnName;
            lbTaskCodeBid.DataTextField = dt.Columns[0].ColumnName;
            lbTaskCodeBid.DataBind();
            string rowfilter = "";

            //string hidbis = "";
            //hidbis = string.Format("wbs_name in ('" + hidsbid.Value.Replace(",", "','") + "')");
            //if (rowfilter == "") rowfilter = string.Format("wbs_name in ('" + hidsbid.Value.Replace(",", "','") + "')");
            //else rowfilter = string.Format("{0} and ({1})", rowfilter, hidbis);

            if (hidsbid.Value != "")
            {
                string[] array = { };
                array = hidsbid.Value.Split('|');
                string hidbis = "";
                for (int t = 0; t < array.Length; t++)
                {
                    if (string.IsNullOrEmpty(hidbis))
                    {
                        hidbis = "'" + array[t] + "'";
                    }
                    else
                    {
                        hidbis += ",'" + array[t] + "'";
                    }
                }
                if (rowfilter == "") rowfilter = string.Format("wbs_name in (" + hidbis + ")");
                //else rowfilter = string.Format("{0} and ({1})", rowfilter, hidbis);
            }
            DataView dv = new DataView(dt);
            dv.RowFilter = rowfilter;
            for (int i = 0; i < dv.Count; i++)
            {
                lbTaskCodeBid.Items.FindByText(dv[i]["wbs_name"].ToString()).Selected = true;
            }
        }
        private void Initasknamebid(DataTable dtbid)
        {
            DataTable dt = dtbid.DefaultView.ToTable(true, "task_name");
            lbtasknamebid.Items.Clear();
            lbtasknamebid.DataSource = dt;
            lbtasknamebid.DataValueField = dt.Columns[0].ColumnName;
            lbtasknamebid.DataTextField = dt.Columns[0].ColumnName;
            lbtasknamebid.DataBind();
            string rowfilter = "";
            if (hidBidTaskName.Value != "")
            {
                string[] array = { };
                array = hidBidTaskName.Value.Split(',');
                string task_name = "";
                for (int q = 0; q < array.Length; q++)
                {
                    if (string.IsNullOrEmpty(task_name))
                    {
                        task_name = string.Format("task_name = '{0}'", array[q]);
                    }
                    else
                    {
                        task_name += string.Format("or task_name = '{0}'", array[q]);
                    }
                }
                rowfilter = task_name;
                DataView dv = new DataView(dt);
                dv.RowFilter = rowfilter;
                for (int i = 0; i < dv.Count; i++)
                {
                    lbtasknamebid.Items.FindByText(dv[i]["task_name"].ToString()).Selected = true;

                }
            }
        }
        private void inilbtaskcodejob(DataTable dtjob)
        {
            DataTable dt = dtjob.DefaultView.ToTable(true, "wbs_name");
            lbTaskCodeJob.Items.Clear();
            lbTaskCodeJob.DataSource = dt;
            lbTaskCodeJob.DataValueField = dt.Columns[0].ColumnName;
            lbTaskCodeJob.DataTextField = dt.Columns[0].ColumnName;
            lbTaskCodeJob.DataBind();
            string rowfilter = "";
            if (hidsjob.Value != "")
            {
                string[] array = { };
                array = hidsjob.Value.Split('|');
                string hidjob = "";
                for (int t = 0; t < array.Length; t++)
                {
                    if (string.IsNullOrEmpty(hidjob))
                    {
                        hidjob = "'" + array[t] + "'";
                    }
                    else
                    {
                        hidjob += ",'" + array[t] + "'";
                    }
                }
                if (rowfilter == "") rowfilter = string.Format("wbs_name in (" + hidjob + ")");
                //else rowfilter = string.Format("{0} and ({1})", rowfilter, hidjob);
            }
            DataView dv = new DataView(dt);
            dv.RowFilter = rowfilter;
            for (int i = 0; i < dv.Count; i++)
            {
                lbTaskCodeJob.Items.FindByText(dv[i]["wbs_name"].ToString()).Selected = true;
            }
        }
        private void Initasknamejob(DataTable dtjob)
        {
            DataTable dt = dtjob.DefaultView.ToTable(true, "task_name");
            lbtasknamejob.Items.Clear();
            lbtasknamejob.DataSource = dt;
            lbtasknamejob.DataValueField = dt.Columns[0].ColumnName;
            lbtasknamejob.DataTextField = dt.Columns[0].ColumnName;
            lbtasknamejob.DataBind();
            string rowfilter = "";
            if (hidJobTaskName.Value != "")
            {
                string[] array = { };
                array = hidJobTaskName.Value.Split(',');
                string task_name = "";
                for (int q = 0; q < array.Length; q++)
                {
                    if (string.IsNullOrEmpty(task_name))
                    {
                        task_name = string.Format("task_name = '{0}'", array[q]);
                    }
                    else
                    {
                        task_name += string.Format("or task_name = '{0}'", array[q]);
                    }
                }
                rowfilter = task_name;
                DataView dv = new DataView(dt);
                dv.RowFilter = rowfilter;
                for (int i = 0; i < dv.Count; i++)
                {
                    lbtasknamejob.Items.FindByText(dv[i]["task_name"].ToString()).Selected = true;
                }
            }
        }
        protected void default_Multisel(string type)
        {
            if (type == "bid")
            {
                //bid
                for (int i = 0; i < lbTaskCodeBid.Items.Count; i++)
                {
                    lbTaskCodeBid.Items[i].Selected = true;

                }
                for (int i = 0; i < lbtasknamebid.Items.Count; i++)
                {
                    lbtasknamebid.Items[i].Selected = true;
                }
                for (int i = 0; i < lbassignbid.Items.Count; i++)
                {
                    lbassignbid.Items[i].Selected = true;
                }
            }
            if (type == "job")
            {
                //job
                for (int i = 0; i < lbTaskCodeJob.Items.Count; i++)
                {
                    lbTaskCodeJob.Items[i].Selected = true;
                }
                for (int i = 0; i < lbtasknamejob.Items.Count; i++)
                {
                    lbtasknamejob.Items[i].Selected = true;
                }
                for (int i = 0; i < lbassignjob.Items.Count; i++)
                {
                    lbassignjob.Items[i].Selected = true;
                }
            }

        }
        private void Picklbbid()
        {
            string taskcodebid = "";
            foreach (ListItem item in lbTaskCodeBid.Items)
            {
                if (item.Selected)
                {
                    if (taskcodebid == "")
                    {
                        taskcodebid = item.Text;
                    }
                    else
                    {
                        taskcodebid += "|" + item.Text;
                    }
                }
            }
            hidsbid.Value = taskcodebid;

            string tasknamebid = "";
            foreach (ListItem item in lbtasknamebid.Items)
            {
                if (item.Selected)
                {
                    if (tasknamebid == "")
                    {
                        tasknamebid = item.Text;
                    }
                    else
                    {
                        tasknamebid += "," + item.Text;
                    }
                }
            }
            hidBidTaskName.Value = tasknamebid;

            string taskstatusbid = "";
            foreach (ListItem item in lbstatusbid.Items)
            {
                if (item.Selected)
                {
                    if (taskstatusbid == "")
                    {
                        taskstatusbid = item.Value;
                    }
                    else
                    {
                        taskstatusbid += "," + item.Value;
                    }
                }
            }
            hidBidTaskStatus.Value = taskstatusbid;

            string assignbid = "";
            foreach (ListItem item in lbassignbid.Items)
            {
                if (item.Selected)
                {
                    if (assignbid == "")
                    {
                        assignbid = item.Text;
                    }
                    else
                    {
                        assignbid += "," + item.Text;
                    }
                }
            }
            hidBidAssignStatus.Value = assignbid;
        }
        private void Picklbjob()
        {
            string taskcodejob = "";
            foreach (ListItem item in lbTaskCodeJob.Items)
            {
                if (item.Selected)
                {
                    if (taskcodejob == "")
                    {
                        taskcodejob = item.Text;
                    }
                    else
                    {
                        taskcodejob += "|" + item.Text;
                    }
                }
            }
            hidsjob.Value = taskcodejob;

            string tasknamejob = "";
            foreach (ListItem item in lbtasknamejob.Items)
            {
                if (item.Selected)
                {
                    if (tasknamejob == "")
                    {
                        tasknamejob = item.Text;
                    }
                    else
                    {
                        tasknamejob += "," + item.Text;
                    }
                }
            }
            hidJobTaskName.Value = tasknamejob;

            string taskstatusjob = "";
            foreach (ListItem item in lbstatusjob.Items)
            {
                if (item.Selected)
                {
                    if (taskstatusjob == "")
                    {
                        taskstatusjob = item.Value;
                    }
                    else
                    {
                        taskstatusjob += "|" + item.Value;
                    }
                }
            }
            hidJobTaskStatus.Value = taskstatusjob;

            string assignjob = "";
            foreach (ListItem item in lbassignjob.Items)
            {
                if (item.Selected)
                {
                    if (assignjob == "")
                    {
                        assignjob = item.Text;
                    }
                    else
                    {
                        assignjob += "," + item.Text;
                    }
                }
            }
            hidJobAssignStatus.Value = assignjob;

        }
        protected void btnfilterbid_Click(object sender, EventArgs e)
        {
            Clearhiddenfield("bid");
            Picklbbid();
            SaveCheck();
            filterP6("bid");
            //checkassigneestatusgv1();
            //filterassignbid();
            btnAssignBid.Enabled = false;
            TabBid.Attributes.Add("class", "tab-pane active");
            TabJob.Attributes.Add("class", "tab-pane");
            liTabBid.Attributes.Add("class", "active");
            liTabJob.Attributes.Remove("class");
        }
        protected void btnfilterjob_Click(object sender, EventArgs e)
        {
            Clearhiddenfield("job");
            Picklbjob();
            SaveCheck();
            filterP6("job");
            btnAssignJob.Enabled = false;
            if (ViewState["first"].ToString() != "1")
            {
                TabJob.Attributes.Add("class", "tab-pane active");
                TabBid.Attributes.Add("class", "tab-pane");
                liTabJob.Attributes.Add("class", "active");
                liTabBid.Attributes.Remove("class");
            }


        }

        private void SaveCheck()
        {
            //gv1
            string checkbidid = "";
            for (int i = 0; i < gv1.Rows.Count; i++)
            {
                var cb = ((CheckBox)gv1.Rows[i].FindControl("gv1CheckBox")).Checked;
                GridView gv1Sub = (GridView)gv1.Rows[i].FindControl("gv1Sub");
                foreach (GridViewRow gvr in gv1Sub.Rows)
                {
                    //var cb = ((CheckBox)gvr.FindControl("gv1CheckBox")).Checked;
                    var bidtaskid = ((Label)gvr.FindControl("gv1taskid")).Text;

                    if (cb == true)
                    {
                        if (checkbidid == "")
                        {
                            checkbidid = bidtaskid;
                        }
                        else
                        {
                            checkbidid += "," + bidtaskid;
                        }
                    }
                }

            }
            hidCheckedBid.Value = checkbidid;
            //gv2
            string checkjobid = "";
            for (int i = 0; i < gv2.Rows.Count; i++)
            {
                var cb = ((CheckBox)gv2.Rows[i].FindControl("gv2CheckBox")).Checked;
                GridView gv2Sub = (GridView)gv2.Rows[i].FindControl("gv2Sub");
                foreach (GridViewRow gvr in gv2Sub.Rows)
                {
                    var jobtaskid = ((Label)gvr.FindControl("gv2taskid")).Text;
                    if (cb == true)
                    {
                        if (checkjobid == "")
                        {
                            checkjobid = jobtaskid;
                        }
                        else
                        {
                            checkjobid += "," + jobtaskid;
                        }
                    }
                }
            }
            hidCheckedJob.Value = checkjobid;
        }
        private void checkassigneestatusgv1()
        {
            string xtask_id = "";
            string sql = "";
            DropDownList ddlstaff;
            DataTable dtbid = Session["ssdtbid"] as DataTable;
            foreach (GridViewRow dgi in gv1.Rows)
            {
                GridView gv1Sub = (GridView)dgi.FindControl("gv1Sub");
                foreach (GridViewRow gvr in gv1Sub.Rows)
                {
                    CheckBox cb = ((CheckBox)gvr.FindControl("gv1CheckBox"));
                    bool cstatus = ((Label)gvr.FindControl("gv1statuscode")).Text.Equals("TK_Complete");

                    if (cstatus)
                    {
                        cb.Visible = false;
                        //lbedit.Visible = false;
                    }
                }
            }
        }
        private void Clearhiddenfield(string bidjob)
        {
            if (bidjob == "bid")
            {
                hidsbid.Value = string.Empty;
                hidBidTaskName.Value = string.Empty;
                hidBidTaskStatus.Value = string.Empty;
                hidBidAssignStatus.Value = string.Empty;
            }
            else
            {
                hidsjob.Value = string.Empty;
                hidJobTaskName.Value = string.Empty;
                hidJobTaskStatus.Value = string.Empty;
                hidJobAssignStatus.Value = string.Empty;
            }
        }
        protected void gv1Sub_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                //CheckBox cb = ((CheckBox)e.Row.FindControl("gv1CheckBox"));
                //bool cstatus = ((Label)e.Row.FindControl("gv1statuscode")).Text.Equals("TK_Complete");

                //if (cstatus) cb.Visible = false;
            }
        }
        protected void gv2Sub_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //bool chk = true;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //int a = e.Row.RowIndex + 1;
                //CheckBox cb = ((CheckBox)gv2.Rows[a].FindControl("gv2CheckBox"));
                //bool cstatus = ((Label)e.Row.FindControl("gv2statuscode")).Text.Equals("TK_Complete");
                //if (cstatus) cb.Visible = false;   
            }
        }
        private void ini_form()
        {

            if (hidWorkID.Value == "") // Start Workflow
            {
                // get Approval Persons
                EmpInfoClass empInfo = new EmpInfoClass();
                var emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidRequestorId.Value = emp.PERNR;
                hidReviewerId.Value = emp.SECTION_HEAD_ID;
                hidApproverId.Value = emp.MANAGER_ID;
                hidDirectorId.Value = emp.DIRECTOR_ID;
                ini_ddlassignto();
            }
            else
            {

            }
        }
        private void ini_ddlassignto()
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);
            DataTable dtStaff = new DataTable();
            dtStaff.Columns.Add("assign_id", typeof(string));
            dtStaff.Columns.Add("assign_name", typeof(string));

            //Get staff list from AD
            //string[] staffList = empInfo.getEmpListInDepartment_WithoutMD(emp.ORGSNAME4); //
            string[] staffList = empInfo.getEmpIDAndNameDepHead(emp.ORGSNAME3);
            string[] staffList2 = empInfo.getEmpIDAndNameSectionHead(emp.ORGSNAME4);

            //Bind to dropdown list
            foreach (string staffInfo in staffList)
            {
                string[] staff = staffInfo.Split(':');
                DataRow dr = dtStaff.Rows.Add();
                dr["assign_id"] = staff[0];
                dr["assign_name"] = staff[1] + "(" + staff[2] + ")" + "(" + staff[0] + ")";
                dtStaff.AcceptChanges();
            }
            foreach (string staffInfo in staffList2)
            {
                string[] staff = staffInfo.Split(':');
                DataRow dr = dtStaff.Rows.Add();
                dr["assign_id"] = staff[0];
                dr["assign_name"] = staff[1] + "(" + staff[2] + ")" + "(" + staff[0] + ")";
                dtStaff.AcceptChanges();
            }
            try
            {
                //DataRow dr = dtStaff.NewRow();
                //dr["assign_name"] = "Please Select Staff";
                //dr["assign_id"] = "";
                //dtStaff.Rows.InsertAt(dr, 0);

                ddlBidAssignTo.Items.Clear();
                ddlBidAssignTo.DataSource = dtStaff;
                ddlBidAssignTo.DataValueField = "assign_id";
                ddlBidAssignTo.DataTextField = "assign_name";
                ddlBidAssignTo.DataBind();

                ddlJobAssignTo.Items.Clear();
                ddlJobAssignTo.DataSource = dtStaff;
                ddlJobAssignTo.DataValueField = "assign_id";
                ddlJobAssignTo.DataTextField = "assign_name";
                ddlJobAssignTo.DataBind();
            }
            catch (Exception ex)
            {
                LogHelper.Write("ERRMSG : " + ex.Message);
            }
            Session.Add("ssdtStaff", dtStaff);

        }
        private void CheckMode()
        {
            string mode = string.Empty;
            if (!string.IsNullOrEmpty(hidLogin.Value))
            {
                EmpInfoClass empInfo = new EmpInfoClass();
                var emp = empInfo.getInFoByEmpID(hidLogin.Value);

                if (emp.POSITION.Contains("หัวหน้ากอง"))
                {
                    hidMode.Value = "Manager";
                }
                else if (emp.POSITION.Contains("หัวหน้าแผนก"))
                {
                    hidMode.Value = "Section";
                }
                else
                {
                    hidMode.Value = "Section";
                }
            }
        }
        protected void btnAssignBid_Click(object sender, EventArgs e)
        {

            string staff_center = string.Empty;
            bool ff = false;
            EmpInfoClass empInfo = new EmpInfoClass();
            foreach (GridViewRow dgi in gv1.Rows)
            {
                int index = dgi.RowIndex;
                bool cb = ((CheckBox)dgi.FindControl("gv1CheckBox")).Checked;
                if (cb == true)
                {
                    clickRowCommand(index, "bid");
                    //DataTable dt = Session["ssdtbid"] as DataTable;
                    GridView gv1Sub = (GridView)dgi.FindControl("gv1Sub");
                    gv1Sub.Visible = true;
                    foreach (GridViewRow gvr in gv1Sub.Rows)
                    {
                        string taskid = ((Label)gvr.FindControl("gv1taskid")).Text;
                        string taskcode = ((Label)gvr.FindControl("gv1Task_code")).Text;
                        string taskname = ((Label)gvr.FindControl("gv1Task_name")).Text;
                        string Date_start = ((Label)gvr.FindControl("gv1hidStartDate")).Text;
                        string Date_end = ((Label)gvr.FindControl("gv1hidEndDate")).Text;
                        //DataRow[] dr = dt.Select("task_id = " + taskid + "");
                        if (gvr.Visible == true && ddlBidAssignTo.SelectedValue != "")
                        {
                            foreach (ListItem item in ddlBidAssignTo.Items)
                            {
                                if (item.Selected)
                                {
                                    //saveDataBid(index, ddlBidAssignTo.SelectedValue, taskid, taskcode, taskname, Date_start, Date_end);
                                    saveDataBid(index, item.Value, taskid, taskcode, taskname, Date_start, Date_end);
                                    ff = true;

                                }
                            }

                        }
                    }
                }
            }


            if (ff)
            {
                Response.Write("<script> alert('Assign to staff successfully.');</script>");
                MailNoti mailNoti = new MailNoti();
                mailNoti.SendMailOut(hidTaskAssi.Value, "", "bid");
            }
            else
            {
                Response.Write("<script> alert('Please select task to assign.');</script>");
            }
            //p6Class.Logout();

            Clearhiddenfield();
            Clearselectlb("bid");
            Setview("bid");
            filterP6("bid");
            //bindddlstaff("bid");
            //checkassigneestatusgv1();
            //Set view
            if (hidMode.Value.ToString().ToLower() == "section")
            {
                ShowOnlyAssign();
            }
            else
            {
                ShowOnlyNotAssign("bid");
            }

            Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);

            ddlBidAssignTo.SelectedIndex = 0;
            btnAssignBid.Enabled = false;
            TabBid.Attributes.Add("class", "tab-pane active");
            TabJob.Attributes.Add("class", "tab-pane");
            liTabBid.Attributes.Add("class", "active");
            liTabJob.Attributes.Remove("class");
            hidTaskAssi.Value = "";
        }
        protected void btnAssignJob_Click(object sender, EventArgs e)
        {
            //p6Class.AuthP6();
            bool ff = false;
            foreach (GridViewRow dgi in gv2.Rows)
            {
                int index = dgi.RowIndex;
                bool cb = ((CheckBox)dgi.FindControl("gv2CheckBox")).Checked;
                if (cb == true)
                {
                    clickRowCommand(index, "job");
                    GridView gv2Sub = (GridView)dgi.FindControl("gv2Sub");
                    gv2Sub.Visible = true;
                    foreach (GridViewRow gvr in gv2Sub.Rows)
                    {

                        string taskid = ((Label)gvr.FindControl("gv2taskid")).Text;
                        string taskcode = ((Label)gvr.FindControl("gv2Task_code")).Text;
                        string taskname = ((Label)gvr.FindControl("gv2Task_name")).Text;
                        string Date_start = ((Label)gvr.FindControl("gv2hidStartDate")).Text;
                        string Date_end = ((Label)gvr.FindControl("gv2hidEndDate")).Text;

                        if (gvr.Visible == true && ddlJobAssignTo.SelectedValue != "")
                        {
                            foreach (ListItem item in ddlJobAssignTo.Items)
                            {
                                if (item.Selected)
                                {
                                    saveDataJob(index, item.Value, taskid, taskcode, taskname, Date_start, Date_end);
                                    ff = true;
                                }
                            }
                        }
                    }
                }
            }
            if (ff)
            {
                Response.Write("<script> alert('Assign to staff successfully.');</script>");
                MailNoti mailNoti = new MailNoti();
                mailNoti.SendMailOut(hidTaskAssi.Value, "", "job");
            }
            else
            {
                Response.Write("<script> alert('Please select task to assign.');</script>");
            }
            //p6Class.Logout();

            Clearhiddenfield();
            Clearselectlb("job");
            Setview("job");
            filterP6("job");
            //bindddlstaff("job");
            //checkassigneestatusgv2();
            //Set view 
            if (hidMode.Value.ToString().ToLower() == "section")
                ShowOnlyAssign();
            else
                ShowOnlyNotAssign("job");

            Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            ddlJobAssignTo.SelectedIndex = 0;
            btnAssignJob.Enabled = false;
            TabJob.Attributes.Add("class", "tab-pane active");
            TabBid.Attributes.Add("class", "tab-pane");
            liTabJob.Attributes.Add("class", "active");
            liTabBid.Attributes.Remove("class");
            hidTaskAssi.Value = "";
        }
        private void saveDataBid(int rowindex, string emp_id, string taskid, string taskcode, string taskname, string Date_start, string Date_end)
        {
            //string taskid = ((Label)gv1.Rows[rowindex].FindControl("gv1taskid")).Text;
            //string taskcode = ((Label)gv1.Rows[rowindex].FindControl("gv1Task_code")).Text;
            string xstaff_center = "";
            string sql = "";
            sql = " delete from wf_tc_taskassignment where task_id = '" + taskid + "' and task_code = '" + taskcode + "' and staff_id ='" + emp_id + "'";
            zdbUtil.ExecNonQuery(sql, zetl4eisdb);

            sql = " delete from ps_t_p6_task_assign where p6_task_id = '" + taskid + "' and assign_id ='" + emp_id + "'";
            zdbUtil.ExecNonQuery(sql, zetl4eisdb);

            //string xempid = ((DropDownList)(gv1.Rows[rowindex].FindControl("gv1ddlAssignTo"))).SelectedValue;
            string xempid = emp_id;

            //if (string.IsNullOrEmpty(xempid))
            //{
            //    xempid = ddlBidAssignTo.SelectedValue;
            //}
            string xassign_type = "Center";
            if (!string.IsNullOrEmpty(xempid))
            {
                xstaff_center = xempid;
                p6Class.ActionUDFstaff(int.Parse(taskid), xstaff_center); //Add Staff To P6
                saveAssignnee(xempid, xassign_type, rowindex, "gv1", taskid, taskname, taskcode);
                saveP6assign(xempid, xassign_type, rowindex, "gv1", taskid, taskname, Date_start, Date_end);
            }

            if (hidTaskAssi.Value == "")
            {
                hidTaskAssi.Value = "'" + taskcode + "'";
            }
            else
            {
                if (hidTaskAssi.Value.IndexOf(taskcode) > -1) { }
                else
                {
                    hidTaskAssi.Value += ", '" + taskcode + "'";
                }
            }

        }
        private void saveDataJob(int rowindex, string emp_id, string taskid, string taskcode, string taskname, string Date_start, string Date_end)
        {
            //string taskid = ((Label)gv2.Rows[rowindex].FindControl("gv2taskid")).Text;
            //string taskcode = ((Label)gv2.Rows[rowindex].FindControl("gv2Task_code")).Text;
            string xstaff_center = "";
            string sql = "";
            sql = " delete from wf_tc_taskassignment where task_id = '" + taskid + "' and task_code = '" + taskcode + "' and staff_id ='" + emp_id + "'";
            zdbUtil.ExecNonQuery(sql, zetl4eisdb);

            sql = " delete from ps_t_p6_task_assign where p6_task_id = '" + taskid + "' and assign_id ='" + emp_id + "'";
            zdbUtil.ExecNonQuery(sql, zetl4eisdb);

            //string xempid = ((DropDownList)(gv2.Rows[rowindex].FindControl("gv2ddlAssignTo"))).SelectedValue;
            string xempid = emp_id;
            //if (string.IsNullOrEmpty(xempid))
            //{
            //    xempid = ddlJobAssignTo.SelectedValue;
            //}
            string xassign_type = "Center";
            if (!string.IsNullOrEmpty(xempid))
            {
                xstaff_center = xempid;
                p6Class.ActionUDFstaff(int.Parse(taskid), xstaff_center); //Add Staff To P6
                saveAssignnee(xempid, xassign_type, rowindex, "gv2", taskid, taskname, taskcode);
                saveP6assign(xempid, xassign_type, rowindex, "gv2", taskid, taskname, Date_start, Date_end);
            }



            if (hidTaskAssi.Value == "")
            {
                hidTaskAssi.Value = "'" + taskcode + "'";
            }
            else
            {
                if (hidTaskAssi.Value.IndexOf(taskcode) > -1) { }
                else
                {
                    hidTaskAssi.Value += ", '" + taskcode + "'";
                }
                //hidTaskAssi.Value += ",'" + taskcode + "'";
            }

        }
        private void saveAssignnee(string xemp_id, string xassign_type, int rowindex, string gv, string taskid, string taskname, string taskcode)
        {
            EmpInfoClass empinfostaff = new EmpInfoClass();
            Employee empstaff = empinfostaff.getInFoByEmpID(xemp_id);
            EmpInfoClass empinfoassign = new EmpInfoClass();
            Employee empassign = empinfoassign.getInFoByEmpID(hidLogin.Value);
            string bidno = string.Empty;
            string jobno = string.Empty;
            string spBid = "";
            string spJob = "";
            string sub = "";


            GridView gridView;
            if (gv == "gv1")
            {
                gridView = gv1;
                bidno = ((Label)gridView.Rows[rowindex].FindControl("gv1bid_no")).Text;
                //spBid = bidno.Replace("B-", "");
                string[] arrbid = bidno.Replace("B-", "").Split('-');
                if (arrbid.Length >= 3)
                    spBid = arrbid[0] + "-" + arrbid[1].Replace("0", "") + "-" + arrbid[2];
            }
            else
            {
                gridView = gv2;
                jobno = ((Label)gridView.Rows[rowindex].FindControl("gv2Job_no")).Text;
                if (jobno != "")
                {
                    sub = jobno.Substring(0, 1);
                    if (sub.ToUpper() == "J") spJob = jobno.Replace("J-", "");
                    else if (sub.ToUpper() == "R") spJob = jobno.Replace("R-", "");

                    string[] arrjob = spJob.Split('-');
                    if (arrjob.Length >= 3)
                        spJob = arrjob[0] + "-" + arrjob[1].Replace("0", "") + "-" + arrjob[2];

                }
            }
            string projectname = ((Label)gridView.Rows[rowindex].FindControl("" + gv + "wbs_name")).Text;
            //string taskid = ((Label)gridView.Rows[rowindex].FindControl("" + gv + "taskid")).Text;
            //string taskcode = ((Label)gridView.Rows[rowindex].FindControl("" + gv + "Task_code")).Text;
            //string taskname = ((Label)gridView.Rows[rowindex].FindControl("" + gv + "task_name")).Text;
            string joblocal = string.Empty;
            string dtstype = string.Empty;
            string orasql = @"select proj_id from admuser.task
                                where task_id = " + taskid + "";
            string dt = zordbUtil._getDS(orasql, zp6db).Tables[0].Rows[0][0].ToString();
            if (!string.IsNullOrEmpty(dt))
            {
                joblocal = p6Class.getlocaljob(int.Parse(dt));
                dtstype = p6Class.GetDTSType(int.Parse(dt));
            }
            string drawing = p6Class.GetDrawing(taskid);
            string location_code = "";//p6Class.ReadSubLineName(int.Parse(taskid)).comment;
            string sql = string.Empty;
            sql = @" insert into wf_tc_taskassignment(job_no, bid_no, project_name, task_id, task_code, task_name, job_local, drawing, dts_type, location_code, staff_id, staff_name
                            ,staff_position, staff_position_full, assign_type, assignby_id, assignby_name, assignby_position, assignby_position_full, assignstatus, assigned_datetime)
                            values  ( ";
            //1
            sql += "'" + spJob + "',";
            sql += "'" + spBid + "',";
            sql += "'" + projectname + "',";
            sql += "'" + taskid + "',";
            sql += "'" + taskcode + "',";
            sql += "'" + taskname + "',";
            sql += "'" + joblocal + "',";
            sql += "'" + drawing + "',";
            sql += "'" + dtstype + "',";
            sql += "'" + location_code + "',";
            sql += "'" + empstaff.PERNR + "',";
            sql += "'" + empstaff.SNAME + "',";
            sql += "'" + empstaff.MC_SHORT + "',";
            sql += "'" + empstaff.POSITION + "',";
            sql += "'" + xassign_type + "',";
            sql += "'" + empassign.PERNR + "',";
            sql += "'" + empassign.SNAME + "',";
            sql += "'" + empassign.MC_SHORT + "', ";
            sql += "'" + empassign.POSITION + "', ";
            sql += "'Complete', ";
            sql += "'" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' ) ";

            zdbUtil.ExecNonQuery(sql, zetl4eisdb);
        }
        private void saveP6assign(string xemp_id, string xassign_type, int rowindex, string gv, string taskid, string taskname, string Date_start, string Date_end)
        {
            DataTable dt = new DataTable();
            EmpInfoClass empinfostaff = new EmpInfoClass();
            Employee empstaff = empinfostaff.getInFoByEmpID(xemp_id);
            EmpInfoClass empinfoassign = new EmpInfoClass();
            Employee empassign = empinfoassign.getInFoByEmpID(hidLogin.Value);
            string bidno = string.Empty;
            string jobno = string.Empty;
            string spBid = "";
            string spJob = "";
            string sub = "";

            //
            //string Date_start = "";
            //string Date_end = "";
            //string task_name = "";

            GridView gridView;
            if (gv == "gv1")
            {
                gridView = gv1;
                bidno = ((Label)gridView.Rows[rowindex].FindControl("gv1bid_no")).Text;
                //Date_start = ((Label)gridView.Rows[rowindex].FindControl("gv1hidStartDate")).Text;
                //Date_end = ((Label)gridView.Rows[rowindex].FindControl("gv1hidEndDate")).Text;
                //task_name = ((Label)gridView.Rows[rowindex].FindControl("gv1Task_name")).Text;
                string[] arrbid = bidno.Replace("B-", "").Split('-');
                if (arrbid.Length >= 3)
                    spBid = arrbid[0] + "-" + arrbid[1].Replace("0", "") + "-" + arrbid[2];


            }
            else
            {
                gridView = gv2;
                jobno = ((Label)gridView.Rows[rowindex].FindControl("gv2Job_no")).Text;
                //Date_start = ((Label)gridView.Rows[rowindex].FindControl("gv2hidStartDate")).Text;
                //Date_end = ((Label)gridView.Rows[rowindex].FindControl("gv2hidEndDate")).Text;
                //task_name = ((Label)gridView.Rows[rowindex].FindControl("gv2Task_name")).Text;
                if (jobno != "")
                {
                    sub = jobno.Substring(0, 1);
                    if (sub.ToUpper() == "J") spJob = jobno.Replace("J-", "");
                    else if (sub.ToUpper() == "R") spJob = jobno.Replace("R-", "");

                    string[] arrjob = spJob.Split('-');
                    if (arrjob.Length >= 3)
                        spJob = arrjob[0] + "-" + arrjob[1].Replace("0", "") + "-" + arrjob[2];
                }
            }
            //tring taskid = ((Label)gridView.Rows[rowindex].FindControl("" + gv + "taskid")).Text;
            string sql = string.Empty;
            string jrev = "";
            string brev = "";

            sql = @" insert into ps_t_p6_task_assign(job_no
                          ,job_revision
                          ,bid_no
                          ,bid_revision
                          ,p6_task_id
                          ,p6_task_name
                          ,p6_plan_start
                          ,p6_plan_end
                          ,assign_id
                          ,created_by
                          ,created_datetime
                          ,updated_by
                          ,updated_datetime)
                            values  ( ";
            //1
            sql += "'" + spJob + "',";
            sql += "'" + jrev + "',";
            sql += "'" + spBid + "',";
            sql += "'" + brev + "',";
            sql += "'" + taskid + "',";
            sql += "'" + taskname + "',";
            sql += "'" + Date_start + "',";
            sql += "'" + Date_end + "',";
            sql += "'" + empstaff.PERNR + "',";
            sql += "'" + hidLogin.Value + "',";
            sql += "'" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "',";
            sql += "'" + hidLogin.Value + "',";
            sql += "'" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "' ) ";

            zdbUtil.ExecNonQuery(sql, zetl4eisdb);

        }
        private void Clearhiddenfield()
        {
            hidCheckedBid.Value = string.Empty;
            hidsbid.Value = string.Empty;
            hidBidTaskName.Value = string.Empty;
            hidBidTaskStatus.Value = string.Empty;
            hidBidAssignStatus.Value = string.Empty;

            hidCheckedJob.Value = string.Empty;
            hidsjob.Value = string.Empty;
            hidJobTaskName.Value = string.Empty;
            hidJobTaskStatus.Value = string.Empty;
            hidJobAssignStatus.Value = string.Empty;
        }
        private void Clearselectlb(string bidjob)
        {
            if (bidjob == "bid" || string.IsNullOrEmpty(bidjob))
            {
                lbTaskCodeBid.ClearSelection();
                lbTaskCodeJob.ClearSelection();
                lbtasknamebid.ClearSelection();
                lbtasknamejob.ClearSelection();
            }
            if (bidjob == "job" || string.IsNullOrEmpty(bidjob))
            {
                lbassignbid.ClearSelection();
                lbassignjob.ClearSelection();
                lbstatusbid.ClearSelection();
                lbstatusjob.ClearSelection();
            }
        }
        private void Setview(string bidjob)
        {
            if (hidMode.Value.ToString().ToLower() == "section")
            {
                if (bidjob == "bid" || string.IsNullOrEmpty(bidjob))
                {
                    hidBidAssignStatus.Value = "Assign";
                    lbassignbid.Items[0].Selected = true;
                    hidBidTaskStatus.Value = "TK_NotStart";
                    lbstatusbid.Items[0].Selected = true;
                    try
                    {
                        ShowOnlyAssign();

                    }
                    catch { }

                }
                if (bidjob == "job" || string.IsNullOrEmpty(bidjob))

                {
                    hidJobAssignStatus.Value = "Assign";
                    lbassignjob.Items[0].Selected = true;
                    hidJobTaskStatus.Value = "TK_NotStart";
                    lbstatusjob.Items[0].Selected = true;
                    try
                    {
                        ShowOnlyAssign();
                    }
                    catch { }

                }

            }
            if (hidMode.Value.ToString().ToLower() == "manager")
            {
                if (bidjob == "bid" || string.IsNullOrEmpty(bidjob))
                {
                    hidBidAssignStatus.Value = "Not Assign";
                    lbassignbid.Items[1].Selected = true;

                    hidBidTaskStatus.Value = "TK_NotStart";
                    lbstatusbid.Items[0].Selected = true;
                    try
                    {
                        ShowOnlyNotAssign(bidjob);

                    }
                    catch { }

                }
                if (bidjob == "job" || string.IsNullOrEmpty(bidjob))
                {
                    hidJobAssignStatus.Value = "Not Assign";
                    lbassignjob.Items[1].Selected = true;
                    hidJobTaskStatus.Value = "TK_NotStart";
                    lbstatusjob.Items[0].Selected = true;
                    try
                    {
                        ShowOnlyNotAssign(bidjob);

                    }
                    catch { }
                }
            }

        }
        private void ShowOnlyNotAssign(string bidjob)
        {
            if (bidjob == "bid" || string.IsNullOrEmpty(bidjob))
            {
                foreach (GridViewRow dgi in gv1.Rows)
                {
                    GridView gv1Sub = (GridView)dgi.FindControl("gv1Sub");

                    foreach (GridViewRow grv in gv1Sub.Rows)
                    {
                        string xassign_status = ((Label)(grv.FindControl("gv1Status"))).Text;
                        if (xassign_status != "Not Assign")
                        {
                            grv.Visible = false;
                        }
                    }
                }

            }
            if (bidjob == "job" || string.IsNullOrEmpty(bidjob))
            {
                foreach (GridViewRow dgi in gv2.Rows)
                {
                    GridView gv2Sub = (GridView)dgi.FindControl("gv2Sub");

                    foreach (GridViewRow grv in gv2Sub.Rows)
                    {
                        string xassign_status = ((Label)(grv.FindControl("gv2Status"))).Text;

                        if (xassign_status != "Not Assign")
                        {
                            grv.Visible = false;
                        }
                    }
                }
            }
        }
        private void ShowOnlyAssign()
        {
            foreach (GridViewRow dgi in gv1.Rows)
            {
                GridView gv1Sub = (GridView)dgi.FindControl("gv1Sub");

                foreach (GridViewRow grv in gv1Sub.Rows)
                {
                    string xassign_status = ((Label)(grv.FindControl("gv1Status"))).Text;
                    if (xassign_status != "Assign")
                    {
                        grv.Visible = false;
                    }
                }

            }
            foreach (GridViewRow dgi in gv2.Rows)
            {
                GridView gv2Sub = (GridView)dgi.FindControl("gv2Sub");

                foreach (GridViewRow grv in gv2Sub.Rows)
                {
                    string xassign_status = ((Label)(grv.FindControl("gv2Status"))).Text;

                    if (xassign_status != "Assign")
                    {
                        grv.Visible = false;
                    }
                }
            }
        }
        protected void btnClearbid_Click(object sender, EventArgs e)
        {
            hidCheckedBid.Value = string.Empty;
            Clearselectlb("bid");
            Clearhiddenfield();
            //Setview("bid");
            Picklbbid();
            //Picklbjob();
            filterP6(string.Empty);
            //bindddlstaff(string.Empty);
            //checkassigneestatusgv1();
            for (int i = 0; i < lbTaskCodeBid.Items.Count; i++)
            {
                lbTaskCodeBid.Items[i].Selected = false;

            }
            for (int i = 0; i < lbtasknamebid.Items.Count; i++)
            {
                lbtasknamebid.Items[i].Selected = false;
            }
            //DataTable dtlbb = dtddlbid();
            //inilbtaskcodebid(dtlbb);
            //Initasknamebid(dtlbb);

            Setview("bid");
            ddlBidAssignTo.SelectedIndex = 0;
            btnAssignBid.Enabled = false;
            TabBid.Attributes.Add("class", "tab-pane active");
            TabJob.Attributes.Add("class", "tab-pane");
            liTabBid.Attributes.Add("class", "active");
            liTabJob.Attributes.Remove("class");
        }
        protected void btnClearjob_Click(object sender, EventArgs e)
        {
            hidCheckedJob.Value = string.Empty;
            Clearselectlb("job");
            Clearhiddenfield();
            //Setview("job");
            //Picklbbid();
            Picklbjob();
            filterP6(string.Empty);
            //bindddlstaff(string.Empty);
            //checkassigneestatusgv2();
            //DataTable dtlbj = dtddljob();
            //inilbtaskcodejob(dtlbj);
            //Initasknamejob(dtlbj);
            for (int i = 0; i < lbTaskCodeJob.Items.Count; i++)
            {
                lbTaskCodeJob.Items[i].Selected = false;
            }
            for (int i = 0; i < lbtasknamejob.Items.Count; i++)
            {
                lbtasknamejob.Items[i].Selected = false;
            }
            Setview("job");
            ddlJobAssignTo.SelectedIndex = 0;
            btnAssignJob.Enabled = false;
            TabJob.Attributes.Add("class", "tab-pane active");
            TabBid.Attributes.Add("class", "tab-pane");
            liTabJob.Attributes.Add("class", "active");
            liTabBid.Attributes.Remove("class");
        }

    }
}