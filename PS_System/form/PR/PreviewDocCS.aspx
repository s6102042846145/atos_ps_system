﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PreviewDocCS.aspx.cs" Inherits="PS_System.form.PR.PreviewDocCS" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" style="text-align:center; width:95%; height:1200px" runat="server">
        <div style="text-align:center; width:100%; height:100%">
            <iframe id="docview" src="" runat="server" width="100%"  height="100%"></iframe>
            <br />
            <%--ดาวน์โหลดเอกสารกรุณาคลิ้ก--%>&nbsp;<asp:LinkButton ID="btnDownloadFile" runat="server" Text="ที่นี่" OnClick="btnDownloadFile_Click" Visible="False"></asp:LinkButton>
            <br />
            <asp:Button ID="btnEdit" runat="server" Text="Edit" Width="100px" Visible="False" /><asp:Button ID="btnSubmit" runat="server" Text="Submit" Width="100px" Visible="False" />
            <asp:Button ID="btnClose" runat="server" Text="Cancel" OnClick="btnClose_Click" Visible="False" Width="100px" />
        </div>
    </form>
</body>
</html>
