﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="ucEmp2.ascx.cs" Inherits="PS_System.form.PR.ucEmp2" %>
<style type="text/css">
    .auto-style1 {
        width: 500px;
        font-family:Tahoma;
        font-size:10pt;
        color: black;
    }
</style>

<table class="auto-style1">
    <tr>
        <td>
            <asp:TextBox ID="txtSPosition" runat="server" Width="200px"></asp:TextBox>
            <asp:Button ID="btnClearText" runat="server" Font-Names="tahoma" Font-Size="8pt" OnClick="btnClearText_Click" Text="[X]" Visible="False" />
            <asp:ImageButton ID="ibtnSearch" runat="server" Height="20px" ImageUrl="~/images/search.png" OnClick="ibtnSearch_Click" />
        </td>
        <td>
            <asp:Label ID="Label2" runat="server" Text="(กรุณาระบุเป็นชื่อย่อตำแหน่ง)" ForeColor="Gray"></asp:Label>
        </td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="5">
            <asp:Panel ID="p1" runat="server" ScrollBars="Vertical" Width="800px" Height="120px">
            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="False" Width="760px"
                OnRowCommand="gv1_OnRowCommand"
                >
                <Columns>
                    <asp:TemplateField HeaderText="Select">
                        <ItemTemplate>
                            <asp:ImageButton ID="gvbtnSelect" runat="server"  CommandName="selectdata" CommandArgument='<%# Container.DataItemIndex %>' Height="22px" ImageUrl="~/images/edit.png" ToolTip="Select data" />
                        </ItemTemplate>
                         <ItemStyle Width="80px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="รหัส">
                        <ItemTemplate>
                            <asp:Label ID="gvEmpID" runat="server" Text='<%# Bind("emp_id") %>' Font-Names="tahoma" Font-Size="9pt" ></asp:Label>
                        </ItemTemplate>
                         <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อ-นามสกุล">
                        <ItemTemplate>
                            <asp:Label ID="gvEmpName" runat="server" Text='<%# Bind("emp_name") %>' Font-Names="tahoma" Font-Size="9pt" ></asp:Label>
                        </ItemTemplate>
                         <ItemStyle Width="250px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อย่อตำแหน่ง">
                        <ItemTemplate>
                            <asp:Label ID="gvEmpSPosition" runat="server" Text='<%# Bind("short_position") %>' Font-Names="tahoma" Font-Size="9pt" ></asp:Label>
                        </ItemTemplate>
                         <ItemStyle Width="150px" />
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ชื่อเต็มตำแหน่ง">
                        <ItemTemplate>
                            <asp:Label ID="gvEmpFPosition" runat="server" Text='<%# Bind("full_position") %>' Font-Names="tahoma" Font-Size="9pt" ></asp:Label>
                        </ItemTemplate>
                         <ItemStyle Width="250px" />
                    </asp:TemplateField>
                </Columns>
                <HeaderStyle BackColor="#3333FF" Font-Names="tahoma" Font-Size="9pt" ForeColor="White" />
            </asp:GridView>
            </asp:Panel>
          
            <asp:HiddenField ID="hidEmpID" runat="server" />
            <asp:HiddenField ID="hidEmpName" runat="server" />
            <asp:HiddenField ID="hidEmpSPosition" runat="server" />
            <asp:HiddenField ID="hidEmpFPosition" runat="server" />
          
            <br />
        </td>
    </tr>
</table>

