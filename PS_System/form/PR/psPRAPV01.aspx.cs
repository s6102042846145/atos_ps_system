﻿using PS_Library;
using PS_Library.DocumentManagement;
using PS_Library.WorkflowService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.PR
{
    public partial class psPRAPV01 : System.Web.UI.Page
    {
        private string strParentPdfNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString(); //ConfigurationManager.AppSettings["wf_job_att_node_id"].ToString();
        private Node doc_node = new Node();

        public string zisDev = ConfigurationManager.AppSettings["isDev"].ToString();
        public string zmode = "";
        public string zworkid = "";
        public string zsubworkid = "";
        public string ztaskid = "";
        public string zpsdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string zhost_name = ConfigurationManager.AppSettings["HOST_NAME"].ToString();
        private string strWfJobMapId = ConfigurationManager.AppSettings["wf_job_map_id"].ToString();
        private string strWfBidMapId = ConfigurationManager.AppSettings["wf_job_map_id"].ToString();
        public string zwf_pr = "2993511"; 

        public EmpInfoClass zempInfo = new EmpInfoClass();
        clsWfPsAPV01 objWf = new clsWfPsAPV01();

        static PS_Library.OTFunctions zotFunctions = new PS_Library.OTFunctions();
        static PS_Library.DbControllerBase zdbUtil = new PS_Library.DbControllerBase();
        static PS_Library.LogHelper zLogHelper = new PS_Library.LogHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

            //TestStartWf();
            //TestStartWfBid();

            if (!IsPostBack)
            {

                try { zmode = Request.QueryString["zmode"].ToString().ToUpper(); }
                catch { zmode = "APV1"; }
                try { zworkid = Request.QueryString["workid"].ToString().ToUpper(); }
                catch { zworkid = ""; }
                try { zsubworkid = Request.QueryString["subworkid"].ToString().ToUpper(); }
                catch { zsubworkid = ""; }
                try { ztaskid = Request.QueryString["taskid"].ToString().ToUpper(); }
                catch { ztaskid = "1"; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); }
                catch { hidLogin.Value = "z575798"; }

                try { hidPRNo.Value = Request.QueryString["pr_no"].ToString(); }
                catch { hidPRNo.Value = ""; }

                hidMode.Value = zmode;
                hidWorkID.Value = zworkid;
                hidSubworkID.Value = zsubworkid;
                hidTaskID.Value = ztaskid;
                //Debug
                if (hidPRNo.Value == "")
                {
                    hidPRNo.Value = "PR-256400007";
                }
                // hidMode.Value = "APV1"; 
                ini_object();
                LoadData();
               
            }
        }
        private void ini_object()
        {
            ini_gvA();
            ini_gvC();
            ini_gvP();
        }
        private void LoadData()
        {
            string xpr_no = hidPRNo.Value;
            string xpdf_node_id = ""; 
            string sql = " select * from ps_t_pr where pr_no = '" + xpr_no + "' ";
            var ds= zdbUtil.ExecSql_DataSet(sql, zpsdb);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xpdf_node_id = ds.Tables[0].Rows[0]["pdf_node_id"].ToString();
                getMemo(xpdf_node_id);
            }
            loadData_header(ds.Tables[0]); 
            checkMode();
        }
        private void loadData_header(DataTable dt)
        {
            var dr = dt.Rows[0];
            lblPRNO.Text = dr["pr_no"].ToString();
            lblRequestorVal.Text = dr["from_emp_name"].ToString() + " (" + dr["from_emp_s_position"].ToString() + ") ";
            lblToVal.Text = dr["apvfinal_name"].ToString() + " (" + dr["apvfinal_s_position"].ToString() + ") ";
            lblSubjectVal.Text = dr["subject"].ToString();
            hidPRNo.Value = lblPRNO.Text;
            hidBidNo.Value = dr["bid_no"].ToString();
            hidBidRevision.Value = dr["bid_revision"].ToString();
        }
        private void loadData_price()
        {
            ddlSchedule.Items.Clear();
            string sql = "select distinct schedule_name from ps_xls_template where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value ;
            var ds = zdbUtil.ExecSql_DataSet(sql, zpsdb);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlSchedule.DataSource = ds.Tables[0];
                ddlSchedule.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                ddlSchedule.DataValueField = ds.Tables[0].Columns[0].ColumnName;
                ddlSchedule.DataBind();
                ListItem li = new ListItem();
                li.Text = "-Select Schedule-";
                li.Value = "";
                ddlSchedule.Items.Insert(0, li);
                ddlSchedule.SelectedIndex = 0;
                bind_gvP();
            }
            else { ini_gvP(); }

        }
        private void bind_gvP()
        {
            string sql = " select * from ps_xls_template where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value;
            if (ddlSchedule.SelectedIndex > 0)
            {
                sql += " and schedule_name = '" + ddlSchedule.SelectedValue.ToString() + "' ";
            }
            var ds = zdbUtil.ExecSql_DataSet(sql, zpsdb);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvP.DataSource = ds.Tables[0];
                gvP.DataBind();
            }
            else { ini_gvP(); }
        }
        private void getMemo(string xpdf_node_id)
        {
            string xdoc_url = "";
           
            string strMergedPdfNodeId = xpdf_node_id;
            xdoc_url = "https://" + zhost_name + "/tmps/forms/viewCSDoc?docnodeid=" + strMergedPdfNodeId;
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");
            ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);

        }
        private void checkMode()
        {
            p_Purchase.Visible = false;p_Account.Visible = false;
            btnApprove.Visible = false; btnAssign.Visible = false;
            btnSubmit.Visible = false;
            btnReject.Visible = false;
            lblSchedule.Visible = false;
            ddlSchedule.Visible = false;
            pgv_Price.Visible = false; 
            switch (hidMode.Value.ToUpper())
            {
                case "APV1"://approver1
                    {
                        btnApprove.Visible = true;
                        btnSubmit.Visible = false;
                        btnReject.Visible = true;
                    }; break;
                case "APV2"://approver2
                    {
                        btnApprove.Visible = true;
                        btnSubmit.Visible = false;
                        btnReject.Visible = true;
                    }; break;
                case "APV3"://approver3
                    {
                        btnApprove.Visible = true;
                        btnSubmit.Visible = false;
                        btnReject.Visible = true;
                    }; break;
                case "APVFINAL"://approver4
                    {
                        btnApprove.Visible = true;
                        btnSubmit.Visible = false;
                        btnReject.Visible = true;

                    }; break;
                case "PURCHASE_MGR_ASSIGN"://approver5
                    {
                        p_Purchase.Visible = true;
                        btnAssign.Visible = true;
                        btnReject.Visible = true;
                    }; break;
                case "PURCHASE_SECTION_ASSIGN"://approver5
                    {
                        p_Purchase.Visible = true;
                        btnAssign.Visible = true;
                        btnReject.Visible = true;
                    }; break;
                case "BUYER"://approver5
                    {
                        p_Purchase.Visible = true;
                        btnSubmit.Visible = true;
                        btnReject.Visible = true ;
                        lblSchedule.Visible = true;
                        ddlSchedule.Visible = true;
                        pgv_Price.Visible = true;
                        loadData_price();
                    }; break;
                case "PURCHASE_MGR_APV"://approver5
                    {
                        p_Purchase.Visible = true;
                        btnApprove.Visible = true;
                        btnReject.Visible = true;
                        lblSchedule.Visible = true;
                        ddlSchedule.Visible = true;
                        pgv_Price.Visible = true;
                        loadData_price();
                    }; break;
                case "PURCHASE_SECTION_APV"://approver5
                    {
                        p_Purchase.Visible = true;
                        btnApprove.Visible = true;
                        btnReject.Visible = true;
                        lblSchedule.Visible = true;
                        ddlSchedule.Visible = true;
                        pgv_Price.Visible = true;
                        loadData_price();
                    }; break;
                case "ACC_MGR_ASSIGN"://approver5
                    {
                        p_Account.Visible = true;
                        btnAssign.Visible = true;
                        btnReject.Visible = true;
                    }; break;
                case "ACC_SECTION_ASSIGN"://approver5
                    {
                        p_Account.Visible = true;
                        btnAssign.Visible = true;
                        btnReject.Visible = true;
                    }; break;
                case "ACC_STAFF"://approver5
                    {
                        p_Account.Visible = true;
                        btnSubmit.Visible = true;
                        btnReject.Visible = true;
                        lblSchedule.Visible = true;
                        ddlSchedule.Visible = true;
                        pgv_Price.Visible = true;
                        loadData_price();
                    }; break;
                case "ACC_MGR_APV"://approver5
                    {
                        p_Account.Visible = true;
                        btnApprove.Visible = true;
                        btnReject.Visible = true;
                        lblSchedule.Visible = true;
                        ddlSchedule.Visible = true;
                        pgv_Price.Visible = true;
                        loadData_price();
                    }; break;
                case "ACC_SECTION_APV"://approver5
                    {
                        p_Account.Visible = true;
                        btnApprove.Visible = true;
                        btnReject.Visible = true;
                        lblSchedule.Visible = true;
                        ddlSchedule.Visible = true;
                        pgv_Price.Visible = true;
                        loadData_price();
                    }; break;
            }

        }
        private void ini_gvA()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("itemno", typeof(string));
            dt.Columns.Add("attachtype", typeof(string));
            dt.Columns.Add("description", typeof(string));
            dt.Columns.Add("docname", typeof(string));


            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            gvA.DataSource = dt;
            gvA.DataBind();


        }
        private void ini_gvC()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("itemno", typeof(string));
            dt.Columns.Add("datetime", typeof(string));
            dt.Columns.Add("commented_by", typeof(string));
            dt.Columns.Add("commented", typeof(string));
          

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            gvC.DataSource = dt;
            gvC.DataBind();


        }
        private void ini_gvP()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("item_no", typeof(string));
            dt.Columns.Add("description", typeof(string));
            dt.Columns.Add("qty", typeof(string));
            dt.Columns.Add("unit", typeof(string));
            dt.Columns.Add("std_price", typeof(string));
            dt.Columns.Add("account_assignment_code", typeof(string));
            dt.Columns.Add("currency", typeof(string));
            dt.Columns.Add("costcenter", typeof(string));
            dt.Columns.Add("wbsno", typeof(string));

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            gvP.DataSource = dt;
            gvP.DataBind();


        }
        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                string sql = "";
                string sqlDetail = "";
                string xform_status = "";
                switch (hidMode.Value.ToUpper())
                {
                    case "APV1"://approver1
                        {
                            
                        }; break;
                    case "APV2"://approver2
                        {
                           
                        }; break;
                    case "APV3"://approver3
                        {
                           
                        }; break;
                    case "APVFINAL"://approver4
                        {
                           

                        }; break;
                    case "PURCHASE_MGR"://approver5
                        {
                          

                        }; break;
                    case "PURCHASE_SECTION"://approver5
                        {


                        }; break;
                    case "BUYER"://approver5
                        {


                        }; break;
                    case "ACC_MGR"://approver5
                        {


                        }; break;
                    case "ACC_SECTION"://approver5
                        {


                        }; break;
                    case "ACC_STAFF"://approver5
                        {


                        }; break;
                }

                // Next WORKFLOW Process
                ApplicationData[] appDatas;
                if (hidWorkID.Value == "")
                {
                    appDatas = zotFunctions.getWFStartAppData(zwf_pr.ToString());
                }
                else
                {
                    appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
                }

                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }

                if (wfAttr != null)
                {
                    if (hidMode.Value.ToUpper() == "REVIEWER") // Review by Dept. Head //approver1
                    {
                        //zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_STATUS", "APPROVED");
                        //zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver1 Approved");
                        //if (int.Parse(hidApproverCount.Value) > 1)
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER2");
                        //else
                        //{
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                        //}
                    }
                    else if (hidMode.Value.ToUpper() == "APV1") // Review by Section Head//approver2
                    {
                       
                    }
                    else if (hidMode.Value.ToUpper() == "APV2") // Review by Dept. Head//approver3
                    {
                      
                    }
                    if (hidMode.Value.ToUpper() == "APV3")//approver4
                    {
                       
                    }
                    else if (hidMode.Value.ToUpper() == "APVFINAL")//approver5
                    {
                       
                    }
                    else if (hidMode.Value.ToUpper() == "PURCHASE_MGR")//approver5
                    {

                    }
                    else if (hidMode.Value.ToUpper() == "PURCHASE_SECTION")//approver5
                    {

                    }
                    else if (hidMode.Value.ToUpper() == "BUYER")//approver5
                    {

                    }
                    else if (hidMode.Value.ToUpper() == "ACC_MGR")//approver5
                    {

                    }
                    else if (hidMode.Value.ToUpper() == "ACC_SECTION")//approver5
                    {

                    }
                    else if (hidMode.Value.ToUpper() == "ACC_STAFF")//approver5
                    {

                    }


                    zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
                   
                    Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been approved.');</script>");
                    string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                    string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
                }

                //  Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                //  Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {
            try
            {
                var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                string sql = "";
                string sqlDetail = "";
                string xform_status = "";
                switch (hidMode.Value.ToUpper())
                {
                    case "APV1"://approver1
                        {
                            xform_status = "APPROVER1 REJECTED";
                           
                        }; break;
                    case "APV2"://approver2
                        {
                            xform_status = "APPROVER2 REJECTED";
                            
                        }; break;
                    case "APV3"://approver3
                        {
                            xform_status = "APPROVER3 REJECTED";
                            
                        }; break;
                    case "APVFINAL"://approver4
                        {
                            xform_status = "APPROVER4 REJECTED";
                            

                        }; break;
                    case "PURCHASE_MGR"://approver5
                        {
                            xform_status = "APPROVER5 REJECTED";
                           

                        }; break;
                    case "PURCHASE_SECTION"://approver6
                        {
                            xform_status = "APPROVER6 REJECTED";
                          
                        }; break;
                    case "BUYER"://approver6
                        {
                            xform_status = "APPROVER6 REJECTED";

                        }; break;
                    case "ACC_MGR"://approver5
                        {
                            xform_status = "APPROVER5 REJECTED";


                        }; break;
                    case "ACC_SECTION"://approver6
                        {
                            xform_status = "APPROVER6 REJECTED";

                        }; break;
                    case "ACC_STAFF"://approver6
                        {
                            xform_status = "APPROVER6 REJECTED";

                        }; break;
                }

                zdbUtil.ExecNonQuery(sql, zpsdb);
                zdbUtil.ExecNonQuery(sqlDetail, zpsdb);

                // Next WORKFLOW Process
                ApplicationData[] appDatas;
                if (hidWorkID.Value == "")
                {
                    // appDatas = zotFunctions.getWFStartAppData(strWfJobMapId.ToString());
                    appDatas = zotFunctions.getWFStartAppData(zwf_pr.ToString());

                }
                else
                {
                    appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
                }

                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }

                if (wfAttr != null)
                {
                    if (hidMode.Value.ToUpper() == "APV1") // Review by Section Head
                    {
                        //zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW__DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW__STATUS", "Rejected");
                        //zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Reviewer Rejected");
                    }
                    else if (hidMode.Value.ToUpper() == "APV2") // Review by Dept. Head
                    {
                        
                    }
                    if (hidMode.Value.ToUpper() == "APV3")
                    {
                       
                    }
                    else if (hidMode.Value.ToUpper() == "APVFINAL")
                    {
                        
                    }
                    else if (hidMode.Value.ToUpper() == "PURCHASE_MGR")
                    {
                      

                        //updateDirector_Approve();
                    }
                    else if (hidMode.Value.ToUpper() == "PURCHASE_SECTION")
                    {


                        //updateDirector_Approve();
                    }
                    else if (hidMode.Value.ToUpper() == "BUYER")
                    {


                        //updateDirector_Approve();
                    }
                    else if (hidMode.Value.ToUpper() == "ACC_MGR")
                    {


                        //updateDirector_Approve();
                    }
                    else if (hidMode.Value.ToUpper() == "ACC_SECTION")
                    {


                        //updateDirector_Approve();
                    }
                    else if (hidMode.Value.ToUpper() == "ACC_STAFF")
                    {


                        //updateDirector_Approve();
                    }
                    zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
                    

                    Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been rejected.');</script>");

                    string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                    string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
                }

                Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
        }


        //private void TestStartWf()
        //{
        //    //--------------Test Start WF---------------//
        //    List<clsJobWf> lstWF = new List<clsJobWf>();
        //    clsWfPsAPV01 objWfPs = new clsWfPsAPV01();
        //    clsJobWf objJobWf = new clsJobWf();
        //    objJobWf.strJobNo = "TS12-11-S06";
        //    objJobWf.iJobRevision = 3;
        //    objJobWf.strActivityId = "10040";
        //    lstWF.Add(objJobWf);
        //    //string strError = objWfPs.StartWf(lstWF);
        //    //--------------Test Start WF---------------//
        //}

        //private void TestStartWfBid()
        //{
        //    //--------------Test Start WF---------------//
        //    List<clsJobWf> lstWF = new List<clsJobWf>();
        //    clsWfPsAPV01 objWfPs = new clsWfPsAPV01();
            
        //    clsJobWf objJobWf = new clsJobWf();
        //    objJobWf.strBidNo = "TS12-TX-02";
        //    objJobWf.iBidRevision = 0;
        //    objJobWf.strActivityIdBid = "90020";
        //    lstWF.Add(objJobWf);
        //    //string strError = objWfPs.StartWfBid(lstWF);
        //    //--------------Test Start WF---------------//
        //}

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {

        }
        #region gvA
        protected void gvA_OnRowCommand(object sender, CommandEventArgs e)
        {

        }
        #endregion
        #region gvC
        protected void gvC_OnRowCommand(object sender, CommandEventArgs e)
        {

        }
        #endregion
        #region gvP
        protected void gvP_OnRowCommand(object sender, CommandEventArgs e)
        {

        }
        #endregion

        protected void ddlSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            bind_gvP();
        }

        protected void btnupdate_gvP_Click(object sender, EventArgs e)
        {
            savePrice(); 
        }
        private void savePrice()
        {
            Response.Write("<script> alert('-Save successfully-');</script>");
        }
    }
}