﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.PR
{
    public partial class PreviewDocCS : System.Web.UI.Page
    {
        static OTFunctions zotFunctions = new OTFunctions();
        public string strNodeID = "";
        public string xdoc_url = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    strNodeID = Request.QueryString["nodeid"].ToString();
                    if (strNodeID != "")
                    {
                        xdoc_url = "viewCSDoc.aspx?docnodeid=" + strNodeID;

                        Response.Headers.Remove("X-Frame-Options");
                        Response.AddHeader("X-Frame-Options", "AllowAll");
                        ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);
                    }
                }
                catch
                {
                    strNodeID = "";
                }
            }
        }
        protected void btnDownloadFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (Request.QueryString["nodeid"].ToString() != "")
                {
                    var doc_node = zotFunctions.getNodeByID(Request.QueryString["nodeid"].ToString());

                    var xcontent = zotFunctions.downloadDoc(Request.QueryString["nodeid"].ToString());
                    Response.Clear();
                    Response.Buffer = true;
                    Response.Charset = "";
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.ContentType = "binary/octet-stream";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + doc_node.Name.Replace(",", "_"));
                    Response.BinaryWrite(xcontent);
                    Response.Flush();
                    Response.End();
                }
            }
            catch
            {
                strNodeID = "";
            }
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            Response.Write("<script> window.close();</script>");
        }
    }
}