﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MemoPRtemplate.aspx.cs" Inherits="PS_System.form.Bid.MemoPRtemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }

        .treeView img { 
             width:25px; 
             height:25px; 

         }

        .auto-style1 {
            width: 135px;
        }

    </style>
    
 <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
  <script >
      $(document).ready(function () {
        $('#Multi_Status').multiselect({
          includeSelectAllOption: true,
          numberDisplayed: 3,
          maxHeight: 300
         });
      });

      $(document).keypress(
          function (event) {
              if (event.which == '13') {
                  event.preventDefault();
              }
          });
     
      function openAddRecord(mode) {
          if (mode == "open") {
              document.getElementById("add_data").style.display = "block";
              document.getElementById("show_data").style.display = "none";

          }
          else {
              document.getElementById("add_data").style.display = "none";
              document.getElementById("show_data").style.display = "block";
              document.getElementById("hidTempCode").value = "";
              document.getElementById("hidStatus").value = "";
          }
          clearData();
          return true;
      }

      function clearData() {
          $('#TxtTempName').val("");
          $('#txtDesc').val("");

      }
      function clearDataModal() {
       
          $("#txtSect").val("");
          $("#txtActicle").val("");
          $("#DDLdoctype").val("");
      }
  </script>
</head>
<body>
    <form id="form1" runat="server">
         <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Memo PR Template"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
       
         <div id="show_data" runat="server" style="padding: 10px 10px 10px 10px;">
             <table class="nav-justified">
                 <tr>
                     <td class="auto-style1">Document Number</td>
                     <td>
                        <asp:TextBox ID="txtDocNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">From Request</td>
                     <td>
                        <asp:TextBox ID="txtReqBy" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">To Request</td>
                     <td>
                        <asp:TextBox ID="txtReqTo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">Request Date</td>
                     <td>
                        <asp:TextBox ID="txtReqDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">Bid No</td>
                     <td>
                        <asp:TextBox ID="txtBidNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">&nbsp;</td>
                     <td>
                        <asp:TextBox ID="txtDocNo4" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">&nbsp;</td>
                     <td>
                        <asp:TextBox ID="txtDocNo5" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">&nbsp;</td>
                     <td>
                        <asp:TextBox ID="txtDocNo6" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">&nbsp;</td>
                     <td>
                        <asp:TextBox ID="txtDocNo7" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
                 <tr>
                     <td class="auto-style1">&nbsp;</td>
                     <td>
                        <asp:TextBox ID="txtDocNo8" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="243px"></asp:TextBox></td>
                 </tr>
             </table>
        </div>
            <div id="add_data" style="display: none; padding: 10px 10px 10px 10px;" runat="server">
                <table style="width: 100%;">
                   <tr>
                    <td style="width:10%">
                        <asp:Label ID="Label1" runat="server" Text="Label">Template Name:</asp:Label>
                    </td>
                    <td style="width:17%">
                        <asp:TextBox ID="TxtTempName" runat="server" Width="250px"></asp:TextBox>
                    </td>
                    <td style="width:8%">
                        <asp:Label ID="Label2" runat="server" Text="Label">Description:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDesc" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                     <tr>
                    <td colspan="4" style="vertical-align: middle; width: 100%">
            
                        <br />
       
                            <div id="dvTree" runat="server" class="panel panel-info" style="width:fit-content;">
                             
                          <div class="panel-heading">Section</div>
                          <div class="panel-body">
                                  <asp:TreeView id="treeTemp" CssClass="treeView"  runat="server"  OnSelectedNodeChanged="treeTemp_SelectedNodeChanged" >
                                              <RootNodeStyle   Font-Size="20px" />
                                              <ParentNodeStyle Font-Size="18px"  />
                                              <LeafNodeStyle Font-Size="16px"  />
                                     
                                      </asp:TreeView>
                          </div>
                              
                            </div>  
             
                    </td>
                </tr>
                        <tr><td> <%--<asp:Button id="btncreate" runat="server" CssClass="btn btn-primary" Text="Create"/>--%>
                            <button onclick="document.getElementById('myModal').style.display='block';clearDataModal();return false;" class="btn btn-primary" >Create</button>
                            </td></tr>
                    <tr>
                        <td colspan="4" style="width: 100%;">
                             <table>
                                    <tr>
                                        <td style="padding: 5px 5px 5px 5px; text-align: left">

                                            <asp:RadioButton ID="rdbAc" runat="server" Text="Active" GroupName="searchtype"  />
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <asp:RadioButton ID="rdbInAc" runat="server" Text="In-Active" GroupName="searchtype"  />
                                        </td>
                                        
                                    </tr>
                                </table>
                        </td>
                    </tr>
                  <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Button ID="btn_save" CssClass="btn btn-primary" runat="server"
                           Text="Save" OnClick="btn_save_Click" />
                        <asp:Button ID="btnClose" CssClass="btn btn-danger" runat="server"
                            Text="Close" OnClientClick="if(openAddRecord('close')) return false;" />
                    </td>
                </tr>
                </table>
        </div>
                <div id="myModal" runat="server" class="modal">
            <!-- Modal content  -->
            <div style="width:40%;" class="modal-content" align="center">
                <br />
                <table style="margin-left:4%;">
                    <tr>
                        <td style="width:20%;">
                            <asp:Label ID="Label3" runat="server" >Section:</asp:Label>
                            &nbsp;<asp:TextBox ID="txtSect" runat="server" Width="50%"></asp:TextBox>                    
                        </td>
                          <td > 
                              <asp:Label ID="Label4" runat="server" >Article:</asp:Label>
                            &nbsp;<asp:TextBox ID="txtActicle" runat="server" Width="70%"></asp:TextBox>
                        </td>
                            <td >
                                <asp:Label ID="Label5" runat="server" Text="Label">Document type:</asp:Label>
                            &nbsp;<asp:DropDownList ID="DDLdoctype" runat="server"></asp:DropDownList>
                              
                        </td>
                    </tr>
                    <tr>
                        <td colspan="3" style="text-align:right;">
                            <br />
                             <asp:Button id="btnSavetree" runat="server" CssClass="btn btn-primary" Text="save" OnClick="btnSavetree_Click" />
                            &nbsp;&nbsp;<button class="btn btn-danger" onclick="document.getElementById('myModal').style.display='none';return false;">Close</button>
                        </td>
                        
                    </tr>
                </table>
                   
            </div>
        </div>
                <asp:HiddenField ID="hidLogin" runat="server" />
    </form>
</body>
</html>

