﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.PR
{
    public partial class viewCSDoc : System.Web.UI.Page
    {
        static OTFunctions zotFunctions = new OTFunctions();
        static DbControllerBase zdbUtil = new DbControllerBase();
        static LogHelper zLogHelper = new LogHelper();
        public string xdocnodeid = "0";

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try { xdocnodeid = Request.QueryString["docnodeid"].ToString().ToUpper(); }
                catch { xdocnodeid = ""; }

                var doc_node = zotFunctions.getNodeByID(xdocnodeid);
                if (doc_node != null)
                {

                    try
                    {
                        var xcontent = zotFunctions.downloadDoc(doc_node.ID.ToString());
                        // MemoryStream memo = new MemoryStream(xcontent);
                        // List<MemoryStream> listfile = new List<MemoryStream>();
                        // listfile.Add(memo);
                        // MemoryStream[] memories = listfile.ToArray();
                        //MargePDF merge = new MargePDF();
                        // var file = merge.margePDF(memories);
                        // //  Response.BinaryWrite(file);

                        //Response.ContentType = "application/pdf"
                        //Response.AddHeader("content-disposition", "attachment; filename=VisitorPass.pdf")
                        //Response.AddHeader("Content-Length", s.Length.ToString())
                        //Response.AddHeader("Content-Type", "application/pdf")

                        //Response.Buffer = true;
                        //Response.ContentType = "application/pdf";
                        //Response.AddHeader("content - disposition", "attachment; filename = Example.pdf");
                        //Response.AddHeader("Content-Length", file.Length.ToString());
                        //Response.AddHeader("Content-Type", "application/pdf");
                        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        ////Response.Write(memo);
                        //Response.BinaryWrite(file);
                        //New code
                        
                        Response.ContentType = "application/pdf";
                        Response.AddHeader("content-disposition", "inline; filename=" + doc_node.Name );
                        Response.BinaryWrite(xcontent);
                        Response.Flush();
                    }
                    catch (Exception ex)
                    {
                        var xcontent = zotFunctions.downloadDoc(doc_node.ID.ToString());
                        Response.Clear();
                        Response.Buffer = true;
                        Response.Charset = "";
                        Response.Cache.SetCacheability(HttpCacheability.NoCache);
                        Response.ContentType = "binary/octet-stream";
                        Response.AppendHeader("Content-Disposition", "attachment; filename=" + doc_node.Name.Replace(",", "_"));
                        Response.BinaryWrite(xcontent);
                        Response.Flush();
                        Response.End();
                    }

                }
            }
        }

        protected void btnDownloadFile_Click(object sender, EventArgs e)
        {
            if (xdocnodeid != null)
            {
                Response.Clear();

                var xcontent = zotFunctions.downloadDoc(xdocnodeid);
                MemoryStream ms = new MemoryStream(xcontent);

                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=Example.pdf");
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                Response.End();
            }
        }
    }
}