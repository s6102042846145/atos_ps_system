﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="psPR01.aspx.cs" Inherits="PS_System.form.PR.psPR01" %><%@ Register src="ucEmp.ascx" tagname="ucEmp" tagprefix="uc1" %>

<%@ Register src="ucEmp2.ascx" tagname="ucEmp2" tagprefix="uc2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>PR01</title>
    <style type="text/css">
        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }

        .ddlstyle {
            width: 200px;
        }

        .auto-style14 {
            width: 700px;
        }
        .auto-style18 {
            height: 22px;
        }
        .auto-style22 {
            width: 1166px;
        }
        .auto-style23 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 10pt;
            color: rgb(35, 34, 34);
            background-color: white;
            border-color: silver;
            border-radius: 2px;
        }
        .auto-style25 {
            width: 652px;
        }
        .auto-style27 {
            height: 36px;
        }
        .auto-style29 {
            width: 8%;
        }
        .auto-style32 {
            width: 23px;
        }
        .auto-style33 {
            width: 188px;
        }
        .auto-style34 {
            width: 8%;
            height: 142px;
        }
        .auto-style35 {
            height: 142px;
        }
        .auto-style36 {
            width: 1241px;
        }
        </style>
       <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
    $(function () {
        $('[id*=txtReqDate]').datepicker({
            changeMonth: true,
            changeYear: true,
            format: "dd/mm/yyyy",
            language: "tr"
        });
    });
</script>

</head>

<body>
    <form id="form1" runat="server">
        <div   style=" padding: 20px;">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Memo Purchase Requisition"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id ="divJob" class="div_90">

            <table  class="auto-style22">
                <tr>
                    <td>
                        <asp:Label ID="lbWFID" runat="server"  Text="สำหรับ BID No." CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBid" runat="server" OnSelectedIndexChanged="ddlBid_SelectedIndexChanged" CssClass="ddl-md" AutoPostBack="True">
                        </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbWFID0" runat="server"  Text="Revision" CssClass="Label_md"></asp:Label>
                        &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtRevision" runat="server" CssClass="auto-style23" Width="102px"></asp:TextBox>
                        &nbsp;&nbsp;<asp:ImageButton ID="ibtnSearchBid" runat="server" Height="23px" ImageUrl="~/images/search.png" OnClick="ibtnSearchBid_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblPRNO" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                    &nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMode" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtBidDesc" CssClass="Text_lg" runat="server" TextMode="MultiLine" Width="800px"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbProject" runat="server"  Text="สำหรับโครงการ:" CssClass="Label_md" Width="200px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtProject" CssClass="Text_lg" runat="server"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <asp:Label ID="lbDetail0" runat="server" CssClass="Label_xlg_blue" Text="หน่วยงานจัดซื้อ"></asp:Label>
                        
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                         <br />
                        <table>
                            <tr>
                                <td class="auto-style33"><asp:Label ID="lbOrganization0" runat="server"  Text="Purchase Org:" CssClass="Label_md" Width="200px"></asp:Label></td>
                                <td>&nbsp; <asp:DropDownList ID="ddlPurchaseOrg" runat="server" CssClass="ddl-md" AutoPostBack="True" ></asp:DropDownList></td>
                            </tr>
                             <tr>
                                <td class="auto-style33"><asp:Label ID="lbOrganization1" runat="server"  Text="Purchase Group:" CssClass="Label_md"></asp:Label></td>
                                <td>&nbsp; <asp:DropDownList ID="ddlPurchaseGroup" runat="server" CssClass="ddl-md" AutoPostBack="True" ></asp:DropDownList></td>
                            </tr>
                        </table>
                       
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                         <asp:Label ID="lbDetail" runat="server" CssClass="Label_xlg_blue" Text="รายละเอียดขอซื้อหรือขอจ้าง"></asp:Label>
                        
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbOrganization" runat="server"  Text="ออกในนามหน่วยงาน:" CssClass="Label_md" Width="200px"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtOrganization" CssClass="Text_md" runat="server" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbDocumentType" runat="server"  Text="ประเภทเอกสาร:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlPurchaseMethod" runat="server" CssClass="ddl-normal">
                            <asp:ListItem>บันทึกขอซื้อขอจ้าง</asp:ListItem>
                                      
                                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbDocNo" runat="server"  Text="เลขที่เอกสาร" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDocno" CssClass="Text_md" runat="server" ></asp:TextBox>
                    &nbsp;<asp:Label ID="lbDate" runat="server"  Text="ลงวันที่" CssClass="Label_md"></asp:Label>
                    &nbsp;<asp:TextBox ID="txtReqDate" runat="server" CssClass="Text_md"></asp:TextBox>
                    &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbFrom" runat="server"  Text="จาก:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <uc2:ucEmp2 ID="ucEmpFrom" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbTo" runat="server"  Text="เรียน:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <uc2:ucEmp2 ID="ucEmpTo" runat="server"   />
                   
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbCopyTo" runat="server"  Text="สำเนาเรียน:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCopyTo" runat="server"  CssClass="Text_lg"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server"  Text="เรื่อง:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSubject" runat="server" CssClass="Text_lg" Width="800px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server"  Text="วิธีที่จะซื้อหรือจ้าง:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:RadioButtonList ID="cblPurchase" runat="server" CssClass="cb-normal" Width="928px" RepeatColumns="3" Height="69px" >
                            <asp:ListItem Value="1">ตกลงราคา</asp:ListItem>
                            <asp:ListItem Value="2">สอบราคา</asp:ListItem>
                            <asp:ListItem Value="3">พิเศษ</asp:ListItem>
                            <asp:ListItem Value="4">ประกวดราคา</asp:ListItem>
                            <asp:ListItem Value="5">ประกวดราคา 2 ของ</asp:ListItem>
                            <asp:ListItem Value="6">ประกวดราคา 2 ของ แบบ PO</asp:ListItem>
                        </asp:RadioButtonList>
                    </td>
                </tr>
            </table>
           
        </div>
        <p></p>
        <div id="additional" class="div_90">
             <table >
                <tr>
                     <td >
                        <asp:Label ID="Label3" runat="server"  Text="ข้อมูลเพิ่มเติม" CssClass="Label_xlg_blue"></asp:Label>
                    </td>
                    
                </tr>
                <tr>
                    <td   >
                        <asp:Label ID="Label4" runat="server"  Text="1.ราคากลางรวมทั้งหมดเป็นเงิน:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td  >
                        <asp:TextBox ID="txtStdPrice" runat="server"  CssClass="Text_md"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label5" runat="server"  Text="2.กำหนดขายเอกสารประกวดราคา:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtBidDocSelldate" runat="server"  CssClass="Text_md"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label6" runat="server"  Text="3.กำหนดรับและเปิดซองประกวดราคา:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td  >
                        <asp:TextBox ID="txtBidDocRecivedate" runat="server"  CssClass="Text_md"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label8" runat="server"  Text="และซองราคา" CssClass="Label_md"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtBidTabDate" runat="server"  CssClass="Text_md"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="Label9" runat="server"  Text="4.กำหนดออกหนังสือสนองรับราคา:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtLIdate" runat="server" CssClass="Text_md"></asp:TextBox>
                    </td>
                </tr>
                  <tr>
                     <td >
                        <asp:Label ID="Label10" runat="server"  Text="5.กำหนดแล้วเสร็จของงาน:" CssClass="Label_md"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtJobPeriod" runat="server"  CssClass="Text_md"></asp:TextBox>
                    </td>
                     
                </tr>
                 <tr>
                     <td >
                        <asp:Label ID="Label11" runat="server"  Text="กำหนดส่งของ:" CssClass="Label_md" Visible="False"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtDeliveryDate" runat="server"  CssClass="Text_md" Visible="False"></asp:TextBox>
                    </td>
                     
                </tr>
                 <tr>
                     <td >
                        <asp:Label ID="Label12" runat="server"  Text="หน่วยงานเจ้าของเรื่อง:" CssClass="Label_md" Visible="False"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="txtSubjectDept" runat="server"  CssClass="Text_md" Visible="False"></asp:TextBox>
                    </td>
                     
                </tr>
                 <tr>
                     <td >
                        <asp:Label ID="Label54" runat="server"  Text="วัตถุประสงค์" CssClass="Label_md"></asp:Label>
                    </td>
                    <td >
                        <asp:TextBox ID="TxtObjective" runat="server"  CssClass="auto-style23" Width="800px" TextMode="MultiLine"></asp:TextBox>
                    </td>
                     
                </tr>
                <tr>
                     <td >
                        <asp:Label ID="Label52" runat="server"  Text="คำลงท้าย" CssClass="Label_md"></asp:Label>
                     </td>
                    <td >
                        <asp:TextBox ID="TxtClosing" runat="server"  CssClass="auto-style23" Width="800px"></asp:TextBox>
                    </td>
                    
                     
                </tr>
                <tr>
                     <td >
                        <asp:Label ID="Label53" runat="server"  Text="หมายเหตุ" CssClass="Label_md"></asp:Label>
                     </td>
                    <td >
                        <asp:TextBox ID="txtRemark" runat="server"  CssClass="auto-style23" Width="800px"></asp:TextBox>
                    </td>
                </tr>
             
            </table>         
     
        </div>
      
        <div >
              
          <asp:Button ID="btnPreViewDoc" CssClass="btn_normal_blue" runat="server" Text="Preview Document" OnClick="btnPreViewDoc_Click" />
             
        </div>
            <p></p>
        <div id="div_Attach" class="div_90">
            <div><asp:Label Text="รายละเอียดสิ่งแนบ" runat="server" CssClass="Label_lg_blue"></asp:Label></div>
            <table >
                
                                 <tr>
                                      <td style="background-color: #CCCCCC" >
                                          &nbsp;</td>
                                      <td style="background-color: #CCCCCC" >
                                          &nbsp;  

                                      </td>
                                     <td style="background-color: #CCCCCC" >
                                         <asp:Label ID="Label41" runat="server" CssClass="Label_md" Width="150px" Text="input file:"></asp:Label>

                                      </td>
                                     <td style="background-color: #CCCCCC">
                                         <asp:Label ID="Label42" runat="server" CssClass="Label_md" Width="150px" Text="รายละเอียด"></asp:Label>

                                      </td>
                                     <td style="background-color: #CCCCCC">&nbsp;</td>
                                    
                                 </tr>
                
                                 <tr>
                                      <td class="table_body_lefttop" >
                                          &nbsp;</td>
                                      <td class="table_body_lefttop" >
                                          &nbsp;</td>
                                     <td class="table_body_lefttop" >
                                          <asp:FileUpload ID="inputFileAttach"  runat="server" CssClass="auto-style23" Width="260px" />
                                         <asp:TextBox ID="txtA_Itemno" runat="server" CssClass="Text_60" Visible="False" ></asp:TextBox>
                                          <asp:RadioButtonList ID="rdlAttachType" CssClass="rdl-normal" runat="server" RepeatDirection="Horizontal" Width="327px" AutoPostBack="True" OnSelectedIndexChanged="rdlAttachType_SelectedIndexChanged" Visible="False">
                                             <asp:ListItem Selected="True" Value="document">เอกสาร</asp:ListItem>
                                             <asp:ListItem Value="hardcopy">Hard copy</asp:ListItem>
                                              </asp:RadioButtonList>
                                     </td>
                                     <td class="table_body_lefttop" >
                                         &nbsp;
                                          <asp:TextBox ID="txtAttachDesc" runat="server" CssClass="auto-style23" Width="302px" ></asp:TextBox>
                                     </td>
                                     <td class="table_body_lefttop" >&nbsp;
                                         <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn_normal_blue" OnClick="btnUpload_Click" />

                                     </td>
                                    
                                 </tr>
                             </table>
            <div>
                  <asp:GridView ID="gvA" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No data found." Font-Names="tahoma" Font-Size="9pt" Width="71%" OnRowCommand="gvA_OnRowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_lblNo" CssClass="Text_60" runat="server" Text='<%# Bind("itemno") %>'></asp:Label>
                                            <%--Text='<%# Bind("job_no") %>'--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ประเภทสิ่งแนบ" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_attachtype" runat="server" Text='<%# Bind("attachtype") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_Desc" CssClass="Text_400" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_docname" CssClass="Text_400" runat="server" Text='<%# Bind("docname") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Document NodeID" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_doc_nodeid" CssClass="Text_400" runat="server" Text='<%# Bind("docnode") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Remove">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgRemove" runat="server" Height="20px" ImageAlign="Right" ImageUrl="~/images/bin.png" Width="20px" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'  />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                  <br />
            </div>
         </div>
      
         <div>
            <asp:Panel ID="Panel3" runat="server"  >
                <table>
                    <tr>
                        <td class="auto-style36">
                            <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="เสนอเพื่อทบทวนร่าง"></asp:Label>
                        </td>
                    </tr>
                    </table>
                        
                 <table style="width: 100%;">
                    <tr>
                        <td class="auto-style34">
                             <asp:Label ID="Label17" runat="server"  Text="เสนอโดย" Font-Size="10pt" Width="100px"></asp:Label>
                        </td>
                         <td class="auto-style35">
                              <uc1:ucEmp ID="ucEmpReq" runat="server" />
                        </td>
                    </tr>
                     <tr>
                         <td colspan="2">
                             <asp:Label ID="Label43" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="ผู้อนุมัติร่างพิจารณา"></asp:Label>
                         </td>
                     </tr>
                    </table>
            </asp:Panel>
        </div>
          <div>
                <asp:Panel ID="Panel4" runat="server"  >
                <table class="nav-justified">
              <tr>
                  <td class="auto-style32">

                      &nbsp;</td>
                  <td class="auto-style29">
                      <asp:Label ID="Label49" runat="server" Text="ลำดับที่ 1" Font-Size="10pt" Width="100px"></asp:Label>
                  </td>
                  <td colspan="3">
                      <uc1:ucEmp ID="ucEmpApv1" runat="server" />
                  </td>
              </tr>
              
                

                  
                    <tr>
                        <td class="auto-style32">&nbsp;</td>
                        <td class="auto-style29">
                            <asp:Label ID="Label50" runat="server" Font-Size="10pt" Text="ลำดับที่ 2" Width="100px"></asp:Label>
                        </td>
                        <td class="auto-style25">
                            <uc1:ucEmp ID="ucEmpApv2" runat="server" />
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
              
                

                  
                    <tr>
                        <td class="auto-style32">&nbsp;</td>
                        <td class="auto-style29">
                            <asp:Label ID="Label51" runat="server" Font-Size="10pt" Text="ลำดับที่ 3" Width="100px"></asp:Label>
                        </td>
                        <td class="auto-style25">
                            <uc1:ucEmp ID="ucEmpApv3" runat="server" />
                        </td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
              
                

                  
                    <tr>
                        <td colspan="5" class="auto-style27">
                            <asp:Label ID="Label46" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="ส่วนอนุมัติ"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style32">
                            &nbsp;</td>
                        <td class="auto-style29">
                            <asp:Label ID="Label47" runat="server" Font-Size="10pt" Text="ผู้อนุมัติ" Width="100px"></asp:Label>
                        </td>
                        <td colspan="3">
                            <uc1:ucEmp ID="ucEmpApvFinal" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
              
                

                  
             </table>
               
                        
                
            </asp:Panel>
           </div>

              <div>
                <asp:Panel ID="Panel5" runat="server"  >
                <table style="width: 100%;">
              <tr>
                  <td class="auto-style18">

                      <asp:Button ID="btnSave" runat="server" BackColor="#164094" class="w3-button w3-blue" OnClick="btnSave_Click" Text="Save" />
                      <asp:Button ID="btnSubmit" runat="server" BackColor="#164094" class="w3-button w3-blue" OnClick="btnSubmit_Click" Text="Submit" />
                      <asp:Button ID="btnCancel" runat="server" BackColor="#164094" class="w3-button w3-blue" OnClick="btnCancel_Click" Text="Cancel" />
                  </td>
              </tr>
                <tr>
                    <td class="auto-style18">
                        
                        &nbsp;&nbsp;&nbsp;
                        <br />
                        <asp:Panel ID="pStaff" runat="server">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <table class="nav-justified">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label55" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="ผู้ปฏิบัติงาน"></asp:Label>
                                        <uc1:ucEmp ID="ucEmpStaff" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </asp:Panel>
                        
                    </td>
                </tr>
              
                

                  
                    <tr>
                        <td class="auto-style14" rowspan="3" style="width: 75%;">&nbsp;</td>
                        <td rowspan="3" style="width: 25%;">
                            &nbsp;&nbsp; &nbsp;&nbsp;
                        </td>
                    </tr>
              
                

                  
                    <tr>
                        <td class="auto-style14" style="width: 75%;">&nbsp;</td>
                        <td style="width: 25%;">&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style14" style="width: 75%;">&nbsp;</td>
                        <td style="width: 25%;">&nbsp;</td>
                    </tr>
              
                

                  
             </table>
               
                        
                
            </asp:Panel>
           </div>
        
        </div>
<div class="hidden">
    <asp:TextBox ID="txtPrNo" runat="server"></asp:TextBox>
</div>
        <asp:HiddenField ID="hidPDF_NODE_ID" runat="server" />
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRevision" runat="server" />
        <asp:HiddenField ID="hidPRNo" runat="server" />
        <br />
        <asp:HiddenField ID="hidMode" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
    </form>
    
</body>
</html>

