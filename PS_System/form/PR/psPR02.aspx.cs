﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.PR
{
    public partial class psPR02 : System.Web.UI.Page
    {
        private string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        static DbControllerBase db = new DbControllerBase();
        DataTable dt = new DataTable();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z594900";

                if (Request.QueryString["bidno"] != null)
                    hidBidNo.Value = Request.QueryString["bidno"];
                else
                    hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;

                string sql = "select distinct pr_status from ps_t_pr order by pr_status ";
                dt = db.ExecSql_DataTable(sql, zetl4eisdb);
                drpStatus.Items.Clear();
                drpStatus.Items.Insert(0, new ListItem("-Select All Status-", ""));
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    string theValue = dt.Rows[i].ItemArray[0].ToString();
                    drpStatus.Items.Add(theValue);
                }
                Refreshgrid();

            }
        }
        protected void btnFil_Click(object sender, EventArgs e)
        {
            Refreshgrid();
        }
       

        protected void Refreshgrid()
        {
            string rowfilter = "";
            string Pr_no = txtPr_no.Text.Trim();
            string From_org = txtFrom_org.Text.Trim();
            string Pr_status = drpStatus.SelectedValue.ToString();

            string sql = "SELECT a.* FROM ps_t_pr a ";
            //string sql = "SELECT a.row_id,a.pr_no,a.bid_no,a.subject, a.from_org,a.to_org,a.pr_status FROM ps_t_pr a ";

            if (Pr_no != "")
            {
                rowfilter = string.Format("(a.pr_no = '{0}')", Pr_no);
            }
            if (From_org != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(a.from_org = '{0}')", From_org);
                else rowfilter = string.Format("{0} and a.from_org = '{1}'", rowfilter, From_org);
            }
            if (Pr_status != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(a.pr_status = '{0}')", Pr_status);
                else rowfilter = string.Format("{0} and a.pr_status = '{1}'", rowfilter, Pr_status);
            }
            sql += rowfilter == "" ? "" : " where " + rowfilter;
            var dt = db.ExecSql_DataTable(sql, zetl4eisdb);
            gvData.DataSource = dt;
            gvData.DataBind();
        }
       
        //protected void editData_Click(object sender, EventArgs e)
        //{

        //    var nameValues = HttpUtility.ParseQueryString(Request.QueryString.ToString());
        //    nameValues.Set("pr_no", "4");
        //    nameValues.Set("bid_no", "5");
        //    nameValues.Set("bid_revision", "5");


        //    string url = Request.Url.AbsolutePath;
        //    Response.Redirect(url.Replace("psPR02", "psPR01") + "?" + nameValues); // ToString() is called implicitly
        //}

    }
}