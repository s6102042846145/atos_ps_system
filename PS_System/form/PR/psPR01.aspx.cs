﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using PS_Library;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text.RegularExpressions;
using Newtonsoft.Json;
using ECMCreateCommon;
//using PS_Library.RestSharp;
using PS_Library;
using RestSharp;

namespace PS_System.form.PR
{
    public partial class psPR01 : System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        OTFunctions zot = new OTFunctions();
        #endregion 
        

        protected void Page_Load(object sender, EventArgs e)
        {
           
            if (!IsPostBack)
            {
                
                try { hidPRNo.Value = Request.QueryString["pr_no"].ToString(); }
                catch { hidPRNo.Value = ""; }
                try { hidBidNo.Value = Request.QueryString["bid_no"].ToString(); }
                catch { hidBidNo.Value = ""; }
                try { hidBidRevision.Value = Request.QueryString["bid_revision"].ToString(); }
                catch { hidBidRevision.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); } //zuserlogin=z591543
                catch { hidLogin.Value = ""; }
                
                if (hidLogin.Value == "") // Debug Mode
                {
                    // Debug 
                    //pr_no = "PR-1-132021"; 
                    hidLogin.Value = "z593299";
                    hidMode.Value = "CREATE";
                }
                if (hidLogin.Value != "")
                {
                    try
                    {
                        ucEmpStaff.setByEmpID(hidLogin.Value.Replace("z", "").Replace("Z", ""));
                    }
                    catch
                    {

                    }
                }
               
                lblMode.Text = hidMode.Value.ToUpper();
                if (hidPRNo.Value != "")
                {
                    lblPRNO.Text = hidPRNo.Value;
                }
                ini_Data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
               
            }
            txtOrganization.Text = ucEmpTo.getShortPosition();
            if (ucEmpFrom.getEmpID() != "")
            {
                ucEmpReq.setByEmpID(ucEmpFrom.getEmpID());
            }
        }
       
        private void ini_Data()
        {
            ini_purchaseOrg();
            ini_purchaseGroup();
            ini_bid_no();
            ini_gvA();
            ini_rdlAttachType(); 
            //if (hidMode.Value == "create")
            //{
            //    pStaff.Visible = false; 
            //}
        }
        private void ini_purchaseOrg()
        {
            string sql = "select * from P_M_SAP_PURCHASINGORG order by Name ";
            var ds = ora._getDS(sql, zcommondb);
            ddlPurchaseOrg.Items.Clear();
            ddlPurchaseOrg.DataSource = ds.Tables[0];
            ddlPurchaseOrg.DataTextField = ds.Tables[0].Columns["Name"].ColumnName;
            ddlPurchaseOrg.DataValueField = ds.Tables[0].Columns["Code"].ColumnName;
            ddlPurchaseOrg.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Please select Org.-";
            li.Value = "";
            ddlPurchaseOrg.Items.Insert(0, li);
            
            try { ddlPurchaseOrg.SelectedValue = "5000"; } catch { ddlPurchaseOrg.SelectedIndex = 0; }
        }
        private void ini_purchaseGroup()
        {
            string sql = "select * from P_M_SAP_PURCHASINGGROUP where LOWER(name) not like '%null%' and code like '%0' and name in ( 'หซตส-ห.','หจตส-ห.') order by Name ";
            var ds = ora._getDS(sql, zcommondb);
            ddlPurchaseGroup.Items.Clear();
            ddlPurchaseGroup.DataSource = ds.Tables[0];
            ddlPurchaseGroup.DataTextField = ds.Tables[0].Columns["Name"].ColumnName;
            ddlPurchaseGroup.DataValueField = ds.Tables[0].Columns["Code"].ColumnName;
            ddlPurchaseGroup.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Please select Group.-";
            li.Value = "";
            ddlPurchaseGroup.Items.Insert(0, li);
            ddlPurchaseGroup.SelectedIndex = 0;
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        private void ini_gvA()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("itemno", typeof(string));
            dt.Columns.Add("attachtype", typeof(string));
            dt.Columns.Add("description", typeof(string)); 
            dt.Columns.Add("docname", typeof(string));
            dt.Columns.Add("docnode", typeof(string));

            DataRow dr = dt.NewRow();
            dt.Rows.Add(dr);
            gvA.DataSource = dt;
            gvA.DataBind(); 


        }

        protected void gvA_OnRowCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "removedata")
            {
                // Delete data from ps_t_pr_attachment

            }
        }
        private void ini_rdlAttachType()
        {
            if (rdlAttachType.SelectedValue.Contains("hardcopy") == true)
            {
                zAttachDocType = "hardcopy";
                inputFileAttach.Visible = false; 

            } else { inputFileAttach.Visible = true;
                     zAttachDocType = "fileupload";
            }

        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
       
        private void ini_bid_no()
        {
            ddlBid.Items.Clear(); 
            string sql = "select distinct bid_no, bid_no as col2  from ps_t_bid_monitoring order by bid_no ";
            DataSet ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlBid.DataSource = ds.Tables[0];
            ddlBid.DataTextField = ds.Tables[0].Columns[0].ColumnName;
            ddlBid.DataValueField = ds.Tables[0].Columns[1].ColumnName;
            ddlBid.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Select Bid No.-";
            li.Value = "";
            ddlBid.Items.Insert(0, li);
            if (hidBidNo.Value != "")
            {
                try { ddlBid.SelectedValue = hidBidNo.Value; } catch { ddlBid.SelectedIndex = 0; }
            }
            if (hidBidRevision.Value != "") { txtRevision.Text = hidBidRevision.Value; }
        }
        private void bind_bid(string xbidno, string xrevision)
        {
            string sql = "select *  from bid_no where bid_no = '"+ xbidno + "' and bid_revision = "+ xrevision + " order by bid_no ";
            DataSet ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                txtBidDesc.Text = StripHTML( dr["bid_desc"].ToString());
            }
        }
        private byte[] ReplaceWordPR()
        {
            //hidDocxName.Value = hidTempNo.Value + ".docx";
            byte[] outputContent = new byte[0];
            EgatED.Class.ReplaceDocx rpldoc = new EgatED.Class.ReplaceDocx();

            List<EgatED.Class.ReplaceDocx.TagData> liData = new List<EgatED.Class.ReplaceDocx.TagData>();
            EgatED.Class.ReplaceDocx.TagData tagdata = new EgatED.Class.ReplaceDocx.TagData();
 

            for (int i=0; i <= cblPurchase.Items.Count -1; i++ )
            {
                if (cblPurchase.Items[i].Selected == true)
                {
                    tagdata = new EgatED.Class.ReplaceDocx.TagData();
                    tagdata.string_tag = true;
                    tagdata.tagname = "#chktype" + i + "#";
                    tagdata.tagvalue = "/";
                    liData.Add(tagdata);
                }
                else
                {
                    tagdata = new EgatED.Class.ReplaceDocx.TagData();
                    tagdata.string_tag = true;
                    tagdata.tagname = "#chktype" + i + "#";
                    tagdata.tagvalue = "";
                    liData.Add(tagdata);


                }

            }


            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtOrganization#";
            tagdata.tagvalue = txtOrganization.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtFrom#";
            tagdata.tagvalue = ucEmpFrom.getShortPosition();// txtFrom.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#prno#";
            tagdata.tagvalue = lblPRNO.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtSubject#";
            tagdata.tagvalue = txtSubject.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtReqDate#";
            tagdata.tagvalue = txtReqDate.Text.Trim();
            liData.Add(tagdata);


            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtTo#";
            tagdata.tagvalue = ucEmpTo.getShortPosition();// txtTo.Text.Trim();
            liData.Add(tagdata);
            
            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtBidNo#";
            tagdata.tagvalue = ddlBid.SelectedValue.ToString();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtBidDesc#";
            tagdata.tagvalue = txtBidDesc.Text ;
            liData.Add(tagdata);


            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#objective#";
            tagdata.tagvalue = TxtObjective.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtStdPrice#";
            tagdata.tagvalue = txtStdPrice.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtBidDocSelldate#";
            tagdata.tagvalue = txtBidDocSelldate.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtBidDocRecivedate#";
            tagdata.tagvalue = txtBidDocRecivedate.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtLIdate#";
            tagdata.tagvalue = txtLIdate.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtJobPeriod#";
            tagdata.tagvalue = txtJobPeriod.Text.Trim();
            liData.Add(tagdata);


            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#closingtext#";
            tagdata.tagvalue = TxtClosing.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#remark#";
            tagdata.tagvalue = txtRemark.Text.Trim();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtReqName#";
            tagdata.tagvalue = ucEmpReq.getEmpName();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtReqPosition#";
            tagdata.tagvalue = ucEmpReq.getFullPosition();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtCopyTo#";
            tagdata.tagvalue = txtCopyTo.Text.Trim();
            liData.Add(tagdata);

            //APV1
            #region APV1
            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv1shortposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue =ucEmpApv1.getShortPosition();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv1name#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv1.getEmpName();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv1fullposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv1.getFullPosition();
            liData.Add(tagdata);
            #endregion

            #region APV2
            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv2shortposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv2.getShortPosition();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv2name#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv2.getEmpName();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv2fullposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv2.getFullPosition();
            liData.Add(tagdata);
            #endregion

            #region APVFinal
            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apvfinalshortposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApvFinal.getShortPosition();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apvfinalname#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApvFinal.getEmpName();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apvfinalfullposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApvFinal.getFullPosition();
            liData.Add(tagdata);
            #endregion

            #region APV3
            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv3shortposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv3.getShortPosition();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv3name#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv3.getEmpName();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#apv3fullposition#";
            //tagdata.tagvalue = txtBidNo.Text.Trim();
            tagdata.tagvalue = ucEmpApv3.getFullPosition();
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtbuyerstaff#";
            tagdata.tagvalue = "";
            liData.Add(tagdata);

            tagdata = new EgatED.Class.ReplaceDocx.TagData();
            tagdata.string_tag = true;
            tagdata.tagname = "#txtCC#";
            tagdata.tagvalue = txtCopyTo.Text.Trim() ;
            liData.Add(tagdata);

            #endregion 
            string fn_name = "";
           
            fn_name="output_pr_" + System.DateTime.Now.ToString("yyyyMMdd_HHmmss");
            
            string xtemplate_name = zPRTemplate ;
            string xtemplate_output = zPROutputfile;
            string xtemplate_outputfile = xtemplate_output + fn_name+ ".docx";

            var xcontent = rpldoc.ReplaceData(liData
                , xtemplate_name
                , xtemplate_output
                , xtemplate_outputfile
                , false);
            outputContent = System.IO.File.ReadAllBytes(xtemplate_outputfile);
            string xpdf_outputfile = xtemplate_output + fn_name + ".pdf";
            var pdf_content =  rpldoc.convertDOCtoPDF(xtemplate_outputfile, xpdf_outputfile, true);
            var pr_number_node_id = zot.getNodeByName(zPR_NodeID, lblPRNO.Text.Trim());
            if (pr_number_node_id == null)
            {
                pr_number_node_id = zot.createFolder(zPR_NodeID, lblPRNO.Text.Trim());
            }
            var pdf_node_id =  zot.uploadDoc(pr_number_node_id.ID.ToString(), lblPRNO.Text.Trim() + ".pdf", lblPRNO.Text.Trim() + ".pdf", pdf_content, "").ToString();
            hidPDF_NODE_ID.Value = pdf_node_id; 
            //Response.ContentType = "application/pdf";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=OutPut.pdf");
            //Response.TransmitFile("D:\\ATOS\\Output\\OutPut.pdf");
            //Response.End();
            //hidDocxNodeID.Value = zot.uploadDoc(hidLetterNodeID.Value, hidDocxName.Value, hidDocxName.Value, outputContent, "").ToString();
            //string sql = " update tbl_create_letter_master set word_node_id = '" + hidDocxNodeID.Value + "' where temp_no = '" + hidTempNo.Value + "' ";
            //zdb._runSQL(sql, zconnstr);
            return outputContent;
        }

        protected void btnPreViewDoc_Click(object sender, EventArgs e)
        {
            ReplaceWordPR();

            string url = "previewdoccs.aspx?nodeid="+hidPDF_NODE_ID.Value;
            Response.Write("<script> window.open('" + url + "');</script>");
            //var pdf_content = zot.downloadDoc(hidPDF_NODE_ID.Value);
            //Response.ContentType = "application/pdf";
            //Response.AddHeader("content-disposition", "inline; filename=" + txtPrNo.Text.Trim() + ".pdf");
            //Response.BinaryWrite(pdf_content);
            //Response.Flush();
        }
       
        
        protected void ddlBid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //find max revision
            string sql = " select * from ps_t_bid_monitoring  where bid_no = '"+ ddlBid.SelectedValue.ToString() + "' order by bid_no asc , bid_revision desc ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var xbid_revision = ds.Tables[0].Rows[0]["bid_revision"].ToString();
                txtRevision.Text = xbid_revision; 
                bind_bid(ddlBid.SelectedValue.ToString(), txtRevision.Text);
                hidBidNo.Value = ddlBid.SelectedValue.ToString();
                hidBidRevision.Value = xbid_revision;

                //Check Medium Price
                txtStdPrice.Text = checkMediumPrice(hidBidNo.Value, hidBidRevision.Value).ToString("###,###,##0.00");
                //Check TOR DOC
                checkTORDoc(hidBidNo.Value, hidBidRevision.Value);
                //Check P6
                checkP6(hidBidNo.Value, hidBidRevision.Value);
            }
        }
        private decimal checkMediumPrice (string xbid_no, string xbid_revision)
        {
            decimal xtt = 0;
            string sql = @"  select sum( 
(supply_equip_unit_price * supply_equip_amonut) +
(local_exwork_unit_price * local_exwork_amonut) +
(local_tran_unit_price * local_tran_amonut)) as tt_medium_price
 from ps_t_mytask_equip
 where bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and path_code not like 'Z%'";
            var dt = zdb.ExecSql_DataTable(sql, zconnectionstr);
            if (dt.Rows.Count > 0)
            {
                var dr = dt.Rows[0];
                try
                {
                    xtt = Decimal.Parse(dr["tt_medium_price"].ToString());
                }
                catch { }
            }
            return xtt; 
        }
        private void checkTORDoc(string xbid_no, string xbid_revision)
        {
            DataTable dt = new DataTable();
             dt.Columns.Add("itemno", typeof(string));
             dt.Columns.Add("attachtype", typeof(string));
             dt.Columns.Add("description", typeof(string));
             dt.Columns.Add("docname", typeof(string));
            dt.Columns.Add("docnode", typeof(string));

            string sql = "";
            sql = "select * from ps_t_pr_attachment where pr_no = '" + lblPRNO.Text + "' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    var dr = ds.Tables[0].Rows[i];
                    DataRow drNew = dt.NewRow();
                    drNew["itemno"] = dr["attach_item_no"].ToString();
                    drNew["attachtype"] = "";
                    drNew["description"] = dr["description"].ToString();
                    drNew["docname"] = dr["attach_doc_name"].ToString();
                    drNew["docnode"] = dr["attach_doc_nodeid"].ToString();
                    dt.Rows.Add(drNew);
                    gvA.DataSource = dt;
                    gvA.DataBind();
                }
            }
            else {

                sql = "select * from ps_t_tor where bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision;
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dr = ds.Tables[0].Rows[i];
                        DataRow drNew = dt.NewRow();
                        drNew["itemno"] = (i + 1).ToString();
                        drNew["attachtype"] = "";
                        drNew["description"] = dr["description"].ToString();
                        drNew["docname"] = dr["tor_doc_name"].ToString();
                        drNew["docnode"] = dr["tor_doc_nodeid"].ToString();
                        dt.Rows.Add(drNew);
                        gvA.DataSource = dt;
                        gvA.DataBind();
                    }
                }
            }
            if (dt.Rows.Count == 0)
            {
                DataRow drNew = dt.NewRow();
                dt.Rows.Add(drNew);
            }
            gvA.DataSource = dt;
            gvA.DataBind();
        }
        private string convertToTHDateString(DateTime xdate)
        {
            string xdd = xdate.Day.ToString();
            string xMMMM = "";
            string xyyyy = "";
            switch (xdate.Month)
            {
                case 1: xMMMM = "มกราคม";break;
                case 2: xMMMM = "กุมภาพันธ์"; break;
                case 3: xMMMM = "มีนาคม"; break;
                case 4: xMMMM = "เมษายน"; break;
                case 5: xMMMM = "พฤษภาคม"; break;
                case 6: xMMMM = "มิถุนายน"; break;
                case 7: xMMMM = "กรกฎาคม"; break;
                case 8: xMMMM = "สิงหาคม"; break;
                case 9: xMMMM = "กันยายน"; break;
                case 10: xMMMM = "ตุลาคม"; break;
                case 11: xMMMM = "พฤศจิกายน"; break;
                case 12: xMMMM = "ธันวาคม"; break;
            }
            if ((xdate.Year - 543) > 2500) { xyyyy = xdate.Year.ToString(); }
            else { xyyyy = (xdate.Year + 543).ToString(); }
            return (xdd + " " + xMMMM + " " + xyyyy);
        }
        private void checkP6(string xbid_no, string xbid_revision)
        {
            string xbid_issue_date = "";
            string xbid_receiveprice_date = "";
            string xbid_open_date = "";
            string xbid_compare_date = "";
            string xbid_loa_date = "";
            string xbid_job_complete_date = "";
            string xbid_delivery_date = "";

            p6Class p6 = new p6Class();
            var dtData = p6.getActivityBid(xbid_no);
            if (dtData.Rows.Count > 0)
            {
                for (int i = 0; i < dtData.Rows.Count; i++)
                {
                    var dr = dtData.Rows[i];
                    if (dr["task_name"].ToString().Contains("B :"))
                    {
                        xbid_issue_date = convertToTHDateString( Convert.ToDateTime(dr["target_start_date"]));
                        txtBidDocSelldate.Text = xbid_issue_date;
                    }
                    if (dr["task_name"].ToString().Contains("C :"))
                    {
                        xbid_receiveprice_date = convertToTHDateString(Convert.ToDateTime(dr["target_start_date"]));
                        txtBidDocRecivedate.Text = xbid_receiveprice_date;
                    }
                    if (dr["task_name"].ToString().Contains("Bid Eva"))
                    {
                        xbid_open_date = convertToTHDateString(Convert.ToDateTime(dr["target_start_date"]));
                        txtBidTabDate.Text = xbid_open_date;
                    }
                    if (dr["task_name"].ToString().Contains("Gi :"))
                    {
                        xbid_loa_date = convertToTHDateString(Convert.ToDateTime(dr["target_start_date"]));
                        txtLIdate.Text = xbid_loa_date;
                    }
                    if (dr["task_name"].ToString().Contains("Gi : "))
                    {
                        xbid_loa_date = convertToTHDateString(Convert.ToDateTime(dr["target_start_date"]));
                        txtLIdate.Text = xbid_loa_date;
                    }
                    if (dr["task_name"].ToString().Contains("J : "))
                    {
                        xbid_delivery_date = convertToTHDateString(Convert.ToDateTime(dr["target_start_date"]));
                        txtDeliveryDate.Text = xbid_delivery_date;
                    }
                    if (dr["task_name"].ToString().Contains("K : ") )
                    {
                        xbid_job_complete_date = convertToTHDateString(Convert.ToDateTime(dr["target_start_date"]));
                        txtJobPeriod.Text = xbid_job_complete_date;
                    }
                }
            }
        }
        protected void rdlAttachType_SelectedIndexChanged(object sender, EventArgs e)
        {
            ini_rdlAttachType();
        }
        
        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (lblPRNO.Text == "")
            {
                saveData();
            }
            OTFunctions zot = new OTFunctions();

            DataTable dtUpload = (DataTable)ViewState["dtAddDoc"];
            if (dtUpload == null)
            {
                    dtUpload = new DataTable();
                    dtUpload.Columns.Add("itemno");
                    dtUpload.Columns.Add("attachtype");
                    dtUpload.Columns.Add("description");
                    dtUpload.Columns.Add("docname");
                    dtUpload.Columns.Add("docnode");
            }
            DataRow dr;                
            if ((inputFileAttach.PostedFile != null) && (inputFileAttach.PostedFile.ContentLength > 0))
            {
          

                foreach (HttpPostedFile uploadedFile in inputFileAttach.PostedFiles)
                {
                    string strNewFileName = uploadedFile.FileName;
                    var xpr_number_nodeid = zot.getNodeByName(zPR_NodeID, lblPRNO.Text);
                    if (xpr_number_nodeid == null)
                    {
                        xpr_number_nodeid = zot.createFolder(zPR_NodeID, lblPRNO.Text);
                    }
                    
                    var xnode_id = zot.uploadDoc(xpr_number_nodeid.ID.ToString(), uploadedFile.FileName,
                       uploadedFile.FileName, inputFileAttach.FileBytes, "").ToString();

                    DataRow[] foundFileName = dtUpload.Select("docname = '" + strNewFileName + "'");
                        if (foundFileName.Length == 0)
                        {
         
                            //uploadedFile.InputStream
                            Stream fs = uploadedFile.InputStream;
                            BinaryReader br = new BinaryReader(fs);
                            byte[] bytesUl = br.ReadBytes((Int32)fs.Length);
                       
                            dr = dtUpload.NewRow();
                            if (((Label)(gvA.Rows[gvA.Rows.Count - 1].FindControl("gvA_lblNo"))).Text.Trim() == "")
                            {
                                txtA_Itemno.Text = "1";
                            }
                            else
                            {
                                txtA_Itemno.Text = (int.Parse(((Label)(gvA.Rows[gvA.Rows.Count - 1].FindControl("gvA_lblNo"))).Text) + 1).ToString(); 
                            }
                            
                            dr["itemno"] = txtA_Itemno.Text;
                            dr["attachtype"] = "File Document";
                            dr["description"] = txtAttachDesc.Text;
                            dr["docname"] = strNewFileName;
                            dr["docnode"] = xnode_id;

                            dtUpload.Rows.Add(dr);
                        string sql = "";
                        if (checkExistingDoc(lblPRNO.Text, strNewFileName) == false)
                        {
                            // Insert Attachment
                            sql = @"insert into ps_t_pr_attachment (
                            pr_no,
                            attach_item_no,
                            hard_copy_yesno,
                            description,
                            attach_doc_name,
                            attach_doc_nodeid
                                ) values (
                        '" + lblPRNO.Text + @"', 
                        '" + txtA_Itemno.Text + @"', 
                        '" + "" + @"', 
                        '" + txtAttachDesc.Text + @"', 
                        '" + strNewFileName + @"', 
                        '" + xnode_id + @"'
                        ) ";
                        }
                        else
                        {
                            // Update Attachment
                            sql = @"update  ps_t_pr_attachment set
                           
                            attach_item_no =  '" + txtA_Itemno.Text + @"',
                            hard_copy_yesno =  '" + "" + @"', 
                            description = '" + txtAttachDesc.Text + @"',
                            attach_doc_nodeid = '" + xnode_id + @"'
                            ";
                            sql += " where pr_no = '" + lblPRNO.Text + "' and attach_doc_name = '" + strNewFileName + "' ";
                        }
                        zdb.ExecNonQuery(sql, zconnectionstr);
                    }
                }
                if (dtUpload.Rows.Count > 0)
                {
                        ViewState["dtAddDoc"] = dtUpload;
                        gvA.DataSource = dtUpload;
                        gvA.DataBind();
                }

            }
            else
            {
                dr = dtUpload.NewRow();
                dr["itemno"] = txtA_Itemno.Text;
                dr["attachtype"] = "Hard Copy";
                dr["description"] = txtAttachDesc.Text;
                dr["docname"] = "";
                dr["docnode"] = null;

                dtUpload.Rows.Add(dr);

                if (dtUpload.Rows.Count > 0)
                {
                    ViewState["dtAddDoc"] = dtUpload;
                    gvA.DataSource = dtUpload;
                    gvA.DataBind();
                }
            }
            txtAttachDesc.Text = "";
        }
       
        protected void btnSave_Click(object sender, EventArgs e)
        {
            string x = txtReqDate.Text;
            saveData();
        }
        #region Data
       
        private string genPRDOCNo()
        {
            string prno = ""; 
            string xno = "";
            string xprefix = "PR";
            string xyearthai = (System.DateTime.Now.Year + 543).ToString();
            int xrunno = 0;
            string sql = "select * from wf_runno where prefix = '"+xprefix+"' and year_thai = " + xyearthai;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xrunno = System.Convert.ToInt32(ds.Tables[0].Rows[0]["runno"].ToString()) +1;
                // Update
                sql = " update wf_runno set runno = " + xrunno.ToString() + " where  prefix = '" + xprefix + "' and year_thai = " + xyearthai;
                zdb.ExecNonQueryReturnID(sql, zconnectionstr);
            }
            else
            {
                xrunno = 1;
                // Insert 
                sql = " insert into wf_runno (prefix, year_thai, runno) " +
                    " values (" +
                    " '" + xprefix + "' " +
                    ", '" + xyearthai + "' " +
                    ", " + xrunno.ToString() + ") "; 
                zdb.ExecNonQuery(sql, zconnectionstr);
            }
            prno = xprefix + "-" + xyearthai.ToString() + xrunno.ToString("00000");  
            return prno; 
        }
        private DateTime convertStringToDate(string xstr)
        {
            DateTime xdate = new System.DateTime();
            return xdate; 
        }
        private String convertYMDdate(string xddmmyyyy)
        {    
            //        dd/mm/yyyy
            string xdd = "";
            string xmm = "";
            string xyyyy = "";  
            try { xdd = xddmmyyyy.Substring(0, 2); } catch { xdd = ""; }
            try { xmm = xddmmyyyy.Substring(3, 2); } catch { xmm = ""; }
            try { xyyyy = xddmmyyyy.Substring(6, 4); } catch { xyyyy = ""; }
            return xyyyy + "-" + xmm + "-" + xdd;
        }
        private void saveData()
        {
            string sql = "";
            EmpInfoClass empinfo = new EmpInfoClass();
            var empstaff = empinfo.getInFoByEmpID(ucEmpStaff.getEmpID());
            if (validateSave() == true)
            {
                
                if (lblPRNO.Text.Trim() == "") {
                   lblPRNO.Text =  genPRDOCNo();
                    ReplaceWordPR();
                    
                    //Insert 
                    #region AddData
                    sql += @" insert into ps_t_pr (
                        pr_no,
                        purchase_org,
                        purchase_group,
                        egat_doc_no,
                        from_org,
                        to_org,
                        subject,
                        req_date,
                        bid_no,
                        bid_revision,
                        purchase_method_id,
                        description,
                        total_medium_price,
                        selling_date,
                        opening_date,
                        li_date,
                        duedate_text_within_month,
                        duedate_delivery,
                        objective,
                        closing_text,
                        remark,
                        pdf_node_id,
                        from_org_emp_id,
                        from_org_emp_name,
                        from_org_emp_s_position,
                        from_org_emp_f_position,
                        to_org_emp_id,
                        to_org_emp_name,
                        to_org_emp_s_position,
                        to_org_emp_f_position,
                        from_emp_id,
                        from_emp_name,
                        from_emp_s_position,
                        from_emp_f_position,
                        from_section_id,
                        from_section_name,
                        from_section_s_position,
                        from_section_f_position,
                        from_mgr_id,
                        from_mgr_name,
                        from_mgr_s_position,
                        from_mgr_f_position,
                        apv1_id,
                        apv1_name,
                        apv1_s_position,
                        apv1_f_position,
                        apv1_status,
                        apv2_id,
                        apv2_name,
                        apv2_s_position,
                        apv2_f_position,
                        apv2_status,
                        apv3_id,
                        apv3_name,
                        apv3_s_position,
                        apv3_f_position,
                        apv3_status,
                        apvfinal_id,
                        apvfinal_name,
                        apvfinal_s_position,
                        apvfinal_f_position,
                        apvfinal_status
                         )
                        values (";
                    sql += "'" + lblPRNO.Text.Trim() + "' ";
                    sql += ",'" + ddlPurchaseOrg.SelectedValue.ToString() + "' ";
                    sql += ",'" + ddlPurchaseGroup.SelectedValue.ToString() + "' ";
                    sql += ",'" + txtDocno.Text.Trim() + "' "; //egat_doc_no,
                    sql += ",'" + ucEmpFrom.getShortPosition() + "' "; //from_org,
                    sql += ",'" + ucEmpTo.getShortPosition() + "' "; //to_org,
                    sql += ",'" + txtSubject.Text.Trim() + "' "; //subject,
                    sql += ",'" + convertYMDdate( txtReqDate.Text.Trim()) + "' "; //req_date,
                    sql += ",'" + hidBidNo.Value + "' ";//    bid_no,
                    sql += "," + hidBidRevision.Value + " "; //    bid_revision,
                    sql += ",'" + ddlPurchaseMethod.SelectedValue.ToString() + "' ";//    purchase_method_id,
                    sql += ",'" + txtBidDesc.Text  + "' ";//    description,
                    sql += "," + txtStdPrice.Text.Replace(",","") + " "; //    total_medium_price,
                    sql += ",'" + txtBidDocSelldate.Text + "' ";//    selling_date,
                    sql += ",'" + txtBidDocRecivedate.Text + "' ";//    opening_date,
                    sql += ",'" + txtLIdate.Text + "' ";//    li_date,
                    sql += ",'" + txtJobPeriod.Text + "' ";//    duedate_text_within_month,
                    sql += ",'" + txtDeliveryDate.Text + "' ";//    duedate_delivery,
                    sql += ",'" + TxtObjective.Text + "' ";//    objective,
                    sql += ",'" + TxtClosing.Text + "' ";//    closing_text,
                    sql += ",'" + txtRemark.Text + "' ";//    remark,
                    sql += ",'" + hidPDF_NODE_ID.Value + "' "; //PDF_NODE_ID
                    sql += ",'" + ucEmpFrom.getEmpID() + "' ";
                    sql += ",'" + ucEmpFrom.getEmpName() + "' ";
                    sql += ",'" + ucEmpFrom.getShortPosition() + "' ";
                    sql += ",'" + ucEmpFrom.getFullPosition() + "' ";
                    sql += ",'" + ucEmpTo.getEmpID() + "' ";
                    sql += ",'" + ucEmpTo.getEmpName() + "' ";
                    sql += ",'" + ucEmpTo.getShortPosition() + "' ";
                    sql += ",'" + ucEmpTo.getFullPosition() + "' ";
                    sql += ",'" + ucEmpStaff.getEmpID() + "' ";
                    sql += ",'" + ucEmpStaff.getEmpName() + "' ";
                    sql += ",'" + ucEmpStaff.getShortPosition() + "' ";
                    sql += ",'" + ucEmpStaff.getFullPosition() + "' ";
                    sql += ",'" + empstaff.SECTION_HEAD_ID + "' ";
                    sql += ",'" + empstaff.SECTION_HEAD_NAME + "' ";
                    sql += ",'" + empstaff.SECTION_HEAD_POSITION + "' ";
                    sql += ",'" + empstaff.SECTION_HEAD_POSITION_FULL + "' ";
                    sql += ",'" + empstaff.MANAGER_ID + "' ";
                    sql += ",'" + empstaff.MANAGER_NAME + "' ";
                    sql += ",'" + empstaff.MANAGER_POSITION + "' ";
                    sql += ",'" + empstaff.MANAGER_POSITION_FULL + "' ";
                    sql += ",'" + ucEmpApv1.getEmpID() + "' ";
                    sql += ",'" + ucEmpApv1.getEmpName() + "' ";
                    sql += ",'" + ucEmpApv1.getShortPosition() + "' ";
                    sql += ",'" + ucEmpApv1.getFullPosition() + "' ";
                    sql += ",'" + " " + "' ";
                    sql += ",'" + ucEmpApv2.getEmpID() + "' ";
                    sql += ",'" + ucEmpApv2.getEmpName() + "' ";
                    sql += ",'" + ucEmpApv2.getShortPosition() + "' ";
                    sql += ",'" + ucEmpApv2.getFullPosition() + "' ";
                    sql += ",'" + " " + "' ";
                    sql += ",'" + ucEmpApv3.getEmpID() + "' ";
                    sql += ",'" + ucEmpApv3.getEmpName() + "' ";
                    sql += ",'" + ucEmpApv3.getShortPosition() + "' ";
                    sql += ",'" + ucEmpApv3.getFullPosition() + "' ";
                    sql += ",'" + " " + "' ";
                    sql += ",'" + ucEmpApvFinal.getEmpID() + "' ";
                    sql += ",'" + ucEmpApvFinal.getEmpName() + "' ";
                    sql += ",'" + ucEmpApvFinal.getShortPosition() + "' ";
                    sql += ",'" + ucEmpApvFinal.getFullPosition() + "' ";
                    sql += ",'" + " " + "') ";
                    #endregion 
                }

                else
                {
                    // Update
                    #region UpdateData
                    sql += @" update  ps_t_pr set
                        pr_no =  '" + lblPRNO.Text.Trim() + @"'  ,
                        purchase_org = '"+ddlPurchaseOrg.SelectedValue+ @"',
                        purchase_group =  '" + ddlPurchaseGroup.SelectedValue + @"',
                        egat_doc_no  = '" + txtDocno.Text.Trim() + @"' ,
                        from_org  = '" + ucEmpFrom.getShortPosition() + @"' ,
                        to_org  = '"+ ucEmpTo.getShortPosition() + @"' ,
                        subject  = '"+ txtSubject.Text.Trim() + @"' ,
                        req_date  = '"+ convertYMDdate(txtReqDate.Text.Trim()) + @"' ,
                        bid_no  = '"+ hidBidNo.Value + @"' ,
                        bid_revision  =  " + hidBidRevision.Value + @" ,
                        purchase_method_id  = '"+ ddlPurchaseMethod.SelectedValue.ToString() + @"' ,
                        description  = '"+ txtBidDesc.Text + @"' ,
                        total_medium_price  = " + txtStdPrice.Text.Replace(",","") + @" ,
                        selling_date  = '"+ txtBidDocSelldate.Text + @"' ,
                        opening_date  = '"+ txtBidDocRecivedate.Text + @"' ,
                        li_date  = '"+ txtLIdate.Text + @"' ,
                        duedate_text_within_month  = '"+ txtJobPeriod.Text + @"' ,
                        duedate_delivery  = '"+ txtDeliveryDate.Text + @"' ,
                        objective  = '"+ TxtObjective.Text + @"' ,
                        closing_text  = '" + TxtClosing.Text + @"' ,
                        remark  = '" + txtRemark.Text + @"' ,
                        pdf_node_id  = '"+ hidPDF_NODE_ID.Value + @"' ,
                        from_org_emp_id  = '" + ucEmpFrom.getEmpID() + @"' ,
                        from_org_emp_name  = '"+ ucEmpFrom.getEmpName() + @"' ,
                        from_org_emp_s_position  = '"+ ucEmpFrom.getShortPosition() + @"' ,
                        from_org_emp_f_position  = '" + ucEmpFrom.getFullPosition() + @"' ,
                        to_org_emp_id  = '" + ucEmpTo.getEmpID() + @"' ,
                        to_org_emp_name  = '" + ucEmpTo.getEmpName() + @"' ,
                        to_org_emp_s_position  = '" + ucEmpTo.getShortPosition() + @"' ,
                        to_org_emp_f_position  = '" + ucEmpTo.getFullPosition() + @"' ,
                        from_emp_id  = '" + ucEmpStaff.getEmpID() + @"' ,
                        from_emp_name  = '" + ucEmpStaff.getEmpName() + @"' ,
                        from_emp_s_position  = '" + ucEmpStaff.getShortPosition() + @"' ,
                        from_emp_f_position  = '" + ucEmpStaff.getFullPosition() + @"' ,
                        from_section_id  = '" + empstaff.SECTION_HEAD_ID + @"' ,
                        from_section_name  = '" + empstaff.SECTION_HEAD_NAME + @"' ,
                        from_section_s_position  = '" + empstaff.SECTION_HEAD_POSITION + @"' ,
                        from_section_f_position  = '" + empstaff.SECTION_HEAD_POSITION_FULL+ @"' ,
                        from_mgr_id  = '" + empstaff.MANAGER_ID + @"' ,
                        from_mgr_name  = '" + empstaff.MANAGER_NAME+ @"' ,
                        from_mgr_s_position  = '" + empstaff.MANAGER_POSITION+ @"' ,
                        from_mgr_f_position  = '" + empstaff.MANAGER_POSITION_FULL+ @"' ,
                       
                        apv1_id  = '" + ucEmpApv1.getEmpID() + @"' ,
                        apv1_name  = '"+ ucEmpApv1.getEmpName() + @"' ,
                        apv1_s_position  = '"+ ucEmpApv1.getShortPosition() + @"' ,
                        apv1_f_position  = '" + ucEmpApv1.getFullPosition() + @"' ,
                        apv1_status  = '' ,
                        apv2_id  = '"+ ucEmpApv2.getEmpID() + @"' ,
                        apv2_name = '" + ucEmpApv2.getEmpName() + @"' ,
                        apv2_s_position  = '"+ ucEmpApv2.getShortPosition() + @"' ,
                        apv2_f_position  = '"+ ucEmpApv2.getFullPosition() + @"' ,
                        apv2_status  = '' ,
                        apv3_id  = '"+ ucEmpApv3.getEmpID() + @"' ,
                        apv3_name  = '" + ucEmpApv3.getEmpName() + @"' ,
                        apv3_s_position  = '" + ucEmpApv3.getShortPosition() + @"' ,
                        apv3_f_position  = '"+ucEmpApv3.getFullPosition() + @"' ,
                        apv3_status  = '' ,
                        apvfinal_id  = '"+ ucEmpApvFinal.getEmpID() + @"' ,
                        apvfinal_name  = '"+ ucEmpApvFinal.getEmpName() + @"' ,
                        apvfinal_s_position  = '"+ ucEmpApvFinal.getShortPosition() +@"' ,
                        apvfinal_f_position  = '"+ ucEmpApvFinal.getFullPosition() + @"' ,
                        apvfinal_status  = '' 
                       ";
                    sql += " where pr_no = '" + lblPRNO.Text + "' ";
                    
                    #endregion 
                }
                //Saving 
                zdb.ExecNonQuery(sql, zconnectionstr);
                string xattach_item_no = "";
                string xhard_copy_yesno = "";
                string xattach_description = "";   
                string xattach_doc_name = "";
                string xattach_node_id = "";
                // Save Attach
                for (int a = 0;  a < gvA.Rows.Count; a++)
                {
                    //
                    xattach_item_no = (a+1).ToString();
                    xattach_doc_name = ((Label)gvA.Rows[a].FindControl("gvA_docname")).Text;
                    xattach_description = ((Label)gvA.Rows[a].FindControl("gvA_Desc")).Text;
                    xattach_node_id = ((Label)gvA.Rows[a].FindControl("gvA_doc_nodeid")).Text;

                    if (checkExistingDoc(lblPRNO.Text, xattach_doc_name) == false)
                    {
                        // Insert Attachment
                        sql = @"insert into ps_t_pr_attachment (
                            pr_no,
                            attach_item_no,
                            hard_copy_yesno,
                            description,
                            attach_doc_name,
                            attach_doc_nodeid
                                ) values (
                        '"+ lblPRNO.Text + @"', 
                        '" + xattach_item_no + @"', 
                        '" + xhard_copy_yesno + @"', 
                        '" + xattach_description + @"', 
                        '" + xattach_doc_name + @"', 
                        '" + xattach_node_id + @"'
                        ) ";
                    }
                    else
                    {
                        // Update Attachment
                        sql = @"update  ps_t_pr_attachment set
                           
                            attach_item_no =  '" + xattach_item_no + @"',
                            hard_copy_yesno =  '" + xhard_copy_yesno + @"', 
                            description = '" + xattach_description + @"',
                            attach_doc_nodeid = '" + xattach_node_id + @"'
                            ";
                        sql += " where pr_no = '" + lblPRNO.Text + "' and attach_doc_name = '" + xattach_doc_name + "' ";
                    }
                    zdb.ExecNonQuery(sql, zconnectionstr);
                }
                Response.Write("<script> alert('Save successfully.');</script>");
            }
            else
            {
                Response.Write("<script> alert('Can not save please check data again.');</script>");
            }
        }
        private bool checkExistingDoc(string xpr_no, string xdoc_name)
        {
            bool xfound = false;
            string sql = " select * from ps_t_pr_attachment where pr_no = '" + xpr_no + "' and attach_doc_name = '"+ xdoc_name+"' ";
            var dt = zdb.ExecSql_DataTable(sql, zconnectionstr);
            if (dt.Rows.Count > 0)
            {
                xfound = true;
            }
            return xfound; 
        }
        #endregion
        private bool validateSave()
        {
            bool xpass = true;
            if (ddlBid.SelectedIndex <= 0) { xpass = false; }
            if (txtReqDate.Text.Trim() == "") { xpass = false; }
           // if (txtProject.Text.Trim() == "") { xpass = false; }
            return xpass;
        }
        protected void ibtnSearchBid_Click(object sender, ImageClickEventArgs e)
        {
            bind_bid(ddlBid.SelectedValue.ToString(), txtRevision.Text.Trim());
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Call Submit to WF

            // Submit doc to Common
            // Submit doc to Common
            SubmitFunction();
           
        }
        public CommonDocResult POSTData(string form_cc, string subject
                                        , string approver_id, string approver_position, string createdby, string strDoc_NodeID
                                        , string doc_no)
        {
            CommonDocResult resultJSON = new CommonDocResult();

            string strJSON_Data = "{\"DocDept\": \"S611030\"" +
                                    ",\"DocDeptName\":\"กบค-ส. กองบริหารงานโครงการ\"" +
                                    ",\"DocDeptOwner\":\"กองจัดซื้อจัดจ้างต่างประเทศสายงานระบบส่ง\"" +
                                    ",\"DocFromName\":\"กบค-ส.\"" +
                                    ",\"DocToName\":\"" + form_cc + "\"" +
                                    ",\"Topic\":\"" + doc_no + " " + subject.Replace("ขออนุมัติ", "ขอส่ง") + "\"" +
                                    ",\"Description\":\"" + doc_no + " " + subject.Replace("ขออนุมัติ", "ขอส่ง") + " ที่ได้รับอนุมัติแล้ว รายละเอียดตามเอกสารแนบ" + "\"" +
                                    ",\"Tel\":\"\"" +
                                    ",\"Fax\":\"\"" +
                                    ",\"Approver\":\"" + approver_id + "\"" +
                                    ",\"ApproverPosition\":\"" + approver_position + "\"" +
                                    ",\"CreatedBy\":\"" + createdby + "\"" +
                                    ",\"SentTos\":\"BS141040\"" +
                                    ",\"SentRemark\":\"บันทึกฉบับนี้ส่งมาจากระบบ Transmission System\"" +
                                    ",\"Attachments\":" + (strDoc_NodeID == "" ? "null}" : "[\"" + strDoc_NodeID.Replace(",", "\",\"") + "\"]}");

            LogHelper.Write("POSTData");
            LogHelper.Write(strJSON_Data);
            JsonSerializerSettings settings = new JsonSerializerSettings();
            settings.MissingMemberHandling = MissingMemberHandling.Ignore;
            try
            {
                var client = new RestClient("https://ecmqacom.egat.co.th/api/InternalDoc");
                client.Timeout = -1;
                var request = new RestRequest(Method.POST);
                request.AddHeader("Content-Type", "application/json");
                request.AddHeader("CommonTicket", "1b8ab4b5cdf12e88bc93dfe56960fa25");
                request.AddParameter("application/json", strJSON_Data, ParameterType.RequestBody);
                IRestResponse response = client.Execute(request);

                LogHelper.Write("ResultData");
                LogHelper.Write(response.Content);

                resultJSON = JsonConvert.DeserializeObject<CommonDocResult>(response.Content, settings);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);

                resultJSON.Status = "E";
                resultJSON.Message = ex.StackTrace;
            }
            return resultJSON;
        }

        protected void btnCancel_Click(object sender, EventArgs e)
        {

        }
        #region Workflow
        private void update_FromOrg()
        {

        }
        private void update_ToOrg() { }
        private void update_STAFF() { }
        private void update_SECTION() { }
        private void update_MGR() { }
        private void update_AD() { }
        private void update_DIRECTOR() { }
        private void update_APPROVER1() { }
        private void update_APPROVER2() { }
        private void update_APPROVER3() { }
        private void update_FINAL_APPROVER() { }
        private void Load_WFAttributes() { }
        private void Update_WFAttribute() { }
        private void Complete_WFTask() { }
        private void SubmitFunction()
        {
                string xlevel = "";
                string xnext_step = "";
                if (hidMode.Value.ToUpper() == "CREATE")
                {
                    EmpInfoClass empinfo = new EmpInfoClass();
                    var empStaff = empinfo.getInFoByEmpID(ucEmpStaff.getEmpID());

                // Check isStaff
                if ( empStaff.MC_SHORT == empStaff.ORGNAME1)
                {
                    xlevel = "VP";
                }
                else if (empStaff.MC_SHORT == empStaff.ORGNAME2)
                {
                    xlevel = "AVP";
                }
                else if (empStaff.MC_SHORT == empStaff.ORGNAME3)
                {
                    xlevel = "DIRECTOR";
                }
                else if (empStaff.MC_SHORT == empStaff.ORGNAME4)
                {
                    xlevel = "MGR";
                }
                else if (empStaff.MC_SHORT == empStaff.ORGNAME5)
                {
                    xlevel = "SECTION";
                }
                else if (empStaff.MC_SHORT == empStaff.ORGNAME5)
                {
                    xlevel = "STAFF";
                }
                
                // Check isSectionHead
                // Check isMgr
                var dtStaff = empinfo.getEmpListInSection("หอจฟ-ส.");

                }
                else if (hidMode.Value.ToUpper() == "EDIT")
                {

                }
                else if (hidMode.Value.ToUpper() == "SECTION")
                {

                }
                else if (hidMode.Value.ToUpper() == "MGR")
                {

                }
                else if (hidMode.Value.ToUpper() == "AD")
                {

                }
                else if (hidMode.Value.ToUpper() == "DIRECTOR")
                {

                }
                else if (hidMode.Value.ToUpper() == "APPROVE 1")
                {

                }
                else if (hidMode.Value.ToUpper() == "APPROVE 2")
                {

                }
                else if (hidMode.Value.ToUpper() == "APPROVE 3")
                {

                }
                else if (hidMode.Value.ToUpper() == "FINAL APPROVE")
                {

                  

                
                }
            CommonDocResult resultJSON = new CommonDocResult();
            if (hidPDF_NODE_ID.Value != "")
            {
                resultJSON = POSTData("", txtSubject.Text.Trim()
                                      , ucEmpApvFinal.getEmpID(), ucEmpApvFinal.getShortPosition()
                                      , ucEmpReq.getEmpID(), hidPDF_NODE_ID.Value, lblPRNO.Text.Trim());
                Response.Write("<script> alert('-Submitted-'); </script>");
                string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                Response.Redirect(strUrl);
            }
            else
            {
                resultJSON.Status = "E";
                resultJSON.Message = "File not found.";
            }
        }
        #endregion

       
    }
}