﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.PR
{
    public partial class ucEmp : System.Web.UI.UserControl
    {
        #region Public
        public string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        #endregion 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ini_gv();
                p1.Visible = false; 
            }
        }
        private void ini_gv()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("emp_id", typeof(string));
            dt.Columns.Add("emp_name", typeof(string));
            dt.Columns.Add("short_position", typeof(string));
            dt.Columns.Add("full_position", typeof(string));

            var dr = dt.NewRow();
            dt.Rows.Add(dr);
            gv1.DataSource = dt;
            gv1.DataBind(); 
        }

        protected void btnReset_Click(object sender, EventArgs e)
        {
            txtEmpID.Text = "";
            txtEmpName.Text = "";
            txtSPosition.Text = "";
            txtFPosition.Text = ""; 
            
        }

        protected void gv1_OnRowCommand (object sender, CommandEventArgs e)
        {
            if (e.CommandName == "selectdata")
            {
                int a = System.Convert.ToInt32(e.CommandArgument);
                string xemp_id = ((Label)gv1.Rows[a].FindControl("gvEmpID")).Text;
                string xemp_name = ((Label)gv1.Rows[a].FindControl("gvEmpName")).Text;
                string xshort_position = ((Label)gv1.Rows[a].FindControl("gvEmpSPosition")).Text;
                string xfull_position = ((Label)gv1.Rows[a].FindControl("gvEmpFPosition")).Text;
                txtEmpID.Text = xemp_id;
                txtEmpName.Text = xemp_name;
                txtSPosition.Text = xshort_position;
                txtFPosition.Text = xfull_position;
                p1.Visible = false; 
            }
        }

        protected void ibtnSearch_Click(object sender, ImageClickEventArgs e)
        {
            var dt = GetApproverCommon( txtEmpID.Text.Trim(), txtEmpName.Text , txtSPosition.Text , txtFPosition.Text );
            if (dt.Rows.Count > 0)
            {
                gv1.DataSource = dt;
                gv1.DataBind(); 
            }
            else { ini_gv(); }
            p1.Visible = true;
        }
        public DataTable GetApproverCommon(string strEmpId,  string strFullName, string strSPosition, string strFposition)
        {
            DataTable dt = new DataTable();
            string strFilter = string.Empty;
            string strSql = @" SELECT distinct ID as emp_id, FULLNAME as emp_name, POSITION_SHORT as short_position,  POSITION_FULLNAME as full_position
                            FROM V_EMPLOYEE_SEARCH 
                            WHERE ID IS NOT NULL  ";

            if (!String.IsNullOrEmpty(strEmpId)) strFilter += " AND ID LIKE '%" + strEmpId + "%' ";
            if (!String.IsNullOrEmpty(strFullName)) strFilter += " AND UPPER(FULLNAME) LIKE '%" + strFullName.ToUpper() + "%' ";
           if (!String.IsNullOrEmpty(strSPosition)) strFilter += " AND UPPER(POSITION_SHORT) LIKE '%" + strSPosition.ToUpper() + "%' ";
            if (!String.IsNullOrEmpty(strFposition)) strFilter += " AND UPPER(POSITION_FULLNAME) LIKE '%" + strFposition.ToUpper() + "%' ";

            strSql += strFilter + " ORDER BY FULLNAME ASC";
          
            dt = ora._getDT(strSql, zcommondb);

            return dt;
        }
        public void setByEmpID(string strEmpId)
        {
            DataTable dt = new DataTable();
            string strFilter = string.Empty;
            string strSql = @" SELECT distinct ID as emp_id, FULLNAME as emp_name, POSITION_SHORT as short_position,  POSITION_FULLNAME as full_position
                            FROM V_EMPLOYEE_SEARCH 
                            WHERE ID IS NOT NULL  ";

            if (!String.IsNullOrEmpty(strEmpId)) strFilter += " AND ID LIKE '%" + strEmpId + "%' ";
           
            strSql += strFilter + " ORDER BY FULLNAME ASC";

            dt = ora._getDT(strSql, zcommondb);
            if (dt.Rows.Count > 0)
            {
                txtEmpID.Text = dt.Rows[0]["EMP_ID"].ToString();
                txtEmpName.Text = dt.Rows[0]["EMP_NAME"].ToString();
                txtSPosition.Text = dt.Rows[0]["SHORT_POSITION"].ToString();
                txtFPosition.Text = dt.Rows[0]["FULL_POSITION"].ToString();
            }
        }
        public string getEmpID()
        {
            return txtEmpID.Text.Trim(); 
        }
        public string getEmpName()
        {
            return txtEmpName.Text.Trim();
        }
        public string getShortPosition()
        {
            return txtSPosition.Text.Trim();
        }
        public string getFullPosition()
        {
            return txtFPosition.Text.Trim();
        }

        protected void btnClearText_Click(object sender, EventArgs e)
        {
            txtEmpID.Text = "";
            txtEmpName.Text = "";
            txtSPosition.Text = "";
            txtFPosition.Text = "";
        }
    }
}