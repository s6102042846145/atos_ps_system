﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="psPRAPV01.aspx.cs" Inherits="PS_System.form.PR.psPRAPV01" %>

<%@ Register src="ucEmp.ascx" tagname="ucEmp" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 138px;
        }

        .auto-style3 {
            width: 600px;
        }

        #docview {
            height: 396px;
            width: 893px;
        }
        .auto-style4 {
            width: 1000px;
        }
        .auto-style5 {
            width: 247px;
        }
        .auto-style6 {
            width: 1017px;
        }
    
*,
*:before,
*:after {
    -webkit-box-sizing: border-box;
    -moz-box-sizing: border-box;
    box-sizing: border-box;
}

    * {
        color: #000 !important;
        text-shadow: none !important;
        background: transparent !important;
        box-shadow: none !important;
    }

    *,*:before,*:after{box-sizing:inherit}

th {
    text-align: center;
}

.Text_60 {
    margin-top: 2px;
    margin-left: 2px;
    font-family: Tahoma;
    font-size: 10pt;
    color: rgb(35, 34, 34);
    width: 60px;
    background-color: white;
    border-color: silver;
    border-radius: 2px;
}
.Text_400 {
    margin-top: 2px;
    margin-left: 2px;
    font-family: Tahoma;
    font-size: 10pt;
    color: rgb(35, 34, 34);
    width: 400px;
    background-color: white;
    border-color: silver;
    border-radius: 2px;
}
        </style>
      <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel5" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="lblHeader" runat="server" Font-Bold="True" Font-Names="Tahoma" Font-Size="11pt" Text="CM - PR Workflow"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <table class="auto-style1" style="font-family: tahoma; font-size: 10pt; color: #333333">
            <tr>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Title"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style3">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel9" runat="server" Width="90px">
                                    <asp:Label ID="lblRequestor" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" Text="Requestor :" Width="150px"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" Width="500px">
                                    <asp:Label ID="lblRequestorVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Panel ID="Panel7" runat="server" Width="60px">
                                    <asp:Label ID="lblSubmitDate" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" Text="PR No.  :" Width="100px"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" Width="500px">
                                    <asp:Label ID="lblPRNO" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTo" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="To :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel4" runat="server" Width="500px">
                                    <asp:Label ID="lblToVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>

                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Subject :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server">
                                    <asp:Label ID="lblSubjectVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="p_PDF" runat="server" BackColor="#666666" Height="400px" Width="900px">
                        <iframe id="docview" src="about:blank" runat="server" class="auto-style6"></iframe>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="p_Purchase" runat="server">
                        <table class="auto-style4">
                            <tr>
                                <td class="auto-style5">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="Label22" runat="server" ForeColor="Blue" Text="Assign to Section" Width="200px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">
                                    &nbsp;</td>
                                <td>
                                    <uc1:ucEmp ID="ucEmpSection" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>
                                    <asp:Label ID="Label23" runat="server" ForeColor="Blue" Text="Assign to Staff" Width="200px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>
                                    <uc1:ucEmp ID="ucBuyer" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="p_Account" runat="server">
                        <table class="auto-style4">
                            <tr>
                                <td class="auto-style5">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="Label24" runat="server" ForeColor="Blue" Text="Assign to Section" Width="200px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>
                                    <uc1:ucEmp ID="ucACCSection" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">
                                    &nbsp;</td>
                                <td>
                                    <asp:Label ID="Label25" runat="server" ForeColor="Blue" Text="Assign to Staff" Width="200px"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>
                                    <uc1:ucEmp ID="ucAccStaff" runat="server" />
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style5">&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Note"></asp:Label>
                </td>
            </tr>
            <tr>

                <td>
                    <asp:Panel ID="p_Comment" runat="server">
                        <asp:TextBox ID="txtComment" runat="server" Font-Names="tahoma" Font-Size="10pt" Height="80px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style2">
                        <tr>
                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Font-Names="Tahoma" Font-Size="10pt"  Text="SUBMIT" Width="100px" OnClick="btnSubmit_Click" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                               <asp:Button ID="btnAssign" runat="server" Font-Names="Tahoma" Font-Size="10pt"  Text="ASSIGN" Width="100px"  />
                            
                            </td>
                            <td><asp:Button ID="btnApprove" runat="server" Font-Names="Tahoma" Font-Size="10pt" OnClick="btnApprove_Click" Text="Approve" Width="100px" /></td>
                            <td>
                                <asp:Button ID="btnReject" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Reject" Width="100px" OnClick="btnReject_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                   <asp:Label ID="lblSchedule" runat="server" Text="Price Schedule" Width="200px" ForeColor="Blue"></asp:Label>
                   <asp:DropDownList ID="ddlSchedule" runat="server" CssClass="ddl_normal" Width="450px" AutoPostBack="True" OnSelectedIndexChanged="ddlSchedule_SelectedIndexChanged"></asp:DropDownList>
                  </td>
            </tr>
            <tr>
                <td>
                     
                     <asp:Panel ID="pgv_Price" runat="server" Height="300px" ScrollBars="Vertical" BackColor="#669999">
                         <asp:GridView ID="gvP" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No data found." Font-Names="tahoma" Font-Size="9pt" Width="86%" OnRowCommand="gvP_OnRowCommand">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Item No.">
                                    <ItemTemplate>
                                        <asp:Label ID="gvP_lblNo" CssClass="Text_60" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                      
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="gvP_lblDesc" CssClass="Text_200" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                     
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="200px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label ID="gvP_lblQty" CssClass="Text_100" runat="server" Text='<%# Bind("qty") %>'></asp:Label>
                                     
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Unit">
                                    <ItemTemplate>
                                        <asp:Label ID="gvP_lblUnit" CssClass="Text_100" runat="server" Text='<%# Bind("unit") %>'></asp:Label>
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Std. Price">
                                    <ItemTemplate>
                                        <asp:Label ID="gvP_lblStdPrice" CssClass="Text_100" runat="server" Text='<%# Bind("std_price") %>'></asp:Label>
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="120px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Currency">
                                    <ItemTemplate>
                                        <asp:Label ID="gvP_lblCurrency" CssClass="Text_100" runat="server" Text='<%# Bind("currency") %>'></asp:Label>
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="120px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Acc. Assignment code">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvP_txtAccAsignmentCode" CssClass="Text_100" runat="server" Text='<%# Bind("account_assignment_code") %>'></asp:TextBox>
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="130px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Cost Center">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvP_txtCC" CssClass="Text_100" runat="server" Text='<%# Bind("costcenter") %>'></asp:TextBox>
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="130px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="WBS No.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvP_txtWBS" CssClass="Text_200" runat="server" Text='<%# Bind("wbsno") %>'></asp:TextBox>
                                       
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="130px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                         <asp:Button ID="btnupdate_gvP" Text="Save" runat="server" CssClass="btn_normal_blue" OnClick="btnupdate_gvP_Click" /> 
                    </asp:Panel>
              
                </td>
            </tr>
            <tr>
                <td>
                                    <asp:Label ID="Label26" runat="server" Text="Attachments" Width="200px" ForeColor="Blue"></asp:Label>
                  </td>
            </tr>
            <tr>
                <td>
              
                     <asp:Panel ID="pgv_Attach" runat="server" Height="200px" ScrollBars="Vertical">
                        <asp:GridView ID="gvA" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No data found." Font-Names="tahoma" Font-Size="9pt" Width="71%" OnRowCommand="gvA_OnRowCommand">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <asp:Label ID="gvA_lblNo" CssClass="Text_60" runat="server" Text='<%# Bind("itemno") %>'></asp:Label>
                                        <%--Text='<%# Bind("job_no") %>'--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="ประเภทสิ่งแนบ">
                                    <ItemTemplate>
                                        <asp:Label ID="gvA_attachtype" runat="server" Text='<%# Bind("attachtype") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="gvA_Desc" CssClass="Text_400" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document Name">
                                    <ItemTemplate>
                                        <asp:Label ID="gvA_docname" CssClass="Text_400" runat="server" Text='<%# Bind("docname") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Remove">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgRemove" runat="server" Height="20px" ImageAlign="Right" ImageUrl="~/images/bin.png" Width="20px" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'  />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
              
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label27" runat="server" Text="Comment Logs" Width="200px" ForeColor="Blue"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;<asp:TextBox ID="txtCommentLog" runat="server" Width="792px"></asp:TextBox>
&nbsp;<asp:ImageButton ID="ibtnComment" runat="server" Height="30px" ImageUrl="~/images/save.png" ToolTip="Insert Comment" />
&nbsp;<asp:Panel ID="pgv_Comment" runat="server" Height="200px" ScrollBars="Vertical">
                        <asp:GridView ID="gvC" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No data found." Font-Names="tahoma" Font-Size="9pt" Width="71%" OnRowCommand="gvC_OnRowCommand">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <asp:Label ID="gvC_lblNo" CssClass="Text_60" runat="server" Text='<%# Bind("itemno") %>'></asp:Label>
                                        <%--Text='<%# Bind("job_no") %>'--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                </asp:TemplateField>
                                  <asp:TemplateField HeaderText="DateTime">
                                    <ItemTemplate>
                                        <asp:Label ID="gvC_Datetime" runat="server" Text='<%# Bind("datetime") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Commented By">
                                    <ItemTemplate>
                                        <asp:Label ID="gvC_Commented_By" runat="server" Text='<%# Bind("commented_by") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Comment">
                                    <ItemTemplate>
                                        <asp:Label ID="gvC_Comment" CssClass="Text_400" runat="server" Text='<%# Bind("commented") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                                </asp:TemplateField>
                               
                                <asp:TemplateField HeaderText="Remove">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="imgRemove" runat="server" Height="20px" ImageAlign="Right" ImageUrl="~/images/bin.png" Width="20px" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'  />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidRequestorId" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" />
        <asp:HiddenField ID="hidWorkID" runat="server" />
        <asp:HiddenField ID="hidSubworkID" runat="server" />
        <asp:HiddenField ID="hidTaskID" runat="server" />
        <asp:HiddenField ID="hidPRNo" runat="server" />
        <asp:HiddenField ID="hidProcessID" runat="server" />
        <br />
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <br />
        <asp:HiddenField ID="hidBidRevision" runat="server" />
    </form>
</body>
</html>
