﻿<%@ Page Language="C#" MasterPageFile="~/MainPagesMaster.Master" AutoEventWireup="true" CodeBehind="psPR02.aspx.cs" Inherits="PS_System.form.PR.psPR02" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - PR Search"></asp:Label>
    <div id="dvMain" runat="server" style="width: 100%; padding-top: 10px;">
        <table style="width: 100%; margin: 10px">
            <tr>
                <td colspan="4" style="text-align: left; width: 8%">
                    <asp:Label ID="Label6" runat="server" Text="PR No: " Width="6%"></asp:Label>
                    <asp:TextBox ID="txtPr_no" runat="server" />
                    <br />
                     <asp:Label ID="Label1" runat="server" Text="From Org: " Width="6%"></asp:Label>
                    <asp:TextBox ID="txtFrom_org" runat="server" />
                    <br />
                     <asp:Label ID="Label2" runat="server" Text="PR Status: " Width="6%"></asp:Label>
                    <%--<asp:TextBox ID="txtPr_status" runat="server" />   --%>             
                    <asp:DropDownList ID="drpStatus" runat="server"></asp:DropDownList>
                    <br />
                    <asp:Button ID="btnFil" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFil_Click"/>
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 100%; overflow: scroll; height: 530px;">
        <asp:GridView ID="gvData" runat="server" Width="100%" 
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="true">
            <FooterStyle BackColor="#164094" ForeColor="#000066" />
            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField HeaderText="No.">
                    <ItemTemplate>
                        <asp:Label ID="lbrow_id" runat="server" Text='<%# Bind("row_id") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PR No">
                    <ItemTemplate>
                        <asp:Label ID="lbpr_no" runat="server" Text='<%# Bind("pr_no") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Bid No">
                    <ItemTemplate>
                        <asp:Label ID="lbbid_no" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                			 			

                <asp:TemplateField HeaderText="Subject">
                    <ItemTemplate>
                        <asp:Label ID="lbsubject" runat="server" Text='<%# Bind("subject") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="left" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="From Org">
                    <ItemTemplate>
                        <asp:Label ID="lbfrom_org" runat="server" Text='<%# Bind("from_org") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="To Org">
                    <ItemTemplate>
                        <asp:Label ID="lbto_org" runat="server" Text='<%# Bind("to_org") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PR Status">
                    <ItemTemplate>
                        <asp:Label ID="lbpr_status" runat="server" Text='<%# Bind("pr_status") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
               <asp:TemplateField>
                    <ItemTemplate>
                        <%--<asp:ImageButton ID="gvBtnEdit" runat="server" OnClick="editData_Click" ImageUrl="~/images/edit.png" Height="17px" Width="17px" />--%>                        
                        <%--<asp:Label ID="lbpr_status" runat="server" Text='Edit' OnClick="editData_Click"></asp:Label>--%>
                      <%--  <a runat="server" href="#" onserverclick="editData_Click">Edit</a>
                        <asp:HiddenField ID="hidBidNo" runat="server" />--%>
                        <a href="#"  onclick="EditData('<%#Eval("pr_no")%>','<%#Eval("bid_no")%>','<%#Eval("bid_revision")%>');">Edit</a>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
   
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRev" runat="server" />
    <asp:HiddenField ID="hidJobNo" runat="server" />
    <asp:HiddenField ID="hidJobRev" runat="server" />
    <asp:HiddenField ID="hidLogin" runat="server" />
    <asp:HiddenField ID="hidDept" runat="server" />
    <asp:HiddenField ID="hidSect" runat="server" />
    <asp:HiddenField ID="hidMc" runat="server" />
    <script>

        function EditData(pr_no, bid_no, bid_revision) {
            var url = window.location.href+"?";  
            var link =  url.replace("psPR02", "psPR01") + "pr_no=" + pr_no + "&bid_no=" + bid_no + "&bid_revision=" + bid_revision;
            window.open(link.replace("#",""), '_blank');
        }
    </script>


</asp:Content>