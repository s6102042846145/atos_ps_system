﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTaskBid.aspx.cs" Inherits="PS_System.form.Bid.MyTaskBid" MaintainScrollPositionOnPostback="true" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        
        .GVFixedHeader {

            position: relative;
            top: expression(this.parentNode.parentNode.parentNode.scrollTop-1);
        }
        .overlay {
            position: fixed;
            width: 100%;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background: rgba(0,0,0,.85);
            z-index: 10000;
        }
        .popup {
            width: 33.333333%;
            padding: 15px;
            left: 0;
            margin-left: 33.333333%;
            border: 1px solid #ccc;
            border-radius: 10px;
            background: white;
            position: fixed;
            top: 15%;
            box-shadow: 5px 5px 5px #000;
           z-index: 1;
        }
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .btnclass{
            
            background:white;
            color:#508bcb;
            border:none;
            height:40px;
            width:80px;
            border-radius:5px;
            font-size:small;
        }
        .btnSel{
            
            background:white;
            color:black;
            height:40px;
            width:80px;
            border-left:solid;
            border-top:solid;
            border-right:solid;
            border-bottom:none;
            border-width:thin;
            border-radius:5px 5px 0px 0px;
            font-size:small;
        }
        .btnclass:hover {
            background: #eeeeee;
        }
        .btnPartSel{
            color:white;
            border-color:#357ebd;
            background-color:#357ebd;
            border:1px solid transparent;
            border-radius:3px;
        }

        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .Colsmall {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
             .Colsmall2 {
            width: 80px;
            max-width: 80px;
            min-width: 80px;
            text-align:center;
        }
            #gvAllEqui   th {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
      
    </style>
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>



    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <script>
        $(document).ready(function () {

            
            $('[id*=ddl_dr]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=MultiselectDr]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=ddl_wa]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=ddl_meterialGrp]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=ddl_inco]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 1,
                maxHeight: 300
            });
            $('#ddl_jobNoDr').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#ddl_jobPart').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#ddl_schedulePart').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#ddl_schNoDr').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });

            $('#ddl_jobNoeq').multiselect({
                includeSelectAllOption: true,
                enableFiltering: false,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });

            $('#ddl_schNoeq').multiselect({
                includeSelectAllOption: true,
                enableFiltering: false,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });

            $('#ddl_jobNoDoc').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#ddl_schNoDoc').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#Multiselect_Inco').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });

            
            $('#Multiselect_Sch').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('#Multiselect_Job').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('#Multiselect_Drbid').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('#Multiselect_Drname').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_Inco]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_Bid]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#Multiselect_AddJob').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
            $('#Multiselect_AddSch').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });

            $('[id*=Multiselect_DocJob]').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_DocSch]').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_DDJob]').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_DDSch]').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_DocSelectJob]').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            $('[id*=Multiselect_DocSelectSch]').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });
            
            //document.getElementById("rdbEquip").checked = true;
            setDisplay_SearchTypeFirst();
            datatableConfig();
            
        });
        $(document).ready(function () {
            var width = new Array();
            var table = $("table[id*=gvAllEqui]"); //Pass your gridview id here.
            table.find("th").each(function (i) {
                width[i] = $(this).width();
            });
            headerRow = table.find("tr:first");
            headerRow.find("th").each(function (i) {
                $(this).width(width[i]);
            });
            firstRow = table.find("tr:first").next();
            firstRow.find("td").each(function (i) {
                $(this).width(width[i]);
            });
            var header = table.clone();
            header.empty();
            header.append(headerRow);
            //header.append(firstRow);
            header.css("width", width);
            $("#container").before(header);
            table.find("tr:first td").each(function (i) {
                $(this).width(width[i]);
            });
            $("#container").height(700);
            //$("#container").width(table.width() + 20);

            $("#container").append(table);
        });
        function datatableConfig() {

            $('#gvItem').DataTable({
            destroy: true,
            searching: false,
            lengthChange: true,
            paging: true
        });
        }
        function chkrmk(x) {
            document.getElementById("tb_val").value = x;
        }
        
        
        function delPart(part,job,sch,bid) {
            document.getElementById("hidDelPart").value = part;
            document.getElementById("hidDelJobNo").value = job;
            document.getElementById("hidDelSchNo").value = sch;
            document.getElementById("hidDelBidNo").value = bid;
            
            OverlayPopup.style["display"] = "block";
        }
        function selectPart(objCHK) {
            var selectValue = objCHK;
            document.getElementById("hidSelPart").value = selectValue;
            document.getElementById("tbAddPath").value = selectValue;
            
            
        }
        function GetFileName(myFile) {
            var file = myFile.files[0];
            document.getElementById("tb_filename").value = file.name.substring(0, file.name.lastIndexOf('.'));;
        }
        function GetFileNameDoc(myFile) {
            var file = myFile.files[0];
            document.getElementById("tb_filenameDoc").value = file.name.substring(0, file.name.lastIndexOf('.'));;
        }
        function NumOnlyChk(e) {
            var key;
            if (window.event) // IE
            {
                key = e.keyCode;
            }
            else if (e.which) // Netscape/Firefox/Opera
            {
                key = e.which;
            }
            var vchar = String.fromCharCode(key);
            if (((vchar >= "0" && vchar <= "9") && (key != 8) || ((vchar == "." || key == 49) && (key != 8)))) {

                e.returnValue = true;
            }
            else {
                return false;
            }
            e.returnValue = true;
            return true;
        }
        function NumOnly(num) {

            var Str = 'Lot,Lump sum';
            var chkStr = Str.includes(num.value);
            var chkNum = isNaN(parseFloat(num.value));
            if (chkStr == true || chkNum == false) {
                e.returnValue = true;
            }
            else {
                alert("Please re-check QTY. You can use numeric and Text(Lot / Lump sum) only.");
                num.value = "";
                return false;
            }
            e.returnValue = true;
            return true;
        }
        function CalcSOE(index) {
            var unit = document.getElementById("gv_SelEquip_tb_supUnit_" + index).value;
            var qty = document.getElementById("gv_SelEquip_tb_Req_" + index).value;
            if (qty != "") {
                var chk = isNaN(parseInt(qty));
                if (chk == true) qty = 1;
                    document.getElementById("gv_SelEquip_tb_supAmou_" + index).value = unit * qty;
            }
        }
        function CalcLSE(index) {
            var unit = document.getElementById("gv_SelEquip_tb_localSUnit_" + index).value;
            var qty = document.getElementById("gv_SelEquip_tb_Req_" + index).value;
            if (qty != "") {
                var chk = isNaN(parseInt(qty));
                if (chk == true) qty = 1;
                document.getElementById("gv_SelEquip_tb_localSAmou_" + index).value = unit * qty;
            }
        }
        function CalcLTC(index) {
            var unit = document.getElementById("gv_SelEquip_tb_localTUnit_" + index).value;
            var qty = document.getElementById("gv_SelEquip_tb_Req_" + index).value;
            if (qty != "") {
                var chk = isNaN(parseInt(qty));
                if (chk == true) qty = 1;
                document.getElementById("gv_SelEquip_tb_localTAmou_" + index).value = unit * qty;
            }
        }
        function setDisplay_SearchTypeFirst() {
            if (document.getElementById("rdbBom").checked == true) {
                document.getElementById("tbsearchtype").style.display = "block";
                document.getElementById("ddlSelectBid").style.display = "none";
                document.getElementById("ddlSelectBom").style.display = "none";
                document.getElementById("tbsearchtype").placeholder = "BOM Name or BOM Description";
                document.getElementById("btnSearch").style.display = "block";

            }
            else if (document.getElementById("rdbEquip").checked == true) {
                document.getElementById("tbsearchtype").style.display = "block";
                document.getElementById("ddlSelectBid").style.display = "none";
                document.getElementById("ddlSelectBom").style.display = "none";
                document.getElementById("tbsearchtype").placeholder = "Equipment Code or Full Description or Short Description or Service";
                document.getElementById("btnSearch").style.display = "block";

            }
            //else if (document.getElementById("rdbMytemp").checked == true) {
            //    document.getElementById("tbsearchtype").style.display = "none";
            //    document.getElementById("ddlSelectBid").style.display = "block";
            //    document.getElementById("ddlSelectBom").style.display = "none";
            //    document.getElementById("btnSearch").style.display = "block";

            //}
            //else if (document.getElementById("rdbPrejob").checked == true) {
            //    document.getElementById("tbsearchtype").style.display = "none";
            //    document.getElementById("ddlSelectBid").style.display = "block";
            //    document.getElementById("ddlSelectBom").style.display = "none";
            //    document.getElementById("btnSearch").style.display = "block";

            //}
            else if (document.getElementById("rdbOther").checked == true) {
                document.getElementById("tbsearchtype").style.display = "none";
                document.getElementById("ddlSelectBid").style.display = "none";
                document.getElementById("ddlSelectBom").style.display = "none";
                document.getElementById("btnSearch").style.display = "none";
                document.getElementById("btnAddItem").style.visibility = "visible";
            }

        }
        function setDisplay_SearchType() {
            if (document.getElementById("rdbBom").checked == true) {
                document.getElementById("tbsearchtype").style.display = "block";
                document.getElementById("ddlSelectBid").style.display = "none";
                document.getElementById("ddlSelectBom").style.display = "none";
                document.getElementById("tbsearchtype").placeholder = "BOM Name or BOM Description";
                document.getElementById("btnSearch").style.display = "block";
                $('[id*=gvItem]').hide();
                document.getElementById("tbsearchtype").value = "";
               
            }
            else if (document.getElementById("rdbEquip").checked == true) {
                document.getElementById("tbsearchtype").style.display = "block";
                document.getElementById("ddlSelectBid").style.display = "none";
                document.getElementById("ddlSelectBom").style.display = "none";
                document.getElementById("tbsearchtype").placeholder = "Equipment Code or Full Description or Short Description";
                document.getElementById("btnSearch").style.display = "block";
                $('[id*=gvItem]').hide();
                document.getElementById("tbsearchtype").value = "";
            }
            //else if (document.getElementById("rdbMytemp").checked == true) {
            //    document.getElementById("tbsearchtype").style.display = "none";
            //    document.getElementById("ddlSelectBid").style.display = "block";
            //    document.getElementById("ddlSelectBom").style.display = "none";
            //    document.getElementById("btnSearch").style.display = "block";
            //    $('[id*=gvItem]').hide();
            //    document.getElementById("tbsearchtype").value = "";
            //}
            //else if (document.getElementById("rdbPrejob").checked == true) {
            //    document.getElementById("tbsearchtype").style.display = "none";
            //    document.getElementById("ddlSelectBid").style.display = "block";
            //    document.getElementById("ddlSelectBom").style.display = "none";
            //    document.getElementById("btnSearch").style.display = "block";
                
            //}
            else if (document.getElementById("rdbOther").checked == true) {
                document.getElementById("tbsearchtype").style.display = "none";
                document.getElementById("ddlSelectBid").style.display = "none";
                document.getElementById("ddlSelectBom").style.display = "none";
                document.getElementById("btnSearch").style.display = "none";
                document.getElementById("btnAddItem").style.visibility = "visible";
            }
        }
        function closePopup() {
            var dv = document.getElementById("myModal");
            if (dv != null) {
                dv.style.display = "none";
            }
        }
        function closePop() {
            var dv = document.getElementById("OverlayPopup");
            if (dv != null) {
                dv.style.display = "none";
            }
        }
        function closeDr() {
            var dv = document.getElementById("myModal");
            var dvDr = document.getElementById("dvhiddraw");
            if (dv != null && dvDr != null) {
                dv.style.display = "none";
                dvDr.style.display = "block";
            }
        }
        function closeDoc() {
            var dv = document.getElementById("myModal");
            var dvDr = document.getElementById("dvhidDoc");
            if (dv != null && dvDr != null) {
                dv.style.display = "none";
                dvDr.style.display = "block";
            }
        }
        function HeaderClick(CheckBox, gv) {
            var TargetBaseControl = document.getElementById(gv);
            var TargetChildControl = "cb_sub";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                    Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false)
                    Inputs[n].checked = CheckBox.checked;
        }

        function openPop(flag) {
            var dveqpop = document.getElementById("dv_addEqui");
            var mainpop = document.getElementById("myModal");
            
            var dvpop = document.getElementById("dvpopUpload");
            var dvatt = document.getElementById("dvAtt");
            var dvadddoc = document.getElementById("dvAddDocument");

            var TabUploadDoc = document.getElementById("TabUploadDoc");
            var TabSelectDoc = document.getElementById("TabSelectDoc");
            var liSelDoc = document.getElementById("liSelDoc");
            var liUpDoc = document.getElementById("liUpDoc");
            var TabSelect = document.getElementById("TabSelect"); //dv_docgv
            var TabUpload = document.getElementById("TabUpload"); //dv_docup
            //var liSel = document.getElementById("liSel");
            var liUp = document.getElementById("liUp");
            var TabAllDoc = document.getElementById("TabAllDoc");
            var TabAll = document.getElementById("TabAll");
            var dvpart = document.getElementById("dv_addPart");
            var dv_viewLegend = document.getElementById("dv_viewLegend");
            var li1 = document.getElementById("li1");
            var li2 = document.getElementById("li2");
            var TabLegend = document.getElementById("TabLegend");
            var TabEquip = document.getElementById("TabEquip");
            var dvConfig = document.getElementById("dvConfig");
            
            if (mainpop != null) {
                mainpop.style.display = "block";
            }
            if (flag == '1') {
                dvpop.style.display = "block";
                dvatt.style.display = "table";
                dvadddoc.style.display = "none";
                dveqpop.style.display = "none";

                //TabSelectDoc.style.display = "none";
                //TabUploadDoc.style.display = "none";
                //liSel.removeAttribute("class");
                liUp.setAttribute("class", "active");
                TabUpload.setAttribute("class", "tab-pane active");
                TabSelect.setAttribute("class", "tab-pane");
                TabAll.style.display = "block";
                dvpart.style.display = "none";
                dv_viewLegend.style.display = "none";
                dvConfig.style.display = "none";

            }
            else if (flag == '2') {
                dveqpop.style.display = "block";
                TabAllDoc.style.display = "none";
                TabAll.style.display = "none";
                dvpart.style.display = "none";
                dv_viewLegend.style.display = "none";
                li1.setAttribute("class", "active");
                li2.removeAttribute("class");
                TabEquip.setAttribute("class", "tab-pane active");
                TabLegend.setAttribute("class", "tab-pane");
                dvConfig.style.display = "none";
            }
            else if (flag == "3") {
                dvpop.style.display = "none";
                dvatt.style.display = "none";
                dvadddoc.style.display = "table";
                dveqpop.style.display = "none";
                //TabSelect.style.display = "none";
                //TabUpload.style.display = "none";
                liSelDoc.removeAttribute("class");
                liUpDoc.setAttribute("class", "active");
                TabUploadDoc.setAttribute("class", "tab-pane active");
                TabSelectDoc.setAttribute("class", "tab-pane");
                TabAllDoc.style.display = "block";
                dvpart.style.display = "none";
                dv_viewLegend.style.display = "none";
                dvConfig.style.display = "none";
            }
            else if (flag == "4") {
                dveqpop.style.display = "none";
                dvdrpop.style.display = "none";
                dvdocpop.style.display = "block";
                dv_docgv.style.display = "none";
                dv_docup.style.display = "block";
                dvpart.style.display = "none";
                dv_viewLegend.style.display = "none";
                dvConfig.style.display = "none";
            }
            else if (flag=="5") {
                dveqpop.style.display = "none";
                dvadddoc.style.display = "none";
                dveqpop.style.display = "none";
                TabAllDoc.style.display = "none";
                TabAll.style.display = "none";
                dvpart.style.display = "block";
                dv_viewLegend.style.display = "none";
                dvConfig.style.display = "none";
            }
            else if (flag == "6") {
                dvConfig.style.display = "block";
                dveqpop.style.display = "none";
                dvadddoc.style.display = "none";
                dveqpop.style.display = "none";
                TabAllDoc.style.display = "none";
                TabAll.style.display = "none";
                dvpart.style.display = "none";
                dv_viewLegend.style.display = "none";
            }
        }
        function openTabEquiSub(dv) {
            var div = document.getElementById(dv.id);
            if (div.style.display == "none") {
                div.style.display = "block"
            }
            else {
                div.style.display = "none"
            }
        }
        //function openTabEquiSub(dv) {
        //    var dv = document.getElementById(dv.id);
        //    alert(dv.style);
        //    if (window.getComputedStyle(dv).display === "collapse") {
        //        // Do something..
        //        dv.style.visibility = "visibie";
        //    }
        //    else {
        //        dv.style.visibility = "collapse"
        //    }

        //}
        function openTabDr(dvdrhidden) {
            var dv = document.getElementById("dvdrhidden");
            if (dv.style.display == "none") {
                dv.style.display = "block"
            }
            else {
                dv.style.display = "none"
            }
        }
        function openTabEqui(dvequihidden) {
            var dv = document.getElementById("dvequihidden");
            if (dv.style.display == "none") {
                dv.style.display = "block"
            }
            else {
                dv.style.display = "none"
            }
        }
        function openTabDoc(dvdochidden) {
            var dv = document.getElementById("dvdochidden");
            if (dv.style.display == "none") {
                dv.style.display = "block"
            }
            else {
                dv.style.display = "none"
            }
        }
        function HeaderClickEquiTabAll(CheckBox, gv) {
            var cbAll = document.getElementById(CheckBox);
            var TargetBaseControl = document.getElementById(gv);
            var TargetChildControl = "cb_sub";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                    Inputs[n].id.indexOf(TargetChildControl, 0) >= 0)
                    Inputs[n].checked = cbAll.checked;
        }
        function getPart() {
            document.getElementById("hidSearchPart").value = document.getElementById("ddl_searchPart").value;
        }
        function chkAll(Checkbox, gv) {
            var TargetControl = document.getElementById(gv);
            var TargetChildControl = "CbAttChk,CbTemChk,CbHisChk,CbItemChk,CbDocChk,CbDChk,cb_sub";

            var Inputs = TargetControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; n++) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= -1 && Inputs[n].disabled == false) {
                    Inputs[n].checked = Checkbox.checked;
                }
            }
        }

        function openTab(chk, ima) {
            var x = document.getElementById(chk);
            var img = document.getElementById(ima);
            if (x.style.display === "none") {
                x.style.display = "block";
                img.src = '../../images/sort_asc.png';

            } else {
                x.style.display = "none";
                img.src = '../../images/sort_desc.png';
            }
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - My Task (Bid)"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center" >
            <table style="width: 95%">
                <tr style="padding-left: 5px">
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left; color: #006699;"><b>My Task (Bid)</b></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">
                                    <asp:Label ID="lb_bidNo" runat="server" Text="Bid No:"></asp:Label>

                                </td>
                                <td style="width: 50%">
                                    <asp:TextBox ID="tb_bidNo" runat="server" Width="100px" ReadOnly="True"></asp:TextBox>
                                    <asp:Label ID="Label1" runat="server" Width="2%" visibility="hidden"></asp:Label>
                                    <asp:Label ID="lb_rev" runat="server" Text="Rev.:" Width="8%"></asp:Label>
                                    <asp:TextBox ID="tb_rev" runat="server" Width="50px" ReadOnly="True"></asp:TextBox>
                                </td>
                                <td style="width: 40%">
                                    <asp:Label ID="lb_acName" runat="server" Text="Activity Name:" ></asp:Label>
                                    <asp:TextBox ID="tb_acName" runat="server" Width="350px" ReadOnly="True"></asp:TextBox>
                                    <%--<table style="width: 95%;">
                                        <tr style="height: 10%;">
                                            <td style="width: 20%; text-align: right; vertical-align: initial;">
                                                <asp:Label ID="Label22" runat="server" Text="Bid Progress:"></asp:Label>
                                            </td>
                                            <td style="width: 80%; text-align: left; vertical-align: middle;">
                                                <div class="progress">
                                                    <div id="divProg" runat="server" class="progress-bar progress-bar-striped" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 90%;">
                                            <td colspan="2">
                                                <asp:Label ID="lblProgress_Department" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>--%>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lb_bidDesc" runat="server" Text="Bid Description:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tb_desc" runat="server" Width="650px" ReadOnly="True" BorderStyle="Solid" BorderColor="#D8D8D8"></asp:TextBox>
                                </td>
                                   <td rowspan="5" style="vertical-align:top;">
                                     <div style="overflow-y:auto;height:250px;">
                                        <asp:GridView ID="gvAttach" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." >
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Attachment Name">
                                    <ItemTemplate>
                                     <asp:LinkButton ID="lnkAtt_view" runat="server" Text='<%# Bind("att_url") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="350px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Attachment Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAtt_date" runat="server" Text='<%# Bind("upload_datetime") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                             <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                               
                                <td >
                               
                                    <asp:Label ID="Label8" runat="server" Text="Plan Start Date:"></asp:Label>
                                </td>
                                <td style="text-align:left;">
                                    <asp:TextBox ID="txtStartDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="39%"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label26" runat="server" Text="Plan End Date:"></asp:Label>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtEndDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="40%"></asp:TextBox>
                                </td>
                               
                              <%--  <td>
                                    
                                </td>
                                <td style="text-align:left;">
                                 
                                </td>--%>
                            </tr>
                 
                              <tr>
                                <td>
                                    <asp:Label ID="Label27" runat="server" Text="Completion Date:"></asp:Label>
                                </td>
                                <td colspan="1" style="text-align:left;">
                                  <div  id="dvlblCom" runat="server" style="border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;">
                                    <asp:Label ID="lblCom" runat="server" ></asp:Label>
                                      </div>
                                </td>
                            </tr>
                           <tr>
                                <td>
                                    <asp:Label ID="Label30" runat="server" Text="Remark Bid:"></asp:Label>
                                </td>
                                <td >
                                    <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="96%" Rows="3" TextMode="MultiLine"></asp:TextBox>

                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td style="text-align: left; color: #006699;" colspan="2"><b>Bid - Schedule Summary</b></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:GridView ID="gvBidSumary" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvBidSumary_RowDataBound">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                         <%--   <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="cb_all" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_sub" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Schedule No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="7%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleDesc" runat="server" Font-Underline="True" ForeColor="Blue"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sub Line Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_SubName" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobDesc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="65%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Revision">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_revision" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Work Assignment">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Job Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_jobStatus" runat="server"  ForeColor="#6BB373"></asp:Label>
                                    </ItemTemplate>
                                     <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <%--   <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="cb_all" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_sub" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                <td colspan="3">
                                    <br />
                                    <div id="dvOpen" runat="server" onclick="openTab('dvhiddraw','Imag1');" style="background-color: cornflowerblue; width: 100%; height: 30px; text-align: left; color: #006699; cursor: pointer; background-color: #164094; color: white;display:block">
                                        <asp:Image ID="Imag1" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                                        <asp:Label ID="lbbid" runat="server" Text="Bid 1"><b>Design Drawing / Typical Drawing</b></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Panel ID="pnl_dvhiddraw" runat="server">
                                         <div id="dvhiddraw" runat="server" style="display: none;">
                                        <br />
                                        <%--<button onclick="openPop('1');return false;" class="btn btn-primary">Upload / Select</button>--%>
                                               <asp:Button ID="bt_upload" runat="server" Text="Upload / Select" class="btn btn-primary"  onclientclick="openPop('1');return false;"/>
                                        <asp:Button ID="btnpreview" runat="server" Text="Preview" class="btn btn-primary" OnClick="btnpreview_Click" />
                                        <br />
                                        <br />
                                        <asp:GridView ID="gvDrawing" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvDrawing_RowDataBound">
                                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                            <Columns>
                                            <%--    <asp:TemplateField HeaderText="">
                                              
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CbChk" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbNo" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lbDocno" runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="กอง">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbGruop" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="แผนก">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDivis" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Document Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDoct" runat="server"></asp:Label>
                                                        <asp:Label ID="lbDoctSH" runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDes" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="File Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbFilename" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sheet No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbSheet" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Revision">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbRevi" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUpdated" runat="server"></asp:Label>
                                                        <asp:Label ID="lbCreby" runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbCrebydate" runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbhidDate" runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUpby" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Job No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbBid" runat="server"></asp:Label>
                                                      
                                            <asp:Label ID="lbRevbiddr" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbhidSch" runat="server" Visible="false"></asp:Label>
                                   
                                                        <%--   <asp:ListBox ID="Multiselect_Bid" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>

                                                        <%--  <select id="Multiselect_Bid" multiple="multiple">
                                    <option value="cheese">Cheese</option>
                                    <option value="tomatoes">Tomatoes</option>
                                    <option value="mozarella">Mozzarella</option>
                                    <option value="mushrooms">Mushrooms</option>
                                    <option value="pepperoni">Pepperoni</option>
                                    <option value="onions">Onions</option>
                                    </select>--%>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="250px" />
                                                </asp:TemplateField>
                                                <%--              <asp:TemplateField HeaderText="Schdule No.">
                                    <ItemTemplate>
                                      <asp:Label ID="lbScheno" runat="server" ></asp:Label>
                                    </ItemTemplate>
                             </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Schdule Name">
                                    <ItemTemplate>
                                      
                                    </ItemTemplate>
                             </asp:TemplateField>--%>
                                                        <asp:TemplateField HeaderText="Schdule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSchnamedr" runat="server" Text=""></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ViewDoc">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="link_DrawDoc" runat="server">ViewDoc</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgDel" runat="server" src="../../images/bin.png" Width="20px" Height="20px" OnClick="imgDel_Click" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <%--<asp:TemplateField HeaderText="Job Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_jobStatus" runat="server"  ForeColor="#6BB373"></asp:Label>
                                    </ItemTemplate>
                                     <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>--%>
                                    </div>
                                    </asp:Panel>
                                  
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div id="dveruiH" runat="server" onclick="openTabEqui('dvequihidden');" style="background-color: cornflowerblue; width: 100%; height: 30px; text-align: left; color: #006699; cursor: pointer; background-color: #164094; color: white;display:block">
                                        <asp:Image ID="Image1" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                                        <asp:Label ID="Label21" runat="server" Text="Bid 1"><b>Equipment List</b></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    
                                         <div id="dvequihidden" runat="server" style="display: none;">
                                        <br />
                                        <%--<button class="btn btn-primary" onclick="openPop('6');return false;">Configure Categories</button>--%>
                                        <asp:Button ID="bt_Configure" runat="server" Text="Configure Categories" class="btn btn-primary" onclientclick="openPop('6');return false;"/>
                                        <asp:Button ID="bt_preview" runat="server" Text="Preview" OnClick="bt_preview_Click" class="btn btn-primary" />
                                        <asp:Button ID="bt_price" runat="server" Text="ดึงข้อมูลราคาฐาน" OnClick="bt_price_Click" class="btn btn-primary" />
                                        
                                        <br />
                                        <br />
                                        <div class="container" id="allTabEquip" runat="server" style="width:100%;">
                                            <ul class="nav nav-tabs" id="ultab" runat="server">


                                            </ul>
                                            <div id="dv_gvEquipRela" runat="server" style="position:relative;overflow-x:auto;overflow-y:hidden;height:890px;">
                                            <div id="dv_gvEquipAbs" runat="server" style="position:absolute;">
                                                <div id="container" style="overflow-y: auto;">
    </div>
                                             <asp:GridView ID="gvAllEqui" HeaderStyle-CssClass="GVFixedHeader" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAllEqui_RowDataBound">
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Meterial Group">
                                            <ItemTemplate>
                                               <%-- <asp:ListBox ID="ddl_meterialGrp" runat="server"></asp:ListBox>--%>
                                                 <asp:Label ID="lb_meterialGrp" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_codeNo" runat="server"></asp:Label>
                                                <asp:Label ID="lb_legend" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item No.">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_itmNo" runat="server" Width="60px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_itmNo" runat="server"></asp:Label>
                                                
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Description">
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_fDesc2" runat="server" Visible="false"></asp:Label>
                                                <asp:Label ID="lb_fDesc" runat="server"></asp:Label>
                                                 <asp:Label ID="lb_hide" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_jobno" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_scheno" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_bidno" runat="server" Text="Label" Visible="False"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Additional Description">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_addDesc" runat="server"></asp:TextBox>--%>
                                                <asp:Label ID="lb_addDesc" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField HeaderText="Standard Drawing/Ref No.">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_stdDr" runat="server"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Required QTY">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_reqQTY" runat="server" Width="25px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_reqQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                              <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_unit" runat="server"></asp:Label>
                                               
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reserved QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_revQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Confirmed Reservation QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="conrevQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bid QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_bidQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Stock Availability">
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lnk_res" runat="server" OnClick="lnk_res_Click">Reserve</asp:LinkButton>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Incoterms">
                                            <ItemTemplate>
                                                <%--<asp:ListBox ID="ddl_inco" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                                                <asp:Label ID="lb_inco" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Waive Test">
                                            <ItemTemplate>
                                                <%--<asp:ListBox ID="ddl_wa" runat="server"  SelectionMode="Multiple"></asp:ListBox>--%>
                                                 <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Related Document">
                                            <ItemTemplate>
                                                <%-- <asp:LinkButton ID="lnk_doc" runat="server">linkdoc</asp:LinkButton>--%>
                                                 <asp:Label ID="lb_doc" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                <%--<asp:DropDownList ID="ddl_cur" runat="server"></asp:DropDownList>--%>
                                                  <asp:Label ID="lb_cur" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOE. Unit Price">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_supUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_supUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOE. Amount">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_supAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_supAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LSE. Unit Price">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localSUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localSUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LSE. Amount">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localSAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localSAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LTC. Unit Price">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localTUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_localTUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LTC. Amount">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localTAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localTAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall2" />
                                        </asp:TemplateField>
                                        <%--<asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="img_delEQ" runat="server" src="../../images/bin.png" Width="20px" Height="20px" OnClick="img_delEQ_Click" />
                                               
                                            </ItemTemplate>
                                             <ItemStyle  CssClass="Colsmall2" />
                                        </asp:TemplateField>--%>
                                    </Columns>
                                </asp:GridView>
                                          </div>
                                       </div>
                                        <div id="divTabEqui" runat="server">
                                        </div>
                                           <div id="dvPart" runat="server">

                                           </div >
                                            <div style="padding-top:3px">
                                               
                                            <%--<button onclick="openPop('5'); return false;" class="btn btn-primary">Add Part</button>
                                            <button onclick="openPop('2'); return false;" class="btn btn-primary">Add Equipment / Legend</button>--%>
                                                <asp:Button ID="bt_AddPart" runat="server" Text="Add Part" class="btn btn-primary" onclientclick="openPop('5');return false;"/>
                                                <asp:Button ID="bt_AddEquip" runat="server" Text="Add Equipment / Legend" class="btn btn-primary" onclientclick="openPop('2');return false;"/>
                                                <asp:Button ID="bt_Save" runat="server" Text="Save" class="btn btn-primary" OnClick="bt_Save_Click" OnClientClick="chkrmk('1')"/>
                                                
                                                <asp:TextBox ID="tb_val" runat="server" style="display:none"></asp:TextBox>
                                            </div>
                                            
                                          </div>
                                        <asp:GridView ID="gv_equiList" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gv_equiList_RowDataBound">
                                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <HeaderTemplate>
                                                        <asp:CheckBox ID="cb_all" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="cb_sub" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbEqNo" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Material Group">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbMater" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Code No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbCodeno" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Item No.">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbItmno" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Full Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbFulldes" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Additional Description">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbAdddes" runat="server" Rows="3" TextMode="MultiLine" Width="80px"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Standrad Drawing / Ref No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbStand" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Unit">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUnit" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Currency">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbCurrency" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Required QTY">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbReq" runat="server" Width="50px"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Reserved QTY">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbRes" runat="server" Width="50px"></asp:TextBox>
                                                        <br />
                                                        <asp:LinkButton ID="linkRes" runat="server" >Stock Reserve</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Confirmed Resevation QTY">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbConQ" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Bid QTY">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbBidQ" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <%--    <asp:TemplateField HeaderText="Stock Availability">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkRes" runat="server">LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"/>
                             </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbStatusEq" runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Incoterms">
                                                    <ItemTemplate>
                                                        <select id="Multiselect_Inco" multiple="multiple">
                                                            <option value="cheese">Cheese</option>
                                                            <option value="tomatoes">Tomatoes</option>
                                                            <option value="mozarella">Mozzarella</option>
                                                            <option value="mushrooms">Mushrooms</option>
                                                            <option value="pepperoni">Pepperoni</option>
                                                            <option value="onions">Onions</option>
                                                        </select>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table style="width: 100%;">
                                                            <tr>
                                                                <td colspan="2">
                                                                    <asp:Label ID="Label17" runat="server" Text="Waive Test"></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="Label18" runat="server" Text="Factory Test"></asp:Label>
                                                                </td>
                                                                <td>
                                                                    <asp:Label ID="Label19" runat="server" Text="Manual Test"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table style="width: 100%; text-align: center">
                                                            <tr>
                                                                <td>
                                                                    <asp:CheckBox ID="CheckBox2" runat="server" />
                                                                </td>
                                                                <td>
                                                                    <asp:CheckBox ID="CheckBox1" runat="server" />
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>

                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Related Documents">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="link_viewDoc" runat="server">ViewDoc</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Est. Price">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="tbPrice" runat="server" Width="70px"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table style="width: 100%; text-align: center;">
                                                            <tr style="text-align: center; border-bottom: solid; border-width: thin">
                                                                <td colspan="4">Subply of Equiment
                                               
                                                                </td>

                                                            </tr>
                                                            <tr style="text-align: center; border-bottom: solid; border-width: thin">
                                                                <td colspan="2" style="border-right: solid; border-width: thin">Foreign Supply
                                               
                                                                </td>

                                                                <td colspan="2">Local Supply
                                               
                                                                </td>

                                                            </tr>
                                                            <tr style="border-bottom: solid; border-width: thin">
                                                                <td colspan="2" style="border-right: solid; border-width: thin">CIF Thai Port
                                               
                                                                </td>

                                                                <td colspan="2">Ex-Works Price
                                                                    <br />
                                                                    ( excluding VAT )<br />
                                                                    Bath
                                                                </td>

                                                            </tr>
                                                            <tr>
                                                                <td style="border-right: solid; border-width: thin; width: 25%; text-align: center">Unit
                                               
                                                                </td>
                                                                <td style="border-right: solid; border-width: thin; width: 25%; text-align: center">Amount
                                               
                                                                </td>
                                                                <td style="border-right: solid; border-width: thin; width: 25%; text-align: center">Unit Price
                                               
                                                                </td>
                                                                <td style="width: 25%; text-align: center">Amount
                                                
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table style="width: 100%; text-align: center">
                                                            <tr>
                                                                <td style="width: 25%; border-right: solid; border-width: thin; border-color: #CCCCCC">
                                                                    <asp:Label ID="Label15" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%; border-right: solid; border-width: thin; border-color: #CCCCCC">
                                                                    <asp:Label ID="Label16" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%; border-right: solid; border-width: thin; border-color: #CCCCCC">
                                                                    <asp:Label ID="Label17" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%;">
                                                                    <asp:Label ID="Label18" runat="server"></asp:Label>
                                                                </td>

                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderTemplate>
                                                        <table style="width: 100%; text-align: center;">
                                                            <tr>
                                                                <td colspan="2" style="padding-bottom: 3%">Local Transporation,<br />
                                                                    Contraction and
                                                                    <br />
                                                                    Installation
                                                                    <br />
                                                                    ( excluding VAT )
                                                                    <br />
                                                                    Bath
                                                                </td>
                                                            </tr>
                                                            <tr style="border-top: solid; border-width: thin">
                                                                <td style="width: 50%; border-right: solid; border-width: thin">Unit Price

                                                                </td>
                                                                <td style="width: 50%">Amount

                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <table style="width: 100%; text-align: center">
                                                            <tr>
                                                                <td style="width: 25%; border-right: solid; border-width: thin; border-color: #CCCCCC">
                                                                    <asp:Label ID="Label19" runat="server"></asp:Label>
                                                                </td>
                                                                <td style="width: 25%;">
                                                                    <asp:Label ID="Label20" runat="server"></asp:Label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="ImageButton3" runat="server" src="../../images/edit.png" Width="20px" Height="20px" />
                                                        <asp:ImageButton ID="ImageButton4" runat="server" src="../../images/bin.png" Width="20px" Height="20px" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="80px" HorizontalAlign="Center" />

                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>


                                        <%--   <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="cb_all" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_sub" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            
                                    </div>
                                   
                                   
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <div id="dvDocH" runat="server" onclick="openTab('dvhidDoc','Image2');" style="background-color: cornflowerblue; width: 100%; height: 30px; text-align: left; color: #006699; cursor: pointer; background-color: #164094; color: white;display:block">
                                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                                        <asp:Label ID="Label23" runat="server" Text="Bid 1"><b>Documents</b></asp:Label>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:Panel ID="pnl_dvhidDoc" runat="server">
                                         <div id="dvhidDoc" runat="server" style="display: none;">
                                        <br />
                                       <%-- <button onclick="openPop('3');return false;" class="btn btn-primary">Upload / Selectton</button>--%>
                                             <asp:Button ID="bt_Docup" runat="server" Text="Upload / Selection" class="btn btn-primary" onclientclick="openPop('3');return false;"/>
                                              <asp:Button ID="bt_refresh" runat="server" Text="Refresh" class="btn btn-primary"  OnClick="bt_refresh_Click"/>  
                                        <br />
                                        <br />

                                        <asp:GridView ID="gvTaskDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvTaskDoc_RowDataBound">
                                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                            <RowStyle ForeColor="#000066" />
                                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                            <Columns>
                                              <%--  <asp:TemplateField HeaderText="">
                                                
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CbTDocChk" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                                <asp:TemplateField HeaderText="No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbTDocNo" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="กอง">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbGroup" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="แผนก">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDivis" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Document Type">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDocCode" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lbDoctSH" runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Description">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbDescrip" runat="server" Text=""></asp:Label>

                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="File Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbFilename" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Sheet No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbSheetno" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Revision">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbRev" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUpdatedate" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lbhidDate3" runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Update by">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbUpby" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Job No.">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbBidno" runat="server" Text=""></asp:Label>
                                                        <asp:Label ID="lbRevbid" runat="server" Visible="false"></asp:Label>
                                                        <asp:Label ID="lbDocno" runat="server" Visible="false"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="250px" />
                                                </asp:TemplateField>
                                               
                                                <asp:TemplateField HeaderText="Schdule Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lbSchname" runat="server" Text=""></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="ViewDoc">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="link_DrawDoc" runat="server">ViewDoc</asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="imgDocdel" runat="server" src="../../images/bin.png" Width="20px" Height="20px" OnClick="imgDocdel_Click" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                        <%--    <asp:TemplateField HeaderText="">
                                              
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CbChk" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                    </div>
                                    </asp:Panel>
                                

                                </td>
                            </tr>
                              <tr>
                    <td colspan="3" style="text-align: right;">
                        <br />
                        <asp:Button ID="btnSubtask" runat="server" Text="Submit" class="btn btn-primary" OnClick="btnSubtask_Click" />

                    </td>
                </tr>
                        </table>
                    </td>
                </tr>

            </table>
        </div>
        
        <div class="overlay" runat="server" id="OverlayPopup" style="display:none">
        <div class="popup" runat="server" id="confPopup" >
  <p>Do you want to delete a part</p>
  <div class="text-right">
    <button class="btn btn-cancel" onclick="closePop();return false;">Cancel</button>
    <%--<button class="btn btn-primary">Ok</button>--%>
      <asp:Button ID="bt_confdelPart" runat="server" Text="OK"  class="btn btn-primary" OnClick="bt_confdelPart_Click"/>
  </div>
</div>
            </div>
        <div id="myModal"  runat="server" class="modal">  
            <div class="modal-content">
                <%--<span class="close" onclick="closePopup()">&times;</span>--%>

                <div id="dvViewBom" runat="server" style="display:none;width:100%">
                     <table align="center" style="width:95%;">
                        <tr>
                            <td  colspan="4">
                                <asp:GridView ID="gv_viewBOM" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found."  OnRowDataBound="gv_viewBOM_RowDataBound" >
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbItemno" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="กอง">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_depm" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="แผนก">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_division" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Material Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_mertGrp" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiCode" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Legend">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_legend" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Part">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_Part" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_fullDesc" runat="server" Text=""></asp:Label>
                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_shortDesc" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiType" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiGrp" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:Label ID="lbUnit" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>  
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                         <tr>
                             <td colspan="4" style="text-align:right">
                                  <asp:Button ID="bt_cancView" runat="server" Text="Cencel" class="btn btn-danger" OnClick="bt_cancView_Click"/>
                             </td>
                         </tr>
                     </table>
                </div>
                <div id="dvConfig" runat="server" style="display:none;width:100%">
                    <asp:Label ID="Label56" runat="server" style="text-align: left; color: #006699;margin-left:15px"><b>Configure Categories</b></asp:Label>
                     <table align="center" style="width:95%;">
                        <tr>
                            <td  style="width:33%;height:10px;">

                            </td>
                            <td style="width:33%;">

                            </td>
                            <td style="width:33%;">

                            </td>
                        </tr>
                         <tr>
                             <td colspan="3">
                                 <div>
                                     <asp:GridView ID="gv_config" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gv_config_RowDataBound">
                                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="Category">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lb_cateName" runat="server" ></asp:Label>
                                                                <asp:Label ID="lb_cateCode" runat="server" Visible="False"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle  HorizontalAlign="Left" VerticalAlign="Top" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="% Reservation">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="tb_reser" runat="server" placeholder="Default as 0"></asp:TextBox>
                                                                 <asp:Label ID="lb_persen" runat="server" Text="%"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle  HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Round up OR Not Round up">
                                                            <ItemTemplate>
                                                                <asp:DropDownList ID="ddl_round" runat="server"></asp:DropDownList>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                 </div>
                             </td>
                         </tr>
                         
                         <tr>
                             <td colspan="3" style="text-align:right">
                                 <asp:Button ID="bt_saveConfig" runat="server" Text="Save" class="btn btn-primary" OnClick="bt_saveConfig_Click"/>
                                 <asp:Button ID="bt_cancConfig" runat="server" Text="Cencel" class="btn btn-danger" OnClick="bt_cancConfig_Click"/>
                             </td>
                         </tr>
                    </table>
                </div>
                <div id="dvAddDocument" runat="server" style="width: 100%;">
                    <div id="TabAllDoc" class="container" style="width: 100%;">
                        <ul class="nav nav-tabs">
                            <li id="liUpDoc" runat="server" class="active">
                                <a href="#TabUploadDoc" data-toggle="tab">Upload</a>
                            </li>
                            <li id="liSelDoc" runat="server"><a href="#TabSelectDoc" data-toggle="tab">Select From Database</a>
                            </li>
                        </ul>

                        <div class="tab-content ">
                           
                            <br />
                            <table align="center" style="width: 95%; margin-left: 2.5%;">
                                <tr>
                                    <td colspan="2" style="text-align: left; color: #006699;">
                                        <asp:Label ID="Label10" runat="server"><b>Documents</b></asp:Label>
                                    </td>

                                </tr>
                                <tr style="height: 10px">
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style3">
                                        <asp:Label ID="Label24" runat="server" Text="Bid No:"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtDocjobs" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                        &nbsp;&nbsp;&nbsp;<asp:Label ID="Label33" runat="server" Text="Rev:"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;
                                    <asp:TextBox ID="txtDocRevs" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                    </td>
                                    <%--   <asp:ListBox ID="Multiselect_Bid" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                                </tr>
                                <tr style="height: 10px">
                                    <td></td>
                                </tr>


                            </table>
                            <div class="tab-pane active" id="TabUploadDoc" runat="server">

                                <table align="center" style="width: 95%; margin-left: 2.5%;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label35" runat="server" Text="Job No.:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;<asp:ListBox ID="Multiselect_DocJob" runat="server" SelectionMode="Single" OnSelectedIndexChanged="Multiselect_DocJob_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>

                                        </td>
                                        <td>
                                            <asp:Label ID="Label36" runat="server" Text="Schedule Name:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;
                                    <asp:ListBox ID="Multiselect_DocSch" runat="server" SelectionMode="Single"></asp:ListBox>

                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label38" runat="server" Text="Document Type:"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DDLUpallDoc" runat="server" Width="250px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="Label37" runat="server" Text="" Style="text-align: left; color: #006699;"><b>Upload:</b></asp:Label>
                                            &nbsp;
                   
                            <asp:FileUpload ID="FileUpload2" runat="server" Style="display: inline-flex;" onchange="GetFileNameDoc(this);"></asp:FileUpload>
                                            <%--  <select id="Multiselect_Bid" multiple="multiple">
                                    <option value="cheese">Cheese</option>
                                    <option value="tomatoes">Tomatoes</option>
                                    <option value="mozarella">Mozzarella</option>
                                    <option value="mushrooms">Mushrooms</option>
                                    <option value="pepperoni">Pepperoni</option>
                                    <option value="onions">Onions</option>
                                    </select>--%>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label65" runat="server" Width="72px" Text="File name:" Style="text-align: left;"></asp:Label>
                                            &nbsp;&nbsp;<asp:TextBox ID="tb_filenameDoc" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>

                                        </td>
                                    </tr>
                                     <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label42" runat="server" Text="Description:" Style="text-align: left;"></asp:Label>
                                            &nbsp;&nbsp;<asp:TextBox ID="txtDescript" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>

                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Label ID="Label66" runat="server" Width="72px" Text="Sheet No.:" Style="text-align: left;"></asp:Label>
                                            &nbsp;&nbsp;<asp:TextBox ID="tb_sheetnoDoc" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px" placeholder="00/00"></asp:TextBox>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right;">
                                            <asp:Button ID="btnUploadFile" runat="server" Text="Upload" class="btn btn-primary" OnClick="btnUploadFile_Click" />
                                            &nbsp;
                       <asp:Button ID="btnClosealldoc" runat="server" Text="Close" class="btn btn-danger" onclientclick="closeDoc();return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="tab-pane" id="TabSelectDoc" runat="server">

                                <table align="center" style="width: 95%;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label43" runat="server" Text="Job No.:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;<asp:ListBox ID="Multiselect_DocSelectJob" runat="server" SelectionMode="Single" AutoPostBack="true" OnSelectedIndexChanged="Multiselect_DocSelectJob_SelectedIndexChanged"></asp:ListBox>

                                        </td>
                                        <td>
                                            <asp:Label ID="Label44" runat="server" Text="Schedule Name:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;
                                    <asp:ListBox ID="Multiselect_DocSelectSch" runat="server" SelectionMode="Single"></asp:ListBox>

                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label34" runat="server" Text="Document Type:"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DDLAllDoc" runat="server" Width="250px" OnSelectedIndexChanged="DDLAllDoc_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                    </tr>

                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left; color: #006699;"><b>Attached File from OTCS</b>
                                            <br />
                                            <asp:GridView ID="gvAddDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAddDoc_RowDataBound">
                                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <HeaderTemplate>
                                                            <asp:CheckBox ID="CbDAll" runat="server" />
                                                        </HeaderTemplate>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="CbDChk" runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="No.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbDno" runat="server" Text=""></asp:Label>
                                                            <asp:Label ID="lbDCode" runat="server" Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="กอง">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbDGroup" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="แผนก">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbDocDivis" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document Type">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbADoctype" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Document No.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbADno" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbDescription" runat="server" Text=""></asp:Label>
                                                            <asp:Label ID="lbFilename" runat="server" Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Sheet No.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbDSheetno" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Revision">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbADRev" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Update Date">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbADUpdatedate" runat="server" Text=""></asp:Label>
                                                            <asp:Label ID="lbhidADDate" runat="server" Visible="false"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Update by">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbADUpby" runat="server" Text=""></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                            <div align="right">
                                                <br />
                                                <asp:Button ID="btaddDocall" runat="server" Text="Add" class="btn btn-primary" OnClick="btaddDocall_Click" />
                                                &nbsp;
                                            <asp:Button ID="btcancelDocall" runat="server" Text="Close" class="btn btn-danger" onclientclick="closeDoc();return false;" />
                                            </div>

                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
                 <br />
                <div id="dvpopUpload" runat="server" style="width: 100%;">
                    <div id="TabAll" class="container" style="width: 100%;">
                        <ul class="nav nav-tabs">
                            <li id="liUp" runat="server" class="active">
                                <a href="#TabUpload" data-toggle="tab">Upload</a>
                            </li>
                           <%-- <li id="liSel" runat="server"><a href="#TabSelect" data-toggle="tab">Select From Database</a>
                            </li>--%>
                        </ul>

                        <div class="tab-content ">

                            <br />
                            <table align="center" style="width: 95%; margin-left: 2.5%;">
                                <tr>
                                    <td colspan="2" style="text-align: left; color: #006699;">
                                        <asp:Label ID="lbTitle" runat="server"><b>Design Drawing / Typical Drawing</b></asp:Label>
                                    </td>

                                </tr>
                                <tr style="height: 10px">
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style3">
                                        <asp:Label ID="Label5" runat="server" Text="Bid No:"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;<asp:TextBox ID="tbPopJob" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                        &nbsp;&nbsp;&nbsp;<asp:Label ID="Label13" runat="server" Text="Rev:"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="tbPopRev" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="height: 10px">
                                    <td></td>
                                </tr>

                            </table>
                            <div class="tab-pane active" id="TabUpload" runat="server">

                                <table align="center" style="width: 95%; margin-left: 2.5%;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label11" runat="server" Text="Job No.:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;<asp:ListBox ID="Multiselect_Job" runat="server" SelectionMode="Single" OnSelectedIndexChanged="Multiselect_Job_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label122" runat="server" Text="Schedule Name:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;<asp:ListBox ID="Multiselect_Sch" runat="server" SelectionMode="Single"></asp:ListBox>

                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label25" runat="server" Text="Document Type:"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DDLUpload" runat="server" Width="250px"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            <asp:Label ID="Label6" runat="server" Text="" Style="text-align: left; color: #006699;"><b>Upload:</b></asp:Label>
                                            &nbsp;
                   
          <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-flex;" onchange="GetFileName(this);"></asp:FileUpload>
                                            <%--              <asp:TemplateField HeaderText="Schdule No.">
                                    <ItemTemplate>
                                      <asp:Label ID="lbScheno" runat="server" ></asp:Label>
                                    </ItemTemplate>
                             </asp:TemplateField>
                                  <asp:TemplateField HeaderText="Schdule Name">
                                    <ItemTemplate>
                                      
                                    </ItemTemplate>
                             </asp:TemplateField>--%>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Label ID="Label61" runat="server" Width="72px" Text="File name:" Style="text-align: left;"></asp:Label>
                                            &nbsp;&nbsp;<asp:TextBox ID="tb_filename" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>

                                        </td>
                                    </tr>
                                     <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <asp:Label ID="lblabel" runat="server" Text="Description:" Style="text-align: left;"></asp:Label>
                                            &nbsp;&nbsp;<asp:TextBox ID="txtDescripttion" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>

                                        </td>
                                    </tr>
                                     <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                     <tr>
                                        <td>
                                            <asp:Label ID="Label63" runat="server" Width="72px" Text="Sheet No.:" Style="text-align: left;"></asp:Label>
                                            &nbsp;&nbsp;<asp:TextBox ID="tb_sheetno" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px" placeholder="00/00"></asp:TextBox>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: right;">
                                            <asp:Button ID="btnSaveDr" runat="server" Text="Upload" class="btn btn-primary" OnClick="btnSaveDr_Click" />
                                            &nbsp;
                                <asp:Button ID="btnCloseDr" runat="server" Text="Close" class="btn btn-danger" onclientclick="closeDr();return false;" />
                                        </td>
                                    </tr>
                                </table>
                            </div>

                            <div class="tab-pane" id="TabSelect" runat="server">

                                <table align="center" style="width: 95%;">
                                    <tr>
                                        <td>
                                            <asp:Label ID="Label40" runat="server" Text="Job No.:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;<asp:ListBox ID="Multiselect_DDJob" runat="server" SelectionMode="Single" AutoPostBack="true" OnSelectedIndexChanged="Multiselect_DDJob_SelectedIndexChanged"></asp:ListBox>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label41" runat="server" Text="Schedule Name:"></asp:Label>
                                            &nbsp; &nbsp; &nbsp;<asp:ListBox ID="Multiselect_DDSch" runat="server" SelectionMode="Single"></asp:ListBox>

                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <asp:Label ID="Label7" runat="server" Text="Document Type:"></asp:Label>
                                            &nbsp;&nbsp;&nbsp;<asp:DropDownList ID="DDLDoctype" runat="server" Width="250px" OnSelectedIndexChanged="DDLDoctype_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="height: 10px">
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2" style="text-align: left; color: #006699;"><b>Attached File from OTCS</b>
                                            <br />
                                            <div id="dvAtt" runat="server" style="width: 100%;">
                                                <asp:GridView ID="gvAttFile" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAttFile_RowDataBound">
                                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                                    <Columns>
                                                        <asp:TemplateField HeaderText="">
                                                            <HeaderTemplate>
                                                                <asp:CheckBox ID="CbAttAll" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:CheckBox ID="CbAttChk" runat="server" />
                                                            </ItemTemplate>
                                                            <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="No.">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbAttno" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lbDocCode" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lbRowid" runat="server" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle Width="30px" HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="กอง">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbGroup" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="แผนก">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbDivis" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Drawing No.">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbDrawno" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Description">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbDescrip" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="File Name">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbFilename" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Sheet No.">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbSheetno" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Revision">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbRev" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Update Date">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbUpdatedate" runat="server" Text=""></asp:Label>
                                                                <asp:Label ID="lbhidDate2" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lbCreateby" runat="server" Visible="false"></asp:Label>
                                                                <asp:Label ID="lbCreatedate" runat="server" Visible="false"></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Update by">
                                                            <ItemTemplate>
                                                                <asp:Label ID="lbUpby" runat="server" Text=""></asp:Label>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Center" />
                                                        </asp:TemplateField>

                                                    </Columns>
                                                </asp:GridView>
                                                <div align="right">
                                                    <br />
                                                    <asp:Button ID="btAttsave" runat="server" Text="Add" class="btn btn-primary" OnClick="btAttsave_Click" />
                                                    &nbsp;
                       <asp:Button ID="btAttcencel" runat="server" Text="Close" class="btn btn-danger" OnClick="btAttcencel_Click" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </div>

                        </div>
                    </div>

                </div>
                <div id="dv_viewLegend" runat="server" style="width: 100%;display:none">
                    <table style="width:100%">
                        <tr>
                            <td style="width:15%">
                                <asp:Label ID="Label2" runat="server" Text="Equipment Code: "></asp:Label>
                            </td>
                            <td style="width:85%">
                                <asp:Label ID="lblEquipCode" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label29" runat="server" Text="Unit: "></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEquipUnit" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Label31" runat="server" Text="Equipment Description: "></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblEquipDesc" runat="server" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>

                            </td>
                            <td style="text-align:right">
                                <asp:Button ID="bt_close" runat="server" Text="Close" class="btn btn-danger" OnClick="bt_close_Click"/>

                            </td>
                        </tr>
                    </table>
                </div>
                <div id="dv_addEqui" runat="server" style="width: 100%;">
                     <div id="TabAllEq" class="container" style="width: 100%;">
                        <ul class="nav nav-tabs">
                            <li id="li1" runat="server" class="active">
                                <a href="#TabEquip" data-toggle="tab">Equipment</a>
                            </li>
                            <li id="li2" runat="server"><a href="#TabLegend" data-toggle="tab">Legend</a>
                            </li>
                        </ul>
                         	<div class="tab-content clearfix">
                                 <div class="tab-pane active" id="TabEquip" runat="server">
         <table align="center" style="width: 95%">
                        <tr>
                            <td style="text-align: left; color: #006699;"><b>Add Equipment</b></td>
                        </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="lb_bidNoeq" runat="server" Text="Bid No:"></asp:Label>
                            </td>
                            <td style="width: 35%">
                                <asp:TextBox ID="tb_bidNoeq" runat="server" Width="100px"></asp:TextBox>
                                <asp:Label ID="lb_rev2" runat="server" Text="Rev.:"></asp:Label>
                                <asp:TextBox ID="tb_rev2" runat="server" Width="50px"></asp:TextBox>
                                 <asp:Label ID="Label57" runat="server" Width="50px" Font-Bold="True"></asp:Label>
                                <asp:Label ID="Label28" runat="server" Text="Part:" Font-Bold="True" ForeColor="Red"></asp:Label>
                                 <asp:Label ID="la_part" runat="server" Font-Bold="True" Text="" ForeColor="Red"></asp:Label>
                            </td>
                            <td>
                                 
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <asp:Label ID="Label45" runat="server" Text="Search Parts"></asp:Label>
                            </td>
                            <td>
                                 <asp:DropDownList ID="ddl_searchPart" runat="server"></asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbsearch" runat="server" Text="Search Type:"></asp:Label>
                            </td>
                            <td colspan="3">
                                <table>
                                    <tr>
                                        <td style="padding: 5px 5px 5px 5px; text-align: left">

                                            <asp:RadioButton ID="rdbEquip" runat="server" Text="Equipment/Service" GroupName="searchtype" onchange="setDisplay_SearchType();" />
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <asp:RadioButton ID="rdbBom" runat="server" Text="BOM" GroupName="searchtype" onchange="setDisplay_SearchType();" />
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <%--<asp:RadioButton ID="rdbMytemp" runat="server" Text="My Template" GroupName="searchtype" onchange="setDisplay_SearchType();" />--%>
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <%--<asp:RadioButton ID="rdbPrejob" runat="server" Text="Previous Bid" GroupName="searchtype" onchange="setDisplay_SearchType();" />--%>
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <asp:RadioButton ID="rdbOther" runat="server" Text="Other" GroupName="searchtype" onchange="setDisplay_SearchType();" />
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <asp:TextBox ID="tbsearchtype" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="450px" ClientIDMode="Static"></asp:TextBox>
                                            <asp:DropDownList ID="ddlSelectBid" ClientIDMode="Static" runat="server"></asp:DropDownList>
                                            <asp:DropDownList ID="ddlSelectBom" ClientIDMode="Static" runat="server" Width="150px"></asp:DropDownList>
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <asp:Button ID="btnSearch" runat="server" Text="Search" class="btn btn-primary" OnClick="btnSearch_Click" />

                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                               
                            </td>
                            <td colspan="3">
                                
                                <asp:TextBox ID="tb_jobNoPart" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="tb_jobrevPart" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="tb_schNoPart" runat="server" Visible="False"></asp:TextBox>
                                <asp:TextBox ID="tb_schNamePart" runat="server" Visible="False"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <%--    <asp:TemplateField HeaderText="Stock Availability">
                                    <ItemTemplate>
                                        <asp:LinkButton ID="linkRes" runat="server">LinkButton</asp:LinkButton>
                                    </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Center" VerticalAlign="Top"/>
                             </asp:TemplateField>--%>
                            </td>

                        </tr>
                        <tr>
                            <td colspan="3">
                                <asp:GridView ID="gvItem" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvItem_RowDataBound">
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="cb_all" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cb_sub" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbItemno" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="กอง">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_depm" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="แผนก">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_division" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Material Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_mertGrp" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiCode" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Legend">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_legend" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Part">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_Part" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_fullDesc" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Short Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_shortDesc" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiType" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiGrp" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:Label ID="lbUnit" runat="server" Text=""></asp:Label>
                                                <%-- <asp:Label ID="lb_Part" runat="server" Text="" Visible="False"></asp:Label>--%>
                                                 <asp:Label ID="lb_equipBom" runat="server" Text="" Visible="False"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Explan Equipment">
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cb_explan" runat="server" />
                                            </ItemTemplate>
                                              <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                
                                <asp:TextBox ID="tbAddPath" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="25%" style="Visibility:hidden"></asp:TextBox>
                             
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right;">
                                <asp:Button ID="btnAddItem" runat="server" Text="Select" class="btn btn-primary" OnClick="btnAddItem_Click" style="Visibility:hidden" />
                                
                      
                            </td>
                        </tr>

                        <tr>
                            <td>
                                 <asp:Label ID="Label4" runat="server" Text="Breakdown " Visible="False"></asp:Label>
                                <asp:CheckBox ID="cb_brakedown" runat="server" Visible="False" />
                            </td>
                        </tr>
                         <tr>
                            <td colspan="3">
                                <div id="dv_equip" runat="server" style="position:relative;overflow:auto;height:300px;">
                                     <div id="dv_selEquip" runat="server" style="position:absolute;">
                                         <asp:GridView ID="gv_SelEquip" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  OnRowCommand="gv_SelEquip_RowCommand" OnRowDataBound="gv_SelEquip_RowDataBound" OnRowDeleting="gv_SelEquip_RowDeleting">
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lbItemno" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="กอง">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_depm" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="แผนก">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_division" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Material Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_mertGrp" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Equipment Code">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiCode" runat="server" Text=""></asp:Label>
                                                <asp:Label ID="lb_equipBom" runat="server" Visible="False"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Legend">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_legend" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Part">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_Part" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_fullDesc" runat="server" Text=""></asp:Label>
                                                <asp:Label ID="lb_fullDesc2" runat="server" Visible="False"></asp:Label>
                                                 <asp:Label ID="lb_rowid" runat="server" Visible="False"></asp:Label>
                                                 <asp:ImageButton ID="img_view" runat="server" ImageUrl="../../Images/view.png" Width="20px" Height="20px" CommandName="viewdata" CommandArgument='<%# Container.DataItemIndex %>'/>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Item No.">
                                            <ItemTemplate>
                                              <asp:TextBox ID="tb_itmNo" runat="server" Width="80px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Additional Description">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_AddDesc" runat="server" Width="150px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <%--<asp:TemplateField HeaderText="Standard Drawing/Ref No.">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_stdDr" runat="server" Width="150px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Reference Doc.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_stdDr" runat="server" ></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Design Drawing">
                                            <ItemTemplate>
                                                <asp:ListBox ID="ddl_dr" runat="server"  SelectionMode="Multiple"></asp:ListBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                       <%-- <asp:TemplateField HeaderText="Short Description">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_shortDesc" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                         <asp:TemplateField HeaderText="Incoterms">
                                            <ItemTemplate>
                                                <asp:ListBox ID="ddl_inco" runat="server" SelectionMode="Multiple"></asp:ListBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Waive Test">
                                            <ItemTemplate>
                                                <asp:ListBox ID="ddl_wa" runat="server"  SelectionMode="Multiple"></asp:ListBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                 <asp:DropDownList ID="ddl_cur" runat="server"></asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                       <%-- <asp:TemplateField HeaderText="Equipment Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiType" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                       <%-- <asp:TemplateField HeaderText="Equipment Group">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_equiGrp" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:Label ID="lbUnit" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                         <%--<asp:TemplateField HeaderText="Additional Description">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_addDesc" runat="server" TextMode="MultiLine"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>--%>
                                         <asp:TemplateField HeaderText="Required QTY">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_Req" runat="server" Width="50px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOE. Unit Price">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_supUnit" runat="server"  Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOE. Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_supAmou" runat="server"  Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LSE. Unit Price">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_localSUnit" runat="server"  Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LSE. Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_localSAmou" runat="server"  Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LTC. Unit Price">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_localTUnit" runat="server"  Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LTC. Amount">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_localTAmou" runat="server"  Width="90px"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:CommandField ShowDeleteButton="True" ButtonType="Image" DeleteImageUrl="../../images/bin.png" ControlStyle-Width="16px"  ItemStyle-HorizontalAlign="Center"/>
                                    </Columns>
                                </asp:GridView>

                                     </div>
                                    </div>
                                
                            </td>
                        </tr>
                     <tr>
                          <td style="text-align:right" colspan="3">
                                          <asp:Button ID="btn_AddEquip" runat="server" Text="Add" class="btn btn-primary" OnClick="btn_AddEquip_Click"  />
                                &nbsp;
                                           <asp:Button ID="btnCancelItem" runat="server" Text="Close" CssClass="btn btn-danger" OnClick="btnCancelItem_Click"  />
                          </td>
                     </tr>
                    </table>

				                    </div>
                                 <div class="tab-pane " id="TabLegend" runat="server" style="width:100%">
                                      <table align="center" style="width: 95%">
                            <tr style="padding-left: 5px">
                                <td style="width:100%">
                                    <asp:GridView ID="gvLegend" runat="server" AutoGenerateColumns="False" Width="600px"
                            Visible="true" ClientIDMode="Static" OnRowCommand="gvLegend_RowCommand" OnRowDataBound="gvLegend_RowDataBound"
                            class="table table-striped table-bordered table-sm">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Module / Assembly / Legend">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvLegend_txtLegend" runat="server" ></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="QTY">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvLegend_txtQTY" runat="server" ></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View Legend">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvLegend_btnView" ClientIDMode="Static" runat="server" ImageUrl="../../Images/view.png" CommandName="viewdata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="View Legend" Width="30" ImageAlign="NotSet" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvLegend_btnAdd" ClientIDMode="Static" runat="server" ImageUrl="../../Images/add.png" CommandName="adddata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                        <asp:ImageButton ID="gvLegend_btnRemove" ClientIDMode="Static" runat="server" ImageUrl="../../Images/remove.png" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CCFF" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>

                                    </td>
                                </tr>
                                          <tr>
                                              <td style="text-align:right">
                                                  <asp:Button ID="bt_AddLegend" runat="server" Text="Add" OnClick="bt_AddLegend_Click" class="btn btn-primary"/>
                                                  <asp:Button ID="bt_cancLegend" runat="server" Text="Close" class="btn btn-danger" OnClick="bt_cancLegend_Click"/>
                                              </td>
                                          </tr>
                                          
                                    </table>
				                    </div>
                            </div>
                         
                        </div>

                    
                    <div style="height: 20px"></div>
                    <%--  <asp:TemplateField HeaderText="">
                                                
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CbTDocChk" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                    <div style="height: 20px"></div>
                    <%--    <asp:TemplateField HeaderText="">
                                              
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CbChk" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                    <div style="height: 20px"></div>
                   
                </div>
                 <div id="dv_addPart" runat="server" style="width: 100%;">
                      <table align="center" style="width: 95%">
                          <tr>
                            <td style="text-align: left; color: #006699;"><b>Add Part</b></td>
                        </tr>
                          <tr>
                              <td style="height:10px">

                              </td>
                          </tr>
                        <tr>
                            <td style="width: 10%">
                                <asp:Label ID="Label9" runat="server" Text="Bid No:"></asp:Label>
                            </td>
                            <td style="width: 35%">
                                <asp:TextBox ID="tb_bidNoPart" runat="server" Width="100px"></asp:TextBox>
                                <asp:Label ID="Label12" runat="server" Text="Rev.:"></asp:Label>
                                <asp:TextBox ID="tb_revPart" runat="server" Width="50px"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                          <tr>
                              <td style="height:10px">

                              </td>
                          </tr>
                          <tr>
                            <td>
                                <asp:Label ID="Label14" runat="server" Text="Job No.:"></asp:Label>
                            </td>
                            <td>
                                <asp:ListBox ID="ddl_jobPart" runat="server" SelectionMode="Single"></asp:ListBox>
                            </td>
                            <td>
                                <asp:Label ID="Label22" runat="server" Text="Schedule Name:" Width="120px"></asp:Label>
                                <asp:ListBox ID="ddl_schedulePart" runat="server" SelectionMode="Single"></asp:ListBox>
                            </td>
                        </tr>
                          <tr>
                              <td style="height:10px">

                              </td>
                          </tr>
                          <%-- <tr>
                            <td>
                                <asp:Label ID="Label26" runat="server" Text="Part:"></asp:Label>
                                <asp:Label ID="Label8" runat="server" Text="*" ForeColor="Red"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tb_part" runat="server"  Width="80px"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label ID="Label27" runat="server" Text="Part Description:" Width="120px"></asp:Label>
                                 <asp:TextBox ID="tb_partDesc" runat="server"  Width="550px"></asp:TextBox>
                                
                            </td>
                        </tr>--%>
                          <tr>
                              <td colspan="2">
                                  <div style="height:300px;overflow-y:auto">

                                  
                                  <asp:GridView ID="gvAllPart" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAllPart_RowDataBound">
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <HeaderTemplate>
                                                <asp:CheckBox ID="cb_all" runat="server" />
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:CheckBox ID="cb_sub" runat="server" />
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Part">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_part" runat="server" Text=""></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Description">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_Desc" runat="server" Width="400px"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Important">
                                            <ItemTemplate>
                                                <asp:TextBox ID="tb_Import" runat="server" Width="400px"></asp:TextBox>
                                            </ItemTemplate>
                                            <ItemStyle Width="50px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                                      </div>
                              </td>
                          </tr>
                          <tr>
                              <td style="height:10px">

                              </td>
                          </tr>
                          <tr>
                            <td colspan="3" style="text-align: right;">
                                <asp:Button ID="bt_savePart" runat="server" Text="Add" class="btn btn-primary" OnClick="bt_savePart_Click"  />
                                &nbsp;
                       <asp:Button ID="bt_cancPart" runat="server" Text="Close" CssClass="btn btn-danger" OnClick="bt_cancPart_Click"  />
                            </td>
                        </tr>
                      </table>
                 </div>
            </div>
        </div>
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidWorkID" runat="server" />
        <asp:HiddenField ID="hidSubWorkID" runat="server" />
        <asp:HiddenField ID="hidTaskID" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidAcID" runat="server" />
        <asp:HiddenField ID="BidNo" runat="server" />
        <asp:HiddenField ID="rev" runat="server" />
        <asp:HiddenField ID="hidNo" runat="server" />
        <asp:HiddenField ID="part" runat="server" />
        <asp:HiddenField ID="hidJobNo" runat="server" />
         <asp:HiddenField ID="hidJobRev" runat="server" />
        <asp:HiddenField ID="hidScheName" runat="server" />
        <asp:HiddenField ID="hidScheNo" runat="server" />
         <asp:HiddenField ID="hidSelPart" runat="server" />
          <asp:HiddenField ID="hidDelPart" runat="server" />
          <asp:HiddenField ID="hidDelJobNo" runat="server" />
         <asp:HiddenField ID="hidDelBidNo" runat="server" />
          <asp:HiddenField ID="hidDelSchNo" runat="server" />
        <asp:HiddenField ID="hidChkTab" runat="server" />
         <asp:HiddenField ID="hidNoLG" runat="server" />
        <asp:HiddenField ID="partLG" runat="server" />
        <asp:HiddenField ID="hidNoEQ" runat="server" />
        <asp:HiddenField ID="partEQ" runat="server" />
        <asp:HiddenField ID="hidSearchPart" runat="server" />
         <asp:HiddenField ID="hidchkassign" runat="server" />
        <asp:HiddenField ID="hidPID" runat="server" />
         <asp:HiddenField ID="hidPartTemp" runat="server" />
        <asp:TextBox ID="tb_remarkprt" runat="server" Visible="False" Width="300px"></asp:TextBox>
        <asp:TextBox ID="tb_descprt" runat="server" Visible="False" Width="300px"></asp:TextBox>
         <asp:TextBox ID="tb_import" runat="server" Visible="False" Width="300px"></asp:TextBox>
    </form>
</body>
</html>
