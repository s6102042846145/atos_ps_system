﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidSchedule.aspx.cs" Inherits="PS_System.form.BidSchedule" ValidateRequest="false" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Schedule</title>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .stynone {
            display:none;
        }
    </style>
    <script type="text/javascript">
        function numeric(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && ((charCode >= 48 && charCode <= 57) || charCode == 46))
                return true;
            else {
                alert('Please Enter Numeric values.');
                return false;
            }
        }
        function calcCost(objInput) {
            alert(objInput.id);
        }

        $(function () {

        });

        function sumxTotal(id, lc, fe, con) {

            var text1 = document.getElementById(fe);
            var text2 = document.getElementById(lc);
            var text3 = document.getElementById(con);
            var txtTotal = document.getElementById(id);
            var sum = 0;
            sum = parseFloat(text1.value == "" ? 0 : text1.value) + parseFloat(text2.value == "" ? 0 : text2.value) + parseFloat(text3.value == "" ? 0 : text3.value);
            txtTotal.value = sum.toFixed(2);
        }
  
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close" onclick="closePopup()">&times;</span>
                <p class="cls_model_desc"></p>
            </div>
        </div>
        <div id="myModal_Bid" class="modal" runat="server">
            <!-- Modal content -->
            <div class="modal-content">
                <table style="width: 100%;">
                    <tr>
                        <td style="width: 100px; height: 34px; text-align: right;">
                            <asp:Label ID="Label3" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Search :"></asp:Label>
                        </td>
                        <td style="width: 350px; height: 34px; text-align: left;">&nbsp;<asp:TextBox ID="txtSearchBidno" onkeypress="return disableKeyEnter(event)" runat="server" BorderColor="Silver" BorderStyle="Solid"
                            BorderWidth="1px" Font-Names="Tahoma" Font-Size="10pt" Width="249px"></asp:TextBox>
                            &nbsp;
                            <asp:Button ID="btnSearch" ClientIDMode="Static" runat="server" CssClass="w3-blue" BorderWidth="1px" Height="30px" Width="70px"
                                Font-Names="tahoma" Font-Size="10pt" Text="Search" OnClick="btnSearch_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="width: 1000px; height: 300px;">
                            <asp:GridView ID="gvBid" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" EmptyDataText="No data found." ForeColor="#333333" GridLines="Vertical"
                                OnPageIndexChanging="gvBid_PageIndexChanging"
                                PageSize="10" ShowHeaderWhenEmpty="true" Width="100%">
                                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="White" />
                                <EditRowStyle BackColor="#999999" />
                                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" Font-Names="tahoma" Font-Size="10pt" />
                                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" Font-Names="tahoma" Font-Size="10pt" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                                <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast" PageButtonCount="5" />
                                <Columns>
                                    <asp:BoundField DataField="bid_no" HeaderText="Bid No." SortExpression="bid_no"
                                        ItemStyle-Font-Names="tahoma"
                                        ItemStyle-Font-Size="10pt" ItemStyle-Width="20%" ItemStyle-VerticalAlign="Top"
                                        ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="bid_revision" HeaderText="Revision" SortExpression="bid_revision"
                                        ItemStyle-Font-Names="tahoma" ItemStyle-Font-Size="10pt"
                                        ItemStyle-Width="20%" ItemStyle-Wrap="true" HtmlEncode="false"
                                        ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="center" HeaderStyle-Width="20%" />
                                    <asp:BoundField DataField="bid_desc" HeaderText="Bid Description" SortExpression="bid_desc"
                                        ItemStyle-Font-Names="tahoma" ItemStyle-Font-Size="10pt"
                                        ItemStyle-Width="15%" ItemStyle-Wrap="true" HtmlEncode="false"
                                        ItemStyle-VerticalAlign="Top" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="50%" />
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:Label ID="lblDC" runat="server" Text='<%# Bind("completion_date") %>' Visible="false"></asp:Label>
                                            <asp:LinkButton runat="server" OnClick="lnkAction_Click" Text="Select"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnClose_Popup" ClientIDMode="Static" runat="server" CssClass="w3-right w3-red" BorderWidth="1px" Height="30px" Width="70px"
                                Font-Names="tahoma" Font-Size="10pt" Text="Close" OnClick="btnClose_Popup_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-----------------------------------------END MODAL-------------------------------------->
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Schedule"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;">
            <table style="width: 100%">
                <tr>
                    <td>
                        <asp:Label ID="lblBidno" runat="server" Text="Label">Bid Number:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtBidNo" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="lblRev" runat="server" Text="Label">Revision:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="TxtRevision" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/images/search.png" Height="20px" Width="20px" CssClass="w3-right" />
                    </td>
                    <td>
                        <asp:LinkButton ID="btnSearch_Bid" runat="server" Font-Names="tahoma" Font-Size="10pt" OnClick="btnViewBid_Click">Select Bid</asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; vertical-align: top;">
                        <asp:Label ID="Label26" runat="server" Text="Bid Description:"></asp:Label>
                    </td>
                    <td colspan="5">
                        <asp:TextBox ID="TxtBidDesc" runat="server" TextMode="MultiLine" Rows="3" Width="80%" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Delivery/Completion Date:"></asp:Label>
                    </td>
                    <td>
                            <div  id="dvlblCom" runat="server" style="border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;">
                                    <asp:Label ID="lblCom" runat="server" ></asp:Label>
                                      </div>
                    </td>
                </tr>
            <tr>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="P.Q."></asp:Label>
                    </td>
                    <td>
                        <asp:CheckBox ID="cbPQ" runat="server" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: center; vertical-align: middle; width: 100%">
                        <asp:GridView ID="gvSchedule" runat="server" AutoGenerateColumns="False" Width="600px"
                            Visible="true"  OnRowCommand="gvSchedule_RowCommand"
                            class="table table-striped table-bordered table-sm" OnRowDataBound="gvSchedule_RowDataBound">
                            <Columns>
                                <asp:TemplateField HeaderText="Schedule No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Job No.">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="gvSchedule_ddlJobNo" runat="server"></asp:DropDownList>
                                        <asp:HiddenField ID="gvSchedule_hidJobNo_Revision" runat="server" Value='<%# Bind("jobno_revision") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule Name">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvSchedule_txtScheduleName" runat="server" Text='<%# Bind("schedule_name") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="P.Q." Visible="false">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="gvSchedule_chkPQ" runat="server" Checked='<%#Eval("pq").ToString() == "1" ? true : false%>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="F.E.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvSchedule_txtFE" runat="server" onclick="this.select();" onkeyup="if (this.value * 1 != this.value) { this.value = 0 }"  Text='<%# Bind("fe") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="L.C.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvSchedule_txtLC" runat="server" onclick="this.select();" onkeyup="if (this.value * 1 != this.value) { this.value = 0 }"  Text='<%# Bind("lc") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Construction">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvSchedule_txtConstruction" runat="server" onclick="this.select();" onkeyup="if (this.value * 1 != this.value) { this.value = 0 }"   Text='<%# Bind("construction") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Total">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvSchedule_txtTotal" runat="server"   Text='<%# Bind("total") %>' ></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvLegend_btnAdd" ClientIDMode="Static" runat="server" ImageUrl="../../Images/add.png" CommandName="adddata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                        <asp:ImageButton ID="gvLegend_btnRemove" ClientIDMode="Static" runat="server" ImageUrl="../../Images/remove.png" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CCFF" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: left; vertical-align: middle; width: 300px;">
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnClose" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Close" OnClick="btnClose_Click" />
                    </td>
                </tr>
            </table>
        </div>
        <!-----------------------------------------START EDITING GV ------------------------------------>
    </form>
</body>
</html>
