﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GroupingSchedule.aspx.cs" Inherits="PS_System.form.Bid.GroupingSchedule" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
      
            .auto-style1 {
                height: 22px;
            }
       

          .cls_view_doc {
            color: #337ab7;
            cursor: pointer;
            text-decoration: none;
        }

            .cls_view_doc:hover {
                text-decoration: underline;
            }
      
    </style>
    
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

        <script>

                $(document).keypress(
                function (event) {
                    if (event.which == '13') {
                        event.preventDefault();
                    }
                }); 

            $(function () {
                $(".picker").datepicker();
            });
        </script>

</head>
<body>
    <form id="form1" runat="server">
          <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                       <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Grouping Schedule"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
       <div align="center">
            <table style="width: 95%">
                <tr style="padding-left: 5px">
                    <td colspan="2">
                        <table style="width: 100%;text-align:left;">
                            <tr>
                                <td style="text-align: left; color: #006699;" class="auto-style1"><b>Grouping Schedule</b></td>
                            </tr>
                            <tr>
                                <td style="width: 5%">
                                    <asp:Label ID="lb_bidNo" runat="server" Text="Bid No:"></asp:Label>
                                </td>
                                <td style="width: 42%">
                                    <asp:TextBox ID="txt_bidNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="10%"></asp:TextBox>
                                    <asp:Label ID="lb_rev" runat="server" Text="Rev.:" Width="12%" Style="text-align: center"></asp:Label>
                                    <asp:TextBox ID="txt_rev" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="40px"></asp:TextBox>
                                 
                                </td>

                  
                            </tr>
                                <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lb_bisDesc" runat="server" Text="Bid Description:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_desc" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="70%"></asp:TextBox>
                                </td>
                               
                            </tr>
                           <tr style="height: 10px">
                                <td></td>
                            </tr>
                           <tr>
                               
                                <td style="vertical-align:top;">
                               
                                    <asp:Label ID="Label8" runat="server" Text="Plan Start Date:"></asp:Label>
                                </td>
                                <td style="text-align:left;vertical-align:top;">
                                    <asp:TextBox ID="txtStartDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="6%"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:Label ID="Label11" runat="server" Text="Plan End Date:" ></asp:Label>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtEndDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="6%"></asp:TextBox>
                                </td>
                              <%--  <td>
                                    
                                </td>
                                <td style="text-align:left;">
                                 
                                </td>--%>
                            </tr>
                       <tr style="height: 10px">
                                <td></td>
                            </tr>
                              <tr>
                                <td style="vertical-align:top;">
                                    <asp:Label ID="Label10" runat="server" Text="Completion Date:"></asp:Label>
                                </td>
                                <td colspan="1" style="text-align:left;vertical-align:top;">
                                  <div  id="dvlblCom" runat="server" style="border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;">
                                    <asp:Label ID="lblCom" runat="server" ></asp:Label>
                                      </div>
                                </td>
                            </tr>
                                  <tr style="height: 10px">
                                <td></td>
                            </tr>
                           <tr>
                                <td style="vertical-align:top;">
                                    <asp:Label ID="Label9" runat="server" Text="Remark Bid:"></asp:Label>
                                </td>
                                <td style="vertical-align:top;">
                                    <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="70%" Rows="3" TextMode="MultiLine"></asp:TextBox>

                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                          <%--  <tr>
                                <td>
                                    <asp:Label ID="lb_projName" runat="server" Text="Project Name:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_projName" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="650px"></asp:TextBox>

                                </td>
                            </tr>
                                <tr style="height: 10px">
                                <td></td>
                            </tr>--%>
                            <tr>
                                <td style="text-align: left; color: #006699;" colspan="2"><b>Bid - Schedule Summary</b></td>
                            </tr>
                            <tr>
                                <td colspan="3" style="overflow:auto;height:50px">
                                    <asp:GridView ID="gvJobSumary" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvJobSumary_RowDataBound">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                       <%--     <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="cb_all" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_sub" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Schedule No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="7%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleDesc" runat="server" Font-Underline="True" ForeColor="Blue"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobDesc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="45%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Revision">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_revision" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="Work Assignment">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                        <br />
                    </td>
                </tr>
                   <tr>
                       
                    <td style="text-align: left; color: #006699;"><b></b></td>
                </tr>
                <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">

                        <asp:GridView ID="gvEquip" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt"  EmptyDataText="No data found.">
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Selected">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbAssign" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Project">
                                    <ItemTemplate>
                                        <asp:Label ID="lblProject" runat="server" Text='<%# Bind("project_id") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Schedule">
                                    <ItemTemplate>
                                        <asp:Label ID="lblSch" runat="server" Text='<%# Bind("part_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Description [Equipment Group/ Manufacturer]">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDescEq" runat="server" Text='<%# Bind("part_description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Estimated Price">
                                    <ItemTemplate>
                                        <asp:Label ID="lblTotal" runat="server"  Text='<%# Eval("total_price", "{0:0.00}") %>'></asp:Label>  
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" /> 
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:Button ID="btnView" runat="server" Text="View"  class="btn btn-primary" OnClick="btnView_Click"/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align:left;">
                          <asp:Button ID="btnGroup" runat="server" Text="Assing Grouping" width="130px" class="btn btn-primary" OnClick="btnGroup_Click"/>
                    </td>
                </tr>
            </table>
           </div>
        <div id="dvPartAdd" runat="server" style="display:none;">
           <div id="dvNewSch" runat="server" >
                  <table align="left" style="width: 40%;margin-top:10px;margin-bottom:10px;margin-left:20px">
                      <tr>
                          <td style="text-align:left">
                              <asp:Label ID="lblSch" runat="server" Text="Schedule:" Visible="false"></asp:Label>
                              &nbsp;&nbsp;<asp:DropDownList ID="ddlSche_edit" runat="server" OnSelectedIndexChanged="ddlSche_edit_SelectedIndexChanged" AutoPostBack="true" Visible="false"></asp:DropDownList>
                          </td>
                      </tr>
                     <tr>
   <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">

                        <asp:GridView ID="gvEdit" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt"  EmptyDataText="No data found." >
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Selected">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbEdit" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                              <asp:TemplateField HeaderText="Assign Schedule" Visible="true">
                                    <ItemTemplate>
                                     <asp:Label ID="lblEditAssiSch" runat="server" Text='<%# Bind("part_assign") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Assign Item">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditAssiItm" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditEquipC" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center"  />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditDesc" runat="server" Text='<%# Bind("equip_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditQty" runat="server" Text='<%# Bind("equip_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Job No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditJobno" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="100px"/>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Ref. Part">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEditPart" runat="server" Text='<%# Bind("refer_part") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="50px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Substation">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEditSub" runat="server"  Text='<%# Bind("substation") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Equipment Group">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEditEqGroup" runat="server"  Text='<%# Bind("equip_group") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Manufacturer">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEditManu" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                </asp:TemplateField>
                                 
                            </Columns>
                        </asp:GridView>
                    </td>
                     </tr>
                     </table>
               </div>
           <div style="display: table-cell;width: 20%;vertical-align: middle;text-align:center">
               <asp:ImageButton ID="ImgRight" runat="server" src="../../images/arrow-right.png" width="20px" OnClick="ImgRight_Click" />
               <br />
               <asp:ImageButton ID="ImgLeft" runat="server" src="../../images/arrow-left.png" width="20px" OnClick="ImgLeft_Click" />
           </div>
            <%--display: table-cell;--%>
            <div id="dvCreate" runat="server" style=" width: 100%;display:none;">
               <table align="right" style="width: 100%;margin-right:20px;margin-top:5px">
                     <tr>
                          <td style="text-align:left">
                              <asp:Label ID="Label2" runat="server" Text="Schedule:"></asp:Label>
                              &nbsp;&nbsp;<asp:TextBox ID="txtSch_new" runat="server"></asp:TextBox>
                                   &nbsp;&nbsp;     <asp:Label ID="Label4" runat="server" Text="Description:"></asp:Label>
                              &nbsp;&nbsp;<asp:TextBox ID="txtSch_desc" runat="server"></asp:TextBox>
                          </td>
                      </tr>
                     <tr>
   <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">

                        <asp:GridView ID="gvCreate" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt"  EmptyDataText="No data found." >
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Selected">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="cbCreate" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Assign Schedule" Visible="false">
                                    <ItemTemplate>
                                     <asp:Label ID="lblCreateAssiSch" runat="server" Text='<%# Bind("part_assign") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Assign Item"  Visible="false">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateAssiItm" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="New Item No.">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtItmno" runat="server" Width="50px" Text='<%# Bind("item_no_new") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateEquipC" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center"  />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateDesc" runat="server" Text='<%# Bind("equip_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateQty" runat="server" Text='<%# Bind("equip_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Job No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreateJobno" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left"  Width="100px"/>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Ref. Part">
                                    <ItemTemplate>
                                        <asp:Label ID="lblCreatePart" runat="server" Text='<%# Bind("refer_part") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="50px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Substation">
                                    <ItemTemplate>
                                          <asp:Label ID="lblCreateSub" runat="server"  Text='<%# Bind("substation") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Equipment Group">
                                    <ItemTemplate>
                                         <asp:Label ID="lblCreateEqGroup" runat="server"  Text='<%# Bind("equip_group") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Manufacturer">
                                    <ItemTemplate>
                                          <asp:Label ID="lblCreateManu" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="20px"/>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Delivery Date">
                                    <ItemTemplate>
                                         <input type="text" id="dateDelivery" runat="server" class="picker" autocomplete="off" style="width:120px"  value='<%# Bind("delivery_date") %>'/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                 
                            </Columns>
                        </asp:GridView>
                    </td>
                     </tr>
                     </table>

           </div>
            <div id="dvSave" runat="server" style="text-align:right;padding-right:20px;display:none;">
                <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click"/>
            </div>
        </div>
 
          <div id="myModal"  runat="server"  class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                     <tr>
   <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">

                        <asp:GridView ID="gvPopup" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt"  EmptyDataText="No data found." OnRowDataBound="gvPopup_RowDataBound">
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                 <asp:TemplateField HeaderText="Assign Schedule">
                                    <ItemTemplate>
                                     <asp:Label ID="lblAssiSch" runat="server" Text='<%# Bind("part_assign") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Assign Item">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAssiItm" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lblEquipC" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="250px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lblDesc" runat="server" Text='<%# Bind("equip_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label ID="lblQty" runat="server" Text='<%# Bind("equip_qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="60px"/>
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Job No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblJobno" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Ref. Part">
                                    <ItemTemplate>
                                        <asp:Label ID="lblPart" runat="server" Text='<%# Bind("refer_part") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" Width="50px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Substation">
                                    <ItemTemplate>
                                          <asp:Label ID="lblSub" runat="server"  Text='<%# Bind("substation") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Equipment Group">
                                    <ItemTemplate>
                                         <asp:Label ID="lblEqGroup" runat="server"  Text='<%# Bind("equip_group") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Manufacturer">
                                    <ItemTemplate>
                                          <asp:Label ID="lblManu" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                 
                                      <%--  <asp:TemplateField HeaderText="Edit to Schedule">
                                    <ItemTemplate>
                                          <asp:TextBox ID="txtEditSch" runat="server" ></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Edit to Item No.">
                                    <ItemTemplate>
                                          <asp:TextBox ID="txtEditItm" runat="server" ></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Delivery Date">
                                    <ItemTemplate>
                                         <asp:Label ID="lblDeDate" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        <asp:HyperLink ID="lnkView" runat="server"></asp:HyperLink>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" Width="30px"/>
                                </asp:TemplateField>--%>
                            </Columns>
                        </asp:GridView>
                    </td>
                     </tr>
                     <tr>
                         <td style="text-align:right">
                             <asp:Button ID="btnClose" runat="server" Text="Close" OnClick="btnClose_Click" class="btn btn-primary"/>
                         </td>
                     </tr>
                     </table>
                </div>
             </div>
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidRev" runat="server" />
         <asp:HiddenField ID="hidProject" runat="server" />
    </form>
</body>
</html>
