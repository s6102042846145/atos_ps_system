﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidActivityTemplate.aspx.cs" Inherits="PS_System.form.Bid.BidActivityTemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <%--    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>--%>
    <style>
        .table table tbody tr td a,
        .table table tbody tr td span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .table table > tbody > tr > td > span {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .table table > tbody > tr > td:first-child > a,
        .table table > tbody > tr > td:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .table table > tbody > tr > td:last-child > a,
        .table table > tbody > tr > td:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .table table > tbody > tr > td > a:hover,
        .table table > tbody > tr > td > span:hover,
        .table table > tbody > tr > td > a:focus,
        .table table > tbody > tr > td > span:focus {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }

        .btn {
            display: inline-block;
            padding: 4px 8px;
            margin-bottom: 0;
            font-size: 11px;
            font-weight: normal;
            line-height: 1.428571429;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            -o-user-select: none;
            user-select: none;
        }
    </style>
    <script type="text/javascript">  
        $(document).ready(
            function () {
                SearchText();

                $("#ddlEmailTemplate").change(function () {
                    GetEmailContent();

                });

                 $('#tbxWeightFactor').keypress(function (event) {
                    if ((event.which != 46 || $(this).val().indexOf('.') != -1) &&
                        ((event.which < 48 || event.which > 57) &&
                            (event.which != 0 && event.which != 8))) {
                        event.preventDefault();
                    }

                    var text = $(this).val();

                    if ((text.indexOf('.') != -1) &&
                        (text.substring(text.indexOf('.')).length > 2) &&
                        (event.which != 0 && event.which != 8) &&
                        ($(this)[0].selectionStart >= text.length - 2)) {
                        event.preventDefault();
                    }
                });

                $('[id*=lstNextActivity]').multiselect({
                    includeSelectAllOption: true,
                    enableFiltering: true,
                    enableCaseInsensitiveFiltering: true,
                    disableIfEmpty: true,
                    maxHeight: 500,
                    length: 500,
                    numberDisplayed: 1
                });
            });




        function SearchText() {
            $(".autosuggest").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "BidActivityTemplate.aspx/GetTemplateNames",
                        data: "{'prefixText':'" + document.getElementById('tbxTemplateName').value + "'}",
                        dataType: "json",
                        success: function (data) {
                            response(data.d);
                        },
                        error: function (result) {
                            console.log(result.toString());
                        }
                    });
                }
            });
        }

        function CreateClick() {
            $('#lblMode').text('Create');
            $('#hdnLastMode').val('C');
            document.getElementById('divAddEdit').style.display = 'block';
            ClearData();
            $('#tbxTemplateName').attr('disabled', false);
            $('#tbxActivityId').attr('disabled', false);
            GetEmailContent();
        }

        function EditClick() {
            $('#lblMode').text('Edit');
            $('#hdnLastMode').val('E');
            document.getElementById('divAddEdit').style.display = 'block';
            // ClearData();
            $('#tbxTemplateName').attr('readonly', true);
            $('#tbxActivityId').attr('readonly', true);

        }


        function ClearData() {
            $('#tbxTemplateId').val('');
            $('#tbxActivityId').val('');
            $('#tbxActivityName').val('');
            $('#tbxNextActivity').val('');
            $("#lstbEmailDetail").empty();
            //cbxReqDrawingP
            //cbxReqDrawing
            //cbxReqDocP
            //ddlEmaplTemplate
        }

        function GetEmailContent() {
            var selectedVal = $('#ddlEmailTemplate').val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "BidActivityTemplate.aspx/GetEmailContent",
                data: "{'strEmailTemplateId':'" + selectedVal + "'}",
                dataType: "json",
                success: function (data) {
                    //response(data.d);
                    $('#tbxEmailContent').val(data.d)
                },
                error: function (result) {
                    console.log(result.toString());
                }
            });
        }
    </script>

    <style>
        @media screen and (min-width: 900px) {
            .modal-dialog {
                max-width: 900px; /* New width for default modal */
            }
        }
    </style>

    <%--    -------------------------------------------%>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding: 10px">
            <table style="width: 100%;">
                <tr>
                    <td colspan="6" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="text-align: left; font-family: Tahoma; font-size: 11pt"><b>Bid Activity Template</b></td>
                </tr>
                <tr>
                    <td style="width: 10%"><b>Template Name :</b></td>
                    <td style="text-align: left; width: 90%" colspan="5">
                        <asp:DropDownList ID="ddlTemplateList" runat="server"></asp:DropDownList>
                    </td>

                </tr>
                <tr>
                    <td style="width: 10%"><b>Activity Name :</b></td>
                    <td style="text-align: left; width: 90%" colspan="5">
                        <asp:TextBox ID="tbxActivityNameSearch" runat="server" Width="500px"></asp:TextBox>
                </tr>
                <tr>
                    <td colspan="6">
                        <div class="float-left">
                            <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btnSearch_Click" />
                            <button onclick="CreateClick();return false;" class="btn btn-primary">Add Activity</button>
                            <asp:HiddenField ID="hdnDepartment" runat="server" />
                            <asp:HiddenField ID="hdnSection" runat="server" />
                            <asp:HiddenField ID="hdnLastMode" runat="server" />
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-bottom: 15px;">
                        <asp:GridView ID="gvTemplateList" runat="server" CssClass="table table-striped table-bordered table-hover"
                            AutoGenerateColumns="False" OnRowCommand="gvTemplateList_RowCommand" Font-Names="tahoma" Font-Size="10px" PageSize="10"
                            AllowPaging="true" OnPageIndexChanging="gvTemplateList_PageIndexChanging" OnRowDataBound="gvTemplateList_RowDataBound">

                            <Columns>
                                <asp:TemplateField HeaderText="Template Name">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblTemplateName" runat="server" Text='<%# Bind("temp_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Activity ID">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblActivityId" runat="server" Text='<%# Bind("activity_id") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Activity Name">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblActivityName" runat="server" Text='<%# Bind("activity_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Require Equipment">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="gvCbxReqEquip" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("req_equip")) %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="center" VerticalAlign="Top" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Equipment Assignee">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="gvHdnEquipAssignee" runat="server" Value='<%# Bind("assignee_equip") %>' />
                                        <asp:Label ID="gvLblEquipAssignee" runat="server" Text='<%# Bind("assignee_equip") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Require Drawing">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="gvCbxReqDrawing" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("req_drawing")) %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="center" VerticalAlign="Top" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Drawing Assignee">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="gvHdnDrawingAssignee" runat="server" Value='<%# Bind("assignee_drawing") %>' />
                                        <asp:Label ID="gvLblDrawingAssignee" runat="server" Text='<%# Bind("assignee_drawing") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Require Document">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="gvCbxReqDoc" runat="server" Enabled="false" Checked='<%# Convert.ToBoolean(Eval("req_tech_doc")) %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="center" VerticalAlign="Top" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document Assignee">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="gvHdnDocAssignee" runat="server" Value='<%# Bind("assignee_tech_doc") %>' />
                                        <asp:Label ID="gvLblDocAssignee" runat="server" Text='<%# Bind("assignee_tech_doc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Next Activity">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblNextActivity" runat="server" Text='<%# Bind("next_activity_id") %>'></asp:Label>
                                        <asp:HiddenField ID="gvHdnNextActivity" runat="server" Value='<%# Bind("next_activity_id") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="300px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Approval Routing">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblAppRoute" runat="server" Text='<%# Bind("approval_routing") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Email Notify Template">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblEmailNoti" runat="server" Text='<%# Bind("email_noti_name") %>'></asp:Label>
                                        <asp:HiddenField ID="gvHdnEmailNotiId" runat="server" Value='<%# Bind("email_noti_id") %>' />
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Weight">
                                    <ItemTemplate>
                                        <asp:Label ID="gvLblWeightFactor" runat="server" Text='<%# Bind("weight_factor") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="Left" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:ImageButton ID="gvBtnEdit" runat="server" OnClick="editModal_Click" ImageUrl="~/images/edit.png" Height="17px" Width="17px" />
                                        <asp:ImageButton ID="gvImageButton1" runat="server" ImageUrl="~/images/bin.png" Height="17px" Width="17px" CommandName="del" OnClientClick="return confirm('Are you sure you want to delete this Activity ID ?');"></asp:ImageButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="50px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
        <div id="divAddEdit" class="w3-modal">

            <div class="w3-modal-content">
                <div class="w3-container" style="margin: auto auto auto auto">
                    <div>
                        <asp:Label runat="server" ID="lblMode" Font-Bold="true" Font-Size="Larger" Text="edit"></asp:Label>

                    </div>
                    <div>
                        <table style="width: 100%; font-family: Tahoma; font-size: 11px; vertical-align: top; text-align: left">

                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblTmpName" Font-Bold="true" Text="Template Name :"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbxTemplateName" runat="server" CssClass="autosuggest"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblReqEquipment" Font-Bold="true" Text="Required Equipment :"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbxReqEquip" runat="server" Checked='<%# Convert.ToBoolean(Eval("req_equip")) %>' />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblEquipmentAssignee" Font-Bold="true" Text="Equipment Assignee :"></asp:Label>
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="tbxEquipAssignee" runat="server"></asp:TextBox>--%>
                                    <asp:DropDownList ID="ddlEquipAssignee" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                            </tr>
                            <td>
                                <asp:Label runat="server" ID="lblActivityID" Font-Bold="true" Text="Activity ID :"></asp:Label>
                            </td>
                            <td>
                                <asp:TextBox ID="tbxActivityId" runat="server"></asp:TextBox>
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblReqDrawing" Font-Bold="true" Text="Required Drawing :"></asp:Label>
                            </td>
                            <td>
                                <asp:CheckBox ID="cbxReqDrawing" runat="server" Checked='<%# Convert.ToBoolean(Eval("req_drawing")) %>' />
                            </td>
                            <td>
                                <asp:Label runat="server" ID="lblDrawingAssignee" Font-Bold="true" Text="Drawing Assignee :"></asp:Label>
                            </td>
                            <td>
                                <%--<asp:TextBox ID="tbxDrawingAssignee" runat="server"></asp:TextBox>--%>
                                <asp:DropDownList ID="ddlDrawingAssignee" runat="server"></asp:DropDownList>
                            </td>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblActivityName" Font-Bold="true" Text="Activity Name :"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbxActivityName" runat="server"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblReqDocument" Font-Bold="true" Text="Required Document :"></asp:Label>
                                </td>
                                <td>
                                    <asp:CheckBox ID="cbxReqDoc" runat="server" Checked='<%# Convert.ToBoolean(Eval("req_tech_doc")) %>' />
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblDocumentAssignee" Font-Bold="true" Text="Document Assignee :"></asp:Label>
                                </td>
                                <td>
                                    <%--<asp:TextBox ID="tbxDocAssignee" runat="server"></asp:TextBox>--%>
                                    <asp:DropDownList ID="ddlDocAssignee" runat="server"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label runat="server" ID="lblWeightFactor" Font-Bold="true" Text="Weight :"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="tbxWeightFactor" runat="server"></asp:TextBox>
                                </td>
                                 <td>
                                    <asp:Label runat="server" ID="lblSectionApp" Font-Bold="true" Text="Section Approver :"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlSectionApp" runat="server"></asp:DropDownList>
                                </td>
                                <td>
                                    <asp:Label runat="server" ID="lblDepartmentApp" Font-Bold="true" Text="Department Approver:"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlDepartmentApp" runat="server"></asp:DropDownList>
                                </td>
                               
                            </tr>
                            <tr>
                                <td style="vertical-align: top; text-align: left">
                                    <asp:Label runat="server" ID="lblEmailNotiTemplate" Font-Bold="true" Text="Email Template :"></asp:Label>
                                </td>
                                <td style="vertical-align: top; text-align: left">
                                    <asp:DropDownList ID="ddlEmailTemplate" runat="server">
                                    </asp:DropDownList>
                                </td>
                               
                                <td>
                                    <asp:Label runat="server" ID="lblNextActivity" Font-Bold="true" Text="Next Activity :"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <%--<asp:TextBox ID="tbxNextActivity" runat="server"></asp:TextBox>--%>
                                    <asp:ListBox ID="lstNextActivity" runat="server" Class="form-control" SelectionMode="Multiple"></asp:ListBox>
                                </td>
                            </tr>
                            <tr>
                                 
                                <td style="vertical-align: top">
                                    <asp:Label runat="server" ID="lblEmailPreview" Font-Bold="true" Text="Email Preview :"></asp:Label>
                                </td>
                                <td colspan="5">
                                    <asp:TextBox ID="tbxEmailContent" runat="server" Width="100%" TextMode="MultiLine" Rows="5" Enabled="false"></asp:TextBox>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </div>
                    <div style="padding: 10px 10px 10px 10px">
                        <asp:Button ID="btnSave" class="btn btn-primary btn-lg" runat="server" Text="Save" OnClick="btnSaveAct_Click" />
                        <asp:Button ID="btnClose" class="btn btn-primary btn-lg" runat="server" Text="Close" OnClientClick="document.getElementById('id01').style.display='none'" OnClick="btnClose_Click" />
                        <asp:Label ID="lblErrorMsg" runat="server" Text="" ForeColor="Red"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
