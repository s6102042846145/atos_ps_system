﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidDashboard_M.aspx.cs" Inherits="PS_System.form.Bid.BidDashboard_M" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
         .btnclass{
            
            background:white;
            color:#508bcb;
            border:none;
            height:40px;
            width:80px;
            border-radius:5px
        }
        .btnSel{
            
            background:white;
            color:black;
            height:40px;
            width:80px;
            border-left:solid;
            border-top:solid;
            border-right:solid;
            border-bottom:none;
            border-width:thin;
            border-radius:5px 5px 0px 0px;
        }
        .btnclass:hover {
            background: #eeeeee;
        }
        .btnPartSel{
            color:white;
            border-color:#357ebd;
            background-color:#357ebd;
            border:1px solid transparent;
            border-radius:3px;
        }
        .Colsmall {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
             .Colsmall2 {
            width: 80px;
            max-width: 80px;
            min-width: 80px;
           
        }
            #gvAllEqui   th {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <script>

        $(document).ready(function () {
            var width = new Array();
            var table = $("table[id*=gvAllEqui]"); //Pass your gridview id here.
            table.find("th").each(function (i) {
                width[i] = $(this).width();
            });
            headerRow = table.find("tr:first");
            headerRow.find("th").each(function (i) {
                $(this).width(width[i]);
            });
            firstRow = table.find("tr:first").next();
            firstRow.find("td").each(function (i) {
                $(this).width(width[i]);
            });
            var header = table.clone();
            header.empty();
            header.append(headerRow);
            //header.append(firstRow);
            header.css("width", width);
            $("#container").before(header);
            table.find("tr:first td").each(function (i) {
                $(this).width(width[i]);
            });
            $("#container").height(700);
            //$("#container").width(table.width() + 20);

            $("#container").append(table);
        });

        function openTab(chk, ima) {
            var x = document.getElementById(chk);
            var img = document.getElementById(ima);
            if (x.style.display === "none") {
                x.style.display = "block";
                img.src = '../../images/sort_asc.png';
                document.getElementById("hidTab").value = "1";

            } else {
                x.style.display = "none";
                img.src = '../../images/sort_desc.png';
            }
        }

        function changeGrid() {
            var gvall = document.getElementById("<%= gvAllDoc.ClientID %>");
            var gvtak2 = document.getElementById("<%= gvTaskDoc2.ClientID %>");
            if (gvtak2.style.display === "none") {
                gvtak2.style.display = "inline-table";
                gvall.style.display = "none";
                document.getElementById("hidFix").value = "1";
                

            } else {
                gvtak2.style.display = "none";
                gvall.style.display = "inline-table";
                document.getElementById("hidFix").value = "0";
            }
        }
        function clearHidden() {
            document.getElementById("hidTab").value = "";
        }
        function chkfilterEQ() {
            document.getElementById("hidChk").value = "1";
        }

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });


        function chkAll(Checkbox, gv) {
            var TargetControl = document.getElementById(gv);
            var cb = document.getElementById(Checkbox);
            var TargetChildControl = "cboVerify";
            var Inputs = TargetControl.getElementsByTagName("input");

            for (var n = 0; n < Inputs.length; n++) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false) {
                    Inputs[n].checked = cb.checked;
                }
            }
        }
        function chkAllTaskDoc(Checkbox, gv) {
            var TargetControl = document.getElementById(gv);
            var cb = document.getElementById(Checkbox);
            var TargetChildControl = "cbVerify";
            var Inputs = TargetControl.getElementsByTagName("input");

            for (var n = 0; n < Inputs.length; n++) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false) {
                    Inputs[n].checked = cb.checked;
                }
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
          <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                     
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Dashboard"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <table style="width: 95%">
                <tr style="padding-left: 5px">
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left; color: #006699;"><b>Bid Dashboard</b></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">
                                    <asp:Label ID="lb_bidNo" runat="server" Text="Bid No:"></asp:Label>
                                </td>
                                <td style="width: 42%">
                                    <asp:TextBox ID="txt_bidNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                    <asp:Label ID="lb_rev" runat="server" Text="Rev.:" Width="10%" Style="text-align: center"></asp:Label>
                                    <asp:TextBox ID="txt_rev" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="40px"></asp:TextBox>
                                    <asp:Label ID="Label2" runat="server" Width="5%" visibility="hidden"></asp:Label>
                                    <asp:Button ID="Button1" runat="server" Text="Compare PS" class="btn btn-primary" visible="false"/>
                                </td>

                                <td style="width: 45%" rowspan="2">
                                    <table style="width: 95%;">
                                        <tr style="height: 10%;">
                                            <td style="width: 20%; text-align: right; vertical-align: initial;">
                                                <asp:Label ID="Label1" runat="server" Text="Bid Progress:"></asp:Label>
                                            </td>
                                            <td style="width: 80%; text-align: left; vertical-align: middle;">
                                                <div class="progress">
                                                    <div id="ProcessBar" runat="server"  class="progress-bar progress-bar-striped" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 90%;">
                                            <td colspan="2">
                                                <asp:Label ID="lblProgress_Department" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                                <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lb_bisDesc" runat="server" Text="Bid Description:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_desc" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%"></asp:TextBox>
                                </td>
                                <td rowspan="5" style="vertical-align:top;">
                                     <div style="overflow-y:auto;height:250px;">
                                        <asp:GridView ID="gvAttach" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." >
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Attachment Name">
                                    <ItemTemplate>
                                     <asp:LinkButton ID="lnkAtt_view" runat="server" Text='<%# Bind("att_url") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="350px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Attachment Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAtt_date" runat="server" Text='<%# Bind("upload_datetime") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                           <tr style="height: 10px">
                                <td></td>
                            </tr>
                           <tr>
                               
                                <td style="vertical-align:top;">
                               
                                    <asp:Label ID="Label8" runat="server" Text="Plan Start Date:"></asp:Label>
                                </td>
                                <td style="text-align:left;vertical-align:top;">
                                    <asp:TextBox ID="txtStartDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="39%"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label11" runat="server" Text="Plan End Date:"></asp:Label>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtEndDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="40%"></asp:TextBox>
                                </td>
                              <%--  <td>
                                    
                                </td>
                                <td style="text-align:left;">
                                 
                                </td>--%>
                            </tr>
                 
                              <tr>
                                <td style="vertical-align:top;">
                                    <asp:Label ID="Label10" runat="server" Text="Completion Date:"></asp:Label>
                                </td>
                                <td colspan="1" style="text-align:left;vertical-align:top;">
                                  <div  id="dvlblCom" runat="server" style="border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;">
                                    <asp:Label ID="lblCom" runat="server" ></asp:Label>
                                      </div>
                                </td>
                            </tr>
                           <tr>
                                <td style="vertical-align:top;">
                                    <asp:Label ID="Label9" runat="server" Text="Remark Bid:"></asp:Label>
                                </td>
                                <td style="vertical-align:top;">
                                    <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>

                                </td>
                            </tr>
                            <tr >
                                <td>
                                    <asp:Button ID="btnExecl" runat="server" class="btn btn-primary" Text="Excel Equipment" OnClick="btnExecl_Click" />
                                </td>
                            </tr>
                             <tr style="height: 10px">
                                <td></td>
                            </tr>
                          <%--  <tr>
                                <td>
                                    <asp:Label ID="lb_projName" runat="server" Text="Project Name:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_projName" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="650px"></asp:TextBox>

                                </td>
                            </tr>
                                <tr style="height: 10px">
                                <td></td>
                            </tr>--%>
                            <tr>
                                <td style="text-align: left; color: #006699;" colspan="2"><b>Bid - Job Summary</b></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:GridView ID="gvJobSumary" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvJobSumary_RowDataBound">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                       <%--     <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="cb_all" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_sub" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Schedule No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="7%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleDesc" runat="server" Font-Underline="True" ForeColor="Blue"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Sub/Line Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_Subline" runat="server" Text='<%# Bind("sub_line_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobDesc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="45%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Revision">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_revision" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="Work Assignment">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>

           <div id="dvAllTab" runat="server" align="Center" style="width:100%;padding-top:20px">
                      <table style="width: 95%;">
                          <tr>
                              <td >
                                  <div id="dvDrawshow" runat="server" onclick="openTab('dvhiddraw','Imag2');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:none">
                            <asp:Image ID="Imag2" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="Label14" runat="server" Text="Bid 1"><b>Design Drawing / Typical Drawing</b></asp:Label>
                                 </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div id="dvhiddraw" runat="server" style="display: none;">
                                      <asp:GridView ID="gvDrawing" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvDrawing_RowDataBound">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbNo" runat="server" Text=' <%# Container.DataItemIndex + 1 %>'></asp:Label>
                                            <asp:Label ID="lbDocno" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="lbGruop" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDivis" runat="server" Text='<%# Bind("section_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDoct" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                             <asp:Label ID="lbDoctSH" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDes" runat="server" Text='<%# Bind("tr_doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbFilename" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sheet No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSheet" runat="server" Text='<%# Bind("doc_sheetno") %>'></asp:Label>
                                        </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:Label ID="lbRevi" runat="server" Text='<%# Bind("doc_revision") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpdated" runat="server" Text='<%# Eval("tr_updated_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            <asp:Label ID="lbCreby" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbCrebydate" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbhidDate" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update by">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpby" runat="server" Text='<%# Bind("tr_updated_by") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bid No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbBid" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                                                <asp:Label ID="lbRevbiddr" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbhidSch" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px" />
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schdule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSchnamedr" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ViewDoc">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="link_DrawDoc" runat="server">ViewDoc</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                  </div>
                                  <br />
                              </td>
                          </tr>
                       <%--   <tr>
                              <td style="text-align:left;">
                                  <button onclick="document.getElementById('myModal').style.display='block';return false;" class="btn btn-primary" visible="false">Assign to Staff</button>
                              </td>
                          </tr>--%>
                           <tr>
                            <td>
                                 <div id="dvAssign" runat="server" onclick="openTab('dvhidAssign','Image1');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block;">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="Label3" runat="server" Text="Bid 1"><b>Activity, Assignment and Status</b></asp:Label>
                                 </div>
                            </td>
                          </tr>
                          <tr>
                              <td>
                                  <div id="dvhidAssign" runat="server" style="display: none;">
                                           <asp:GridView ID="gvaas" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found."  OnRowCommand="gvaas_RowCommand">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                           <asp:ImageButton ID="ibtnAssigned" CommandName="viewdata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png" />
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top"/>
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="กอง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_division" runat="server"  Text='<%# Bind("depart_position_name") %>'></asp:Label>
                               
                                                                <asp:GridView ID="gvaasSub" runat="server" AutoGenerateColumns="false" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Names="tahoma" Font-Size="9pt"
                                                                   OnRowDataBound="gvaasSub_RowDataBound" Visible="false">
                                                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />

                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Select">
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="cb_sub" runat="server" onclick="chkSelect(this);" Enabled="false"/>

                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="แผนก">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_depm" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_sec" runat="server" ></asp:Label>
                                                                                <asp:Label ID="lb_reqDraw" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_reqEqui" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_reqTechDoc" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_ActID" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_nxtActID" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_apprRout" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_emailNoti" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_emailCont" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_bidNo" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_bidRev" runat="server" Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Activity">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_activity" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Process">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_aas" runat="server"></asp:Label>
                                                                                 <asp:Label ID="lbhidStatus" runat="server" Visible="false"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Design Assigned to:">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddl_da" runat="server" Enabled="false">
                                                                                </asp:DropDownList>
                                                                                    <asp:Label ID="lblReq_dr" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdf_da" runat="server" Value='<%# Bind("assignee_drawing") %>'/>
                                                                            </ItemTemplate>
                                                                              <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_statusDA" runat="server" Text="-"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Equiment /Price Schedule Assigned to:">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddl_epsa" runat="server" Enabled="false">
                                                                                </asp:DropDownList>
                                                                                  <asp:Label ID="lblReq_eq" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdf_epsa" runat="server" Value='<%# Bind("assignee_equip") %>'/>
                                                                            </ItemTemplate>
                                                                                 <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_statusDPSA" runat="server" Text="-"></asp:Label>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Assign to:">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddl_dsa" runat="server" Enabled="false">
                                                                                </asp:DropDownList>
                                                                               <asp:Label ID="lblReq_doc" runat="server" Text="*" ForeColor="Red" visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdf_dsa" runat="server" Value='<%# Bind("assignee_tech_doc") %>'/>
                                                                            </ItemTemplate>
                                                                                 <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_statusDSA" runat="server" Text="-"></asp:Label>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                         <asp:TemplateField HeaderText="Rounting">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_headDept" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_headSect" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_headProject" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_assiDept" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_dept" runat="server"  Visible="False"></asp:Label>
                                                                                <asp:ImageButton ID="imgbt" runat="server" src="../../images/plus.png" width="20px" height="20px" Enabled="false"/>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                        
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                  </div>
                                          <br />
                              </td>
                          </tr>
                          <tr>
                            <td>
                                 <div id="dvOpen" runat="server" onclick="openTab('dvhidEq','Imag1');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block;">
                            <asp:Image ID="Imag1" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="lbbid" runat="server" Text="Bid 1"><b>Equipment List</b></asp:Label>
                                 </div>
                            </td>
                          </tr>
                          <tr>
                              <td>

                                   <div id="dvhidEq" runat="server" style="display: none;">
                                            <asp:Label ID="Label6" runat="server" Text="Part: "></asp:Label>
                                 &nbsp; <asp:DropDownList ID="ddlPart" runat="server" Width="150px"></asp:DropDownList>
                                   &nbsp;&nbsp;<asp:Button ID="btnPart" runat="server" Text="Filter" class="btn btn-primary" OnClientClick="chkfilterEQ();"/>
                                        <div class="container" id="allTabEquip" runat="server" style="width:100%;">
                                            <ul class="nav nav-tabs" id="ultab" runat="server">
                                            </ul>
                                            <div id="dv_gvEquipRela" runat="server" style="position:relative;overflow-x:auto;overflow-y:hidden;height:890px;">
                                            <div id="dv_gvEquipAbs" runat="server" style="position:absolute;">
                                                <div id="container" style="overflow-y: auto;"> </div>
                                                <asp:GridView ID="gvAllEqui" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAllEqui_RowDataBound" >
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Meterial Group" >
                                            <ItemTemplate >
                                               <%-- <asp:ListBox ID="ddl_meterialGrp" runat="server"></asp:ListBox>--%>
                                                 <asp:Label ID="lb_meterialGrp" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle  CssClass="Colsmall" />
                                            
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_codeNo" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item No.">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_itmNo" runat="server" Width="60px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_itmNo" runat="server"></asp:Label>
                                                <asp:Label ID="lb_legend" runat="server"></asp:Label>
                                            </ItemTemplate>
                                                  <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Description">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_fDesc" runat="server"></asp:Label>
                                                 <asp:Label ID="lb_fDesc2" runat="server" Visible="false"></asp:Label>
                                                 <asp:Label ID="lb_hide" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_jobno" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_scheno" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_bidno" runat="server" Text="Label" Visible="False"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Additional Description">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_addDesc" runat="server"></asp:TextBox>--%>
                                                <asp:Label ID="lb_addDesc" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Required QTY">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_reqQTY" runat="server" Width="25px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_reqQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_unit" runat="server"></asp:Label>
                                               
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reserved QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_revQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Confirmed Reservation QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="conrevQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                                  <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bid QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_bidQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Incoterms">
                                            <ItemTemplate>
                                                <%--<asp:ListBox ID="ddl_inco" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                                                <asp:Label ID="lb_inco" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Waive Test">
                                            <ItemTemplate>
                                                <%--<asp:ListBox ID="ddl_wa" runat="server"  SelectionMode="Multiple"></asp:ListBox>--%>
                                                 <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Related Document">
                                            <ItemTemplate>
                                               <%-- <asp:LinkButton ID="lnk_doc" runat="server">linkdoc</asp:LinkButton>--%>
                                                 <asp:Label ID="lb_doc" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                <%--<asp:DropDownList ID="ddl_cur" runat="server"></asp:DropDownList>--%>
                                                  <asp:Label ID="lb_cur" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh1" runat="server" Text="SOE. Unit Price" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_supUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_supUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                            <HeaderTemplate>
                                                  <asp:Label ID="lblh2" runat="server" Text="SOE. Amount" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_supAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_supAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh3" runat="server" Text="LSE. Unit Price" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localSUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localSUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh4" runat="server" Text="LSE. Amount" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localSAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localSAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh5" runat="server" Text="LTC. Unit Price" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localTUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_localTUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh6" runat="server" Text="LTC. Amount" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localTAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localTAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall2"  />
                                        </asp:TemplateField>
                                     
                                    </Columns>
                                </asp:GridView>

                                            </div>
                                                <div id="dvPart" runat="server">

                                           </div>
                                            </div>
                                        </div>
                                   </div>
                                  <br />
                              </td>
                          </tr>
                          <tr>
                              <td>
                                   <div id="dvOpenDoc" runat="server" onclick="openTab('dvhidDoc','Imag3');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block;">
                            <asp:Image ID="Imag3" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="Label31" runat="server" Text="Bid 1"><b>Documents</b></asp:Label>
                                    </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div id="dvhidDoc" runat="server" style="display: none">
                                      <asp:Label ID="Label4" runat="server" Text="Department: "></asp:Label>
                                       &nbsp;<asp:DropDownList ID="ddlDept" runat="server" Width="150px"></asp:DropDownList>
                                      &nbsp;&nbsp;   <asp:Label ID="Label5" runat="server" Text="Document Type: "></asp:Label>
                                     &nbsp; <asp:DropDownList ID="ddlDoctype" runat="server" Width="150px"></asp:DropDownList>
                                     &nbsp;&nbsp; <asp:Button ID="btnFilter" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFilter_Click" OnClientClick="clearHidden();"/>
                                       <asp:Button ID="btnChange" runat="server" Text="Change View" class="btn btn-primary" align="right" OnClientClick="changeGrid(); return false;"/>
                                      <asp:Button ID="btnVerify" runat="server" Text="Verify Document" class="btn btn-primary" style="position:sticky;left:92%" OnClick="btnVerify_Click" /> 
                                      <br />
                                       <asp:GridView ID="gvAllDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." OnRowDataBound="gvAllDoc_RowDataBound" style="display:inline-table">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
      		                <asp:TemplateField HeaderText="Document Type">
                                        <ItemTemplate>
                                              <asp:Label ID="lbDocumentype" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                             <asp:Label ID="lbDocumentcode" runat="server" Text='<%# Bind("doctype_code") %>' Visible="false"></asp:Label>
                                            	      </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" Width="15%" VerticalAlign="Top"/>
                                    </asp:TemplateField>
                                    	 <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                            <asp:GridView ID="gvTaskDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." OnRowDataBound="gvTaskDoc_RowDataBound">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbTDocNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="lbGroup" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDivis" runat="server"  Text='<%# Bind("section_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Type" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDocCode" runat="server"  Text='<%# Bind("doctype_name") %>'></asp:Label>
                                            <asp:Label ID="lbCode" runat="server"  Text='<%# Bind("doctype_code") %>'></asp:Label>
                                            <asp:Label ID="lbDoctSH" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDescrip" runat="server"  Text='<%# Bind("tr_doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbFilename" runat="server"  Text='<%# Bind("doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sheet No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSheetno" runat="server"  Text='<%# Bind("doc_sheetno") %>'></asp:Label>
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:Label ID="lbRev" runat="server"  Text='<%# Bind("doc_revision") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpdatedate" runat="server"  Text='<%# Eval("tr_updated_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            <asp:Label ID="lbhidDate3" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update by">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpby" runat="server"  Text='<%# Bind("tr_updated_by") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbBidno" runat="server"  Text='<%# Bind("job_no") %>'></asp:Label>
                                            <asp:Label ID="lbRevbid" runat="server"  Text='<%# Bind("job_revision") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lbDocno" runat="server"  Text='<%# Bind("doc_no") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schdule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSchname" runat="server"  Text='<%# Bind("schedule_name") %>'></asp:Label>
                                            <asp:Label ID="lbSchnohid" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ViewDoc">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="link_DrawDoc" runat="server" >ViewDoc</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Verify By">
                                               <HeaderTemplate>
                                                <asp:CheckBox ID="cbAllverify" runat="server" />
                                               </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cbVerify" runat="server" />  <br />
                                               <asp:Label ID="lbVerifyby" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Verify Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbverify" runat="server" Text='<%# Eval("doc_verify_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>

                                      <asp:TemplateField HeaderText="Confirm By">
                                        <ItemTemplate>
                                          
                                               <asp:Label ID="lbNotVerifyby" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Confirm Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbNotverify" runat="server" Text='<%# Eval("tor_verify_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

		      </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center"  VerticalAlign="Top"/>
                                    </asp:TemplateField>
          	         </Columns>
                            </asp:GridView>


                                      <asp:GridView ID="gvTaskDoc2" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." style="display:none;" 
                                OnRowDataBound="gvTaskDoc2_RowDataBound">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblTDocNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="lblGroup" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDivis" runat="server"  Text='<%# Bind("section_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDocCode" runat="server"  Text='<%# Bind("doctype_name") %>'></asp:Label>
                                              <asp:Label ID="lblCode" runat="server"  Text='<%# Bind("doctype_code") %>'></asp:Label>
                                            <asp:Label ID="lblDoctSH" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lblDescrip" runat="server"  Text='<%# Bind("tr_doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblFilename" runat="server"  Text='<%# Bind("doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sheet No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSheetno" runat="server"  Text='<%# Bind("doc_sheetno") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:Label ID="lblRev" runat="server"  Text='<%# Bind("doc_revision") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUpdatedate" runat="server"  Text='<%# Eval("tr_updated_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                            <asp:Label ID="lblhidDate3" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update by">
                                        <ItemTemplate>
                                            <asp:Label ID="lblUpby" runat="server"  Text='<%# Bind("tr_updated_by") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lblBidno" runat="server"  Text='<%# Bind("job_no") %>'></asp:Label>
                                            <asp:Label ID="lblRevbid" runat="server"  Text='<%# Bind("job_revision") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblDocno" runat="server"  Text='<%# Bind("doc_no") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schdule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lblSchname" runat="server"  Text='<%# Bind("schedule_name") %>'></asp:Label>
                                            <asp:Label ID="lblSchnohid" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ViewDoc">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="link_DrawDoc" runat="server" >ViewDoc</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Verify By">
                                               <HeaderTemplate>
                                                <asp:CheckBox ID="cboAllverify" runat="server" />
                                               </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="cboVerify" runat="server" /> <br />
                                                 <asp:Label ID="lblVerifyby" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                          <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Verify Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblverify" runat="server" Text='<%# Eval("doc_verify_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Confirm By">
                                        <ItemTemplate>
                                          
                                               <asp:Label ID="lblNotVerifyby" runat="server" ></asp:Label>
                                        </ItemTemplate>
                                      <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Confirm Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lblNotverify" runat="server" Text='<%# Eval("tor_verify_datetime", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                        </ItemTemplate>
                                         <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                                  <br />
        
                              </td>
                          </tr>
                      </table>
                  </div>


    <div id="myModal"  runat="server" class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                     <tr>
                     <td style="padding-left:15px">
                         <asp:DropDownList ID="ddlStaff" runat="server" Width="150px"></asp:DropDownList>
                             <%-- &nbsp;&nbsp;<asp:DropDownList ID="ddlrev1" runat="server"  Width="110px"></asp:DropDownList>--%>
                          &nbsp; &nbsp;<asp:Button ID="btnAssi" runat="server" Text="Assign" class="btn btn-primary" />

                    </td>
                    </tr>
                                 <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                             <button onclick="document.getElementById('myModal').style.display='none';return false;" class="btn btn-danger">Close</button>
                         </td>
                     </tr>
                     </table>
                </div>
             </div>
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRev" runat="server" />
        <asp:HiddenField ID="hidJobNo" runat="server" />
        <asp:HiddenField ID="hidJobRev" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidMc" runat="server" />
        <asp:HiddenField ID="hidTab" runat="server" />
        <asp:HiddenField ID="hidChk" runat="server" />
        <asp:HiddenField ID="hidClick" runat="server" />
        <asp:HiddenField ID="hidFilter" runat="server" />
           <asp:HiddenField ID="hidFix" runat="server" />
    </form>
</body>
</html>
