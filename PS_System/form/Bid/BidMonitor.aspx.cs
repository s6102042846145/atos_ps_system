﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using PS_Library;

namespace PS_System.form.Monitor
{
    public partial class BidMonitor : System.Web.UI.Page
    {
        public string zhost_name = ConfigurationManager.AppSettings["HOST_NAME"].ToString();
        clsBidMonitor objMoni = new clsBidMonitor();
        EmpInfoClass empInfo = new EmpInfoClass();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z524220";// "z596353";//"z490385";//
                Employee emp = empInfo.getInFoByEmpID_PM(hidLogin.Value);
                hidLevel.Value = emp.ORGLEVEL;
                hidOrg3.Value = emp.ORGSNAME3;
                if (hidLevel.Value == "" && hidOrg3.Value == "")
                {
                    emp = empInfo.getInFoByEmpID(hidLogin.Value);
                    hidLevel.Value = emp.ORGLEVEL;
                    hidOrg3.Value = emp.ORGSNAME3;
                }
                bind_data();
                bind_DDL();
            }
        }
    
        protected void bind_DDL()
        {
            ddlBidno1.Items.Clear();
            ddlBidno2.Items.Clear();
            DataTable dtBid = new DataTable();
            dtBid = objMoni.getDisDocview();
            //DataTable dtbidno = dtBid.DefaultView.ToTable(true, "bid_no");
            //DataTable dtrev = dtBid.DefaultView.ToTable(true, "bid_revision");
            //DataView dvbid = new DataView(dtbidno);
            //dvbid.Sort = "bid_no";
            //DataView dvrev = new DataView(dtrev);
            //dvrev.Sort = "bid_revision";

            foreach (DataRow dr in dtBid.Rows)
            {
                ListItem item1 = new ListItem();
                item1.Text = dr["bid_no"].ToString() + " ( Rev. " + dr["bid_revision"].ToString() + " )";
                item1.Value = dr["bid_no"].ToString() + "|" + dr["bid_revision"].ToString();
                ddlBidno1.Items.Add(item1);

                ListItem item2 = new ListItem();
                item2.Text = dr["bid_no"].ToString() + " ( Rev. " + dr["bid_revision"].ToString() + " )";
                item2.Value = dr["bid_no"].ToString() + "|" + dr["bid_revision"].ToString();
                ddlBidno2.Items.Add(item2);
            }

            //ddlBidno1.DataSource = dtBid;
            //ddlBidno1.DataTextField = "bid_no";
            //ddlBidno1.DataValueField = "bid_no";
            //ddlBidno1.DataBind();

            //ddlBidno2.DataSource = dtBid;
            //ddlBidno2.DataTextField = "bid_no";
            //ddlBidno2.DataValueField = "bid_no";
            //ddlBidno2.DataBind();

            //ddlrev1.DataSource = dvrev;
            //ddlrev1.DataTextField = "bid_revision";
            //ddlrev1.DataValueField = "bid_revision";
            //ddlrev1.DataBind();

            //ddlrev2.DataSource = dvrev;
            //ddlrev2.DataTextField = "bid_revision";
            //ddlrev2.DataValueField = "bid_revision";
            //ddlrev2.DataBind();

            ddlBidno1.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlBidno2.Items.Insert(0, new ListItem("<-- Select -->", ""));
            //ddlrev1.Items.Insert(0, new ListItem("<-- Select -->", ""));
            //ddlrev2.Items.Insert(0, new ListItem("<-- Select -->", ""));
            int level = (hidLevel.Value == "" ? 0 : Convert.ToInt32(hidLevel.Value));
            if (level >= 5) { }
            else
            {
                try
                {
                    List<Employee> liemp = new List<Employee>();
                    liemp = empInfo.getInFoByOrgName_PM(hidOrg3.Value, hidLevel.Value);
                    ddlStaff.Items.Clear();
                    ddlStaff.Items.Insert(0, new ListItem("<-- Select -->", ""));
                    foreach (var item in liemp)
                    {
                        ListItem item1 = new ListItem();
                        item1.Text = item.SNAME + " (" + item.MC_SHORT + ")(" + item.PERNR + ")";
                        item1.Value = item.PERNR + "|" + item.MC_SHORT;
                        ddlStaff.Items.Add(item1);
                    }
                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                }
            }
        }
        protected void bind_data()
        {
            int level = (hidLevel.Value == "" ? 0 : Convert.ToInt32(hidLevel.Value));
            if (level > 3)
            {
               string bidList = objMoni.getAssign(hidLogin.Value);
                DataTable dtBid = new DataTable();
                if (bidList != "")
                {
                    dtBid = objMoni.getDisMonitor(bidList.Replace(",", "','"));
                    this.gvBidInfo.DataSource = dtBid;
                    this.gvBidInfo.DataBind();
                }
                else
                {
                    this.gvBidInfo.DataSource = null;
                    this.gvBidInfo.DataBind();
                }
            }
            else
            {
                DataTable dtBid = new DataTable();
                dtBid = objMoni.getDisMonitor("");
                this.gvBidInfo.DataSource = dtBid;
                this.gvBidInfo.DataBind();
            }
        }
        protected void btnReq_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            string strPositionList = objMoni.getActivityBid(lblBidno.Text);
            EmpInfoClass emp = new EmpInfoClass();
            string strEmailList = emp.getEmailListByPositionList(strPositionList);

            MailNoti mail = new MailNoti();
            mail.SendMailMonitor(strEmailList, lblBidno.Text, "req","");
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Send Successfully');", true);
        }

        protected void btnStart_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");

            objMoni.insertStatus(lblBidno.Text,lblBidrev.Text,"Start_Bid",hidLogin.Value);

            string strPositionList = objMoni.getActivityBid(lblBidno.Text);
            EmpInfoClass emp = new EmpInfoClass();
            string strEmailList = emp.getEmailListByPositionList(strPositionList);

            MailNoti mail = new MailNoti();
            mail.SendMailMonitor(strEmailList, lblBidno.Text, "start","");
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);

        }

        protected void gvBidInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                clsBidDashbaord objbid = new clsBidDashbaord();
                DataRowView drv = (DataRowView)e.Row.DataItem;
                GridView gv = (GridView)e.Row.FindControl("gvSubBidInfo");
                GridView gvstatus = (GridView)e.Row.FindControl("gvSubBidStatus");
                Button btnAssign = (Button)e.Row.FindControl("btnAssign");
                HyperLink hlnk_Bidno = (HyperLink)e.Row.FindControl("hlnk_Bidno");
                HyperLink hypGotoOut = (HyperLink)e.Row.FindControl("hypGotoOut");
                Button btnSup = (Button)e.Row.FindControl("btnSup");
                int level = (hidLevel.Value == "" ? 0 : Convert.ToInt32(hidLevel.Value));
                if (level >= 5) btnAssign.Visible = false;
                else btnAssign.Visible = true;

                //Button btnSup = (Button)e.Row.FindControl("btnSup");
               
                DataTable dt = objMoni.getDataMonitor(drv["bid_no"].ToString());
                DataTable dtchk = objMoni.chkSupply(drv["bid_no"].ToString());
                if (dtchk.Rows.Count > 0)
                {
                    if (dtchk.Rows[0]["is_epc"].ToString() == "1")
                    {
                        btnSup.Style["display"] = "none";
                        hlnk_Bidno.Text = "<a href = '" + ConfigurationManager.AppSettings["url_Biddash_M"].ToString() + "?bidno=" + drv["bid_no"].ToString() + "&rev=" + drv["bid_revision"].ToString() + "&zuserlogin=" + hidLogin.Value + "' TARGET='openWindow' > " + drv["bid_no"].ToString() + " </ a >";
                    }
                    else
                    {
                        hlnk_Bidno.Text = "<a href = '" + ConfigurationManager.AppSettings["url_Biddash_M_Sup"].ToString() + "?bidno=" + drv["bid_no"].ToString() + "&rev=" + drv["bid_revision"].ToString() + "&zuserlogin=" + hidLogin.Value + "' TARGET='openWindow' > " + drv["bid_no"].ToString() + " </ a >";
                    }
                }
                gv.DataSource = dt;
                gv.DataBind();
            
                DataTable dt2 = objMoni.getDept(drv["bid_no"].ToString(), drv["bid_revision"].ToString());
                for (int j = 0; j < dt2.Rows.Count; j++)
                {
                    if (dt2.Rows[j]["depart_position_name"].ToString() == "กวศ-ส.")
                    {
                        DataTable dtspecial = objMoni.getDeptKor(drv["bid_no"].ToString(), drv["bid_revision"].ToString());
         
                        DataRow dr2 = dt2.NewRow();
                        dr2["depart_position_name"] = "กวอ-ส.";
                        dr2["order_no"] = "011";
                        for (int q = 0; q < dtspecial.Rows.Count; q++)
                        {
                            dr2["status"] = dtspecial.Rows[q]["status"].ToString(); 
                            dr2["statusorder"] = dtspecial.Rows[q]["statusorder"].ToString();
                        }      
                        dt2.Rows.Add(dr2);
                    }
                }
                DataView dvsort = new DataView(dt2);
                dvsort.Sort = "order_no asc";
                DataTable dtsort = dvsort.ToTable();
                DataTable dtdept = dtsort.DefaultView.ToTable(true, "depart_position_name");
                dtdept.Columns.Add("status");
                DataView dv = new DataView(dt2);
              
                for (int i = 0; i < dtdept.Rows.Count; i++)
                {
                    string status = "";
                    DataRow dr = dtdept.Rows[i];
 
                    dv.RowFilter = "depart_position_name='" + dtdept.Rows[i]["depart_position_name"].ToString() + "'";
                    dv.Sort = "statusorder asc";
                    for (int k = 0; k < dv.Count; k++)
                    {
                        status = dv[k]["status"].ToString();
                    }
                
                    dr["status"] = (status == "" ? "Not Start" : status);
                    dtdept.AcceptChanges();
                }
                gvstatus.DataSource = dtdept;
                gvstatus.DataBind();
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnTor_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");

            objMoni.insertStatus(lblBidno.Text, lblBidrev.Text, "Start_TOR", hidLogin.Value);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
        }

        protected void btnSell_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");

            objMoni.insertStatus(lblBidno.Text, lblBidrev.Text, "Sell_Bid", hidLogin.Value);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);

        }
        protected void btnOpen_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");

            objMoni.insertStatus(lblBidno.Text, lblBidrev.Text, "Open_Bid", hidLogin.Value);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
        }
        protected void btnView1_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            string xdoc_url = "";
            string bidno = "";
            string rev = "";
          if(ddlBidno1.SelectedValue != "")
            {
                string[] sp = ddlBidno1.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }

            DataTable dt = objMoni.getDocview(bidno, rev);
            if (dt.Rows.Count > 0)
            {
                string strMergedPdfNodeId = dt.Rows[0]["document_nodeid"].ToString();
                xdoc_url = "https://" + zhost_name + "/tmps/forms/viewGC3?bidno=" + bidno + "&bidrev=" + rev + " #toolbar=0";
                Response.Headers.Remove("X-Frame-Options");
                Response.AddHeader("X-Frame-Options", "AllowAll");
                ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);
            }
            else
            {
                ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", "about:blank");
            }
        }

        protected void btnView2_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            string xdoc_url = "";
            string bidno = "";
            string rev = "";
            if (ddlBidno2.SelectedValue != "")
            {
                string[] sp = ddlBidno2.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
            DataTable dt = objMoni.getDocview(bidno, rev);
            if (dt.Rows.Count > 0)
            {
                string strMergedPdfNodeId = dt.Rows[0]["document_nodeid"].ToString();
                xdoc_url = "https://" + zhost_name + "/tmps/forms/viewGC3?bidno=" + bidno + "&bidrev=" + rev + "#toolbar=0";
                Response.Headers.Remove("X-Frame-Options");
                Response.AddHeader("X-Frame-Options", "AllowAll");
                ((HtmlControl)(form1.FindControl("docview2"))).Attributes.Add("src", xdoc_url);
            }
            else
            {
                ((HtmlControl)(form1.FindControl("docview2"))).Attributes.Add("src", "about:blank");
            }
        }

        protected void btnDoc_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");

            string strUrl = ConfigurationManager.AppSettings["url_Biddash_M"].ToString() + "?zuserlogin=" + hidLogin.Value + "&bidno=" + lblBidno.Text + "&rev=" + lblBidrev.Text;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_blank')", true);
        }

        protected void btnSup_Click(object sender, EventArgs e)
        {
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");

            string strUrl = ConfigurationManager.AppSettings["url_Grouping"].ToString() + "?zuserlogin=" + hidLogin.Value + "&bidno=" + lblBidno.Text + "&rev=" + lblBidrev.Text;
            ScriptManager.RegisterStartupScript(Page, Page.GetType(), "popup", "window.open('" + strUrl + "','_blank')", true);
        }

        protected void btnChange_Click(object sender, EventArgs e)
        {
            dvDetailMail.Style["display"] = "block";
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");
            hidBidno.Value = lblBidno.Text;
            hidRev.Value = lblBidrev.Text;


            //string strPositionList = objMoni.getActivityBid(lblBidno.Text);
            //EmpInfoClass emp = new EmpInfoClass();
            //string strEmailList = emp.getEmailListByPositionList(strPositionList);

            //objMoni.insertStatus(lblBidno.Text, lblBidrev.Text, "Change_Bid", hidLogin.Value);
            //MailNoti mail = new MailNoti();
            //mail.SendMailMonitor("", lblBidno.Text, "change");
            //ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
        }

        protected void btnAssiStaff_Click(object sender, EventArgs e)
        {
            if (ddlStaff.SelectedValue != "")
            {
                string empid = "";
                string position = "";
                string[] sp = ddlStaff.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    empid = sp[0];
                    position = sp[1];
                }

                objMoni.AssignToStaff(hidBidno.Value, hidRev.Value, empid, position, hidLogin.Value);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select to Assign');", true);
            }
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            dvAssign.Style["display"] = "block";
            Button btn = sender as Button;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Label lblBidno = (Label)gvr.FindControl("lblBidno");
            Label lblBidrev = (Label)gvr.FindControl("lblBidrev");
            hidBidno.Value = lblBidno.Text;
            hidRev.Value = lblBidrev.Text;
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
        }

        protected void btnCloseAssi_Click(object sender, EventArgs e)
        {
            dvAssign.Style["display"] = "none";
        }

        protected void btnCloseMail_Click(object sender, EventArgs e)
        {
            dvDetailMail.Style["display"] = "none";
        }

        protected void btnSendMail_Click(object sender, EventArgs e)
        {
            objMoni.insertStatus(hidBidno.Value, hidRev.Value, "Change_Bid", hidLogin.Value);
            MailNoti mail = new MailNoti();
            mail.SendMailMonitor(txtTo.Text, hidBidno.Value, "change", txtContent.Text);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
        }
    }
}