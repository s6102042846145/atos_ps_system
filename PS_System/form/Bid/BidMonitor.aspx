﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidMonitor.aspx.cs" Inherits="PS_System.form.Monitor.BidMonitor"  MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
            .docview {
            height: 450px;
            width: 100%;
        }
    
      
    </style>
    <link href="../../Content/w3.css" rel="stylesheet" />
    
    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script type="text/javascript">
        function changeBid() {
            window.open('../Bid/ChangeBidPlan.aspx', '_blank');
            return false;
        }

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });
        function openPopup() {
        
            document.getElementById('dvAssign').style.display = "block";
    
        }
        $(document).ready(function () {
            $('[id*=ddlBidno1]').multiselect({
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 300
        });

            $('[id*=ddlBidno2]').multiselect({
            enableCaseInsensitiveFiltering: true,
            enableFiltering: true,
            maxHeight: 300
            });

            $('[id*=ddlStaff]').multiselect({
                enableCaseInsensitiveFiltering: true,
                enableFiltering: true,
                maxHeight: 300
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Monitor"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="padding-bottom:15px;">
                        <div style="margin:20px">
              <%--              <asp:Button ID="btnCompare" runat="server" Text="Compare" class="btn btn-primary" onclick="document.getElementById('myModal').style.display='block';return false;" />--%>
                              <div style="text-align:right;padding-bottom:10px"><button onclick="document.getElementById('myModal').style.display='block';return false;" class="btn btn-primary">Compare</buttoCompare</button></div> 
                        <asp:GridView ID="gvBidInfo" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvBidInfo_RowDataBound" EmptyDataText="No data found." >
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White"/>
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Bid No.">
                                    <ItemTemplate>
                                       <asp:HyperLink ID="hlnk_Bidno" runat="server" ></asp:HyperLink>
                                       <asp:Label ID="lblBidno" runat="server" Text='<%# Bind("bid_no") %>' Visible="false"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="60px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Revision">
                                    <ItemTemplate>
                                        <asp:Label ID="lblBidrev" runat="server" Text='<%# Bind("bid_revision") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Related Jobs">
                                    <ItemTemplate>
                                              <asp:GridView ID="gvSubBidInfo" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="99%" BackColor="White" BorderColor="#CCCCCC" 
                                        EmptyDataText="No data found.">
                                            <Columns>
                                                    <asp:TemplateField HeaderText="Job No.">
                                                    <ItemTemplate>
                                          
                                                          <asp:Label ID="lblJobno" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Revision">
                                                    <ItemTemplate>
                                                    <asp:Label ID="Label302" runat="server" Text='<%# Bind("job_revision") %>'></asp:Label>

                                                    </ItemTemplate>
                                                    <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Job Progress(%)">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lalbel22" runat="server" Text='<%# Eval("weight_percent", "{0:0}%") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                             <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                               <asp:TemplateField HeaderText="Bid Prep Status" >
                                    <ItemTemplate>
                                         <asp:GridView ID="gvSubBidStatus" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="99%" BackColor="White" BorderColor="#CCCCCC"  
                                        EmptyDataText="No data found.">
                                            <Columns>
                                                    <asp:TemplateField HeaderText="หน่วยงาน">
                                                    <ItemTemplate>
                                                          <asp:Label ID="lblDept" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Status">
                                                    <ItemTemplate>
                                                    <asp:Label ID="Label302" runat="server" Text='<%# Bind("status") %>' ></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="50%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                             <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                           <asp:TemplateField HeaderText="Action" >
                                    <ItemTemplate>
                                        <asp:Button ID="btnReq" runat="server" CssClass="btn btn-primary" Text="Request to Pending" Width="150px" OnClick="btnReq_Click" style="display:none;"/>
                                      <asp:Button ID="btnAssign" runat="server" CssClass="btn btn-primary" Text="Assign to Staff" Width="160px" OnClick="btnAssign_Click"  />
                                         &nbsp;  <asp:Button ID="btnSup" runat="server" CssClass="btn btn-primary" Text="Manage Bid Supply" Width="160px" OnClick="btnSup_Click"  />
                                            <br />  <br />  <asp:Button ID="btnDoc" runat="server" CssClass="btn btn-primary" Text="Validate Document" Width="160px" OnClick="btnDoc_Click"/>
                                            &nbsp;     <asp:Button ID="btnStart" runat="server" CssClass="btn btn-primary" Text="Start Bid Preparation" Width="160px" OnClick="btnStart_Click" />
                                 <br />  <br /> <asp:Button ID="btnTor" runat="server" CssClass="btn btn-primary" Text="Start TOR" Width="160px" OnClick="btnTor_Click"  />
                              &nbsp;    <asp:Button ID="btnChange" runat="server" CssClass="btn btn-primary" Text="Change Bid Plan" Width="160px" OnClick="btnChange_Click"  />
                                    <asp:Button ID="btnSell" runat="server" CssClass="btn btn-primary" Text="Selling Bid Preparation" Width="160px" OnClick="btnSell_Click"  style="display:none;"/>
                                  <asp:Button ID="btnOpen" runat="server" CssClass="btn btn-primary" Text="Open Bid Preparation" Width="160px" OnClick="btnOpen_Click" style="display:none;"/>
                                    
                                    </ItemTemplate>
                                    <ItemStyle Width="25%" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Bid Status" >
                                    <ItemTemplate>
                                        <asp:Label ID="lblStatus" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                            </div>
                    </td>
                </tr>
            </table>
        </div>

          <div id="myModal"  runat="server" class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                     <tr>
                     <td style="padding-left:15px">
                         <asp:DropDownList ID="ddlBidno1" runat="server" Width="150px"></asp:DropDownList>
                         <%--      &nbsp;&nbsp;<asp:DropDownList ID="ddlrev1" runat="server"  Width="110px"></asp:DropDownList>--%>&nbsp; &nbsp;<asp:Button ID="btnView1" runat="server" Text="View" class="btn btn-primary" OnClick="btnView1_Click" />

                    </td>
                    </tr>
                    <tr>
                         <td align="center">
                       <asp:Panel ID="p_PDF" runat="server"  Height="450px" Width="98%">
                        <iframe id="docview" src="about:blank" runat="server" class="docview"></iframe>
                    </asp:Panel>
                         </td>
                        </tr>
                        <tr><td style="height:10px"></td></tr>
                      <tr>
                     <td style="padding-left:15px">
                         <asp:DropDownList ID="ddlBidno2" runat="server" Width="150px"></asp:DropDownList>
                         <%--         &nbsp;&nbsp;<asp:DropDownList ID="ddlrev2" runat="server" Width="110px"></asp:DropDownList>--%>&nbsp;&nbsp;<asp:Button ID="btnView2" runat="server" Text="View" class="btn btn-primary" OnClick="btnView2_Click" />
                    </td>
                    </tr>
                    <tr>
                         <td align="center">
                       <asp:Panel ID="p_PDF2" runat="server" Height="450px" Width="98%">
                        <iframe id="docview2" src="about:blank" runat="server" class="docview"></iframe>
                    </asp:Panel>
                         </td>
                        </tr>
                               <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                             <asp:Button ID="btnClose" runat="server" Text="Close" class="btn btn-danger" OnClick="btnClose_Click"/>
                             <%--<button onclick="document.getElementById('myModal').style.display='none';return false;" class="btn btn-danger">Close</button>--%>
                         </td>
                     </tr>
                     </table>
                </div>
             </div>

           <div id="dvAssign"  runat="server"  class="modal"> <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                     <tr>
                     <td style="padding-left:15px">
                         <asp:DropDownList ID="ddlStaff" runat="server" Width="320px"></asp:DropDownList>
                         <%-- &nbsp;&nbsp;<asp:DropDownList ID="ddlrev1" runat="server"  Width="110px"></asp:DropDownList>--%>&nbsp; &nbsp;<asp:Button ID="btnAssiStaff" runat="server" Text="Assign" class="btn btn-primary" OnClick="btnAssiStaff_Click" />

                    </td>
                    </tr>
                                 <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                             <asp:Button ID="btnCloseAssi" runat="server" Text="Close" class="btn btn-danger" OnClick="btnCloseAssi_Click"/>
                             <%--  <button onclick="document.getElementById('dvAssign').style.display='none';return false;" class="btn btn-danger">Close</button>--%>
                         </td>
                     </tr>
                     </table>
                </div>
             </div>

         <div id="dvDetailMail"  runat="server" class="modal" > <%--   class="modal"  --%>
            <div class="modal-content">
                 <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                     <tr>
                     <td style="padding-left:15px">
                         <asp:Label ID="Label2" runat="server" Text="To:"></asp:Label>
                           <asp:TextBox ID="txtTo" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="96.5%" ></asp:TextBox>
                   

                    </td>
                     <tr>
                     <td style="padding-left:15px">
                         <asp:Label ID="Label1" runat="server" Text="Email Content:"></asp:Label>
                           <asp:TextBox ID="txtContent" runat="server" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                   

                    </td>
                    </tr>
                                 <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                              <asp:Button ID="btnSendMail" runat="server" Text="Send" class="btn btn-primary" OnClick="btnSendMail_Click"  />
                             <asp:Button ID="btnCloseMail" runat="server" Text="Close" class="btn btn-danger" OnClick="btnCloseMail_Click" />
                         </td>
                     </tr>
                     </table>
                </div>
             </div>
              
          <asp:HiddenField ID="hidLogin" runat="server" />
          <asp:HiddenField ID="hidLevel" runat="server" />
         <asp:HiddenField ID="hidOrg3" runat="server" />
           <asp:HiddenField ID="hidBidno" runat="server" />
           <asp:HiddenField ID="hidRev" runat="server" />
    </form>
</body>
</html>
