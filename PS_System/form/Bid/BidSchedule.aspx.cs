﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form
{
    public partial class BidSchedule : System.Web.UI.Page
    {
        public clsBidSchedule clsBid = new clsBidSchedule();
        static DbControllerBase zdbUtil = new DbControllerBase();


        protected void Page_Load(object sender, EventArgs e)
        {
            dvlblCom.Attributes.Add("style", "border:1px solid ;width:150px;height:26px;border-radius:2px;");
        }

        #region " Select Bid No "
        protected void btnViewBid_Click(object sender, EventArgs e)
        {
            this.myModal_Bid.Style.Add("display", "block");
            BindGridView_Bid();
        }
        protected void btnClose_Popup_Click(object sender, EventArgs e)
        {
            this.myModal_Bid.Style.Add("display", "none");
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindGridView_Bid();
        }
        private void BindGridView_Bid()
        {
            DataTable dtBid = clsBid.getBidInfo(this.txtSearchBidno.Text);

            this.gvBid.DataSource = dtBid;
            this.gvBid.DataBind();
        }
        protected void gvBid_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvBid.PageIndex = e.NewPageIndex;
            BindGridView_Bid();
        }
        protected void lnkAction_Click(object sender, EventArgs e)
        {
            int rowid = ((GridViewRow)(sender as Control).NamingContainer).RowIndex;
            TxtBidNo.Text = gvBid.Rows[rowid].Cells[0].Text;
            TxtRevision.Text = gvBid.Rows[rowid].Cells[1].Text;
            TxtBidDesc.Text = gvBid.Rows[rowid].Cells[2].Text;
            Label lblDC = (Label)gvBid.Rows[rowid].FindControl("lblDC");
            if (lblDC.Text != "") { lblCom.Text = lblDC.Text; dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;border-radius:2px;"); }
            else dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;border-radius:2px;");

            if (TxtBidNo.Text != "")
                GetData(TxtBidNo.Text, TxtRevision.Text);

            this.myModal_Bid.Style.Add("display", "none");
        }
        #endregion


        protected void GetData(string strBidno, string strBidRevision)
        {
            DataTable dtBidSchedule = clsBid.getSchedule(strBidno, strBidRevision);

            DataRow drNew = dtBidSchedule.NewRow();
            drNew["schedule_no"] = dtBidSchedule.Rows.Count + 1;
            drNew["schedule_name"] = "";
            drNew["pq"] = 0;
            drNew["fe"] = 0;
            drNew["lc"] = 0;
            drNew["construction"] = 0;
            drNew["total"] = 0;
            dtBidSchedule.Rows.Add(drNew);

            this.gvSchedule.DataSource = dtBidSchedule;
            this.gvSchedule.DataBind();
            Session.Add("dtBidSchedule", dtBidSchedule);
        }
        protected void gvSchedule_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DropDownList ddlJobNo = (DropDownList)e.Row.FindControl("gvSchedule_ddlJobNo");
                HiddenField hidJobNo_Revision = (HiddenField)e.Row.FindControl("gvSchedule_hidJobNo_Revision");
                TextBox gvSchedule_txtTotal = (TextBox)e.Row.FindControl("gvSchedule_txtTotal");
                TextBox gvSchedule_txtFE = (TextBox)e.Row.FindControl("gvSchedule_txtFE");
                TextBox gvSchedule_txtLC = (TextBox)e.Row.FindControl("gvSchedule_txtLC");
                TextBox gvSchedule_txtConstruction = (TextBox)e.Row.FindControl("gvSchedule_txtConstruction");

                gvSchedule_txtFE.Attributes.Add("onblur", "javascript:sumxTotal('" + gvSchedule_txtTotal.ClientID + "','"+ gvSchedule_txtFE.ClientID + "','"+ gvSchedule_txtLC.ClientID + "','"+ gvSchedule_txtConstruction.ClientID + "');");
                gvSchedule_txtLC.Attributes.Add("onblur", "javascript:sumxTotal('" + gvSchedule_txtTotal.ClientID + "','" + gvSchedule_txtFE.ClientID + "','" + gvSchedule_txtLC.ClientID + "','" + gvSchedule_txtConstruction.ClientID + "');");
                gvSchedule_txtConstruction.Attributes.Add("onblur", "javascript:sumxTotal('" + gvSchedule_txtTotal.ClientID + "','" + gvSchedule_txtFE.ClientID + "','" + gvSchedule_txtLC.ClientID + "','" + gvSchedule_txtConstruction.ClientID + "');");
                gvSchedule_txtTotal.Attributes.Add("readonly", "readonly");
                DataTable dtJobRelate = clsBid.loadBid_Job_Relate(TxtBidNo.Text, TxtRevision.Text);
                ddlJobNo.DataSource = dtJobRelate;
                ddlJobNo.DataValueField = "jobno_revision";
                ddlJobNo.DataTextField = "jobno_re_display";
                ddlJobNo.DataBind();

                if (hidJobNo_Revision.Value != "")
                    ddlJobNo.SelectedValue = hidJobNo_Revision.Value;
            }
        }
        protected void gvSchedule_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "adddata")
            {
                DataTable dtBidSchedule = (DataTable)Session["dtBidSchedule"];
                dtBidSchedule.Rows.Clear();
                foreach (GridViewRow g1 in gvSchedule.Rows)
                {
                    TextBox txtScheduleName = (TextBox)g1.FindControl("gvSchedule_txtScheduleName");
                    CheckBox chkPQ = (CheckBox)g1.FindControl("gvSchedule_chkPQ");
                    TextBox txtFE = (TextBox)g1.FindControl("gvSchedule_txtFE");
                    TextBox txtLC = (TextBox)g1.FindControl("gvSchedule_txtLC");
                    TextBox txtConstruction = (TextBox)g1.FindControl("gvSchedule_txtConstruction");
                    TextBox txtTotal = (TextBox)g1.FindControl("gvSchedule_txtTotal");
                    

                    DataRow drOld = dtBidSchedule.NewRow();
                    drOld["schedule_no"] = dtBidSchedule.Rows.Count + 1;
                    drOld["schedule_name"] = txtScheduleName.Text.Trim();
                    drOld["pq"] = (chkPQ.Checked == true ? 1 : 0);
                    drOld["fe"] = Convert.ToDecimal(txtFE.Text);
                    drOld["lc"] = Convert.ToDecimal(txtLC.Text);
                    drOld["construction"] = Convert.ToDecimal(txtConstruction.Text);//ToInt64
                    drOld["total"] = Convert.ToDecimal(txtTotal.Text);
                    dtBidSchedule.Rows.Add(drOld);
                }

                DataRow drNew = dtBidSchedule.NewRow();
                drNew["schedule_no"] = dtBidSchedule.Rows.Count + 1;
                drNew["schedule_name"] = "";
                drNew["pq"] = 0;
                drNew["fe"] = 0;
                drNew["lc"] = 0;
                drNew["construction"] = 0;
                drNew["total"] = 0;
                dtBidSchedule.Rows.Add(drNew);

                this.gvSchedule.DataSource = dtBidSchedule;
                this.gvSchedule.DataBind();

                Session.Add("dtBidSchedule", dtBidSchedule);
            }
            else if (e.CommandName == "removedata")
            {
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvSchedule = (GridView)sender;
                string xScheduleName = ((TextBox)gvSchedule.Rows[xrowid].FindControl("gvSchedule_txtScheduleName")).Text;

                DataTable dtBidSchedule = (DataTable)Session["dtBidSchedule"];
                DataRow[] drSelect = dtBidSchedule.Select("schedule_name = '" + xScheduleName + "'");
                if (drSelect.Length > 0)
                    dtBidSchedule.Rows.Remove(drSelect[0]);

                this.gvSchedule.DataSource = dtBidSchedule;
                this.gvSchedule.DataBind();

                Session.Add("dtBidSchedule", dtBidSchedule);
            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                int iRow = 1;
                string strValue = "";
                double sumfe = 0.0;
                double sumlc = 0.0;
                double sumcon = 0.0;
                double sumtotal = 0.0;
                foreach (GridViewRow g1 in gvSchedule.Rows)
                {
                    DropDownList ddlJobNo = (DropDownList)g1.FindControl("gvSchedule_ddlJobNo");
                    TextBox txtScheduleName = (TextBox)g1.FindControl("gvSchedule_txtScheduleName");
                    CheckBox chkPQ = (CheckBox)g1.FindControl("gvSchedule_chkPQ");
                    TextBox txtFE = (TextBox)g1.FindControl("gvSchedule_txtFE");
                    TextBox txtLC = (TextBox)g1.FindControl("gvSchedule_txtLC");
                    TextBox txtConstruction = (TextBox)g1.FindControl("gvSchedule_txtConstruction");
                    TextBox txtTotal = (TextBox)g1.FindControl("gvSchedule_txtTotal");
                    //sum = Math.Round(Convert.ToDouble(txtFE.Text), 1);
                    if (cbPQ.Checked) chkPQ.Checked = true;
                    else chkPQ.Checked = false;
                    sumfe = (txtFE.Text == "" || txtFE.Text == "0") ? 0 : sumfe = Math.Round(Math.Round(Convert.ToDouble(txtFE.Text), 1));
                    sumlc = (txtLC.Text == "" || txtLC.Text == "0") ? 0 : sumlc = Math.Round(Math.Round(Convert.ToDouble(txtLC.Text), 1));
                    sumcon = (txtConstruction.Text == "" || txtConstruction.Text == "0") ? 0 : sumcon = Math.Round(Math.Round(Convert.ToDouble(txtConstruction.Text), 1)); 
                    sumtotal = (txtTotal.Text == "" || txtTotal.Text == "0") ? 0 : sumtotal = Math.Round(Math.Round(Convert.ToDouble(txtTotal.Text), 1));

                    if (txtScheduleName.Text != "")
                        strValue += (strValue == "" ? "" : "|") + ddlJobNo.SelectedValue + "_" + iRow.ToString() + "_" + txtScheduleName.Text.Trim() + "_" + (chkPQ.Checked == true ? "1" : "0")
                                    + "_" + sumfe + "_" + sumlc + "_" + sumcon + "_" + sumtotal;
                    
                       
                }
                if (strValue != "")
                {
                    clsBid.insertSchedule(TxtBidNo.Text, TxtRevision.Text, strValue, "admin");

                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully.');", true);
                }
                else
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Schedule Name.');", true);
                }
               

            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnClose_Click(object sender, EventArgs e)
        {
            TxtBidNo.Text = "";
            TxtRevision.Text = "";
            TxtBidDesc.Text = "";
           
            gvSchedule.DataSource = null;
            gvSchedule.DataBind();
        }
    }
}




