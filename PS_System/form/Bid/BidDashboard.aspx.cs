﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PS_Library;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Data;
using System.Drawing;
using System.IO.Compression;

namespace PS_System.form.Bid
{
	public partial class BidDashboard : System.Web.UI.Page
	{
		public string zetl4eisdb = ConfigurationManager.AppSettings["db_ps"].ToString();
		static DbControllerBase zdbUtil = new DbControllerBase();
		protected void Page_Load(object sender, EventArgs e)
		{
			
			if (!this.IsPostBack)
			{
				if (Request.QueryString["zuserlogin"] != null)
				{
					hidLogin.Value = Request.QueryString["zuserlogin"];
                }
                else
                {
					hidLogin.Value = "z575798";//"z594900";//"z572993";//"z596353"; //
				}
				if (Request.QueryString["bidno"] != null)
				{
					hidBidNo.Value = Request.QueryString["bidno"];
                }
                else
                {
					hidBidNo.Value = "TS12-S-02";//"TS12-TX-14";// "IPP3-CON-99";//"TS12-L-01";//"TS12-S-02";//"TS12-FRS-05 and "GBA3-FRS-04";//
				}
				if (Request.QueryString["taskid"] != null)
				{
					hidTaskID.Value = Request.QueryString["taskid"];
				}
				else
				{
					hidTaskID.Value = "0";
				}
				if (Request.QueryString["zmode"] != null)
					hidMode.Value = Request.QueryString["zmode"].ToLower();
				else
					hidMode.Value = "";
				EmpInfoClass empInfo = new EmpInfoClass();
				Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
				hidDept.Value = emp.ORGSNAME4;
				hidSect.Value = emp.ORGSNAME5;
				hidMcShort.Value = emp.MC_SHORT;
				bind_detail();
				bind_gvAAS(); 
				bind_rounting(); 
				bind_DDL(); 
				bind_bidDeshboard();
				bind_Doc();
			
			}
			else
			{
				dvViewActivity.Style["display"] = "none";
			
			}
		}
		protected void bind_rounting()
        {
            clsBidDashbaord objdb = new clsBidDashbaord();
            DataTable dt = objdb.getHead(hidDept.Value, hidSect.Value);
            if (dt.Rows.Count > 0)
            {
				ddl_headDept.Items.Clear();
				ddl_headDept.DataSource = dt;//หัวหน้าแผนก
				ddl_headDept.DataTextField = "orgsname5";
				ddl_headDept.DataValueField = "orgsname5";
				ddl_headDept.DataBind();

				ddl_headSect.Items.Clear();
				ddl_headSect.DataSource = dt;//หัวหน้ากอง
				ddl_headSect.DataTextField = "orgsname5";
				ddl_headSect.DataValueField = "orgsname5";
				ddl_headSect.DataBind();

				ddl_assiDept.Items.Clear();
				ddl_assiDept.DataSource = dt;//ช.ฝ่าย
				ddl_assiDept.DataTextField = "orgsname5";
				ddl_assiDept.DataValueField = "orgsname5";
				ddl_assiDept.DataBind();

				ddl_dept.Items.Clear();
				ddl_dept.DataSource = dt;//ฝ่าย
				ddl_dept.DataTextField = "orgsname5";
				ddl_dept.DataValueField = "orgsname5";
				ddl_dept.DataBind();

				ddl_headProject.Items.Clear();
				ddl_headProject.DataSource = dt;//หัวหน้าโครงการ
				ddl_headProject.DataTextField = "orgsname5";
				ddl_headProject.DataValueField = "orgsname5";
				ddl_headProject.DataBind();

			}
            
            ddl_headDept.Items.Insert(0, new ListItem("Not assign", ""));
            ddl_headSect.Items.Insert(0, new ListItem("Not assign", ""));
            ddl_headProject.Items.Insert(0, new ListItem("Not assign", ""));
            ddl_assiDept.Items.Insert(0, new ListItem("Not assign", ""));
            ddl_dept.Items.Insert(0, new ListItem("Not assign", ""));
        }
		private void bind_DDL()
		{
			clsBidDashbaord objBid = new clsBidDashbaord();
			EmpInfoClass empInfo = new EmpInfoClass();
			Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);//this.hidLogin.Value

			string[] staffList = empInfo.getEmpIDAndNameInDepartment(emp.ORGSNAME4);

			DDL_DessignReq.Items.Clear();
			//DDL_EqsignReq.Items.Clear();
			//DDL_DocsignReq.Items.Clear();

			DDL_DessignReq.Items.Insert(0, new ListItem("<-- Select -->", ""));
			//DDL_EqsignReq.Items.Insert(0, new ListItem("<-- Select -->", ""));
			//DDL_DocsignReq.Items.Insert(0, new ListItem("<-- Select -->", ""));

			foreach (string staffInfo in staffList)
			{
				string[] staff = staffInfo.Split(':');

				ListItem item1 = new ListItem();
				//ListItem item2 = new ListItem();
				//ListItem item3 = new ListItem();

				item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
				item1.Value = staff[0].ToString();
				//item2.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
				//item2.Value = staff[0].ToString();
				//item3.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
				//item3.Value = staff[0].ToString();
				DDL_DessignReq.Items.Add(item1);
				//DDL_EqsignReq.Items.Add(item2);
				//DDL_DocsignReq.Items.Add(item3);
			}

			DataTable dtreq = objBid.SelectActivityReqBid(hidDept.Value);
			if (dtreq.Rows.Count > 0)
			{
				DDL_Template.Items.Clear();
				DDL_Template.DataSource = dtreq;//หัวหน้าแผนก
				DDL_Template.DataTextField = "temp_name";
				DDL_Template.DataValueField = "temp_name";
				DDL_Template.DataBind();

			}
			//DDL_Template.Items.Insert(0, new ListItem("<-- Select Template -->", ""));
		}
		private void bind_Doc()
		{
			clsBidDashbaord objdb = new clsBidDashbaord();
			DataTable dt = objdb.getDoc(txt_bidNo.Text);
			if (dt.Rows.Count > 0)
			{
				gv_MainDoc.DataSource = dt;
				gv_MainDoc.DataBind();
			}
			else
			{
				gv_MainDoc.DataSource = null;
				gv_MainDoc.DataBind();
			}
		}

		private void bind_detail()
		{
			DataTable dt = new DataTable();

			clsBidDashbaord objdb = new clsBidDashbaord();
			dt = objdb.GetDetail(hidBidNo.Value);
			if (dt.Rows.Count > 0)
			{
				txt_bidNo.Text = dt.Rows[0]["bid_no"].ToString();
				txt_rev.Text = dt.Rows[0]["bid_revision"].ToString();
				txt_desc.Text = dt.Rows[0]["bid_desc"].ToString();
				DateTime dtSta = new DateTime();
				DateTime dtEnd = new DateTime();

				if (dt.Rows[0]["p6_plan_start"].ToString() != "" && dt.Rows[0]["p6_plan_start"].ToString() != null)
				{
					dtSta = Convert.ToDateTime(dt.Rows[0]["p6_plan_start"].ToString());
					txtStartDate.Text = dtSta.ToString("dd/MM/yyyy");
				}
				if (dt.Rows[0]["p6_plan_end"].ToString() != "" && dt.Rows[0]["p6_plan_end"].ToString() != null)
				{
					dtEnd = Convert.ToDateTime(dt.Rows[0]["p6_plan_end"].ToString());
					txtEndDate.Text = dtEnd.ToString("dd/MM/yyyy");
				}
				if (dt.Rows[0]["completion_date"].ToString() != "") { lblCom.Text = dt.Rows[0]["completion_date"].ToString(); dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;"); }
				else dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
				txtRemark.Text = dt.Rows[0]["remark"].ToString();
            }
            else
            {
				dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
			}

			dt = objdb.getAttachWAbid(hidBidNo.Value);
			if (dt.Rows.Count > 0)
			{
				gvAttach.DataSource = dt;
				gvAttach.DataBind();
			}
			else
			{
				gvAttach.DataSource = null;
				gvAttach.DataBind();
			}
		}
		
		protected void bind_bidDashboardDocOther()
		{
			DataTable dt = new DataTable();
			clsBidDashbaord objdb = new clsBidDashbaord();
			dt = objdb.GetDocOther(txt_bidNo.Text,txt_rev.Text);
			if (dt.Rows.Count > 0)
			{
				gv_docOther.DataSource = dt;
				gv_docOther.DataBind();
				gv_docOther.UseAccessibleHeader = true;
				gv_docOther.HeaderRow.TableSection = TableRowSection.TableHeader;
				
			}
            else
            {
				gv_docOther.DataSource = null;
				gv_docOther.DataBind();
			}
		}
		protected void bind_bidDashboardDoc()
		{

   //         clsBidDashbaord objdb = new clsBidDashbaord();
   //         DataTable dt = objdb.GetDoc(txt_bidNo.Text);
   //         if (dt.Rows.Count > 0)
   //         {
   //             gv_biding.DataSource = dt;
   //             gv_biding.DataBind();
   //         }
   //         else
   //         {
			//	gv_biding.DataSource = null;
			//	gv_biding.DataBind();
			//}
        }
		protected void bind_gvAAS()
		{
			//	string sql = @"SELECT TOP(10) [depart_position_name]
			// ,[section_position_name]
			// ,[activity_name]
			// ,[req_drawing]
			// ,[assignee_drawing]
			// ,[req_equip]
			// ,[assignee_equip]
			// ,[req_tech_doc]
			// ,[assignee_tech_doc]
			// ,[activity_id]
			// ,[next_activity_id]
			// ,[approval_routing]
			// ,[email_noti_id]
			// ,[email_content]
			// ,[status_drawing]
			// ,[status_equip]
			// ,[status_tech_doc]
			// ,[job_no]
			// ,[job_revision]
			//FROM [ECM_DEV].[dbo].[vw_ps_bid_dashboard_activity]";

			clsBidDashbaord objdb = new clsBidDashbaord();
			DataTable dt = objdb.GetAct(txt_bidNo.Text, txt_rev.Text);
			if (dt.Rows.Count>0)
            {
				bt_assign.Visible = true;
				gvaas.DataSource = dt;
				gvaas.DataBind();
                //gvaas.UseAccessibleHeader = true;
                //gvaas.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
				bt_assign.Visible = false;
				gvaas.DataSource = null;
				gvaas.DataBind();
			}

			//if (ds.Tables.Count > 0)
			//{
			//	gvaas.DataSource = ds;
			//	gvaas.DataBind();
			//}
		}
		private void bind_bidDeshboard()

		{
			//	string sql = @"SELECT TOP(10) [Rowno]
			// ,[job_no]
			// ,[job_desc]
			// ,[job_revision]
			// ,[schedule_no]
			// ,[schedule_name]
			// ,[schedule_status]
			// ,[bid_no]
			// ,[bid_revision]
			//FROM [ECM_DEV].[dbo].[vw_ps_bid_dashboard_relate]";
			//	DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
			//	DataTable dtAdd = new DataTable("tblBidDash");
			//	dtAdd.Merge(dt);
			//	DataSet ds = new DataSet();
			//	ds.Tables.Add(dtAdd);
			clsBidDashbaord objdb = new clsBidDashbaord();
			DataTable dt = objdb.GetSummary(txt_bidNo.Text);
			if (dt.Rows.Count > 0)
			{
				gvJobSumary.DataSource = dt;
				gvJobSumary.DataBind();
				gvJobSumary.UseAccessibleHeader = true;
				gvJobSumary.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
				gvJobSumary.DataSource = null;
				gvJobSumary.DataBind();
			}

		
		}
		protected void gvaasSub_RowDataBound(object sender, GridViewRowEventArgs e)
		{
            if (e.Row.RowType==DataControlRowType.Header)
            {
                CheckBox cb_all = (CheckBox)e.Row.FindControl("cb_all");
				//comment by kie
                //((CheckBox)e.Row.FindControl("cb_all")).Attributes.Add("onclick",
                //   "javascript:SelectAll('" +
                //   ((CheckBox)e.Row.FindControl("cb_all")).ClientID + "')");
				
				//GridViewRow row = e.Row;
				//GridView gvaasSub = (GridView)row.NamingContainer;
				//cb_all.Attributes.Add("onclick", "javascript:chkAll('" + cb_all.ClientID + "' ,'" + gvaasSub.ClientID + "');");

			}
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_depm = (Label)e.Row.FindControl("lb_depm");
				lb_depm.Text = drv["depart_position_name"].ToString();

				Label lb_sec = (Label)e.Row.FindControl("lb_sec");
				lb_sec.Text = drv["section_position_name"].ToString();

				Label lb_activity = (Label)e.Row.FindControl("lb_activity");
				lb_activity.Text = drv["activity_name"].ToString();
				//Label lb_aas = (Label)e.Row.FindControl("lb_aas");
				//lb_aas.Text = drv["test4"].ToString();
				Label lb_statusDA = (Label)e.Row.FindControl("lb_statusDA");
				Label lb_statusDPSA = (Label)e.Row.FindControl("lb_statusDPSA");
				Label lb_statusDSA = (Label)e.Row.FindControl("lb_statusDSA");
				Label lbhidStatus = (Label)e.Row.FindControl("lbhidStatus");
				//if (drv["status_drawing"].ToString() != "") lb_statusDA.Text = drv["status_drawing"].ToString(); else lb_statusDA.Text = "-";
				//if (drv["status_tech_doc"].ToString() != "") lb_statusDSA.Text = drv["status_tech_doc"].ToString(); else lb_statusDSA.Text = "-";
				//if (drv["status_equip"].ToString() != "") lb_statusDPSA.Text = drv["status_equip"].ToString(); else lb_statusDPSA.Text = "-";


				CheckBox cb_sub = (CheckBox)e.Row.FindControl("cb_sub");
				DropDownList ddl_da = (DropDownList)e.Row.FindControl("ddl_da");
				DropDownList ddl_epsa = (DropDownList)e.Row.FindControl("ddl_epsa");
				DropDownList ddl_dsa = (DropDownList)e.Row.FindControl("ddl_dsa");
				HiddenField hdf_da = (HiddenField)e.Row.FindControl("hdf_da");
				//if(drv["assignee_drawing"].ToString().ToLower()!="null" && drv["assignee_drawing"]!=null) hdf_da.Value = drv["assignee_drawing"].ToString();
				HiddenField hdf_epsa = (HiddenField)e.Row.FindControl("hdf_epsa");
				//if(drv["assignee_equip"].ToString().ToLower()!= "null" && drv["assignee_equip"]!=null) hdf_epsa.Value = drv["assignee_equip"].ToString();
				HiddenField hdf_dsa = (HiddenField)e.Row.FindControl("hdf_dsa");
				//if(drv["assignee_tech_doc"].ToString().ToLower()!= "null" && drv["assignee_tech_doc"]!=null) hdf_dsa.Value = drv["assignee_tech_doc"].ToString();

				Label lb_reqDraw = (Label)e.Row.FindControl("lb_reqDraw");
				lb_reqDraw.Text = drv["req_drawing"].ToString();
				Label lb_reqEqui = (Label)e.Row.FindControl("lb_reqEqui");
				lb_reqEqui.Text = drv["req_equip"].ToString();
				Label lb_reqTechDoc = (Label)e.Row.FindControl("lb_reqTechDoc");
				lb_reqTechDoc.Text = drv["req_tech_doc"].ToString();
				Label lb_ActID = (Label)e.Row.FindControl("lb_ActID");
				lb_ActID.Text = drv["activity_id"].ToString();
				Label lb_nxtActID = (Label)e.Row.FindControl("lb_nxtActID");
				lb_nxtActID.Text = drv["next_activity_id"].ToString();
				Label lb_apprRout = (Label)e.Row.FindControl("lb_apprRout");
				lb_apprRout.Text = drv["approval_routing"].ToString();
				Label lb_emailNoti = (Label)e.Row.FindControl("lb_emailNoti");
				lb_emailNoti.Text = drv["email_noti_id"].ToString();
				Label lb_emailCont = (Label)e.Row.FindControl("lb_emailCont");
				lb_emailCont.Text = drv["email_content"].ToString();
				Label lb_bidNo = (Label)e.Row.FindControl("lb_bidNo");
				lb_bidNo.Text = drv["bid_no"].ToString();
				Label lb_bidRev = (Label)e.Row.FindControl("lb_bidRev");
				lb_bidRev.Text = drv["bid_revision"].ToString();
				//if (drv["is_select"].ToString() == "1") cb_sub.Checked = true;
				//else cb_sub.Checked = false;
				lbhidStatus.Text = drv["status_activity"].ToString();
				Label lblReq_dr = (Label)e.Row.FindControl("lblReq_dr");
				Label lblReq_eq = (Label)e.Row.FindControl("lblReq_eq");
				Label lblReq_doc = (Label)e.Row.FindControl("lblReq_doc");

				EmpInfoClass empInfo = new EmpInfoClass();
				Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
				string[] staffList = empInfo.getEmpIDAndNameInDepartment(drv["depart_position_name"].ToString());//emp.ORGSNAME4
				ddl_da.Items.Clear();
				//ddl_epsa.Items.Clear();
				//ddl_dsa.Items.Clear();
				ddl_da.Items.Insert(0, new ListItem("<-- Select -->", ""));
				//ddl_epsa.Items.Insert(0, new ListItem("<-- Select -->", ""));
				//ddl_dsa.Items.Insert(0, new ListItem("<-- Select -->", ""));
				
				foreach (string staffInfo in staffList)
				{
					string[] staff = staffInfo.Split(':');

					ListItem item1 = new ListItem();

					item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
					item1.Value = staff[0].ToString();

					ddl_da.Items.Add(item1);

					//ListItem item2 = new ListItem();
					//item2.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
					//item2.Value = staff[0].ToString();
					//ddl_epsa.Items.Add(item2);

					//ListItem item3 = new ListItem();
					//item3.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
					//item3.Value = staff[0].ToString();
					//ddl_dsa.Items.Add(item3);
				}
				Label lb_headDept = (Label)e.Row.FindControl("lb_headDept");
				Label lb_headSect = (Label)e.Row.FindControl("lb_headSect");
				Label lb_headProject = (Label)e.Row.FindControl("lb_headProject");
				Label lb_assiDept = (Label)e.Row.FindControl("lb_assiDept");
				Label lb_dept = (Label)e.Row.FindControl("lb_dept");
				ImageButton imgbt = (ImageButton)e.Row.FindControl("imgbt");

				string tooltip = "";
				string[] rounting = GetRoutingApproval(drv["approval_routing"].ToString());
				if (rounting.Length > 0)
				{
					if (rounting.Length == 1 && rounting[0] != "null")
					{
						lb_headDept.Text = rounting[0];
						tooltip = "Approve1 : " + lb_headDept.Text;

					}
					else if (rounting.Length == 2)
					{
						lb_headDept.Text = rounting[0];
						lb_headSect.Text = rounting[1];
						tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
							"Approve2 : " + lb_headSect.Text;
					}
					else if (rounting.Length == 3)
					{
						lb_headDept.Text = rounting[0];
						lb_headSect.Text = rounting[1];
						lb_headProject.Text = rounting[2];
						tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
							"Approve2 : " + lb_headSect.Text + "\n" +
							"Approve3 : " + lb_headProject.Text;
					}
					else if (rounting.Length == 4)
					{
						lb_headDept.Text = rounting[0];
						lb_headSect.Text = rounting[1];
						lb_headProject.Text = rounting[2];
						lb_assiDept.Text = rounting[3];
						tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
							"Approve2 : " + lb_headSect.Text + "\n" +
							"Approve3 : " + lb_headProject.Text + "\n" +
							"Approve4 : " + lb_assiDept.Text;
					}
					else if (rounting.Length == 5)
					{
						lb_headDept.Text = rounting[0];
						lb_headSect.Text = rounting[1];
						lb_headProject.Text = rounting[2];
						lb_assiDept.Text = rounting[3];
						lb_dept.Text = rounting[4];
						tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
							"Approve2 : " + lb_headSect.Text + "\n" +
							"Approve3 : " + lb_headProject.Text + "\n" +
							"Approve4 : " + lb_assiDept.Text + "\n" +
							"Approve5 : " + lb_dept.Text;
					}
					imgbt.ToolTip = tooltip;
				}
				else
				{
					clsBidDashbaord objdb = new clsBidDashbaord();
					DataTable dt = objdb.getHead(hidDept.Value, hidSect.Value);
					if (dt.Rows.Count > 0)
					{
						lb_headDept.Text = dt.Rows[0]["orgsname5"].ToString();
						lb_headSect.Text = dt.Rows[1]["orgsname5"].ToString();
						lb_headProject.Text = dt.Rows[2]["orgsname5"].ToString();
						lb_assiDept.Text = dt.Rows[3]["orgsname5"].ToString();
                        if (dt.Rows.Count == 5)
                        {
							lb_dept.Text = dt.Rows[4]["orgsname5"].ToString();
							tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
								"Approve2 : " + lb_headSect.Text + "\n" +
								"Approve3 : " + lb_headProject.Text + "\n" +
								"Approve4 : " + lb_assiDept.Text + "\n" +
								"Approve5 : " + lb_dept.Text;
							imgbt.ToolTip = tooltip;
						}
                        else
                        {
							tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
								"Approve2 : " + lb_headSect.Text + "\n" +
								"Approve3 : " + lb_headProject.Text + "\n" +
								"Approve4 : " + lb_assiDept.Text;
							imgbt.ToolTip = tooltip;
						}
					
					}

				}


				//if (lb_reqDraw.Text == "False" && lb_reqEqui.Text == "False" && lb_reqTechDoc.Text == "False") cb_sub.Enabled = false;

				if (lb_reqDraw.Text == "False")
				{
					ddl_da.SelectedIndex = 0;
					//ddl_da.Enabled = false;
					lb_statusDA.Text = "-";
                } else
					lblReq_dr.Visible = true;

				//if (lb_reqEqui.Text == "False")
				//{
				//	ddl_epsa.SelectedIndex = 0;
				//	//ddl_epsa.Enabled = false;
				//	lb_statusDPSA.Text = "-";
				//} else
				//	lblReq_eq.Visible = true;

				//if (lb_reqTechDoc.Text == "False")
				//{
				//	ddl_dsa.SelectedIndex = 0;
				//	//ddl_dsa.Enabled = false;
				//	lb_statusDSA.Text = "-";
				// }else
				//	lblReq_doc.Visible = true;

				if (hidDept.Value != drv["depart_position_name"].ToString())
				{
					//ใช้ได้ตามกอง/แผนก
					cb_sub.Enabled = false;
					imgbt.Enabled = false;
					ddl_da.Enabled = false;
					//ddl_epsa.Enabled = false;
					//ddl_dsa.Enabled = false;
					lblReq_dr.Visible = false;
					//lblReq_eq.Visible = false;
					//lblReq_doc.Visible = false;

				}

				if (hdf_da.Value != "")
				{
					ddl_da.SelectedValue = hdf_da.Value;
				}
				//if (hdf_epsa.Value != "")
				//{
				//	ddl_epsa.SelectedValue = hdf_epsa.Value;
				//}
				//if (hdf_dsa.Value != "")
				//{
				//	ddl_dsa.SelectedValue = hdf_dsa.Value;
				//}

				if (ddl_da.SelectedIndex != 0 && drv["status_drawing"].ToString() != "") lb_statusDA.Text = drv["status_drawing"].ToString();
				else lb_statusDA.Text = "-";
				//if (ddl_epsa.SelectedIndex != 0 && drv["status_equip"].ToString() != "") lb_statusDPSA.Text = drv["status_equip"].ToString();
				//else lb_statusDPSA.Text = "-";
				//if (ddl_dsa.SelectedIndex != 0 && drv["status_tech_doc"].ToString() != "") lb_statusDSA.Text = drv["status_tech_doc"].ToString();
				//else lb_statusDSA.Text = "-";

				if (drv["status_activity"].ToString() == "WF in progress" || drv["status_activity"].ToString() == "FINISHED")
				{
					imgbt.Enabled = false;
					cb_sub.Enabled = false;
					ddl_da.Enabled = false;
					//ddl_epsa.Enabled = false;
					//ddl_dsa.Enabled = false;
					lblReq_dr.Visible = false;
					//lblReq_eq.Visible = false;
					//lblReq_doc.Visible = false;

				}
			}
		}
		private string[] GetRoutingApproval(string strRouteApproval)
		{
			return strRouteApproval.Split(new char[] { ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
		}
		protected void gvJobSumary_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header)
			{
			}
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_scheduleNo = (Label)e.Row.FindControl("lb_scheduleNo");
				lb_scheduleNo.Text = drv["schedule_no"].ToString();
				Label lb_scheduleDesc = (Label)e.Row.FindControl("lb_scheduleDesc");
				lb_scheduleDesc.Text = drv["schedule_name"].ToString();
				Label lb_jobNo = (Label)e.Row.FindControl("lb_jobNo");
				Label lb_wa = (Label)e.Row.FindControl("lb_wa");
				lb_wa.Text = drv["wa_url"].ToString();
				//lb_jobNo.Text = drv["job_no"].ToString();

				//if (ViewState["Jno"].ToString().IndexOf(drv["job_no"].ToString()) > -1)
				//            {
				//	//int  s = ViewState["Jno"].ToString().IndexOf(ViewState["Jno"].ToString());
				//	ViewState["Jno"] += "," + drv["job_no"].ToString();

				//	lb_jobNo.Text = "";
				//}
				//            else
				//            {
				//	//int  ss = ViewState["Jno"].ToString().IndexOf(ViewState["Jno"].ToString());
				//	ViewState["Jno"] += "," + drv["job_no"].ToString();
				//	lb_jobNo.Text = drv["job_no"].ToString();
				//}
				lb_jobNo.Text = drv["job_no"].ToString();
				Label lb_jobDesc = (Label)e.Row.FindControl("lb_jobDesc");
				lb_jobDesc.Text = drv["job_desc"].ToString();
				Label lb_revision = (Label)e.Row.FindControl("lb_revision");
				lb_revision.Text = drv["job_revision"].ToString();
				//Label lb_jobStatus = (Label)e.Row.FindControl("lb_jobStatus");
				//lb_jobStatus.Text = drv["schedule_status"].ToString();

			}
		}

		protected void gvaas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			//if (e.Row.RowType == DataControlRowType.Header)
			//{
			//	CheckBox cb_all = (CheckBox)e.Row.FindControl("cb_all");
			//	cb_all.Attributes.Add("onclick","javascript:clickAll("+cb_all.ClientID+");");
			//}
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_division = (Label)e.Row.FindControl("lb_division");
				lb_division.Text = drv["depart_position_name"].ToString();
				Label lb_depm = (Label)e.Row.FindControl("lb_depm");
				//lb_depm.Text = drv["section_position_name"].ToString();
				//Label lb_activity = (Label)e.Row.FindControl("lb_activity");
				//lb_activity.Text = drv["activity_name"].ToString();
				//Label lb_aas = (Label)e.Row.FindControl("lb_aas");
				//lb_aas.Text = drv["test4"].ToString();
				//DropDownList ddl_da = (DropDownList)e.Row.FindControl("ddl_da");
				//DropDownList ddl_epsa = (DropDownList)e.Row.FindControl("ddl_epsa");
				//DropDownList ddl_dsa = (DropDownList)e.Row.FindControl("ddl_dsa");

    //            EmpInfoClass empInfo = new EmpInfoClass();
    //            Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
    //            string[] staffList = empInfo.getEmpIDAndNameInDepartment(emp.ORGSNAME4);
    //            ddl_da.Items.Clear();
				//ddl_epsa.Items.Clear();
				//ddl_dsa.Items.Clear();
				//foreach (string staffInfo in staffList)
    //            {
    //                string[] staff = staffInfo.Split(':');

    //                ListItem item1 = new ListItem();
    //                item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
    //                item1.Value = staff[0].ToString();
    //                ddl_da.Items.Add(item1);
				//	ddl_epsa.Items.Clear();
				//	ddl_dsa.Items.Clear();

				//}
                clsBidDashbaord objdb = new clsBidDashbaord();
				DataTable dt = objdb.GetActivity(txt_bidNo.Text,txt_rev.Text);
				DataView dv = new DataView(dt);
				dv.RowFilter = "depart_position_name ='"+ lb_division.Text+ "'";
				GridView gv = (GridView)e.Row.FindControl("gvaasSub");

				if (dv.Count > 0)
				{
					gv.DataSource = dv;
					gv.DataBind();
					
				}

			}
		}

		protected void gv_biding_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType==DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_no = (Label)e.Row.FindControl("lb_no");
				int no = Convert.ToInt32(e.Row.RowIndex) + 1;
				lb_no.Text = no.ToString();
				Label lb_docType = (Label)e.Row.FindControl("lb_docType");
				lb_docType.Text = drv["doctype_name"].ToString();
				Label lb_docNo = (Label)e.Row.FindControl("lb_docNo");
				lb_docNo.Text = drv["doc_no"].ToString();
				Label lb_Desc = (Label)e.Row.FindControl("lb_Desc");
				lb_Desc.Text = drv["doc_desc"].ToString();
				Label lb_sheetNo = (Label)e.Row.FindControl("lb_sheetNo");
				lb_sheetNo.Text = drv["doc_sheetno"].ToString();
				Label lb_scheduleNo = (Label)e.Row.FindControl("lb_scheduleNo");
				lb_scheduleNo.Text = drv["schedule_no"].ToString();
				Label lb_scheduleName = (Label)e.Row.FindControl("lb_scheduleName");
				lb_scheduleName.Text = drv["schedule_name"].ToString();
				Label lb_status = (Label)e.Row.FindControl("lb_status");
				lb_status.Text = "";
				LinkButton lnk_viewDoc = (LinkButton)e.Row.FindControl("lnk_viewDoc");
				//lnk_viewDoc.Attributes.Add("href", drv["link_doc"].ToString());
				lnk_viewDoc.Text = " <a href=" + drv["link_doc"].ToString() + " TARGET='openWindow' > ViewDoc </a>";

			}
		}

		protected void gv_docOther_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_division = (Label)e.Row.FindControl("lb_division");
				Label lb_depm = (Label)e.Row.FindControl("lb_depm");
				Label lb_activity = (Label)e.Row.FindControl("lb_activity");
				Label lb_aas = (Label)e.Row.FindControl("lb_aas");
				Label lb_da = (Label)e.Row.FindControl("lb_da");
				Label lb_statusDA = (Label)e.Row.FindControl("lb_statusDA");
				Label lb_epsa = (Label)e.Row.FindControl("lb_epsa");
				Label lb_statusDPSA = (Label)e.Row.FindControl("lb_statusDPSA");
				Label lb_dsa = (Label)e.Row.FindControl("lb_dsa");
				Label lb_statusDSA = (Label)e.Row.FindControl("lb_statusDSA");

			}
		}

        protected void bt_assign_Click(object sender, EventArgs e)
		{
			if (hidchkSelect.Value != "") 
			{
				clsBidDashbaord objBid = new clsBidDashbaord();
				bool chkFirst = false;
				DataTable chkCount = objBid.CheckCountTransBid(txt_bidNo.Text, txt_rev.Text);
				if (chkCount.Rows.Count == 0) chkFirst = true;

				bool chkUpdate = true;
				int gvcount = 0;
				int count = gvaas.Rows.Count - 1;
				foreach (GridViewRow dgi2 in gvaas.Rows)
				{
					GridView gvSub = (GridView)dgi2.FindControl("gvaasSub");
					string[] str = gvSub.ClientID.Split('_');

					if (str.Length > 2)
					{
						if (str[2] != "")
						{
							gvcount = Convert.ToInt32(str[2]);
						}

					}
					foreach (GridViewRow gvr2 in gvSub.Rows)
					{
						CheckBox cb_sub2 = (CheckBox)gvr2.FindControl("cb_sub");
						DropDownList ddl_da = (DropDownList)gvr2.FindControl("ddl_da");
						DropDownList ddl_epsa = (DropDownList)gvr2.FindControl("ddl_epsa");
						DropDownList ddl_dsa = (DropDownList)gvr2.FindControl("ddl_dsa");
						Label lb_reqDraw = (Label)gvr2.FindControl("lb_reqDraw");
						Label lb_reqEqui = (Label)gvr2.FindControl("lb_reqEqui");
						Label lb_reqTechDoc = (Label)gvr2.FindControl("lb_reqTechDoc");
						//if (ddl_da.SelectedValue != "" || ddl_epsa.SelectedValue != "" || ddl_dsa.SelectedValue != "")
						//{
						if (cb_sub2.Checked)
						{
							foreach (GridViewRow gvr3 in gvSub.Rows)
							{
								DropDownList ddl_da1 = (DropDownList)gvr3.FindControl("ddl_da");
								DropDownList ddl_epsa1 = (DropDownList)gvr3.FindControl("ddl_epsa");
								DropDownList ddl_dsa1 = (DropDownList)gvr3.FindControl("ddl_dsa");
								Label lblReq_dr = (Label)gvr3.FindControl("lblReq_dr");
								Label lblReq_eq = (Label)gvr3.FindControl("lblReq_eq");
								Label lblReq_doc = (Label)gvr3.FindControl("lblReq_doc");
								CheckBox cb_sub3 = (CheckBox)gvr3.FindControl("cb_sub");
								if (cb_sub3.Checked)
								{
									if (ddl_da1.SelectedValue != "")//|| ddl_epsa1.SelectedValue != "" || ddl_dsa1.SelectedValue != ""
									{
										if ((lb_reqDraw.Text == "True" && (ddl_da1.SelectedValue == "" || ddl_da1.SelectedValue == null))) lblReq_dr.Visible = true;
										//else lblReq_dr.Visible = false;

										//if ((lb_reqEqui.Text == "True" && (ddl_epsa1.SelectedValue == "" || ddl_epsa1.SelectedValue == null))) lblReq_eq.Visible = true;
										////else lblReq_eq.Visible = false;

										//if ((lb_reqTechDoc.Text == "True" && (ddl_dsa1.SelectedValue == "" || ddl_dsa1.SelectedValue == null))) lblReq_doc.Visible = true;
										////else lblReq_doc.Visible = false;
									}
									else if (ddl_da1.SelectedValue == "")//&& ddl_epsa1.SelectedValue == "" && ddl_dsa1.SelectedValue == ""
									{
										if ((lb_reqDraw.Text == "True" && (ddl_da1.SelectedValue == "" || ddl_da1.SelectedValue == null))) lblReq_dr.Visible = true;

										//if ((lb_reqEqui.Text == "True" && (ddl_epsa1.SelectedValue == "" || ddl_epsa1.SelectedValue == null))) lblReq_eq.Visible = true;

										//if ((lb_reqTechDoc.Text == "True" && (ddl_dsa1.SelectedValue == "" || ddl_dsa1.SelectedValue == null))) lblReq_doc.Visible = true;
									}
									else
									{
										lblReq_dr.Visible = false;
										//lblReq_eq.Visible = false;
										//lblReq_doc.Visible = false;
									}
								}

							}


							if ((lb_reqDraw.Text == "True" && (ddl_da.SelectedValue == "" || ddl_da.SelectedValue == null))) //|| (lb_reqEqui.Text == "True" && (ddl_epsa.SelectedValue == "" || ddl_epsa.SelectedValue == null)) || (lb_reqTechDoc.Text == "True" && (ddl_dsa.SelectedValue == "" || ddl_dsa.SelectedValue == null))
							{
								chkUpdate = false;
								ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please select assign.');", true);
								//cb_sub2.Checked = false;
								break;
							}
							if ((ddl_da.SelectedValue == "" || ddl_da.SelectedValue == null))// && (ddl_epsa.SelectedValue == "" || ddl_epsa.SelectedValue == null) && (ddl_dsa.SelectedValue == "" || ddl_dsa.SelectedValue == null)
							{
								chkUpdate = false;
								ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please select assign.');", true);
								//cb_sub2.Checked = false;
								break;
							}
						}
					}
				}
				if (count == gvcount)
				{
					if (chkUpdate == true)
					{

						List<clsJobWf> lstWF = new List<clsJobWf>();
						clsWfPsAPV01 objWfPs = new clsWfPsAPV01();
						foreach (GridViewRow dgi in gvaas.Rows)
						{
							GridView gvSub = (GridView)dgi.FindControl("gvaasSub");
							foreach (GridViewRow gvr in gvSub.Rows)
							{
								CheckBox cb_sub = (CheckBox)gvr.FindControl("cb_sub");

								DropDownList ddl_da = (DropDownList)gvr.FindControl("ddl_da");
								DropDownList ddl_epsa = (DropDownList)gvr.FindControl("ddl_epsa");
								DropDownList ddl_dsa = (DropDownList)gvr.FindControl("ddl_dsa");
								Label lb_depmb = (Label)gvr.FindControl("lb_depm");
								Label lb_sec = (Label)gvr.FindControl("lb_sec");

								Label lb_activity = (Label)gvr.FindControl("lb_activity");
								Label lb_statusDA = (Label)gvr.FindControl("lb_statusDA");
								Label lb_statusDPSA = (Label)gvr.FindControl("lb_statusDPSA");
								Label lb_statusDSA = (Label)gvr.FindControl("lb_statusDSA");


								Label lb_reqDraw = (Label)gvr.FindControl("lb_reqDraw");
								Label lb_reqEqui = (Label)gvr.FindControl("lb_reqEqui");
								Label lb_reqTechDoc = (Label)gvr.FindControl("lb_reqTechDoc");
								Label lb_ActID = (Label)gvr.FindControl("lb_ActID");
								Label lb_nxtActID = (Label)gvr.FindControl("lb_nxtActID");
								Label lb_apprRout = (Label)gvr.FindControl("lb_apprRout");
								Label lb_emailNoti = (Label)gvr.FindControl("lb_emailNoti");
								Label lb_emailCont = (Label)gvr.FindControl("lb_emailCont");
								Label lb_bidNo = (Label)gvr.FindControl("lb_bidNo");
								Label lb_bidRev = (Label)gvr.FindControl("lb_bidRev");
								Label lbhidStatus = (Label)gvr.FindControl("lbhidStatus");

								Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
								Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
								Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
								Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
								Label lb_dept = (Label)gvr.FindControl("lb_dept");
								//clsBidDashbaord objdb = new clsBidDashbaord();
								//objdb.insertActivity(lb_depmb.Text, lb_sec.Text, lb_activity.Text, ddl_da.SelectedValue, ddl_epsa.SelectedValue,
								//ddl_dsa.SelectedValue, lb_statusDA.Text, lb_statusDPSA.Text, lb_statusDSA.Text, lb_reqDraw.Text,
								//lb_reqEqui.Text, lb_reqTechDoc.Text, lb_ActID.Text, lb_nxtActID.Text, lb_apprRout.Text, lb_emailNoti.Text,
								//lb_emailCont.Text, lb_bidNo.Text, lb_bidRev.Text, hidLogin.Value);

								if (chkFirst == true)
								{
									bidActivity objdb = new bidActivity();
									if (ddl_da.SelectedValue != "")//|| ddl_epsa.SelectedValue != "" || ddl_dsa.SelectedValue != ""
									{
										objdb.status = "ASSIGNED";
										lbhidStatus.Text = "ASSIGNED";
										//objdb.is_select = 1;
									}
									else
									{
										objdb.status = "";
										//objdb.is_select = 0;
									}
									string val = "";
									if (lb_headDept.Text != "")
									{
										val += "[" + lb_headDept.Text + "]";

									}
									if (lb_headSect.Text != "")
									{
										val += "[" + lb_headSect.Text + "]";
									}
									if (lb_headProject.Text != "")
									{
										val += "[" + lb_headProject.Text + "]";
									}
									if (lb_assiDept.Text != "")
									{
										val += "[" + lb_assiDept.Text + "]";
									}
									if (lb_dept.Text != "")
									{
										val += "[" + lb_dept.Text + "]";
									}
									if (lbhidStatus.Text == "ASSIGNED" || lbhidStatus.Text == "")
									{
										objdb.bid_no = lb_bidNo.Text;
										objdb.bid_revision = Convert.ToInt32(lb_bidRev.Text);
										objdb.depart_position_name = lb_depmb.Text;
										objdb.section_position_name = lb_sec.Text;
										objdb.activity_id = lb_ActID.Text;

										objdb.req_drawing = Convert.ToBoolean(lb_reqDraw.Text);
										objdb.req_bid_schedulename = true;
										objdb.assignee_drawing = ddl_da.SelectedValue;
										objdb.req_equip = Convert.ToBoolean(lb_reqEqui.Text);
										objdb.assignee_equip = ddl_da.SelectedValue; //ddl_epsa.SelectedValue;
										objdb.req_tech_doc = Convert.ToBoolean(lb_reqTechDoc.Text);
										objdb.assignee_tech_doc = ddl_da.SelectedValue; //ddl_dsa.SelectedValue;
										objdb.approval_routing = val;// lb_apprRout.Text;
										objdb.next_activity_id = lb_nxtActID.Text;
										if (lb_emailNoti.Text != "") objdb.email_noti_id = Convert.ToInt32(lb_emailNoti.Text); else objdb.email_noti_id = 0;
										objdb.created_by = hidLogin.Value;
										objdb.created_datetime = DateTime.Now;
										objdb.updated_by = hidLogin.Value;
										objdb.updated_datetime = DateTime.Now;

										if (hidDept.Value == lb_depmb.Text) objdb.chkDept = "1";
										else objdb.chkDept = "0";

										clsBidDashbaord objbid = new clsBidDashbaord();
										objbid.insertActivity(objdb);
										ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);

									}

								}
								if (cb_sub.Checked)
								{
									bidActivity objdb = new bidActivity();
									if (ddl_da.SelectedValue != "")//|| ddl_epsa.SelectedValue != "" || ddl_dsa.SelectedValue != ""
									{
										objdb.status = "ASSIGNED";
										lbhidStatus.Text = "ASSIGNED";
										//objdb.is_select = 1;
									}
									else
									{
										objdb.status = "";
										//objdb.is_select = 0;
									}
									string val = "";
									if (lb_headDept.Text != "")
									{
										val += "[" + lb_headDept.Text + "]";

									}
									if (lb_headSect.Text != "")
									{
										val += "[" + lb_headSect.Text + "]";
									}
									if (lb_headProject.Text != "")
									{
										val += "[" + lb_headProject.Text + "]";
									}
									if (lb_assiDept.Text != "")
									{
										val += "[" + lb_assiDept.Text + "]";
									}
									if (lb_dept.Text != "")
									{
										val += "[" + lb_dept.Text + "]";
									}
									if (lbhidStatus.Text == "ASSIGNED" || lbhidStatus.Text == "")
									{
										objdb.bid_no = lb_bidNo.Text;
										objdb.bid_revision = Convert.ToInt32(lb_bidRev.Text);
										objdb.depart_position_name = lb_depmb.Text;
										objdb.section_position_name = lb_sec.Text;
										objdb.activity_id = lb_ActID.Text;

										objdb.req_drawing = Convert.ToBoolean(lb_reqDraw.Text);
										objdb.req_bid_schedulename = true;
										objdb.assignee_drawing = ddl_da.SelectedValue;
										objdb.req_equip = Convert.ToBoolean(lb_reqEqui.Text);
										objdb.assignee_equip = ddl_da.SelectedValue; //ddl_epsa.SelectedValue;
										objdb.req_tech_doc = Convert.ToBoolean(lb_reqTechDoc.Text);
										objdb.assignee_tech_doc = ddl_da.SelectedValue; //ddl_dsa.SelectedValue;
										objdb.approval_routing = val;// lb_apprRout.Text;
										objdb.next_activity_id = lb_nxtActID.Text;
										if (lb_emailNoti.Text != "") objdb.email_noti_id = Convert.ToInt32(lb_emailNoti.Text); else objdb.email_noti_id = 0;
										objdb.created_by = hidLogin.Value;
										objdb.created_datetime = DateTime.Now;
										objdb.updated_by = hidLogin.Value;
										objdb.updated_datetime = DateTime.Now;

										if (hidDept.Value == lb_depmb.Text) objdb.chkDept = "1";
										else objdb.chkDept = "0";

										clsBidDashbaord objbid = new clsBidDashbaord();
										objbid.insertActivity(objdb);
										ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);

									}

								}


							}
						}
						//bind_gvAAS();
						//string strError = objWfPs.StartWf(lstWF);
						//string strError = objWfPs.StartWfBid(lstWF);
						//if (strError != "") ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('" + strError + "');", true);
						//else ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
						ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
						bind_gvAAS();
					}
				}
				hidchkSelect.Value = "";
			}
            else
            {
				ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select Assign');", true);
			}
			
		
		}

        protected void gv_MainDoc_RowDataBound1(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_division = (Label)e.Row.FindControl("lb_division");
				lb_division.Text = drv["depart_position_name"].ToString();

				clsBidDashbaord objdb = new clsBidDashbaord();
				DataTable dt = objdb.getDocDetail(txt_bidNo.Text);
				DataView dv = new DataView(dt);
				dv.RowFilter = "depart_position_name ='" + lb_division.Text + "'";
				GridView gv = (GridView)e.Row.FindControl("gv_biding");

				if (dv.Count > 0)
				{
					gv.DataSource = dv;
					gv.DataBind();

				}
			}
        }
		protected void imgbt_Click(object sender, ImageClickEventArgs e)
		{
			ImageButton img = sender as ImageButton;
			GridViewRow gvr = img.NamingContainer as GridViewRow;
			Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
			Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
			Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
			Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
			Label lb_dept = (Label)gvr.FindControl("lb_dept");
			string[] gvrow = gvr.ClientID.Split('_');
			if (gvrow.Length == 3)
			{
				int id = Convert.ToInt32(gvrow[2]) + 1;
				lb_rowindex.Text = gvr.RowIndex.ToString();
				hidExpDiv.Value = id.ToString();
				hidFindIndexM.Value = gvrow[2];
				hidFindIndexS.Value = img.ClientID.Substring(23, lb_rowindex.Text.Length);
				myModal.Style["display"] = "block";
				dvRounting.Style["display"] = "block";
				try { if (lb_headDept.Text != "") ddl_headDept.SelectedValue = lb_headDept.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
				try { if (lb_headSect.Text != "") ddl_headSect.SelectedValue = lb_headSect.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
				try { if (lb_headProject.Text != "") ddl_headProject.SelectedValue = lb_headProject.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
				try { if (lb_assiDept.Text != "") ddl_assiDept.SelectedValue = lb_assiDept.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
				try { if (lb_dept.Text != "") ddl_dept.SelectedValue = lb_dept.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
			}
		}
		protected void bt_save_Click(object sender, EventArgs e)
		{
			GridView gv = (GridView)gvaas.Rows[Convert.ToInt32(hidFindIndexM.Value)].FindControl("gvaasSub");
			if (gv != null)
			{
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_headDept")).Text = ddl_headDept.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_headSect")).Text = ddl_headSect.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_headProject")).Text = ddl_headProject.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_assiDept")).Text = ddl_assiDept.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_dept")).Text = ddl_dept.SelectedValue;
            }
			//string gv = ((Label)gv1.Rows[rowindex].FindControl("gv")).Text;
			ScriptManager.RegisterStartupScript(Page, GetType(), "divexpandcollapse", "<script>JavaScript:divexpandcollapse('div"+ hidExpDiv.Value + "')</script>", false);
			myModal.Style["display"] = "none";
			dvRounting.Style["display"] = "none";
			bind_rounting();
		}
		protected void bt_canc_Click(object sender, EventArgs e)
		{
			ScriptManager.RegisterStartupScript(Page, GetType(), "divexpandcollapse", "<script>JavaScript:divexpandcollapse('div" + hidExpDiv.Value + "')</script>", false);
			myModal.Style["display"] = "none";
			dvRounting.Style["display"] = "none";
			bind_rounting();
		}

        protected void gvAttach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
			//if (e.Row.RowType == DataControlRowType.DataRow)
			//{
			//	DataRowView drv = (DataRowView)e.Row.DataItem;
			//	LinkButton lnkAtt_view = (LinkButton)e.Row.FindControl("lnkAtt_view");
			//	Label lblAtt_date = (Label)e.Row.FindControl("lblAtt_date");
			//	lblAtt_date.Text = drv["upload_datetime"].ToString();
			//	lnkAtt_view.Text = drv["att_url"].ToString();
			//}
		}
		protected void rd_approval_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (rd_approval.SelectedItem.Value == "1")
			{
				td3.Visible = false;
				td3_3.Visible = false;
				lb_assiDept.Text = "Approve 3";
				lb_dept.Text = "Approve 4";
			}
			else if (rd_approval.SelectedItem.Value == "2")
			{
				td3.Visible = true;
				td3_3.Visible = true;
				td3.Controls.Add(lb_headProject);
				td3_3.Controls.Add(ddl_headProject);
				td4.Controls.Add(lb_assiDept);
				td4_4.Controls.Add(ddl_assiDept);
				td5.Controls.Add(lb_dept);
				td5_5.Controls.Add(ddl_dept);
				lb_assiDept.Text = "Approve 4";
				lb_dept.Text = "Approve 5";
			}
			else if (rd_approval.SelectedItem.Value == "3")
			{
				td3.Visible = true;
				td3_3.Visible = true;
				td3.Controls.Add(lb_assiDept);
				td3_3.Controls.Add(ddl_assiDept);
				td4.Controls.Add(lb_dept);
				td4_4.Controls.Add(ddl_dept);
				td5.Controls.Add(lb_headProject);
				td5_5.Controls.Add(ddl_headProject);
				lb_assiDept.Text = "Approve 3";
				lb_dept.Text = "Approve 4";
				lb_headProject.Text = "Approve 5";
			}
			bind_rounting();
		}
		protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
		{
			string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
			Response.Redirect(strUrl);
		}
		protected void btnSelect_Click(object sender, EventArgs e)
		{
			//kie16-02
			clsBidDashbaord objbid = new clsBidDashbaord();
			DataTable dtassign = objbid.AssignActivityReqBid(hidDept.Value, DDL_Template.SelectedValue);

			bool chkFirst = false;
			DataTable chkCount = objbid.CheckCountTransBid(txt_bidNo.Text, txt_rev.Text);
			if (chkCount.Rows.Count == 0) chkFirst = true;

			if (dtassign.Rows.Count > 0)
			{
				objbid.DeleteActivityBid(txt_bidNo.Text, txt_rev.Text, hidDept.Value, hidSect.Value);
				foreach (DataRow dr in dtassign.Rows)
				{
					string des = "";
					string equi = "";
					string doc = "";
					bidActivity objdb = new bidActivity();

					if (dr["depart_position_name"].ToString() == hidDept.Value)
					{
						if (dr["req_drawing"].ToString() == "True") des = DDL_DessignReq.SelectedValue;
						if (dr["req_equip"].ToString() == "True") equi = DDL_DessignReq.SelectedValue; //DDL_EqsignReq.SelectedValue;
						if (dr["req_tech_doc"].ToString() == "True") doc = DDL_DessignReq.SelectedValue; //DDL_DocsignReq.SelectedValue;
						objdb.chkDept = "1";
                        if (des !="" || equi !="" || doc !="") objdb.status = "NEW";
						else objdb.status = "";
					}
                    else
                    {
						objdb.chkDept = "";
						objdb.status = "";
					}
					
					objdb.bid_no = txt_bidNo.Text;
					objdb.bid_revision = Convert.ToInt32(txt_rev.Text);
					objdb.depart_position_name = hidDept.Value;
					objdb.section_position_name = hidSect.Value;
					objdb.activity_id = dr["activity_id"].ToString();

					objdb.req_drawing = Convert.ToBoolean(dr["req_drawing"].ToString());
					objdb.req_bid_schedulename = true;
					objdb.assignee_drawing = des;
					objdb.req_equip = Convert.ToBoolean(dr["req_equip"].ToString());
					objdb.assignee_equip = equi;
					objdb.req_tech_doc = Convert.ToBoolean(dr["req_tech_doc"].ToString());
					objdb.assignee_tech_doc = doc;
					objdb.approval_routing = dr["approval_routing"].ToString();
					objdb.next_activity_id = dr["next_activity_id"].ToString();
					if (dr["email_noti_id"].ToString() != "") objdb.email_noti_id = Convert.ToInt32(dr["email_noti_id"].ToString()); else objdb.email_noti_id = 0;
					objdb.created_by = hidLogin.Value;
					objdb.created_datetime = DateTime.Now;
					objdb.updated_by = hidLogin.Value;
					objdb.updated_datetime = DateTime.Now;

					objbid.AssignReqBid(objdb);

					foreach (GridViewRow dgi in gvaas.Rows)
					{
						bidActivity objdb2 = new bidActivity();
						GridView gvSub = (GridView)dgi.FindControl("gvaasSub");
						foreach (GridViewRow gvr in gvSub.Rows)
						{
							CheckBox cb_sub = (CheckBox)gvr.FindControl("cb_sub");


							Label lb_depmb = (Label)gvr.FindControl("lb_depm");
							Label lb_sec = (Label)gvr.FindControl("lb_sec");

							Label lb_activity = (Label)gvr.FindControl("lb_activity");
							Label lb_statusDA = (Label)gvr.FindControl("lb_statusDA");
							Label lb_statusDPSA = (Label)gvr.FindControl("lb_statusDPSA");
							Label lb_statusDSA = (Label)gvr.FindControl("lb_statusDSA");


							Label lb_reqDraw = (Label)gvr.FindControl("lb_reqDraw");
							Label lb_reqEqui = (Label)gvr.FindControl("lb_reqEqui");
							Label lb_reqTechDoc = (Label)gvr.FindControl("lb_reqTechDoc");
							Label lb_ActID = (Label)gvr.FindControl("lb_ActID");
							Label lb_nxtActID = (Label)gvr.FindControl("lb_nxtActID");
							Label lb_apprRout = (Label)gvr.FindControl("lb_apprRout");
							Label lb_emailNoti = (Label)gvr.FindControl("lb_emailNoti");
							Label lb_emailCont = (Label)gvr.FindControl("lb_emailCont");
							Label lb_bidNo = (Label)gvr.FindControl("lb_bidNo");
							Label lb_bidRev = (Label)gvr.FindControl("lb_bidRev");
							Label lbhidStatus = (Label)gvr.FindControl("lbhidStatus");

							Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
							Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
							Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
							Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
							Label lb_dept = (Label)gvr.FindControl("lb_dept");

							des = "";
							equi = "";
							doc = "";
                            if (chkFirst == true)
                            {
								string val = "";
								if (lb_headDept.Text != "")
								{
									val += "[" + lb_headDept.Text + "]";

								}
								if (lb_headSect.Text != "")
								{
									val += "[" + lb_headSect.Text + "]";
								}
								if (lb_headProject.Text != "")
								{
									val += "[" + lb_headProject.Text + "]";
								}
								if (lb_assiDept.Text != "")
								{
									val += "[" + lb_assiDept.Text + "]";
								}
								if (lb_dept.Text != "")
								{
									val += "[" + lb_dept.Text + "]";
								}
								objdb2.bid_no = lb_bidNo.Text;
								objdb2.bid_revision = Convert.ToInt32(lb_bidRev.Text);
								objdb2.depart_position_name = lb_depmb.Text;
								objdb2.section_position_name = lb_sec.Text;
								objdb2.activity_id = lb_ActID.Text;

								objdb2.req_drawing = Convert.ToBoolean(lb_reqDraw.Text);
								objdb2.req_bid_schedulename = true;
								objdb2.assignee_drawing = des;
								objdb2.req_equip = Convert.ToBoolean(lb_reqEqui.Text);
								objdb2.assignee_equip = equi;
								objdb2.req_tech_doc = Convert.ToBoolean(lb_reqTechDoc.Text);
								objdb2.assignee_tech_doc = doc;
								objdb2.approval_routing = val;// lb_apprRout.Text;
								objdb2.next_activity_id = lb_nxtActID.Text;
								if (lb_emailNoti.Text != "") objdb2.email_noti_id = Convert.ToInt32(lb_emailNoti.Text); else objdb2.email_noti_id = 0;
								objdb2.created_by = hidLogin.Value;
								objdb2.created_datetime = DateTime.Now;
								objdb2.updated_by = hidLogin.Value;
								objdb2.updated_datetime = DateTime.Now;
								objdb2.chkDept = "";
								objdb2.status = "";
								if (hidDept.Value == lb_depmb.Text)
								{
									//if (lbreqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
									//if (lbreqEq.Text == "True") equi = DDL_EqsignReq.SelectedValue;
									//if (lbreqDoc.Text == "True") doc = DDL_DocsignReq.SelectedValue;
									//chkDept = "1";
								}
								else
								{

									objbid.AssignReqBid(objdb2);
								}
							}
                            else
                            {
								if (lbhidStatus.Text != "WF in progress" && lbhidStatus.Text != "FINISHED" && lbhidStatus.Text != "ASSIGNED")//kie22-02
								{
									string val = "";
									if (lb_headDept.Text != "")
									{
										val += "[" + lb_headDept.Text + "]";

									}
									if (lb_headSect.Text != "")
									{
										val += "[" + lb_headSect.Text + "]";
									}
									if (lb_headProject.Text != "")
									{
										val += "[" + lb_headProject.Text + "]";
									}
									if (lb_assiDept.Text != "")
									{
										val += "[" + lb_assiDept.Text + "]";
									}
									if (lb_dept.Text != "")
									{
										val += "[" + lb_dept.Text + "]";
									}
									objdb2.bid_no = lb_bidNo.Text;
									objdb2.bid_revision = Convert.ToInt32(lb_bidRev.Text);
									objdb2.depart_position_name = lb_depmb.Text;
									objdb2.section_position_name = lb_sec.Text;
									objdb2.activity_id = lb_ActID.Text;

									objdb2.req_drawing = Convert.ToBoolean(lb_reqDraw.Text);
									objdb2.req_bid_schedulename = true;
									objdb2.assignee_drawing = des;
									objdb2.req_equip = Convert.ToBoolean(lb_reqEqui.Text);
									objdb2.assignee_equip = equi;
									objdb2.req_tech_doc = Convert.ToBoolean(lb_reqTechDoc.Text);
									objdb2.assignee_tech_doc = doc;
									objdb2.approval_routing = val;// lb_apprRout.Text;
									objdb2.next_activity_id = lb_nxtActID.Text;
									if (lb_emailNoti.Text != "") objdb2.email_noti_id = Convert.ToInt32(lb_emailNoti.Text); else objdb2.email_noti_id = 0;
									objdb2.created_by = hidLogin.Value;
									objdb2.created_datetime = DateTime.Now;
									objdb2.updated_by = hidLogin.Value;
									objdb2.updated_datetime = DateTime.Now;
									objdb2.chkDept = "";
									objdb2.status = "";
									if (hidDept.Value == lb_depmb.Text)
									{
										//if (lbreqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
										//if (lbreqEq.Text == "True") equi = DDL_EqsignReq.SelectedValue;
										//if (lbreqDoc.Text == "True") doc = DDL_DocsignReq.SelectedValue;
										//chkDept = "1";
									}
									else
									{

										objbid.AssignReqBid(objdb2);
									}
								}
							}
                         

							
						}
					}

				}
			}
			else
			{
				foreach (GridViewRow dgi in gvaas.Rows)
				{
					bidActivity objdb2 = new bidActivity();
					GridView gvSub = (GridView)dgi.FindControl("gvaasSub");
					foreach (GridViewRow gvr in gvSub.Rows)
					{
						CheckBox cb_sub = (CheckBox)gvr.FindControl("cb_sub");


						Label lb_depmb = (Label)gvr.FindControl("lb_depm");
						Label lb_sec = (Label)gvr.FindControl("lb_sec");

						Label lb_activity = (Label)gvr.FindControl("lb_activity");
						Label lb_statusDA = (Label)gvr.FindControl("lb_statusDA");
						Label lb_statusDPSA = (Label)gvr.FindControl("lb_statusDPSA");
						Label lb_statusDSA = (Label)gvr.FindControl("lb_statusDSA");


						Label lb_reqDraw = (Label)gvr.FindControl("lb_reqDraw");
						Label lb_reqEqui = (Label)gvr.FindControl("lb_reqEqui");
						Label lb_reqTechDoc = (Label)gvr.FindControl("lb_reqTechDoc");
						Label lb_ActID = (Label)gvr.FindControl("lb_ActID");
						Label lb_nxtActID = (Label)gvr.FindControl("lb_nxtActID");
						Label lb_apprRout = (Label)gvr.FindControl("lb_apprRout");
						Label lb_emailNoti = (Label)gvr.FindControl("lb_emailNoti");
						Label lb_emailCont = (Label)gvr.FindControl("lb_emailCont");
						Label lb_bidNo = (Label)gvr.FindControl("lb_bidNo");
						Label lb_bidRev = (Label)gvr.FindControl("lb_bidRev");
						Label lbhidStatus = (Label)gvr.FindControl("lbhidStatus");

						Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
						Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
						Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
						Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
						Label lb_dept = (Label)gvr.FindControl("lb_dept");

						string des = "";
						string equi = "";
						string doc = "";
                        if (chkFirst == true)
                        {
							string val = "";
							if (lb_headDept.Text != "")
							{
								val += "[" + lb_headDept.Text + "]";

							}
							if (lb_headSect.Text != "")
							{
								val += "[" + lb_headSect.Text + "]";
							}
							if (lb_headProject.Text != "")
							{
								val += "[" + lb_headProject.Text + "]";
							}
							if (lb_assiDept.Text != "")
							{
								val += "[" + lb_assiDept.Text + "]";
							}
							if (lb_dept.Text != "")
							{
								val += "[" + lb_dept.Text + "]";
							}

							if (hidDept.Value == lb_depmb.Text)
							{
								if (lb_reqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
								if (lb_reqEqui.Text == "True") equi = DDL_DessignReq.SelectedValue; //DDL_EqsignReq.SelectedValue;
								if (lb_reqTechDoc.Text == "True") doc = DDL_DessignReq.SelectedValue; //DDL_DocsignReq.SelectedValue;
								objdb2.chkDept = "1";
								if (des != "" || equi != "" || doc != "") objdb2.status = "NEW";
								else objdb2.status = "";
							}
							else
							{
								objdb2.chkDept = "";
								objdb2.status = "";
							}
							objdb2.bid_no = lb_bidNo.Text;
							objdb2.bid_revision = Convert.ToInt32(lb_bidRev.Text);
							objdb2.depart_position_name = lb_depmb.Text;
							objdb2.section_position_name = lb_sec.Text;
							objdb2.activity_id = lb_ActID.Text;

							objdb2.req_drawing = Convert.ToBoolean(lb_reqDraw.Text);
							objdb2.req_bid_schedulename = true;
							objdb2.assignee_drawing = des;
							objdb2.req_equip = Convert.ToBoolean(lb_reqEqui.Text);
							objdb2.assignee_equip = equi;
							objdb2.req_tech_doc = Convert.ToBoolean(lb_reqTechDoc.Text);
							objdb2.assignee_tech_doc = doc;
							objdb2.approval_routing = val;// lb_apprRout.Text;
							objdb2.next_activity_id = lb_nxtActID.Text;
							if (lb_emailNoti.Text != "") objdb2.email_noti_id = Convert.ToInt32(lb_emailNoti.Text); else objdb2.email_noti_id = 0;
							objdb2.created_by = hidLogin.Value;
							objdb2.created_datetime = DateTime.Now;
							objdb2.updated_by = hidLogin.Value;
							objdb2.updated_datetime = DateTime.Now;

							objbid.AssignReqBid(objdb2);
						}
                        else
                        {
							if (lbhidStatus.Text != "WF in progress" && lbhidStatus.Text != "FINISHED" && lbhidStatus.Text != "ASSIGNED")
							{
								string val = "";
								if (lb_headDept.Text != "")
								{
									val += "[" + lb_headDept.Text + "]";

								}
								if (lb_headSect.Text != "")
								{
									val += "[" + lb_headSect.Text + "]";
								}
								if (lb_headProject.Text != "")
								{
									val += "[" + lb_headProject.Text + "]";
								}
								if (lb_assiDept.Text != "")
								{
									val += "[" + lb_assiDept.Text + "]";
								}
								if (lb_dept.Text != "")
								{
									val += "[" + lb_dept.Text + "]";
								}

								if (hidDept.Value == lb_depmb.Text)
								{
									if (lb_reqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
									if (lb_reqEqui.Text == "True") equi = DDL_DessignReq.SelectedValue; //DDL_EqsignReq.SelectedValue;
									if (lb_reqTechDoc.Text == "True") doc = DDL_DessignReq.SelectedValue; //DDL_DocsignReq.SelectedValue;
									objdb2.chkDept = "1";
									if (des != "" || equi != "" || doc != "") objdb2.status = "NEW";
									else objdb2.status = "";
								}
								else
								{
									objdb2.chkDept = "";
									objdb2.status = "";
								}
								objdb2.bid_no = lb_bidNo.Text;
								objdb2.bid_revision = Convert.ToInt32(lb_bidRev.Text);
								objdb2.depart_position_name = lb_depmb.Text;
								objdb2.section_position_name = lb_sec.Text;
								objdb2.activity_id = lb_ActID.Text;

								objdb2.req_drawing = Convert.ToBoolean(lb_reqDraw.Text);
								objdb2.req_bid_schedulename = true;
								objdb2.assignee_drawing = des;
								objdb2.req_equip = Convert.ToBoolean(lb_reqEqui.Text);
								objdb2.assignee_equip = equi;
								objdb2.req_tech_doc = Convert.ToBoolean(lb_reqTechDoc.Text);
								objdb2.assignee_tech_doc = doc;
								objdb2.approval_routing = val;// lb_apprRout.Text;
								objdb2.next_activity_id = lb_nxtActID.Text;
								if (lb_emailNoti.Text != "") objdb2.email_noti_id = Convert.ToInt32(lb_emailNoti.Text); else objdb2.email_noti_id = 0;
								objdb2.created_by = hidLogin.Value;
								objdb2.created_datetime = DateTime.Now;
								objdb2.updated_by = hidLogin.Value;
								objdb2.updated_datetime = DateTime.Now;

								objbid.AssignReqBid(objdb2);
							}

						}
					}
				}
			}

			ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
			bind_gvAAS();
		}

        protected void btnExecl_Click(object sender, EventArgs e)
		{
			List<string> listFileName =  new List<string>();
			string listName = "";
			clsBidDashbaord objbid = new clsBidDashbaord();
			cExcel xec = new cExcel();
			DataTable dt = objbid.GetSummary(txt_bidNo.Text);
			bool chkbid = false;
			if (dt.Rows.Count > 0)
			{
				string path0 = @"c:\Xls";
                string[] chkdirs = Directory.GetDirectories(@"c:\", "Xls*", SearchOption.TopDirectoryOnly);
                if (chkdirs.Length > 0)
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(path0);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }

                }

                string path1 = @"c:\PDF";
				string path2 = @"c:\PDF\All_Equipment";
				string path3 = @"c:\PDF\All_Equipment\Equipment.zip";
				DirectoryInfo di1 = Directory.CreateDirectory(path1);
				DirectoryInfo di0 = Directory.CreateDirectory(path0);

				foreach (DataRow dr in dt.Rows)
				{
					if (dr["schedule_name"].ToString() != "")
					{
						chkbid = true;
						string repSch = dr["schedule_name"].ToString().Replace("/", "_");
						string filename = dr["bid_no"].ToString() + "-"+ dr["schedule_no"].ToString() + " (" +repSch +")";
						listName = xec.genExcel(filename, hidDept.Value, hidSect.Value, dr["bid_no"].ToString(), dr["schedule_no"].ToString(), dr["job_no"].ToString(), dr["job_revision"].ToString(), dr["schedule_name"].ToString(), dr["bid_revision"].ToString(), "E");
                    }
                    else
					{
						string filename = dr["bid_no"].ToString() + "-" + dr["job_no"].ToString();
						listName = xec.genExcel(filename, hidDept.Value, hidSect.Value, dr["bid_no"].ToString(), dr["schedule_no"].ToString(), dr["job_no"].ToString(), dr["job_revision"].ToString(), dr["schedule_name"].ToString(), dr["bid_revision"].ToString(), "S");
					}
					listFileName.Add(listName);
				}
				if (chkbid == true) xec.genExcelSumTotal(hidBidNo.Value, listFileName);
				else xec.genExcelDelivery(txt_bidNo.Text, hidDept.Value, txt_bidNo.Text, txt_rev.Text, "S");
				string[] dirs = Directory.GetDirectories(@"c:\PDF", "All*", SearchOption.TopDirectoryOnly);
				if (dirs.Length > 0)
				{
					var diraa = new DirectoryInfo(dirs[0]);
					diraa.Delete(true);
				}
				DirectoryInfo di2 = Directory.CreateDirectory(path2);
				string[] filePaths = Directory.GetFiles(@"c:\Xls", "*.*", SearchOption.TopDirectoryOnly);
                if (filePaths.Length > 0)
                {
					ZipFile.CreateFromDirectory(path0, path3, CompressionLevel.Optimal, false);
					Response.ContentType = "Application/zip";
					Response.AppendHeader("Content-Disposition", "attachment; filename=Equipment.zip");
					Response.TransmitFile("C:\\PDF\\All_Equipment\\Equipment.zip");
					Response.Flush();
					Response.End();
                }
                else
                {
					ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Excel is not creating');", true);
				}
			}

		}

        protected void lnkActivity_Click(object sender, EventArgs e)
        { 
			clsBidDashbaord objbid = new clsBidDashbaord();
			dvViewActivity.Style["display"] = "block";
			LinkButton lnk = sender as LinkButton;
			GridViewRow gvr = lnk.NamingContainer as GridViewRow;
			string lb_ActID = ((Label)gvr.FindControl("lb_ActID")).Text;
			string lb_activity = ((Label)gvr.FindControl("lb_activity")).Text;
			DataTable dtbefore = objbid.ViewBeforeActivity(hidBidNo.Value, txt_rev.Text, lb_ActID);
			DataTable dtnext = objbid.ViewNextActivity(hidBidNo.Value, txt_rev.Text, lb_ActID);
			string before = "";
			string next = "";
			DataTable dtlogbefore = new DataTable();
			DataTable dtlognext = new DataTable();
			DataTable dtlognow = new DataTable();
			if (dtbefore.Rows.Count > 0)
			{
				DataTable dtlog = new DataTable();
				dtlog.Columns.Add("apv_order");
				dtlog.Columns.Add("emp_id");
				dtlog.Columns.Add("emp_name");
				dtlog.Columns.Add("emp_position");
				dtlog.Columns.Add("emp_apvdate");
				dtlog.Columns.Add("apv_status");
				dtlog.Columns.Add("ps_subject");
				dtlog.Columns.Add("process_id");
				foreach (DataRow dr in dtbefore.Rows)
				{
					before += (before == "" ? "" : ",") + "- " + dr["activity_name"].ToString();
					//acbefore += (acbefore == "" ? "" : "|") + dr["activity_id"].ToString();
				 dtlogbefore = objbid.ViewLogActivity(hidBidNo.Value, txt_rev.Text, dr["activity_id"].ToString(), dtlog);
				}	
				lblBefore.Text = before.Replace(",", "<br/>");
				dtlogbefore.DefaultView.Sort = "process_id,apv_order asc";
				gvBefore.DataSource = dtlogbefore;
				gvBefore.DataBind();
				lblBefore.Visible = true;
				Image1.Visible = true;
			}
			else
			{
				lblBefore.Visible = false;
				Image1.Visible = false;
				gvBefore.DataSource = null;
				gvBefore.DataBind();
			}
			if (dtnext.Rows.Count > 0)
			{
				DataTable dtlog = new DataTable();
				dtlog.Columns.Add("apv_order");
				dtlog.Columns.Add("emp_id");
				dtlog.Columns.Add("emp_name");
				dtlog.Columns.Add("emp_position");
				dtlog.Columns.Add("emp_apvdate");
				dtlog.Columns.Add("apv_status");
				dtlog.Columns.Add("ps_subject");
				dtlog.Columns.Add("process_id");
				foreach (DataRow dr in dtnext.Rows)
				{
					next += (next == "" ? "" : ",") + "- " + dr["activity_name"].ToString();
					dtlognext = objbid.ViewLogActivity(hidBidNo.Value, txt_rev.Text, dr["activity_id"].ToString(), dtlog);
				}
				lblNext.Text = next.Replace(",", "<br/>");
				dtlognext.DefaultView.Sort = "process_id,apv_order asc";
				gvNext.DataSource = dtlognext;
				gvNext.DataBind();
				lblNext.Visible = true;
				Image2.Visible = true;
			}
			else
			{
				lblNext.Visible = false;
				Image2.Visible = false;
				gvNext.DataSource = null;
				gvNext.DataBind();
			}
			DataTable dtlogNow = new DataTable();
			dtlogNow.Columns.Add("apv_order");
			dtlogNow.Columns.Add("emp_id");
			dtlogNow.Columns.Add("emp_name");
			dtlogNow.Columns.Add("emp_position");
			dtlogNow.Columns.Add("emp_apvdate");
			dtlogNow.Columns.Add("apv_status");
			dtlogNow.Columns.Add("ps_subject");
			dtlogNow.Columns.Add("process_id");
			lblNow.Text = "- "+ lb_activity;
			dtlogNow = objbid.ViewLogActivity(hidBidNo.Value, txt_rev.Text, lb_ActID, dtlogNow);
			dtlogNow.DefaultView.Sort = "process_id,apv_order asc";
			gvNow.DataSource = dtlogNow;
			gvNow.DataBind();



		}
		//protected void btnCloseView_Click(object sender, EventArgs e)
		//{
		//	dvViewActivity.Style["display"] = "none";
		//}
	}
}