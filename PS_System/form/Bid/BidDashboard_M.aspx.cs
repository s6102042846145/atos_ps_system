﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.Bid
{
    public partial class BidDashboard_M : System.Web.UI.Page
    {
        clsWfPsAPV01 objWf = new clsWfPsAPV01();
        clsBidDashboard_M objbidm = new clsBidDashboard_M();
        protected void Page_Load(object sender, EventArgs e)
        {
			if (!this.IsPostBack)
			{
				if (Request.QueryString["zuserlogin"] != null)
					hidLogin.Value = Request.QueryString["zuserlogin"];
				else
					hidLogin.Value = "z594900";

				if (Request.QueryString["bidno"] != null)
					hidBidNo.Value = Request.QueryString["bidno"];
				else
					hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";

                hidFix.Value = "0";
                EmpInfoClass empInfo = new EmpInfoClass();
				Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
				hidDept.Value = emp.ORGSNAME4;
				hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;

                bind_detail();
				bind_bidDashboard();
				bindAllTab();
				bind_equipment();
                bindDDL();

            }
			else
			{
				if (hidTab.Value == "1")
				{
					bind_equipment();
					dvAllTab.Style["display"] = "block";
					dvhidEq.Style["display"] = "block";
                }
                else
                {
                    bind_equipment();
                }
			}
		}
        private void bindDDL()
        {
            ddlDept.Items.Clear();
            DataTable dt = objbidm.getDDLDept();
            ddlDept.DataSource = dt;
            ddlDept.DataTextField = "depart_position_name";
            ddlDept.DataValueField = "depart_position_name";
            ddlDept.DataBind();

            ddlDoctype.Items.Clear();
            DataTable dt2 = objbidm.getDoctype();
            ddlDoctype.DataSource = dt2;
            ddlDoctype.DataTextField = "doctype_name";
            ddlDoctype.DataValueField = "doctype_code";
            ddlDoctype.DataBind();

            ddlPart.Items.Clear();
            DataTable dtPart = objbidm.getPart(hidBidNo.Value,hidBidRev.Value);
            DataTable dtPart2 = dtPart.DefaultView.ToTable(true, "part_name");
            ddlPart.DataSource = dtPart2;
            ddlPart.DataTextField = "part_name";
            ddlPart.DataValueField = "part_name";
            ddlPart.DataBind();

            ddlDept.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlDoctype.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlPart.Items.Insert(0, new ListItem("<-- Select -->", ""));
        }
		private void bind_detail()
		{
			DataTable dt = new DataTable();

			clsBidDashbaord objdb = new clsBidDashbaord();
			dt = objdb.GetDetail(hidBidNo.Value);
			if (dt.Rows.Count > 0)
			{
				txt_bidNo.Text = dt.Rows[0]["bid_no"].ToString();
				txt_rev.Text = dt.Rows[0]["bid_revision"].ToString();
				txt_desc.Text = dt.Rows[0]["bid_desc"].ToString();
                DateTime dtSta = new DateTime();
                DateTime dtEnd = new DateTime();

                if (dt.Rows[0]["p6_plan_start"].ToString() != "" && dt.Rows[0]["p6_plan_start"].ToString() != null)
                {
                    dtSta = Convert.ToDateTime(dt.Rows[0]["p6_plan_start"].ToString());
                    txtStartDate.Text = dtSta.ToString("dd/MM/yyyy");
                }
                if (dt.Rows[0]["p6_plan_end"].ToString() != "" && dt.Rows[0]["p6_plan_end"].ToString() != null)
                {
                    dtEnd = Convert.ToDateTime(dt.Rows[0]["p6_plan_end"].ToString());
                    txtEndDate.Text = dtEnd.ToString("dd/MM/yyyy");
                }

				if (dt.Rows[0]["completion_date"].ToString() != "") { lblCom.Text = dt.Rows[0]["completion_date"].ToString(); dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;"); }
				else dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
				txtRemark.Text = dt.Rows[0]["remark"].ToString();
			}
			else
			{
				dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
			}

			dt = objdb.getAttachWAbid(hidBidNo.Value);
			if (dt.Rows.Count > 0)
			{
				gvAttach.DataSource = dt;
				gvAttach.DataBind();
			}
			else
			{
				gvAttach.DataSource = null;
				gvAttach.DataBind();
			}

            clsBidDashbaord objdbid = new clsBidDashbaord();
            DataTable dtbid = objdbid.GetAct(txt_bidNo.Text, txt_rev.Text);
            if (dtbid.Rows.Count > 0)
            {
                gvaas.DataSource = dtbid;
                gvaas.DataBind();
            }
            else
            {
                gvaas.DataSource = null;
                gvaas.DataBind();
            }
        }
		private void bind_bidDashboard()
		{
			clsBidDashbaord objdb = new clsBidDashbaord();
			DataTable dt = objdb.GetSummary(txt_bidNo.Text);
			if (dt.Rows.Count > 0)
			{
				gvJobSumary.DataSource = dt;
				gvJobSumary.DataBind();
				gvJobSumary.UseAccessibleHeader = true;
				gvJobSumary.HeaderRow.TableSection = TableRowSection.TableHeader;
			}
			else
			{
				gvJobSumary.DataSource = null;
				gvJobSumary.DataBind();
			}


		}
        protected void bindAllTab()
        {
            //DataTable dtDraw = objWf.GetDrawing(hidBidNo.Value, "draw", hidBidRev.Value,"bid");
            //if (dtDraw.Rows.Count > 0)
            //{
            //    gvDrawing.DataSource = dtDraw;
            //    gvDrawing.DataBind();
            //}
            //else
            //{
            //    gvDrawing.DataSource = null;
            //    gvDrawing.DataBind();
            //}

            DataTable dtAll = objbidm.getDoctype();
            if (dtAll.Rows.Count > 0)
            {
                gvAllDoc.DataSource = dtAll;
                gvAllDoc.DataBind();
            }
            else
            {
                gvAllDoc.DataSource = null;
                gvAllDoc.DataBind();
            }
            DataTable dt = objbidm.GetDataAllDoc(hidBidNo.Value, "", hidBidRev.Value, "bid");
            DataView dv = new DataView(dt);
            dv.Sort = "order_no asc";
            if (dv.Count > 0)
            {
             
                gvTaskDoc2.DataSource = dv;
                gvTaskDoc2.DataBind();
            }
            else
            {
                gvTaskDoc2.DataSource = null;
                gvTaskDoc2.DataBind();
            }

       
            bind_equipment();
        }
        protected void bind_equipment()
        {
            DataTable dt = new DataTable();
            string mode = "";
            dt = objWf.GetSummary(hidBidNo.Value); mode = "bid";
            string schName = "";
            ultab.Controls.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                Button bt_part = new Button();
                if (dt.Rows[i]["bid_no"].ToString() != "Bid Supply" && dt.Rows[i]["bid_no"].ToString().Length > 10) bt_part.Text = dt.Rows[i]["bid_no"].ToString().Substring(0, 10) + "...";
                else bt_part.Text = dt.Rows[i]["job_no"].ToString();
                bt_part.ID = "bt_tab" + i.ToString();
                bt_part.ToolTip = dt.Rows[i]["job_no"].ToString() + " " + dt.Rows[i]["schedule_name"].ToString();
                bt_part.Width = 100;
                bt_part.CommandName = dt.Rows[i]["schedule_name"].ToString();
                bt_part.Attributes["runat"] = "server";
                if (hidChk.Value != "")
                {
                    if (dt.Rows[i]["job_no"].ToString() == hidClick.Value)
                    {
                        schName = dt.Rows[i]["schedule_name"].ToString();
                        bt_part.CssClass = "btnSel";
                    }
                    else if (hidClick.Value == "" && i == 0)
                    {
                        bt_part.CssClass = "btnSel";
                        schName = dt.Rows[i]["schedule_name"].ToString();
                    }
                    else
                    {
                        bt_part.CssClass = "btnclass";
                    }
                  
                }
                else
                {
                
                    if (i == 0)
                    {
                        bt_part.CssClass = "btnSel";
                        schName = dt.Rows[i]["schedule_name"].ToString();
                        hidClick.Value = dt.Rows[i]["job_no"].ToString();

                    }
                    else
                    {
                        bt_part.CssClass = "btnclass";

                    }
                }
               
                bt_part.Click += new EventHandler(selTab);
                li.Controls.Add(bt_part);
                ultab.Controls.Add(li);
              
            }
            //if (hidJobNo.Value != "") { dtpart = objWf.getBtnPaths_job(hidJobNo.Value); mode = "job"; }
            //else if (hidBidNo.Value != "") { dtpart = objWf.getBtnPaths_bid(hidBidNo.Value); mode = "bid"; }
            DataTable dtgv = new DataTable();
            dtgv = objbidm.getGVSelPart(hidBidNo.Value, hidBidRev.Value, hidClick.Value, hidJobRev.Value, mode, schName);
            if (hidChk.Value != "")
            {   
                string filter = "";
                DataView dv = new DataView(dtgv);
                if (ddlPart.SelectedValue != "") filter = string.Format("(path_code = '{0}')", ddlPart.SelectedValue);
                if (hidClick.Value != "")
                {
                    if (filter != "") filter = string.Format("{0} and ({1})", filter, "job_no ='" + hidClick.Value + "'");
                    else filter = string.Format("(job_no = '{0}')", hidClick.Value);

                }
              
                dv.RowFilter = filter;
                if (dv.Count > 0)
                {
                    gvAllEqui.DataSource = dv;
                    gvAllEqui.DataBind();

                }
                else
                {
                    gvAllEqui.DataSource = null;
                    gvAllEqui.DataBind();
                }
            }
            else
            {
                if (dtgv.Rows.Count > 0)
                {
                    gvAllEqui.DataSource = dtgv;
                    gvAllEqui.DataBind();

                }
                else
                {
                    gvAllEqui.DataSource = null;
                    gvAllEqui.DataBind();
                }
            }
            dvhidDoc.Style["display"] = "none";
        }
        protected void selTab(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            Button bt = sender as Button;
            string schName = "";
            string a = bt.ID.Substring(bt.ID.IndexOf(bt.ID) + bt.ID.Length - 1);
            schName = bt.CommandName;
            string mode = "";
           dt = objWf.GetSummary(hidBidNo.Value); mode = "bid"; 
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = "bt_tab" + i;
                Button btn = (Button)this.FindControl(id);
                if (a == i.ToString())
                {
                    btn.CssClass = "btnSel";
                    hidClick.Value = dt.Rows[i]["job_no"].ToString();
                }
                else
                {
                    btn.CssClass = "btnclass";
                }
         
            }
            DataTable dtgv = new DataTable();
            dtgv = objbidm.getGVSelPart(hidBidNo.Value, hidBidRev.Value, hidClick.Value, hidJobRev.Value, mode, schName);
            if (dtgv.Rows.Count > 0)
            {
                gvAllEqui.DataSource = dtgv;
                gvAllEqui.DataBind();
            }
            else
            {
                gvAllEqui.DataSource = null;
                gvAllEqui.DataBind();

            }
        }
        protected void gvAllDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                GridView gv = (GridView)e.Row.FindControl("gvTaskDoc");
                string doccode = ((Label)e.Row.FindControl("lbDocumentcode")).Text;
                DataTable dt = objbidm.getDataDoc(hidBidNo.Value, hidBidRev.Value, doccode);
                if (hidFilter.Value != "")
                {
                    DataView dv = new DataView(dt);
                    dv.RowFilter = hidFilter.Value;
                    if (dv.Count > 0)
                    {
                      
                        gv.DataSource = dv;
                        gv.DataBind();

                    }
                }
                else
                {
                    if (dt.Rows.Count > 0)
                    {
                        gv.DataSource = dt;
                        gv.DataBind();

                    }
                }
            }
        }
        protected void gvDrawing_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton link_DrawDoc = (LinkButton)e.Row.FindControl("link_DrawDoc");
                link_DrawDoc.Text = " <a href='" + drv["link_doc"].ToString() + "' target='_blank'> ViewDoc </a>";
            }
        }
        protected void gvTaskDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow row = e.Row;
                GridView gvTaskDoc = (GridView)row.NamingContainer;
                CheckBox cbAllverify = (CheckBox)e.Row.FindControl("cbAllverify");
                cbAllverify.Attributes.Add("onclick", "javascript:chkAllTaskDoc('" + cbAllverify.ClientID + "' ,'" + gvTaskDoc.ClientID + "');");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton link_DrawDoc = (LinkButton)e.Row.FindControl("link_DrawDoc");
                CheckBox cbVerify = (CheckBox)e.Row.FindControl("cbVerify");
                Label lbverify = (Label)e.Row.FindControl("lbverify");
                Label lbVerifyby = (Label)e.Row.FindControl("lbVerifyby");
                Label lbNotverify = (Label)e.Row.FindControl("lbNotverify");
                Label lbNotVerifyby = (Label)e.Row.FindControl("lbNotVerifyby");

                EmpInfoClass empInfo = new EmpInfoClass();
         
                if (drv["doc_verify"].ToString() == "1")
                {
                    Employee emp = empInfo.getInFoByEmpID(drv["doc_verify_by"].ToString());
                    string verify_name = emp.SNAME;
                    verify_name = "Verified by" + "<Br/>" + verify_name;
                    lbVerifyby.Text = verify_name;
                    lbVerifyby.Visible = true;
                    lbverify.Visible = true;
                    cbVerify.Checked = true;
         
                }
                else
                {
                    lbVerifyby.Visible = false;
                    lbverify.Visible = false;
                    cbVerify.Checked = false;
                }
                if (drv["tor_verify"].ToString() == "1")
                {
                    Employee emp = empInfo.getInFoByEmpID(drv["tor_verify_by"].ToString());
                    string verify_name = emp.SNAME;
                    verify_name = "Verified by" + "<Br/>" + verify_name;
                    lbNotVerifyby.Text = verify_name;
                    lbNotVerifyby.Visible = true;
                    lbNotverify.Visible = true;

                }
                else
                {
                    lbNotVerifyby.Visible = false;
                    lbNotverify.Visible = false;
                }
                link_DrawDoc.Text = " <a href='" + drv["link_doc"].ToString() + "' target='_blank'> ViewDoc </a>";
            }
        }
        protected void gvAllEqui_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lb_meterialGrp = (Label)e.Row.FindControl("lb_meterialGrp");
                lb_meterialGrp.Text = drv["equip_mas_grp"].ToString();
                Label lb_codeNo = (Label)e.Row.FindControl("lb_codeNo");
                lb_codeNo.Text = drv["equip_code"].ToString();
                Label lb_itmNo = (Label)e.Row.FindControl("lb_itmNo");
                lb_itmNo.Text = drv["equip_item_no"].ToString();
                Label lb_fDesc = (Label)e.Row.FindControl("lb_fDesc");
                Label lb_fDesc2 = (Label)e.Row.FindControl("lb_fDesc2");
                lb_fDesc2.Text = drv["equip_full_desc"].ToString();
                lb_fDesc.Text = drv["equip_full_desc"].ToString();
                if (lb_fDesc.Text.Length > 30)
                {
                    lb_fDesc.Text = lb_fDesc.Text.Substring(0, 30) + "...";
                    lb_fDesc.ToolTip = drv["equip_full_desc"].ToString();
                }

                Label lb_addDesc = (Label)e.Row.FindControl("lb_addDesc");
                lb_addDesc.Text = drv["equip_add_desc"].ToString() + " " + drv["equip_standard_drawing"].ToString();
                if (lb_addDesc.Text.Length > 30)
                {
                    lb_addDesc.Text = lb_addDesc.Text.ToString().Substring(0, 30) + "...";
                    lb_addDesc.ToolTip = drv["equip_add_desc"].ToString() + " " + drv["equip_standard_drawing"].ToString();
                }
                //TextBox tb_stdDr = (TextBox)e.Row.FindControl("tb_stdDr");
                //tb_stdDr.Text = drv["equip_standard_drawing"].ToString();
                Label lb_reqQTY = (Label)e.Row.FindControl("lb_reqQTY");
                lb_reqQTY.Text = drv["equip_qty"].ToString();
                Label lb_unit = (Label)e.Row.FindControl("lb_unit");
                lb_unit.Text = drv["equip_unit"].ToString();
                Label lb_revQTY = (Label)e.Row.FindControl("lb_revQTY");
                Label conrevQTY = (Label)e.Row.FindControl("conrevQTY");
                Label lb_bidQTY = (Label)e.Row.FindControl("lb_bidQTY");
                Label lb_inco = (Label)e.Row.FindControl("lb_inco");
                lb_inco.Text = drv["equip_incoterm"].ToString();

                string wa = "";
                if (drv["factory_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[0].Selected = true;
                    wa = "factory";
                }
                if (drv["manual_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[1].Selected = true;
                    wa += " manual";
                }
                if (drv["routine_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[2].Selected = true;
                    wa += " routine";
                }
                Label lb_wa = (Label)e.Row.FindControl("lb_wa");
                lb_wa.Text = wa;
                LinkButton lnk_res = (LinkButton)e.Row.FindControl("lnk_res");

                Label lb_cur = (Label)e.Row.FindControl("lb_cur");
                lb_cur.Text = drv["currency"].ToString();

                Label lb_supUnit = (Label)e.Row.FindControl("lb_supUnit");
                if (drv["supply_equip_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["supply_equip_unit_price"]);
                    lb_supUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_supAmou = (Label)e.Row.FindControl("lb_supAmou");
                if (drv["supply_equip_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["supply_equip_amonut"]);
                    lb_supAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localSUnit = (Label)e.Row.FindControl("lb_localSUnit");
                if (drv["local_exwork_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_exwork_unit_price"]);
                    lb_localSUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localSAmou = (Label)e.Row.FindControl("lb_localSAmou");
                if (drv["local_exwork_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_exwork_amonut"]);
                    lb_localSAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localTUnit = (Label)e.Row.FindControl("lb_localTUnit");
                if (drv["local_tran_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_tran_unit_price"]);
                    lb_localTUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localTAmou = (Label)e.Row.FindControl("lb_localTAmou");
                if (drv["local_tran_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_tran_amonut"]);
                    lb_localTAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_hide = (Label)e.Row.FindControl("lb_hide");
                lb_hide.Text = drv["rowid"].ToString();
                Label lb_jobno = (Label)e.Row.FindControl("lb_jobno");
                lb_jobno.Text = drv["job_no"].ToString();
                Label lb_scheno = (Label)e.Row.FindControl("lb_scheno");
                lb_scheno.Text = drv["schedule_no"].ToString();
                Label lb_bidno = (Label)e.Row.FindControl("lb_bidno");
                lb_bidno.Text = drv["bid_no"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                if (drv["legend"].ToString() != "") lb_legend.Text = "(" + drv["legend"].ToString() + ")";
                Label lb_doc = (Label)e.Row.FindControl("lb_doc");
            }
        }
        protected void gvJobSumary_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lb_scheduleNo = (Label)e.Row.FindControl("lb_scheduleNo");
                lb_scheduleNo.Text = drv["schedule_no"].ToString();
                Label lb_scheduleDesc = (Label)e.Row.FindControl("lb_scheduleDesc");
                lb_scheduleDesc.Text = drv["schedule_name"].ToString();
                Label lb_jobNo = (Label)e.Row.FindControl("lb_jobNo");
                Label lb_wa = (Label)e.Row.FindControl("lb_wa");
                lb_wa.Text = drv["wa_url"].ToString();
               
                lb_jobNo.Text = drv["job_no"].ToString();
                Label lb_jobDesc = (Label)e.Row.FindControl("lb_jobDesc");
                lb_jobDesc.Text = drv["job_desc"].ToString();
                Label lb_revision = (Label)e.Row.FindControl("lb_revision");
                lb_revision.Text = drv["job_revision"].ToString();
            }
        }
        protected void gvTaskDoc2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox cboAllverify = (CheckBox)e.Row.FindControl("cboAllverify");
                cboAllverify.Attributes.Add("onclick", "javascript:chkAll('" + cboAllverify.ClientID + "' ,'" + gvTaskDoc2.ClientID + "');");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                CheckBox cboVerify = (CheckBox)e.Row.FindControl("cboVerify");
                Label lblverify = (Label)e.Row.FindControl("lblverify");
                Label lblVerifyby = (Label)e.Row.FindControl("lblVerifyby");
                Label lblNotverify = (Label)e.Row.FindControl("lblNotverify");
                Label lblNotVerifyby = (Label)e.Row.FindControl("lblNotVerifyby");
                EmpInfoClass empInfo = new EmpInfoClass();
        

                if (drv["doc_verify"].ToString() == "1")
                {
                    Employee emp = empInfo.getInFoByEmpID(drv["doc_verify_by"].ToString());
                    string verify_name = emp.SNAME;
                    verify_name = "Verified by" + "<Br/>" + verify_name;
                    lblVerifyby.Text = verify_name;
                    lblVerifyby.Visible = true;
                    cboVerify.Checked = true;
                    lblverify.Visible = true;
                }
                else
                {
                    lblVerifyby.Visible = false;
                    cboVerify.Checked = false;
                    lblverify.Visible = false;
                }
                if (drv["tor_verify"].ToString() == "1")
                {
                    Employee emp = empInfo.getInFoByEmpID(drv["tor_verify_by"].ToString());
                    string verify_name = emp.SNAME;
                    verify_name = "Verified by" + "<Br/>" + verify_name;
                    lblNotVerifyby.Text = verify_name;
                    lblNotVerifyby.Visible = true;
                    lblNotverify.Visible = true;

                }
                else
                {
                    lblNotVerifyby.Visible = false;
                    lblNotverify.Visible = false;
                }
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void btnExecl_Click(object sender, EventArgs e)
        {
            List<string> listFileName = new List<string>();
            string listName = "";
            clsBidDashbaord objbid = new clsBidDashbaord();
            cExcel xec = new cExcel();
            bool chkbid = false;
            DataTable dt = objbid.GetSummary(txt_bidNo.Text);
            if (dt.Rows.Count > 0)
            {
                string path0 = @"c:\Xls";
                string[] chkdirs = Directory.GetDirectories(@"c:\", "Xls*", SearchOption.TopDirectoryOnly);
                if (chkdirs.Length > 0)
                {
                    System.IO.DirectoryInfo di = new DirectoryInfo(path0);
                    foreach (FileInfo file in di.GetFiles())
                    {
                        file.Delete();
                    }

                }

                string path1 = @"c:\PDF";
                string path2 = @"c:\PDF\All_Equipment";
                string path3 = @"c:\PDF\All_Equipment\Equipment.zip";
                DirectoryInfo di1 = Directory.CreateDirectory(path1);
                DirectoryInfo di0 = Directory.CreateDirectory(path0);

                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["schedule_name"].ToString() != "")
                    {
                        chkbid = true;
                        string repSch = dr["schedule_name"].ToString().Replace("/", "_");
                        string filename = dr["bid_no"].ToString() + "-" + dr["schedule_no"].ToString() + " (" + repSch + ")";
                        listName = xec.genExcel(filename, hidDept.Value, hidSect.Value, dr["bid_no"].ToString(), dr["schedule_no"].ToString(), dr["job_no"].ToString(), dr["job_revision"].ToString(), dr["schedule_name"].ToString(), dr["bid_revision"].ToString(), "E");
                    }
                    else
                    {
                        string filename = dr["bid_no"].ToString() + "-" + dr["job_no"].ToString();
                        listName = xec.genExcel(filename, hidDept.Value, hidSect.Value, dr["bid_no"].ToString(), dr["schedule_no"].ToString(), dr["job_no"].ToString(), dr["job_revision"].ToString(), dr["schedule_name"].ToString(), dr["bid_revision"].ToString(), "S");
                    }
                    listFileName.Add(listName);
                }
                if (chkbid == true) xec.genExcelSumTotal(hidBidNo.Value, listFileName);
                else xec.genExcelDelivery(txt_bidNo.Text, hidDept.Value, txt_bidNo.Text, txt_rev.Text, "S");
                string[] dirs = Directory.GetDirectories(@"c:\PDF", "All*", SearchOption.TopDirectoryOnly);
                if (dirs.Length > 0)
                {
                    var diraa = new DirectoryInfo(dirs[0]);
                    diraa.Delete(true);
                }
                DirectoryInfo di2 = Directory.CreateDirectory(path2);
                string[] filePaths = Directory.GetFiles(@"c:\Xls", "*.*", SearchOption.TopDirectoryOnly);
                if (filePaths.Length > 0)
                {
                    ZipFile.CreateFromDirectory(path0, path3, CompressionLevel.Optimal, false);
                    Response.ContentType = "Application/zip";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=Equipment.zip");
                    Response.TransmitFile("C:\\PDF\\All_Equipment\\Equipment.zip");
                    Response.Flush();
                    Response.End();
                }
                else
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Excel is not creating');", true);
                }
            }

        }

        protected void btnFilter_Click(object sender, EventArgs e)
        {
            string filterall = "";
            string filtertype = "";
            string filter = "";
            DataTable dtAll = objbidm.getDoctype();
            DataView dvall = new DataView(dtAll);

            if (ddlDept.SelectedValue != "")
            {
                filter = string.Format("(depart_position_name = '{0}')", ddlDept.SelectedValue);
                filterall = string.Format("(depart_position_name = '{0}')", ddlDept.SelectedValue);
                hidFilter.Value = filterall;
            }
           

            if (ddlDoctype.SelectedValue != "")
            {
                if (filter != "") filter = string.Format("{0} and ({1})", filter, "doctype_code ='" + ddlDoctype.SelectedValue + "'");
                else filter = string.Format("(doctype_code = '{0}')", ddlDoctype.SelectedValue);
                filtertype = string.Format("(doctype_code = '{0}')", ddlDoctype.SelectedValue);
          
            }

            dvall.RowFilter = filtertype;
            if (dvall.Count > 0)
            {
                gvAllDoc.DataSource = dvall;
                gvAllDoc.DataBind();
            }
            else
            {
                gvAllDoc.DataSource = null;
                gvAllDoc.DataBind();
            }

            DataTable dtask = objbidm.GetDataAllDoc(hidBidNo.Value, "", hidBidRev.Value, "bid");
            DataView dv = new DataView(dtask);
            dv.RowFilter = filter;
            if (dv.Count > 0)
            {
                gvTaskDoc2.DataSource = dv;
                gvTaskDoc2.DataBind();
            }
            else
            {
                gvTaskDoc2.DataSource = null;
                gvTaskDoc2.DataBind();
            }

            if (hidFix.Value =="1")
            {
                gvTaskDoc2.Style["display"] = "inline-table";
                gvAllDoc.Style["display"] = "none";

            }
            else if (hidFix.Value == "0")
            {
                gvTaskDoc2.Style["display"] = "none";
                gvAllDoc.Style["display"] = "inline-table";
            }
           
            hidFilter.Value = "";
            hidTab.Value = "1";
            dvhidDoc.Style["display"] = "block";
            dvhidEq.Style["display"] = "none";
            dvhidAssign.Style["display"] = "none";
        }

        protected void gvaas_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "viewdata")
            {
                dvhidDoc.Style["display"] = "none";
                dvhidEq.Style["display"] = "none";
                dvhidAssign.Style["display"] = "block";
                hidTab.Value = "1";
                int i = System.Convert.ToInt32(e.CommandArgument);
                var lbl = ((Label)gvaas.Rows[i].FindControl("lb_division"));
                var gv = ((GridView)gvaas.Rows[i].FindControl("gvaasSub"));

                if (gv.Visible == false)
                {
                    gv.Visible = true;

                    clsBidDashbaord objdb = new clsBidDashbaord();
                    DataTable dtview = objdb.GetActivity(txt_bidNo.Text, txt_rev.Text);
                    DataView dv = new DataView(dtview);
                    dv.RowFilter = "depart_position_name ='" + lbl.Text + "'";

                    if (dv.Count > 0)
                    {
                        gv.DataSource = dv;
                        gv.DataBind();

                    }

                }
                else { gv.Visible = false; }

            }

        }
        protected void gvaasSub_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["depart_position_name"].ToString();

                Label lb_sec = (Label)e.Row.FindControl("lb_sec");
                lb_sec.Text = drv["section_position_name"].ToString();

                Label lb_activity = (Label)e.Row.FindControl("lb_activity");
                lb_activity.Text = drv["activity_name"].ToString();
                Label lb_statusDA = (Label)e.Row.FindControl("lb_statusDA");
                Label lb_statusDPSA = (Label)e.Row.FindControl("lb_statusDPSA");
                Label lb_statusDSA = (Label)e.Row.FindControl("lb_statusDSA");
                Label lbhidStatus = (Label)e.Row.FindControl("lbhidStatus");


                CheckBox cb_sub = (CheckBox)e.Row.FindControl("cb_sub");
                DropDownList ddl_da = (DropDownList)e.Row.FindControl("ddl_da");
                DropDownList ddl_epsa = (DropDownList)e.Row.FindControl("ddl_epsa");
                DropDownList ddl_dsa = (DropDownList)e.Row.FindControl("ddl_dsa");
                HiddenField hdf_da = (HiddenField)e.Row.FindControl("hdf_da");
                HiddenField hdf_epsa = (HiddenField)e.Row.FindControl("hdf_epsa");
                HiddenField hdf_dsa = (HiddenField)e.Row.FindControl("hdf_dsa");

                Label lb_reqDraw = (Label)e.Row.FindControl("lb_reqDraw");
                lb_reqDraw.Text = drv["req_drawing"].ToString();
                Label lb_reqEqui = (Label)e.Row.FindControl("lb_reqEqui");
                lb_reqEqui.Text = drv["req_equip"].ToString();
                Label lb_reqTechDoc = (Label)e.Row.FindControl("lb_reqTechDoc");
                lb_reqTechDoc.Text = drv["req_tech_doc"].ToString();
                Label lb_ActID = (Label)e.Row.FindControl("lb_ActID");
                lb_ActID.Text = drv["activity_id"].ToString();
                Label lb_nxtActID = (Label)e.Row.FindControl("lb_nxtActID");
                lb_nxtActID.Text = drv["next_activity_id"].ToString();
                Label lb_apprRout = (Label)e.Row.FindControl("lb_apprRout");
                lb_apprRout.Text = drv["approval_routing"].ToString();
                Label lb_emailNoti = (Label)e.Row.FindControl("lb_emailNoti");
                lb_emailNoti.Text = drv["email_noti_id"].ToString();
                Label lb_emailCont = (Label)e.Row.FindControl("lb_emailCont");
                lb_emailCont.Text = drv["email_content"].ToString();
                Label lb_bidNo = (Label)e.Row.FindControl("lb_bidNo");
                lb_bidNo.Text = drv["bid_no"].ToString();
                Label lb_bidRev = (Label)e.Row.FindControl("lb_bidRev");
                lb_bidRev.Text = drv["bid_revision"].ToString();
                lbhidStatus.Text = drv["status_activity"].ToString();
                Label lblReq_dr = (Label)e.Row.FindControl("lblReq_dr");
                Label lblReq_eq = (Label)e.Row.FindControl("lblReq_eq");
                Label lblReq_doc = (Label)e.Row.FindControl("lblReq_doc");

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                string[] staffList = empInfo.getEmpIDAndNameInDepartment(emp.ORGSNAME4);
                ddl_da.Items.Clear();
                ddl_epsa.Items.Clear();
                ddl_dsa.Items.Clear();
                ddl_da.Items.Insert(0, new ListItem("<-- Select -->", ""));
                ddl_epsa.Items.Insert(0, new ListItem("<-- Select -->", ""));
                ddl_dsa.Items.Insert(0, new ListItem("<-- Select -->", ""));

                foreach (string staffInfo in staffList)
                {
                    string[] staff = staffInfo.Split(':');

                    ListItem item1 = new ListItem();

                    item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item1.Value = staff[0].ToString();

                    ddl_da.Items.Add(item1);

                    ListItem item2 = new ListItem();
                    item2.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item2.Value = staff[0].ToString();
                    ddl_epsa.Items.Add(item2);

                    ListItem item3 = new ListItem();
                    item3.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item3.Value = staff[0].ToString();
                    ddl_dsa.Items.Add(item3);
                }
                Label lb_headDept = (Label)e.Row.FindControl("lb_headDept");
                Label lb_headSect = (Label)e.Row.FindControl("lb_headSect");
                Label lb_headProject = (Label)e.Row.FindControl("lb_headProject");
                Label lb_assiDept = (Label)e.Row.FindControl("lb_assiDept");
                Label lb_dept = (Label)e.Row.FindControl("lb_dept");
                ImageButton imgbt = (ImageButton)e.Row.FindControl("imgbt");

                string tooltip = "";
                string[] rounting = GetRoutingApproval(drv["approval_routing"].ToString());
                if (rounting.Length > 0)
                {
                    if (rounting.Length == 1 && rounting[0] != "null")
                    {
                        lb_headDept.Text = rounting[0];
                        tooltip = "Approve1 : " + lb_headDept.Text;

                    }
                    else if (rounting.Length == 2)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text;
                    }
                    else if (rounting.Length == 3)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        lb_headProject.Text = rounting[2];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text + "\n" +
                            "Approve3 : " + lb_headProject.Text;
                    }
                    else if (rounting.Length == 4)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        lb_headProject.Text = rounting[2];
                        lb_assiDept.Text = rounting[3];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text + "\n" +
                            "Approve3 : " + lb_headProject.Text + "\n" +
                            "Approve4 : " + lb_assiDept.Text;
                    }
                    else if (rounting.Length == 5)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        lb_headProject.Text = rounting[2];
                        lb_assiDept.Text = rounting[3];
                        lb_dept.Text = rounting[4];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text + "\n" +
                            "Approve3 : " + lb_headProject.Text + "\n" +
                            "Approve4 : " + lb_assiDept.Text + "\n" +
                            "Approve5 : " + lb_dept.Text;
                    }
                    imgbt.ToolTip = tooltip;
                }
                else
                {
                    clsBidDashbaord objdb = new clsBidDashbaord();
                    DataTable dt = objdb.getHead(hidDept.Value, hidSect.Value);
                    if (dt.Rows.Count > 0)
                    {
                        lb_headDept.Text = dt.Rows[0]["orgsname5"].ToString();
                        lb_headSect.Text = dt.Rows[1]["orgsname5"].ToString();
                        lb_headProject.Text = dt.Rows[2]["orgsname5"].ToString();
                        lb_assiDept.Text = dt.Rows[3]["orgsname5"].ToString();
                        if (dt.Rows.Count == 5)
                        {
                            lb_dept.Text = dt.Rows[4]["orgsname5"].ToString();
                            tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                                "Approve2 : " + lb_headSect.Text + "\n" +
                                "Approve3 : " + lb_headProject.Text + "\n" +
                                "Approve4 : " + lb_assiDept.Text + "\n" +
                                "Approve5 : " + lb_dept.Text;
                            imgbt.ToolTip = tooltip;
                        }
                        else
                        {
                            tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                                "Approve2 : " + lb_headSect.Text + "\n" +
                                "Approve3 : " + lb_headProject.Text + "\n" +
                                "Approve4 : " + lb_assiDept.Text;
                            imgbt.ToolTip = tooltip;
                        }

                    }

                }


                //if (lb_reqDraw.Text == "False" && lb_reqEqui.Text == "False" && lb_reqTechDoc.Text == "False") cb_sub.Enabled = false;

                if (lb_reqDraw.Text == "False")
                {
                    ddl_da.SelectedIndex = 0;
                    //ddl_da.Enabled = false;
                    lb_statusDA.Text = "-";
                }
                else
                    lblReq_dr.Visible = true;

                if (lb_reqEqui.Text == "False")
                {
                    ddl_epsa.SelectedIndex = 0;
                    //ddl_epsa.Enabled = false;
                    lb_statusDPSA.Text = "-";
                }
                else
                    lblReq_eq.Visible = true;

                if (lb_reqTechDoc.Text == "False")
                {
                    ddl_dsa.SelectedIndex = 0;
                    //ddl_dsa.Enabled = false;
                    lb_statusDSA.Text = "-";
                }
                else
                    lblReq_doc.Visible = true;

                if (hidDept.Value != drv["depart_position_name"].ToString())
                {
                    //ใช้ได้ตามกอง/แผนก
                    cb_sub.Enabled = false;
                    imgbt.Enabled = false;
                    ddl_da.Enabled = false;
                    ddl_epsa.Enabled = false;
                    ddl_dsa.Enabled = false;
                    lblReq_dr.Visible = false;
                    lblReq_eq.Visible = false;
                    lblReq_doc.Visible = false;

                }

                if (hdf_da.Value != "")
                {
                    ddl_da.SelectedValue = hdf_da.Value;
                }
                if (hdf_epsa.Value != "")
                {
                    ddl_epsa.SelectedValue = hdf_epsa.Value;
                }
                if (hdf_dsa.Value != "")
                {
                    ddl_dsa.SelectedValue = hdf_dsa.Value;
                }

                if (ddl_da.SelectedIndex != 0 && drv["status_drawing"].ToString() != "") lb_statusDA.Text = drv["status_drawing"].ToString();
                else lb_statusDA.Text = "-";
                if (ddl_epsa.SelectedIndex != 0 && drv["status_equip"].ToString() != "") lb_statusDPSA.Text = drv["status_equip"].ToString();
                else lb_statusDPSA.Text = "-";
                if (ddl_dsa.SelectedIndex != 0 && drv["status_tech_doc"].ToString() != "") lb_statusDSA.Text = drv["status_tech_doc"].ToString();
                else lb_statusDSA.Text = "-";

                //if (drv["status_activity"].ToString() == "WF in progress" || drv["status_activity"].ToString() == "FINISHED")
                //{
                //    imgbt.Enabled = false;
                //    cb_sub.Enabled = false;
                //    ddl_da.Enabled = false;
                //    ddl_epsa.Enabled = false;
                //    ddl_dsa.Enabled = false;
                //    lblReq_dr.Visible = false;
                //    lblReq_eq.Visible = false;
                //    lblReq_doc.Visible = false;

                //}

                imgbt.Enabled = false;
                cb_sub.Enabled = false;
                ddl_da.Enabled = false;
                ddl_epsa.Enabled = false;
                ddl_dsa.Enabled = false;
                lblReq_dr.Visible = false;
                lblReq_eq.Visible = false;
                lblReq_doc.Visible = false;
            }
        }
        private string[] GetRoutingApproval(string strRouteApproval)
        {
            return strRouteApproval.Split(new char[] { ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
        }

        protected void btnVerify_Click(object sender, EventArgs e)
        {
            if (hidFix.Value == "1")
            {
                foreach (GridViewRow gvr2 in gvTaskDoc2.Rows)
                {
                    CheckBox cb2 = (CheckBox)gvr2.FindControl("cboVerify");
                    string doccode = ((Label)gvr2.FindControl("lblCode")).Text;
                    string docid = ((Label)gvr2.FindControl("lblDocno")).Text;
                    string jobno = ((Label)gvr2.FindControl("lblBidno")).Text;
                    string jobrev = ((Label)gvr2.FindControl("lblRevbid")).Text;
                    if (cb2.Checked)
                    {
                        objbidm.Verify_Document(hidBidNo.Value, hidBidRev.Value, jobno, jobrev, docid, doccode, hidLogin.Value, hidMc.Value, "1");
                    }
                    else
                    {
                        objbidm.Verify_Document(hidBidNo.Value, hidBidRev.Value, jobno, jobrev, docid, doccode, "", "", "0");
                    }
                }

            }
            else if (hidFix.Value == "0")
            {
                foreach (GridViewRow dgi in gvAllDoc.Rows)
                {
                    GridView gv = (GridView)dgi.FindControl("gvTaskDoc");
                    foreach (GridViewRow gvr in gv.Rows)
                    {
                        CheckBox cb = (CheckBox)gvr.FindControl("cbVerify");
                        string doccode = ((Label)gvr.FindControl("lbCode")).Text;
                        string docid = ((Label)gvr.FindControl("lbDocno")).Text;
                        string jobno = ((Label)gvr.FindControl("lbBidno")).Text;
                        string jobrev = ((Label)gvr.FindControl("lbRevbid")).Text;
                        if (cb.Checked)
                        {
                            objbidm.Verify_Document(hidBidNo.Value, hidBidRev.Value, jobno, jobrev, docid, doccode, hidLogin.Value, hidMc.Value, "1");
                        }
                        else
                        {
                            objbidm.Verify_Document(hidBidNo.Value, hidBidRev.Value, jobno, jobrev, docid, doccode, "", "", "0");
                        }
                    }
                }
            }
            
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true);
            bindAllTab();

            if (hidFix.Value == "1")
            {
                gvTaskDoc2.Style["display"] = "inline-table";
                gvAllDoc.Style["display"] = "none";

            }
            else if (hidFix.Value == "0")
            {
                gvTaskDoc2.Style["display"] = "none";
                gvAllDoc.Style["display"] = "inline-table";
            }
            hidTab.Value = "1";
            dvhidDoc.Style["display"] = "block";
            dvhidEq.Style["display"] = "none";
            dvhidAssign.Style["display"] = "none";
        }
    }
}