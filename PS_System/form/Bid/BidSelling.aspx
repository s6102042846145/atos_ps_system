﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidSelling.aspx.cs" Inherits="PS_System.form.Bid.BidSelling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Contract Management</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .auto-style1 {
            height: 20px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            height: 26px;
        }
        .auto-style6 {
            height: 23px;
            width: 147px;
        }
        .auto-style8 {
            width: 147px;
        }
        .auto-style9 {
            height: 23px;
            width: 140px;
        }
        .auto-style10 {
            height: 20px;
            width: 140px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Selling"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;" >
            <table style="width: 100%;">
                <tr>
                    <td colspan="4" style="background-color: #007ACC; color: #FFFFFF;">
                        <asp:Label ID="Label60" runat="server" Text="ข้อมูลผู้ซื้อเอกสารประกวดราคา" Font-Bold="True" Font-Size="Small"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label1" runat="server" Text="Bid No."></asp:Label>
                    </td>
                    <td class="auto-style4">
                                 <select id="mtBidNo" runat="server" name="D3" style="width: 550px">
                                 </select>

                    </td>
                    <td class="auto-style8" >
                        <asp:Label ID="Label16" runat="server" Text="Bidder No."></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtBidderNo" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style10">
                        <asp:Label ID="Label52" runat="server" Text="Bid Description"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtBidDescription" runat="server" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Enabled="False" ReadOnly="True" Rows="3" TextMode="MultiLine" Width="544px" Height="100px"></asp:TextBox>
                    </td>
                  
                </tr>
                
           
                <tr>
                    <td colspan="4" style="background-color: #007ACC; color: #FFFFFF;" class="auto-style5">
                        <asp:Label ID="Label53" runat="server" Text="รายละเอียดบริษัท" Font-Bold="True" Font-Size="Small"></asp:Label>
                    &nbsp;</td>
                    <td class="auto-style5">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label2" runat="server" Text="Vender Code"></asp:Label>
                    </td>
                    <td class="auto-style4">
                                 <select id="mtVenderCode" runat="server" name="D3" style="width: 550px">
                                 </select></td>
                    <td class="auto-style8" >
                        <asp:Label ID="Label3" runat="server" Text="Telephone"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtTelephone" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
                  <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label4" runat="server" Text="Company Name"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtCompanyName" runat="server" ReadOnly="True" Enabled="False" Width="540px"></asp:TextBox>
                        </td>
                    <td class="auto-style6">
                        <asp:Label ID="Label5" runat="server" Text="Fax"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtFax" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
                  <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label6" runat="server" Text="TAX ID"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtTaxID" runat="server" ReadOnly="True" Enabled="False" Width="540px"></asp:TextBox>
                        </td>
                    <td class="auto-style6">
                        <asp:Label ID="Label7" runat="server" Text="E-mail"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtEmail" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
                  <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label8" runat="server" Text="EGP No."></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtEGPNo" runat="server" ReadOnly="True" Enabled="False" Width="540px"></asp:TextBox>
                        </td>
                    <td class="auto-style6">
                        <asp:Label ID="Label9" runat="server" Text="Contact Name"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtConName" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
                  <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label10" runat="server" Text="EGP Register Date"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtRegDate" runat="server" ReadOnly="True" Enabled="False" Width="100px"></asp:TextBox>
                        &nbsp;<asp:Label ID="lbDate" runat="server" Text="วว/ดด/ปปปป (พ.ศ.)" ForeColor="Gray"></asp:Label>
                      </td>
                    <td class="auto-style6">
                        <asp:Label ID="Label11" runat="server" Text="Contack Tel"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtConTel" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
                  <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label12" runat="server" Text="Address"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtAddress" runat="server" ReadOnly="True" Enabled="False" Width="540px"></asp:TextBox>
                        </td>
                    <td class="auto-style6">
                        <asp:Label ID="Label13" runat="server" Text="Contack Fax"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtConFax" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
                  <tr>
                    <td class="auto-style9">
                        <asp:Label ID="Label14" runat="server" Text="Country"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtCountry" runat="server" ReadOnly="True" Enabled="False" Width="540px"></asp:TextBox>
                        </td>
                    <td class="auto-style6">
                        <asp:Label ID="Label15" runat="server" Text="Contack E-mail"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtConEmail" runat="server" ReadOnly="True" Enabled="False" Width="400px"></asp:TextBox>
                        </td>
                </tr>
               
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Save" OnClick="btnSave_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnCancel" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
