﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidContractMgt.aspx.cs" Inherits="PS_System.form.Bid.BidContractMgt" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Contract Management</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Contract Management"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;" >
            <table style="width: 100%;">
                <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" Text="Bid No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstBidNo" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label2" runat="server" Text="LOA Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLOADate" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Contract No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContractNo" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label4" runat="server" Text="LOA Confirmed:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLOAConfirm" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Description:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContractDesc" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Contract Sign Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSignDate" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Job No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstJonNo" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label8" runat="server" Text="Delivery Date/Completion Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtJ_K_Date" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Text="Schedule No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstScheduleNo" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label10" runat="server" Text="Schedule Name:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtScheduleName" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Panel ID="pnlSupply" runat="server" BackColor="#0099CC" Height="17pt" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="White">
                            Supply and Construction
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label11" runat="server" Text="Scope of Work:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSOW" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="Project:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtProject" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label13" runat="server" Text="Transmission Line No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTrans_LineNo" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label14" runat="server" Text="Line Length:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLineLength" runat="server" ReadOnly="true"></asp:TextBox>&nbsp;<span style="font-family: Tahoma; font-size: 15px; font-weight: bold">KM</span>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#0099CC" Height="17pt" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="White">
                            Parameter
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label15" runat="server" Text="Insurance Percentage:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtInsurance" runat="server" Text="0.00"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label16" runat="server" Text="VAT:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtVAT" runat="server" Text="0.07"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label17" runat="server" Text="Customer Clearance:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustomer_Clearance" runat="server" Text="0.01"></asp:TextBox>
                    </td>
                    <td>
                        <asp:Label ID="Label18" runat="server" Text="Local Preference:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLocal_Preference" runat="server" Text="0.07"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Panel ID="Panel3" runat="server" BackColor="#0099CC" Height="17pt" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="White">
                            Purchasing Data
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label19" runat="server" Text="Contractor:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstContractor" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label20" runat="server" Text="Default Requisitioner:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstRequisitioner" runat="server"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label21" runat="server" Text="Purchasing Method:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstPurchasing_Method" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label22" runat="server" Text="Purchasing Group:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstPurchasing_Group" runat="server"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label23" runat="server" Text="Purchasing Organization:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstOrganization" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label25" runat="server" Text="Default Plant:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstPlant" runat="server"></asp:ListBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Panel ID="Panel4" runat="server" BackColor="#0099CC" Height="17pt" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="White">
                            Exchange Rate
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label26" runat="server" Text="Exchange Rate Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="lstExchange_Rate_Date" runat="server"></asp:ListBox>
                    </td>
                    <td>
                        <asp:Label ID="Label27" runat="server" Text="Exchange Rate:"></asp:Label>
                    </td>
                    <td>
                       <asp:GridView ID="gvLegend" runat="server" AutoGenerateColumns="False" Width="600px"
                            Visible="true" ClientIDMode="Static" ShowHeaderWhenEmpty="true" EmptyDataText="No Record."
                            class="table table-striped table-bordered table-sm">
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Currencr(-ies)">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvLegend_txtLegend" runat="server" Text='<%# Bind("legend") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Exchange Rate(s)">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvLegend_txtQTY" runat="server" Text='<%# Bind("legend_qty") %>'></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CCFF" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Save Draft" />
                        &nbsp;&nbsp;<asp:Button ID="btnSubmit" CssClass="btn btn-success" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Submit" />
                        &nbsp;&nbsp;<asp:Button ID="btnCancel" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
