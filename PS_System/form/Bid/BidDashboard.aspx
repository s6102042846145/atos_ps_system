﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidDashboard.aspx.cs" Inherits="PS_System.form.Bid.BidDashboard"  MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
      
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>




    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <%--    <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>--%>    <%--<link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />--%>

    <%--  <script type="text/javascript" src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.min.css" rel="stylesheet" type="text/css" />--%>    <%--<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>--%><%--    <link rel="stylesheet" href="dist/css/bootstrap-multiselect.css" type="text/css" />
    <script type="text/javascript" src="dist/js/bootstrap-multiselect.js"></script>
    <link rel="stylesheet" href="docs/css/bootstrap-4.5.2.min.css" type="text/css"/>--%>
    <script>  //$(document).ready(function () {

        //    datatableConfig();

        //});

        function SelectAll(aa) {
            var cb = document.getElementById(aa);
            var gvM = document.getElementById("<%= gvaas.ClientID %>");
            var gvsub = gvM.getElementsByTagName("table");
            for (var i = 0; i < gvsub.length; i++) {
                if (gvsub[i].id.indexOf("gv_aasSub") > -1) {
                    var getcb = document.getElementById(gvsub[i].id);
                    var thiscb = getcb.getElementsByTagName("input");
                    for (var j = 0; j < thiscb.length; j++) {
                        if (thiscb[j].id.indexOf("cb_sub") > -1 && thiscb[j].type == 'checkbox') {
                            thiscb[j].checked = cb.checked;
                        }
                    }
                }
            }
        }

        function chkAll(Checkbox, gv) {
            var TargetControl = document.getElementById(gv);
            var cb = document.getElementById(Checkbox);
            var TargetChildControl = "cb_sub";
            var Inputs = TargetControl.getElementsByTagName("input");

            for (var n = 0; n < Inputs.length; n++) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false) {
                    Inputs[n].checked = cb.checked;
                }
            }
        }

        //function divexpandcollapse(divname) {
        //    var div = document.getElementById(divname);
        //    var img = document.getElementById('img' + divname);
        //    if (div.style.display == "none") {
        //        div.style.display = "inline";
        //        img.src = "../../images/minus.png";
        //    } else {
        //        div.style.display = "none";
        //        img.src = "../../images/plus.png";
        //    }
        //}
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var td = document.getElementById('td' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                td.style.display = "table-row";
                img.src = "../../images/minus.png";
            } else {
                div.style.display = "none";
                td.style.display = "none";
                img.src = "../../images/plus.png";
            }
        }
        function divexpandcollapseDoc(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var td = document.getElementById('td' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                td.style.display = "table-row";
                img.src = "../../images/minus.png";
            } else {
                div.style.display = "none";
                td.style.display = "none";
                img.src = "../../images/plus.png";
            }
        }
        //function divexpandcollapse(divname) {
        //    var img = "img" + divname;
        //    if ($("#" + img).attr("src") == "../../images/plus.png") {
        //        $("#" + img).closest("tr").after("<tr><td></td><td colspan='100%'> " + $("#" + divname).html() + "</td></tr>")
        //        $("#" + img).attr("src", "../../images/details_close.png");
        //        //colspan = '100%'
        //    } else {
        //        $("#" + img).closest("tr").next().remove();
        //        $("#" + img).attr("src", "../../images/plus.png");
        //    }
        //}

        //$(function () {
        //    datatableConfig(); // bind data table on first page load
        //    // bind data table on every UpdatePanel refresh
        //    Sys.WebForms.PageRequestManager.getInstance().add_endRequest(datatableConfig);
        //});
        //function datatableConfig() {

        //    $('#gvJobSumary').DataTable({
        //        destroy: true,
        //        searching: false,
        //        lengthChange: true,
        //        paging: true,
        //        columnDefs: [{
        //            orderable: false,

        //            targets: 0
        //        }],

        //        dom: 'lfrtip',
        //        "fnStateSave": function (oSettings, oData) {
        //            localStorage.setItem('gvJobSumary', JSON.stringify(oData));
        //        },
        //        "fnStateLoad": function (oSettings) {
        //            return JSON.parse(localStorage.getItem('gvJobSumary'));
        //        }
        //    });
        //}
        //var prm = Sys.WebForms.PageRequestManager.getInstance();
        //if (prm != null) {
        //    prm.add_endRequest(function (sender, e) {
        //        if (sender._postBackSettings.panelsToUpdate != null) {
        //            datatableConfig();
        //        }
        //    });
        //};
        function HeaderClick(CheckBox) {
            var TargetBaseControl = document.getElementById('<%= this.gvJobSumary.ClientID %>');
            var TargetChildControl = "cb_sub";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                    Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false)
                    Inputs[n].checked = CheckBox.checked;
        }
        function clickAll(CheckBox) {
            var TargetBaseControl = document.getElementById('<%= this.gvaas.ClientID %>');
            var TargetChildControl = "cb_subAc";
            var Inputs = TargetBaseControl.getElementsByTagName("input");
            for (var n = 0; n < Inputs.length; ++n)
                if (Inputs[n].type == 'checkbox' &&
                    Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false)
                    Inputs[n].checked = CheckBox.checked;
        }
        function openPop() {
            var mainpop = document.getElementById("myModal");
            var dvRounting = document.getElementById("dvRounting");
            if (mainpop != null && dvRounting!=null) {
                mainpop.style.display = "block";
                dvRounting.style.display = "block";
            }

        }

        $(document).ready(function () {


            $('[id*=ddl_dsa]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=ddl_epsa]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=ddl_da]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });


            $('[id*=DDL_DessignReq]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=DDL_EqsignReq]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=DDL_DocsignReq]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
        });

        function chkSelect(objCHK) {

            var selectValueDoc = "|" + objCHK + "|";

            if (objCHK.checked == true) {
                if (document.getElementById("hidchkSelect").value.includes(selectValueDoc) == false) {
                    document.getElementById("hidchkSelect").value = document.getElementById("hidchkSelect").value + selectValueDoc;

                }
            }
            else {
                document.getElementById("hidchkSelect").value = document.getElementById("hidchkSelect").value.replace(selectValueDoc, "");

            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                     
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Monitor"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <table style="width: 95%">
                <tr style="padding-left: 5px">
                    <td colspan="2">
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left; color: #006699;"><b>Bid Dashboard</b></td>
                            </tr>
                            <tr>
                                <td style="width: 10%">
                                    <asp:Label ID="lb_bidNo" runat="server" Text="Bid No:"></asp:Label>
                                </td>
                                <td style="width: 42%">
                                    <asp:TextBox ID="txt_bidNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                    <asp:Label ID="lb_rev" runat="server" Text="Rev.:" Width="10%" Style="text-align: center"></asp:Label>
                                    <asp:TextBox ID="txt_rev" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="40px"></asp:TextBox>
                                    <asp:Label ID="Label2" runat="server" Width="5%" visibility="hidden"></asp:Label>
                                    <asp:Button ID="Button1" runat="server" Text="Compare PS" class="btn btn-primary" />
                                </td>

                                <td style="width: 45%" rowspan="2">
                                    <table style="width: 95%;">
                                        <tr style="height: 10%;">
                                            <td style="width: 20%; text-align: right; vertical-align: initial;">
                                                <asp:Label ID="Label1" runat="server" Text="Bid Progress:"></asp:Label>
                                            </td>
                                            <td style="width: 80%; text-align: left; vertical-align: middle;">
                                                <div class="progress">
                                                    <div id="ProcessBar" runat="server"  class="progress-bar progress-bar-striped" role="progressbar" style="width: 20%" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100">20%</div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 90%;">
                                            <td colspan="2">
                                                <asp:Label ID="lblProgress_Department" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                                <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="lb_bisDesc" runat="server" Text="Bid Description:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_desc" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%"></asp:TextBox>
                                </td>
                                <td rowspan="5" style="vertical-align:top;">
                                     <div style="overflow-y:auto;height:250px;">
                                        <asp:GridView ID="gvAttach" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." OnRowDataBound="gvAttach_RowDataBound">
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Attachment Name">
                                    <ItemTemplate>
                                     <asp:LinkButton ID="lnkAtt_view" runat="server" Text='<%# Bind("att_url") %>'></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="350px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Attachment Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAtt_date" runat="server" Text='<%# Bind("upload_datetime") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                           <tr style="height: 10px">
                                <td></td>
                            </tr>
                           <tr>
                               
                                <td style="vertical-align:top;">
                               
                                    <asp:Label ID="Label8" runat="server" Text="Plan Start Date:"></asp:Label>
                                </td>
                                <td style="text-align:left;vertical-align:top;">
                                    <asp:TextBox ID="txtStartDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="39%"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label11" runat="server" Text="Plan End Date:"></asp:Label>
                                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <asp:TextBox ID="txtEndDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="40%"></asp:TextBox>
                                </td>
                              <%--  <td>
                                    
                                </td>
                                <td style="text-align:left;">
                                 
                                </td>--%>
                            </tr>
                 
                              <tr>
                                <td style="vertical-align:top;">
                                    <asp:Label ID="Label10" runat="server" Text="Completion Date:"></asp:Label>
                                </td>
                                <td colspan="1" style="text-align:left;vertical-align:top;">
                                  <div  id="dvlblCom" runat="server" style="border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;">
                                    <asp:Label ID="lblCom" runat="server" ></asp:Label>
                                      </div>
                                </td>
                            </tr>
                           <tr>
                                <td style="vertical-align:top;">
                                    <asp:Label ID="Label9" runat="server" Text="Remark Bid:"></asp:Label>
                                </td>
                                <td style="vertical-align:top;">
                                    <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                               
                                </td>
                            </tr>
                            <tr >
                                <td>
                                    <asp:Button ID="btnExecl" runat="server" class="btn btn-primary" Text="Excel Equipment" OnClick="btnExecl_Click" />
                                </td>
                            </tr>
                             <tr style="height: 10px">
                                <td></td>
                            </tr>
                          <%--  <tr>
                                <td>
                                    <asp:Label ID="lb_projName" runat="server" Text="Project Name:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txt_projName" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="650px"></asp:TextBox>

                                </td>
                            </tr>
                                <tr style="height: 10px">
                                <td></td>
                            </tr>--%>
                            <tr>
                                <td style="text-align: left; color: #006699;" colspan="2"><b>Bid - Job Summary</b></td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                    <asp:GridView ID="gvJobSumary" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvJobSumary_RowDataBound">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                       <%--     <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:CheckBox ID="cb_all" runat="server" />
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="cb_sub" runat="server" />
                                                </ItemTemplate>
                                                <HeaderStyle Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>--%>
                                            <asp:TemplateField HeaderText="Schedule No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="7%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleDesc" runat="server" Font-Underline="True" ForeColor="Blue"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Sub/Line Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_Subline" runat="server" Text='<%# Bind("sub_line_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Job Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_jobDesc" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="45%" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Revision">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_revision" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                   <asp:TemplateField HeaderText="Work Assignment">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <HeaderStyle Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField HeaderText="Job Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_jobStatus" runat="server"  ForeColor="#6BB373"></asp:Label>
                                    </ItemTemplate>
                                     <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>--%>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 20px;">
        </div>
        <div id="dvActivity" runat="server" align="center">
            <table style="width: 95%">
                <tr style="padding-left: 5%">
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left; color: #006699; width: 60%;"><b>Activity, Assignment and Status</b></td>
                                <td style="width: 40%;" rowspan="6"></td>
                            </tr>
                    <tr>
                    <td colspan="2">
                        <asp:Label ID="Label12" runat="server" Text="Template Name:"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_Template" runat="server"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:Label ID="Label13" runat="server" Text="Assigned to:"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_DessignReq" runat="server"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:Label ID="Label14" runat="server" Text="Equipment/Price Schdule Assigned to:" Visible="false"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_EqsignReq" runat="server" Visible="false"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:Label ID="Label15" runat="server" Text="Document Assigned to:" Visible="false"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_DocsignReq" runat="server" Visible="false"></asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSelect" runat="server" Text="Select" class="btn btn-primary" OnClick="btnSelect_Click"/>
                    </td>
                </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:GridView ID="gvaas" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvaas_RowDataBound">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <a href="JavaScript:divexpandcollapse('div<%# Eval("rowno") %>');">
                                                        <img id='imgdiv<%# Eval("rowno") %>' border="0" src="../../images/plus.png" width="20px" height="20px" /></a>


                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="กอง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_division" runat="server"></asp:Label>
                                                    <tr id="tddiv<%# Eval("rowno") %>" style="display: none;">
                                                        <td colspan="3">
                                                            <div id="div<%# Eval("rowno") %>" style="display: none;">
                                                                <asp:GridView ID="gvaasSub" runat="server" AutoGenerateColumns="false" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Names="tahoma" Font-Size="9pt"
                                                                    OnRowDataBound="gvaasSub_RowDataBound">
                                                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />

                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="Select">
                                                             <%--               <HeaderTemplate>

                                                                                <asp:CheckBox ID="cb_all" runat="server" />
                                                                            </HeaderTemplate>--%>
                                                                            <ItemTemplate>
                                                                                <asp:CheckBox ID="cb_sub" runat="server" onclick="chkSelect(this);"/>

                                                                            </ItemTemplate>
                                                                            <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="แผนก">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_depm" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_sec" runat="server" ></asp:Label>
                                                                                <asp:Label ID="lb_reqDraw" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_reqEqui" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_reqTechDoc" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_ActID" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_nxtActID" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_apprRout" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_emailNoti" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_emailCont" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_bidNo" runat="server" Visible="false"></asp:Label>
                                                                                <asp:Label ID="lb_bidRev" runat="server" Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Activity">
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnkActivity" runat="server" Text='<%# Bind("activity_name") %>' OnClick="lnkActivity_Click" ></asp:LinkButton>
                                                                                <asp:Label ID="lb_activity" runat="server" Visible="false"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Process">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_aas" runat="server"></asp:Label>
                                                                                 <asp:Label ID="lbhidStatus" runat="server" Visible="false"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Assigned to:">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddl_da" runat="server">
                                                                                </asp:DropDownList>
                                                                                    <asp:Label ID="lblReq_dr" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdf_da" runat="server" Value='<%# Bind("assignee_drawing") %>'/>
                                                                            </ItemTemplate>
                                                                              <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_statusDA" runat="server" Text="-"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Equiment /Price Schedule Assigned to:" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddl_epsa" runat="server">
                                                                                </asp:DropDownList>
                                                                                  <asp:Label ID="lblReq_eq" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdf_epsa" runat="server" Value='<%# Bind("assignee_equip") %>'/>
                                                                            </ItemTemplate>
                                                                                 <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_statusDPSA" runat="server" Text="-"></asp:Label>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document Assign to:" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:DropDownList ID="ddl_dsa" runat="server">
                                                                                </asp:DropDownList>
                                                                               <asp:Label ID="lblReq_doc" runat="server" Text="*" ForeColor="Red" visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdf_dsa" runat="server" Value='<%# Bind("assignee_tech_doc") %>'/>
                                                                            </ItemTemplate>
                                                                                 <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status" Visible="false">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_statusDSA" runat="server" Text="-"></asp:Label>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                         <asp:TemplateField HeaderText="Rounting">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_headDept" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_headSect" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_headProject" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_assiDept" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_dept" runat="server"  Visible="False"></asp:Label>
                                                                                <asp:ImageButton ID="imgbt" runat="server" src="../../images/plus.png" width="20px" height="20px" OnClick="imgbt_Click"/>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <br />
                                    <div style="text-align: right">
                                        <asp:Button ID="bt_assign" runat="server" class="btn btn-primary" Text="Assign" OnClick="bt_assign_Click" />
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 20px;">
        </div>
        <div align="center">
            <table style="width: 95%">
                <tr style="padding-left: 5%">
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <td style="text-align: left; color: #006699"><b>Bidding Document:</b></td>

                            </tr>
                            <tr>
                                <td rowspan="2">
                                    <asp:GridView ID="gv_MainDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gv_MainDoc_RowDataBound1">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <a href="JavaScript:divexpandcollapseDoc('divDoc<%# Eval("posDocid ") %>');">
                                                        <img id='imgdivDoc<%# Eval("posDocid") %>' border="0" src="../../images/plus.png" width="20px" height="20px" /></a>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="กอง">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_division" runat="server"></asp:Label>
                                                    <tr id="tddivDoc<%# Eval("posDocid ") %>" style="display: none;">
                                                        <td colspan="3">
                                                            <div id="divDoc<%# Eval("posDocid ") %>" style="display: none;">
                                                                <asp:GridView ID="gv_biding" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gv_biding_RowDataBound">
                                                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                                    <RowStyle ForeColor="#000066" />
                                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                                                    <Columns>
                                                                        <asp:TemplateField HeaderText="No.">

                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_no" runat="server"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <%-- <asp:TemplateField>
                                     
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>--%>
                                                                        <asp:TemplateField HeaderText="Ducument Type">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_docType" runat="server" Font-Underline="False"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Document No.">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_docNo" runat="server"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Description">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_Desc" runat="server"></asp:Label>
                                                                            </ItemTemplate>

                                                                            <ItemStyle HorizontalAlign="Left" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Sheet No.">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_sheetNo" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Schedule No.">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_scheduleNo" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Schedule Name">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_scheduleName" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:LinkButton ID="lnk_viewDoc" runat="server">View Doc</asp:LinkButton>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="Status">
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="lb_status" runat="server"></asp:Label>
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>


                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>

                                    <%--<asp:GridView ID="gv_biding" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"   EmptyDataText="No data found." OnRowDataBound="gv_biding_RowDataBound" >
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px"/>
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center"/>
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    
                                    <ItemTemplate>
                                        <asp:Label ID="lb_no" runat="server"></asp:Label>
                                    </ItemTemplate>
                                    
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                     
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Ducument Type">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_docType" runat="server"  Font-Underline="False"></asp:Label>
                                    </ItemTemplate>
                                    
                                    <ItemStyle HorizontalAlign="Left"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_docNo" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                   
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_Desc" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                 
                                    <ItemStyle HorizontalAlign="Left"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Sheet No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_sheetNo" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_scheduleNo" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                     <ItemStyle HorizontalAlign="Center"/>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_scheduleName" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lb_status" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 20px;">
        </div>
        <div align="center">
            <table style="width: 95%">
                <tr style="padding-left: 5%">
                    <td>
                        <table style="width: 100%">
                            <tr>
                                <%-- <td style="text-align:left;color:#006699"><b>Other Document:</b></td>--%>
                                <td></td>
                            </tr>
                            <tr>
                                <td rowspan="2">
                                    <asp:GridView ID="gv_docOther" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gv_docOther_RowDataBound">
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="No.">

                                                <ItemTemplate>
                                                    <asp:Label ID="lb_no" runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>

                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Ducument Type">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_docType" runat="server" Font-Underline="False"></asp:Label>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Document No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_docNo" runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Description">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_Desc" runat="server"></asp:Label>
                                                </ItemTemplate>

                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Sheet No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_sheetNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleNo" runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schedule Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_scheduleName" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField></asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lb_status" runat="server"></asp:Label>
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div style="height: 20px">
        </div>
        <div style="text-align: right; width: 96.5%">
            <asp:Button ID="bt_submit" runat="server" Text="Close" class="btn btn-primary" />
        </div>
         <div id="myModal"  runat="server" class="modal">  
            <div class="modal-content">
                        <div id="dvRounting" runat="server" style="display:none;width:100%">
                    <asp:Label ID="Label56" runat="server" style="text-align: left; color: #006699;margin-left:15px"><b>Rounting</b></asp:Label>
                     <table align="center" style="width:95%;" id="tbRounting" runat="server">
                        <tr>
                             <td colspan="5">
                                 <asp:RadioButtonList ID="rd_approval" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rd_approval_SelectedIndexChanged" AutoPostBack="true">
                                     <asp:ListItem Text="Approval option 1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Approval option 2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Approval option 3" Value="3"></asp:ListItem>
                                 </asp:RadioButtonList>
                             </td>
                         </tr>
                        <tr>
                            <td style="width:20%" id="td1">
                                <asp:Label ID="lb_headDept" runat="server" Text="Approve 1"></asp:Label>
                            </td>
                            <td style="width:20%" id="td2">
                                 <asp:Label ID="lb_headSect" runat="server" Text="Approve 2"></asp:Label>
                            </td>
                            <td style="width:20%" id="td3">
                                <asp:Label ID="lb_headProject" runat="server" Text="Approve 3"></asp:Label>
                            </td>
                            <td style="width:20%" id="td4">
                                <asp:Label ID="lb_assiDept" runat="server" Text="Approve 4"></asp:Label>
                            </td>
                            <td style="width:20%" id="td5">
                                <asp:Label ID="lb_dept" runat="server" Text="Approve 5" ></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td id="td1_1" >
                                <asp:DropDownList ID="ddl_headDept" runat="server" Width="140px"></asp:DropDownList>
                                
                            </td>
                            <td id="td2_2">
                                <asp:DropDownList ID="ddl_headSect" runat="server"  Width="140px"></asp:DropDownList>
                            </td>
                            <td id="td3_3">
                                <asp:DropDownList ID="ddl_headProject" runat="server" Width="140px"></asp:DropDownList>
                            </td>
                            <td id="td4_4">
                                <asp:DropDownList ID="ddl_assiDept" runat="server" Width="140px"></asp:DropDownList>
                            </td>
                            <td id="td5_5">
                                <asp:DropDownList ID="ddl_dept" runat="server" Width="140px"></asp:DropDownList>

                            </td>
                        </tr>
                         <tr>
                             <td style="text-align:right" colspan="5">
                                 <asp:Label ID="lb_rowindex" runat="server"  Visible="False"></asp:Label>
                                 <asp:Button ID="bt_save" runat="server" Text="Save" class="btn btn-primary" OnClick="bt_save_Click"/>
                                 <asp:Button ID="bt_canc" runat="server" Text="Cencel" class="btn btn-danger" OnClick="bt_canc_Click"/>
                             </td>
                         </tr>
                    </table>
                </div>
                </div>
             </div>

          <div id="dvViewActivity"  runat="server" class="modal" > <%--   class="modal"  --%>
            <div class="modal-content">
                 <table  style="width: 100%">
                     <tr>
                         <td style="width:200px;vertical-align:top;">
                             <asp:Label ID="lblBefore" runat="server" ></asp:Label>
                             </td>
                             <td align="center" style="width:90px">
                             <asp:Image ID="Image1" runat="server" ImageUrl="../../images/fast-forward.png" Width="90px" Height="50px" />
                                 </td>
                          <td  style="width:200px;">
                                <asp:Label ID="lblNow" runat="server" ></asp:Label>
                         </td>
                          <td align="center" style="width:90px">
                             <asp:Image ID="Image2" runat="server" ImageUrl="../../images/fast-forward.png" Width="90px" Height="50px"/>
                         </td>
                          <td  style="width:200px;vertical-align:top;">
                                <asp:Label ID="lblNext" runat="server" ></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="vertical-align:top;">
                               <asp:GridView ID="gvBefore" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                             <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" Text='<%# Bind("apv_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Activity Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActiname" runat="server" Text='<%# Bind("ps_subject") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppr" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server"  Text='<%# Bind("emp_apvdate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server" Text='<%# Bind("apv_status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                         </td>
                         <td>
                         </td>
                             <td style="vertical-align:top;">
                               <asp:GridView ID="gvNow" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                             <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" Text='<%# Bind("apv_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Activity Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActiname" runat="server" Text='<%# Bind("ps_subject") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppr" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server"   Text='<%# Bind("emp_apvdate") %>'  ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server" Text='<%# Bind("apv_status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                         </td>
                         <td></td>
                             <td style="vertical-align:top;">
                              <asp:GridView ID="gvNext" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                             <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" Text='<%# Bind("apv_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderText="Activity Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActiname" runat="server" Text='<%# Bind("ps_subject") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppr" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server"   Text='<%# Bind("emp_apvdate") %>' ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server" Text='<%# Bind("apv_status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                         </td>
                     </tr>
                     <tr>
                         <td colspan="5" style="text-align:right">
                             <%--<asp:Button ID="btnCloseView" runat="server" Text="Close" class="btn btn-danger" OnClick="btnCloseView_Click" />--%>
                              <button onclick="document.getElementById('dvViewActivity').style.display='none';return false;" class="btn btn-danger" visible="false">Close</button>
                         </td>
                     </tr>
                    </table>
                </div>
             </div>
               <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRev" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
         <asp:HiddenField ID="hidMcShort" runat="server" />
        <asp:HiddenField ID="hidExpDiv" runat="server" />
        <asp:HiddenField ID="hidFindIndexM" runat="server" />
        <asp:HiddenField ID="hidFindIndexS" runat="server" />
         <asp:HiddenField ID="hidTaskID" runat="server" />
         <asp:HiddenField ID="hidchkSelect" runat="server" />
          <asp:HiddenField ID="hidMode" runat="server" />
    </form>
</body>
</html>
