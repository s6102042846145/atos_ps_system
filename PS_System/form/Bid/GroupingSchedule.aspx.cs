﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using System.Data;
using PS_Library;
using System.Web.UI.HtmlControls;

namespace PS_System.form.Bid
{
	public partial class GroupingSchedule : System.Web.UI.Page
	{
		clsGroupingSchedule objsch = new clsGroupingSchedule();
		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				if (Request.QueryString["zuserlogin"] != null)
					this.hidLogin.Value = Request.QueryString["zuserlogin"];
				else
					this.hidLogin.Value = "z572993";

				if (Request.QueryString["bidno"] != null)
					this.hidBidNo.Value = Request.QueryString["bidno"];
				else
					this.hidBidNo.Value = "TS12-S-02";

				if (Request.QueryString["rev"] != null)
					hidRev.Value = Request.QueryString["rev"];
				else
					hidRev.Value = "0";

				EmpInfoClass empInfo = new EmpInfoClass();
				Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
				hidDept.Value = emp.ORGSNAME4;
				hidSect.Value = emp.ORGSNAME5;
				bind_detail();
				bind_bidDeshboard();
				bind_dataSch();
				bind_dataNewSch();
			}
		}
		private void bind_dataNewSch()
		{
			string strCode = "";
			string strPath = "";
			DataTable dt = new DataTable();
			dt = objsch.getisSelect(hidBidNo.Value, hidRev.Value);
      
			for (int i = 0; i < dt.Rows.Count; i++)
			{
				txtSch_new.Text = dt.Rows[0]["schedule_new"].ToString();
				txtSch_desc.Text = dt.Rows[0]["schedule_desc_new"].ToString();

				if (strCode == "") strCode = dt.Rows[i]["equip_code"].ToString();
				else strCode += "," + dt.Rows[i]["equip_code"].ToString();

				if (strPath == "") strPath = dt.Rows[i]["schedule_old"].ToString();
				else
					strPath += "," + dt.Rows[i]["schedule_old"].ToString();
			}
           
			strPath = strPath.Replace(",", "','");
			strCode = strCode.Replace(",", "','");
			DataTable dt2 = objsch.getDataEquip(hidBidNo.Value, hidRev.Value, strPath, strCode, "","","1","");
			dt2.Columns.Add("item_no_new");
			dt2.Columns.Add("delivery_date");
			if (dt2.Rows.Count > 0)
			{
				//DataView dv = new DataView(dt2);
				//dv.Sort = "equip_code asc";
				//dt2.DefaultView.Sort = "equip_code asc";
				for (int i = 0; i < dt.Rows.Count; i++)
				{
                    //if ( i < dv.Count)
                    //{
                    //	dv[i]["item_no_new"] = dt.Rows[i]["item_no_new"].ToString();
                    //	dv[i]["delivery_date"] = dt.Rows[i]["delivery_date"].ToString();
                    //}
                    DataRow dr = dt2.Rows[i];
                    dr["item_no_new"] = dt.Rows[i]["item_no_new"].ToString();
                    dr["delivery_date"] = dt.Rows[i]["delivery_date"].ToString();
                    dt2.AcceptChanges();
                }


                ViewState["dtCreatesch"] = dt2;
				gvCreate.DataSource = dt2;
				gvCreate.DataBind();
				dvPartAdd.Style["display"] = "block";
				dvCreate.Style["display"] = "table-cell";
				dvSave.Style["display"] = "block";

			}
            else
            {
				dvPartAdd.Style["display"] = "none";
            }
			
		}
		private void bind_dataSch()
		{
			DataTable dt = new DataTable();
			dt = objsch.getDataSch(hidBidNo.Value, hidRev.Value);
			if (dt.Rows.Count > 0)
			{
				hidProject.Value = dt.Rows[0]["project_id"].ToString();
				gvEquip.DataSource = dt;
				gvEquip.DataBind();
			}
		}
		private void bind_detail()
		{
			DataTable dt = new DataTable();
			DateTime dtSta = new DateTime();
			DateTime dtEnd = new DateTime();
			clsBidDashbaord objdb = new clsBidDashbaord();
			dt = objdb.GetDetail(hidBidNo.Value);
			if (dt.Rows.Count > 0)
			{
				txt_bidNo.Text = dt.Rows[0]["bid_no"].ToString();
				txt_rev.Text = dt.Rows[0]["bid_revision"].ToString();
				txt_desc.Text = dt.Rows[0]["bid_desc"].ToString();
                if (dt.Rows[0]["p6_plan_start"].ToString() != null && dt.Rows[0]["p6_plan_start"].ToString() != "")
				{
					dtSta = Convert.ToDateTime(dt.Rows[0]["p6_plan_start"].ToString());
					txtStartDate.Text = dtSta.ToString("dd/MM/yyyy");
				}
				if (dt.Rows[0]["p6_plan_end"].ToString() != null && dt.Rows[0]["p6_plan_end"].ToString() != "")
				{
					dtEnd = Convert.ToDateTime(dt.Rows[0]["p6_plan_end"].ToString());
					txtEndDate.Text = dtEnd.ToString("dd/MM/yyyy");
				}
				if (dt.Rows[0]["completion_date"].ToString() != "") { lblCom.Text = dt.Rows[0]["completion_date"].ToString(); dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;"); }
				else dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
				txtRemark.Text = dt.Rows[0]["remark"].ToString();
			}
			else
			{
				dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
			}


		}
		private void bind_bidDeshboard()
		{
			clsBidDashbaord objdb = new clsBidDashbaord();
			DataTable dt = objdb.GetSummary(txt_bidNo.Text);
			if (dt.Rows.Count > 0)
			{
				gvJobSumary.DataSource = dt;
				gvJobSumary.DataBind();
				gvJobSumary.UseAccessibleHeader = true;
				gvJobSumary.HeaderRow.TableSection = TableRowSection.TableHeader;
			}
			else
			{
				gvJobSumary.DataSource = null;
				gvJobSumary.DataBind();
			}

		}
		protected void gvJobSumary_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.Header)
			{

			}
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lb_scheduleNo = (Label)e.Row.FindControl("lb_scheduleNo");
				lb_scheduleNo.Text = drv["schedule_no"].ToString();
				Label lb_scheduleDesc = (Label)e.Row.FindControl("lb_scheduleDesc");
				lb_scheduleDesc.Text = drv["schedule_name"].ToString();
				Label lb_jobNo = (Label)e.Row.FindControl("lb_jobNo");
				Label lb_wa = (Label)e.Row.FindControl("lb_wa");
				lb_wa.Text = drv["wa_url"].ToString();

				lb_jobNo.Text = drv["job_no"].ToString();
				Label lb_jobDesc = (Label)e.Row.FindControl("lb_jobDesc");
				lb_jobDesc.Text = drv["job_desc"].ToString();
				Label lb_revision = (Label)e.Row.FindControl("lb_revision");
				lb_revision.Text = drv["job_revision"].ToString();

			}
		}
		protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
		{
			string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
			Response.Redirect(strUrl);
		}

		protected void btnClose_Click(object sender, EventArgs e)
		{
			myModal.Style["display"] = "none";
		}

		protected void btnView_Click(object sender, EventArgs e)
		{
			myModal.Style["display"] = "block";
			Button btn = (Button)sender;
			GridViewRow gvr = btn.NamingContainer as GridViewRow;
			string lblSch = ((Label)gvr.FindControl("lblSch")).Text;
			DataTable dt = new DataTable();
			dt = objsch.getDataEquip(hidBidNo.Value, hidRev.Value, lblSch,"", "","","","");
			if (dt.Rows.Count > 0)
			{
				gvPopup.DataSource = dt;
				gvPopup.DataBind();

			}
		}
		protected void btnGroup_Click(object sender, EventArgs e)
		{
			string strPath = "";
			string strProject = "";
			foreach (GridViewRow gvr in gvEquip.Rows)
			{
				string lblProject = ((Label)gvr.FindControl("lblProject")).Text;
				string lblSch = ((Label)gvr.FindControl("lblSch")).Text;
				CheckBox cb = (CheckBox)gvr.FindControl("cbAssign");
                if (cb.Checked)
                {
					if (strPath == "") strPath = lblSch;
					else strPath += "," + lblSch;
					if (strProject == "") strProject = lblProject;
					else
					{
						if (lblProject.IndexOf(strProject) > -1) { }
						else
							strProject += "," + lblProject;
					}
				}
			}
			strPath = strPath.Replace(",", "','");
			strProject = strProject.Replace(",", "','");
			DataTable dt = new DataTable();
            dt = objsch.getDataEquip(hidBidNo.Value, hidRev.Value, strPath,"", "","","0","");
			ViewState["dtNewsch"] = dt;
			gvEdit.DataSource = dt;
			gvEdit.DataBind();

			ddlSche_edit.Items.Clear();
			DataTable dt2 = new DataTable();
			dt2 = objsch.getDisDataSch(hidBidNo.Value, hidRev.Value, strProject, strPath);
			for (int d = 0; d < dt2.Rows.Count; d++)
			{
				ddlSche_edit.Items.Insert(d, new ListItem(dt2.Rows[d]["project_id"].ToString() + "/" + dt2.Rows[d]["part_name"].ToString() , dt2.Rows[d]["part_name"].ToString()));
			}
			ddlSche_edit.Items.Insert(0, new ListItem("<-- Select All -->", ""));
			dvPartAdd.Style["display"] = "block";
			ddlSche_edit.Visible = true;
			lblSch.Visible = true;
		}

		protected void gvPopup_RowDataBound(object sender, GridViewRowEventArgs e)
        {
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				DataRowView drv = (DataRowView)e.Row.DataItem;
				Label lblManu = (Label)e.Row.FindControl("lblManu");
				string rep = drv["menufac_file"].ToString().Replace("&lt;", "<");
				lblManu.Text = rep.Replace("&gt;", ">");
			}
		}

        protected void ImgRight_Click(object sender, ImageClickEventArgs e)
        {
			string strPath = "";
			string strCode = "";
			string strItm = "";
			string strRef = "";
			string strJob = "";
			string index = "";
			DataTable dt = new DataTable();
            if (ViewState["dtNewsch"] != null) dt = (DataTable)ViewState["dtNewsch"];
            for (int i = 0; i < gvEdit.Rows.Count; i++)
            {
                CheckBox cb = (CheckBox)gvEdit.Rows[i].FindControl("cbEdit");
				string part = ((Label)gvEdit.Rows[i].FindControl("lblEditAssiSch")).Text;
				string code = ((Label)gvEdit.Rows[i].FindControl("lblEditEquipC")).Text;
				string itmno = ((Label)gvEdit.Rows[i].FindControl("lblEditAssiItm")).Text;
				string refer = ((Label)gvEdit.Rows[i].FindControl("lblEditPart")).Text;
				string jobno = ((Label)gvEdit.Rows[i].FindControl("lblEditJobno")).Text; 
				if (cb.Checked)
				{
					if (strPath == "") strPath = part;
					else strPath += "," + part;
					if (strCode == "") strCode = code;
					else
					{
						if (code.IndexOf(strCode) > -1) { }
						else
							strCode += "," + code;
					}
					if (strItm == "") strItm = itmno;
					else strItm += "," + itmno;

					if (index == "") index = i.ToString();
					else index += "," + i.ToString();

					if (strRef == "") strRef = refer;
					else strRef += "," + refer;

					strJob += (strJob == "" ? "" : ",") + jobno;
				}
            }
            for (int i = 0; i < index.Length; i++)
			{
				string[] sp = index.ToString().Split(',');
				if (i == sp.Length) break;
				dt.Rows.RemoveAt(Convert.ToInt32(sp[i]) - i);
			}
     
			strPath = strPath.Replace(",", "','");
			strCode = strCode.Replace(",", "','");
			strItm = strItm.Replace(",", "','");
			strRef = strRef.Replace(",", "','");
			strJob = strJob.Replace(",", "','");
			ViewState["dtNewsch"] = dt;
            gvEdit.DataSource = dt;
            gvEdit.DataBind();

			DataTable dt2 = new DataTable();
			dt2.Columns.Add("equip_code");
			dt2.Columns.Add("equip_desc");
			dt2.Columns.Add("equip_qty");
			dt2.Columns.Add("bid_no");
			dt2.Columns.Add("bid_revision");
			dt2.Columns.Add("job_no");
			dt2.Columns.Add("refer_part");
			dt2.Columns.Add("substation");
			dt2.Columns.Add("equip_group");
			dt2.Columns.Add("part_assign");
			dt2.Columns.Add("item_no");
			dt2.Columns.Add("menufac_file");
			dt2.Columns.Add("is_new_schedule");
			dt2.Columns.Add("item_no_new");
			dt2.Columns.Add("delivery_date");
			if (ViewState["dtCreatesch"] != null) dt2 = (DataTable)ViewState["dtCreatesch"];
			DataTable dtAdd = objsch.getDataEquip(hidBidNo.Value, hidRev.Value, strPath, strCode, strItm, strRef,"", strJob);
			foreach (DataRow dtRow in dtAdd.Rows)
			{
				string filter = "equip_code='" + dtRow["equip_code"].ToString() + "' and refer_part ='" + dtRow["refer_part"].ToString() + "' and job_no ='" + dtRow["job_no"].ToString() + "'";
				DataRow[] drSelect = dt2.Select(filter);
				if (drSelect.Length == 0)
				{
					DataRow row = dt2.NewRow();
					row["equip_code"] = dtRow["equip_code"].ToString();
					row["equip_desc"] = dtRow["equip_desc"].ToString();
					row["equip_qty"] = dtRow["equip_qty"].ToString();
					row["bid_no"] = dtRow["bid_no"].ToString();
					row["bid_revision"] = dtRow["bid_revision"].ToString();
					row["job_no"] = dtRow["job_no"].ToString();
					row["refer_part"] = dtRow["refer_part"].ToString();
					row["substation"] = dtRow["substation"].ToString();
					row["equip_group"] = dtRow["equip_group"].ToString();
					row["part_assign"] = dtRow["part_assign"].ToString();
					row["item_no"] = dtRow["item_no"].ToString();
					row["menufac_file"] = dtRow["menufac_file"].ToString();
					row["is_new_schedule"] = dtRow["is_new_schedule"].ToString();
					dt2.Rows.Add(row);
				}
			}
			ViewState["dtCreatesch"] = dt2;
			gvCreate.DataSource = dt2;
			gvCreate.DataBind();
			dvCreate.Style["display"] = "table-cell";
			dvSave.Style["display"] = "block";
			if (gvEdit.Rows.Count == 0) { ddlSche_edit.Visible = false; lblSch.Visible = false; }
			else { ddlSche_edit.Visible = true; lblSch.Visible = false; }


		}

		protected void ImgLeft_Click(object sender, ImageClickEventArgs e)
        {
			string strPath = "";
			string strCode = "";
			string strItm = "";
			string strRef = "";
			string strJob = "";
			string index = "";
			DataTable dt = new DataTable();
			if (ViewState["dtCreatesch"] != null) dt = (DataTable)ViewState["dtCreatesch"];
			for (int i = 0; i < gvCreate.Rows.Count; i++)
			{
				CheckBox cb = (CheckBox)gvCreate.Rows[i].FindControl("cbCreate");
				string part = ((Label)gvCreate.Rows[i].FindControl("lblCreateAssiSch")).Text;
				string code = ((Label)gvCreate.Rows[i].FindControl("lblCreateEquipC")).Text;
				string itmno = ((Label)gvCreate.Rows[i].FindControl("lblCreateAssiItm")).Text;
				string refer = ((Label)gvCreate.Rows[i].FindControl("lblCreatePart")).Text;
				string jobno = ((Label)gvCreate.Rows[i].FindControl("lblCreateJobno")).Text; 
				if (cb.Checked)
				{
					if (strPath == "") strPath = part;
					else strPath += "," + part;
					if (strCode == "") strCode = code;
					else
					{
						if (code.IndexOf(strCode) > -1) { }
						else
							strCode += "," + code;
					}
					if (strItm == "") strItm = itmno;
					else strItm += "," + itmno;

					if (index == "") index = i.ToString();
					else index += "," + i.ToString();

					if (strRef == "") strRef = refer;
					else strRef += "," + refer;

					strJob += (strJob == "" ? "" : ",") + jobno;
				}
			}
			for (int i = 0; i < index.Length; i++)
			{
				string[] sp = index.ToString().Split(',');
				if (i == sp.Length) break;
				dt.Rows.RemoveAt(Convert.ToInt32(sp[i]) - i);
			}
			strPath = strPath.Replace(",", "','");
			strCode = strCode.Replace(",", "','");
			strItm = strItm.Replace(",", "','");
			strRef = strRef.Replace(",", "','");
			strJob = strJob.Replace(",", "','");
			ViewState["dtCreatesch"] = dt;
			gvCreate.DataSource = dt;
			gvCreate.DataBind();

			DataTable dt2 = new DataTable();
			dt2.Columns.Add("equip_code");
			dt2.Columns.Add("equip_desc");
			dt2.Columns.Add("equip_qty");
			dt2.Columns.Add("bid_no");
			dt2.Columns.Add("bid_revision");
			dt2.Columns.Add("job_no");
			dt2.Columns.Add("refer_part");
			dt2.Columns.Add("substation");
			dt2.Columns.Add("equip_group");
			dt2.Columns.Add("part_assign");
			dt2.Columns.Add("item_no");
			dt2.Columns.Add("menufac_file");
			dt2.Columns.Add("is_new_schedule");
			if (ViewState["dtNewsch"] != null) dt2 = (DataTable)ViewState["dtNewsch"];
			DataTable dtAdd = objsch.getDataEquip(hidBidNo.Value, hidRev.Value, strPath, strCode, strItm, strRef,"", strJob);
			foreach (DataRow dtRow in dtAdd.Rows)
			{
				string filter = "equip_code='" + dtRow["equip_code"].ToString() + "' and refer_part ='" + dtRow["refer_part"].ToString() + "' and job_no ='"+ dtRow["job_no"].ToString() + "'";
				DataRow[] drSelect = dt2.Select(filter);
				if (drSelect.Length == 0)
				{
					DataRow row = dt2.NewRow();
					row["equip_code"] = dtRow["equip_code"].ToString();
					row["equip_desc"] = dtRow["equip_desc"].ToString();
					row["equip_qty"] = dtRow["equip_qty"].ToString();
					row["bid_no"] = dtRow["bid_no"].ToString();
					row["bid_revision"] = dtRow["bid_revision"].ToString();
					row["job_no"] = dtRow["job_no"].ToString();
					row["refer_part"] = dtRow["refer_part"].ToString();
					row["substation"] = dtRow["substation"].ToString();
					row["equip_group"] = dtRow["equip_group"].ToString();
					row["part_assign"] = dtRow["part_assign"].ToString();
					row["item_no"] = dtRow["item_no"].ToString();
					row["menufac_file"] = dtRow["menufac_file"].ToString();
					row["is_new_schedule"] = dtRow["is_new_schedule"].ToString();
					dt2.Rows.Add(row);
				}
			}
			ViewState["dtNewsch"] = dt2;
			gvEdit.DataSource = dt2;
			gvEdit.DataBind();
            if (gvCreate.Rows.Count == 0)
            {
				dvCreate.Style["display"] = "none";
				dvSave.Style["display"] = "none";
			}
            if (gvEdit.Rows.Count == 0) { ddlSche_edit.Visible = false; lblSch.Visible = false; }
            else { ddlSche_edit.Visible = true; lblSch.Visible = false; }
        }

        protected void ddlSche_edit_SelectedIndexChanged(object sender, EventArgs e)
        {
			string strPath = "";
			foreach (GridViewRow gvr in gvEquip.Rows)
			{
				string lblSch = ((Label)gvr.FindControl("lblSch")).Text;
				CheckBox cb = (CheckBox)gvr.FindControl("cbAssign");
				if (cb.Checked)
				{
					if (strPath == "") strPath = lblSch;
					else strPath += "," + lblSch;
				}
			}
			strPath = strPath.Replace(",", "','");
			DataTable dt = new DataTable();
			dt = objsch.getDataEquip(hidBidNo.Value, hidRev.Value, strPath,"", "","","0","");
			DataView dv = new DataView(dt);
			if (ddlSche_edit.SelectedValue!="")
				dv.RowFilter = " part_assign in('" + ddlSche_edit.SelectedValue + "')";
			DataTable dtNew = dv.ToTable();
			ViewState["dtNewsch"] = dtNew;
			gvEdit.DataSource = dv;
			gvEdit.DataBind();
		}

        protected void btnSave_Click(object sender, EventArgs e)
		{
			string project = "";
			string chkcode = "";
			string chkitmno_old = "";
			string chkjob = "";
			int i = gvCreate.Rows.Count;
            if (i != 0)
            {
				if (ddlSche_edit.SelectedValue != "")
				{
					string[] sproject = ddlSche_edit.SelectedItem.Text.Split('/');
					project = sproject[0];

				}
				else
					project = hidProject.Value;
				objsch.DeleteBeforSave(hidBidNo.Value, hidRev.Value, project);
				foreach (GridViewRow gvr in gvCreate.Rows)
				{
					string part = ((Label)gvr.FindControl("lblCreateAssiSch")).Text;
					string code = ((Label)gvr.FindControl("lblCreateEquipC")).Text;
					string itmno = ((TextBox)gvr.FindControl("txtItmno")).Text;
					string jobno = ((Label)gvr.FindControl("lblCreateJobno")).Text;
					string itmno_old = ((Label)gvr.FindControl("lblCreateAssiItm")).Text; 
					HtmlInputText dateDelivery = (HtmlInputText)gvr.FindControl("dateDelivery");

					//chkcode += (chkcode == "" ? "" : ",") + code;
					//chkitmno_old += (chkitmno_old == "" ? "" : ",") + itmno_old;
					//chkjob += (chkjob == "" ? "" : ",") + jobno;
					objsch.InsertNewSch(hidBidNo.Value, hidRev.Value, project, part, txtSch_new.Text, txtSch_desc.Text, itmno, code, dateDelivery.Value, hidLogin.Value, jobno, itmno_old);
					objsch.updateIsSelect(hidBidNo.Value, hidRev.Value, code, jobno, itmno_old);

				}
				//chkcode = chkcode.Replace(",", "','");
				//chkitmno_old = chkitmno_old.Replace(",", "','");
				//chkjob = chkjob.Replace(",", "','");
				//objsch.updateIsSelect(hidBidNo.Value, hidRev.Value, chkcode, chkjob, chkitmno_old);
				//foreach (GridViewRow gvr in gvEdit.Rows)
				//{
				//	//string part = ((Label)gvr.FindControl("lblCreateAssiSch")).Text;
				//	string code = ((Label)gvr.FindControl("lblEditEquipC")).Text;
				//	string itmno = ((Label)gvr.FindControl("lblEditAssiItm")).Text;
				//	string jobno = ((Label)gvr.FindControl("lblEditJobno")).Text; 


				//	objsch.updateNotSelect(hidBidNo.Value, hidRev.Value, code, jobno, itmno);
				//}
				ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
				bind_dataNewSch();

			}
            else
            {
				ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('No data');", true);
			}
		}
    }
}