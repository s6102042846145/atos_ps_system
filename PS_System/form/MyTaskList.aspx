﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="MyTaskList.aspx.cs" Inherits="PS_System.form.MyTaskList" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>My Task List</title>
    <style type="text/css">
        .picker{

            vertical-align:middle;
        }

    </style>
    <link href="../Content/w3.css" rel="stylesheet" />

    <script src="../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../Scripts/jquery-ui.js"></script>

    <link href="../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../Scripts/table/js/addons/datatables.min.js"></script>

    <script type="text/javascript">
        function editDoc() {
            window.open('../Bid/ChangeBidPlan.aspx', '_blank');
            return false;
        }

        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        $(function () {
            $(".picker").datepicker();
        });

        $(document).ready(function () {

            $('[id*=Multi_jobno]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=Multi_bidno]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
          <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - My Task List"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dvJob" runat="server" style="padding: 10px 10px 10px 10px;">
            <table style="width: 100%;">

                  <tr>
                       <td>
                        <asp:Label ID="Label1" runat="server" Text="Label">Job No.:</asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="Multi_jobno" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                       <%--       <td style="width:2%">
                        <asp:Label ID="lblTask" runat="server" Text="Label">Task Name:</asp:Label>
                        &nbsp;&nbsp;<asp:ListBox ID="Multi_Task" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>--%>
                </tr>
                <tr>
                  <td style="vertical-align:bottom">
                     <br />
                    <p>Plan Start Date - End Date:&nbsp;&nbsp;<input type="text" id="dateStart" runat="server" class="picker" autocomplete="off"> -  <input type="text" id="dateEnd" runat="server" class="picker" autocomplete="off"> </p>
                    </td>
                </tr>
                 <tr>
                    <td style="text-align: left;">
                        <br />
                          <asp:Button ID="btnsearch" runat="server" Text="Search" CssClass="btn btn-primary" Width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnsearch_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-bottom:15px;">
                          <br />
                        <asp:GridView ID="gvMyTask" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" OnRowDataBound="gvMyTask_RowDataBound" EmptyDataText="No data found.">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White"/>
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField HeaderText="Job No.">
                                    <ItemTemplate>
                                        <asp:Label ID="Label12" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                           <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                          <asp:GridView ID="gvSubTask" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="99%" BackColor="White" BorderColor="#CCCCCC" 
                                              OnRowDataBound="gvSubTask_RowDataBound" EmptyDataText="No data found.">
                                            <Columns>
                                                    <asp:TemplateField HeaderText="Task Name">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Label32" runat="server" Text='<%# Bind("p6_task_name") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Plan Start Date">
                                                    <ItemTemplate>
                                                      <asp:Label ID="Lalbel12" Text='<%# Eval("p6_plan_start", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Plan End Date">
                                                    <ItemTemplate>
                                                        <asp:Label ID="Lalbel22" runat="server" Text='<%# Eval("p6_plan_end", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>
                                      <%--          <asp:TemplateField HeaderText="Action">
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lnkGoto" runat="server"></asp:LinkButton>
                                                    </ItemTemplate>
                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                </asp:TemplateField>--%>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                             <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </ItemTemplate>
                               <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                             <asp:LinkButton ID="lnkGoto" runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
                <div id="dvBid" runat="server" style="padding: 10px 10px 10px 10px;">
            <table style="width: 100%;">

                  <tr>
                       <td>
                        <asp:Label ID="Label2" runat="server" Text="Label">Bid No.:</asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="Multi_bidno" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                       <%--       <td style="width:2%">
                        <asp:Label ID="lblTask" runat="server" Text="Label">Task Name:</asp:Label>
                        &nbsp;&nbsp;<asp:ListBox ID="Multi_Task" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>--%>
                </tr>
                <tr>
                  <td style="vertical-align:bottom">
                     <br />
                    <p>Plan Start Date - End Date:&nbsp;&nbsp;<input type="text" id="dateStbid" runat="server" class="picker" autocomplete="off"> -  <input type="text" id="dateEndbid" runat="server" class="picker" autocomplete="off"> </p>
                    </td>
                </tr>
                 <tr>
                    <td style="text-align: left;">
                        <br />
                          <asp:Button ID="btnSearchbid" runat="server" Text="Search" CssClass="btn btn-primary" Width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnSearchbid_Click"  />
                    </td>
                </tr>
                <tr>
                    <td colspan="6" style="padding-bottom:15px;">
                          <br />
                        <asp:GridView ID="gvMyTaskBid" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" EmptyDataText="No data found." OnRowDataBound="gvMyTaskBid_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White"/>
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:TemplateField HeaderText="Bid No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lblbidno" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                          <asp:GridView ID="gvSubTaskBid" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="99%" BackColor="White" BorderColor="#CCCCCC" 
                                              OnRowDataBound="gvSubTaskBid_RowDataBound" EmptyDataText="No data found.">
                                            <Columns>
                                                      <asp:TemplateField HeaderText="Task Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblbidtask" runat="server" Text='<%# Bind("p6_task_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Plan Start Date">
                                                <ItemTemplate>
                                                  <asp:Label ID="lblbidst" Text='<%# Eval("p6_plan_start", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Plan End Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblbidend" runat="server" Text='<%# Eval("p6_plan_end", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle Width="60px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                               <%--             <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkGotoBid" runat="server"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>--%>
                                            </Columns>
                                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                             <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </ItemTemplate>
                               <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Action">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="lnkGotoBid" runat="server"></asp:LinkButton>
                                                </ItemTemplate>
                                                <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Top" />
                                            </asp:TemplateField>
                            </Columns>
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
          <asp:HiddenField ID="hidLogin" runat="server" />
          <asp:HiddenField ID="hidMode" runat="server" />
          <asp:HiddenField ID="hidFilter" runat="server" />
    </form>
</body>
</html>
