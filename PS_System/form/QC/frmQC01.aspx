﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmQC01.aspx.cs" Inherits="PS_System.form.QC.frmQC01" %>

<%@ Register src="../PR/ucEmp.ascx" tagname="ucEmp" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>QC Dashboard</title>
     <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <style type="text/css">
        .auto-style1 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 10pt;
            color: rgb(35, 34, 34);
            background-color: white;
            border-color: silver;
            border-radius: 2px;
        }
        .auto-style2 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 9pt;
            width: 160px;
            background-color: transparent;
        }
        .auto-style3 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 10pt;
            color: rgb(35, 34, 34);
            background-color: white;
            border-color: silver;
            border-radius: 2px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding:5px">
              
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="QC Dashboard"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
                <div id ="divSearch" runat="server" class="div_90">
                    <table>
                        <tr>
                            <td colspan="5" style="text-align:right">
                                <asp:Label ID="lblMode" runat="server" CssClass="Label_md"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="Label_sm">BID No : </label></td>
                            <td><asp:DropDownList ID="ddlBidNo" runat="server" CssClass="ddl-md" AutoPostBack="true" OnSelectedIndexChanged="ddlBidNo_SelectedIndexChanged"/>&nbsp;<asp:Label ID="lblBidRevision" runat="server" CssClass="Label_sm"> </asp:Label></td>
                            <td><label class="Label_sm"> Schedule : </label></td>
                            <td><asp:DropDownList ID="ddlSchedule" runat="server" CssClass="ddl-md" AutoPostBack="true" OnSelectedIndexChanged="ddlSchedule_SelectedIndexChanged" /></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label class="auto-style2">Description : </label></td>
                            <td colspan="4"><asp:TextBox ID="txtBidDesc" runat="server" CssClass="auto-style1" TextMode="MultiLine" Width="901px"/></td>
                        </tr>
                        <tr>
                            <td><label class="Label_md"> Contract No. : </label></td>
                            <td><asp:TextBox ID="txtContractNo" runat="server" CssClass="Text_200"/></td>
                            <td><label class="Label_sm"> Vendor : </label></td>
                            <td><asp:TextBox ID="txtVendor" runat="server" CssClass="Text_200"/></td>
                            <td><asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn_normal_blue" /></td>
                        </tr>
                    </table>
                </div>
                <p></p>
                <div id="divData" runat="server" class="div_90">
                    <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" 
                        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                        EmptyDataText="No data found." 
                        Font-Names="tahoma" Font-Size="9pt" Width="95%" OnRowCommand="gv1_OnRowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        <Columns>
                              <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="gvSelectAll" AutoPostBack="true" OnCheckedChanged="gvSelectAll_CheckedChanged" CssClass="Text_60" runat="server" ></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvSelect" CssClass="Text_60" runat="server" ></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="gvItemNo" CssClass="Text_60" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="gvDescription" CssClass="Text_200" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Qty">
                                        <ItemTemplate>
                                            <asp:Label ID="gvQty" CssClass="Text_100" runat="server" Text='<%# Bind("Qty") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Unit">
                                        <ItemTemplate>
                                            <asp:Label ID="gvUnit" CssClass="Text_100" runat="server" Text='<%# Bind("Unit") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="WaiveTest">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvWT"  runat="server" Checked='<%# Bind("waive_test") %>' ></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Master Docs">
                                        <HeaderTemplate>
                                            <table style="border-style:solid; border-width:1px; border-color:gray;font-family:Tahoma;font-size:10pt;color:white;text-align:center">
                                                <tr>
                                                    <td colspan="3">
                                                        <label>Master Docs</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label>PD</label></td>
                                                    <td><label>Design Drawing</label></td>
                                                    <td><label>Typical</label></td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table style="border-style:solid; border-width:1px; border-color:gray;font-family:Tahoma;font-size:10pt;color:black;text-align:center">
                                                <tr>
                                                    <td>
                                                        <label> PD : </label>
                                                    </td>
                                                    <td>
                                                         <asp:LinkButton ID="gvlbtnPD" CssClass="Text_100" runat="server" Text='<%# Bind("ttPD") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <label> Designed Drawing : </label>
                                                    </td>
                                                    <td>
                                                         <asp:LinkButton ID="gvlbtnDD" CssClass="Text_100" runat="server" Text='<%# Bind("ttDD") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                    <td>
                                                        <label> Typical Drawing : </label>
                                                    </td>
                                                    <td>
                                                         <asp:LinkButton ID="gvlbtnTypical" CssClass="Text_100" runat="server" Text='<%# Bind("ttTD") %>'></asp:LinkButton>
                                                    </td>
                                                </tr>
                                            </table>
                                             
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="QC Monitoring">
                                        <HeaderTemplate>
                                            <table style="border-style:solid; border-width:1px; border-color:gray;font-family:Tahoma;font-size:10pt;color:white; text-align:center">
                                                <tr>
                                                    <td colspan="9">
                                                        <label>QC Monitoring</label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><label class="Label_40" style="width:30px;">MS</label></td>
                                                    <td><label class="Label_40" style="width:30px;">QP</label></td>
                                                    <td><label class="Label_40" style="width:30px;">TS</label></td>
                                                    <td><label class="Label_40" style="width:30px;">TP</label></td>
                                                    <td><label class="Label_40" style="width:30px;">IW</label></td>
                                                    <td><label class="Label_40" style="width:30px;">IR</label></td>
                                                    <td><label class="Label_40" style="width:30px;">CR</label></td>
                                                    <td><label class="Label_40" style="width:30px;">TR</label></td>
                                                    <td><label class="Label_40" style="width:30px;">LATR</label></td>
                                                </tr>
                                            </table>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <table style="border-style:solid; border-width:1px; border-color:gray;font-family:Tahoma;font-size:10pt;color:white;text-align:center">
                                                <tr>
                                                    <td><asp:ImageButton ID="gvMS" runat="server" CssClass="Label_40" Width="30px"  ToolTip="Manufacturing and Delivery Schedule" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvQP" runat="server" CssClass="Label_40" Width="30px"  ToolTip="Quality Assurance Program" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvTS" runat="server" CssClass="Label_40" Width="30px"  ToolTip="Test Schedule" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvTP" runat="server" CssClass="Label_40" Width="30px"   ToolTip="Test Procedure" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvIW" runat="server" CssClass="Label_40" Width="30px"   ToolTip="Inspection Work" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvIR" runat="server" CssClass="Label_40" Width="30px"  ToolTip="Inspection Report" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvCR" runat="server" CssClass="Label_40" Width="30px"  ToolTip="Correction Action Report and Clarification Letter" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                    <td><asp:ImageButton ID="gvTR" runat="server" CssClass="Label_40" Width="30px"   ToolTip="Test Report" ImageUrl="~/images/icon_white.png"></asp:ImageButton ></td>
                                                    <td><asp:ImageButton ID="gvLATR" runat="server" CssClass="Label_40" Width="30px"  ToolTip="Letter of Approval for Test Report" ImageUrl="~/images/icon_white.png"></asp:ImageButton></td>
                                                </tr>
                                            </table>
                                             
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Responsible By">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gvResBy" CssClass="Text_110" runat="server" Text='<%# Bind("ResBy") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Remark">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gvRemark" CssClass="Text_200" runat="server" Text='<%# Bind("remark") %>'></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Input Docs">
                                 <ItemTemplate>
                                    <asp:ImageButton ID="gvInputDoc" runat="server" Height="30px" ImageAlign="Right" ImageUrl="~/images/up.png" Width="30px" CommandName="inputdocument" CommandArgument='<%# Container.DataItemIndex %>' ToolTip="upload document" />
                                    <asp:ImageButton ID="gvEdit" runat="server" Height="30px" ImageAlign="Right" ImageUrl="~/images/edit.png" Width="30px" CommandName="editdata" CommandArgument='<%# Container.DataItemIndex %>' ToolTip="Update Data" Visible="false" />
                                  
                                     </ItemTemplate>
                                 <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Hidden" Visible="false">
                                 <ItemTemplate>
                                    <%--<asp:Label ID="Label1" runat="server" Text='<%# Bind("row_id") %>'/>--%>
                                    <asp:Label ID="gvBidNo" runat="server" Text='<%# Bind("bid_no") %>'/>
                                    <asp:Label ID="gvBidRevision" runat="server" Text='<%# Bind("bid_revision") %>' />
                                    <asp:Label ID="gvContract_no" runat="server" Text='<%# Bind("contract_no") %>' />
                                    <asp:Label ID="gvVendor_no" runat="server" Text='<%# Bind("vendor_code") %>' />
                                    <asp:Label ID="gvSchedule_name" runat="server" Text='<%# Bind("schedule_name") %>' />
                                 </ItemTemplate>
                                 <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <asp:Button ID="btnSave" runat="server" Text="Save Data" CssClass="btn_normal_blue" OnClick="btnSave_Click"  />
                
                </div>
                    <p></p>
                <div id="divAssignment" class="div_90" runat="server">
                    <br />
                    
                    <table>
                        <tr>
                            <td colspan="2"><label class="Label_md"> กำหนดผู้รับผิดชอบ : </label></td>
                        </tr>
                         <tr>
                            <td style="text-align:left; width:200px"><label class="Label_md"> Item No : </label></td>
                            <td style="text-align:left; width:600px"><asp:DropDownList ID="ddlItemNo" runat="server" CssClass="ddl-sm" OnSelectedIndexChanged="ddlItemNo_SelectedIndexChanged"></asp:DropDownList></td>
                        </tr>
                         <tr>
                            <td style="text-align:left; width:200px""><label class="Label_md"> Step Name : </label></td>
                            <td style="text-align:left; width:600px""><asp:DropDownList ID="ddlStepName" runat="server" CssClass="ddl-md" AutoPostBack="True" OnSelectedIndexChanged="ddlStepName_SelectedIndexChanged"></asp:DropDownList></td>
                        </tr>
                         <tr>
                            <td colspan="2">
                                <uc1:ucEmp ID="ucEmp1" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="txtAssignment" runat="server" CssClass="auto-style3" Height="80px" TextMode="MultiLine" Width="1211px" ></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                   
                    <br />
                    <asp:Button ID="btnAssign" runat="server" Text="Assign" CssClass="btn_normal_blue" OnClick="btnAssign_Click" />
                </div>

        </div>
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRevision" runat="server" />
    <asp:HiddenField ID="hidMode" runat="server" />
                <br />
    <asp:HiddenField ID="hidLogin" runat="server" />
        <br />
    <asp:HiddenField ID="hidQCNo" runat="server" />
    </form>
    
</body>
</html>
