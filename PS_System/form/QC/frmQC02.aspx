﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmQC02.aspx.cs" Inherits="PS_System.form.QC.frmQC02" %>

<%@ Register src="../PR/ucEmp.ascx" tagname="ucEmp" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>QC Upload Document</title>
     <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <style type="text/css">
        .auto-style1 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 10pt;
            color: rgb(35, 34, 34);
            background-color: white;
            border-color: silver;
            border-radius: 2px;
        }
        .auto-style2 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 9pt;
            width: 160px;
            background-color: transparent;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div style="padding:20px">
          <div>
              <label class="Label_lg_blue">QC ITEM</label>
          </div>
           <p></p>
          <div id ="divSearch" runat="server" class="div_90">
                    <table>
                        <tr>
                            <td colspan="5" style="text-align:right">
                                <asp:Label ID="lblMode" runat="server" CssClass="Label_md"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td><label class="Label_sm">BID No : </label></td>
                            <td><asp:Label ID="lblBidNo" runat="server" CssClass="Label_sm"></asp:Label>&nbsp;<asp:Label ID="lblBidRevision" runat="server" CssClass="Label_sm"> </asp:Label></td>
                            <td><label class="Label_sm"> Schedule : </label></td>
                            <td><asp:Label ID="lblScheduleName" runat="server" CssClass="Label_sm"></asp:Label></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td><label class="auto-style2">Description : </label></td>
                            <td colspan="4"><asp:TextBox ID="txtBidDesc" runat="server" CssClass="auto-style1" TextMode="MultiLine" Width="901px"/></td>
                        </tr>
                        <tr>
                            <td><label class="Label_md"> Contract No. : </label></td>
                            <td><asp:Label ID="lblContractNo" runat="server" CssClass="Text_200"/></td>
                            <td><label class="Label_sm"> Vendor : </label></td>
                            <td><asp:Label ID="lblVendor" runat="server" CssClass="Text_200"/></td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </div>
                <p></p>
          <div id="uploadDoc" class="div_90">
              <table>
                  <tr>
                      <td style="text-align:left; width:200px""><label class="Label_md"> Item No. : </label></td>
                       <td style="text-align:left; width:600px"">
                           <asp:Label ID="lblItemNo" runat="server" CssClass="Text_150"></asp:Label>
                           &nbsp;&nbsp;
                           <label class="Label_md"> Desc. : </label>
                            <asp:Label ID="lblItemDesc" runat="server" CssClass="Text_400"></asp:Label>
                       </td>
                  </tr>
                  <tr>
                   <td style="text-align:left; width:200px""><label class="Label_md"> Type : </label></td>
                   <td style="text-align:left; width:600px""><asp:DropDownList ID="ddlStepName" runat="server" CssClass="ddl-md" AutoPostBack="True" ></asp:DropDownList></td>
               
                  </tr>
                  <tr>
                      <td style="text-align:left; width:200px""><label class="Label_md"> Input Document : </label></td>
                      <td style="text-align:left; width:600px""><asp:FileUpload id="inputFile" runat="server" CssClass="Text_400"/> </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; width:200px""><label class="Label_md"> Note : </label></td>
                      <td style="text-align:left; width:600px""><asp:TextBox id="txtNote" runat="server" Width="600px" CssClass="Text_600" TextMode="MultiLine"/> </td>
                  </tr>
                  <tr>
                      <td style="text-align:left; width:200px""><label class="Label_md"> By : </label></td>
                      <td style="text-align:left; width:600px"">
                          <uc1:ucEmp ID="ucEmp1" runat="server" />
                      </td>
                  </tr>
              </table>
              <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn_normal_blue" />
          </div>
        </div>
        <p></p>
        <div id="doclist" class="div_90">
            <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" 
                        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                        EmptyDataText="No data found." 
                        Font-Names="tahoma" Font-Size="9pt" Width="95%" OnRowCommand="gv1_OnRowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        <Columns>
                              <asp:TemplateField HeaderText="Select">
                                        <HeaderTemplate>
                                            <asp:CheckBox ID="gvSelectAll" AutoPostBack="true" OnCheckedChanged="gvSelectAll_CheckedChanged" CssClass="Text_60" runat="server" ></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvSelect" CssClass="Text_60" runat="server" ></asp:CheckBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Type">
                                        <ItemTemplate>
                                            <asp:Label ID="gvType" CssClass="Text_60" runat="server" Text='<%# Bind("step_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Document Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvDescription" CssClass="Text_200" runat="server" Text='<%# Bind("document_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Note">
                                        <ItemTemplate>
                                            <asp:Label ID="gvNote" CssClass="Text_200" runat="server" Text='<%# Bind("note") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="By">
                                        <ItemTemplate>
                                            <asp:Label ID="gvResBy" CssClass="Text_200" runat="server" Text='<%# Bind("res_by") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Remove">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgRemove" runat="server" Height="20px" ImageAlign="Right" ImageUrl="~/images/bin.png" Width="20px" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'  />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="80px" />
                            </asp:TemplateField>
                      </Columns>
                </asp:GridView>
        </div>
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRevision" runat="server" />
        <asp:HiddenField ID="hidScheduleName" runat="server" />
        <asp:HiddenField ID="hidItemNo" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" />
    </form>
</body>
</html>
