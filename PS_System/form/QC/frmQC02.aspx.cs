﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class frmQC02 : System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        OTFunctions zot = new OTFunctions();
        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                try { hidBidNo.Value = Request.QueryString["bid_no"].ToString(); }
                catch { hidBidNo.Value = ""; }
                try { hidBidRevision.Value = Request.QueryString["bid_revision"].ToString(); }
                catch { hidBidRevision.Value = ""; }
                try { hidScheduleName.Value = Request.QueryString["schedule_name"].ToString(); }
                catch { hidScheduleName.Value = ""; }
                try { hidItemNo.Value = Request.QueryString["item_no"].ToString(); }
                catch { hidItemNo.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                // Debug 
                
                lblMode.Text = hidMode.Value.ToUpper();

                ini_Data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        private void bind_header()
        {
            string sql = " select * from ps_t_qc_item where bid_no = '"+hidBidNo.Value+"' and  bid_revision = "+hidBidRevision.Value+" and schedule_name = '"+hidScheduleName.Value+"' and item_no  = '"+hidItemNo.Value+"' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                lblBidNo.Text = dr["bid_no"].ToString();
                lblBidRevision.Text = dr["bid_revision"].ToString();
                lblScheduleName.Text = dr["schedule_name"].ToString(); 
                lblContractNo.Text = dr["contract_no"].ToString();
                lblVendor.Text = dr["vendor_code"].ToString();
                lblItemNo.Text = dr["item_no"].ToString(); 
                lblItemDesc.Text = dr["description"].ToString();
                get_BidDesc(); 
            }
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        private void get_BidDesc()
        {
            if (lblBidNo.Text.Trim() != "")
            {
                string sql = " select bid_revision, bid_desc from bid_no where bid_no = '" + lblBidNo.Text.Trim() + @"' and status = 'Active' ";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtBidDesc.Text = StripHTML(ds.Tables[0].Rows[0]["bid_desc"].ToString());
                   
                }
            }
        }
        private void bind_stepname()
        {
            string sql = " select step_code, ( step_name + '('+step_code+')' ) as stepname from ps_t_qc_stepname order by step_no";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlStepName.Items.Clear();
            ddlStepName.DataSource = ds.Tables[0];
            ddlStepName.DataTextField = ds.Tables[0].Columns[1].ColumnName;
            ddlStepName.DataValueField = ds.Tables[0].Columns[0].ColumnName;
            ddlStepName.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Select task name-";
            li.Value = "";
            ddlStepName.Items.Insert(0, li);
        }
        private void ini_Data()
        {
            bind_header();
            bind_stepname();
            ini_gv(); 
        }
        #region gv
        private void ini_gv()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("step_name");
            dt.Columns.Add("document_name");
            dt.Columns.Add("note");
            dt.Columns.Add("res_by");
            var dr = dt.NewRow();
            dt.Rows.Add(dr);
            gv1.DataSource = dt;
            gv1.DataBind(); 

        }
        protected void gvSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            var xselect = true;
            if (((CheckBox)gv1.HeaderRow.FindControl("gvSelectAll")).Checked)
            { xselect = true; }
            else { xselect = false; }

            for (int i = 0; i < gv1.Rows.Count; i++)
            {
                ((CheckBox)gv1.Rows[i].FindControl("gvSelect")).Checked = xselect;
            }

        }
        protected void gv1_OnRowCommand(object sender , CommandEventArgs e)
        {
            if (e.CommandName == "removedata")
            {

            }
        }
        #endregion
    }
}