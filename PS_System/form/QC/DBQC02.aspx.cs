﻿using PS_Library;
using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class DBQC02 : System.Web.UI.Page
    {
        clsDashboard objDB = new clsDashboard();
        clsDropdownList objDrp = new clsDropdownList();
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z594900";

                if (Request.QueryString["bidno"] != null)
                    hidBidNo.Value = Request.QueryString["bidno"];
                else
                    hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;

                DropDownList drpAssing = new DropDownList();
                //Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);//this.hidLogin.Value

                string[] staffList = empInfo.getEmpIDAndNameInDepartment("กคภ-ส.");

                drpAssing.Items.Clear();
                drpAssing.Items.Insert(0, new ListItem("--เลือกผู้ปฏิบัติงาน--", ""));


                foreach (string staffInfo in staffList)
                {
                    string[] staff = staffInfo.Split(':');
                    ListItem item1 = new ListItem();
                    item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item1.Value = staff[0].ToString();                   
                    drpAssing.Items.Add(item1);
                }
                //drpAssing.SelectedValue = drpAssing;
                var dt = objDrp.drpDepartment();

                drpDept.Items.Clear();
                drpDept.AppendDataBoundItems = true;
                drpDept.Items.Add(new ListItem("--เลือกหน่วยงาน--", ""));
                drpDept.DataSource = dt;
                drpDept.DataTextField = "depart_code";
                drpDept.DataValueField = "depart_name";
                drpDept.DataBind();

                drpAssign.Items.Clear();
                drpAssign.Items.Add(new ListItem("--เลือก--", ""));
                bindData();


            }
        }
        private void bindData()
        {
            DataTable dt = objDB.getDataDBQC2("","");
            gvXls.DataSource = dt;
            gvXls.DataBind();

        }
        protected DataTable refreshgrid()
        {
            string rowfilter = "";
            string contract_no = txtContract.Text.Trim();
            string desc = txtDesc.Text.Trim();
            string job_no = txtJobNo.Text.Trim();

            string dept = drpDept.SelectedValue;
            //string Sch = ddlSch.SelectedValue;
            //string Part = ddlPart.SelectedValue;

            string assign = drpAssign.SelectedValue;
            if (contract_no != "")
            {
                rowfilter = string.Format("(a.contract_no = '{0}')", contract_no);
            }
            if (desc != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(a.schedule_name = '{0}')", desc);
                else rowfilter = string.Format("{0} and a.schedule_name = '{1}'", rowfilter, desc);
            }

            if (dept != "")
            {
                if (dept != "กคภ-ส.")
                {
                    if (rowfilter == "") rowfilter = string.Format("(a.schedule_name = '{0}')", dept);
                    else rowfilter = string.Format("{0} and a.schedule_name = '{1}'", rowfilter, dept);
                }
            }
            if (assign != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(b.assign_to  = '{0}')", assign);
                else rowfilter = string.Format("{0} and b.assign_to = '{1}'", rowfilter, assign);
            }

            DataTable dtrowfil = objDB.getDataDBQC2(rowfilter, job_no);
            return dtrowfil;
        }
        protected void btnFil_Click(object sender, EventArgs e)
        {
            DataTable dtrowfil = refreshgrid();
            gvXls.DataSource = dtrowfil;
            gvXls.DataBind();
        }
        protected void gvXls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
          
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                DropDownList drpAssing = (DropDownList)e.Row.FindControl("drpAssing");
                HiddenField HidDDLeq = (HiddenField)e.Row.FindControl("HidDDLeq");
             

                EmpInfoClass empInfo = new EmpInfoClass();
                //Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);//this.hidLogin.Value

                string[] staffList = empInfo.getEmpIDAndNameInDepartment("กคภ-ส.");

                drpAssing.Items.Clear();
                drpAssing.Items.Insert(0, new ListItem("--กรุณาเลือกผู้ปฏิบัติงาน--", ""));
                

                foreach (string staffInfo in staffList)
                {
                    string[] staff = staffInfo.Split(':');

                    ListItem item1 = new ListItem();
                    ListItem item2 = new ListItem();
                    ListItem item3 = new ListItem();

                    item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item1.Value = staff[0].ToString();
                    item2.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item2.Value = staff[0].ToString();
                    item3.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item3.Value = staff[0].ToString();
                    drpAssing.Items.Add(item1);
                }
                drpAssing.SelectedIndex = drpAssing.Items.IndexOf(drpAssing.Items.FindByValue(drv["assign_to"].ToString()));



                //if (lbreqDraw.Text == "False" && lbreqEq.Text == "False" && lbreqDoc.Text == "False") CbAssign.Enabled = false;


                //if (HidDDL_des.Value != null && HidDDL_des.Value != "")
                //{
                //    drpAssing.SelectedValue = HidDDL_des.Value;
                //    //drpAssing.Items.FindByValue(HidDDL_des.Value).Selected = true;
                //}
                //if (HidDDLeq.Value != null && HidDDLeq.Value != "")
                //{
                //    DDLequip.SelectedValue = HidDDLeq.Value;
                //    //DDLequip.Items.FindByValue(HidDDLeq.Value).Selected = true;
                //}
                //if (HidDDL_doc.Value != null && HidDDL_doc.Value != "")
                //{
                //    DDLdoc.SelectedValue = HidDDL_doc.Value;
                //    //DDLdoc.Items.FindByValue(HidDDL_doc.Value).Selected = true;
                //}

                //if (drpAssing.SelectedIndex != 0 && drv["status_drawing"].ToString() != "") lbStatusDes.Text = drv["status_drawing"].ToString();
                //else lbStatusDes.Text = "-";
                //if (DDLequip.SelectedIndex != 0 && drv["status_equip"].ToString() != "") lbStatusEP.Text = drv["status_equip"].ToString();
                //else lbStatusEP.Text = "-";
                //if (DDLdoc.SelectedIndex != 0 && drv["status_tech_doc"].ToString() != "") lbStatusDoc.Text = drv["status_tech_doc"].ToString();
                //else lbStatusDoc.Text = "-";

                //if (drv["status_activity"].ToString() == "WF in progress" || drv["status_activity"].ToString() == "FINISHED")
                //{
                //    imgbt.Enabled = false;
                //    CbAssign.Enabled = false;
                //    drpAssing.Enabled = false;
                //    DDLequip.Enabled = false;
                //    DDLdoc.Enabled = false;
                //    lblReq_dr.Visible = false;
                //    lblReq_eq.Visible = false;
                //    lblReq_doc.Visible = false;
                //}
            }
        }
        protected void drpDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpAssign.Items.Clear();
            drpAssign.Items.Add(new ListItem("--เลือก--", ""));
            drpAssign.AppendDataBoundItems = true;
            string dept = drpDept.SelectedValue;
            EmpInfoClass empInfo = new EmpInfoClass();
            string[] staffList = empInfo.getEmpIDAndNameInDepartment(dept);

            foreach (string staffInfo in staffList)
            {
                string[] staff = staffInfo.Split(':');

                ListItem item1 = new ListItem();
                item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                item1.Value = staff[0].ToString();
                drpAssign.Items.Add(item1);
            }


            //drpAssign.DataBind();

        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<QC02> list = new List<QC02>();
                foreach (GridViewRow gv in gvXls.Rows)
                {
                    //GridView grdViewDetail = (GridView)gv.FindControl("gvXls");
                    Label contract_no = (Label)gv.FindControl("lbConNo");
                    Label schedule_name = (Label)gv.FindControl("lbDesc");
                    
                    HiddenField row_id = (HiddenField)gv.FindControl("hdfRowID");
                    HiddenField bid_revision = (HiddenField)gv.FindControl("hdfBidRe");
                    HiddenField bid_no = (HiddenField)gv.FindControl("hdfBidNo");
                    DropDownList drpAssing = (DropDownList)gv.FindControl("drpAssing");                 
                    list.Add(
                    new QC02
                    {
                        row_id = Convert.ToInt32(row_id.Value),
                        contract_no = contract_no.Text,
                        schedule_name = schedule_name.Text,
                        bid_no = bid_no.Value,
                        bid_revision = bid_revision.Value,
                        assign_to = drpAssing.SelectedValue,
                    });
                }
                objDB.AssignQC2(list);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
            }catch(Exception ex)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('"+ex.Message.ToString()+"');", true);
            }
        }
    }
}