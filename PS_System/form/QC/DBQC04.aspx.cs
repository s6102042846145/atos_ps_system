﻿using PS_Library;
using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class DBQC04 : System.Web.UI.Page
    {
        clsDashboard objDB = new clsDashboard();
        protected void Page_Load(object sender, EventArgs e)
        {
            hidHost.Value = ConfigurationManager.AppSettings["HOST_NAME"].ToString();


            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z594900";

                if (Request.QueryString["bidno"] != null)
                    hidBidNo.Value = Request.QueryString["bidno"];
                else
                    hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT; 
                bindData(txtContract.Text.Trim()); 

            }
        }
        protected void btnFil_Click(object sender, EventArgs e)
        {
            string keyword = txtContract.Text.Trim();
            bindData(keyword);
        }
        private void bindData(string keyword)
        {
            DataTable dt = objDB.getDataDBQC04(hidBidNo.Value, hidBidRevision.Value, hidScheduleName.Value, keyword);
            gvXls.DataSource = dt;
            gvXls.DataBind();

        }

        
        [WebMethod]
        public static ResultData getDesc(string bid_no, string bid_revision, string schedule_name, string dep_name, string equip_code)
        {
            ResultData result = new ResultData();
            var objDB = new clsDashboard();
            //string bid_no, string bid_revision, string schedule_name, string equip_code, string dep_name
            DataTable dt = objDB.getDataMsg(bid_no, bid_revision, schedule_name, equip_code, dep_name);
            foreach (DataRow dtRow in dt.Rows)
            {
                foreach (DataColumn dc in dt.Columns)
                {
                    result.doc_name = dtRow["doc_name"].ToString();
                    result.doc_nodeid = dtRow["doc_nodeid"].ToString();
                    result.doc_sheetno = dtRow["doc_sheetno"].ToString();
                    result.doc_desc = dtRow["doc_desc"].ToString();

                }

            }

            //if (reader.Read())
            //{
            //    result.doc_name = reader["id"].ToString();
            //    result.desc = reader["assetos_name"].ToString();
            //}
            return result;
        }
    }
}