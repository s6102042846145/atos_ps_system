﻿<%@ Page Language="C#" MasterPageFile="~/MainPagesMaster.Master" AutoEventWireup="true" CodeBehind="DBQC01.aspx.cs" Inherits="PS_System.form.QC.DBQC01" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Quality Control Department: Activity Setting [TIEC-TX-07]"></asp:Label>
    <div id="dvMain" runat="server" style="width: 100%; padding-top: 10px;">
        <table style="width: 100%; margin: 10px">
            <tr>
                <td style="text-align: left; width: 8%">
                     <asp:Label ID="Label6" runat="server" Text="Contract No: " Width="6%"></asp:Label>
                    <asp:TextBox ID="txtContract" runat="server" />
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 8%">
                    <asp:Label ID="Label1" runat="server" Text="Schedule No: " Width="6%"></asp:Label>
                    <asp:DropDownList ID="drpDept" SelectionMode="Multiple" runat="server" AutoPostBack="True"></asp:DropDownList>
                   
                </td>
            </tr>
            <tr>
                <td style="text-align: left; width: 8%">
                    <asp:Button ID="btnFil" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFil_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div style="width: 100%; overflow: scroll; height: 500px;">
        <asp:GridView ID="gvXls" runat="server" Width="100%" OnRowDataBound="gvXls_RowDataBound"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="true">
            <FooterStyle BackColor="#164094" ForeColor="#000066" />
            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField HeaderText="Part">
                    <ItemTemplate>
                        <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Item No.">
                    <ItemTemplate>
                        <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_No") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Equipment Code">
                    <ItemTemplate>
                        <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" CssClass="Colsmall2" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SOE. Unit Price">
                    <ItemTemplate>
                        <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SOE. Amount">
                    <ItemTemplate>
                        <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LSE. Unit Price">
                    <ItemTemplate>
                        <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LSE. Amount">
                    <ItemTemplate>
                        <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LTC. Unit Price">
                    <ItemTemplate>
                        <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LTC. Amount">
                    <ItemTemplate>
                        <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Comment" HeaderStyle-Width="30px">
                    <ItemTemplate>
                        <asp:TextBox ID="txtComment" runat="server" Rows="2" TextMode="MultiLine" Width="100%"></asp:TextBox>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>--%>


                <%--<asp:TemplateField HeaderText="MS">
                    <EditItemTemplate>  
                        <asp:TextBox ID="txtMS" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>    
                        <asp:DropDownList ID="drpMS" runat="server"></asp:DropDownList>
                        <asp:HiddenField ID="hdfBidNo" runat="server" Value='<%# Bind("bid_no") %>' />
                        <asp:HiddenField ID="hdfBidRe" runat="server" Value='<%# Bind("bid_revision") %>' />
                        <asp:HiddenField ID="hdfSchedule_name" runat="server" Value='<%# Bind("schedule_name") %>' />
                    </ItemTemplate>  
                </asp:TemplateField>

                <asp:TemplateField HeaderText="QP">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtQP" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpQP" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TS">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtTS" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpTS" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>

                <asp:TemplateField HeaderText="TP">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtTP" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpTP" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>

                <asp:TemplateField HeaderText="IW">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtIW" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpIW" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IR">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtIR" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpIR" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FIR">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtFIR" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpFIR" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CR">
                     <EditItemTemplate>  
                        <asp:TextBox ID="txtCR" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpCR" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField>

                <asp:TemplateField HeaderText="TR">  
                    <EditItemTemplate>  
                        <asp:TextBox ID="txtTR" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                        <asp:DropDownList ID="drpTR" runat="server">  
                        </asp:DropDownList>  
                    </ItemTemplate>  
                </asp:TemplateField> --%>


                <asp:TemplateField HeaderText="MS">
                    <ItemTemplate>
                        <select id="drpMS" name="drpMS" runat="server" style="width: 80px;" onchange="getValueMS(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-value="1" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-value="2" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-value="3" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-value="4" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <%--<asp:DropDownList ID="drpMS" runat="server" style="width:80px;" AutoPostBack="true" OnSelectedIndexChanged="MS_Change" ></asp:DropDownList>
                        --%>
                        <%--<asp:DropDownList ID="drpMS" runat="server" onchange="MSchange(this)"></asp:DropDownList>--%>
                        <asp:HiddenField ID="hdfMS" runat="server" />
                        <asp:HiddenField ID="hdfBidNo" runat="server" Value='<%# Bind("bid_no") %>' />
                        <asp:HiddenField ID="hdfBidRe" runat="server" Value='<%# Bind("bid_revision") %>' />
                        <asp:HiddenField ID="hdfSchedule_name" runat="server" Value='<%# Bind("schedule_name") %>' />
                        <asp:HiddenField ID="hdfDesc" runat="server" Value='<%# Bind("description") %>' />
                        <asp:HiddenField ID="hdfQty" runat="server" Value='<%# Bind("qty") %>' />
                        <asp:HiddenField ID="hdfUnit" runat="server" Value='<%# Bind("unit") %>' />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="QP">
                    <ItemTemplate>
                        <select id="drpQP" name="drpQP" runat="server" style="width: 80px;" onchange="getValueQP(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfQP" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TS">
                    <ItemTemplate>
                        <select id="drpTS" name="drpTS" runat="server" style="width: 80px;" onchange="getValueTS(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfTS" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="TP">
                    <ItemTemplate>
                        <select id="drpTP" name="drpTP" runat="server" style="width: 80px;" onchange="getValueTP(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfTP" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="IW">
                    <ItemTemplate>
                        <select id="drpIW" name="drpIW" runat="server" style="width: 80px;" onchange="getValueIW(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfIW" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IR">
                    <ItemTemplate>
                        <select id="drpIR" name="drpIR" runat="server" style="width: 80px;" onchange="getValueIR(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfIR" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FIR">
                    <ItemTemplate>
                        <select id="drpFIR" name="drpFIR" runat="server" style="width: 80px;" onchange="getValueFIR(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfFIR" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CR">
                    <ItemTemplate>
                        <select id="drpCR" name="drpCR" runat="server" style="width: 80px;" onchange="getValueCR(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfCR" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="TR">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtTR" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <select id="drpTR" name="drpTR" runat="server" style="width: 80px;" onchange="getValueTR(this);">
                            <option value="">--เลือก--</option>
                            <option value="" data-image="../../Vendor/fontawesome/img/circle-regular.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/minus-solid.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/weebly-brands.svg"></option>
                            <option value="" data-image="../../Vendor/fontawesome/img/square-regular.svg"></option>
                        </select>
                        <asp:HiddenField ID="hdfTR" runat="server" />
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>
    <div class="text-right mt-15 mr-10">
        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" />
        <%--<asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-defalt" />--%>
    </div>
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRev" runat="server" />
    <asp:HiddenField ID="hidJobNo" runat="server" />
    <asp:HiddenField ID="hidJobRev" runat="server" />
    <asp:HiddenField ID="hidLogin" runat="server" />
    <asp:HiddenField ID="hidDept" runat="server" />
    <asp:HiddenField ID="hidSect" runat="server" />
    <asp:HiddenField ID="hidMc" runat="server" />

    <asp:HiddenField ID="hidBidRevision" runat="server" Value="0" />
    <asp:HiddenField ID="hidScheduleName" runat="server" Value="BANG PA-IN 2 SUBSTATION" />

    <script>

        function getValueMS(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfMS_" + array[3]).val(no);
        }
        function getValueQP(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfQP_" + array[3]).val(no);
        }
        function getValueTS(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfTS_" + array[3]).val(no);
        }
        function getValueTP(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfTP_" + array[3]).val(no);
        }
        function getValueIW(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfIW_" + array[3]).val(no);
        }
        function getValueIR(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfIR_" + array[3]).val(no);
        }
        function getValueFIR(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfFIR_" + array[3]).val(no);
        }
        function getValueCR(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfCR_" + array[3]).val(no);
        }
        function getValueTR(obj) {
            //alert(obj.id);
            var dd = document.getElementById(obj.id + "_title");
            /*alert(dd.innerHTML);*/
            var no = "";
            var arrayInner = dd.innerHTML.split(' ');
            var array = obj.id.split('_');
            if (arrayInner[0] == "<img") {
                var $a = $(dd);
                var $img = $a.find('img');
                var selectedItemSrc = $img.attr('src');
                var arrayImg = selectedItemSrc.split('/');
                //console.log(arrayImg);
                //alert(1);
                var img1 = "circle-regular.svg";
                var img2 = "minus-solid.svg";
                var img3 = "weebly-brands.svg";
                var img4 = "square-regular.svg";
                if (img1 == arrayImg[5]) { no = 1; }
                if (img2 == arrayImg[5]) { no = 2; }
                if (img3 == arrayImg[5]) { no = 3; }
                if (img4 == arrayImg[5]) { no = 4; }
            }
            $("#ContentPlaceHolder1_gvXls_hdfTR_" + array[3]).val(no);
        }



        //$(document).ready(function (e) {
        //    //$('#drpMS').change(function () {
        //    //    alert($(this).val());
        //    //});
        //    $('#myDropdown').ddslick({
        //        onSelected: function (selectedData) {
        //            //callback function: do something with selectedData;
        //        }
        //    });
        //});

        //function MSchange(id) {
        //    //alert(id);
        //    var ddlValue = $(id).val();
        //    //console.log(id);
        //    alert(ddlValue);
        //}
        //function SaveData(id) {
        //    alert(id);
        //}
        function getData() {
            try {
                var pages = $("#pages").msDropdown({
                    on: {
                        change: function (data, ui) {
                            var val = data.value;
                            if (val != "")
                                window.location = val;
                        }
                    }
                }).data("dd");

                var pagename = document.location.pathname.toString();
                pagename = pagename.split("/");
                pages.setIndexByValue(pagename[pagename.length - 1]);
                $("#ver").html(msBeautify.version.msDropdown);


            } catch (e) {
                //console.log(e);	
            }

            $("#ver").html(msBeautify.version.msDropdown);

            //convert
            $("select").msDropdown({ roundedBorder: false });
            $("#tech").data("dd");
        }
        $(document).ready(function (e) {
            $('[id*=drpDept]').multiselect({
                includeSelectAllOption: true
            });
            //no use
            //$('select>option:eq(1)').attr('selected', true);
            //$('#drpMS>option:eq(4)').attr('selected', true);
            //getValueMS(obj);
            //$(".ddTitleText ").addClass("text-center");
            $("ul>li").addClass("text-center");
            getData();
            //$(".ddTitleText ").addClass("aaaa");
            //$(".ddTitleText ").click(function () { myFunction(); });
            /*
            $(".fnone").change(function () { 
                myFunction();
                //console.log("by jquery: ", this.value);

                });
                */



            //$("li").click(function (e) {
            //    //alert(this.innerText);
            //    var $a = $(e.currentTarget); // get a tag

            //    console.log($a);





            //    var $img = $a.find('img'); // find img in a tag
            //    //var selectedItemText = $a.text(); // text from a
            //    var selectedItemSrc = $img.attr('src'); // src from img
            //    //$.ajax({
            //    //    type: "POST",
            //    //    url: "DBQC01.aspx/SaveData",
            //    //    data: "{'img':'" + selectedItemSrc + "'}",
            //    //    contentType: "application/json; charset=utf-8",
            //    //    dataType: "json",
            //    //    success: function (msg) {
            //    //        if (msg.d.err == '0') {
            //    //            alert('บันทึกข้อมูลเรียบร้อยแล้ว');

            //    //        } else {
            //    //            alert(msg.d.errmsg);
            //    //        }
            //    //    }
            //    //});
            //});
            //$(".ddlabel").css("display", "none");

        });

        //$("#ContentPlaceHolder1_gvXls_drpQP_0_msdd").change(function () {
        //    alert("Handler for .change() called.");
        //});


        //$(".ddTitleText ").change(function () {
        //    alert("Handler for .change() called.");
        //});


        //$("#ContentPlaceHolder1_gvXls_drpQP_0_msdd").onchange(function () {
        //    //console.log("by jquery: ", this.value);
        //    alert(1)
        //});
        //$('select>option:eq(1)').attr('selected', true);
        //$('#ContentPlaceHolder1_gvXls_drpMS_0>option:eq(4)').attr('selected', true);
    </script>
</asp:Content>
