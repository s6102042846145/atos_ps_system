﻿<%@ Page Language="C#" MasterPageFile="~/MainPagesMaster.Master" AutoEventWireup="true" CodeBehind="DBQC02.aspx.cs" Inherits="PS_System.form.QC.DBQC02" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Quality Control Department: Assignment"></asp:Label>
    <div id="dvMain" runat="server" style="width: 100%; padding-top: 10px;">
        <table style="width: 100%; margin: 10px">
            <tr>
                <td style="width: 100px;">
                    <asp:Label ID="Label6" runat="server" Text="Contract No: "></asp:Label></td>
                <td style="width: 220px;">
                    <asp:TextBox ID="txtContract" Style="width: 200px;" runat="server" /></td>
                <td style="width: 70px;">
                    <asp:Label ID="Label1" runat="server" Text="หน่วยงาน: "></asp:Label></td>
                <td colspan="5">
                    <asp:DropDownList ID="drpDept" Style="width: 590px; height: 25px;" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpDept_SelectedIndexChanged"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Description: "></asp:Label></td>
                <td colspan="6">
                    <asp:TextBox ID="txtDesc" runat="server" Style="width: 200px;" /></td>
               <%-- <td>
                    <asp:Label ID="Label4" runat="server" Text="Period:"></asp:Label></td>
                <td style="width: 200px;">
                    <asp:DropDownList ID="drpType" Style="width: 200px; height: 25px;" runat="server"></asp:DropDownList></td>
                <td style="width: 100px;">
                    <asp:TextBox ID="txtStDate" runat="server" class="picker" autocomplete="off" /></td>
                <td style="width: 30px;" class="text-center">
                    <asp:Label ID="Label5" runat="server" Text="To"></asp:Label></td>
                <td style="width: 100px;">
                    <asp:TextBox ID="txtEndDate" runat="server" class="picker" autocomplete="off" /></td>--%>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Job No.: "></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtJobNo" Style="width: 200px;" runat="server" /></td>
                <td colspan="6"></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Assign To: "></asp:Label></td>
                <td>
                    <asp:DropDownList ID="drpAssign" Style="width: 200px; height: 25px;" runat="server"></asp:DropDownList></td>
                <td colspan="6"></td>
            </tr>
        </table>
        <asp:Button ID="btnFil" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFil_Click"/>
    </div>
    <div style="width: 100%; overflow: scroll; height: 530px;">
        <asp:GridView ID="gvXls" runat="server" Width="100%" OnRowDataBound="gvXls_RowDataBound"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="true">
            <FooterStyle BackColor="#164094" ForeColor="#000066" />
            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField HeaderText="Contract No.">
                    <ItemTemplate>
                        <asp:Label ID="lbConNo" runat="server" Text='<%# Bind("contract_no") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Job List">
                    <ItemTemplate>
                        <asp:Label ID="lbJobNo" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Job No.">
                    <ItemTemplate>
                        <asp:Label ID="lbJobNo" runat="server" Text=''></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Substation">
                    <ItemTemplate>
                        <asp:Label ID="lbSubStation" runat="server" Text=''></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall2" />
                </asp:TemplateField>--%>

                <asp:TemplateField HeaderText="Assign To">
                    <EditItemTemplate>  
                        <asp:TextBox ID="txtAssign" runat="server"></asp:TextBox>  
                    </EditItemTemplate>  
                    <ItemTemplate>  
                       <%-- <asp:DropDownList ID="drpAssing" runat="server">  
                        </asp:DropDownList>  --%>
                        <%--<select id="drpAssing" name="drpAssing">
                            <option value="">ผู้ปฏิบัติงาน</option>
                          </select>--%>
                        <asp:DropDownList ID="drpAssing" runat="server"></asp:DropDownList>
                            <asp:Label ID="lblReq_eq" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                        <asp:HiddenField ID="hdfRowID" runat="server" Value='<%# Bind("row_id") %>' />
                        <asp:HiddenField ID="hdfBidNo" runat="server" Value='<%# Bind("bid_no") %>' />
                        <asp:HiddenField ID="hdfBidRe" runat="server" Value='<%# Bind("bid_revision") %>' />
                    </ItemTemplate>  
                </asp:TemplateField>


                <asp:TemplateField HeaderText="LOA Date">
                    <ItemTemplate>
                        <asp:Label ID="lbLOADate" runat="server" Text='<%# Bind("loa_date") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LOA Confirm Date">
                    <ItemTemplate>
                        <asp:Label ID="lbLOAConfDate" runat="server" Text='<%# Bind("loa_confirm_date") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>
    <div class="text-right mt-15 mr-10">
            <asp:Button ID="btnSave" runat="server" Text="Assign" class="btn btn-primary" OnClick="btnSave_Click"/>
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-defalt"/>
        
    </div>
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRev" runat="server" />
    <asp:HiddenField ID="hidJobNo" runat="server" />
    <asp:HiddenField ID="hidJobRev" runat="server" />
    <asp:HiddenField ID="hidLogin" runat="server" />
    <asp:HiddenField ID="hidDept" runat="server" />
    <asp:HiddenField ID="hidSect" runat="server" />
    <asp:HiddenField ID="hidMc" runat="server" />

    <script>
        $(function () {
            $(".picker").datepicker();
        });
    </script>
</asp:Content>