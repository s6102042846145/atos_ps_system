﻿using PS_Library;
using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class DBQC01 : System.Web.UI.Page
    {
        clsDashboard objDB = new clsDashboard();
        clsDropdownList objDrp = new clsDropdownList();
        public class returnsave
        {
            public string err; //0 = no error, 1=error
            public string errmsg;
            //public string lastid;
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z594900";

                if (Request.QueryString["bidno"] != null)
                    hidBidNo.Value = Request.QueryString["bidno"];
                else
                    hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;
                bindData();

                var dt = objDrp.drpDepartment();
                drpDept.Items.Clear();
                drpDept.AppendDataBoundItems = true;
                drpDept.Items.Add(new ListItem("--เลือกหน่วยงาน--", ""));
                drpDept.DataSource = dt;
                drpDept.DataTextField = "depart_code";
                drpDept.DataValueField = "depart_name";
                drpDept.DataBind();

            }
        }
        private void bindData()
        {
            DataTable dt = objDB.getDataDBQC(hidBidNo.Value, hidBidRevision.Value, hidScheduleName.Value, txtContract.Text.Trim());
            gvXls.DataSource = dt;
            gvXls.DataBind();

            if (dt.Rows.Count == 0)
            {
                btnSave.Visible = false;
                //btnCancel.Visible = false;
            }
            else
            {
                btnSave.Visible = true;
                //btnCancel.Visible = true;
            }
        }

        private void GetData(string keyword)
        {
            DataTable dtrowfil = objDB.getDataDBQC(hidBidNo.Value, hidBidRevision.Value, hidScheduleName.Value, keyword);
            gvXls.DataSource = dtrowfil;
            gvXls.DataBind();
            if (dtrowfil.Rows.Count == 0)
            {
                btnSave.Visible = false;
                //btnCancel.Visible = false;
            }
            else
            {
                btnSave.Visible = true;
                //btnCancel.Visible = true;
            }
            ScriptManager.RegisterStartupScript(Page, GetType(), "getData", "<script>getData()</script>", false);
        }
        protected void btnFil_Click(object sender, EventArgs e)
        {
            string keyword = txtContract.Text.Trim();
            GetData(keyword);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<QC01> list = new List<QC01>();
                int i = 0;
                foreach (GridViewRow gv in gvXls.Rows)
                {
                    //GridView grdViewDetail = (GridView)gv.FindControl("gvXls");

                    HiddenField bid_no = (HiddenField)gv.FindControl("hdfBidNo");
                    HiddenField bid_revision = (HiddenField)gv.FindControl("hdfBidRe");
                    HiddenField schedule_name = (HiddenField)gv.FindControl("hdfSchedule_name");
                    Label part = (Label)gv.FindControl("lbPart");
                    Label item_no = (Label)gv.FindControl("lbItmno");
                    HiddenField ms = (HiddenField)gv.FindControl("hdfMS");
                    HiddenField qp = (HiddenField)gv.FindControl("hdfQP");
                    HiddenField ts = (HiddenField)gv.FindControl("hdfTS");
                    HiddenField tp = (HiddenField)gv.FindControl("hdfTP");
                    HiddenField iw = (HiddenField)gv.FindControl("hdfIW");
                    HiddenField ir = (HiddenField)gv.FindControl("hdfIR");
                    HiddenField fir = (HiddenField)gv.FindControl("hdfFIR");
                    HiddenField cr = (HiddenField)gv.FindControl("hdfCR");
                    HiddenField tr = (HiddenField)gv.FindControl("hdfTR");

                    HiddenField description = (HiddenField)gv.FindControl("hdfDesc");
                    HiddenField qty = (HiddenField)gv.FindControl("hdfQty");
                    HiddenField unit = (HiddenField)gv.FindControl("hdfUnit");
                    


                    //    DropDownList drpQP = (DropDownList)gv.FindControl("drpQP");
                    //    DropDownList drpTS = (DropDownList)gv.FindControl("drpTS");
                    //    DropDownList drpTP = (DropDownList)gv.FindControl("drpTP");
                    //    DropDownList drpIW = (DropDownList)gv.FindControl("drpIW");
                    //    DropDownList drpIR = (DropDownList)gv.FindControl("drpIR");
                    //    DropDownList drpFIR = (DropDownList)gv.FindControl("drpFIR");
                    //    DropDownList drpCR = (DropDownList)gv.FindControl("drpCR");
                    //    DropDownList drpTR = (DropDownList)gv.FindControl("drpTR");


                    //var a = Convert.ToInt32(ms.Value);
                    //var b = Convert.ToInt32(qp.Value);
                    //var aa = Convert.ToInt32(ts.Value);
                    //var ss = Convert.ToInt32(tp.Value);
                    //var s = Convert.ToInt32(iw.Value);
                    //var c = Convert.ToInt32(ir.Value);
                    //var w = Convert.ToInt32(fir.Value);
                    //var aaa = Convert.ToInt32(cr.Value);
                    //var aaas = Convert.ToInt32(tr.Value);

                    list.Add(
                    new QC01
                    {
                        contract_no=txtContract.Text.Trim(),
                        bid_no = bid_no.Value,
                        bid_revision = bid_revision.Value,
                        schedule_name = schedule_name.Value,
                        part = part.Text,
                        item_no = item_no.Text,
                        description = description.Value,
                        qty = qty.Value,
                        unit = unit.Value,
                        //ms = ms.Value=="" ? 0 : Convert.ToInt32(ms.Value),
                        //qp = qp.Value == "" ? 0 : Convert.ToInt32(qp.Value),
                        //ts = ts.Value == "" ? 0 : Convert.ToInt32(ts.Value),
                        //tp = tp.Value == "" ? 0 : Convert.ToInt32(tp.Value),
                        //iw = iw.Value == "" ? 0 : Convert.ToInt32(iw.Value),
                        //ir = ir.Value == "" ? 0 : Convert.ToInt32(ir.Value),
                        //fir = fir.Value == "" ? 0 : Convert.ToInt32(fir.Value),
                        //cr = cr.Value == "" ? 0 : Convert.ToInt32(cr.Value),
                        //tr = tr.Value == "" ? 0 : Convert.ToInt32(tr.Value),
                        ms = ms.Value,
                        qp = qp.Value,
                        ts = ts.Value,
                        tp = tp.Value,
                        iw = iw.Value,
                        ir = ir.Value,   
                        fir = fir.Value,
                        cr = cr.Value,
                        tr = tr.Value,
                    }); 

                } 

                objDB.AssignQC1(list);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
                string keyword = txtContract.Text.Trim();
                GetData(keyword);
            }
            catch (Exception ex)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('" + ex.Message.ToString() + "');", true);
            }
        }
        protected void gvXls_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                try
                {
                    int i = 0;
                    DataRowView drv = (DataRowView)e.Row.DataItem;
                    HiddenField ms = (HiddenField)e.Row.FindControl("hdfMS");
                    HiddenField qp = (HiddenField)e.Row.FindControl("hdfQP");
                    HiddenField ts = (HiddenField)e.Row.FindControl("hdfTS");
                    HiddenField tp = (HiddenField)e.Row.FindControl("hdfTP");
                    HiddenField iw = (HiddenField)e.Row.FindControl("hdfIW");
                    HiddenField ir = (HiddenField)e.Row.FindControl("hdfIR");
                    HiddenField fir = (HiddenField)e.Row.FindControl("hdfFIR");
                    HiddenField cr = (HiddenField)e.Row.FindControl("hdfCR");
                    HiddenField tr = (HiddenField)e.Row.FindControl("hdfTR");
                    ms.Value = drv["ms"].ToString();
                    qp.Value = drv["qp"].ToString();
                    ts.Value = drv["ts"].ToString();
                    tp.Value = drv["tp"].ToString();
                    iw.Value = drv["iw"].ToString();
                    ir.Value = drv["ir"].ToString();
                    fir.Value = drv["fir"].ToString();
                    cr.Value = drv["cr"].ToString();
                    tr.Value = drv["tr"].ToString();



                    var drpMS = e.Row.FindControl("drpMS") as HtmlSelect;
                    drpMS.SelectedIndex = Convert.ToInt32(ms.Value);

                    var drpQP = e.Row.FindControl("drpQP") as HtmlSelect;
                    drpQP.SelectedIndex = Convert.ToInt32(qp.Value);

                    var drpTS = e.Row.FindControl("drpTS") as HtmlSelect;
                    drpTS.SelectedIndex = Convert.ToInt32(ts.Value);

                    var drpTP = e.Row.FindControl("drpTP") as HtmlSelect;
                    drpTP.SelectedIndex = Convert.ToInt32(tp.Value);

                    var drpIW = e.Row.FindControl("drpIW") as HtmlSelect;
                    drpIW.SelectedIndex = Convert.ToInt32(iw.Value);

                    var drpIR = e.Row.FindControl("drpIR") as HtmlSelect;
                    drpIR.SelectedIndex = Convert.ToInt32(ir.Value);

                    var drpFIR = e.Row.FindControl("drpFIR") as HtmlSelect;
                    drpFIR.SelectedIndex = Convert.ToInt32(fir.Value);

                    var drpCR = e.Row.FindControl("drpCR") as HtmlSelect;
                    drpCR.SelectedIndex = Convert.ToInt32(cr.Value);

                    var drpTR = e.Row.FindControl("drpTR") as HtmlSelect;
                    drpTR.SelectedIndex = Convert.ToInt32(tr.Value);

                    //ListItem li = drpMS.Items.FindByValue("3");
                    //li.Selected = true;


                    ////$('#ContentPlaceHolder1_gvXls_drpMS_0>option:eq(4)').attr('selected', true);
                    //drpMS.SelectedIndex = drpMS.Items.IndexOf(drpMS.Items.FindByText("4"));


                    //var aaa = drv["ts"].ToString();
                }
                catch (Exception ex)
                {

                }
                /*
                DataRowView drv = (DataRowView)e.Row.DataItem;
                DropDownList drpMS = (DropDownList)e.Row.FindControl("drpMS");
                DropDownList drpQP = (DropDownList)e.Row.FindControl("drpQP");
                DropDownList drpTS = (DropDownList)e.Row.FindControl("drpTS");
                DropDownList drpTP = (DropDownList)e.Row.FindControl("drpTP");
                DropDownList drpIW = (DropDownList)e.Row.FindControl("drpIW");
                DropDownList drpIR = (DropDownList)e.Row.FindControl("drpIR");
                DropDownList drpFIR = (DropDownList)e.Row.FindControl("drpFIR");
                DropDownList drpCR = (DropDownList)e.Row.FindControl("drpCR");
                DropDownList drpTR = (DropDownList)e.Row.FindControl("drpTR");

                //ListItem item = new ListItem();
                //item.Value = "aaa";// something
                //item.Text = "/images/icon_white.png";
                //item.Attributes.Add("class", "imageconverter");
                //drpMS.Items.Add(item);

                //drpMS.Items.Clear();
                //drpMS.Items.Insert(0, new ListItem("--เลือก--", ""));
                //ListItem list = new ListItem();
                //list.Value = "1";
                //list.Text = "1";
                ////list.Attributes.Add("data-image", "../../Vendor/fontawesome/img/circle-regular.svg");
                //drpMS.Items.Add(list);

                drpMS.Items.Clear();
                drpMS.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpMS.Items.Insert(1, new ListItem("1", "1"));
                drpMS.Items.Insert(2, new ListItem("2", "2"));
                drpMS.Items.Insert(3, new ListItem("3", "3"));
                drpMS.Items.Insert(4, new ListItem("4", "4"));

                drpQP.Items.Clear();
                drpQP.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpQP.Items.Insert(1, new ListItem("1", "1"));
                drpQP.Items.Insert(2, new ListItem("2", "2"));
                drpQP.Items.Insert(3, new ListItem("3", "3"));
                drpQP.Items.Insert(4, new ListItem("4", "4"));

                drpTS.Items.Clear();
                drpTS.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpTS.Items.Insert(1, new ListItem("1", "1"));
                drpTS.Items.Insert(2, new ListItem("2", "2"));
                drpTS.Items.Insert(3, new ListItem("3", "3"));
                drpTS.Items.Insert(4, new ListItem("4", "4"));

                drpTP.Items.Clear();
                drpTP.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpTP.Items.Insert(1, new ListItem("1", "1"));
                drpTP.Items.Insert(2, new ListItem("2", "2"));
                drpTP.Items.Insert(3, new ListItem("3", "3"));
                drpTP.Items.Insert(4, new ListItem("4", "4"));

                drpIW.Items.Clear();
                drpIW.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpIW.Items.Insert(1, new ListItem("1", "1"));
                drpIW.Items.Insert(2, new ListItem("2", "2"));
                drpIW.Items.Insert(3, new ListItem("3", "3"));
                drpIW.Items.Insert(4, new ListItem("4", "4"));

                drpIR.Items.Clear();
                drpIR.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpIR.Items.Insert(1, new ListItem("1", "1"));
                drpIR.Items.Insert(2, new ListItem("2", "2"));
                drpIR.Items.Insert(3, new ListItem("3", "3"));
                drpIR.Items.Insert(4, new ListItem("4", "4"));

                drpFIR.Items.Clear();
                drpFIR.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpFIR.Items.Insert(1, new ListItem("1", "1"));
                drpFIR.Items.Insert(2, new ListItem("2", "2"));
                drpFIR.Items.Insert(3, new ListItem("3", "3"));
                drpFIR.Items.Insert(4, new ListItem("4", "4"));

                drpCR.Items.Clear();
                drpCR.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpCR.Items.Insert(1, new ListItem("1", "1"));
                drpCR.Items.Insert(2, new ListItem("2", "2"));
                drpCR.Items.Insert(3, new ListItem("3", "3"));
                drpCR.Items.Insert(4, new ListItem("4", "4"));

                drpTR.Items.Clear();
                drpTR.Items.Insert(0, new ListItem("--เลือก--", ""));
                drpTR.Items.Insert(1, new ListItem("1", "1"));
                drpTR.Items.Insert(2, new ListItem("2", "2"));
                drpTR.Items.Insert(3, new ListItem("3", "3"));
                drpTR.Items.Insert(4, new ListItem("4", "4"));

                //


                //EmpInfoClass empInfo = new EmpInfoClass();

                //string[] staffList = empInfo.getEmpIDAndNameInDepartment("กคภ-ส.");

                //drpMS.Items.Clear();
                //drpMS.Items.Insert(0, new ListItem("", ""));
                //drpMS.Items.Insert(1, new ListItem("<i class='far fa-circle'></i>", "1"));
                //drpMS.Items.Insert(2, new ListItem("<i class='fas fa-minus'></i>", "2"));
                //drpMS.Items.Insert(3, new ListItem("<i class='fab fa-weebly'></i>", "3"));
                //drpMS.Items.Insert(4, new ListItem("<i class='far fa-check-square'></i>", "4"));


                //foreach (string staffInfo in staffList)
                //{
                //    string[] staff = staffInfo.Split(':');

                //    ListItem item1 = new ListItem();
                //    item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                //    item1.Value = staff[0].ToString();
                //    drpMS.Items.Add(item1);
                //}
                //drpMS.SelectedIndex = drpMS.Items.IndexOf(drpMS.Items.FindByValue(drv["drpMS"].ToString()));
                */
            }
        }
        protected void MS_Change(object sender, EventArgs e)
        {
            //stuff that never gets hit
        }

        [WebMethod]
        public static returnsave savedata(string img)
        {
            returnsave objreturnsave = new returnsave();
            int no = 0;
            if (img != "")
            {
                string img1 = "../../Vendor/fontawesome/img/circle-regular.svg";
                string img2 = "../../Vendor/fontawesome/img/minus-solid.svg";
                string img3 = "../../Vendor/fontawesome/img/weebly-brands.svg";
                string img4 = "../../Vendor/fontawesome/img/check-square-regular.svg";
                if (img == img1) { no = 1; }
                if (img == img2) { no = 2; }
                if (img == img3) { no = 3; }
                if (img == img4) { no = 4; }

            }
            return objreturnsave;
        }

    }
}