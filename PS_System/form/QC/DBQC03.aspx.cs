﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class DBQC03 : System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zQC_NodeID = ConfigurationManager.AppSettings["QC_NodeID"].ToString();
        string zContract_NodeID = ConfigurationManager.AppSettings["Contract_NodeID"].ToString();

        OTFunctions zot = new OTFunctions();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ini_data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        private void ini_ddlDoctype()
        {
            ddlDoctype.Items.Clear();
            string sql = "select  step_code as col0, (step_code +' - '+ step_name) col1 from  [ps_t_qc_stepname] order by step_no  ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlDoctype.DataSource = ds.Tables[0];
                ddlDoctype.DataTextField = "col1";
                ddlDoctype.DataValueField = "col0";
                ddlDoctype.DataBind();

            }
            ListItem li = new ListItem();
            li.Text = "-Select Doc Type-";
            li.Value = "";
            ddlDoctype.Items.Insert(0, li);
            ddlDoctype.SelectedIndex = 0;
        }
        private void ini_ddlContractNo()
        {
            ddlContractNo.Items.Clear();
            string sql = "select distinct contract_no from  ps_t_bid_selling_cmpo order by contract_no  ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlContractNo.DataSource = ds.Tables[0];
                ddlContractNo.DataTextField = "contract_no";
                ddlContractNo.DataValueField = "contract_no";
                ddlContractNo.DataBind();
                
            }
            ListItem li = new ListItem();
            li.Text = "-Select Contract No.-";
            li.Value = "";
            ddlContractNo.Items.Insert(0, li);
            ddlContractNo.SelectedIndex = 0;
        }
        private void ini_data()
        {
            ini_ddlDoctype();
            ini_ddlContractNo();
        }
        private void get_bidinfo(string xcontract_no)
        {
            lblBidNo.Text = "";
            lblBidDesc.Text = "";
            string sql = @"select cmpo.*, v.vendor_name 
                            from  ps_t_bid_selling_cmpo cmpo
                            left join ps_m_vendor v on (cmpo.vendor_code = v.vendor_code)
                            where cmpo.contract_no = '"+xcontract_no+"' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                lblBidNo.Text = dr["bid_no"].ToString();
                lblBidRevision.Text = dr["bid_revision"].ToString();
                lblScheduleName.Text = dr["schedule_name"].ToString();
                lblVendor.Text = dr["vendor_name"].ToString();
                hidBidNo.Value = lblBidNo.Text;
                hidBidRevision.Value = lblBidRevision.Text;
                hidScheduleName.Value = lblScheduleName.Text;
            }
        }
        protected void ddlContractNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlContractNo.SelectedIndex > 0)
            {
                hidContractNo.Value = ddlContractNo.SelectedValue.ToString();
                get_bidinfo(ddlContractNo.SelectedValue.ToString());
            }
            
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            getItems();
        }
        private string getTemplateID(string xbid_no, string xbid_revision, string xschedule_name)
        {
            string xtemplate_id = "";
            string sql = @" 	 select *
				  from ps_xls_template 
                  where bid_no = '"+xbid_no+"' and bid_revision = '"+xbid_revision+"' and schedule_name = '"+xschedule_name+@"' 
                  order by rowid desc ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                xtemplate_id = dr["template_id"].ToString();
            }
            return xtemplate_id;
        }
        private void getItems()
        {
           var xtemplate_id = getTemplateID(hidBidNo.Value, hidBidRevision.Value, hidScheduleName.Value);
           string sql = @"SELECT a.*,b.contract_no FROM [dbo].[ps_xls_template] a
                        left join ps_t_bid_selling_cmpo b on 
                        (a.bid_no = b.bid_no and a.bid_revision = b.bid_revision 
                        and a.schedule_name = b.schedule_name )
                 where b.contract_no='" + hidContractNo.Value + "' and a.template_id = '"+ xtemplate_id + "' ";

            sql += " order by a.part, a.item_no ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gv1.DataSource = ds.Tables[0];
                gv1.DataBind();
               
            }
            
        }
        protected void gv1_OnRowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "")
            {

            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            saveData();
        }
        private DateTime converPickerDateToDate(string xddmmyyyy)
        {
            DateTime xdate = new DateTime();
            try
            {
                xdate = DateTime.ParseExact(xddmmyyyy, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            catch
            {
               
            }

            return xdate;
        }
        private void saveData()
        {
            string sql = ""; 
            string xpart = "";
            string xitem_no = "";
            string xdoc_name = lblDocName.Text ;
            string xdoc_node_id = hidNodeID.Value;
            string xstep_code = ddlDoctype.SelectedValue;
            string xqty = "";
            string xtest_date = "";
            if (txtTestDate.Text == "") { xtest_date = "null"; }
            else
            {
                xtest_date = "'" + converPickerDateToDate(txtTestDate.Text).ToString("yyyy-MM-dd")+ "'";
            }
            string xsubmit_date = "";
            if (txtSubmitDate.Text == "") { xsubmit_date = "null"; }
            { xsubmit_date = "'" + converPickerDateToDate(txtSubmitDate.Text).ToString("yyyy-MM-dd") + "'"; }

            for (int i = 0; i < gv1.Rows.Count; i++)
            {
                if (txtTestDate.Text == "") { xtest_date = "null"; }
                else
                {
                    converPickerDateToDate(txtTestDate.Text).ToString("yyyy-MM-dd");
                }
                xsubmit_date = "";
                if (txtSubmitDate.Text == "") { xsubmit_date = "null"; }
                { xsubmit_date = converPickerDateToDate(txtSubmitDate.Text).ToString("yyyy-MM-dd"); }

                if ( ((CheckBox)gv1.Rows[i].FindControl("gvSelect")).Checked == true)
                {
                    xpart = ((Label)gv1.Rows[i].FindControl("gvPart")).Text;
                    xitem_no = ((Label)gv1.Rows[i].FindControl("gvItemNo")).Text;
                    try
                    {
                        xqty = ((TextBox)gv1.Rows[i].FindControl("gvInputQty")).Text;
                        if (xqty == "") { xqty = "0"; }
                    }
                    catch
                    {
                        xqty = "0";
                    }
                    sql = @" insert into ps_t_qc_item_upload (
                bid_no, bid_revision, schedule_name, contract_no,
                part, item_no, upload_docname, upload_docnode_id,
                step_code, qty, test_date, submit_date
                ) values(
                '" + hidBidNo.Value + @"' 
                ," + hidBidRevision.Value + @"
                ,'" + hidScheduleName.Value + @"'
                ,'" + hidContractNo.Value + @"'
                ,'" + xpart + @"'
                ,'" + xitem_no + @"'
                ,'" + xdoc_name + @"'
                ,'" + xdoc_node_id + @"'
                ,'" + xstep_code + @"'
                ," + xqty + @"
                ," + xtest_date + @"
                ," + xsubmit_date + @"
                ) ";
                    zdb.ExecNonQuery(sql, zconnectionstr);
                }
               

            }
            Response.Write("<script> alert('-Save complete-');</script>");

        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            var xcontent = FileUpload1.FileBytes;
            var xdocname = FileUpload1.FileName;
            var xnode_contract = zot.getNodeByName(zQC_NodeID, ddlContractNo.SelectedValue);
            if (xnode_contract == null)
            {
                xnode_contract = zot.createFolder(zQC_NodeID, ddlContractNo.SelectedValue);
            }
            var xnode_doctype = zot.getNodeByName(xnode_contract.ID.ToString(), ddlDoctype.SelectedValue);
            if (xnode_doctype == null)
            {
                xnode_doctype = zot.createFolder(xnode_contract.ID.ToString(), ddlDoctype.SelectedValue);
            }
            var xnode_parent = zot.getNodeByName(xnode_doctype.ID.ToString(), System.DateTime.Now.ToString("yyyy-MM-dd"));
            if (xnode_parent == null)
            {
                xnode_parent = zot.createFolder(xnode_doctype.ID.ToString(), System.DateTime.Now.ToString("yyyy-MM-dd"));
            }
            var xdoc_nodeid = zot.uploadDoc(xnode_parent.ID.ToString(), xdocname, xdocname, xcontent, "");
            hidNodeID.Value = xdoc_nodeid.ToString();
            lblDocName.Text = xdocname; 
        }
    }
}