﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class frmQC01 : System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        OTFunctions zot = new OTFunctions();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

             
                try { hidBidNo.Value = Request.QueryString["bid_no"].ToString(); }
                catch { hidBidNo.Value = ""; }
                try { hidBidRevision.Value = Request.QueryString["bid_revision"].ToString(); }
                catch { hidBidRevision.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); }
                catch { hidLogin.Value = ""; }
                // Debug 
                //pr_no = "PR-1-132021"; 
                hidLogin.Value = "z572470";
                hidMode.Value = getMode();
                //hidMode.Value = "View";
                lblMode.Text = hidMode.Value.ToUpper();
               
                ini_Data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        private string getMode()
        {
            string xmode = "View";
            var xtoken = zot.getTokenAdmin();
            var xmembers = zot.getMembers("PSD_QC_APPROVER", xtoken);
            for (int i = 0; i < xmembers.Rows.Count; i++)
            {
                if (hidLogin.Value == xmembers.Rows[i]["name"].ToString())
                {
                    xmode = "Assign";break; 
                }
            }
            return xmode; 
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        #region ini_data
        private void ini_ddlBid()
        {
            string sql = "select distinct bid_no as col1, bid_no as col2 from ps_xls_template order by bid_no ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlBidNo.Items.Clear();
            ddlBidNo.DataSource = ds.Tables[0];
            ddlBidNo.DataTextField = ds.Tables[0].Columns[0].ColumnName;
            ddlBidNo.DataValueField = ds.Tables[0].Columns[1].ColumnName;
            ddlBidNo.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Please select Bid No.-";
            li.Value = "";
            ddlBidNo.Items.Insert(0, li);
            ddlBidNo.SelectedIndex = 0;
        }
        private void ini_ddlSchedule()
        {
            ddlSchedule.Items.Clear();
            if (ddlBidNo.SelectedIndex > 0)
            {
                string sql = "select distinct schedule_name as col1, schedule_name as col2 " +
                    "   from ps_xls_template " +
                    " where bid_no = '" + ddlBidNo.SelectedValue.ToString() + @"' order by schedule_name  ";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);

                ddlSchedule.DataSource = ds.Tables[0];
                ddlSchedule.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                ddlSchedule.DataValueField = ds.Tables[0].Columns[1].ColumnName;
                ddlSchedule.DataBind();
                ListItem li = new ListItem();
                li.Text = "-Please select Schedule.-";
                li.Value = "";
                ddlSchedule.Items.Insert(0, li);
                ddlSchedule.SelectedIndex = 0;
            }
        }
        private void get_BidRevision()
        {
            string sql = " select distinct bid_revision from ps_xls_template where bid_no = '" + ddlBidNo.SelectedValue.ToString() + @"' " +
                " order by bid_revision desc ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
               
                lblBidRevision.Text = ds.Tables[0].Rows[0]["bid_revision"].ToString();
            }
        }
        private void get_BidDesc()
        {
            if (ddlBidNo.SelectedIndex > 0)
            {
                string sql = " select bid_revision, bid_desc from bid_no where bid_no = '" + ddlBidNo.SelectedValue.ToString() + @"' and status = 'Active' ";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtBidDesc.Text = StripHTML( ds.Tables[0].Rows[0]["bid_desc"].ToString());
                    get_BidRevision();
                }
            }
        }
        private void ini_Data()
        {
            ini_ddlBid();
            ini_ddlSchedule();
            ini_gv();
            if (lblMode.Text == "ASSIGN") { divAssignment.Visible = true; } else { divAssignment.Visible = false; }
        }
        private void ini_gv()
        {
           
            DataTable dt = new DataTable();
            
            dt.Columns.Add("bid_no");
            dt.Columns.Add("bid_revision");
            dt.Columns.Add("contract_no");
            dt.Columns.Add("vendor_code");
            dt.Columns.Add("schedule_name");
            dt.Columns.Add("item_no");
            dt.Columns.Add("description");
            dt.Columns.Add("qty");
            dt.Columns.Add("unit");
            dt.Columns.Add("waive_test",typeof(Boolean));
            dt.Columns.Add("ttPD");
            dt.Columns.Add("ttDD");
            dt.Columns.Add("ttTD");
            dt.Columns.Add("ttms");
            dt.Columns.Add("ttqp");
            dt.Columns.Add("ttts");
            dt.Columns.Add("tttp");
            dt.Columns.Add("ttiw");
            dt.Columns.Add("ttir");
            dt.Columns.Add("ttcr");
            dt.Columns.Add("tttr");
            dt.Columns.Add("ttlatr");
            dt.Columns.Add("resby");
            dt.Columns.Add("remark");
            var dr = dt.NewRow();
            dr["waive_test"] = false;
            dr["ttPD"] = "0";
            dr["ttDD"] = "0";
            dr["ttTD"] = "0";
            dr["ttms"] = "0";
            dr["ttqp"] = "0";
            dr["ttts"] = "0";
            dr["tttp"] = "0";
            dr["ttiw"] = "0";
            dr["ttir"] = "0";
            dr["ttcr"] = "0";
            dr["tttr"] = "0";
            dr["ttlatr"] = "0";
            dr["ttlatr"] = "0";
            dr["ResBy"] = "...";
            dr["remark"] = "";
            dt.Rows.Add(dr);
            gv1.DataSource = dt;
            gv1.DataBind();
        }
        #endregion
        protected void gv1_OnRowCommand(object sender, CommandEventArgs e)
        {
            string url = "";
            string xbid_no = "";
            string xbid_revision = "";
            string xschedule_name = "";
            string xitem_no = "";
            string xmode = ""; 

            if (e.CommandName == "inputdocument")
            {
                int a = System.Convert.ToInt32(e.CommandArgument);
                xbid_no = ((Label)gv1.Rows[a].FindControl("gvBidNo")).Text;
                xbid_revision = ((Label)gv1.Rows[a].FindControl("gvBidRevision")).Text;
                xschedule_name = ((Label)gv1.Rows[a].FindControl("gvSchedule_name")).Text;
                xitem_no = ((Label)gv1.Rows[a].FindControl("gvItemNo")).Text;
                xmode = "Upload";
                url = "frmQC02.aspx?bid_no=" + xbid_no + "&bid_revision=" + xbid_revision + "&schedule_name=" + xschedule_name + "&item_no=" + xitem_no + "&zmode=" + xmode;
                Response.Write("<script>window.open('"+url+"');</script>");
            }
            if (e.CommandName == "editdata")
            {
                int a = System.Convert.ToInt32(e.CommandArgument);
                xbid_no = ((Label)gv1.Rows[a].FindControl("gvBidNo")).Text;
                xbid_revision = ((Label)gv1.Rows[a].FindControl("gvBidRevision")).Text;
                xschedule_name = ((Label)gv1.Rows[a].FindControl("gvSchedule_name")).Text;
                xitem_no = ((Label)gv1.Rows[a].FindControl("gvItemNo")).Text;
                xmode = "Edit";
                url = "frmQC02.aspx?bid_no=" + xbid_no + "&bid_revision=" + xbid_revision + "&schedule_name=" + xschedule_name + "&item_no=" + xitem_no + "&zmode=" + xmode;
                Response.Write("<script>window.open('" + url + "');</script>");
            }
        }

        protected void ddlBidNo_SelectedIndexChanged(object sender, EventArgs e)
        {
            ini_ddlSchedule();
            get_BidDesc();
        }

        protected void ddlSchedule_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (ddlSchedule.SelectedIndex > 0)
            {
                bind_gv1();
            }
            else { ini_gv();  }
        }
        #region gv1

        private void bind_gv1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("bid_no");
            dt.Columns.Add("bid_revision");
            dt.Columns.Add("contract_no");
            dt.Columns.Add("vendor_code");
            dt.Columns.Add("schedule_name");
            dt.Columns.Add("item_no");
            dt.Columns.Add("description");
            dt.Columns.Add("qty");
            dt.Columns.Add("unit");
            dt.Columns.Add("waive_test", typeof(Boolean));
            dt.Columns.Add("ttPD");
            dt.Columns.Add("ttDD");
            dt.Columns.Add("ttTD");
            dt.Columns.Add("ttms");
            dt.Columns.Add("ttqp");
            dt.Columns.Add("ttts");
            dt.Columns.Add("tttp");
            dt.Columns.Add("ttiw");
            dt.Columns.Add("ttir");
            dt.Columns.Add("ttcr");
            dt.Columns.Add("tttr");
            dt.Columns.Add("ttlatr");
            dt.Columns.Add("resby");
            dt.Columns.Add("remark");
            string sql = "";
            if (isExistingRecords()== false)
            {
                // select from xls_template and insert into ps_t_qc_item
                sql = "select * from ps_xls_template where bid_no ='" + ddlBidNo.SelectedValue + "' and bid_revision = " + lblBidRevision.Text;
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dr0 = ds.Tables[0].Rows[i];
                        var dr = dt.NewRow();
                        dr["bid_no"] = ddlBidNo.SelectedValue.ToString();
                        dr["bid_revision"] = lblBidRevision.Text.Trim();
                        dr["contract_no"] = txtContractNo.Text.Trim();
                        dr["vendor_code"] = txtVendor.Text.Trim();
                        dr["schedule_name"] = ddlSchedule.SelectedValue.ToString();
                        dr["item_no"] = dr0["item_no"].ToString();
                        dr["description"] = dr0["description"].ToString();
                        dr["qty"] = dr0["qty"].ToString();
                        dr["unit"] = dr0["unit"].ToString();
                        if (dr0["waive_test"].ToString() != "1")
                        {
                            dr["waive_test"] = false;
                        }
                        else if (dr0["waive_test"].ToString() == "1")
                        {
                            dr["waive_test"] = true;
                        }
                        
                        dr["ttPD"] = "0";
                        dr["ttDD"] = "0";
                        dr["ttTD"] = "0";
                        dr["ttms"] = "0";
                        dr["ttqp"] = "0";
                        dr["ttts"] = "0";
                        dr["tttp"] = "0";
                        dr["ttiw"] = "0";
                        dr["ttir"] = "0";
                        dr["ttcr"] = "0";
                        dr["tttr"] = "0";
                        dr["ttlatr"] = "0";
                        dr["ttlatr"] = "0";
                        dr["ResBy"] = "";
                        dr["remark"] = "";
                        dt.Rows.Add(dr);
                        addQCItem(dr);
                    }
                }
            }
            dt.Rows.Clear();
            // Select from ps_t_qc_item
            sql = "select * from ps_t_qc_item where bid_no = '" + ddlBidNo.SelectedValue + "' and bid_revision = " + lblBidRevision.Text + " and Schedule_name = '" + ddlSchedule.SelectedValue.ToString() + "' ";
            var ds1 = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds1.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds1.Tables[0].Rows.Count; i++)
                {
                    var dr0 = ds1.Tables[0].Rows[i];
                    var dr = dt.NewRow();
                    dr["bid_no"] = ddlBidNo.SelectedValue.ToString();
                    dr["bid_revision"] = lblBidRevision.Text.Trim();
                    dr["contract_no"] = dr0["contract_no"].ToString();
                    dr["vendor_code"] = dr0["vendor_code"].ToString();
                    dr["schedule_name"] = dr0["schedule_name"].ToString();
                    dr["item_no"] = dr0["item_no"].ToString();
                    dr["description"] = dr0["description"].ToString();
                    dr["qty"] = dr0["qty"].ToString();
                    dr["unit"] = dr0["unit"].ToString();
                    dr["waive_test"] = System.Convert.ToBoolean(dr0["waive_test"].ToString());
                    dr["ttPD"] = "0";
                    dr["ttDD"] = "0";
                    dr["ttTD"] = "0";
                    dr["ttms"] = "0";
                    dr["ttqp"] = "0";
                    dr["ttts"] = "0";
                    dr["tttp"] = "0";
                    dr["ttiw"] = "0";
                    dr["ttir"] = "0";
                    dr["ttcr"] = "0";
                    dr["tttr"] = "0";
                    dr["ttlatr"] = "0";
                    dr["ttlatr"] = "0";
                    dr["ResBy"] = dr0["res_fullname"].ToString();
                    dr["remark"] = dr0["remark"].ToString();
                    dt.Rows.Add(dr);

                }
            }

            gv1.DataSource = dt;
            gv1.DataBind();
            bind_itemno(dt);
            bind_stepname();
            bind_monitor(); 
        }
        private bool isExistingRecords()
        {
            bool xfound = false;
            string sql = "select * from ps_t_qc_item where bid_no ='" + ddlBidNo.SelectedValue.ToString() + "' and bid_revision = " + lblBidRevision.Text;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xfound = true;
            }
            return xfound; 
        }
        private void addQCItem(DataRow dr)
        {
            string sql = "insert into ps_t_qc_item (" +
                " bid_no, bid_revision, contract_no, vendor_code, schedule_name," +
                " item_no, description, qty, unit, waive_test, " +
                " res_emp_id, res_fullname, res_s_position,res_f_position, " +
                " task_stepname, task_status, assigned_date, assigned_by, remark) " +
                " values(" +
                " '" + dr["bid_no"].ToString() + "' " +
                " ," + dr["bid_revision"].ToString() + " " +
                " ,'" + dr["contract_no"].ToString() + "' " +
                " ,'" + dr["vendor_code"].ToString() + "' " +
                " ,'" + dr["schedule_name"].ToString() + "' " +
                " ,'" + dr["item_no"].ToString() + "' " +
                " ,'" + dr["description"].ToString() + "' " +
                " ," + dr["qty"].ToString() + " " +
                " ,'" + dr["unit"].ToString() + "' " +
                " ,'" + dr["waive_test"].ToString() + "' " +
                " ,'" + "" + "' " +
                " ,'" + "" + "' " +
                " ,'" + "" + "' " +
                " ,'" + "" + "' " +
                " ,'" + "" + "' " +
                " ,'" + "" + "' " +
                " ," + "null" + " " +
                " ,'" + "" + "' " +
                 " ,'" + "" + "' " +
                " )";
            zdb.ExecNonQuery(sql, zconnectionstr);
            
        }
        private void bind_monitor()
        {
            string xitem = "";
            string xbid_no = ddlBidNo.SelectedValue.ToString();
            string xbid_revision = lblBidRevision.Text;
            string xschedule_name = ddlSchedule.SelectedValue.ToString();
            string xstatus = "";
            string temp_imageurl = "";
            string sql = "";
            string xqc_no = hidQCNo.Value; 
            for (int a = 0; a < gv1.Rows.Count; a++)
            {
               xitem = ((Label)gv1.Rows[a].FindControl("gvItemNo")).Text;
                //MS	QP	TS	TP	IW	IR	CR	TR	LATR
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'MS' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString(); 
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvMS")).ImageUrl= "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvMS")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'QP' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvQP")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvQP")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'TS' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvTS")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvTS")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'TP' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvTP")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvTP")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'IW' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvIW")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvIW")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'QP' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvQP")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvQP")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'CR' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvCR")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvCR")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'TR' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvTR")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvTR")).ImageUrl = "~/images/icon_green.png";
                    }
                }
                sql = " select * from ps_t_qc_item_stepname where task_stepname = 'LATR' and bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision + " and schedule_name = '" + xschedule_name + "' and item_no = '" + xitem + "' ";
                ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    xstatus = ds.Tables[0].Rows[0]["status"].ToString();
                    if (xstatus.ToUpper() == "NEW")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvLATR")).ImageUrl = "~/images/icon_orange.png";
                    }
                    else if (xstatus.ToUpper() == "COMPLETE")
                    {
                        ((ImageButton)gv1.Rows[a].FindControl("gvLATR")).ImageUrl = "~/images/icon_green.png";
                    }
                }
            }
             
        }
        private void bind_itemno(DataTable dt)
        {
            ddlItemNo.Items.Clear();
            ddlItemNo.DataSource = dt;
            ddlItemNo.DataTextField = dt.Columns["item_no"].ColumnName;
            ddlItemNo.DataValueField = dt.Columns["item_no"].ColumnName;
            ddlItemNo.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Select Item No-";
            li.Value = "";
            ddlItemNo.Items.Insert(0, li);

        }
        private void bind_stepname()
        {
            string sql = " select step_code, ( step_name + '('+step_code+')' ) as stepname from ps_t_qc_stepname order by step_no";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlStepName.Items.Clear();
            ddlStepName.DataSource = ds.Tables[0];
            ddlStepName.DataTextField = ds.Tables[0].Columns[1].ColumnName;
            ddlStepName.DataValueField = ds.Tables[0].Columns[0].ColumnName;
            ddlStepName.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Select assign task for step..-";
            li.Value = "";
            ddlStepName.Items.Insert(0, li);
        }
        #endregion

        protected void gvSelectAll_CheckedChanged(object sender, EventArgs e)
        {
            var xselect = true;
            if (((CheckBox)gv1.HeaderRow.FindControl("gvSelectAll")).Checked)
            { xselect = true; } else { xselect = false; }

                for (int i = 0; i < gv1.Rows.Count; i++)
                {
                    ((CheckBox)gv1.Rows[i].FindControl("gvSelect")).Checked = xselect;
                }
            
        }

        protected void btnAssign_Click(object sender, EventArgs e)
        {
            saveAssignment();
            sendEmail();
            bind_monitor();
        }
        private void sendEmail()
        {
            string xsubject = "Please update " + ddlStepName.SelectedItem.ToString() + " ";
            string xdisplay = "ECM Admin";
            string xtoempid = ucEmp1.getEmpID();
            
            EmpInfoClass empInfo = new EmpInfoClass();
            var xto_user = empInfo.getInFoByEmpID(xtoempid);

            string xbody = " Dear Khun " + xto_user.SNAME + ", <p></p>";
            xbody += txtAssignment.Text + " ";
            xbody += "<br>";
            xbody += " please click link to update => " + "http://ecmqacon1.egat.co.th/ps/form/qc/frmqc02.aspx?qcno="+hidQCNo.Value;
            xbody += "<br>";
            xbody += " ECM System Admin";
            MailNoti mailnoti = new MailNoti();

            mailnoti.SendEmail(xsubject, xdisplay,xbody,"eknawat.choojaisiriroj@atos.net", "", "eknawat@gmail.com");
            Response.Write("<script> alert('-Send assignment successfully-');</script>");
        }
        private string genQCDOCNo()
        {
            string qcno = "";
            string xno = "";
            string xprefix = "QC";
            string xyearthai = (System.DateTime.Now.Year + 543).ToString();
            int xrunno = 0;
            string sql = "select * from wf_runno where prefix = '" + xprefix + "' and year_thai = " + xyearthai;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xrunno = System.Convert.ToInt32(ds.Tables[0].Rows[0]["runno"].ToString()) + 1;
                // Update
                sql = " update wf_runno set runno = " + xrunno.ToString() + " where  prefix = '" + xprefix + "' and year_thai = " + xyearthai;
                zdb.ExecNonQueryReturnID(sql, zconnectionstr);
            }
            else
            {
                xrunno = 1;
                // Insert 
                sql = " insert into wf_runno (prefix, year_thai, runno) " +
                    " values (" +
                    " '" + xprefix + "' " +
                    ", '" + xyearthai + "' " +
                    ", " + xrunno.ToString() + ") ";
                zdb.ExecNonQuery(sql, zconnectionstr);
            }
            qcno = xprefix + "-" + xyearthai.ToString() + xrunno.ToString("00000");
            return qcno;
        }
        private void saveAssignment()
        {
            string xqc_no = genQCDOCNo();
            hidQCNo.Value = xqc_no; 
            string sql = "";
            sql = " insert into ps_t_qc_assignment (qc_no, bid_no, bid_revision, contract_no, vendor_code, schedule_name, task_stepname, description" +
                ", requestor_id,  reviewer_id, approver_id ) " +
                "values(" +
                "'" + xqc_no + "' " +
                ", '" + ddlBidNo.SelectedValue.ToString() + "' " +
                ", " + lblBidRevision.Text + " " +
                ", '" + txtContractNo.Text.Trim() + "' " +
                ", '" + txtVendor.Text.Trim() + "' " +
                ", '" + ddlSchedule.SelectedValue.ToString() + "' " +
                 ", '" + ddlStepName.SelectedValue.ToString() + "' " +
                  ", '" +txtAssignment.Text  + "' " +
                   ", '" + ucEmp1.getEmpID() + "' " +
                    ", '" + hidLogin.Value + "' " +
                     ", '" + hidLogin.Value + "' " +
                ") ";
            zdb.ExecNonQuery(sql, zconnectionstr);
            string xitem = ""; 
            for (int a = 0; a < gv1.Rows.Count; a++)
            {
               
                    if (((CheckBox)gv1.Rows[a].FindControl("gvSelect")).Checked == true)
                    {
                        xitem = ((Label)gv1.Rows[a].FindControl("gvItemNo")).Text ;
                    }

                
                sql = " insert into ps_t_qc_item_stepname " +
                    "( qc_no, bid_no, bid_revision, contract_no, vendor_code, schedule_name, item_no, task_stepname, status )" +
                    " values (" +
                     "'" + xqc_no + "' " +
                    ", '" + ddlBidNo.SelectedValue.ToString() + "' " +
                    ", " + lblBidRevision.Text + " " +
                    ", '" + txtContractNo.Text.Trim() + "' " +
                    ", '" + txtVendor.Text.Trim() + "' " +
                    ", '" + ddlSchedule.SelectedValue.ToString() + "' " +
                    ", '" + xitem + "' " +
                    ", '" + ddlStepName.SelectedValue.ToString() + "' " +
                    ", '" + "NEW" + "' " +
                    ") ";
                zdb.ExecNonQuery(sql, zconnectionstr);
            }
        }
        protected void ddlStepName_SelectedIndexChanged(object sender, EventArgs e)
        {
            string xassignment = "Please update " + ddlStepName.SelectedItem.ToString() + " ";
            xassignment += " For Bid No. " + ddlBidNo.SelectedValue.ToString() + " Schedule : " + ddlSchedule.SelectedValue.ToString();
            xassignment += " Item No : " + getSelectedItem()+ "";
            txtAssignment.Text = xassignment;

        }
        private string getSelectedItem()
        {
            string xitem = "";
            if (ddlItemNo.SelectedIndex > 0)
            {
                xitem = ddlItemNo.SelectedValue.ToString(); 
            }
            else
            {
                for (int a = 0; a < gv1.Rows.Count; a++)
                {
                    if (((CheckBox)gv1.Rows[a].FindControl("gvSelect")).Checked == true)
                    {
                        xitem += " " + ((Label)gv1.Rows[a].FindControl("gvItemNo")).Text + ",";
                    }
                    
                }
                if (xitem.Length > 0) { xitem = xitem.Substring(0, xitem.Length - 1); }
            }
            return xitem; 
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool xselect = false;
            string xbid_no = "";
            string xbid_revision = "";
            string xcontract_no = "";
            string xvendor_no = "";
            string xschedule_name = "";
            string xitem_no = ""; 
            string sql = "";

            for (int i = 0; i < gv1.Rows.Count; i++)
            {
                xselect = ((CheckBox)gv1.Rows[i].FindControl("gvSelect")).Checked;
                xbid_no = ((TextBox)gv1.Rows[i].FindControl("gvBidNo")).Text;
                xbid_revision = ((TextBox)gv1.Rows[i].FindControl("gvBidRevision")).Text;
                xcontract_no = ((TextBox)gv1.Rows[i].FindControl("gvContract_no")).Text;
                xvendor_no = ((TextBox)gv1.Rows[i].FindControl("gvVendor_no")).Text;
                xschedule_name = ((TextBox)gv1.Rows[i].FindControl("gvSchedule_name")).Text;
                xitem_no = ((TextBox)gv1.Rows[i].FindControl("gvItemNo")).Text;
                if (xselect == true)
                {
                    sql = " update ps_t_qc_item set ";
                    sql += " res_fullname = " + ((TextBox)gv1.Rows[i].FindControl("gvResBy")).Text.Trim() + "'";
                    sql += ", remark = '" + ((TextBox)gv1.Rows[i].FindControl("gvRemark")).Text.Trim() + "'";
                    sql += " where ";
                    sql += " bid_no = '" +xbid_no+ "' ";
                    sql += " and bid_revision = " + xbid_revision + " ";
                    sql += " and contract_no = '" + xcontract_no+ "' ";
                    sql += " and vendor_code = '" + xvendor_no + "' ";
                    sql += " and schedule_name = '" + xschedule_name + "' ";
                    sql += " and item_no = '" + xitem_no+ "' ";
                    zdb.ExecNonQuery(sql, zconnectionstr);
                }
            }
        }

        protected void ddlItemNo_SelectedIndexChanged(object sender, EventArgs e)
        {
              string xassignment = "Please update " + ddlStepName.SelectedItem.ToString() + " ";
                xassignment += " For Bid No. " + ddlBidNo.SelectedValue.ToString() + " Schedule : " + ddlSchedule.SelectedValue.ToString();
                xassignment += " Item No : " + getSelectedItem() + "";
                txtAssignment.Text = xassignment;
        
        }
    }
}