﻿using PS_Library;
using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.QC
{
    public partial class DBQC05 : System.Web.UI.Page
    {
       
        clsDashboard objDB = new clsDashboard();
        clsDropdownList objDrp = new clsDropdownList();
        public class returnsave
        {
            public string err; //0 = no error, 1=error
            public string errmsg;
        }
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z594900";

                if (Request.QueryString["bidno"] != null)
                    hidBidNo.Value = Request.QueryString["bidno"];
                else
                    hidBidNo.Value = "TS12-S-02";

                if (Request.QueryString["rev"] != null)
                    hidBidRev.Value = Request.QueryString["rev"];
                else
                    hidBidRev.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;


                var dt = objDrp.drpDepartment();
                //drpDept.Items.Clear();
                //drpDept.Items.Insert(0, new ListItem("--เลือกหน่วยงาน--", ""));
                //drpDept.DataSource = dt;
                //drpDept.DataTextField = "depart_code";
                //drpDept.DataValueField = "depart_name";
                //drpDept.DataBind();

                drpDept.Items.Clear();
                drpDept.AppendDataBoundItems = true;
                drpDept.Items.Add(new ListItem("--เลือกหน่วยงาน--", ""));
                drpDept.DataSource = dt;
                drpDept.DataTextField = "depart_code";
                drpDept.DataValueField = "depart_name";
                drpDept.DataBind();

                //drpAssign.Items.Clear();
                //drpAssign.AppendDataBoundItems = true;
                //drpAssign.Items.Add(new ListItem("--เลือกผู้ปฏิบัติงาน--", ""));
                //drpAssign.DataSource = dt;
                //drpAssign.DataTextField = "depart_code";
                //drpAssign.DataValueField = "depart_name";
                //drpAssign.DataBind();
                drpAssign.Items.Clear();
                drpAssign.Items.Add(new ListItem("--เลือก--", ""));
                bindData();

            }
        }

        protected void drpDept_SelectedIndexChanged(object sender, EventArgs e)
        {
            drpAssign.Items.Clear();
            drpAssign.Items.Add(new ListItem("--เลือก--", ""));
            drpAssign.AppendDataBoundItems = true;
            string dept = drpDept.SelectedValue;
            EmpInfoClass empInfo = new EmpInfoClass();
            string[] staffList = empInfo.getEmpIDAndNameInDepartment(dept);

            foreach (string staffInfo in staffList)
            {
                string[] staff = staffInfo.Split(':');

                ListItem item1 = new ListItem();
                item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                item1.Value = staff[0].ToString();
                drpAssign.Items.Add(item1);
            }


            //drpAssign.DataBind();

        }



        private void bindData()
        {
            DataTable dt = objDB.getDataDBQC2("","");
            gvXls.DataSource = dt;
            gvXls.DataBind();

        }

        [System.Web.Services.WebMethod]
        public static returnsave savedata(int id, string shipment_date, string remark)
        {
            clsDashboard objDB = new clsDashboard();
            returnsave objreturnsave = new returnsave();
            objreturnsave.err = "0";
            QC02 list = new QC02();
            try
            {

                list.row_id = Convert.ToInt32(id);
                list.shipment_date = Convert.ToDateTime(shipment_date.Trim().ToString()).ToString("MM/dd/yyyy");
                list.remark = remark.Trim().ToString();
                objDB.AssignQC2Display(list);
            }
            catch (Exception e)
            {
                objreturnsave.err = "1";
                objreturnsave.errmsg = "Error : " + e.Message.ToString();
            }
            return objreturnsave;
        }


        [System.Web.Services.WebMethod]
        public static QC02 GetData(int id)
        {
            clsDashboard objDB = new clsDashboard();
           
            QC02 Data = new QC02();
            var result = objDB.getDataDBQC2_display(id);

            //var aaa = "";
            Data.row_id = result.row_id;
            Data.contract_no = result.contract_no;
            Data.schedule_name = result.schedule_name;
            Data.shipment_date = result.shipment_date;
            Data.remark = result.remark;
            return Data;
        }
        protected DataTable refreshgrid()
        {
            string rowfilter = "";
            string contract_no = txtContract.Text.Trim();
            string desc = txtDesc.Text.Trim();
            string job_no = txtJobNo.Text.Trim();

            string dept = drpDept.SelectedValue;
            //string Sch = ddlSch.SelectedValue;
            //string Part = ddlPart.SelectedValue;
            
            string assign = drpAssign.SelectedValue;
            if (contract_no != "")
            {
                rowfilter = string.Format("(a.contract_no = '{0}')", contract_no);
            }
            if (desc != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(a.schedule_name = '{0}')", desc);
                else rowfilter = string.Format("{0} and a.schedule_name = '{1}'", rowfilter, desc);
            }

            if (dept != "")
            {
                if (dept != "กคภ-ส.")
                {
                    if (rowfilter == "") rowfilter = string.Format("(a.schedule_name = '{0}')", dept);
                    else rowfilter = string.Format("{0} and a.schedule_name = '{1}'", rowfilter, dept);
                }
            }
            if (assign != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(b.assign_to  = '{0}')", assign);
                else rowfilter = string.Format("{0} and b.assign_to = '{1}'", rowfilter, assign);
            }
            //if (Sch != "")
            //{
            //    if (rowfilter == "") rowfilter = string.Format("(schedule_name = '{0}')", Sch);
            //    else rowfilter = string.Format("{0} and schedule_name = '{1}'", rowfilter, Sch);
            //}
            //if (Part != "")
            //{
            //    if (rowfilter == "") rowfilter = string.Format("(part_no = '{0}')", Part);
            //    else rowfilter = string.Format("{0} and part_no = '{1}'", rowfilter, Part);
            //}



            DataTable dtrowfil = objDB.getDataDBQC2(rowfilter, job_no);
            return dtrowfil;
        }




        protected void btnFil_Click(object sender, EventArgs e)
        {
            //string rowfilter = txtContract.Text.Trim();
            DataTable dtrowfil = refreshgrid();
            gvXls.DataSource = dtrowfil;
            gvXls.DataBind();
        }
        protected void gvXls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
          
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                Label lbAssing = new Label();

                DropDownList drpAssing = (DropDownList)e.Row.FindControl("drpAssing");
                HiddenField HidDDLeq = (HiddenField)e.Row.FindControl("HidDDLeq");
                lbAssing = (Label)e.Row.FindControl("lbAssing");

                EmpInfoClass empInfo = new EmpInfoClass();
                string[] staffList = empInfo.getEmpIDAndNameInDepartment("กคภ-ส.");


                List<DropdownList> list = new List<DropdownList>();

                foreach (string staffInfo in staffList)
                {
                    string[] staff = staffInfo.Split(':');

                    list.Add(new DropdownList
                    {
                        value = staff[0].ToString(),
                        name = staff[1].ToString()
                    });

                }
                //lbAssing.Text = list.Select(l => l.value = drv["assign_to"].ToString());
                //lbAssing.Text = list.Where(w => w.value == drv["assign_to"].ToString());
                //lbAssing.Text = list.Items.FindByValue(drv["assign_to"].ToString()).Text;

                var x =  from c in list
                         where c.value == drv["assign_to"].ToString()
                         select c.name;


                lbAssing.Text = x.FirstOrDefault();

            }
        }
        
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                List<QC02> list = new List<QC02>();
                foreach (GridViewRow gv in gvXls.Rows)
                {
                    //GridView grdViewDetail = (GridView)gv.FindControl("gvXls");
                    Label contract_no = (Label)gv.FindControl("lbConNo");
                    Label schedule_name = (Label)gv.FindControl("lbDesc");
                    
                    HiddenField row_id = (HiddenField)gv.FindControl("hdfRowID");
                    HiddenField bid_revision = (HiddenField)gv.FindControl("hdfBidRe");
                    HiddenField bid_no = (HiddenField)gv.FindControl("hdfBidNo");
                    DropDownList drpAssing = (DropDownList)gv.FindControl("drpAssing");                 
                    list.Add(
                    new QC02
                    {
                        row_id = Convert.ToInt32(row_id.Value),
                        contract_no = contract_no.Text,
                        schedule_name = schedule_name.Text,
                        bid_no = bid_no.Value,
                        bid_revision = bid_revision.Value,
                        assign_to = drpAssing.SelectedValue,
                    });
                }
                objDB.AssignQC2(list);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
            }catch(Exception ex)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('"+ex.Message.ToString()+"');", true);
            }
        }
    }
}