﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DBQC03.aspx.cs" Inherits="PS_System.form.QC.DBQC03" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>QC Dashboard - Upload Files</title>
      <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
     $(function () {
        $('[id*=txtSubmitDate]').datepicker({
            changeMonth: true,
            changeYear: true,
            format: "dd/mm/yyyy",
            language: "tr"
        });
        $('[id*=txtTestDate]').datepicker({
            changeMonth: true,
            changeYear: true,
            format: "dd/mm/yyyy",
            language: "tr"
        });
     });
     </script>
    <style type="text/css">
        .auto-style1 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
         <div>
              <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
               
            </table>
        </div>
        <div id="divUpload" runat="server" class="div_90">
            <label class="Text_400">Upload Files</label>
            <p>
            </p>
            <table>
                       <tr>
                    <td>
                        <asp:Label ID="Label24" runat="server" CssClass="Text_200" Text="Contract No."></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlContractNo" runat="server" CssClass="ddl-md" width="400px" AutoPostBack="True" OnSelectedIndexChanged="ddlContractNo_SelectedIndexChanged">
                        </asp:DropDownList>

                    </td>
                    <td>
                        <asp:Label ID="lblVendor" runat="server" CssClass="Text_200"></asp:Label>
                           </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Label ID="lblBidNo" runat="server" CssClass="Text_200"></asp:Label>
&nbsp;
                        <asp:Label ID="lblBidRevision" runat="server" CssClass="Text_200" Visible="False"></asp:Label>
&nbsp;
                        <asp:Label ID="lblScheduleName" runat="server" CssClass="Text_200"></asp:Label>

                    </td>
                    <td>
                        <asp:Label ID="lblBidDesc" runat="server" CssClass="Text_200"></asp:Label>

                    </td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                 <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                     <tr>
                    <td>
                        <asp:Label ID="Label23" runat="server" CssClass="Text_200" Text="Doc type"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlDoctype" runat="server" CssClass="ddl-md" width="400px">
                        </asp:DropDownList>

                    </td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                     <tr>
                    <td>
                        <asp:Label ID="Label25" runat="server" CssClass="Text_200" Text="Submit Date"></asp:Label>
                    </td>
                    <td>
                                <asp:TextBox ID="txtSubmitDate"  runat="server" CssClass="Text_200"/></td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                  <tr>
                    <td>
                        <asp:Label ID="Label1" runat="server" CssClass="Text_200" Text="Tested Date"></asp:Label>
                    </td>
                    <td>
                                <asp:TextBox ID="txtTestDate"  runat="server" CssClass="Text_200"/></td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label22" runat="server" CssClass="Text_200" Text="Select File"></asp:Label>
                    </td>
                    <td>
                        <asp:FileUpload ID="FileUpload1" runat="server" CssClass="ddl-md" width="400px"/>

                        <asp:HiddenField ID="hidNodeID" runat="server" />

                        <asp:Label ID="lblDocName" runat="server" CssClass="Text_400"></asp:Label>

                    </td>
                    <td>
                        <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn_normal_blue" OnClick="btnUpload_Click" />
                    </td>
                    <td></td>
                    <td></td>
                </tr>
           
         
               
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:Button ID="btnSearch" runat="server" Text="Search Items" CssClass="btn_normal_blue" OnClick="btnSearch_Click" />
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
           
         
               
                <tr>
                    <td>
                        &nbsp;</td>
                    <td colspan="4">
                        <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No data found." Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv1_OnRowCommand" Width="95%">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Select">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="gvSelect" runat="server" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="60px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Part">
                                    <ItemTemplate>
                                        <asp:Label ID="gvPart" runat="server" CssClass="Text_200" Text='<%# Bind("part") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="250px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Item ">
                                    <ItemTemplate>
                                        <asp:Label ID="gvItemNo" runat="server" CssClass="Text_100" Text='<%# Bind("item_no") %>'></asp:Label>
                                         <asp:Label ID="gvEquipCode" runat="server" CssClass="Text_100" Text='<%# Bind("equip_code") %>'></asp:Label>
                                        <asp:Label ID="gvDescription" runat="server" CssClass="Text_200" Text='<%# Bind("Description") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Qty">
                                    <ItemTemplate>
                                        <asp:Label ID="gvQty" runat="server" CssClass="Text_200" Text='<%# Bind("qty") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="80px" />
                                </asp:TemplateField>
                                 <asp:TemplateField HeaderText="Input Qty">
                                    <ItemTemplate>
                                        <asp:TextBox ID="gvInputQty" runat="server" CssClass="Text_100" ></asp:TextBox>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="80px" />
                                </asp:TemplateField>
                             <%--<asp:TemplateField HeaderText="Contract Detail">
                                        <ItemTemplate>
                                            <table>
                                              
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            
                                            </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                            </asp:TemplateField>--%>
                           
                             <%--   <asp:TemplateField HeaderText="SAP Runno">
                                        <ItemTemplate>
                                          
                                                  
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="200px" />
                            </asp:TemplateField>--%>
                           
<%--                         <asp:TemplateField HeaderText="Upload Signed Doc" >
                                        <ItemTemplate>
                                           
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="200px" />
                            </asp:TemplateField>--%>
                            <%-- <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="gvStatus" CssClass="Text_100" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                            </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Hidden" Visible="false">
                                    <ItemTemplate>
                                    <%--<asp:Label ID="Label1" runat="server" Text='<%# Bind("row_id") %>'/>--%>
                                        <asp:Label ID="gvBidNo" runat="server" Text='<%# Bind("bid_no") %>' />
                                        <asp:Label ID="gvBidRevision" runat="server" Text='<%# Bind("bid_revision") %>' />
                                        <asp:Label ID="gvSchedule" runat="server" Text='<%# Bind("schedule_name") %>' />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
           
         
               
                <tr>
                    <td class="auto-style1">
                        </td>
                    <td class="auto-style1" colspan="4">
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn_normal_blue" OnClick="btnSave_Click" />
                    </td>
                </tr>
            </table>
        </div>
         <asp:HiddenField ID="hidBidNo" runat="server" />
         <asp:HiddenField ID="hidBidRevision" runat="server" />
         <asp:HiddenField ID="hidScheduleName" runat="server" />
         <asp:HiddenField ID="hidContractNo" runat="server" />
    </form>
</body>
</html>
