﻿<%@ Page Language="C#" MasterPageFile="~/MainPagesMaster.Master" AutoEventWireup="true" CodeBehind="DBQC04.aspx.cs" Inherits="PS_System.form.QC.DBQC04" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Dashboard QC"></asp:Label>
    <div id="dvMain" runat="server" style="width: 100%; padding-top: 10px;">
        <table style="width: 100%; margin: 10px">
            <tr>
                <td colspan="4" style="text-align: left; width: 8%">
                    <asp:Label ID="Label6" runat="server" Text="Contract No: " Width="6%"></asp:Label>
                    <asp:TextBox ID="txtContract" runat="server" />
                    <br />
                    <asp:Button ID="btnFil" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFil_Click" />
                </td>
            </tr>
        </table>
    </div>
    <div>
        <!-- The Modal -->
        <div id="myModal" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <span class="close" onclick="closePopup()">&times;</span>
                <p class="cls_model_desc"></p>
            </div>
        </div>
        <div id="myModal_Bid" class="modal">
            <!-- Modal content -->
            <div class="modal-content">
                <table style="width: 100%;">
                    <tr>
                        <td colspan="2" style="width: 1000px; height: 300px;">
                            <div id="divTable" style="margin: -130px 30px 0px 30px;"></div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnClose_Popup" ClientIDMode="Static" runat="server" CssClass="w3-right w3-red" BorderWidth="1px" Height="30px" Width="70px"
                                Font-Names="tahoma" Font-Size="10pt" Text="Close" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <!-----------------------------------------END MODAL-------------------------------------->

    </div>
    <div style="width: 100%; overflow: scroll; height: 600px;">
        <asp:GridView ID="gvXls" runat="server" Width="100%"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="true">
            <FooterStyle BackColor="#164094" ForeColor="#000066" />
            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField HeaderText="Part">
                    <ItemTemplate>
                        <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Item No.">
                    <ItemTemplate>
                        <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_No") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Equipment Code">
                    <ItemTemplate>
                        <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <ItemTemplate>
                        <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Left" CssClass="Colsmall2" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="SOE. Unit Price">
                    <ItemTemplate>
                        <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SOE. Amount">
                    <ItemTemplate>
                        <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LSE. Unit Price">
                    <ItemTemplate>
                        <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LSE. Amount">
                    <ItemTemplate>
                        <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LTC. Unit Price">
                    <ItemTemplate>
                        <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LTC. Amount">
                    <ItemTemplate>
                        <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="LI">
                    <ItemTemplate>                        
                        <div class="text-center">
                            <%#
                                Eval("doc_li").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CD">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_cd").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DA">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_da").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="COR">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_cor").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="PD">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_pd").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="RF">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_rf").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SP">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_sp").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TYP">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("doc_typ").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="กวศ-ส.">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("dep1").ToString() !="" ? "<img onclick=getData('" +Eval("dep1").ToString() +","+Eval("equip_code").ToString()+"') src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;cursor:pointer;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="กวอ-ส.">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("dep2").ToString() !="" ? "<img onclick=getData('" +Eval("dep2").ToString() +"') src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;cursor:pointer;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="กวป-ส.">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("dep3").ToString() !="" ? "<img onclick=getData('" +Eval("dep3").ToString() +"') src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;cursor:pointer;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="กวธ-ส.">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("dep4").ToString() !="" ? "<img onclick=getData('" +Eval("dep4").ToString() +"') src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;cursor:pointer;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="กวสส-ส.">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("dep5").ToString() !="" ? "<img onclick=getData('" +Eval("dep5").ToString() +"') src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;cursor:pointer;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="กวส-ส.">
                    <ItemTemplate>
                        <div class="text-center">
                            <%#
                                Eval("dep6").ToString() !="" ? "<img onclick=getData('" +Eval("dep6").ToString() +"') src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;cursor:pointer;' />":"-"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>




                <asp:TemplateField HeaderText="MS">
                    <ItemTemplate>
                        <%--<asp:Label ID="lb_MS" runat="server" Text='<%# Eval("ms").ToString() =="1" ?"<img src='' />":"ccc" %>'></asp:Label>--%>
                        <div>
                            <%# 
                                Eval("ms").ToString() =="1" ? Eval("chk_ms").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />": 
                                Eval("ms").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("ms").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("ms").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="QP">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("qp").ToString() =="1" ? Eval("chk_qp").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />": 
                                Eval("qp").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("qp").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("qp").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TS">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("ts").ToString() =="1" ? Eval("chk_ts").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />":  
                                Eval("ts").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("ts").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("ts").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TP">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("tp").ToString() =="1" ? Eval("chk_tp").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />":  
                                Eval("tp").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("tp").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("tp").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="IW">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("iw").ToString() =="1" ? Eval("chk_iw").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />": 
                                Eval("iw").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("iw").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("iw").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="IR">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("ir").ToString() =="1" ? Eval("chk_ir").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />": 
                                Eval("ir").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("ir").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("ir").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="FIR">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("fir").ToString() =="1" ? Eval("chk_fir").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />":  
                                Eval("fir").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("fir").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("fir").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="CR">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("cr").ToString() =="1" ? Eval("chk_cr").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />": 
                                Eval("cr").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("cr").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("cr").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="TR">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("tr").ToString() =="1" ? Eval("chk_tr").ToString() !="" ? "<img src='../../Vendor/fontawesome/img/circle-solid.svg' style='width:19px;' />" : "<img src='../../Vendor/fontawesome/img/circle-regular.svg' style='width:19px;' />": 
                                Eval("tr").ToString() =="2" ? "<img src='../../Vendor/fontawesome/img/minus-solid.svg' style='width:19px;' />": 
                                Eval("tr").ToString() =="3" ? "<img src='../../Vendor/fontawesome/img/weebly-brands.svg' style='width:19px;' />": 
                                Eval("tr").ToString() =="4" ? Convert.ToInt32(Eval("amt"))>=Convert.ToInt32(Eval("qty")) ? "<img src='../../Vendor/fontawesome/img/check-square-regular.svg' style='width:19px;' />": Convert.ToInt32(Eval("amt"))!=0 ? "<img src='../../Vendor/fontawesome/img/x-square-regular.svg' style='width:19px;' />": "<img src='../../Vendor/fontawesome/img/square-regular.svg' style='width:19px;' />" : ""
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="DAC">
                    <ItemTemplate>
                        <div>
                            <%# 
                                Eval("dac").ToString() =="" ? "<label style='background-color: red;color: white;'>send<label>": "<label style='background-color: green;color: white;'>send<label>"
                            %>
                        </div>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="%W">
                    <ItemTemplate>
                        <asp:Label ID="lb_W" runat="server" Text='<%# Eval("total", "{0:0.00}") %>'>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="%A">
                    <ItemTemplate>
                        <asp:Label ID="lb_A" runat="server" Text='<%# Eval("cnt", "{0:0.00}") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>


            </Columns>
        </asp:GridView>
    </div>
    <%--<div class="text-right mt-15 mr-10">
        <asp:Button ID="btnSave" runat="server" Text="Save" class="btn btn-primary" OnClick="btnSave_Click" />
        <asp:Button ID="btnCancel" runat="server" Text="Cancel" class="btn btn-defalt" />
    </div>--%>
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRev" runat="server" />
    <asp:HiddenField ID="hidJobNo" runat="server" />
    <asp:HiddenField ID="hidJobRev" runat="server" />
    <asp:HiddenField ID="hidLogin" runat="server" />
    <asp:HiddenField ID="hidDept" runat="server" />
    <asp:HiddenField ID="hidSect" runat="server" />
    <asp:HiddenField ID="hidMc" runat="server" />

    <asp:HiddenField ID="hidBidRevision" runat="server" Value="0" />
    <asp:HiddenField ID="hidScheduleName" runat="server" Value="230/115 kV Ayutthaya 3" />

    <asp:HiddenField ID="hidHost" runat="server" />
    <script>
        //$(document).ready(function (e) {
            
        //});
        function getData(param) {
            var host = "http://" + $("#ContentPlaceHolder1_hidHost").val() + "/tmps/forms/PreviewDocCS?nodeid=";
            var bid_no = $("#ContentPlaceHolder1_hidBidNo").val();
            var bid_revision = $("#ContentPlaceHolder1_hidBidRev").val();
            var schedule_name = $("#ContentPlaceHolder1_hidScheduleName").val();
            var res = param.split(",");
            var dep_name = res[0];
            var equip_code = res[1];
            $.ajax({
                type: "POST",
                url: "DBQC04.aspx/getDesc",
                data: "{'bid_no':'" + bid_no + "','bid_revision':'" + bid_revision + "','schedule_name':'" + schedule_name + "','dep_name':'" + dep_name + "','equip_code':'" + equip_code + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d != '') {
                        $('#divTable').html("<table border=1 style='width:100%'><tr style='background-color: #ccc;'><th>DOC NAME</th><th>DOC DESC</th></th><th>DOC SHEET NO.</th></tr><tr><td><a href='" + host + msg.d.doc_nodeid + "' target='_blank'>" + msg.d.doc_name + "</a></td><td>" + msg.d.doc_desc + "</td><td>" + msg.d.doc_sheetno + "</td></tr></table>"); 
                        $('#myModal_Bid').css('display', 'block');
                    }

                }
            });

            
        }
    </script>
</asp:Content>
