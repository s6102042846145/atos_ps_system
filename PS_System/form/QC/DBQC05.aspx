﻿<%@ Page Language="C#" MasterPageFile="~/MainPagesMaster.Master" AutoEventWireup="true" CodeBehind="DBQC05.aspx.cs" Inherits="PS_System.form.QC.DBQC05" %>

<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Quality Control Department: My Activity"></asp:Label>
    <div id="dvMain" runat="server" style="width: 100%; padding-top: 10px;">
        <table style="width: 100%; margin: 10px">
            <tr>
                <td style="width: 100px;">
                    <asp:Label ID="Label6" runat="server" Text="Contract No: "></asp:Label></td>
                <td style="width: 220px;">
                    <asp:TextBox ID="txtContract" Style="width: 200px;" runat="server" /></td>
                <td style="width: 70px;">
                    <asp:Label ID="Label1" runat="server" Text="หน่วยงาน: "></asp:Label></td>
                <td colspan="5">
                    <asp:DropDownList ID="drpDept" Style="width: 590px; height: 25px;" runat="server" AutoPostBack="True" OnSelectedIndexChanged="drpDept_SelectedIndexChanged"></asp:DropDownList></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Description: "></asp:Label></td>
                <td colspan="6">
                    <asp:TextBox ID="txtDesc" runat="server" Style="width: 200px;" /></td>
               <%-- <td>
                    <asp:Label ID="Label4" runat="server" Text="Period:"></asp:Label></td>
                <td style="width: 200px;">
                    <asp:DropDownList ID="drpType" Style="width: 200px; height: 25px;" runat="server"></asp:DropDownList></td>
                <td style="width: 100px;">
                    <asp:TextBox ID="txtStDate" runat="server" class="picker" autocomplete="off" /></td>
                <td style="width: 30px;" class="text-center">
                    <asp:Label ID="Label5" runat="server" Text="To"></asp:Label></td>
                <td style="width: 100px;">
                    <asp:TextBox ID="txtEndDate" runat="server" class="picker" autocomplete="off" /></td>--%>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Job No.: "></asp:Label></td>
                <td>
                    <asp:TextBox ID="txtJobNo" Style="width: 200px;" runat="server" /></td>
                <td colspan="6"></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Text="Assign To: "></asp:Label></td>
                <td>
                    <asp:DropDownList ID="drpAssign" Style="width: 200px; height: 25px;" runat="server"></asp:DropDownList></td>
                <td colspan="6"></td>
            </tr>
        </table>
        <asp:Button ID="btnFil" runat="server" Text="Filter" class="btn btn-primary" OnClick="btnFil_Click" />
    </div>
    <div style="width: 100%; overflow: scroll; height: 530px;">
        <asp:GridView ID="gvXls" runat="server" Width="100%" OnRowDataBound="gvXls_RowDataBound"
            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." ShowHeader="true">
            <FooterStyle BackColor="#164094" ForeColor="#000066" />
            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <SortedAscendingCellStyle BackColor="#F1F1F1" />
            <SortedAscendingHeaderStyle BackColor="#007DBB" />
            <SortedDescendingCellStyle BackColor="#CAC9C9" />
            <SortedDescendingHeaderStyle BackColor="#00547E" />
            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
            <Columns>
                <asp:TemplateField HeaderText="Contract No." HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbConNo" runat="server" Text='<%# Bind("contract_no") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>

                <asp:TemplateField HeaderText="Job List" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbJobNo" runat="server" Text='<%# Bind("job_no") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <%--<asp:TemplateField HeaderText="Job No.">
                    <ItemTemplate>
                        <asp:Label ID="lbJobNo" runat="server" Text=''></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Substation">
                    <ItemTemplate>
                        <asp:Label ID="lbSubStation" runat="server" Text=''></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall2" />
                </asp:TemplateField>--%>

                <asp:TemplateField HeaderText="Assign To" HeaderStyle-CssClass="text-center">
                    <EditItemTemplate>
                        <asp:TextBox ID="txtAssign" runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:DropDownList ID="drpAssing" runat="server" Style="display: none">
                        </asp:DropDownList>
                        <%--<select id="drpAssing" name="drpAssing">
                            <option value="">ผู้ปฏิบัติงาน</option>
                          </select>--%>
                        <asp:Label ID="lbAssing" runat="server"></asp:Label>
                        <%--<asp:TextBox ID="txtAssign" runat="server"></asp:TextBox>--%>
                        <asp:HiddenField ID="hdfRowID" runat="server" Value='<%# Bind("row_id") %>' />
                        <asp:HiddenField ID="hdfBidNo" runat="server" Value='<%# Bind("bid_no") %>' />
                        <asp:HiddenField ID="hdfBidRe" runat="server" Value='<%# Bind("bid_revision") %>' />
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="LOA Date" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbLOADate" runat="server" Text='<%# Bind("loa_date") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="LOA Confirm Date" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbLOAConfDate" runat="server" Text='<%# Bind("loa_confirm_date") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Shipment Date" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbShipmentDate" runat="server" Text='<%# Bind("shipment_date") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Plan Progress" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Actual Progress" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Remark" HeaderStyle-CssClass="text-center">
                    <ItemTemplate>
                        <asp:Label ID="lbRemark" runat="server" Text='<%# Bind("remark") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <center>
                            <div id="btnedit" style="cursor:pointer; text-align:center; flex-wrap:wrap" onclick="clicknow('<%#Eval("row_id")%>');">
                                <img src="../../images/edit.png" Height="20" Width="20" />
                            </div>
                        </center>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4" />
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <div id="modaldetail" class="modal fade" aria-hidden="true" aria-labelledby="modaldetailLabel" role="dialog" tabindex="-1">
        <div class="modal-dialog" style="width: 750px;">
            <div class="modal-content">
                <div class="modal-header" style="padding: 10px 20px 7px;">
                    <button class="close" data-dismiss="modal" type="button">
                        <span aria-hidden="true">×    <span class="sr-only">Close</span>
                    </button>
                    <h4 id="modaldetailLabel" class="modal-title">บันทึกข้อมูล</h4>
                </div>
                <div class="modal-body">
                    <table>
                        <tr>
                            <td style="width: 110px;">
                                <label>Contract No : </label>
                            </td>
                            <td>
                                <span id="txtContractNo"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <label>Description : </label>
                            </td>
                             <td>
                                 <span id="txtDescription"></span>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                 <label>Shipment Date</label>
                            </td>
                             <td>
                                 <input id="txtShipment_date" type="text" class="picker" autocomplete="off" />
                            </td>
                        </tr>
                        <tr>
                            <td class="media-left">
                                 <label class="mt-5">Remark </label>
                            </td>
                             <td>
                                 <textarea id="txtRemark" rows="4" cols="70" class="mt-5"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer" style="padding: 7px 20px">
                    <h3 id="errmodaldetail" class="sectionError"></h3>
                    <div class="sectionButton">
                        <input type="hidden" id="hdfID" />
                        <button id="btadd" type="button" value="add" class="btn btn-primary" onclick="saveda();">บันทึก</button>
                        <asp:Button ID="btcledit" runat="server" Text="ปิด" class="btn btn-default" data-dismiss="modal" Width="80px" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <asp:HiddenField ID="hidBidNo" runat="server" />
    <asp:HiddenField ID="hidBidRev" runat="server" />
    <asp:HiddenField ID="hidJobNo" runat="server" />
    <asp:HiddenField ID="hidJobRev" runat="server" />
    <asp:HiddenField ID="hidLogin" runat="server" />
    <asp:HiddenField ID="hidDept" runat="server" />
    <asp:HiddenField ID="hidSect" runat="server" />
    <asp:HiddenField ID="hidMc" runat="server" />

    <script>
        
        $(function () {
            $(".picker").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'dd/mm/yy',
            });

            $('#modaldetail').on('hidden.bs.modal', function (e) {
                $('#txtShipment_date').val("");
                $('#txtRemark').val("");

            });
        });
        function clicknow(id) {
            //alert(id);
            //$("#modaldetail").modal('show');
            $.ajax({
                type: "POST",
                url: "DBQC05.aspx/GetData",
                data: "{'id':" + id + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                   // console.log(msg); exit;
                    if (msg.d != '') {
                        $("#hdfID").val(msg.d.row_id);
                        $('#txtContractNo').html(msg.d.contract_no);
                        $('#txtDescription').html(msg.d.schedule_name);
                        $('#txtShipment_date').val(msg.d.shipment_date);
                        $('#txtRemark').val(msg.d.remark);
                        $("#modaldetail").modal('show');

                    }

                }
            });
        }
        function saveda() {
            var id = $('#hdfID').val();
            var shipment_date = $('#txtShipment_date').val();
            var remark = $('#txtRemark').val();

            $.ajax({
                type: "POST",
                url: "DBQC05.aspx/savedata",
                data: "{'id':" + id + ", 'shipment_date':'" + shipment_date + "', 'remark':'" + remark + "' }",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.err == '0') {
                        alert('บันทึกข้อมูลเรียบร้อยแล้ว');
                        $("#modaldetail").modal('hide');
                        $("#ContentPlaceHolder1_btnFil").click();
                    } else {
                        alert(msg.d.errmsg);
                    }
                }
            });

        }
    </script>
</asp:Content>
