﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Configuration;
using PS_Library;
using System.Security.Cryptography;
using System.IO;
using System.Text;
using System.Drawing;


namespace PS_System.form.Dashboard
{
    public partial class JobDashboard : System.Web.UI.Page
    {
        clsJobDashboard objJobDb = new clsJobDashboard();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                {
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                }
                else
                {
                    hidLogin.Value = "z543705";//"z575798"; //"z596353";
                }
                if (Request.QueryString["jobno"] != null)
                {
                    hidJobNo.Value = Request.QueryString["jobno"];
                }
                else
                {
                    hidJobNo.Value = "TS12-11-S06";//"TIPE-01-S03"; //"GBA3-01-S03";//"ECM1-01-A01";//"TS12-04-S02";//
                }
                if (Request.QueryString["taskid"] != null)
                {
                    hidTaskID.Value = Request.QueryString["taskid"];
                }
                else
                {
                    hidTaskID.Value = "0";

                }
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMcShort.Value = emp.MC_SHORT;
                hidJobRev.Value = "11";
                SelectData();
                bindData();
                bindEquip();
                bind_rounting();
                bind_DDL();
                DataTable dtPro = objJobDb.Process(txtJobNo.Text,txtJobRev.Text);
                if (dtPro.Rows.Count > 0)
                { 
                    double val = Convert.ToDouble(dtPro.Rows[0]["weight_percent"]);
                    string process  = String.Format("{0:0}%", val);
                    lblpercent.Text = process;
                    ProcessBar.Attributes.Add("style", "width:"+ process);
                }
            }
            else
            {
                dvViewActivity.Style["display"] = "none";
            }
        }
        protected void bind_rounting()
        {
            DataTable dt = objJobDb.getHead(hidDept.Value, hidSect.Value);
            if (dt.Rows.Count > 0)
            {
                ddl_headDept.Items.Clear();
                ddl_headDept.DataSource = dt;//หัวหน้าแผนก
                ddl_headDept.DataTextField = "orgsname5";
                ddl_headDept.DataValueField = "orgsname5";
                ddl_headDept.DataBind();

                ddl_headSect.Items.Clear();
                ddl_headSect.DataSource = dt;//หัวหน้ากอง
                ddl_headSect.DataTextField = "orgsname5";
                ddl_headSect.DataValueField = "orgsname5";
                ddl_headSect.DataBind();

                ddl_assiDept.Items.Clear();
                ddl_assiDept.DataSource = dt;//ช.ฝ่าย
                ddl_assiDept.DataTextField = "orgsname5";
                ddl_assiDept.DataValueField = "orgsname5";
                ddl_assiDept.DataBind();

                ddl_dept.Items.Clear();
                ddl_dept.DataSource = dt;//ฝ่าย
                ddl_dept.DataTextField = "orgsname5";
                ddl_dept.DataValueField = "orgsname5";
                ddl_dept.DataBind();

                ddl_headProject.Items.Clear();
                ddl_headProject.DataSource = dt;//หัวหน้าโครงการ
                ddl_headProject.DataTextField = "orgsname5";
                ddl_headProject.DataValueField = "orgsname5";
                ddl_headProject.DataBind();

            }
            ddl_headDept.Items.Insert(0, new ListItem("<-- Not assign -->", ""));
            ddl_headSect.Items.Insert(0, new ListItem("<-- Not assign -->", ""));
            ddl_headProject.Items.Insert(0, new ListItem("<-- Not assign -->", ""));
            ddl_assiDept.Items.Insert(0, new ListItem("<-- Not assign -->", ""));
            ddl_dept.Items.Insert(0, new ListItem("<-- Not assign -->", ""));
        }
        private void bind_DDL()
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);//this.hidLogin.Value

            string[] staffList = empInfo.getEmpIDAndNameInDepartment(emp.ORGSNAME4);

            DDL_DessignReq.Items.Clear();
            DDL_EqsignReq.Items.Clear();
            DDL_DocsignReq.Items.Clear();

            DDL_DessignReq.Items.Insert(0, new ListItem("<-- Select -->", ""));
            DDL_EqsignReq.Items.Insert(0, new ListItem("<-- Select -->", ""));
            DDL_DocsignReq.Items.Insert(0, new ListItem("<-- Select -->", ""));

            foreach (string staffInfo in staffList)
            {
                string[] staff = staffInfo.Split(':');

                ListItem item1 = new ListItem();
                ListItem item2 = new ListItem();
                ListItem item3 = new ListItem();

                item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                item1.Value = staff[0].ToString();
                item2.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                item2.Value = staff[0].ToString();
                item3.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                item3.Value = staff[0].ToString();
                DDL_DessignReq.Items.Add(item1);
                DDL_EqsignReq.Items.Add(item2);
                DDL_DocsignReq.Items.Add(item3);
            }

            DataTable dtreq = objJobDb.SelectActivityReq(hidDept.Value);
            if (dtreq.Rows.Count > 0)
            {
                DDL_Template.Items.Clear();
                DDL_Template.DataSource = dtreq;//หัวหน้าแผนก
                DDL_Template.DataTextField = "temp_name";
                DDL_Template.DataValueField = "temp_name";
                DDL_Template.DataBind();

            }
            //DDL_Template.Items.Insert(0, new ListItem("<-- Select Template -->", ""));
        }
        private void bindEquip()
        {
            DataTable dtGruop = objJobDb.GetHeadActiv(hidJobNo.Value, txtJobRev.Text);
            if (dtGruop.Rows.Count > 0)
            {
                btnAssignEq.Visible = true;
                gvEquipment.DataSource = dtGruop;
                gvEquipment.DataBind();
            }
            else
            {
                btnAssignEq.Visible = false;
                gvEquipment.DataSource = null;
                gvEquipment.DataBind();
            }
        }
        private void bindData()
        {
            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value);
            if (dt.Rows.Count > 0)
            {
                gvBidInfo.DataSource = dt;
                gvBidInfo.DataBind();

                //Adds TFOOT section. 
                //gvBidInfo.FooterRow.TableSection = TableRowSection.TableFooter;
            }
            else
            {
                gvBidInfo.DataSource = null;
                gvBidInfo.DataBind();
            }

            DataTable dtGruop2 = objJobDb.GetHeadDoc(hidJobNo.Value);


            if (dtGruop2.Rows.Count > 0)
            {
                gvDoclist.DataSource = dtGruop2;
                gvDoclist.DataBind();
            }
            else
            {
                gvDoclist.DataSource = null;
                gvDoclist.DataBind();
            }
        }

        private void SelectData()
        {
            #region
            //string sql = @"select distinct job.job_no, job.revision, job.sub_line_name, dbo.udf_StripHTML(job.job_desc) as job_desc, dbo.udf_StripHTML(job.remark) as remark, waa.reference
            //from job_no as job
            //  left outer join wf_waa_request_details as waa_detail
            //   inner join wf_waa_request waa
            //   on waa.process_id = waa_detail.process_id and waa.form_status = 'DIRECTOR APPROVED'
            //  on waa_detail.job_no = job.job_no and waa_detail.job_revision = job.revision
            //where job.job_no = '" + reqName + @"' and job.revision = (select max(revision) from job_no where job_no = '" + reqName + @"')";

            //DataTable dt = zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);

            //if (dt.Rows.Count > 0)
            //{
            //    DataRow dr = dt.Rows[0];
            //    txtJobNo.Text = dr["job_no"].ToString();
            //    txtJobRev.Text = dr["revision"].ToString();
            //    txtSubLine.Text = dr["sub_line_name"].ToString();
            //    txtJobDesc.Text = dr["job_desc"].ToString();
            //    txtRemark.Text = dr["remark"].ToString();
            //    txtJobContent.Text = dr["reference"].ToString();
            //}
            #endregion
            var jinfo = objJobDb.getJobInfo(hidJobNo.Value);
            DateTime dtSta = new DateTime();
            DateTime dtEnd = new DateTime();
           
            if (jinfo.p6_plan_start != "" && jinfo.p6_plan_start != null)
            {
                dtSta = Convert.ToDateTime(jinfo.p6_plan_start);
                txtStartDate.Text = dtSta.ToString("dd/MM/yyyy");
            }
            if (jinfo.p6_plan_end != "" && jinfo.p6_plan_end != null)
            {
                dtEnd = Convert.ToDateTime(jinfo.p6_plan_end);
                txtEndDate.Text = dtEnd.ToString("dd/MM/yyyy");
            }

            txtJobNo.Text = jinfo.job_no;
            txtJobRev.Text = jinfo.revision;
            txtSubLine.Text = jinfo.sub_line_name;
            txtJobDesc.Text = jinfo.job_desc;
            txtRemark.Text = jinfo.remark;
            if (jinfo.completion_date != "" && jinfo.completion_date != null) { lblCom.Text = jinfo.completion_date; dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;"); }
            else dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");//dvlblCom.Style["display"] = "none";

            DataTable dt = objJobDb.getAttachWA(hidJobNo.Value);
            if (dt.Rows.Count > 0)
            {
                gvAttach.DataSource = dt;
                gvAttach.DataBind();
            }
            else
            {
                gvAttach.DataSource = null;
                gvAttach.DataBind();
            }
        }

        protected void gvEquipment_RowDataBound(object sender, GridViewRowEventArgs e)
        {

            if (e.Row.RowType == DataControlRowType.Header)
            {

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbGruop = (Label)e.Row.FindControl("lbGruop");
                Label lbhidID = (Label)e.Row.FindControl("lbhidID");
                lbGruop.Text = drv["depart_position_name"].ToString();
                lbhidID.Text = drv["positionid"].ToString();


                GridView grdViewDetail = (GridView)e.Row.FindControl("grdViewDetail");
                DataTable dt0 = objJobDb.GetJobActiv(hidJobNo.Value, txtJobRev.Text);
                DataView dvActi = new DataView(dt0);

                dvActi.RowFilter = "depart_position_name ='" + lbGruop.Text + "'";
                if (dvActi.Count > 0)
                {
                    grdViewDetail.DataSource = dvActi;
                    grdViewDetail.DataBind();
                }
                //ใช้งานได้แค่สิทธิ์ตัวเอง
            }
        }

        protected void gvBidInfo_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //DataRowView drv = (DataRowView)e.Row.DataItem;
                //Label lbJob_no = (Label)e.Row.FindControl("lbJob_no");
                //int ll = Convert.ToInt16(e.Row.RowIndex) + 1;
                //lbJob_no.Text = ll.ToString();
                //if (ViewState["Jobno"].ToString().IndexOf(drv["job_no"].ToString()) > -1)
                //{
                //    ViewState["Jobno"] += "," + drv["job_no"].ToString();

                //    lbJob_no.Text = "";
                //}
                //else
                //{
                //    ViewState["Jobno"] += "," + drv["job_no"].ToString();
                //    lbJob_no.Text = drv["job_no"].ToString();
                //}

            }
        }

        protected void gvDoclist_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                Label lbDocGroup = (Label)e.Row.FindControl("lbDocGroup");
                lbDocGroup.Text = drv["depart_position_name"].ToString();

                GridView grdDocument = (GridView)e.Row.FindControl("grdDocument");
                DataTable dtdoc = objJobDb.GetJobDoc(hidJobNo.Value);
                DataView dvDoc = new DataView(dtdoc);

                dvDoc.RowFilter = "depart_position_name ='" + lbDocGroup.Text + "'";
                if (dvDoc.Count > 0)
                {
                    grdDocument.DataSource = dvDoc;
                    grdDocument.DataBind();
                }
            }
        }
        protected void grdViewDetail_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                GridViewRow row = e.Row;
                GridView grdViewDetail = (GridView)row.NamingContainer;
                //CheckBox cbAll = (CheckBox)e.Row.FindControl("CbAll");

                //cbAll.Attributes.Add("onclick", "javascript:chkAll('" + cbAll.ClientID + "' ,'" + grdViewDetail.ClientID + "');");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbhidDepart = (Label)e.Row.FindControl("lbhidDepart");
                Label lbDivis = (Label)e.Row.FindControl("lbDivis");
                Label lbActiv = (Label)e.Row.FindControl("lbActiv");
                Label lbProc = (Label)e.Row.FindControl("lbProc");
                Label lbStatusDes = (Label)e.Row.FindControl("lbStatusDes");
                Label lbStatusEP = (Label)e.Row.FindControl("lbStatusEP");
                Label lbStatusDoc = (Label)e.Row.FindControl("lbStatusDoc");
                Label lbhidStatus = (Label)e.Row.FindControl("lbhidStatus");

                Label lbAcid = (Label)e.Row.FindControl("lbAcid");
                Label lbnextAc = (Label)e.Row.FindControl("lbnextAc");
                Label lbreqDraw = (Label)e.Row.FindControl("lbreqDraw");
                Label lbAppr = (Label)e.Row.FindControl("lbAppr");
                Label lbreqEq = (Label)e.Row.FindControl("lbreqEq");
                Label lbEmail = (Label)e.Row.FindControl("lbEmail");
                Label lbreqDoc = (Label)e.Row.FindControl("lbreqDoc");

                DropDownList DDLdes = (DropDownList)e.Row.FindControl("DDLdes");
                DropDownList DDLequip = (DropDownList)e.Row.FindControl("DDLequip");
                DropDownList DDLdoc = (DropDownList)e.Row.FindControl("DDLdoc");
                HiddenField HidDDL_des = (HiddenField)e.Row.FindControl("HidDDL_des");
                HiddenField HidDDLeq = (HiddenField)e.Row.FindControl("HidDDLeq");
                HiddenField HidDDL_doc = (HiddenField)e.Row.FindControl("HidDDL_doc");
                CheckBox CbAssign = (CheckBox)e.Row.FindControl("CbAssign");
                //if (drv["is_select"].ToString() == "1") CbAssign.Checked = true;
                //else CbAssign.Checked = false;

                lbDivis.Text = drv["section_position_name"].ToString();
                lbActiv.Text = drv["activity_name"].ToString();
                if (drv["status_drawing"].ToString() != "") lbStatusDes.Text = drv["status_drawing"].ToString(); else lbStatusDes.Text = "-";
                if (drv["status_tech_doc"].ToString() != "") lbStatusDoc.Text = drv["status_tech_doc"].ToString(); else lbStatusDoc.Text = "-";
                if (drv["status_equip"].ToString() != "") lbStatusEP.Text = drv["status_equip"].ToString(); else lbStatusEP.Text = "-";
                lbhidDepart.Text = drv["depart_position_name"].ToString();
                lbAcid.Text = drv["activity_id"].ToString();
                lbnextAc.Text = drv["next_activity_id"].ToString();
                lbreqDraw.Text = drv["req_drawing"].ToString();
                lbAppr.Text = drv["approval_routing"].ToString();
                lbreqEq.Text = drv["req_equip"].ToString();
                lbEmail.Text = drv["email_noti_id"].ToString();
                lbreqDoc.Text = drv["req_tech_doc"].ToString();
                lbhidStatus.Text = drv["status_activity"].ToString();

                Label lblReq_dr = (Label)e.Row.FindControl("lblReq_dr");
                Label lblReq_eq = (Label)e.Row.FindControl("lblReq_eq");
                Label lblReq_doc = (Label)e.Row.FindControl("lblReq_doc");

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);//this.hidLogin.Value

                string[] staffList = empInfo.getEmpIDAndNameInDepartment(drv["depart_position_name"].ToString());//emp.ORGSNAME4

                DDLdes.Items.Clear();
                DDLequip.Items.Clear();
                DDLdoc.Items.Clear();
                DDLdes.Items.Insert(0, new ListItem("<-- Select -->", ""));
                DDLequip.Items.Insert(0, new ListItem("<-- Select -->", ""));
                DDLdoc.Items.Insert(0, new ListItem("<-- Select -->", ""));

                foreach (string staffInfo in staffList)
                {
                    string[] staff = staffInfo.Split(':');

                    ListItem item1 = new ListItem();
                    ListItem item2 = new ListItem();
                    ListItem item3 = new ListItem();

                    item1.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item1.Value = staff[0].ToString();
                    item2.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item2.Value = staff[0].ToString();
                    item3.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                    item3.Value = staff[0].ToString();
                    DDLdes.Items.Add(item1);
                    DDLequip.Items.Add(item2);
                    DDLdoc.Items.Add(item3);
                }

                Label lb_headDept = (Label)e.Row.FindControl("lb_headDept");
                Label lb_headSect = (Label)e.Row.FindControl("lb_headSect");
                Label lb_headProject = (Label)e.Row.FindControl("lb_headProject");
                Label lb_assiDept = (Label)e.Row.FindControl("lb_assiDept");
                Label lb_dept = (Label)e.Row.FindControl("lb_dept");
                ImageButton imgbt = (ImageButton)e.Row.FindControl("imgbt");

                string tooltip = "";
                string[] rounting = GetRoutingApproval(drv["approval_routing"].ToString());
                if (rounting.Length > 0)
                {
                    if (rounting.Length == 1 && rounting[0] != "null")
                    {
                        lb_headDept.Text = rounting[0];
                        tooltip = "Approve1 : " + lb_headDept.Text;

                    }
                    else if (rounting.Length == 2)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text;
                    }
                    else if (rounting.Length == 3)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        lb_headProject.Text = rounting[2];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text + "\n" +
                            "Approve3 : " + lb_headProject.Text;
                    }
                    else if (rounting.Length == 4)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        lb_headProject.Text = rounting[2];
                        lb_assiDept.Text = rounting[3];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text + "\n" +
                            "Approve3 : " + lb_headProject.Text + "\n" +
                            "Approve4 : " + lb_assiDept.Text;
                    }
                    else if (rounting.Length == 5)
                    {
                        lb_headDept.Text = rounting[0];
                        lb_headSect.Text = rounting[1];
                        lb_headProject.Text = rounting[2];
                        lb_assiDept.Text = rounting[3];
                        lb_dept.Text = rounting[4];
                        tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                            "Approve2 : " + lb_headSect.Text + "\n" +
                            "Approve3 : " + lb_headProject.Text + "\n" +
                            "Approve4 : " + lb_assiDept.Text + "\n" +
                            "Approve5 : " + lb_dept.Text;
                    }
                    imgbt.ToolTip = tooltip;
                }
                else
                {
                    clsBidDashbaord objdb = new clsBidDashbaord();
                    DataTable dt = objdb.getHead(hidDept.Value, hidSect.Value);
                    if (dt.Rows.Count > 0)
                    {
                        lb_headDept.Text = dt.Rows[0]["orgsname5"].ToString();
                        lb_headSect.Text = dt.Rows[1]["orgsname5"].ToString();
                        lb_headProject.Text = dt.Rows[2]["orgsname5"].ToString();
                        lb_assiDept.Text = dt.Rows[3]["orgsname5"].ToString();
                        if (dt.Rows.Count == 5)
                        {
                            lb_dept.Text = dt.Rows[4]["orgsname5"].ToString();

                            tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                                "Approve2 : " + lb_headSect.Text + "\n" +
                                "Approve3 : " + lb_headProject.Text + "\n" +
                                "Approve4 : " + lb_assiDept.Text + "\n" +
                                "Approve5 : " + lb_dept.Text;
                            imgbt.ToolTip = tooltip;
                        }
                        else
                        {
                            tooltip = "Approve1 : " + lb_headDept.Text + "\n" +
                                "Approve2 : " + lb_headSect.Text + "\n" +
                                "Approve3 : " + lb_headProject.Text + "\n" +
                                "Approve4 : " + lb_assiDept.Text;
                            imgbt.ToolTip = tooltip;

                        }
                       
                    }

                }
          

                //if (lbreqDraw.Text == "False" && lbreqEq.Text == "False" && lbreqDoc.Text == "False") CbAssign.Enabled = false;
                if (lbreqDraw.Text == "False")
                {
                    DDLdes.SelectedIndex = 0;
                    //DDLdes.Enabled = false; 
                    lbStatusDes.Text = "-";
                }
                else
                    lblReq_dr.Visible = true;

                if (lbreqEq.Text == "False")
                {
                    DDLequip.SelectedIndex = 0;
                    //DDLequip.Enabled = false; 
                    lbStatusEP.Text = "-";
                }
                else
                    lblReq_eq.Visible = true;

                if (lbreqDoc.Text == "False")
                {
                    DDLdoc.SelectedIndex = 0;
                    //DDLdoc.Enabled = false;
                    lbStatusDoc.Text = "-";
                }
                else
                    lblReq_doc.Visible = true;

                if (hidDept.Value != drv["depart_position_name"].ToString())
                {
                    CbAssign.Enabled = false;
                    imgbt.Enabled = false;
                    DDLdes.Enabled = false;
                    DDLequip.Enabled = false;
                    DDLdoc.Enabled = false;
                    lblReq_dr.Visible = false;
                    lblReq_eq.Visible = false;
                    lblReq_doc.Visible = false;
                }

                if (HidDDL_des.Value != null && HidDDL_des.Value != "")
                {
                    DDLdes.SelectedValue = HidDDL_des.Value;
                    //DDLdes.Items.FindByValue(HidDDL_des.Value).Selected = true;
                }
                if (HidDDLeq.Value != null && HidDDLeq.Value != "")
                {
                    DDLequip.SelectedValue = HidDDLeq.Value;
                    //DDLequip.Items.FindByValue(HidDDLeq.Value).Selected = true;
                }
                if (HidDDL_doc.Value != null && HidDDL_doc.Value != "")
                {
                    DDLdoc.SelectedValue = HidDDL_doc.Value;
                    //DDLdoc.Items.FindByValue(HidDDL_doc.Value).Selected = true;
                }

                if (DDLdes.SelectedIndex != 0 && drv["status_drawing"].ToString() != "") lbStatusDes.Text = drv["status_drawing"].ToString();
                else lbStatusDes.Text = "-";
                if (DDLequip.SelectedIndex != 0 && drv["status_equip"].ToString() != "") lbStatusEP.Text = drv["status_equip"].ToString();
                else lbStatusEP.Text = "-";
                if (DDLdoc.SelectedIndex != 0 && drv["status_tech_doc"].ToString() != "") lbStatusDoc.Text = drv["status_tech_doc"].ToString();
                else lbStatusDoc.Text = "-";

                if (drv["status_activity"].ToString() == "WF in progress" || drv["status_activity"].ToString() == "FINISHED")
                {
                    imgbt.Enabled = false;
                    CbAssign.Enabled = false;
                    DDLdes.Enabled = false;
                    DDLequip.Enabled = false;
                    DDLdoc.Enabled = false;
                    lblReq_dr.Visible = false;
                    lblReq_eq.Visible = false;
                    lblReq_doc.Visible = false;
                }
            }
        }
        private string[] GetRoutingApproval(string strRouteApproval)
        {
            return strRouteApproval.Split(new char[] { ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
        }
        protected void grdDocument_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //GridViewRow row = e.Row;
                //GridView grdDocument = (GridView)row.NamingContainer;
                //CheckBox CbApprAll = (CheckBox)e.Row.FindControl("CbApprAll");
                //CbApprAll.Attributes.Add("onclick", "javascript:chkAll('" + CbApprAll.ClientID + "','" + grdDocument.ClientID + "');");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbDocdivis = (Label)e.Row.FindControl("lbDocdivis");
                Label lbDocType = (Label)e.Row.FindControl("lbDocType");
                Label lbDocno = (Label)e.Row.FindControl("lbDocno");
                Label lbDescrip = (Label)e.Row.FindControl("lbDescrip");
                Label lbSheetno = (Label)e.Row.FindControl("lbSheetno");
                Label lbRev = (Label)e.Row.FindControl("lbRev");
                Label lbBidno = (Label)e.Row.FindControl("lbBidno");
                Label lbSchno = (Label)e.Row.FindControl("lbSchno");
                Label lbSchname = (Label)e.Row.FindControl("lbSchname");
                Label lbDocst = (Label)e.Row.FindControl("lbDocst");
                LinkButton LinkButton1 = (LinkButton)e.Row.FindControl("LinkButton1");

                //DataView dvDoc = new DataView(dsList.Tables["tblDoc"]); 
                lbDocdivis.Text = drv["section_position_name"].ToString();
                lbDocType.Text = drv["doctype_name"].ToString();
                lbDocno.Text = drv["doc_no"].ToString();
                lbDescrip.Text = drv["doc_desc"].ToString();
                lbSheetno.Text = drv["doc_sheetno"].ToString();
                lbRev.Text = drv["doc_revision"].ToString();
                lbBidno.Text = drv["bid_no"].ToString();
                lbSchno.Text = drv["schedule_no"].ToString();
                lbSchname.Text = drv["schedule_name"].ToString();
                LinkButton1.Text = " <a href=" + drv["link_doc"].ToString() + " TARGET='openWindow' > ViewDoc </a>";

            }
        }

        protected void btnAssignEq_Click(object sender, EventArgs e)
        {
            if (hidchkSelect.Value != "")
            {
                bool chkFirst = false;
                DataTable chkCount = objJobDb.CheckCountTrans(txtJobNo.Text, txtJobRev.Text);
                if (chkCount.Rows.Count == 0) chkFirst = true;

                string ischk = "";
                string chkaasign = "";
                bool chkUpdate = true;
                int gvcount = 0;
                int count = gvEquipment.Rows.Count - 1;

                foreach (GridViewRow dgi2 in gvEquipment.Rows)
                {
                    GridView grdViewDetail = (GridView)dgi2.FindControl("grdViewDetail");
                    string[] str = grdViewDetail.ClientID.Split('_');

                    if (str.Length > 2)
                    {
                        if (str[2] != "")
                        {
                            gvcount = Convert.ToInt32(str[2]);
                        }
                    }


                    foreach (GridViewRow gvr2 in grdViewDetail.Rows)
                    {
                        CheckBox cb2 = (CheckBox)gvr2.FindControl("CbAssign");
                        DropDownList DDLdes = (DropDownList)gvr2.FindControl("DDLdes");
                        DropDownList DDLequip = (DropDownList)gvr2.FindControl("DDLequip");
                        DropDownList DDLdoc = (DropDownList)gvr2.FindControl("DDLdoc");
                        Label lbreqDraw = (Label)gvr2.FindControl("lbreqDraw");
                        Label lbreqEq = (Label)gvr2.FindControl("lbreqEq");
                        Label lbreqDoc = (Label)gvr2.FindControl("lbreqDoc");

                        if (cb2.Checked)
                        {
                            //if (DDLdes.SelectedValue != "" || DDLequip.SelectedValue != "" || DDLdoc.SelectedValue != "")
                            //{
                            foreach (GridViewRow gvr3 in grdViewDetail.Rows)
                            {
                                DropDownList DDLdes1 = (DropDownList)gvr3.FindControl("DDLdes");
                                DropDownList DDLequip1 = (DropDownList)gvr3.FindControl("DDLequip");
                                DropDownList DDLdoc1 = (DropDownList)gvr3.FindControl("DDLdoc");
                                Label lblReq_dr = (Label)gvr3.FindControl("lblReq_dr");
                                Label lblReq_eq = (Label)gvr3.FindControl("lblReq_eq");
                                Label lblReq_doc = (Label)gvr3.FindControl("lblReq_doc");
                                CheckBox cb3 = (CheckBox)gvr3.FindControl("CbAssign");
                                if (cb3.Checked)
                                {


                                    if (DDLdes1.SelectedValue != "" || DDLequip1.SelectedValue != "" || DDLdoc1.SelectedValue != "")
                                    {
                                        if ((lbreqDraw.Text == "True" && (DDLdes1.SelectedValue == "" || DDLdes1.SelectedValue == null))) lblReq_dr.Visible = true;
                                        //else lblReq_dr.Visible = false;

                                        if ((lbreqEq.Text == "True" && (DDLequip1.SelectedValue == "" || DDLequip1.SelectedValue == null))) lblReq_eq.Visible = true;
                                        //else lblReq_eq.Visible = false;

                                        if ((lbreqDoc.Text == "True" && (DDLdoc1.SelectedValue == "" || DDLdoc1.SelectedValue == null))) lblReq_doc.Visible = true;
                                        //else lblReq_doc.Visible = false;
                                    }
                                    else if (DDLdes1.SelectedValue == "" && DDLequip1.SelectedValue == "" && DDLdoc1.SelectedValue == "")
                                    {
                                        if ((lbreqDraw.Text == "True" && (DDLdes1.SelectedValue == "" || DDLdes1.SelectedValue == null))) lblReq_dr.Visible = true;

                                        if ((lbreqEq.Text == "True" && (DDLequip1.SelectedValue == "" || DDLequip1.SelectedValue == null))) lblReq_eq.Visible = true;

                                        if ((lbreqDoc.Text == "True" && (DDLdoc1.SelectedValue == "" || DDLdoc1.SelectedValue == null))) lblReq_doc.Visible = true;
                                    }
                                    else
                                    {
                                        lblReq_dr.Visible = false;
                                        lblReq_eq.Visible = false;
                                        lblReq_doc.Visible = false;
                                    }
                                }
                            }
                            if ((lbreqDraw.Text == "True" && (DDLdes.SelectedValue == "" || DDLdes.SelectedValue == null)) || (lbreqEq.Text == "True" && (DDLequip.SelectedValue == "" || DDLequip.SelectedValue == null)) || (lbreqDoc.Text == "True" && (DDLdoc.SelectedValue == "" || DDLdoc.SelectedValue == null)))
                            {
                                chkUpdate = false;
                                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please select assign.');", true);
                                //cb2.Checked = false;
                                break;
                            }
                            if ((DDLdes.SelectedValue == "" || DDLdes.SelectedValue == null) && (DDLequip.SelectedValue == "" || DDLequip.SelectedValue == null) && (DDLdoc.SelectedValue == "" || DDLdoc.SelectedValue == null))
                            {
                                chkUpdate = false;
                                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please select assign.');", true);
                                //cb2.Checked = false;
                                break;
                            }
                            //}
                        }

                    }

                }

                if (count == gvcount)
                {
                    if (chkUpdate == true)
                    {
                        List<clsJobWf> lstWF = new List<clsJobWf>();
                        clsWfPsAPV01 objWfPs = new clsWfPsAPV01();
                        //List<clsJobWf> lstWF = new List<clsJobWf>();
                        foreach (GridViewRow dgi in gvEquipment.Rows)
                        {
                            GridView grdViewDetail = (GridView)dgi.FindControl("grdViewDetail");
                            foreach (GridViewRow gvr in grdViewDetail.Rows)
                            {
                                CheckBox cb = (CheckBox)gvr.FindControl("CbAssign");
                                Label lbhidDepart = (Label)gvr.FindControl("lbhidDepart");
                                Label lbDivis = (Label)gvr.FindControl("lbDivis");
                                Label lbActiv = (Label)gvr.FindControl("lbActiv");
                                Label lbProc = (Label)gvr.FindControl("lbProc");
                                Label lbStatusDes = (Label)gvr.FindControl("lbStatusDes");
                                Label lbStatusEP = (Label)gvr.FindControl("lbStatusEP");
                                Label lbStatusDoc = (Label)gvr.FindControl("lbStatusDoc");
                                DropDownList DDLdes = (DropDownList)gvr.FindControl("DDLdes");
                                DropDownList DDLequip = (DropDownList)gvr.FindControl("DDLequip");
                                DropDownList DDLdoc = (DropDownList)gvr.FindControl("DDLdoc");
                                Label lbAcid = (Label)gvr.FindControl("lbAcid");
                                Label lbnextAc = (Label)gvr.FindControl("lbnextAc");
                                Label lbreqDraw = (Label)gvr.FindControl("lbreqDraw");
                                Label lbAppr = (Label)gvr.FindControl("lbAppr");
                                Label lbreqEq = (Label)gvr.FindControl("lbreqEq");
                                Label lbEmail = (Label)gvr.FindControl("lbEmail");
                                Label lbreqDoc = (Label)gvr.FindControl("lbreqDoc");
                                Label lbhidStatus = (Label)gvr.FindControl("lbhidStatus");
                                Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
                                Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
                                Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
                                Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
                                Label lb_dept = (Label)gvr.FindControl("lb_dept");

                                //if (cb.Checked)
                                //{
                                //    ischk = "1";
                                //    chkaasign = "ASSIGNED";
                                //}
                                //else
                                //{
                                //    ischk = "0";
                                //    chkaasign = "";
                                //}
                                if (chkFirst == true)
                                {
                                    if (DDLdes.SelectedValue != "" || DDLequip.SelectedValue != "" || DDLdoc.SelectedValue != "")
                                    {
                                        chkaasign = "ASSIGNED";
                                        lbhidStatus.Text = "ASSIGNED";
                                    }
                                    else chkaasign = "";

                                    string val = "";
                                    if (lb_headDept.Text != "")
                                    {
                                        val += "[" + lb_headDept.Text + "]";

                                    }
                                    if (lb_headSect.Text != "")
                                    {
                                        val += "[" + lb_headSect.Text + "]";
                                    }
                                    if (lb_headProject.Text != "")
                                    {
                                        val += "[" + lb_headProject.Text + "]";
                                    }
                                    if (lb_assiDept.Text != "")
                                    {
                                        val += "[" + lb_assiDept.Text + "]";
                                    }
                                    if (lb_dept.Text != "")
                                    {
                                        val += "[" + lb_dept.Text + "]";
                                    }
                                    string chkDept = "";
                                    if (hidDept.Value == lbhidDepart.Text) chkDept = "1";
                                    else chkDept = "0";

                                    if (lbhidStatus.Text == "ASSIGNED" || lbhidStatus.Text == "")
                                    {

                                        objJobDb.AssignJob(txtJobNo.Text, txtJobRev.Text, lbhidDepart.Text, lbDivis.Text, lbAcid.Text, lbreqDraw.Text, DDLdes.SelectedValue, lbreqEq.Text
                                 , DDLequip.SelectedValue, lbreqDoc.Text, DDLdoc.SelectedValue, val, lbnextAc.Text, lbEmail.Text, this.hidLogin.Value, this.hidLogin.Value, chkaasign, ischk, chkDept);

                                    }

                                }
                                else
                                {
                                    if (cb.Checked)
                                    {
                                        if (DDLdes.SelectedValue != "" || DDLequip.SelectedValue != "" || DDLdoc.SelectedValue != "")
                                        {
                                            chkaasign = "ASSIGNED";
                                            lbhidStatus.Text = "ASSIGNED";
                                        }
                                        else chkaasign = "";

                                        string val = "";
                                        if (lb_headDept.Text != "")
                                        {
                                            val += "[" + lb_headDept.Text + "]";

                                        }
                                        if (lb_headSect.Text != "")
                                        {
                                            val += "[" + lb_headSect.Text + "]";
                                        }
                                        if (lb_headProject.Text != "")
                                        {
                                            val += "[" + lb_headProject.Text + "]";
                                        }
                                        if (lb_assiDept.Text != "")
                                        {
                                            val += "[" + lb_assiDept.Text + "]";
                                        }
                                        if (lb_dept.Text != "")
                                        {
                                            val += "[" + lb_dept.Text + "]";
                                        }
                                        string chkDept = "";
                                        if (hidDept.Value == lbhidDepart.Text) chkDept = "1";
                                        else chkDept = "0";

                                        if (lbhidStatus.Text == "ASSIGNED" || lbhidStatus.Text == "")
                                        {

                                            objJobDb.AssignJob(txtJobNo.Text, txtJobRev.Text, lbhidDepart.Text, lbDivis.Text, lbAcid.Text, lbreqDraw.Text, DDLdes.SelectedValue, lbreqEq.Text
                                     , DDLequip.SelectedValue, lbreqDoc.Text, DDLdoc.SelectedValue, val, lbnextAc.Text, lbEmail.Text, this.hidLogin.Value, this.hidLogin.Value, chkaasign, ischk, chkDept);

                                        }

                                    }
                                }
                            }
                        }
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
                        //start wf 28-01
                        //string strError = objWfPs.StartWf(lstWF);
                        //if (strError != "") ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('" + strError + "');", true);
                        //else ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
                        bindEquip();
                    }
                }
                hidchkSelect.Value = "";
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select Assign');", true);
            }
          
            
        }
        protected void imgbt_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton img = sender as ImageButton;
            GridViewRow gvr = img.NamingContainer as GridViewRow;
            Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
            Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
            Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
            Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
            Label lb_dept = (Label)gvr.FindControl("lb_dept");
            string[] gvrow = gvr.ClientID.Split('_');
            if (gvrow.Length == 3)
            {
                int id = Convert.ToInt32(gvrow[2]) + 1;
                lb_rowindex.Text = gvr.RowIndex.ToString();
                hidExpDiv.Value = id.ToString();
                hidFindIndexM.Value = gvrow[2];
                hidFindIndexS.Value = img.ClientID.Substring(34, lb_rowindex.Text.Length);
                myModal.Style["display"] = "block";
                dvRounting.Style["display"] = "block";
                try { if (lb_headDept.Text != "") ddl_headDept.SelectedValue = lb_headDept.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
                try { if (lb_headSect.Text != "") ddl_headSect.SelectedValue = lb_headSect.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
                try { if (lb_headProject.Text != "") ddl_headProject.SelectedValue = lb_headProject.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
                try { if (lb_assiDept.Text != "") ddl_assiDept.SelectedValue = lb_assiDept.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
                try { if (lb_dept.Text != "") ddl_dept.SelectedValue = lb_dept.Text; } catch (Exception ex) { LogHelper.WriteEx(ex); }
            }

        }
        protected void bt_saveRount_Click(object sender, EventArgs e)
        {
            GridView gv = (GridView)gvEquipment.Rows[Convert.ToInt32(hidFindIndexM.Value)].FindControl("grdViewDetail");
            if (gv != null)
            {
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_headDept")).Text = ddl_headDept.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_headSect")).Text = ddl_headSect.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_headProject")).Text = ddl_headProject.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_assiDept")).Text = ddl_assiDept.SelectedValue;
                ((Label)gv.Rows[Convert.ToInt32(hidFindIndexS.Value)].FindControl("lb_dept")).Text = ddl_dept.SelectedValue;
            }
            //string gv = ((Label)gv1.Rows[rowindex].FindControl("gv")).Text;
            ScriptManager.RegisterStartupScript(Page, GetType(), "divexpandcollapse", "<script>JavaScript:divexpandcollapse('div" + hidExpDiv.Value + "')</script>", false);
            myModal.Style["display"] = "none";
            dvRounting.Style["display"] = "none";
            bind_rounting();
        }
        protected void bt_cancRount_Click(object sender, EventArgs e)
        {
            ScriptManager.RegisterStartupScript(Page, GetType(), "divexpandcollapse", "<script>JavaScript:divexpandcollapse('div" + hidExpDiv.Value + "')</script>", false);
            myModal.Style["display"] = "none";
            dvRounting.Style["display"] = "none";
            bind_rounting();
        }

        protected void gvAttach_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton lnkAtt_view = (LinkButton)e.Row.FindControl("lnkAtt_view");
                Label lblAtt_date = (Label)e.Row.FindControl("lblAtt_date");
                lblAtt_date.Text = drv["upload_datetime"].ToString();
                lnkAtt_view.Text = drv["att_url"].ToString();
            }
        }
        protected void rd_approval_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (rd_approval.SelectedItem.Value == "1") {
                td3.Visible = false;
                td3_3.Visible = false;
            }
            else if (rd_approval.SelectedItem.Value == "2")
            {
                td3.Visible = true;
                td3_3.Visible = true;
                td3.Controls.Add(lb_headProject);
                td3_3.Controls.Add(ddl_headProject);
                td4.Controls.Add(lb_assiDept);
                td4_4.Controls.Add(ddl_assiDept);
                td5.Controls.Add(lb_dept); 
                td5_5.Controls.Add(ddl_dept); 

            }
            else if (rd_approval.SelectedItem.Value == "3")
            {
                td3.Visible = true;
                td3_3.Visible = true;
                td3.Controls.Add(lb_assiDept);
                td3_3.Controls.Add(ddl_assiDept);
                td4.Controls.Add(lb_dept);
                td4_4.Controls.Add(ddl_dept);
                td5.Controls.Add(lb_headProject);
                td5_5.Controls.Add(ddl_headProject);
            }
            bind_rounting();
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnSelect_Click(object sender, EventArgs e)
        {   
            bool chkFirst = false;
            DataTable dtassign = objJobDb.AssignActivityReq(hidDept.Value, DDL_Template.SelectedValue);
            DataTable chkCount = objJobDb.CheckCountTrans(txtJobNo.Text, txtJobRev.Text);
            if (chkCount.Rows.Count == 0) chkFirst = true;
            //objJobDb.DeleteActivity(txtJobNo.Text, txtJobRev.Text, hidDept.Value, hidSect.Value);
            if (dtassign.Rows.Count > 0)
            {
                objJobDb.DeleteActivity(txtJobNo.Text, txtJobRev.Text, hidDept.Value,hidSect.Value);
                foreach (DataRow dr in dtassign.Rows)
                {
                    string chkDept = "";
                    string des = "";
                    string equi = "";
                    string doc = "";
                    string status = "";
                    if (dr["depart_position_name"].ToString() == hidDept.Value)
                    {
                        if (dr["req_drawing"].ToString() == "True") des = DDL_DessignReq.SelectedValue;
                        if (dr["req_equip"].ToString() == "True") equi = DDL_EqsignReq.SelectedValue;
                        if (dr["req_tech_doc"].ToString() == "True") doc = DDL_DocsignReq.SelectedValue;
                        chkDept = "1";
                        if (des != "" || equi != "" || doc != "") status = "NEW";
                        else status = "";

                    }
                    objJobDb.AssignReq(txtJobNo.Text, txtJobRev.Text, hidDept.Value, hidSect.Value, dr["activity_id"].ToString(), dr["req_drawing"].ToString(), des, dr["req_equip"].ToString()
, equi, dr["req_tech_doc"].ToString(), doc, dr["next_activity_id"].ToString(), dr["email_noti_id"].ToString(), this.hidLogin.Value, this.hidLogin.Value, status, dr["approval_routing"].ToString(), chkDept);

                    foreach (GridViewRow dgi in gvEquipment.Rows)
                    {

                        GridView grdViewDetail = (GridView)dgi.FindControl("grdViewDetail");
                        foreach (GridViewRow gvr in grdViewDetail.Rows)
                        {
                            CheckBox cb = (CheckBox)gvr.FindControl("CbAssign");
                            Label lbhidStatus = (Label)gvr.FindControl("lbhidStatus");
                            if (chkFirst == true)
                            {
                                Label lbhidDepart = (Label)gvr.FindControl("lbhidDepart");
                                Label lbDivis = (Label)gvr.FindControl("lbDivis");

                                Label lbAcid = (Label)gvr.FindControl("lbAcid");
                                Label lbnextAc = (Label)gvr.FindControl("lbnextAc");
                                Label lbreqDraw = (Label)gvr.FindControl("lbreqDraw");

                                Label lbreqEq = (Label)gvr.FindControl("lbreqEq");
                                Label lbEmail = (Label)gvr.FindControl("lbEmail");
                                Label lbreqDoc = (Label)gvr.FindControl("lbreqDoc");

                                Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
                                Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
                                Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
                                Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
                                Label lb_dept = (Label)gvr.FindControl("lb_dept");

                                des = "";
                                equi = "";
                                doc = "";


                                string val = "";
                                if (lb_headDept.Text != "")
                                {
                                    val += "[" + lb_headDept.Text + "]";

                                }
                                if (lb_headSect.Text != "")
                                {
                                    val += "[" + lb_headSect.Text + "]";
                                }
                                if (lb_headProject.Text != "")
                                {
                                    val += "[" + lb_headProject.Text + "]";
                                }
                                if (lb_assiDept.Text != "")
                                {
                                    val += "[" + lb_assiDept.Text + "]";
                                }
                                if (lb_dept.Text != "")
                                {
                                    val += "[" + lb_dept.Text + "]";
                                }

                                chkDept = "";
                                if (hidDept.Value == lbhidDepart.Text)
                                {
                                    //if (lbreqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
                                    //if (lbreqEq.Text == "True") equi = DDL_EqsignReq.SelectedValue;
                                    //if (lbreqDoc.Text == "True") doc = DDL_DocsignReq.SelectedValue;
                                    //chkDept = "1";
                                }
                                else
                                {

                                    objJobDb.AssignReq(txtJobNo.Text, txtJobRev.Text, lbhidDepart.Text, lbDivis.Text, lbAcid.Text, lbreqDraw.Text, des, lbreqEq.Text
                                 , equi, lbreqDoc.Text, doc, lbnextAc.Text, lbEmail.Text, this.hidLogin.Value, this.hidLogin.Value, "", val, chkDept);
                                }
                            }
                            else
                            {
                                if (lbhidStatus.Text != "WF in progress" && lbhidStatus.Text != "FINISHED" && lbhidStatus.Text != "ASSIGNED")
                                {
                                    Label lbhidDepart = (Label)gvr.FindControl("lbhidDepart");
                                    Label lbDivis = (Label)gvr.FindControl("lbDivis");

                                    Label lbAcid = (Label)gvr.FindControl("lbAcid");
                                    Label lbnextAc = (Label)gvr.FindControl("lbnextAc");
                                    Label lbreqDraw = (Label)gvr.FindControl("lbreqDraw");

                                    Label lbreqEq = (Label)gvr.FindControl("lbreqEq");
                                    Label lbEmail = (Label)gvr.FindControl("lbEmail");
                                    Label lbreqDoc = (Label)gvr.FindControl("lbreqDoc");

                                    Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
                                    Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
                                    Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
                                    Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
                                    Label lb_dept = (Label)gvr.FindControl("lb_dept");

                                    des = "";
                                    equi = "";
                                    doc = "";


                                    string val = "";
                                    if (lb_headDept.Text != "")
                                    {
                                        val += "[" + lb_headDept.Text + "]";

                                    }
                                    if (lb_headSect.Text != "")
                                    {
                                        val += "[" + lb_headSect.Text + "]";
                                    }
                                    if (lb_headProject.Text != "")
                                    {
                                        val += "[" + lb_headProject.Text + "]";
                                    }
                                    if (lb_assiDept.Text != "")
                                    {
                                        val += "[" + lb_assiDept.Text + "]";
                                    }
                                    if (lb_dept.Text != "")
                                    {
                                        val += "[" + lb_dept.Text + "]";
                                    }

                                    chkDept = "";
                                    if (hidDept.Value == lbhidDepart.Text)
                                    {
                                        //if (lbreqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
                                        //if (lbreqEq.Text == "True") equi = DDL_EqsignReq.SelectedValue;
                                        //if (lbreqDoc.Text == "True") doc = DDL_DocsignReq.SelectedValue;
                                        //chkDept = "1";
                                    }
                                    else
                                    {

                                        objJobDb.AssignReq(txtJobNo.Text, txtJobRev.Text, lbhidDepart.Text, lbDivis.Text, lbAcid.Text, lbreqDraw.Text, des, lbreqEq.Text
                                     , equi, lbreqDoc.Text, doc, lbnextAc.Text, lbEmail.Text, this.hidLogin.Value, this.hidLogin.Value, "", val, chkDept);
                                    }

                                }

                            }
                            

                        }
                    }

                }
            }
            else
            {
                foreach (GridViewRow dgi in gvEquipment.Rows)
                {

                    GridView grdViewDetail = (GridView)dgi.FindControl("grdViewDetail");
                    foreach (GridViewRow gvr in grdViewDetail.Rows)
                    {
                        CheckBox cb = (CheckBox)gvr.FindControl("CbAssign");
                        Label lbhidStatus = (Label)gvr.FindControl("lbhidStatus");
                        if (chkFirst == true)
                        {

                            Label lbhidDepart = (Label)gvr.FindControl("lbhidDepart");
                            Label lbDivis = (Label)gvr.FindControl("lbDivis");

                            Label lbAcid = (Label)gvr.FindControl("lbAcid");
                            Label lbnextAc = (Label)gvr.FindControl("lbnextAc");
                            Label lbreqDraw = (Label)gvr.FindControl("lbreqDraw");
                     
                            Label lbreqEq = (Label)gvr.FindControl("lbreqEq");
                            Label lbEmail = (Label)gvr.FindControl("lbEmail");
                            Label lbreqDoc = (Label)gvr.FindControl("lbreqDoc");

                            Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
                            Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
                            Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
                            Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
                            Label lb_dept = (Label)gvr.FindControl("lb_dept");


                            string des = "";
                            string equi = "";
                            string doc = "";


                            string val = "";
                            if (lb_headDept.Text != "")
                            {
                                val += "[" + lb_headDept.Text + "]";

                            }
                            if (lb_headSect.Text != "")
                            {
                                val += "[" + lb_headSect.Text + "]";
                            }
                            if (lb_headProject.Text != "")
                            {
                                val += "[" + lb_headProject.Text + "]";
                            }
                            if (lb_assiDept.Text != "")
                            {
                                val += "[" + lb_assiDept.Text + "]";
                            }
                            if (lb_dept.Text != "")
                            {
                                val += "[" + lb_dept.Text + "]";
                            }

                            string chkDept = "";
                            string status = "";
                            if (hidDept.Value == lbhidDepart.Text)
                            {
                                if (lbreqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
                                if (lbreqEq.Text == "True") equi = DDL_EqsignReq.SelectedValue;
                                if (lbreqDoc.Text == "True") doc = DDL_DocsignReq.SelectedValue;
                                chkDept = "1";
                                if (des != "" || equi != "" || doc != "") status = "NEW";
                                else status = "";
                            }
                            else chkDept = "";

                            objJobDb.AssignReq(txtJobNo.Text, txtJobRev.Text, lbhidDepart.Text, lbDivis.Text, lbAcid.Text, lbreqDraw.Text, des, lbreqEq.Text
                         , equi, lbreqDoc.Text, doc, lbnextAc.Text, lbEmail.Text, this.hidLogin.Value, this.hidLogin.Value, status, val, chkDept);
                        }
                        else
                        {
                            if (lbhidStatus.Text != "WF in progress" && lbhidStatus.Text != "FINISHED" && lbhidStatus.Text != "ASSIGNED") //ASSIGNED
                            {
                                Label lbhidDepart = (Label)gvr.FindControl("lbhidDepart");
                                Label lbDivis = (Label)gvr.FindControl("lbDivis");

                                Label lbAcid = (Label)gvr.FindControl("lbAcid");
                                Label lbnextAc = (Label)gvr.FindControl("lbnextAc");
                                Label lbreqDraw = (Label)gvr.FindControl("lbreqDraw");

                                Label lbreqEq = (Label)gvr.FindControl("lbreqEq");
                                Label lbEmail = (Label)gvr.FindControl("lbEmail");
                                Label lbreqDoc = (Label)gvr.FindControl("lbreqDoc");

                                Label lb_headDept = (Label)gvr.FindControl("lb_headDept");
                                Label lb_headSect = (Label)gvr.FindControl("lb_headSect");
                                Label lb_headProject = (Label)gvr.FindControl("lb_headProject");
                                Label lb_assiDept = (Label)gvr.FindControl("lb_assiDept");
                                Label lb_dept = (Label)gvr.FindControl("lb_dept");

                                DropDownList DDLdes = (DropDownList)gvr.FindControl("DDLdes");
                                DropDownList DDLequip = (DropDownList)gvr.FindControl("DDLequip");
                                DropDownList DDLdoc = (DropDownList)gvr.FindControl("DDLdoc");


                                string des = "";
                                string equi = "";
                                string doc = "";


                                string val = "";
                                if (lb_headDept.Text != "")
                                {
                                    val += "[" + lb_headDept.Text + "]";

                                }
                                if (lb_headSect.Text != "")
                                {
                                    val += "[" + lb_headSect.Text + "]";
                                }
                                if (lb_headProject.Text != "")
                                {
                                    val += "[" + lb_headProject.Text + "]";
                                }
                                if (lb_assiDept.Text != "")
                                {
                                    val += "[" + lb_assiDept.Text + "]";
                                }
                                if (lb_dept.Text != "")
                                {
                                    val += "[" + lb_dept.Text + "]";
                                }

                                string chkDept = "";
                                string status = "";
                                if (hidDept.Value == lbhidDepart.Text)
                                {
                                    //if (lbreqDraw.Text == "True") des = DDL_DessignReq.SelectedValue;
                                    //if (lbreqEq.Text == "True") equi = DDL_EqsignReq.SelectedValue;
                                    //if (lbreqDoc.Text == "True") doc = DDL_DocsignReq.SelectedValue;
                                    if (lbreqDraw.Text == "True")
                                    {
                                        if (DDLdes.SelectedValue != "") des = DDLdes.SelectedValue;
                                        else des = DDL_DessignReq.SelectedValue;
                                    }
                                    if (lbreqEq.Text == "True")
                                    {
                                        if (DDLequip.SelectedValue != "") equi = DDLequip.SelectedValue;
                                        else equi = DDL_EqsignReq.SelectedValue;

                                    }
                                    if (lbreqDoc.Text == "True")
                                    {
                                        if (DDLdoc.SelectedValue != "") doc = DDLdoc.SelectedValue;
                                        else doc = DDL_DocsignReq.SelectedValue;
                                    }
                                    chkDept = "1";
                                    if (des != "" || equi != "" || doc != "") status = "NEW";
                                    else status = "";
                                }
                                else chkDept = "";

                                objJobDb.AssignReq(txtJobNo.Text, txtJobRev.Text, lbhidDepart.Text, lbDivis.Text, lbAcid.Text, lbreqDraw.Text, des, lbreqEq.Text
                             , equi, lbreqDoc.Text, doc, lbnextAc.Text, lbEmail.Text, this.hidLogin.Value, this.hidLogin.Value, status, val, chkDept);

                            }
                        }
                       

                    }
                }
            }

            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
            bindEquip();
        }

        protected void lnkActivity_Click(object sender, EventArgs e)
        {
            dvViewActivity.Style["display"] = "block";
            LinkButton lnk = sender as LinkButton;
            GridViewRow gvr = lnk.NamingContainer as GridViewRow;
            string lb_ActID = ((Label)gvr.FindControl("lbAcid")).Text;
            string lb_activity = ((Label)gvr.FindControl("lbActiv")).Text;
            DataTable dtbefore = objJobDb.ViewBeforeActivityJob(hidJobNo.Value, txtJobRev.Text, lb_ActID);
            DataTable dtnext = objJobDb.ViewNextActivityJob(hidJobNo.Value, txtJobRev.Text, lb_ActID);
            string before = "";
            string next = "";
            DataTable dtlogbefore = new DataTable();
            DataTable dtlognext = new DataTable();
            DataTable dtlognow = new DataTable();
            if (dtbefore.Rows.Count > 0)
            {
                DataTable dtlog = new DataTable();
                dtlog.Columns.Add("apv_order");
                dtlog.Columns.Add("emp_id");
                dtlog.Columns.Add("emp_name");
                dtlog.Columns.Add("emp_position");
                dtlog.Columns.Add("emp_apvdate");
                dtlog.Columns.Add("apv_status");
                dtlog.Columns.Add("ps_subject");
                dtlog.Columns.Add("process_id");
                foreach (DataRow dr in dtbefore.Rows)
                {
                    before += (before == "" ? "" : ",") + "- " + dr["activity_name"].ToString();
                    dtlogbefore = objJobDb.ViewLogActivityJob(hidJobNo.Value, txtJobRev.Text, dr["activity_id"].ToString(), dtlog);
                }
                lblBefore.Text = before.Replace(",", "<br/>");
                dtlogbefore.DefaultView.Sort = "process_id,apv_order asc";
                gvBefore.DataSource = dtlogbefore;
                gvBefore.DataBind();
                lblBefore.Visible = true;
                Image1.Visible = true;
            }
            else
            {
                lblBefore.Visible = false;
                Image1.Visible = false;
                gvBefore.DataSource = null;
                gvBefore.DataBind();
            }
            if (dtnext.Rows.Count > 0)
            {
                DataTable dtlog = new DataTable();
                dtlog.Columns.Add("apv_order");
                dtlog.Columns.Add("emp_id");
                dtlog.Columns.Add("emp_name");
                dtlog.Columns.Add("emp_position");
                dtlog.Columns.Add("emp_apvdate");
                dtlog.Columns.Add("apv_status");
                dtlog.Columns.Add("ps_subject");
                dtlog.Columns.Add("process_id");
                foreach (DataRow dr in dtnext.Rows)
                {
                    next += (next == "" ? "" : ",") + "- " + dr["activity_name"].ToString();
                    dtlognext = objJobDb.ViewLogActivityJob(hidJobNo.Value, txtJobRev.Text, dr["activity_id"].ToString(), dtlog);
                }
                lblNext.Text = next.Replace(",", "<br/>");
                dtlognext.DefaultView.Sort = "process_id,apv_order asc";
                gvNext.DataSource = dtlognext;
                gvNext.DataBind();
                lblNext.Visible = true;
                Image2.Visible = true;
            }
            else
            {
                lblNext.Visible = false;
                Image2.Visible = false;
                gvNext.DataSource = null;
                gvNext.DataBind();
            }
            DataTable dtlogNow = new DataTable();
            dtlogNow.Columns.Add("apv_order");
            dtlogNow.Columns.Add("emp_id");
            dtlogNow.Columns.Add("emp_name");
            dtlogNow.Columns.Add("emp_position");
            dtlogNow.Columns.Add("emp_apvdate");
            dtlogNow.Columns.Add("apv_status");
            dtlogNow.Columns.Add("ps_subject");
            dtlogNow.Columns.Add("process_id");
            lblNow.Text = "- " + lb_activity;
            dtlogNow = objJobDb.ViewLogActivityJob(hidJobNo.Value, txtJobRev.Text, lb_ActID, dtlogNow);
            dtlogNow.DefaultView.Sort = "process_id,apv_order asc";
            gvNow.DataSource = dtlogNow;
            gvNow.DataBind();

        }
        //protected void btnCloseView_Click(object sender, EventArgs e)
        //{
        //    dvViewActivity.Style["display"] = "none";
        //}
    }
}