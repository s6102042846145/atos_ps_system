﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobDashboard.aspx.cs" Inherits="PS_System.form.Dashboard.JobDashboard" MaintainScrollPositionOnPostback="true"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Job Dashboard</title>
    <style type="text/css">
        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }

        /*  td.details-control {
            background: url('~/images/icon_plus.png') no-repeat center center;
            cursor: pointer;
        }
        tr.shown td.details-control {
            background: url('~/images/icon_plus.png') no-repeat center center;
        }*/

        td.details-control {
            background: url('../../images/details_open.png') no-repeat center center;
            background-size: 20px 20px;
            cursor: pointer;
        }

        tr.shown td.details-control {
            background: url('../../images/details_close.png') no-repeat center center;
        }
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
    </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

<%--    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>


    <script>
        //$(document).ready(function () {

        //    //    datatableConfig();


        //});
        $(document).ready(function () {
          

            $('[id*=DDLdes]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=DDLequip]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=DDLdoc]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });


            $('[id*=DDL_DessignReq]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=DDL_EqsignReq]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
            $('[id*=DDL_DocsignReq]').multiselect({
                enableFiltering: true,
                maxHeight: 300
            });
        });


        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });



        function chkAll(Checkbox, gv) {
            var TargetControl = document.getElementById(gv);
            var cb = document.getElementById(Checkbox);
            var TargetChildControl = "CbAssign";
            var Inputs = TargetControl.getElementsByTagName("input");

            for (var n = 0; n < Inputs.length; n++) {
                if (Inputs[n].type == 'checkbox' && Inputs[n].id.indexOf(TargetChildControl, 0) >= 0 && Inputs[n].disabled == false) {
                    Inputs[n].checked = cb.checked;
                }
            }
        }

        var currentlyOpenedDiv = "";
        var currentlyOpenedtd = "";
        function CollapseExpand(object) {

            var div = document.getElementById(object);
            alert(div).id;
            //var tdk = document.getElementById(td);
            if (currentlyOpenedDiv != "" && currentlyOpenedDiv != div) {
                currentlyOpenedDiv.style.display = "none";
                //currentlyOpenedtd.style.display = "none";

            }

            if (div.style.display == "none") {
                div.style.display = "inline";
                //tdk.style.display = "table-cell";
                currentlyOpenedDiv = div;
                //currentlyOpenedtd = tdk;
            }
            else {
                div.style.display = "none";
                //tdk.style.display = "none";
            }
        }

        function closePopup() {
            var dv = document.getElementById("myModal");
            if (dv != null) {
                dv.style.display = "none";
            }
        }

        function openTab() {
            var x = document.getElementById("dvDoc");
            var img = document.getElementById("Image1");

            if (x.style.display === "none") {
                x.style.display = "block";
                img.src = '../../images/sort_asc.png';

            } else {
                x.style.display = "none";
                img.src = '../../images/sort_desc.png';
            }
        }

        function ToggleAll(cbParrent) {
            var cbParrentId = cbParrent[0].id;
            var cbChildPrefix = cbParrentId.replace("_CbAll", "_CbAssign_")
            var gvId = cbChildPrefix.replace("_CbAssign_", "");
            var gv = document.getElementById(gvId);
            var rowsCount = gv.getElementsByTagName("tr").length;
            var isCheck = true;
            if (document.getElementById(cbParrentId).checked != true) {
                isCheck = false;
            }
            for (i = 0; i < rowsCount - 1; i++) {
                var tempChildCbName = cbChildPrefix + i.toString();
                // alert(tempChildCbName);
                // document.getElementById(tempChildCbName).checked = true;
                $("#" + tempChildCbName).attr("checked", true);
            }
            //alert(rowsCount.length);
        }
        function divexpandcollapse(divname) {
            var div = document.getElementById(divname);
            var img = document.getElementById('img' + divname);
            var td = document.getElementById('td' + divname);
            if (div.style.display == "none") {
                div.style.display = "inline";
                td.style.display = "table-row";
                img.src = "../../images/minus.png";
                img.style.width = "20px"
                img.style.height = "20px"
            } else {
                div.style.display = "none";
                td.style.display = "none";
                img.src = "../../images/plus.png";
            }
        }

        function setSelectValue(objDLL, DLL_Name, HID_Name) {
            var hidValue_Name = objDLL.id.replace(DLL_Name, HID_Name);
            var lbassDes_Name = objDLL.id.replace(DLL_Name, "lbassDes");
            alert(objDLL.value);
            document.getElementById(hidValue_Name).value = "";//objDLL.value;
            document.getElementById(lbassDes_Name).innerText = objDLL.value;
            alert(document.getElementById(hidValue_Name).value);
        }

        function disp_confirm(r) {
            //alert(r);
            if (r == true) {
                alert("Saved Successfully.")
            }
            else {
                alert("Please select assign.")
            }
        }
        function openPop() {
            var mainpop = document.getElementById("myModal");
            var dvRounting = document.getElementById("dvRounting");
            if (mainpop != null && dvRounting != null) {
                mainpop.style.display = "block";
                dvRounting.style.display = "block";
            }
        }

        function chkSelect(objCHK) {

            var selectValueDoc = "|" + objCHK + "|";
           
            if (objCHK.checked == true) {
                if (document.getElementById("hidchkSelect").value.includes(selectValueDoc) == false) {
                    document.getElementById("hidchkSelect").value = document.getElementById("hidchkSelect").value + selectValueDoc;
                  
                }
            }
            else {
                document.getElementById("hidchkSelect").value = document.getElementById("hidchkSelect").value.replace(selectValueDoc, "");
               
            }
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <%-- <asp:ScriptManager ID="ScriptManager1" runat="server" EnablePartialRendering="true" />--%>
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label4" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label5" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Job Dashboard"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div align="center">
            <table style="width: 95%;">
                <tr style="padding-left: 5px;">
                    <td colspan="2">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 10%">
                                    <asp:Label ID="Label3" runat="server" Text="Job No.:"></asp:Label>
                                </td>
                                <td style="width: 20%"><%--style="width: 50%"--%>
                                    <asp:TextBox ID="txtJobNo" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                                    &nbsp;&nbsp;&nbsp;<asp:Label ID="Label6" runat="server" Text="Rev.:"></asp:Label>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtJobRev" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="100px"></asp:TextBox>

                                </td>
                                <td style="width: 9%">
                                    <asp:Label ID="Label7" runat="server" Text="Substation / Line:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSubLine" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="95%"></asp:TextBox>

                                </td>
                                <td rowspan="2" style="width: 40%">
                                    <table style="width: 95%;">
                                        <tr style="height: 10%;">
                                            <td style="width: 15%; text-align: left; vertical-align: middle;">
                                                <asp:Label ID="Label1" runat="server" Text="Job Progress:"></asp:Label>
                                            </td>
                                            <td style="width: 80%; text-align: left; vertical-align: middle;">
                                                <div class="progress">
                                                    <div id="ProcessBar" runat="server" class="progress-bar progress-bar-striped" role="progressbar" style="width: 20%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                         <asp:Label ID="lblpercent" runat="server" Text="20%" ></asp:Label></div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr style="height: 90%;">
                                            <td colspan="2">
                                                <asp:Label ID="lblProgress_Department" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <%-- <tr>
                                <td>
                                    <asp:Label ID="Label7" runat="server" Text="Substation / Line:"></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="txtSubLine" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="80%"></asp:TextBox>
                                </td>
                            </tr>--%>
                            <tr>
                                <td>
                                    <asp:Label ID="Label2" runat="server" Text="Job Description:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtJobDesc" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                                </td>
                                <td rowspan="9" style="vertical-align:top;">
                                    <div style="overflow-y:auto;height:250px;">
                                        <asp:GridView ID="gvAttach" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAttach_RowDataBound">
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="Attachment Name">
                                    <ItemTemplate>
                                      <asp:LinkButton ID="lnkAtt_view" runat="server"></asp:LinkButton>
                                    </ItemTemplate>
                                    <ItemStyle Width="300px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Attachment Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblAtt_date" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="40px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                            </Columns>

                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label8" runat="server" Text="Plan Start Date:"></asp:Label>
                                </td>
                                <td style="text-align:left;">
                                    <asp:TextBox ID="txtStartDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="90%"></asp:TextBox>
                                </td>
                                <td>
                                    <asp:Label ID="Label11" runat="server" Text="Plan End Date:"></asp:Label>
                                </td>
                                <td style="text-align:left;">
                                   <asp:TextBox ID="txtEndDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="96%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                              <tr>
                                <td>
                                    <asp:Label ID="Label10" runat="server" Text="Completion Date:"></asp:Label>
                                </td>
                                <td colspan="3" style="text-align:left;">
                                    <div id="dvlblCom" runat="server" style="border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;">
                                    <asp:Label ID="lblCom" runat="server" ></asp:Label>
                                   </div>
                                 <%--   <asp:TextBox ID="txtCompleteDate" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>--%>
                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label9" runat="server" Text="Remark Job:"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" BorderStyle="Solid" BorderColor="#D8D8D8" Width="98%" Rows="3" TextMode="MultiLine"></asp:TextBox>

                                </td>
                            </tr>
                            <tr style="height: 10px">
                                <td></td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <div class="float-right" align="right" style="padding-right: 1.5%;">
                                        <button onclick="document.getElementById('myModal').style.display='block';return false;" class="btn btn-primary">Compare WA</button>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; color: #006699;"><b>Schedule</b></td>

                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px;">
                        <asp:GridView ID="gvBidInfo" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvBidInfo_RowDataBound" EmptyDataText="No data found.">
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <%--<emptydatatemplate>No data found</emptydatatemplate>   Text='<%# Bind("runno") %>'--%>
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderText="No.">
                                    <ItemTemplate>
                                         <%# Container.DataItemIndex + 1 %>
                                        <%--Text='<%# Bind("job_no") %>'--%>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type of Equipment">
                                    <ItemTemplate>
                                        <asp:Label ID="lbEq_desc" runat="server" Text='<%# Bind("eq_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Bid/Enq. No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lbBid_no" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Revision">
                                    <ItemTemplate>
                                        <asp:Label ID="lbRevi" runat="server" Text='<%# Bind("bid_revision") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule No.">
                                    <ItemTemplate>
                                        <asp:Label ID="lbsch_no" runat="server" Text='<%# Bind("schedule_no") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Schedule Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbsch_name" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>

                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                   <asp:TemplateField HeaderText="Procurement Schedule">
                                    <ItemTemplate>
                                        <asp:Label ID="lbProcure" runat="server" Text='<%# Bind("ps_url") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>
                                <%-- <asp:BoundField HeaderText="Bid/Enq. No." DataField="Bid_no" ps_url />
                                 <asp:BoundField HeaderText="Schedule No." DataField="Sch_no" >
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                 </asp:BoundField>
                                 <asp:BoundField HeaderText="Schedule Name" DataField="Sch_name" />
                                 <asp:BoundField HeaderText="Bid Progress" DataField="Bid_pro" />--%>
                            </Columns>

                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left; color: #006699;"><b>Activity, Assignment, and Status</b></td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label12" runat="server" Text="Template Name:"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_Template" runat="server"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:Label ID="Label13" runat="server" Text="Design Assigned to:"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_DessignReq" runat="server"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:Label ID="Label14" runat="server" Text="Equipment/Price Schdule Assigned to:"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_EqsignReq" runat="server"></asp:DropDownList>
                        &nbsp;&nbsp;<asp:Label ID="Label15" runat="server" Text="Document Assigned to:"></asp:Label>
                        &nbsp;<asp:DropDownList ID="DDL_DocsignReq" runat="server"></asp:DropDownList>
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSelect" runat="server" Text="Select" class="btn btn-primary" OnClick="btnSelect_Click"/>
                    </td>
                </tr>
                <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">

                        <asp:GridView ID="gvEquipment" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvEquipment_RowDataBound" EmptyDataText="No data found.">
                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>

                                <asp:TemplateField ItemStyle-Width="10px">
                                    <ItemTemplate>
                                        <a href="JavaScript:divexpandcollapse('div<%# Eval("positionid") %>');">
                                            <img id='imgdiv<%# Eval("positionid") %>' border="0" src="../../images/plus.png" width="20px" height="20px" /></a>

                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="กอง">
                                    <ItemTemplate>
                                        <asp:Label ID="lbGruop" runat="server" Text=""></asp:Label>
                                        <asp:Label ID="lbhidID" runat="server" Visible="false"></asp:Label>
                                        <tr id="tddiv<%# Eval("positionid") %>" style="display: none;">
                                            <td colspan="3">

                                                <div id="div<%# Eval("positionid") %>" style="display: none;">


                                                    <asp:GridView ID="grdViewDetail" runat="server" AutoGenerateColumns="false" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Names="tahoma" Font-Size="9pt"
                                                        OnRowDataBound="grdViewDetail_RowDataBound">
                                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />

                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Select">
                        <%--                                        <HeaderTemplate>
                                                                    <asp:CheckBox ID="CbAll" runat="server" />
                                                    
                                                                </HeaderTemplate>--%>
                                                                <ItemTemplate>
                                                                    <asp:CheckBox ID="CbAssign" runat="server" onclick="chkSelect(this);"/>
                                                                </ItemTemplate>
                                                                <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="แผนก">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbDivis" runat="server" Text=""></asp:Label>
                                                                    <asp:Label ID="lbhidDepart" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbAcid" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbnextAc" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbreqDraw" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbAppr" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbreqEq" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbEmail" runat="server" Visible="false" Text=""></asp:Label>
                                                                    <asp:Label ID="lbreqDoc" runat="server" Visible="false" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Activity">
                                                                <ItemTemplate>
                                                                     <asp:LinkButton ID="lnkActivity" runat="server" Text='<%# Bind("activity_name") %>' OnClick="lnkActivity_Click" ></asp:LinkButton>
                                                                    <asp:Label ID="lbActiv" runat="server" Text="" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Process">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbProc" runat="server" Text=""></asp:Label>
                                                                     <asp:Label ID="lbhidStatus" runat="server" Visible="false"></asp:Label>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Design Assigned to:">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="DDLdes" runat="server"></asp:DropDownList>
                                                                       <asp:Label ID="lblReq_dr" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                                                                    <%--  onchange="setSelectValue(this,'DDLdes','HidDDLdes');"  --%>
                                                                    <asp:HiddenField ID="HidDDL_des" runat="server" Value='<%# Bind("assignee_drawing") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbStatusDes" runat="server" >-</asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Equipment/Price Schdule Assigned to:">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="DDLequip" runat="server"></asp:DropDownList>
                                                                     <asp:Label ID="lblReq_eq" runat="server" Text="*" ForeColor="Red"  visible="false"></asp:Label>
                                                                    <asp:HiddenField ID="HidDDLeq" runat="server" Value='<%# Bind("assignee_equip") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbStatusEP" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Document Assigned to:">
                                                                <ItemTemplate>
                                                                    <asp:DropDownList ID="DDLdoc" runat="server"></asp:DropDownList>
                                                                       <asp:Label ID="lblReq_doc" runat="server" Text="*" ForeColor="Red" visible="false"></asp:Label>
                                                                    <asp:HiddenField ID="HidDDL_doc" runat="server" Value='<%# Bind("assignee_tech_doc") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Top" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lbStatusDoc" runat="server" Text=""></asp:Label>
                                                                </ItemTemplate>
                                                                 <ItemStyle HorizontalAlign="Center" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Rounting">
                                                                            <ItemTemplate>
                                                                               <asp:Label ID="lb_headDept" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_headSect" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_headProject" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_assiDept" runat="server" Visible="False"></asp:Label>
                                                                                 <asp:Label ID="lb_dept" runat="server"  Visible="False"></asp:Label>
                                                                                <asp:ImageButton ID="imgbt" runat="server" src="../../images/plus.png" width="20px" height="20px" OnClick="imgbt_Click"/>
                                                                            </ItemTemplate>
                                                                             <ItemStyle HorizontalAlign="Center" />
                                                                        </asp:TemplateField>
                                                            <%--        <asp:TemplateField HeaderText="" ItemStyle-Width="50px">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnAssignEq" runat="server" class="w3-button w3-blue" Text="Assign" OnClick="btnAssignEq_Click"/>
                                                      
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>--%>
                                                        </Columns>
                                                    </asp:GridView>

                                                </div>

                                            </td>
                                        </tr>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="left" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                    </td>
                </tr>
                <tr>
                    <td style="text-align: right">
                        <asp:Button ID="btnAssignEq" runat="server" class="btn btn-primary" Text="Assign" OnClick="btnAssignEq_Click" />
                    </td>
                </tr>
                <%--Text='<%# Bind("job_no") %>'--%>
                <%--<asp:TemplateField HeaderText="Job Description">
                                    <ItemTemplate>
                                        <asp:Label ID="Label22" runat="server" Text='<%# Bind("job_Desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>--%>
                <tr>
                    <td style="text-align: left; color: #006699;"><b>Document List:</b></td>
                </tr>
                <%--<asp:TemplateField HeaderText="Revision">
                                    <ItemTemplate>
                                        <asp:Label ID="Label32" runat="server" Text='<%# Bind("revision") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:TemplateField>--%>                <%--<asp:TemplateField HeaderText="Remark">
                                    <ItemTemplate>
                                        <asp:Label ID="Label24" runat="server" Text='<%# Bind("remark") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                                </asp:TemplateField>--%>
                <!--<input type="file" style="visibility: visible;"/>-->
                <!--<input id="Text1" type="text" />-->
                <!--<asp:FileUpload id="FileUpload1"                 
                            runat="server">
                         </asp:FileUpload>-->
                <%--Text='<%# Bind("schedule_no") %>'--%>                <%-- Bid Progress--%>
                <!--<asp:ImageButton ID="ImageButton2" runat="server" ImageAlign="Right" ImageUrl="C:\EGAT\PS_System\Image\upload.png" Height="25px" Width="25px"/>-->
                <%-- <asp:BoundField HeaderText="Bid/Enq. No." DataField="Bid_no" />
                                 <asp:BoundField HeaderText="Schedule No." DataField="Sch_no" >
                                       <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                 </asp:BoundField>
                                 <asp:BoundField HeaderText="Schedule Name" DataField="Sch_name" />
                                 <asp:BoundField HeaderText="Bid Progress" DataField="Bid_pro" />--%><%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                <tr>
                    <td colspan="2">
                        <%-- onchange="ToggleAll(this.childNodes)"  OnCheckedChanged="CbAll_CheckedChanged" AutoPostBack="true" --%>
                        <%--  onchange="setSelectValue(this,'DDLdes','HidDDLdes');"  --%>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">
                        <div id="dvDoc" runat="server" style="display: block;" align="center">

                            <%--        <asp:TemplateField HeaderText="" ItemStyle-Width="50px">
                                                        <ItemTemplate>
                                                            <asp:Button ID="btnAssignEq" runat="server" class="w3-button w3-blue" Text="Assign" OnClick="btnAssignEq_Click"/>
                                                      
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>--%>


                            <asp:GridView ID="gvDoclist" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvDoclist_RowDataBound" EmptyDataText="No data found.">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10px">
                                        <ItemTemplate>
                                            <a href="JavaScript:divexpandcollapse('div<%# Eval("posDocid") %>k');">
                                                <img id='imgdiv<%# Eval("posDocid") %>k' border="0" src="../../images/plus.png" width="20px" height="20px" /></a>

                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDocGroup" runat="server" Text=""></asp:Label>
                                            <tr id="tddiv<%# Eval("posDocid") %>k" style="display: none;">
                                                <td colspan="3">

                                                    <div id="div<%# Eval("posDocid") %>k" style="display: none;">


                                                        <asp:GridView ID="grdDocument" runat="server" AutoGenerateColumns="false" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" Font-Names="tahoma" Font-Size="9pt"
                                                            OnRowDataBound="grdDocument_RowDataBound">
                                                            <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                                            <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />

                                                            <Columns>
                                                              <%--  <asp:TemplateField HeaderText="...">
                                                                    <HeaderTemplate>
                                                                        <asp:CheckBox ID="CbApprAll" runat="server" />
                                                                    </HeaderTemplate>
                                                                    <ItemTemplate>
                                                                        <asp:CheckBox ID="CbDocAppr" runat="server" />

                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>--%>
                                                                <asp:TemplateField HeaderText="แผนก">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDocdivis" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document Type">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDoctype" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="200px" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Document No.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDocno" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Description">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDescrip" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="200px" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Sheet No.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbSheetno" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Revision">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbRev" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Bid No.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbBidno" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="120px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Schdule No.">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbSchno" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Schdule Name">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbSchname" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="100px"  HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                        <asp:LinkButton ID="LinkButton1" runat="server">View Doc</asp:LinkButton>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                                <asp:TemplateField HeaderText="Status">
                                                                    <ItemTemplate>
                                                                        <asp:Label ID="lbDocst" runat="server" Text=""></asp:Label>
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                        </asp:GridView>

                                                    </div>

                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="left" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right;">
                        <button onclick="document.getElementById('id01').style.display='block';return false;" class="btn btn-primary">Close</button>

                    </td>
                </tr>
            </table>

        </div>

         <div id="myModal"  runat="server" class="modal">  
            <div class="modal-content">
                        <div id="dvRounting" runat="server" style="display:none;width:100%">
                    <asp:Label ID="Label56" runat="server" style="text-align: left; color: #006699;margin-left:15px"><b>Rounting</b></asp:Label>
                     <table align="center" style="width:95%;" id="tbRounting" runat="server">
                         <tr>
                             <td colspan="5">
                                 <asp:RadioButtonList ID="rd_approval" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="rd_approval_SelectedIndexChanged" AutoPostBack="true">
                                     <asp:ListItem Text="Approval option 1" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="Approval option 2" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Approval option 3" Value="3"></asp:ListItem>
                                 </asp:RadioButtonList>
                             </td>
                         </tr>
                        <tr>
                            <td style="width:20%" id="td1">
                                <asp:Label ID="lb_headDept" runat="server" Text="Approve 1"></asp:Label>
                            </td>
                            <td style="width:20%" id="td2">
                                 <asp:Label ID="lb_headSect" runat="server" Text="Approve 2"></asp:Label>
                            </td>
                            <td style="width:20%" id="td3">
                                <asp:Label ID="lb_headProject" runat="server" Text="Approve 3"></asp:Label>
                            </td>
                            <td style="width:20%" id="td4">
                                <asp:Label ID="lb_assiDept" runat="server" Text="Approve 4"></asp:Label>
                            </td>
                            <td style="width:20%" id="td5">
                                <asp:Label ID="lb_dept" runat="server" Text="Approve 5" ></asp:Label>
                            </td>
                        </tr>
                         <tr>
                            <td id="td1_1" >
                                <asp:DropDownList ID="ddl_headDept" runat="server" Width="140px"></asp:DropDownList>
                                
                            </td>
                            <td id="td2_2">
                                <asp:DropDownList ID="ddl_headSect" runat="server"  Width="140px"></asp:DropDownList>
                            </td>
                            <td id="td3_3">
                                <asp:DropDownList ID="ddl_headProject" runat="server" Width="140px"></asp:DropDownList>
                            </td>
                            <td id="td4_4">
                                <asp:DropDownList ID="ddl_assiDept" runat="server" Width="140px"></asp:DropDownList>
                            </td>
                            <td id="td5_5">
                                <asp:DropDownList ID="ddl_dept" runat="server" Width="140px"></asp:DropDownList>

                            </td>
                        </tr>
                         <tr>
                             <td style="text-align:right" colspan="5">
                                 <asp:Label ID="lb_rowindex" runat="server"  Visible="False"></asp:Label>
                                 <asp:Button ID="bt_saveRount" runat="server" Text="Save" class="btn btn-primary" OnClick="bt_saveRount_Click"/>
                                 <asp:Button ID="bt_cancRount" runat="server" Text="Cencel" class="btn btn-danger" OnClick="bt_cancRount_Click"/>
                             </td>
                         </tr>
                    </table>
                </div>
                </div>
             </div>

           <div id="dvViewActivity"  runat="server" class="modal" > <%--   class="modal"  --%>
            <div class="modal-content">
                 <table  style="width: 100%">
                     <tr>
                         <td style="width:200px;vertical-align:top;">
                             <asp:Label ID="lblBefore" runat="server" ></asp:Label>
                             </td>
                             <td align="center" style="width:90px">
                             <asp:Image ID="Image1" runat="server" ImageUrl="../../images/fast-forward.png" Width="90px" Height="50px" />
                                 </td>
                          <td  style="width:200px;">
                                <asp:Label ID="lblNow" runat="server" ></asp:Label>
                         </td>
                          <td align="center" style="width:90px">
                             <asp:Image ID="Image2" runat="server" ImageUrl="../../images/fast-forward.png" Width="90px" Height="50px"/>
                         </td>
                          <td  style="width:200px;vertical-align:top;">
                                <asp:Label ID="lblNext" runat="server" ></asp:Label>
                         </td>
                     </tr>
                     <tr>
                         <td style="vertical-align:top;">
                               <asp:GridView ID="gvBefore" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                             <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" Text='<%# Bind("apv_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Activity Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActiname" runat="server" Text='<%# Bind("ps_subject") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppr" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server"  Text='<%# Bind("emp_apvdate") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server" Text='<%# Bind("apv_status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                         </td>
                         <td>
                         </td>
                             <td style="vertical-align:top;">
                               <asp:GridView ID="gvNow" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                             <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" Text='<%# Bind("apv_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField HeaderText="Activity Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActiname" runat="server" Text='<%# Bind("ps_subject") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppr" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server"   Text='<%# Bind("emp_apvdate") %>'  ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server" Text='<%# Bind("apv_status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                         </td>
                         <td></td>
                             <td style="vertical-align:top;">
                              <asp:GridView ID="gvNext" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                        AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                                        <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                        <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                        <RowStyle ForeColor="#000066" />
                                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                        <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                        <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                        <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                        <SortedDescendingHeaderStyle BackColor="#00547E" />
                                        <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                        <Columns>
                                             <asp:TemplateField HeaderText="No.">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblNo" runat="server" Text='<%# Bind("apv_order") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                       
                                            <asp:TemplateField HeaderText="Activity Name">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblActiname" runat="server" Text='<%# Bind("ps_subject") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                             <asp:TemplateField HeaderText="Approved By">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblAppr" runat="server" Text='<%# Bind("emp_name") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Approved Date">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server"   Text='<%# Bind("emp_apvdate") %>' ></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                              <asp:TemplateField HeaderText="Status">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblApprDate" runat="server" Text='<%# Bind("apv_status") %>'></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" />
                                            </asp:TemplateField>
                                        </Columns>
                                    </asp:GridView>
                         </td>
                     </tr>
                     <tr>
                         <td colspan="5" style="text-align:right">
                            <%-- <asp:Button ID="btnCloseView" runat="server" Text="Close" class="btn btn-danger" OnClick="btnCloseView_Click" />--%>
                                <button onclick="document.getElementById('dvViewActivity').style.display='none';return false;" class="btn btn-danger" visible="false">Close</button>
                         </td>
                     </tr>
                    </table>
                </div>
             </div>

        <asp:HiddenField ID="hidJobNo" runat="server" />
        <asp:HiddenField ID="hidJobRev" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidMcShort" runat="server" />
        <asp:HiddenField ID="hidExpDiv" runat="server" />
        <asp:HiddenField ID="hidFindIndexM" runat="server" />
        <asp:HiddenField ID="hidFindIndexS" runat="server" />
        <asp:HiddenField ID="hidTaskID" runat="server" />
        <asp:HiddenField ID="hidchkSelect" runat="server" />
    </form>
</body>
</html>
