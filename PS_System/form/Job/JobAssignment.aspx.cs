﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DemoWeb.form.Job
{
    public partial class JobAssignment : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            // --------------- upload drawing  -------------------------

            DataTable dtDesgin = new DataTable();
            dtDesgin.Columns.Add("isCheck");
            dtDesgin.Columns.Add("designNo");
            dtDesgin.Columns.Add("status");
            DataRow dr = dtDesgin.NewRow();
            dr["isCheck"] = true;
            dr["designNo"] = "Drawing No1";
            dr["status"] = "Approved";
            dtDesgin.Rows.Add(dr);
            dr = dtDesgin.NewRow();
            dr["isCheck"] = true;
            dr["designNo"] = "Drawing No2";
            dr["status"] = "";
            dtDesgin.Rows.Add(dr);
            dr = dtDesgin.NewRow();
            dr["isCheck"] = true;
            dr["designNo"] = "Drawing No3";
            dr["status"] = "";
            dtDesgin.Rows.Add(dr);
            dr = dtDesgin.NewRow();
            dr["isCheck"] = true;
            dr["designNo"] = "Drawing No4";
            dr["status"] = "";
            dtDesgin.Rows.Add(dr);

            gvDrawing.DataSource = dtDesgin;
            gvDrawing.DataBind();

            // --------------- equipment -------------------------
            DataTable dtEquipment = new DataTable();
            dtEquipment.Columns.Add("equipmentName");
            dtEquipment.Columns.Add("equipmentType");
            dtEquipment.Columns.Add("ViewDoc");
            dtEquipment.Columns.Add("status");

            DataRow dr3 = dtEquipment.NewRow();
            dr3["equipmentName"] = "Equipment1";
            dr3["equipmentType"] = "EquipmentType1";
            dr3["status"] = "Approved";
            dtEquipment.Rows.Add(dr3);

            dr3 = dtEquipment.NewRow();
            dr3["equipmentName"] = "Equipment2";
            dr3["equipmentType"] = "EquipmentType2";
            dr3["status"] = "Approved";
            dtEquipment.Rows.Add(dr3);

            dr3 = dtEquipment.NewRow();
            dr3["equipmentName"] = "Equipment3";
            dr3["equipmentType"] = "EquipmentType3";
            dr3["status"] = "Approved";
            dtEquipment.Rows.Add(dr3);

            dr3 = dtEquipment.NewRow();
            dr3["equipmentName"] = "Equipment4";
            dr3["equipmentType"] = "EquipmentType4";
            dr3["status"] = "Approved";
            dtEquipment.Rows.Add(dr3);


            gvEquipment.DataSource = dtEquipment;
            gvEquipment.DataBind();

            //-----------------Doc List -------------------
            DataTable dtListDoc = new DataTable();
            dtListDoc.Columns.Add("docType");
            dtListDoc.Columns.Add("docDetail");
            dtListDoc.Columns.Add("docName");

            DataRow dr5 = dtListDoc.NewRow();
            dr5["docType"] = "docType1";
            dr5["docDetail"] = "docDetail1";
            dr5["docName"] = "docName1";

            dtListDoc.Rows.Add(dr5);

            dr5 = dtListDoc.NewRow();
            dr5["docType"] = "docType2";
            dr5["docDetail"] = "docDetail2";
            dr5["docName"] = "docName2";

            dtListDoc.Rows.Add(dr5);


            dr5 = dtListDoc.NewRow();
            dr5["docType"] = "docType3";
            dr5["docDetail"] = "docDetail3";
            dr5["docName"] = "docName3";

            dtListDoc.Rows.Add(dr5);

            dr5 = dtListDoc.NewRow();
            dr5["docType"] = "docType4";
            dr5["docDetail"] = "docDetail4";
            dr5["docName"] = "docName4";

            dtListDoc.Rows.Add(dr5);

            gvDocList.DataSource = dtListDoc;
            gvDocList.DataBind();



            // --------------- equipment master-------------------------
            DataTable dtEquipmentMas = new DataTable();
            dtEquipmentMas.Columns.Add("equipmentName");
            dtEquipmentMas.Columns.Add("equipmentType");
            dtEquipmentMas.Columns.Add("equipmentDetail");
            dtEquipmentMas.Columns.Add("expDate");

            DataRow dr4 = dtEquipmentMas.NewRow();
            dr4["equipmentName"] = "Equipment1";
            dr4["equipmentType"] = "EquipmentType1";
            dr4["equipmentDetail"] = "Detail1";
            dr4["expDate"] = "12/12/2021";
            dtEquipmentMas.Rows.Add(dr4);

            dr4 = dtEquipmentMas.NewRow();
            dr4["equipmentName"] = "Equipment2";
            dr4["equipmentType"] = "EquipmentType2";
            dr4["equipmentDetail"] = "Detail2";
            dr4["expDate"] = "12/12/2021";
            dtEquipmentMas.Rows.Add(dr4);

            dr4 = dtEquipmentMas.NewRow();
            dr4["equipmentName"] = "Equipment3";
            dr4["equipmentType"] = "EquipmentType3";
            dr4["equipmentDetail"] = "Detail3";
            dr4["expDate"] = "12/12/2021";
            dtEquipmentMas.Rows.Add(dr4);

            dr4 = dtEquipmentMas.NewRow();
            dr4["equipmentName"] = "Equipment4";
            dr4["equipmentType"] = "EquipmentType4";
            dr4["equipmentDetail"] = "Detail4";
            dr4["expDate"] = "12/12/2021";
            dtEquipmentMas.Rows.Add(dr4);


            gvEquipMaster.DataSource = dtEquipmentMas;
            gvEquipMaster.DataBind();

            // --------------- upload doc -------------------------

            DataTable dtUploadDoc = new DataTable();
            dtUploadDoc.Columns.Add("docType");
            dtUploadDoc.Columns.Add("docName");
            dtUploadDoc.Columns.Add("bid");
            dtUploadDoc.Columns.Add("schedule");
            dtUploadDoc.Columns.Add("status");
            DataRow dr2 = dtUploadDoc.NewRow();
            dr2["docType"] = "Invoice";
            dr2["docName"] = "Doc No1";
            dr2["bid"] = "bid 1";
            dr2["schedule"] = "schedule 1";
            dr2["status"] = "Reviewed";
            dtUploadDoc.Rows.Add(dr2);

            dr2 = dtUploadDoc.NewRow();
            dr2["docType"] = "Invoice";
            dr2["docName"] = "Doc No2";
            dr2["bid"] = "bid 2";
            dr2["schedule"] = "schedule 2";
            dr2["status"] = "Reviewed";
            dtUploadDoc.Rows.Add(dr2);

            dr2 = dtUploadDoc.NewRow();
            dr2["docType"] = "Invoice";
            dr2["docName"] = "Doc No3";
            dr2["bid"] = "bid 3";
            dr2["schedule"] = "schedule 3";
            dr2["status"] = "Reviewed";
            dtUploadDoc.Rows.Add(dr2);

            dr2 = dtUploadDoc.NewRow();
            dr2["docType"] = "Invoice";
            dr2["docName"] = "Doc No4";
            dr2["bid"] = "bid 4";
            dr2["schedule"] = "schedule 4";
            dr2["status"] = "Reviewed";
            dtUploadDoc.Rows.Add(dr2);

            gvUpLoadDoc.DataSource = dtUploadDoc;
            gvUpLoadDoc.DataBind();


        }


        //gvDrawing_RowDataBound
        protected void gvDrawing_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                CheckBox chkbox = (CheckBox)e.Row.FindControl("chkSelect");
                if (e.Row.RowIndex % 4 == 0)
                {
                    e.Row.Cells[2].Attributes.Add("rowspan", "4");
                }
                else
                {
                    e.Row.Cells[2].Visible = false;
                }
            }



        }
        protected void gvUpLoadDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex % 4 == 0)
                {
                    e.Row.Cells[4].Attributes.Add("rowspan", "4");
                }
                else
                {
                    e.Row.Cells[4].Visible = false;
                }
            }
        }

        protected void gvEquipment_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.RowIndex % 4 == 0)
                {
                    e.Row.Cells[3].Attributes.Add("rowspan", "4");
                }
                else
                {
                    e.Row.Cells[3].Visible = false;
                }
            }
        }


    }
}