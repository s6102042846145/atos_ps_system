﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using PS_Library.DocumentManagement;
using System.IO;
using System.Text.RegularExpressions;
using System.IO.Compression;
using System.Web.UI.HtmlControls;
using System.Drawing;
using System.Text;


namespace PS_System.form.Job
{
    public partial class MyTaskJob : System.Web.UI.Page
    {
        clsMyTaskJob objJobDb = new clsMyTaskJob();
        CultureInfo th = new CultureInfo("th-TH");
        CultureInfo en = new CultureInfo("en-US");
        public string strFolderAttNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString();
        public string strFolderAttTaskNodeId = ConfigurationManager.AppSettings["wfps_att_folder"].ToString();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                {
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                }
                else
                {
                     
                    hidLogin.Value = "z595985";//"z596353";//  z594900  z548308
                }
                if (Request.QueryString["jobno"] != null)
                {
                    hidJobNo.Value = Request.QueryString["jobno"];
                }
                else
                {
                    hidJobNo.Value = "TS12-11-S06";//TIPE-01-S03  *TS12-04-S02
                }
                if (Request.QueryString["rev"] != null)
                {
                    rev.Value = Request.QueryString["rev"];
                }
                else
                {
                    rev.Value = "3";
                }
                if (Request.QueryString["workid"] != null)
                {
                    hidWorkID.Value = Request.QueryString["workid"];
                }
                else
                {
                    hidWorkID.Value = "3052212";  //2949804

                }
                if (Request.QueryString["subworkid"] != null)
                {
                    hidSubWorkID.Value = Request.QueryString["subworkid"];
                }
                else
                {
                    hidSubWorkID.Value = "3052212"; //2949804

                }
                if (Request.QueryString["taskid"] != null)
                {
                    hidTaskID.Value = Request.QueryString["taskid"];
                }
                else
                {
                    hidTaskID.Value = "0";

                }

                //Debug
                //hidLogin.Value = "z594687";
                //hidWorkID.Value = "2656955";
                //hidSubWorkID.Value = "2656955";
                //hidTaskID.Value = "5";
                getWorkflowAttribute();
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;



                SectionAssign();
                SelectData();
                bind_data();
                DDLbinddata();
                //DataTable tt = new DataTable();
                //bind_equiList("", "", "", "", tt);
                rdbEquip.Checked = true;
                rdbEquipAdd.Checked = true;
                ViewState["history"] = "";
                ViewState["first"] = "";
                ViewState["firstDoc"] = "";
                ViewState["chkflag"] = "";
                ViewState["first"] = "";
                ViewState["addAllPart"] = null;
                ViewState["addAllPartNew"] = "";
                ViewState["itmno"] = "";
                
                bind_equipment("");
                loadLegendData();
                getconfig();
            }
            else
            {

                //bind_data();
                //DataTable tt = new DataTable();
                //bind_equiList("", "", "", "", tt);
                myModal.Style["display"] = "none";
                dvhidEq.Style["display"] = "none";
                dvhiddraw.Style["display"] = "none";
                dvhidDoc.Style["display"] = "none";
                if (tb_val.Text == "")
                {
                    if (hidSelPart.Value != "")
                    {
                        bind_equipment(hidSelPart.Value);
                        dvhidEq.Style["display"] = "block";
                    }
                    else
                    {
                        bind_equipment("");

                    }
                }
                if (hidSelPart.Value != "")
                {
                    la_part.Text = hidSelPart.Value;
                    //hidSelPart.Value = "";
                }
                ViewState["first"] = "1";
                
            }
        }
        protected void getconfig()
        {

            DataTable dt = objJobDb.getconfigure(txtJobNo.Text, txtJobRev.Text, hidDept.Value, hidSect.Value);
            if (dt.Rows.Count > 0)
            {
                gv_config.DataSource = dt;
                gv_config.DataBind();

            }
        }
        private void loadLegendData()
        {

            DataTable dt = new DataTable();
            dt.Columns.Add("legend");
            dt.Columns.Add("legend_qty");
            DataRow dr = dt.NewRow();
            dr["legend"] = "";
            dr["legend_qty"] = "0";
            dt.Rows.Add(dr);
            if (dt.Rows.Count > 0)
            {
                gvLegend.DataSource = dt;
                gvLegend.DataBind();
                Session.Add("dtLegend", dt);
            }

            DataTable dtAdd = new DataTable();
            dtAdd.Columns.Add("legend");
            dtAdd.Columns.Add("legend_qty");
            DataRow drAdd = dtAdd.NewRow();
            drAdd["legend"] = "";
            drAdd["legend_qty"] = "0";
            dtAdd.Rows.Add(drAdd);
            if (dtAdd.Rows.Count > 0)
            {
                gvLegendAdd.DataSource = dtAdd;
                gvLegendAdd.DataBind();
                Session.Add("dtLegendAdd", dtAdd);
            }
        }
        private void getWorkflowAttribute()
        {

            if ((hidWorkID.Value != "" && hidWorkID.Value != "0") && (hidSubWorkID.Value != "" && hidSubWorkID.Value != "0"))
            {
                OTFunctions ot = new OTFunctions();
                var wfAttrs = ot.getWFAttributes(hidWorkID.Value, hidSubWorkID.Value);
                string pidno = ot.getwfAttrValue(ref wfAttrs, "PID");
                string activity_id = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_ID");
                string activity_name = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_NAME");
                hidAcID.Value = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_ID");
                tb_acName.Text = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_NAME");
                hidJobNo.Value = ot.getwfAttrValue(ref wfAttrs, "JOB_NO");
                rev.Value = ot.getwfAttrValue(ref wfAttrs, "JOB_REV");
                hidPID.Value = ot.getwfAttrValue(ref wfAttrs, "PID");
            }
        }
        private void SelectData()
        {

            //DataTable dt = objJobDb.GetData(hidJobNo.Value);
            //    //zdbUtil.ExecSql_DataTable(sql, zetl4eisdb);
            //if (dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        txtJobNo.Text = dt.Rows[i]["job_no"].ToString();
            //        txtJobRev.Text = dt.Rows[i]["revision"].ToString();
            //        txtSubLine.Text = dt.Rows[i]["sub_line_name"].ToString();
            //        txtJobDesc.Text = dt.Rows[i]["job_desc"].ToString();
            //        txtRemark.Text = dt.Rows[i]["remark"].ToString();
            //        txtJobContent.Text = dt.Rows[i]["reference"].ToString();
            //    }
            //}

            var jinfo = objJobDb.getJobInfo(hidJobNo.Value);
            txtJobNo.Text = jinfo.job_no;
            txtJobRev.Text = jinfo.revision;
            txtSubLine.Text = jinfo.sub_line_name;
            txtJobDesc.Text = jinfo.job_desc;
            txtRemark.Text = jinfo.remark;
            if (jinfo.completion_date != "") { lblCom.Text = jinfo.completion_date; dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:fit-content;padding: 1px 2px;height:fit-content;"); }
            else dvlblCom.Attributes.Add("style", "border:2px solid #d8d8d8;width:150px;height:26px;");
            DateTime dtSta = new DateTime();
            DateTime dtEnd = new DateTime();
            if (jinfo.p6_plan_start != "")
            {
                dtSta = Convert.ToDateTime(jinfo.p6_plan_start);
                txtStartDate.Text = dtSta.ToString("dd/MM/yyyy");
            }
            if (jinfo.p6_plan_end != "")
            {
                dtEnd = Convert.ToDateTime(jinfo.p6_plan_end);
                txtEndDate.Text = dtEnd.ToString("dd/MM/yyyy");
            }
            //popup
            tbPopJob.Text = txtJobNo.Text;
            tbPopRev.Text = txtJobRev.Text;
            tbAddjob.Text = txtJobNo.Text;
            tbAddrev.Text = txtJobRev.Text;
            txtDocjobs.Text = txtJobNo.Text;
            txtDocRevs.Text = txtJobRev.Text;
            tb_jobNoPart.Text = txtJobNo.Text;
            tb_revPart.Text = txtJobRev.Text;
            DataTable dt = new DataTable();
            string[] type = txtJobNo.Text.Split('-');
            if (type.Length == 3)
            {
                dt = objJobDb.getAllPart(type[2]);
                if (dt.Rows.Count > 0)
                {
                    gvAllPart.DataSource = dt;
                    gvAllPart.DataBind();
                }
            }

            ddl_bidPart.Items.Clear();
            dt = objJobDb.GetJobRelate(hidJobNo.Value, "");
            ddl_bidPart.DataSource = dt;
            ddl_bidPart.DataTextField = "bid_no";
            ddl_bidPart.DataValueField = "bid_revision";
            ddl_bidPart.DataBind();

            dt = objJobDb.sumPart(txtJobNo.Text, txtJobRev.Text, "");
            foreach (DataRow dr in dt.Rows)
            {
                string[] tmp = dr["part_list"].ToString().Split('|');
                for (int i = 0; i < tmp.Length; i++)
                {
                    if (hidAllPart.Value == "")
                    {
                        hidAllPart.Value = "'" + tmp[i] + "'";
                    }
                    else
                    {
                        if (hidAllPart.Value.IndexOf("'" + tmp[i] + "'") == -1 && hidAllPart.Value.IndexOf(tmp[i] + "'") == -1)
                            hidAllPart.Value += ",'" + tmp[i] + "'";
                    }

                }
            }
            DataTable dt2 = objJobDb.getAttachWAjob(hidJobNo.Value);
            if (dt2.Rows.Count > 0)
            {
                gvAttach.DataSource = dt2;
                gvAttach.DataBind();
            }
            else
            {
                gvAttach.DataSource = null;
                gvAttach.DataBind();
            }
            DataTable dttmp = objJobDb.loadTempDDL(hidDept.Value,hidSect.Value);
            if (dttmp.Rows.Count > 0)
            {
                ddl_temp.Items.Clear();
                ddl_temp.DataSource = dttmp;
                ddl_temp.DataTextField = "mytemp_name";
                ddl_temp.DataValueField = "mytemp_id";
                ddl_temp.DataBind();
            }
        }
        protected void bind_equipment(string part)
        {
            DataTable dt = new DataTable();
            dt = objJobDb.getTabBid(txtJobNo.Text, txtJobRev.Text);
            ultab.Controls.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                //HtmlGenericControl a = new HtmlGenericControl("a");
                //HtmlAnchor a = new HtmlAnchor();
                Button bt_part = new Button();
                //a.Attributes["data-toggle"] = "tab";
                if (dt.Rows[i]["bid_no"].ToString() != "Bid Supply" && dt.Rows[i]["bid_no"].ToString().Length > 10) bt_part.Text = dt.Rows[i]["bid_no"].ToString().Substring(0, 10) + "...";
                else bt_part.Text = dt.Rows[i]["bid_no"].ToString();
                bt_part.ID = "bt_tab" + i.ToString();
                bt_part.ToolTip = dt.Rows[i]["bid_no"].ToString() + " " + dt.Rows[i]["schedule_name"].ToString();
                bt_part.Width = 100;
                bt_part.Attributes["runat"] = "server";
                //bt_part.Style["cursor"]= "pointer";

                if (i == 0)
                {
                    if (part == "" && hidChkTab.Value == "")
                    {
                        bt_part.CssClass = "btnSel";
                        hidBidNo.Value = dt.Rows[i]["bid_no"].ToString();
                        hidScheName.Value = dt.Rows[i]["schedule_name"].ToString();
                        hidScheNo.Value = dt.Rows[i]["schedule_no"].ToString() == "" ? "0" : dt.Rows[i]["schedule_no"].ToString();
                        hidBidRev.Value = dt.Rows[i]["bid_revision"].ToString();
                        if (hidBidNo.Value != "")
                        {
                            if (hidBidNo.Value == "Bid Supply") ddl_bidPart.SelectedValue = "0";
                            else ddl_bidPart.Items.FindByText(hidBidNo.Value).Selected = true;
                            ddl_bidPart.Attributes.Add("disabled", "");
                        }
                        if (hidScheNo.Value != "" && hidScheNo.Value != "0")
                        {
                            ddl_schedulePart.SelectedValue = hidScheNo.Value;
                            ddl_schedulePart.Attributes.Add("disabled", "");
                        }
                        if (hidBidNo.Value == "Bid Supply")
                        {
                            bt_AddPart.Visible = false;
                            hidChkSupply.Value = "true";
                            //rdbMytemp.Visible = false;
                        }
                        else
                        {
                            bt_AddPart.Visible = true;
                            hidChkSupply.Value = "false";
                            //rdbMytemp.Visible = true;
                        }
                    }
                    else if (part != "" && hidChkTab.Value == "")
                    {
                        //bt_part.CssClass = "btnclass";
                        bt_part.CssClass = "btnSel";
                    }
                    else if (part != "" && hidChkTab.Value == "0")
                    {
                        //bt_part.CssClass = "btnclass";
                        bt_part.CssClass = "btnSel";
                    }
                    else
                    {
                        bt_part.CssClass = "btnclass";
                    }

                }
                else { bt_part.CssClass = "btnclass"; 
                
                }
                bt_part.Click += new EventHandler(selTab);
                li.Controls.Add(bt_part);

                ultab.Controls.Add(li);
            }
            if (hidBidNo.Value != "")
            {
                if (hidBidNo.Value == "Bid Supply") ddl_bidPart.SelectedValue = "0";
                else ddl_bidPart.Items.FindByText(hidBidNo.Value).Selected = true;
                ddl_bidPart.Attributes.Add("disabled", "");
            }
            if (hidScheNo.Value != "" && hidScheNo.Value != "0")
            {
                ddl_schedulePart.SelectedValue = hidScheNo.Value;
                ddl_schedulePart.Attributes.Add("disabled", "");
            }
            dt = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, hidJobNo.Value);
            if (dt.Rows.Count > 0)
            {
                //if (part == "") dt = objDB.getGV(hidDept.Value, hidSect.Value, hidJobNo.Value, hidScheNo.Value);
                if (part == "") dt = objJobDb.getGVSelPart(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, dt.Rows[0]["part_name"].ToString(), hidJobNo.Value, rev.Value, hidScheName.Value);
                else dt = objJobDb.getGVSelPart(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, part, hidJobNo.Value, rev.Value, hidScheName.Value);
                if (dt.Rows.Count > 0)
                {
                    //dt.DefaultView.Sort = "equip_item_no asc";
                    gvAllEqui.DataSource = dt;
                    gvAllEqui.DataBind();

                }
                else
                {
                    gvAllEqui.DataSource = null;
                    gvAllEqui.DataBind();

                }
            }
            else
            {
                gvAllEqui.DataSource = null;
                gvAllEqui.DataBind();
            }
            dvPart.Controls.Clear();
            Label lb_part = new Label();
            lb_part.Text = "Part: ";

            dt = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, hidJobNo.Value);
            ddl_searchPart.Items.Clear();
            ddl_searchPart.SelectedIndex = -1;
            ddl_searchPart.Items.Clear();
            ddl_searchPart.DataSource = dt;
            ddl_searchPart.DataTextField = "part_name";
            ddl_searchPart.DataValueField = "part_name";
            ddl_searchPart.DataBind();
            ddl_searchPart.Items.Insert(0, new ListItem("All Parts", "all"));
            ddl_searchPart.Attributes.Add("onclick", "getPart();return false;");
            //if (hidSearchPart.Value != "") ddl_searchPart.SelectedValue = hidSearchPart.Value;
            for (int k = 0; k < dt.Rows.Count; k++)
            {
                string d = ViewState["chkflag"].ToString();
                if (part != "" && part == dt.Rows[k]["part_name"].ToString() && ViewState["chkflag"].ToString()!="")
                {

                        DataTable dt_eq = objJobDb.getGVSelPart(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, part, hidJobNo.Value, rev.Value, hidScheName.Value);
                        gv_SelEquip.DataSource = dt_eq;
                        gv_SelEquip.DataBind();
                        ViewState["dataEQ"] = dt_eq;
                        gv_SelEquip.Visible = true;
                        ViewState["chkflag"] = "";
                        tbAddPath.Text = part;

                    
                }
                else if (part == "" && ViewState["chkflag"].ToString() == "")
                {
                    DataTable dt_eq = objJobDb.getGVSelPart(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, dt.Rows[k]["part_name"].ToString(), hidJobNo.Value, rev.Value, hidScheName.Value);
                    gv_SelEquip.DataSource = dt_eq;
                    gv_SelEquip.DataBind();
                    ViewState["dataEQ"] = dt_eq;
                    gv_SelEquip.Visible = true;
                    ViewState["chkflag"] = "1";//1
                    la_part.Text = dt.Rows[k]["part_name"].ToString();
                    tbAddPath.Text = dt.Rows[k]["part_name"].ToString();
                }
                if (part != "" && part == dt.Rows[k]["part_name"].ToString())
                {
                    Label lb_remark = new Label();
                    lb_remark.Text = "Note: ";
                    dvPart.Controls.Add(lb_remark);
                    tb_remarkprt.Text = dt.Rows[k]["part_remark"].ToString();
                    tb_remarkprt.Visible = true;
                    dvPart.Controls.Add(tb_remarkprt);
                    Label lb_desc = new Label();
                    lb_desc.Text = "Description: ";
                    dvPart.Controls.Add(lb_desc);
                    if (dt.Rows[k]["part_description"].ToString() != "") tb_descprt.Text = dt.Rows[k]["part_description"].ToString();
                    else tb_descprt.Text = "";
                    tb_descprt.Visible = true;
                    dvPart.Controls.Add(tb_descprt);
                    Label lb_import = new Label();
                    lb_import.Text = "Important: ";
                    dvPart.Controls.Add(lb_import);
                    tb_import.Text = dt.Rows[k]["important"].ToString();
                    tb_import.Visible = true;
                    dvPart.Controls.Add(tb_import);
                    Label lb_space = new Label();
                    lb_space.Text = "<Br/>";
                    dvPart.Controls.Add(lb_space);
                }
            }
            for (int j = 0; j < dt.Rows.Count; j++)
            {
                Button btn_part = new Button();
                if (j == 0) { dvPart.Controls.Add(lb_part);
                    hidPartTemp.Value = part == "" ? dt.Rows[j]["part_name"].ToString() : part;
                }
                btn_part.Text = dt.Rows[j]["part_name"].ToString();
                btn_part.Click += new EventHandler(btn_SelPart);
                btn_part.Attributes.Add("runat", "server");
                btn_part.Attributes.Add("onclick", "selectPart('" + btn_part.Text + "')");
                Label lb = new Label();
                lb.Text = " ";
                LinkButton lnk = new LinkButton();
                lnk.Text = "x";
                lnk.ToolTip = "Delete";
                lnk.ForeColor = Color.Red;
                lnk.BackColor = Color.White;
                lnk.Style["position"] = "relative";
                lnk.Style["top"] = "-4px";
                //lnk.Click += new EventHandler(lnk_delPart);
                lnk.Attributes.Add("onclick", "delPart('" + btn_part.Text + "','" + hidBidNo.Value + "','" + hidScheNo.Value + "','"+ hidJobNo.Value + "');return false;");
                if (hidchkassign.Value != "") lnk.Visible = true;
                else lnk.Visible = false;
                dvPart.Controls.Add(btn_part);
                dvPart.Controls.Add(lnk);
                dvPart.Controls.Add(lb);
                //if (j == 0) dvPart.Controls.Add(btn_part);
                //else { dvPart.Controls.Add(lb); dvPart.Controls.Add(btn_part);  }//dvPart.Controls.Add(btn_part);
                lb.CssClass = "";
                if (part != "" && part == dt.Rows[j]["part_name"].ToString())
                {
                    btn_part.CssClass = "btnPartSel";
                }
                else
                {
                    if (j == 0 && part == "") btn_part.CssClass = "btnPartSel";
                    else btn_part.CssClass = "";
                }
                tb_remarkprt.CssClass = "";
            }
            //dvhidEq.Style["display"] = "block";

            OverlayPopup.Style["display"] = "none";
        }
        private void btn_SelPart(object sender, EventArgs e)
        {
            Button bt = sender as Button;
            string path = bt.Text;
            //tbAddPath.Text = bt.Text;
            //la_part.Text = bt.Text;
            OverlayPopup.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";

        }
        protected void selTab(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dvhidEq.Style["display"] = "block";
            Button bt = sender as Button;
            string a = bt.ID.Substring(bt.ID.IndexOf(bt.ID) + bt.ID.Length - 1);
            //dt = objJobDb.GetJobRelate(hidJobNo.Value, "");
            dt = objJobDb.getTabBid(txtJobNo.Text, txtJobRev.Text);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = "bt_tab" + i;
                Button btn = (Button)this.FindControl(id);
                if (a == i.ToString())
                {
                    btn.CssClass = "btnSel";
                    hidBidNo.Value = dt.Rows[i]["bid_no"].ToString();
                    hidScheName.Value = dt.Rows[i]["schedule_name"].ToString();
                    hidScheNo.Value = dt.Rows[i]["schedule_no"].ToString() == "" ? "0" : dt.Rows[i]["schedule_no"].ToString();
                    hidBidRev.Value = dt.Rows[i]["bid_revision"].ToString();
                    if (hidBidNo.Value != "")
                    {
                        ddl_bidPart.SelectedIndex = -1;
                        //ddl_bidPart.SelectedValue = hidBidRev.Value;
                        if (hidBidNo.Value == "Bid Supply") ddl_bidPart.SelectedValue = "0";
                        else ddl_bidPart.Items.FindByText(hidBidNo.Value).Selected = true;
                        ddl_bidPart.Attributes.Add("disabled", "");
                    }
                    if (hidScheNo.Value != "" && hidScheNo.Value != "0")
                    {
                        ddl_schedulePart.SelectedValue = hidScheNo.Value;
                        ddl_schedulePart.Attributes.Add("disabled", "");
                    }
                    if (hidBidNo.Value == "Bid Supply")
                    {
                        bt_AddPart.Visible = false;
                        hidChkSupply.Value = "true";
                        //rdbMytemp.Visible = false;
                    }
                    else
                    {
                        bt_AddPart.Visible = true;
                        hidChkSupply.Value = "false";
                        //rdbMytemp.Visible = true;
                    }
                    hidChkTab.Value = a;
                }
                else
                {
                    btn.CssClass = "btnclass";
                }
            }
            //dt = objDB.getGV(hidDept.Value, hidSect.Value, jobNo, schNo);
            dt = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value,hidJobNo.Value);
            if (dt.Rows.Count > 0)
            {
                dt = objJobDb.getGVSelPart(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, dt.Rows[0]["part_name"].ToString(), hidJobNo.Value, rev.Value, hidScheName.Value);
                if (dt.Rows.Count > 0)
                {
                    gvAllEqui.DataSource = dt;
                    gvAllEqui.DataBind();

                }
                else
                {
                    gvAllEqui.DataSource = null;
                    gvAllEqui.DataBind();

                }
            }
            else
            {
                gvAllEqui.DataSource = null;
                gvAllEqui.DataBind();
            }

            dvPart.Controls.Clear();
            Label lb_part = new Label();
            lb_part.Text = "Part: ";

            dt = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, hidJobNo.Value);
            ddl_searchPart.Items.Clear();
            ddl_searchPart.SelectedIndex = -1;
            ddl_searchPart.DataSource = dt;
            ddl_searchPart.DataTextField = "part_name";
            ddl_searchPart.DataValueField = "part_name";
            ddl_searchPart.DataBind();
            ddl_searchPart.Attributes.Add("onclick", "getPart();return false;");
            ddl_searchPart.Items.Insert(0, new ListItem("All Parts", "all"));

            for (int j = 0; j < dt.Rows.Count; j++)
            {

                Button btn_part = new Button();
                if (j == 0) { dvPart.Controls.Add(lb_part);
                    hidPartTemp.Value = dt.Rows[j]["part_name"].ToString();
                }
                btn_part.Text = dt.Rows[j]["part_name"].ToString();
                btn_part.Click += new EventHandler(btn_SelPart);
                btn_part.Attributes.Add("runat", "server");
                btn_part.Attributes.Add("onclick", "selectPart('" + btn_part.Text + "')");
                Label lb = new Label();
                lb.Text = " ";
                LinkButton lnk = new LinkButton();
                lnk.Text = "x";
                lnk.ToolTip = "Delete";
                lnk.ForeColor = Color.Red;
                lnk.BackColor = Color.White;
                lnk.Style["position"] = "relative";
                lnk.Style["top"] = "-4px";
                lnk.CommandName = dt.Rows[j]["part_name"].ToString() + "," + hidJobNo.Value + "," + hidScheNo.Value;
                //lnk.Click += new EventHandler(lnk_delPart);
                lnk.Attributes.Add("onclick", "delPart('" + btn_part.Text + "','" + hidBidNo.Value + "','" + hidScheNo.Value + "','" + hidJobNo.Value + "');return false;");
                dvPart.Controls.Add(btn_part);
                dvPart.Controls.Add(lnk);
                dvPart.Controls.Add(lb);
                //if (j == 0) dvPart.Controls.Add(btn_part);
                //else { dvPart.Controls.Add(lb); dvPart.Controls.Add(btn_part);  }//dvPart.Controls.Add(btn_part);
                lb.CssClass = "";

                if (j == 0) btn_part.CssClass = "btnPartSel";
                else btn_part.CssClass = "";

                tb_remarkprt.CssClass = "";
                if (j == 0)
                {
                    DataTable dt_eq = objJobDb.getGVSelPart(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, dt.Rows[j]["part_name"].ToString(), hidJobNo.Value, rev.Value, hidScheName.Value);
                    gv_SelEquip.DataSource = dt_eq;
                    gv_SelEquip.DataBind();
                    ViewState["dataEQ"] = dt_eq;
                    gv_SelEquip.Visible = true;
                    ViewState["chkflag"] = "1";
                    la_part.Text = dt.Rows[j]["part_name"].ToString();
                    tbAddPath.Text = dt.Rows[j]["part_name"].ToString();
                }

            }
            hidSelPart.Value = "";
            OverlayPopup.Style["display"] = "none";
            //gv_SelEquip.DataSource = null;
            //gv_SelEquip.DataBind();

        }

        private void bind_data()
        {
            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value, "");
            if (dt.Rows.Count > 0)
            {
                gvTaskSch.DataSource = dt;
                gvTaskSch.DataBind();

                
            }
            else
            {
                gvTaskSch.DataSource = null;
                gvTaskSch.DataBind();
            }

            DataTable dtAtt = objJobDb.GetAttach("draw", hidDept.Value, hidSect.Value);
            if (dtAtt.Rows.Count > 0)
            {
                gvAttFile.DataSource = dtAtt;
                gvAttFile.DataBind();
            }
            else
            {
                gvAttFile.DataSource = null;
                gvAttFile.DataBind();
            }
            DataTable dtAdddoc = objJobDb.GetAttach("", hidDept.Value, hidSect.Value);
            if (dtAdddoc.Rows.Count > 0)
            {
                gvAddDoc.DataSource = dtAdddoc;
                gvAddDoc.DataBind();
            }
            else
            {
                gvAddDoc.DataSource = null;
                gvAddDoc.DataBind();
            }

            DataTable dtDraw = objJobDb.GetDrawing(hidJobNo.Value, "draw", hidDept.Value, hidSect.Value, txtJobRev.Text);
            if (dtDraw.Rows.Count > 0)
            {
                gvDrawing.DataSource = dtDraw;
                gvDrawing.DataBind();
            }
            else
            {
                gvDrawing.DataSource = null;
                gvDrawing.DataBind();
            }
            DataTable dtTask = objJobDb.GetDrawing(hidJobNo.Value, "", hidDept.Value, hidSect.Value, txtJobRev.Text);
            if (dtTask.Rows.Count > 0)
            {
                gvTaskDoc.DataSource = dtTask;
                gvTaskDoc.DataBind();
            }
            else
            {
                gvTaskDoc.DataSource = null;
                gvTaskDoc.DataBind();
            }


            DataTable dtk = new DataTable();
            dtk.Columns.Add("Rowno");
            dtk.Columns.Add("header2");
            DataRow dd = dtk.NewRow();
            dd["Rowno"] = "1";
            dd["header2"] = "2";
            dtk.Rows.Add(dd);

            //gvTemplate.DataSource = dtTaskDoc;
            //gvTemplate.DataBind();
            //if (gvTemplate.Rows.Count > 0)
            //{
            //    gvTemplate.UseAccessibleHeader = true;
            //    gvTemplate.HeaderRow.TableSection = TableRowSection.TableHeader;
            //}
            // gvItem gvHistory gvDocview.   
            //gvItem.DataSource = dtk;
            //gvItem.DataBind();

            //gvHistory.DataSource = dtTaskDoc;
            //gvHistory.DataBind();
            gvDocview.DataSource = dtk;
            gvDocview.DataBind();

            //gvEquipList.DataSource = dtk;
            //gvEquipList.DataBind();
            //gvTaskDoc.DataSource = dtk;
            //gvTaskDoc.DataBind();

            //gvAddDoc.DataSource = dtTaskDoc;
            //gvAddDoc.DataBind();


        }
        protected void gvAllEqui_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[14].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Unit Price";
                e.Row.Cells[15].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Amount";
                e.Row.Cells[16].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[17].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Amount";
                e.Row.Cells[18].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[19].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Amount";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //ListBox ddl_meterialGrp = (ListBox)e.Row.FindControl("ddl_meterialGrp");
                //DataTable dtmet = new DataTable();
                //clsEquipmentMasterdb objEqbb = new clsEquipmentMasterdb();
                //dtmet = objEqbb.GetSapMaterialGroup();
                //ddl_meterialGrp.Items.Clear();
                //ddl_meterialGrp.DataSource = dtmet;
                //ddl_meterialGrp.DataTextField = "CODE";
                //ddl_meterialGrp.DataValueField = "NAME";
                //ddl_meterialGrp.DataBind();
                //if (drv["equip_mas_grp"].ToString() != "")
                //{
                //    ddl_meterialGrp.SelectedValue = drv["equip_mas_grp"].ToString();
                //}
                Label lb_meterialGrp = (Label)e.Row.FindControl("lb_meterialGrp");
                lb_meterialGrp.Text = drv["equip_mas_grp"].ToString();
                Label lb_codeNo = (Label)e.Row.FindControl("lb_codeNo");
                lb_codeNo.Text = drv["equip_code"].ToString();
                Label lb_itmNo = (Label)e.Row.FindControl("lb_itmNo");
                lb_itmNo.Text = drv["equip_item_no"].ToString();
                Label lb_fDesc = (Label)e.Row.FindControl("lb_fDesc");
                Label lb_fDesc2 = (Label)e.Row.FindControl("lb_fDesc2");
                lb_fDesc2.Text = drv["equip_full_desc"].ToString();
                lb_fDesc.Text = drv["equip_full_desc"].ToString();
                if (lb_fDesc.Text.Length > 30)
                {
                    lb_fDesc.Text = lb_fDesc.Text.Substring(0, 30) + "...";
                    lb_fDesc.ToolTip = drv["equip_full_desc"].ToString();
                }

                Label lb_addDesc = (Label)e.Row.FindControl("lb_addDesc");
                lb_addDesc.Text = drv["equip_add_desc"].ToString() + " " + drv["equip_standard_drawing"].ToString();
                if (lb_addDesc.Text.Length > 30)
                {
                    lb_addDesc.Text = lb_addDesc.Text.ToString().Substring(0, 30) + "...";
                    lb_addDesc.ToolTip = drv["equip_add_desc"].ToString() + " " + drv["equip_standard_drawing"].ToString();
                }
                //TextBox tb_stdDr = (TextBox)e.Row.FindControl("tb_stdDr");
                //tb_stdDr.Text = drv["equip_standard_drawing"].ToString();
                Label lb_reqQTY = (Label)e.Row.FindControl("lb_reqQTY");
                lb_reqQTY.Text = drv["equip_qty"].ToString();
                Label lb_unit = (Label)e.Row.FindControl("lb_unit");
                lb_unit.Text = drv["equip_unit"].ToString();
                Label lb_revQTY = (Label)e.Row.FindControl("lb_revQTY");
                Label conrevQTY = (Label)e.Row.FindControl("conrevQTY");
                Label lb_bidQTY = (Label)e.Row.FindControl("lb_bidQTY");
                Label lb_inco = (Label)e.Row.FindControl("lb_inco");
                lb_inco.Text = drv["equip_incoterm"].ToString();
                //ListBox ddl_inco = (ListBox)e.Row.FindControl("ddl_inco");
                //DataTable dtinco = new DataTable();
                //dtinco = objJobDb.getDDLIncoterm();
                //ddl_inco.Items.Clear();
                //ddl_inco.DataSource = dtinco;
                //ddl_inco.DataTextField = "incoterm_name";
                //ddl_inco.DataValueField = "incoterm_code";
                //ddl_inco.DataBind();
                //foreach (ListItem item in ddl_inco.Items)
                //{
                //    if (drv["equip_incoterm"].ToString().IndexOf(item.Value) > -1)
                //    {
                //        item.Selected = true;
                //    }
                //}
                //ListBox ddl_wa = (ListBox)e.Row.FindControl("ddl_wa");
                //ddl_wa.Items.Clear();
                //ddl_wa.Items.Insert(0, new ListItem("Factory Test", "1"));
                //ddl_wa.Items.Insert(1, new ListItem("Manual Test", "2"));
                //ddl_wa.Items.Insert(2, new ListItem("Routine Test", "3"));
                string wa = "";
                if (drv["factory_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[0].Selected = true;
                    wa = "factory";
                }
                if (drv["manual_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[1].Selected = true;
                    wa += " manual";
                }
                if (drv["routine_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[2].Selected = true;
                    wa += " routine";
                }
                if (drv["waive_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[2].Selected = true;
                    wa += " waive";
                }
                Label lb_wa = (Label)e.Row.FindControl("lb_wa");
                lb_wa.Text = wa;
                LinkButton lnk_res = (LinkButton)e.Row.FindControl("lnk_res");
                //DropDownList ddl_cur = (DropDownList)e.Row.FindControl("ddl_cur");
                //ddl_cur.Items.Clear();
                //ddl_cur.Items.Insert(0, new ListItem("THB", "THB"));
                //ddl_cur.Items.Insert(1, new ListItem("USD", "USD"));
                //ddl_cur.Items.Insert(2, new ListItem("EUR", "EUR"));
                //ddl_cur.Items.Insert(3, new ListItem("JPY", "JPY"));
                //ddl_cur.Items.Insert(4, new ListItem("CNY", "CNY"));
                Label lb_cur = (Label)e.Row.FindControl("lb_cur");
                lb_cur.Text = drv["currency"].ToString();

                Label lb_supUnit = (Label)e.Row.FindControl("lb_supUnit");
                if (drv["supply_equip_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["supply_equip_unit_price"]);
                    lb_supUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_supAmou = (Label)e.Row.FindControl("lb_supAmou");
                if (drv["supply_equip_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["supply_equip_amonut"]);
                    lb_supAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localSUnit = (Label)e.Row.FindControl("lb_localSUnit");
                if (drv["local_exwork_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_exwork_unit_price"]);
                    lb_localSUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localSAmou = (Label)e.Row.FindControl("lb_localSAmou");
                if (drv["local_exwork_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_exwork_amonut"]);
                    lb_localSAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localTUnit = (Label)e.Row.FindControl("lb_localTUnit");
                if (drv["local_tran_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_tran_unit_price"]);
                    lb_localTUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localTAmou = (Label)e.Row.FindControl("lb_localTAmou");
                if (drv["local_tran_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_tran_amonut"]);
                    lb_localTAmou.Text = String.Format("{0:0.00}", val);
                }
                //Label lb_supUnit = (Label)e.Row.FindControl("lb_supUnit");
                //lb_supUnit.Text = drv["supply_equip_unit_price"].ToString();
                //Label lb_supAmou = (Label)e.Row.FindControl("lb_supAmou");
                //lb_supAmou.Text = drv["supply_equip_amonut"].ToString();
                //Label lb_localSUnit = (Label)e.Row.FindControl("lb_localSUnit");
                //lb_localSUnit.Text = drv["local_exwork_unit_price"].ToString();
                //Label lb_localSAmou = (Label)e.Row.FindControl("lb_localSAmou");
                //lb_localSAmou.Text = drv["local_exwork_amonut"].ToString();

                //Label lb_localTUnit = (Label)e.Row.FindControl("lb_localTUnit");
                //lb_localTUnit.Text = drv["local_tran_unit_price"].ToString();
                //Label lb_localTAmou = (Label)e.Row.FindControl("lb_localTAmou");
                //lb_localTAmou.Text = drv["local_tran_amonut"].ToString();

                Label lb_hide = (Label)e.Row.FindControl("lb_hide");
                lb_hide.Text = drv["rowid"].ToString();
                Label lb_jobno = (Label)e.Row.FindControl("lb_jobno");
                lb_jobno.Text = drv["job_no"].ToString();
                Label lb_scheno = (Label)e.Row.FindControl("lb_scheno");
                lb_scheno.Text = drv["schedule_no"].ToString();
                Label lb_bidno = (Label)e.Row.FindControl("lb_bidno");
                lb_bidno.Text = drv["bid_no"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                if (drv["legend"].ToString() != "") lb_legend.Text = "(" + drv["legend"].ToString() + ")";

                Label lb_doc = (Label)e.Row.FindControl("lb_doc");
                DataTable dtdoc = objJobDb.getListDoc(lb_codeNo.Text, drv["depart_position_name"].ToString(), drv["section_position_name"].ToString());
                 //DataTable dtdoc = objJobDb.getListDoc("CVC3SWG03", "กวอ-ส.", "หวอ-ส.");
                foreach (DataRow dr in dtdoc.Rows)
                {
                    if (lb_doc.Text == "") lb_doc.Text = "-" + dr["doc_name"].ToString();
                    else lb_doc.Text += "<br/>-" + dr["doc_name"].ToString();
                }
            }
        }

        protected void bind_equiList(string path, string bid, string sche, string flag, DataTable myDT)
        {
            if (flag == "select")
            {
                divTabEqui.Controls.Clear();
                ViewState["GetAllDT"] = myDT;
                DataTable getTab = new DataTable();
                getTab = objJobDb.getTabEqui(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text);
                for (int i = 0; i < getTab.Rows.Count; i++)
                {

                    DataTable dt2 = new DataTable();
                    dt2.Columns.Add(" ");
                    //dt2.Columns.Add("No.");
                    dt2.Columns.Add("Meterial Group");
                    dt2.Columns.Add("Code No.");
                    dt2.Columns.Add("Item No.");
                    dt2.Columns.Add("Full Description");
                    dt2.Columns.Add("Additional Description");
                    dt2.Columns.Add("Standard Drawing/Ref No.");
                    dt2.Columns.Add("Required QTY");
                    dt2.Columns.Add("Unit");
                    dt2.Columns.Add("Reserved QTY");
                    dt2.Columns.Add("Confirmed Reservation QTY");
                    dt2.Columns.Add("Bid QTY");
                    dt2.Columns.Add("Stock Availability");
                    dt2.Columns.Add("Status");
                    dt2.Columns.Add("Incoterms");
                    dt2.Columns.Add("Waive Test");
                    dt2.Columns.Add("Related Document");


                    dt2.Columns.Add("Currency");
                    dt2.Columns.Add("Head1");
                    dt2.Columns.Add("Head2");
                    dt2.Columns.Add("Head3");
                    dt2.Columns.Add("Head4");
                    dt2.Columns.Add("Head5");
                    dt2.Columns.Add("Head6");
                    dt2.Columns.Add("  ");
                    dt2.Columns.Add("hide");
                    dt2.Columns.Add("jobno");
                    dt2.Columns.Add("scheno");
                    dt2.Columns.Add("bidno");
                    dt2.Columns.Add("parts");
                    dt2.Columns.Add("ft");
                    dt2.Columns.Add("mt");
                    dt2.Columns.Add("rt");
                    Table tb = new Table();
                    TableRow tbRow = new TableRow();
                    TableCell tbCell = new TableCell();
                    TableRow tbRow2 = new TableRow();
                    TableCell tbCell2 = new TableCell();
                    TableRow tbRow3 = new TableRow();
                    TableCell tbCell3 = new TableCell();
                    DataTable dtPth = new DataTable();
                    System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divMain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divAdd = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divAbso = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");

                    divMain.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    tb.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    Label lb = new Label();
                    lb.ID = "bidNo" + i;
                    lb.Text = getTab.Rows[i]["bid_no"].ToString();
                    Label lb2 = new Label();
                    lb2.Text = getTab.Rows[i]["schedule_name"].ToString();
                    Label lbDetail = new Label();
                    lb2.ID = "SchNo" + i;
                    lbDetail.Text = "bid No. " + lb.Text + " | " + "Schedule " + lb2.Text;
                    Label lb3 = new Label();
                    lb3.Text = getTab.Rows[i]["schedule_no"].ToString();
                    Label lb4 = new Label();
                    lb4.Text = getTab.Rows[i]["bid_revision"].ToString();
                    createDiv.ID = "Div" + i;
                    divAdd.ID = "DivGv" + i;
                    divAdd.Attributes.Add("runat", "server");
                    divAdd.Attributes.Add("style", "overflow:auto;height:300px;position:relative;");
                    //createDiv.Style.Add("background-color", "#c4d5f6");
                    createDiv.Attributes.Add("style", "height:30px;padding-top:0.5%;background-color:#c4d5f6;cursor:pointer");
                    divMain.Attributes.Add("onclick", "javascript:openTabEquiSub(" + divAdd.ClientID + ");");
                    createDiv.Controls.Add(lbDetail);
                    divAdd.Style["display"] = "none";
                    divAbso.ID = "DivAbso" + i;
                    divAbso.Attributes.Add("runat", "server");
                    divAbso.Attributes.Add("style", "position: absolute;");
                    tbCell.Controls.Add(createDiv);
                    tbRow.Controls.Add(tbCell);
                    tb.Controls.Add(tbRow);

                    DataView dv = new DataView(myDT);
                    if (lb.Text == bid && sche == lb3.Text && (path != "" && bid != "" && sche != ""))
                    {
                        //dtGV = objDB.getGVSelPath(hidDept.Value, hidSect.Value, bid, sche, path);
                        dv.RowFilter = "bidno='" + getTab.Rows[i]["bid_no"].ToString() + "' and scheno='" + getTab.Rows[i]["schedule_no"].ToString() + "'" +
                            "and [parts]='" + path + "'";
                        path = ""; sche = ""; path = "";
                    }
                    else
                    {
                        //dtGV = objDB.getGV(hidDept.Value, hidSect.Value, lb.Text, lb2.Text);
                        dv.RowFilter = "bidno='" + getTab.Rows[i]["bid_no"].ToString() + "' and scheno='" + getTab.Rows[i]["schedule_no"].ToString() + "'";
                    }
                    //dv.RowFilter = "jobno='"+ getTab.Rows[i]["job_no"].ToString() + "' and scheno='"+ getTab.Rows[i]["schedule_no"].ToString() + "'";
                    for (int j = 0; j < dv.Count; j++)
                    {

                        DataRow dr = dt2.NewRow();
                        dr[" "] = "";
                        dr["Meterial Group"] = dv[j]["Meterial Group"].ToString();
                        dr["Code No."] = "";
                        dr["Item No."] = dv[j]["Item No."].ToString();
                        dr["Full Description"] = dv[j]["Full Description"].ToString();
                        dr["Additional Description"] = dv[j]["Additional Description"].ToString();
                        dr["Standard Drawing/Ref No."] = dv[j]["Standard Drawing/Ref No."].ToString();
                        dr["Required QTY"] = dv[j]["Required QTY"].ToString();
                        dr["Unit"] = "";
                        dr["Reserved QTY"] = "";
                        dr["Confirmed Reservation QTY"] = "";
                        dr["Bid QTY"] = "";
                        dr["Stock Availability"] = "";
                        dr["Status"] = "";
                        dr["Incoterms"] = dv[j]["Incoterms"].ToString();
                        dr["Waive Test"] = "";

                        dr["ft"] = dv[j]["ft"].ToString();
                        dr["mt"] = dv[j]["mt"].ToString();
                        dr["rt"] = dv[j]["rt"].ToString();

                        dr["Related Document"] = "";


                        dr["Currency"] = "";
                        dr["Head1"] = "";
                        dr["Head2"] = "";
                        dr["Head3"] = "";
                        dr["Head4"] = "";
                        dr["Head5"] = "";
                        dr["Head6"] = "";
                        dr["  "] = "";
                        dr["hide"] = dv[j]["hide"].ToString();
                        dr["jobno"] = dv[j]["jobno"].ToString();
                        dr["scheno"] = dv[j]["scheno"].ToString();
                        dr["bidno"] = dv[j]["bidno"].ToString();
                        dr["bidno"] = dv[j]["bidno"].ToString();
                        dr["parts"] = dv[j]["parts"].ToString();
                        dt2.Rows.Add(dr);
                    }
                    GridView gv = new GridView();
                    gv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    if (dt2.Rows.Count > 0)//
                    {
                        gv.ID = "GV" + i;
                        gv.Attributes.Add("runat", "server");
                        gv.RowDataBound += new GridViewRowEventHandler(GV0_RowDataBound);
                        gv.DataSource = dt2;
                        gv.DataBind();

                    }
                    divAdd.Controls.Add(divAbso);
                    divAbso.Controls.Add(gv);

                    //tbCell2.Controls.Add(divAdd);
                    //tbRow2.Controls.Add(tbCell2);
                    //tb.Controls.Add(tbRow2);

                    //dtPth = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);
                    for (int z = 0; z < dtPth.Rows.Count; z++)
                    {
                        Button btn = new Button();
                        btn.Text = dtPth.Rows[z]["part_name"].ToString();
                        btn.CommandName = lb.Text + "," + lb3.Text + "," + lb2.Text + "," + lb4.Text;
                        if (i >= 10) btn.ID = z + dtPth.Rows[z]["part_name"].ToString() + (i).ToString();
                        else btn.ID = dtPth.Rows[z]["part_name"].ToString() + (i).ToString();
                        btn.Attributes.Add("runat", "server");
                        btn.Click += new EventHandler(btn_Add);

                        if (z == 0)
                        {
                            Label lb_path = new Label();
                            lb_path.Text = "Part:   ";
                            divAbso.Controls.Add(lb_path);
                        }
                        divAbso.Controls.Add(btn);
                    }
                    Label lb_ln = new Label();
                    lb_ln.Text = "<br />";
                    divAbso.Controls.Add(lb_ln);
                    Button bt_addPart = new Button();
                    bt_addPart.Text = "Add Part";
                    bt_addPart.ID = "bt_addPart" + i;
                    bt_addPart.Attributes.Add("runat", "server");
                    bt_addPart.CssClass = "btn btn-primary";
                    bt_addPart.OnClientClick = "openPopup('5'); return false;";
                    divAbso.Controls.Add(bt_addPart);
                    Label lb_space = new Label();
                    lb_space.Text = " ";
                    divAbso.Controls.Add(lb_space);
                    Button bt_save = new Button();
                    bt_save.Text = "Save";
                    bt_save.ID = i.ToString();
                    bt_save.Attributes.Add("runat", "server");
                    bt_save.CssClass = "btn btn-primary";
                    bt_save.Click += new EventHandler(bt_saveEq);
                    divAbso.Controls.Add(bt_save);

                    divMain.Controls.Add(tb);

                    divTabEqui.Controls.Add(divMain);
                    divTabEqui.Controls.Add(divAdd);
                    divTabEqui.Style["display"] = "block";
                }
            }
            else if (flag == "history")
            {
                string JobnoHis = "";
                if (Multiselect_HisJob.SelectedValue != "") JobnoHis = Multiselect_HisJob.SelectedItem.Text;
                divTabEqui.Controls.Clear();
                DataTable dt = new DataTable();
                dt = objJobDb.getTabEqui(hidDept.Value, hidSect.Value, JobnoHis, txtHisRev.Text);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //System.Web.UI.HtmlControls.HtmlGenericControl divList = (System.Web.UI.HtmlControls.HtmlGenericControl)this.FindControl(div);
                        Table tb = new Table();
                        TableRow tbRow = new TableRow();
                        TableCell tbCell = new TableCell();
                        TableRow tbRow2 = new TableRow();
                        TableCell tbCell2 = new TableCell();
                        TableRow tbRow3 = new TableRow();
                        TableCell tbCell3 = new TableCell();

                        DataTable dtPth = new DataTable();
                        System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        System.Web.UI.HtmlControls.HtmlGenericControl divMain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        System.Web.UI.HtmlControls.HtmlGenericControl divAdd = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        System.Web.UI.HtmlControls.HtmlGenericControl divAbso = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        //divAdd.Controls.Clear();
                        divMain.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        tb.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        Label lb = new Label();
                        lb.ID = "bidNo" + i;
                        lb.Text = dt.Rows[i]["bid_no"].ToString();
                        Label lb2 = new Label();
                        lb2.Text = dt.Rows[i]["schedule_name"].ToString();
                        Label lbDetail = new Label();
                        lb2.ID = "SchNo" + i;
                        if (lb.Text != "" || lb2.Text != "") lbDetail.Text = "Bid No. " + lb.Text + " | " + "Schedule " + lb2.Text;
                        else lbDetail.Text = "Other";
                        Label lb3 = new Label();
                        lb3.Text = dt.Rows[i]["schedule_no"].ToString();
                        Label lb4 = new Label();
                        lb4.Text = dt.Rows[i]["bid_revision"].ToString();
                        createDiv.ID = "Div" + i;
                        divAdd.ID = "DivGv" + i;
                        divAdd.Attributes.Add("runat", "server");
                        divAdd.Attributes.Add("style", "overflow:auto;height:300px;position:relative;");
                        //createDiv.Style.Add("background-color", "#c4d5f6");
                        createDiv.Attributes.Add("style", "height:30px;padding-top:0.5%;background-color:#c4d5f6;cursor:pointer");
                        divMain.Attributes.Add("onclick", "javascript:openTabEquiSub(" + divAdd.ClientID + ");");
                        createDiv.Controls.Add(lbDetail);
                        divAdd.Style["display"] = "none";
                        divAbso.ID = "DivAbso" + i;
                        divAbso.Attributes.Add("runat", "server");
                        divAbso.Attributes.Add("style", "position: absolute;");
                        tbCell.Controls.Add(createDiv);
                        tbRow.Controls.Add(tbCell);
                        tb.Controls.Add(tbRow);

                        Label lb_bidnoeq = new Label();
                        lb_bidnoeq.Text = "Bid No.:";
                        TextBox tb_bidnoeq = new TextBox();

                        Label lb_reveq = new Label();
                        lb_reveq.Text = "Rev No.:";
                        TextBox tb_reveq = new TextBox();

                        Label lb_eq = new Label();
                        lb_eq.Text = "Equipment ";
                        TextBox tb_eq = new TextBox();

                        //divAdd.Controls.Add(lb_bidnoeq);
                        //divAdd.Controls.Add(tb_bidnoeq);
                        //divAdd.Controls.Add(lb_reveq);
                        //divAdd.Controls.Add(tb_reveq);
                        //divAdd.Controls.Add(lb_eq);
                        //divAdd.Controls.Add(tb_eq);
                        //divAdd.Controls.Add(newLine);
                        //Label lb_eq = new Label();
                        //lb_eq.Text = "Equipment ";
                        //TextBox tb_eq = new TextBox();


                        DataTable dtGV = new DataTable();
                        dtGV = objJobDb.getGV(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);

                        //if (lb.Text == bid && sche == lb2.Text && (path != "" && bid != "" && sche != ""))
                        //{
                        //    dtGV = objDB.getGVSelPath(hidDept.Value, hidSect.Value, bid, sche, path);
                        //    path = ""; sche = ""; path = "";
                        //}
                        //else
                        //{
                        //    dtGV = objDB.getGV(hidDept.Value, hidSect.Value, lb.Text, lb2.Text);
                        //}
                        DataTable dt2 = new DataTable();
                        GridView gv = new GridView();
                        gv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        //dom
                        //for (int k = 0; k < dtGV.Columns.Count; k++)
                        //{
                        //    dt2.Columns.Add(dtGV.Columns[k].ColumnName);
                        //}
                        //for (int o = 0; o < dtGV.Rows.Count; o++)
                        //{
                        //    DataRow dr = dt2.NewRow();
                        //    //dr[dt.Columns[k].ColumnName] = dt.Rows[o][dt.Columns[k].ColumnName].ToString();
                        //    for (int a = 0; a < dtGV.Columns.Count; a++)
                        //    {
                        //        for (int b = 0; b < a; b++)
                        //        {
                        //            dr[dtGV.Columns[a].ColumnName] = dtGV.Rows[o][dtGV.Columns[a].ColumnName].ToString();

                        //        }
                        //    }
                        //    dt2.Rows.Add(dr);
                        //}
                        //dom

                        dt2.Columns.Add(" ");
                        //dt2.Columns.Add("No.");
                        dt2.Columns.Add("Meterial Group");
                        dt2.Columns.Add("Code No.");
                        dt2.Columns.Add("Item No.");
                        dt2.Columns.Add("Full Description");
                        dt2.Columns.Add("Additional Description");
                        dt2.Columns.Add("Standard Drawing/Ref No.");
                        dt2.Columns.Add("Required QTY");
                        dt2.Columns.Add("Unit");
                        dt2.Columns.Add("Reserved QTY");
                        dt2.Columns.Add("Confirmed Reservation QTY");
                        dt2.Columns.Add("Bid QTY");
                        dt2.Columns.Add("Stock Availability");
                        dt2.Columns.Add("Status");
                        dt2.Columns.Add("Incoterms");
                        dt2.Columns.Add("Waive Test");
                        dt2.Columns.Add("Related Document");


                        dt2.Columns.Add("Currency");
                        dt2.Columns.Add("Head1");
                        dt2.Columns.Add("Head2");
                        dt2.Columns.Add("Head3");
                        dt2.Columns.Add("Head4");
                        dt2.Columns.Add("Head5");
                        dt2.Columns.Add("Head6");
                        dt2.Columns.Add("  ");
                        dt2.Columns.Add("hide");
                        dt2.Columns.Add("jobno");
                        dt2.Columns.Add("scheno");
                        dt2.Columns.Add("bidno");
                        dt2.Columns.Add("parts");
                        dt2.Columns.Add("ft");
                        dt2.Columns.Add("mt");
                        dt2.Columns.Add("rt");
                        //dt2.Columns.Add("Del", typeof(System.Web.UI.WebControls.Button));

                        //System.Web.UI.HtmlControls.HtmlButton

                        for (int y = 0; y < dtGV.Rows.Count; y++)
                        {
                            //dtGV.Rows[y]["equip_add_desc"].ToString();
                            DataRow dr = dt2.NewRow();
                            dr[" "] = "";
                            //dr["No."] = dtGV.Rows[y]["rowid"].ToString();
                            dr["Meterial Group"] = dtGV.Rows[y]["equip_mas_grp"].ToString();
                            dr["Code No."] = dtGV.Rows[y]["equip_code"].ToString();
                            dr["Item No."] = dtGV.Rows[y]["equip_item_no"].ToString();
                            dr["Full Description"] = dtGV.Rows[y]["equip_full_desc"].ToString();
                            dr["Additional Description"] = dtGV.Rows[y]["equip_add_desc"].ToString();
                            dr["Standard Drawing/Ref No."] = dtGV.Rows[y]["equip_standard_drawing"].ToString();
                            dr["Required QTY"] = dtGV.Rows[y]["equip_qty"].ToString();
                            dr["Unit"] = dtGV.Rows[y]["equip_unit"].ToString();
                            dr["Reserved QTY"] = "";
                            dr["Confirmed Reservation QTY"] = "";
                            dr["Bid QTY"] = dtGV.Rows[y]["equip_bid_qty"].ToString();
                            dr["Stock Availability"] = "";
                            dr["Status"] = "";
                            dr["Incoterms"] = dtGV.Rows[y]["equip_incoterm"].ToString();
                            if (dtGV.Rows[y]["factory_test"].ToString() == "True")
                            {
                                dr["ft"] = "1";
                            }
                            if (dtGV.Rows[y]["manual_test"].ToString() == "True")
                            {
                                dr["mt"] = "1";
                            }
                            if (dtGV.Rows[y]["routine_test"].ToString() == "True")
                            {
                                dr["rt"] = "1";
                            }

                            dr["Related Document"] = "";


                            dr["Currency"] = dtGV.Rows[y]["currency"].ToString();
                            dr["Head1"] = dtGV.Rows[y]["supply_equip_unit_price"].ToString();
                            dr["Head2"] = dtGV.Rows[y]["supply_equip_amonut"].ToString();
                            dr["Head3"] = dtGV.Rows[y]["local_exwork_unit_price"].ToString();
                            dr["Head4"] = dtGV.Rows[y]["local_exwork_amonut"].ToString();
                            dr["Head5"] = dtGV.Rows[y]["local_tran_unit_price"].ToString();
                            dr["Head6"] = dtGV.Rows[y]["local_tran_amonut"].ToString();
                            dr["  "] = "";
                            dr["hide"] = dtGV.Rows[y]["rowid"].ToString();
                            dr["jobno"] = dtGV.Rows[y]["job_no"].ToString();
                            dr["scheno"] = dtGV.Rows[y]["schedule_no"].ToString();
                            dr["bidno"] = dtGV.Rows[y]["bid_no"].ToString();
                            dr["parts"] = dtGV.Rows[y]["path_code"].ToString();
                            dt2.Rows.Add(dr);
                        }

                        //int j = dt2.Columns.Count;
                        //DataRow row;
                        //foreach (DataColumn cl in dt2.Columns)
                        //{
                        //    row = dt2.NewRow();

                        //    for (int o = 0; o < dt.Rows.Count; o++)
                        //    {
                        //        row[o] = dt.Rows[o][dt.Columns[o].ColumnName].ToString();
                        //    }
                        //    dt2.Rows.Add(row);
                        //}
                        //for (int o = 0; o < dt.Rows.Count; o++)
                        //{
                        //    DataRow dr = dt2.NewRow();
                        //    dt2.Rows.Add(dr);
                        //}

                        //for (int j = 0; j < dt.Rows.Count; j++)
                        //{
                        //    DataRow dr = dt2.NewRow();
                        //    dr[dt.Columns[j = 0].ColumnName] = dt.Rows[j][dt.Columns[j].ColumnName].ToString();
                        //}

                        //dt2.Rows.Add(dr);



                        //DataTable dt2 = new DataTable();
                        //dt2.Columns.Add("test1");
                        //dt2.Columns.Add("test2");
                        //DataRow dr = dt2.NewRow();
                        //dr["test1"] = "1"+i;
                        //dr["test2"] = "11115"+i;
                        //dt2.Rows.Add(dr);
                        //GridView gv = new GridView();
                        //for (int j = 0; j < 22; j++)
                        //{
                        //    TemplateField tfield = new TemplateField();
                        //    tfield.HeaderText = "Country"+j;
                        //    gv.Columns.Add(tfield);
                        //}

                        if (dt2.Rows.Count > 0)//
                        {
                            gv.ID = "GV" + i;
                            gv.Attributes.Add("runat", "server");
                            gv.RowDataBound += new GridViewRowEventHandler(GV0_RowDataBound);
                            gv.DataSource = dt2;
                            gv.DataBind();
                            //string myID = gv.ID; 
                            //GridView thisgv = (GridView)this.FindControl("GV0");
                            //if (thisgv!=null)
                            //{

                            //}
                            //GridView gridveiw1 = (GridView)this.FindControl("GV0");
                            //if (gridveiw1!=null)
                            //{

                            //}
                            //System.Web.UI.WebControls.GridView gvf = (System.Web.UI.WebControls.GridView)this.FindControl("GV0");
                            //if (gvf!=null)
                            //{

                            //}

                        }
                        divAdd.Controls.Add(divAbso);
                        divAbso.Controls.Add(gv);

                        //tbCell2.Controls.Add(divAdd);
                        //tbRow2.Controls.Add(tbCell2);
                        //tb.Controls.Add(tbRow2);

                        //dtPth = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);
                        for (int z = 0; z < dtPth.Rows.Count; z++)
                        {
                            Button btn = new Button();
                            btn.Text = dtPth.Rows[z]["part_name"].ToString();
                            btn.CommandName = lb.Text + "," + lb3.Text + "," + lb2.Text + "," + lb4.Text;
                            if (i >= 10) btn.ID = z + dtPth.Rows[z]["part_name"].ToString() + (i).ToString();
                            else btn.ID = dtPth.Rows[z]["part_name"].ToString() + (i).ToString();
                            btn.Attributes.Add("runat", "server");
                            btn.Click += new EventHandler(btn_Add);

                            if (z == 0)
                            {
                                Label lb_path = new Label();
                                lb_path.Text = "Part:   ";
                                divAbso.Controls.Add(lb_path);
                            }
                            divAbso.Controls.Add(btn);
                        }
                        Label lb_ln = new Label();
                        lb_ln.Text = "<br />";
                        divAbso.Controls.Add(lb_ln);
                        Button bt_addPart = new Button();
                        bt_addPart.Text = "Add Part";
                        bt_addPart.ID = "bt_addPart" + i;
                        bt_addPart.Attributes.Add("runat", "server");
                        bt_addPart.CssClass = "btn btn-primary";
                        bt_addPart.OnClientClick = "openPopup('5'); return false;";
                        divAbso.Controls.Add(bt_addPart);
                        Label lb_space = new Label();
                        lb_space.Text = " ";
                        divAbso.Controls.Add(lb_space);
                        Button bt_save = new Button();
                        bt_save.Text = "Save";
                        bt_save.ID = i.ToString();
                        bt_save.Attributes.Add("runat", "server");
                        bt_save.CssClass = "btn btn-primary";
                        bt_save.Click += new EventHandler(bt_saveEq);
                        divAbso.Controls.Add(bt_save);

                        //tbCell2.Controls.Add(divAdd);
                        //tbRow2.Controls.Add(tbCell2);
                        //tb.Controls.Add(tbRow2);

                        //tbRow3.Controls.Add(tbCell3);
                        //tb.Controls.Add(tbRow3);
                        divMain.Controls.Add(tb);

                        divTabEqui.Controls.Add(divMain);
                        divTabEqui.Controls.Add(divAdd);
                        //for (int iGV = 0; iGV < divAdd.Controls.Count; iGV++)
                        //{
                        //    try
                        //    {
                        //        GridView gvEq = (GridView)divAdd.Controls[iGV];
                        //        foreach (GridViewRow gvRow in gvEq.Rows)
                        //        {
                        //            TextBox txt = (TextBox)gvRow.FindControl("");

                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        LogHelper.WriteEx(ex);
                        //    }
                        //}
                    }
                }
                else
                {
                    Table tb = new Table();
                    TableRow tbRow = new TableRow();
                    TableCell tbCell = new TableCell();
                    TableRow tbRow2 = new TableRow();
                    TableCell tbCell2 = new TableCell();
                    TableRow tbRow3 = new TableRow();
                    TableCell tbCell3 = new TableCell();

                    DataTable dtPth = new DataTable();
                    System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divMain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divAdd = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divAbso = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    //divAdd.Controls.Clear();
                    divMain.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    tb.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    Label lb = new Label();
                    lb.ID = "bidNo0";
                    lb.Text = "Other";
                    Label lb2 = new Label();
                    lb2.Text = "Other";
                    Label lbDetail = new Label();
                    lb2.ID = "SchNo0";
                    //lbDetail.Text = "Bid No. " + lb.Text + " | " + "Schedule " + lb2.Text;
                    lbDetail.Text = "Other";
                    Label lb3 = new Label();
                    lb3.Text = "";// dt.Rows[i]["schedule_no"].ToString();
                    Label lb4 = new Label();
                    lb4.Text = "";
                    createDiv.ID = "Div0";
                    divAdd.ID = "DivGv0";
                    divAdd.Attributes.Add("runat", "server");
                    divAdd.Attributes.Add("style", "overflow:auto;height:300px;position:relative;");
                    //createDiv.Style.Add("background-color", "#c4d5f6");
                    createDiv.Attributes.Add("style", "height:30px;padding-top:0.5%;background-color:#c4d5f6;cursor:pointer");
                    divMain.Attributes.Add("onclick", "javascript:openTabEquiSub(" + divAdd.ClientID + ");");
                    createDiv.Controls.Add(lbDetail);
                    divAdd.Style["display"] = "none";
                    divAbso.ID = "DivAbso0";
                    divAbso.Attributes.Add("runat", "server");
                    divAbso.Attributes.Add("style", "position: absolute;");
                    tbCell.Controls.Add(createDiv);
                    tbRow.Controls.Add(tbCell);
                    tb.Controls.Add(tbRow);

                    Label lb_bidnoeq = new Label();
                    lb_bidnoeq.Text = "Bid No.:";
                    TextBox tb_bidnoeq = new TextBox();

                    Label lb_reveq = new Label();
                    lb_reveq.Text = "Rev No.:";
                    TextBox tb_reveq = new TextBox();

                    Label lb_eq = new Label();
                    lb_eq.Text = "Equipment ";
                    TextBox tb_eq = new TextBox();



                    DataTable dtGV = new DataTable();
                    dtGV = objJobDb.getGV(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);

                    DataTable dt2 = new DataTable();
                    GridView gv = new GridView();
                    gv.Style.Add(HtmlTextWriterStyle.Width, "100%");


                    dt2.Columns.Add(" ");
                    //dt2.Columns.Add("No.");
                    dt2.Columns.Add("Meterial Group");
                    dt2.Columns.Add("Code No.");
                    dt2.Columns.Add("Item No.");
                    dt2.Columns.Add("Full Description");
                    dt2.Columns.Add("Additional Description");
                    dt2.Columns.Add("Standard Drawing/Ref No.");
                    dt2.Columns.Add("Required QTY");
                    dt2.Columns.Add("Unit");
                    dt2.Columns.Add("Reserved QTY");
                    dt2.Columns.Add("Confirmed Reservation QTY");
                    dt2.Columns.Add("Bid QTY");
                    dt2.Columns.Add("Stock Availability");
                    dt2.Columns.Add("Status");
                    dt2.Columns.Add("Incoterms");
                    dt2.Columns.Add("Waive Test");
                    dt2.Columns.Add("Related Document");


                    dt2.Columns.Add("Currency");
                    dt2.Columns.Add("Head1");
                    dt2.Columns.Add("Head2");
                    dt2.Columns.Add("Head3");
                    dt2.Columns.Add("Head4");
                    dt2.Columns.Add("Head5");
                    dt2.Columns.Add("Head6");
                    dt2.Columns.Add("  ");
                    dt2.Columns.Add("hide");
                    dt2.Columns.Add("jobno");
                    dt2.Columns.Add("scheno");
                    dt2.Columns.Add("bidno");
                    dt2.Columns.Add("parts");
                    dt2.Columns.Add("ft");
                    dt2.Columns.Add("mt");
                    dt2.Columns.Add("rt");
                    //dt2.Columns.Add("Del", typeof(System.Web.UI.WebControls.Button));

                    //System.Web.UI.HtmlControls.HtmlButton

                    for (int y = 0; y < dtGV.Rows.Count; y++)
                    {
                        //dtGV.Rows[y]["equip_add_desc"].ToString();
                        DataRow dr = dt2.NewRow();
                        dr[" "] = "";
                        //dr["No."] = dtGV.Rows[y]["rowid"].ToString();
                        dr["Meterial Group"] = dtGV.Rows[y]["equip_mas_grp"].ToString();
                        dr["Code No."] = dtGV.Rows[y]["equip_code"].ToString();
                        dr["Item No."] = dtGV.Rows[y]["equip_item_no"].ToString();
                        dr["Full Description"] = dtGV.Rows[y]["equip_full_desc"].ToString();
                        dr["Additional Description"] = dtGV.Rows[y]["equip_add_desc"].ToString();
                        dr["Standard Drawing/Ref No."] = dtGV.Rows[y]["equip_standard_drawing"].ToString();
                        dr["Required QTY"] = dtGV.Rows[y]["equip_qty"].ToString();
                        dr["Unit"] = dtGV.Rows[y]["equip_unit"].ToString();
                        dr["Reserved QTY"] = "";
                        dr["Confirmed Reservation QTY"] = "";
                        dr["Bid QTY"] = dtGV.Rows[y]["equip_bid_qty"].ToString();
                        dr["Stock Availability"] = "";
                        dr["Status"] = "";
                        dr["Incoterms"] = dtGV.Rows[y]["equip_incoterm"].ToString();
                        if (dtGV.Rows[y]["factory_test"].ToString() == "True")
                        {
                            dr["ft"] = "1";
                        }
                        if (dtGV.Rows[y]["manual_test"].ToString() == "True")
                        {
                            dr["mt"] = "1";
                        }
                        if (dtGV.Rows[y]["routine_test"].ToString() == "True")
                        {
                            dr["rt"] = "1";
                        }

                        dr["Related Document"] = "";


                        dr["Currency"] = dtGV.Rows[y]["currency"].ToString();
                        dr["Head1"] = dtGV.Rows[y]["supply_equip_unit_price"].ToString();
                        dr["Head2"] = dtGV.Rows[y]["supply_equip_amonut"].ToString();
                        dr["Head3"] = dtGV.Rows[y]["local_exwork_unit_price"].ToString();
                        dr["Head4"] = dtGV.Rows[y]["local_exwork_amonut"].ToString();
                        dr["Head5"] = dtGV.Rows[y]["local_tran_unit_price"].ToString();
                        dr["Head6"] = dtGV.Rows[y]["local_tran_amonut"].ToString();
                        dr["  "] = "";
                        dr["hide"] = dtGV.Rows[y]["rowid"].ToString();
                        dr["jobno"] = dtGV.Rows[y]["job_no"].ToString();
                        dr["scheno"] = dtGV.Rows[y]["schedule_no"].ToString();
                        dr["bidno"] = dtGV.Rows[y]["bid_no"].ToString();
                        dr["parts"] = dtGV.Rows[y]["path_code"].ToString();
                        dt2.Rows.Add(dr);
                    }



                    if (dt2.Rows.Count > 0)//
                    {
                        gv.ID = "GV0";
                        gv.Attributes.Add("runat", "server");
                        gv.RowDataBound += new GridViewRowEventHandler(GV0_RowDataBound);
                        gv.DataSource = dt2;
                        gv.DataBind();


                    }
                    divAdd.Controls.Add(divAbso);
                    divAbso.Controls.Add(gv);

                    //tbCell2.Controls.Add(divAdd);
                    //tbRow2.Controls.Add(tbCell2);
                    //tb.Controls.Add(tbRow2);

                    //dtPth = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);
                    for (int z = 0; z < dtPth.Rows.Count; z++)
                    {
                        Button btn = new Button();
                        btn.Text = dtPth.Rows[z]["part_name"].ToString();
                        btn.CommandName = lb.Text + "," + lb3.Text + "," + lb2.Text + "," + lb4.Text;
                        //if (i >= 10) btn.ID = z + dtPth.Rows[z]["path_code"].ToString() + (i).ToString();
                        //else btn.ID = dtPth.Rows[z]["path_code"].ToString() + (i).ToString();
                        btn.ID = dtPth.Rows[z]["part_name"].ToString() + 0;
                        btn.Attributes.Add("runat", "server");
                        btn.Click += new EventHandler(btn_Add);

                        if (z == 0)
                        {
                            Label lb_path = new Label();
                            lb_path.Text = "Part:   ";
                            divAbso.Controls.Add(lb_path);
                        }
                        divAbso.Controls.Add(btn);
                    }
                    Label lb_ln = new Label();
                    lb_ln.Text = "<br />";
                    divAbso.Controls.Add(lb_ln);
                    Button bt_addPart = new Button();
                    bt_addPart.Text = "Add Part";
                    bt_addPart.ID = "bt_addPart";
                    bt_addPart.Attributes.Add("runat", "server");
                    bt_addPart.CssClass = "btn btn-primary";
                    bt_addPart.OnClientClick = "openPopup('5'); return false;";
                    divAbso.Controls.Add(bt_addPart);
                    Label lb_space = new Label();
                    lb_space.Text = " ";
                    divAbso.Controls.Add(lb_space);
                    Button bt_save = new Button();
                    bt_save.Text = "Save";
                    bt_save.ID = "0";
                    bt_save.Attributes.Add("runat", "server");
                    bt_save.CssClass = "btn btn-primary";
                    bt_save.Click += new EventHandler(bt_saveEq);
                    divAbso.Controls.Add(bt_save);

                    //tbCell2.Controls.Add(divAdd);
                    //tbRow2.Controls.Add(tbCell2);
                    //tb.Controls.Add(tbRow2);

                    //tbRow3.Controls.Add(tbCell3);
                    //tb.Controls.Add(tbRow3);
                    divMain.Controls.Add(tb);

                    divTabEqui.Controls.Add(divMain);
                    divTabEqui.Controls.Add(divAdd);

                }
            }
            else
            {
                divTabEqui.Controls.Clear();
                DataTable dt = new DataTable();
                dt = objJobDb.getTabEqui(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text);
                if (dt.Rows.Count > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //System.Web.UI.HtmlControls.HtmlGenericControl divList = (System.Web.UI.HtmlControls.HtmlGenericControl)this.FindControl(div);
                        Table tb = new Table();
                        TableRow tbRow = new TableRow();
                        TableCell tbCell = new TableCell();
                        TableRow tbRow2 = new TableRow();
                        TableCell tbCell2 = new TableCell();
                        TableRow tbRow3 = new TableRow();
                        TableCell tbCell3 = new TableCell();

                        DataTable dtPth = new DataTable();
                        System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        System.Web.UI.HtmlControls.HtmlGenericControl divMain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        System.Web.UI.HtmlControls.HtmlGenericControl divAdd = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        System.Web.UI.HtmlControls.HtmlGenericControl divAbso = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                        //divAdd.Controls.Clear();
                        divMain.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        tb.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        Label lb = new Label();
                        lb.ID = "bidNo" + i;
                        lb.Text = dt.Rows[i]["bid_no"].ToString();
                        Label lb2 = new Label();
                        lb2.Text = dt.Rows[i]["schedule_name"].ToString();
                        Label lbDetail = new Label();
                        lb2.ID = "SchNo" + i;
                        if (lb.Text != "" || lb2.Text != "") lbDetail.Text = "Bid No. " + lb.Text + " | " + "Schedule " + lb2.Text;
                        else lbDetail.Text = "Other";
                        Label lb3 = new Label();
                        lb3.Text = dt.Rows[i]["schedule_no"].ToString();
                        Label lb4 = new Label();
                        lb4.Text = dt.Rows[i]["bid_revision"].ToString();
                        createDiv.ID = "Div" + i;
                        divAdd.ID = "DivGv" + i;
                        divAdd.Attributes.Add("runat", "server");
                        divAdd.Attributes.Add("style", "overflow:auto;height:300px;position:relative;");
                        //createDiv.Style.Add("background-color", "#c4d5f6");
                        createDiv.Attributes.Add("style", "height:30px;padding-top:0.5%;background-color:#c4d5f6;cursor:pointer");
                        divMain.Attributes.Add("onclick", "javascript:openTabEquiSub(" + divAdd.ClientID + ");");
                        createDiv.Controls.Add(lbDetail);
                        divAdd.Style["display"] = "none";
                        divAbso.ID = "DivAbso" + i;
                        divAbso.Attributes.Add("runat", "server");
                        divAbso.Attributes.Add("style", "position: absolute;");
                        tbCell.Controls.Add(createDiv);
                        tbRow.Controls.Add(tbCell);
                        tb.Controls.Add(tbRow);

                        Label lb_bidnoeq = new Label();
                        lb_bidnoeq.Text = "Bid No.:";
                        TextBox tb_bidnoeq = new TextBox();

                        Label lb_reveq = new Label();
                        lb_reveq.Text = "Rev No.:";
                        TextBox tb_reveq = new TextBox();

                        Label lb_eq = new Label();
                        lb_eq.Text = "Equipment ";
                        TextBox tb_eq = new TextBox();

                        //divAdd.Controls.Add(lb_bidnoeq);
                        //divAdd.Controls.Add(tb_bidnoeq);
                        //divAdd.Controls.Add(lb_reveq);
                        //divAdd.Controls.Add(tb_reveq);
                        //divAdd.Controls.Add(lb_eq);
                        //divAdd.Controls.Add(tb_eq);
                        //divAdd.Controls.Add(newLine);
                        //Label lb_eq = new Label();
                        //lb_eq.Text = "Equipment ";
                        //TextBox tb_eq = new TextBox();


                        DataTable dtGV = new DataTable();
                        dtGV = objJobDb.getGV(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);

                        //if (lb.Text == bid && sche == lb2.Text && (path != "" && bid != "" && sche != ""))
                        //{
                        //    dtGV = objDB.getGVSelPath(hidDept.Value, hidSect.Value, bid, sche, path);
                        //    path = ""; sche = ""; path = "";
                        //}
                        //else
                        //{
                        //    dtGV = objDB.getGV(hidDept.Value, hidSect.Value, lb.Text, lb2.Text);
                        //}
                        DataTable dt2 = new DataTable();
                        GridView gv = new GridView();
                        gv.Style.Add(HtmlTextWriterStyle.Width, "100%");
                        //dom
                        //for (int k = 0; k < dtGV.Columns.Count; k++)
                        //{
                        //    dt2.Columns.Add(dtGV.Columns[k].ColumnName);
                        //}
                        //for (int o = 0; o < dtGV.Rows.Count; o++)
                        //{
                        //    DataRow dr = dt2.NewRow();
                        //    //dr[dt.Columns[k].ColumnName] = dt.Rows[o][dt.Columns[k].ColumnName].ToString();
                        //    for (int a = 0; a < dtGV.Columns.Count; a++)
                        //    {
                        //        for (int b = 0; b < a; b++)
                        //        {
                        //            dr[dtGV.Columns[a].ColumnName] = dtGV.Rows[o][dtGV.Columns[a].ColumnName].ToString();

                        //        }
                        //    }
                        //    dt2.Rows.Add(dr);
                        //}
                        //dom

                        dt2.Columns.Add(" ");
                        //dt2.Columns.Add("No.");
                        dt2.Columns.Add("Meterial Group");
                        dt2.Columns.Add("Code No.");
                        dt2.Columns.Add("Item No.");
                        dt2.Columns.Add("Full Description");
                        dt2.Columns.Add("Additional Description");
                        dt2.Columns.Add("Standard Drawing/Ref No.");
                        dt2.Columns.Add("Required QTY");
                        dt2.Columns.Add("Unit");
                        dt2.Columns.Add("Reserved QTY");
                        dt2.Columns.Add("Confirmed Reservation QTY");
                        dt2.Columns.Add("Bid QTY");
                        dt2.Columns.Add("Stock Availability");
                        dt2.Columns.Add("Status");
                        dt2.Columns.Add("Incoterms");
                        dt2.Columns.Add("Waive Test");
                        dt2.Columns.Add("Related Document");


                        dt2.Columns.Add("Currency");
                        dt2.Columns.Add("Head1");
                        dt2.Columns.Add("Head2");
                        dt2.Columns.Add("Head3");
                        dt2.Columns.Add("Head4");
                        dt2.Columns.Add("Head5");
                        dt2.Columns.Add("Head6");
                        dt2.Columns.Add("  ");
                        dt2.Columns.Add("hide");
                        dt2.Columns.Add("jobno");
                        dt2.Columns.Add("scheno");
                        dt2.Columns.Add("bidno");
                        dt2.Columns.Add("parts");
                        dt2.Columns.Add("ft");
                        dt2.Columns.Add("mt");
                        dt2.Columns.Add("rt");
                        //dt2.Columns.Add("Del", typeof(System.Web.UI.WebControls.Button));

                        //System.Web.UI.HtmlControls.HtmlButton

                        for (int y = 0; y < dtGV.Rows.Count; y++)
                        {
                            //dtGV.Rows[y]["equip_add_desc"].ToString();
                            DataRow dr = dt2.NewRow();
                            dr[" "] = "";
                            //dr["No."] = dtGV.Rows[y]["rowid"].ToString();
                            dr["Meterial Group"] = dtGV.Rows[y]["equip_mas_grp"].ToString();
                            dr["Code No."] = dtGV.Rows[y]["equip_code"].ToString();
                            dr["Item No."] = dtGV.Rows[y]["equip_item_no"].ToString();
                            dr["Full Description"] = dtGV.Rows[y]["equip_full_desc"].ToString();
                            dr["Additional Description"] = dtGV.Rows[y]["equip_add_desc"].ToString();
                            dr["Standard Drawing/Ref No."] = dtGV.Rows[y]["equip_standard_drawing"].ToString();
                            dr["Required QTY"] = dtGV.Rows[y]["equip_qty"].ToString();
                            dr["Unit"] = dtGV.Rows[y]["equip_unit"].ToString();
                            dr["Reserved QTY"] = "";
                            dr["Confirmed Reservation QTY"] = "";
                            dr["Bid QTY"] = dtGV.Rows[y]["equip_bid_qty"].ToString();
                            dr["Stock Availability"] = "";
                            dr["Status"] = "";
                            dr["Incoterms"] = dtGV.Rows[y]["equip_incoterm"].ToString();
                            if (dtGV.Rows[y]["factory_test"].ToString() == "True")
                            {
                                dr["ft"] = "1";
                            }
                            if (dtGV.Rows[y]["manual_test"].ToString() == "True")
                            {
                                dr["mt"] = "1";
                            }
                            if (dtGV.Rows[y]["routine_test"].ToString() == "True")
                            {
                                dr["rt"] = "1";
                            }

                            dr["Related Document"] = "";


                            dr["Currency"] = dtGV.Rows[y]["currency"].ToString();
                            dr["Head1"] = dtGV.Rows[y]["supply_equip_unit_price"].ToString();
                            dr["Head2"] = dtGV.Rows[y]["supply_equip_amonut"].ToString();
                            dr["Head3"] = dtGV.Rows[y]["local_exwork_unit_price"].ToString();
                            dr["Head4"] = dtGV.Rows[y]["local_exwork_amonut"].ToString();
                            dr["Head5"] = dtGV.Rows[y]["local_tran_unit_price"].ToString();
                            dr["Head6"] = dtGV.Rows[y]["local_tran_amonut"].ToString();
                            dr["  "] = "";
                            dr["hide"] = dtGV.Rows[y]["rowid"].ToString();
                            dr["jobno"] = dtGV.Rows[y]["job_no"].ToString();
                            dr["scheno"] = dtGV.Rows[y]["schedule_no"].ToString();
                            dr["bidno"] = dtGV.Rows[y]["bid_no"].ToString();
                            dr["parts"] = dtGV.Rows[y]["path_code"].ToString();
                            dt2.Rows.Add(dr);
                        }

                        //int j = dt2.Columns.Count;
                        //DataRow row;
                        //foreach (DataColumn cl in dt2.Columns)
                        //{
                        //    row = dt2.NewRow();

                        //    for (int o = 0; o < dt.Rows.Count; o++)
                        //    {
                        //        row[o] = dt.Rows[o][dt.Columns[o].ColumnName].ToString();
                        //    }
                        //    dt2.Rows.Add(row);
                        //}
                        //for (int o = 0; o < dt.Rows.Count; o++)
                        //{
                        //    DataRow dr = dt2.NewRow();
                        //    dt2.Rows.Add(dr);
                        //}

                        //for (int j = 0; j < dt.Rows.Count; j++)
                        //{
                        //    DataRow dr = dt2.NewRow();
                        //    dr[dt.Columns[j = 0].ColumnName] = dt.Rows[j][dt.Columns[j].ColumnName].ToString();
                        //}

                        //dt2.Rows.Add(dr);



                        //DataTable dt2 = new DataTable();
                        //dt2.Columns.Add("test1");
                        //dt2.Columns.Add("test2");
                        //DataRow dr = dt2.NewRow();
                        //dr["test1"] = "1"+i;
                        //dr["test2"] = "11115"+i;
                        //dt2.Rows.Add(dr);
                        //GridView gv = new GridView();
                        //for (int j = 0; j < 22; j++)
                        //{
                        //    TemplateField tfield = new TemplateField();
                        //    tfield.HeaderText = "Country"+j;
                        //    gv.Columns.Add(tfield);
                        //}

                        if (dt2.Rows.Count > 0)//
                        {
                            gv.ID = "GV" + i;
                            gv.Attributes.Add("runat", "server");
                            gv.RowDataBound += new GridViewRowEventHandler(GV0_RowDataBound);
                            gv.DataSource = dt2;
                            gv.DataBind();
                            //string myID = gv.ID; 
                            //GridView thisgv = (GridView)this.FindControl("GV0");
                            //if (thisgv!=null)
                            //{

                            //}
                            //GridView gridveiw1 = (GridView)this.FindControl("GV0");
                            //if (gridveiw1!=null)
                            //{

                            //}
                            //System.Web.UI.WebControls.GridView gvf = (System.Web.UI.WebControls.GridView)this.FindControl("GV0");
                            //if (gvf!=null)
                            //{

                            //}

                        }
                        divAdd.Controls.Add(divAbso);
                        divAbso.Controls.Add(gv);

                        //tbCell2.Controls.Add(divAdd);
                        //tbRow2.Controls.Add(tbCell2);
                        //tb.Controls.Add(tbRow2);

                        //dtPth = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);
                        for (int z = 0; z < dtPth.Rows.Count; z++)
                        {
                            Button btn = new Button();
                            btn.Text = dtPth.Rows[z]["part_name"].ToString();
                            btn.CommandName = lb.Text + "," + lb3.Text + "," + lb2.Text + "," + lb4.Text;
                            if (i >= 10) btn.ID = z + dtPth.Rows[z]["part_name"].ToString() + (i).ToString();
                            else btn.ID = dtPth.Rows[z]["part_name"].ToString() + (i).ToString();
                            btn.Attributes.Add("runat", "server");
                            btn.Click += new EventHandler(btn_Add);

                            if (z == 0)
                            {
                                Label lb_path = new Label();
                                lb_path.Text = "Part:   ";
                                divAbso.Controls.Add(lb_path);
                            }
                            divAbso.Controls.Add(btn);
                        }
                        Label lb_ln = new Label();
                        lb_ln.Text = "<br />";
                        divAbso.Controls.Add(lb_ln);
                        Button bt_addPart = new Button();
                        bt_addPart.Text = "Add Part";
                        bt_addPart.ID = "bt_addPart" + i;
                        bt_addPart.Attributes.Add("runat", "server");
                        bt_addPart.CssClass = "btn btn-primary";
                        bt_addPart.OnClientClick = "openPopup('5'); return false;";
                        divAbso.Controls.Add(bt_addPart);
                        Label lb_space = new Label();
                        lb_space.Text = " ";
                        divAbso.Controls.Add(lb_space);
                        Button bt_save = new Button();
                        bt_save.Text = "Save";
                        bt_save.ID = i.ToString();
                        bt_save.Attributes.Add("runat", "server");
                        bt_save.CssClass = "btn btn-primary";
                        bt_save.Click += new EventHandler(bt_saveEq);
                        divAbso.Controls.Add(bt_save);

                        //tbCell2.Controls.Add(divAdd);
                        //tbRow2.Controls.Add(tbCell2);
                        //tb.Controls.Add(tbRow2);

                        //tbRow3.Controls.Add(tbCell3);
                        //tb.Controls.Add(tbRow3);
                        divMain.Controls.Add(tb);

                        divTabEqui.Controls.Add(divMain);
                        divTabEqui.Controls.Add(divAdd);
                        //for (int iGV = 0; iGV < divAdd.Controls.Count; iGV++)
                        //{
                        //    try
                        //    {
                        //        GridView gvEq = (GridView)divAdd.Controls[iGV];
                        //        foreach (GridViewRow gvRow in gvEq.Rows)
                        //        {
                        //            TextBox txt = (TextBox)gvRow.FindControl("");

                        //        }
                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        LogHelper.WriteEx(ex);
                        //    }
                        //}
                    }
                }
                else
                {
                    Table tb = new Table();
                    TableRow tbRow = new TableRow();
                    TableCell tbCell = new TableCell();
                    TableRow tbRow2 = new TableRow();
                    TableCell tbCell2 = new TableCell();
                    TableRow tbRow3 = new TableRow();
                    TableCell tbCell3 = new TableCell();

                    DataTable dtPth = new DataTable();
                    System.Web.UI.HtmlControls.HtmlGenericControl createDiv = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divMain = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divAdd = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    System.Web.UI.HtmlControls.HtmlGenericControl divAbso = new System.Web.UI.HtmlControls.HtmlGenericControl("DIV");
                    //divAdd.Controls.Clear();
                    divMain.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    tb.Style.Add(HtmlTextWriterStyle.Width, "100%");
                    Label lb = new Label();
                    lb.ID = "bidNo0";
                    lb.Text = "Other";
                    Label lb2 = new Label();
                    lb2.Text = "Other";
                    Label lbDetail = new Label();
                    lb2.ID = "SchNo0";
                    //lbDetail.Text = "Bid No. " + lb.Text + " | " + "Schedule " + lb2.Text;
                    lbDetail.Text = "Other";
                    Label lb3 = new Label();
                    lb3.Text = "";// dt.Rows[i]["schedule_no"].ToString();
                    Label lb4 = new Label();
                    lb4.Text = "";
                    createDiv.ID = "Div0";
                    divAdd.ID = "DivGv0";
                    divAdd.Attributes.Add("runat", "server");
                    divAdd.Attributes.Add("style", "overflow:auto;height:300px;position:relative;");
                    //createDiv.Style.Add("background-color", "#c4d5f6");
                    createDiv.Attributes.Add("style", "height:30px;padding-top:0.5%;background-color:#c4d5f6;cursor:pointer");
                    divMain.Attributes.Add("onclick", "javascript:openTabEquiSub(" + divAdd.ClientID + ");");
                    createDiv.Controls.Add(lbDetail);
                    divAdd.Style["display"] = "none";
                    divAbso.ID = "DivAbso0";
                    divAbso.Attributes.Add("runat", "server");
                    divAbso.Attributes.Add("style", "position: absolute;");
                    tbCell.Controls.Add(createDiv);
                    tbRow.Controls.Add(tbCell);
                    tb.Controls.Add(tbRow);

                    Label lb_bidnoeq = new Label();
                    lb_bidnoeq.Text = "Bid No.:";
                    TextBox tb_bidnoeq = new TextBox();

                    Label lb_reveq = new Label();
                    lb_reveq.Text = "Rev No.:";
                    TextBox tb_reveq = new TextBox();

                    Label lb_eq = new Label();
                    lb_eq.Text = "Equipment ";
                    TextBox tb_eq = new TextBox();



                    DataTable dtGV = new DataTable();
                    dtGV = objJobDb.getGV(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);

                    DataTable dt2 = new DataTable();
                    GridView gv = new GridView();
                    gv.Style.Add(HtmlTextWriterStyle.Width, "100%");


                    dt2.Columns.Add(" ");
                    //dt2.Columns.Add("No.");
                    dt2.Columns.Add("Meterial Group");
                    dt2.Columns.Add("Code No.");
                    dt2.Columns.Add("Item No.");
                    dt2.Columns.Add("Full Description");
                    dt2.Columns.Add("Additional Description");
                    dt2.Columns.Add("Standard Drawing/Ref No.");
                    dt2.Columns.Add("Required QTY");
                    dt2.Columns.Add("Unit");
                    dt2.Columns.Add("Reserved QTY");
                    dt2.Columns.Add("Confirmed Reservation QTY");
                    dt2.Columns.Add("Bid QTY");
                    dt2.Columns.Add("Stock Availability");
                    dt2.Columns.Add("Status");
                    dt2.Columns.Add("Incoterms");
                    dt2.Columns.Add("Waive Test");
                    dt2.Columns.Add("Related Document");


                    dt2.Columns.Add("Currency");
                    dt2.Columns.Add("Head1");
                    dt2.Columns.Add("Head2");
                    dt2.Columns.Add("Head3");
                    dt2.Columns.Add("Head4");
                    dt2.Columns.Add("Head5");
                    dt2.Columns.Add("Head6");
                    dt2.Columns.Add("  ");
                    dt2.Columns.Add("hide");
                    dt2.Columns.Add("jobno");
                    dt2.Columns.Add("scheno");
                    dt2.Columns.Add("bidno");
                    dt2.Columns.Add("parts");
                    dt2.Columns.Add("ft");
                    dt2.Columns.Add("mt");
                    dt2.Columns.Add("rt");
                    //dt2.Columns.Add("Del", typeof(System.Web.UI.WebControls.Button));

                    //System.Web.UI.HtmlControls.HtmlButton

                    for (int y = 0; y < dtGV.Rows.Count; y++)
                    {
                        //dtGV.Rows[y]["equip_add_desc"].ToString();
                        DataRow dr = dt2.NewRow();
                        dr[" "] = "";
                        //dr["No."] = dtGV.Rows[y]["rowid"].ToString();
                        dr["Meterial Group"] = dtGV.Rows[y]["equip_mas_grp"].ToString();
                        dr["Code No."] = dtGV.Rows[y]["equip_code"].ToString();
                        dr["Item No."] = dtGV.Rows[y]["equip_item_no"].ToString();
                        dr["Full Description"] = dtGV.Rows[y]["equip_full_desc"].ToString();
                        dr["Additional Description"] = dtGV.Rows[y]["equip_add_desc"].ToString();
                        dr["Standard Drawing/Ref No."] = dtGV.Rows[y]["equip_standard_drawing"].ToString();
                        dr["Required QTY"] = dtGV.Rows[y]["equip_qty"].ToString();
                        dr["Unit"] = dtGV.Rows[y]["equip_unit"].ToString();
                        dr["Reserved QTY"] = "";
                        dr["Confirmed Reservation QTY"] = "";
                        dr["Bid QTY"] = dtGV.Rows[y]["equip_bid_qty"].ToString();
                        dr["Stock Availability"] = "";
                        dr["Status"] = "";
                        dr["Incoterms"] = dtGV.Rows[y]["equip_incoterm"].ToString();
                        if (dtGV.Rows[y]["factory_test"].ToString() == "True")
                        {
                            dr["ft"] = "1";
                        }
                        if (dtGV.Rows[y]["manual_test"].ToString() == "True")
                        {
                            dr["mt"] = "1";
                        }
                        if (dtGV.Rows[y]["routine_test"].ToString() == "True")
                        {
                            dr["rt"] = "1";
                        }

                        dr["Related Document"] = "";


                        dr["Currency"] = dtGV.Rows[y]["currency"].ToString();
                        dr["Head1"] = dtGV.Rows[y]["supply_equip_unit_price"].ToString();
                        dr["Head2"] = dtGV.Rows[y]["supply_equip_amonut"].ToString();
                        dr["Head3"] = dtGV.Rows[y]["local_exwork_unit_price"].ToString();
                        dr["Head4"] = dtGV.Rows[y]["local_exwork_amonut"].ToString();
                        dr["Head5"] = dtGV.Rows[y]["local_tran_unit_price"].ToString();
                        dr["Head6"] = dtGV.Rows[y]["local_tran_amonut"].ToString();
                        dr["  "] = "";
                        dr["hide"] = dtGV.Rows[y]["rowid"].ToString();
                        dr["jobno"] = dtGV.Rows[y]["job_no"].ToString();
                        dr["scheno"] = dtGV.Rows[y]["schedule_no"].ToString();
                        dr["bidno"] = dtGV.Rows[y]["bid_no"].ToString();
                        dr["parts"] = dtGV.Rows[y]["path_code"].ToString();
                        dt2.Rows.Add(dr);
                    }



                    if (dt2.Rows.Count > 0)//
                    {
                        gv.ID = "GV0";
                        gv.Attributes.Add("runat", "server");
                        gv.RowDataBound += new GridViewRowEventHandler(GV0_RowDataBound);
                        gv.DataSource = dt2;
                        gv.DataBind();


                    }
                    divAdd.Controls.Add(divAbso);
                    divAbso.Controls.Add(gv);

                    //tbCell2.Controls.Add(divAdd);
                    //tbRow2.Controls.Add(tbCell2);
                    //tb.Controls.Add(tbRow2);

                    //dtPth = objJobDb.getBtnPaths(hidDept.Value, hidSect.Value, lb.Text, lb3.Text);
                    for (int z = 0; z < dtPth.Rows.Count; z++)
                    {
                        Button btn = new Button();
                        btn.Text = dtPth.Rows[z]["part_name"].ToString();
                        btn.CommandName = lb.Text + "," + lb3.Text + "," + lb2.Text + "," + lb4.Text;
                        //if (i >= 10) btn.ID = z + dtPth.Rows[z]["path_code"].ToString() + (i).ToString();
                        //else btn.ID = dtPth.Rows[z]["path_code"].ToString() + (i).ToString();
                        btn.ID = dtPth.Rows[z]["part_name"].ToString() + 0;
                        btn.Attributes.Add("runat", "server");
                        btn.Click += new EventHandler(btn_Add);

                        if (z == 0)
                        {
                            Label lb_path = new Label();
                            lb_path.Text = "Part:   ";
                            divAbso.Controls.Add(lb_path);
                        }
                        divAbso.Controls.Add(btn);
                    }
                    Label lb_ln = new Label();
                    lb_ln.Text = "<br />";
                    divAbso.Controls.Add(lb_ln);
                    Button bt_addPart = new Button();
                    bt_addPart.Text = "Add Part";
                    bt_addPart.ID = "bt_addPart";
                    bt_addPart.Attributes.Add("runat", "server");
                    bt_addPart.CssClass = "btn btn-primary";
                    bt_addPart.OnClientClick = "openPopup('5'); return false;";
                    divAbso.Controls.Add(bt_addPart);
                    Label lb_space = new Label();
                    lb_space.Text = " ";
                    divAbso.Controls.Add(lb_space);
                    Button bt_save = new Button();
                    bt_save.Text = "Save";
                    bt_save.ID = "0";
                    bt_save.Attributes.Add("runat", "server");
                    bt_save.CssClass = "btn btn-primary";
                    bt_save.Click += new EventHandler(bt_saveEq);
                    divAbso.Controls.Add(bt_save);

                    //tbCell2.Controls.Add(divAdd);
                    //tbRow2.Controls.Add(tbCell2);
                    //tb.Controls.Add(tbRow2);

                    //tbRow3.Controls.Add(tbCell3);
                    //tb.Controls.Add(tbRow3);
                    divMain.Controls.Add(tb);

                    divTabEqui.Controls.Add(divMain);
                    divTabEqui.Controls.Add(divAdd);

                }

            }
        }
        protected void GV0_RowDataBound(object sender, System.Web.UI.WebControls.GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox cb_all = new CheckBox();
                cb_all.ID = "cb_all";
                cb_all.Checked = true;

                e.Row.Cells[0].Controls.Add(cb_all);
                e.Row.Cells[0].Visible = false;
                GridViewRow row = e.Row;
                GridView gv = (GridView)row.NamingContainer;
                CheckBox cb = (CheckBox)gv.FindControl("cb_all");
                if (gv != null)
                {
                    cb_all.Attributes.Add("onclick", "javascript:HeaderClickEquiTabAll('" + cb_all.ClientID + "','" + gv.ClientID + "');");
                }
                e.Row.Cells[18].Text = @"Supply of Equipment<br>
Foreign Supply<br>
CIF Thai Port<br>
Unit Price";
                e.Row.Cells[19].Text = @"Supply of Equipment<br>
Foreign Supply<br>
CIF Thai Port<br>
Amount";
                e.Row.Cells[20].Text = @"Local Supply<br>
Ex-works Price<br>
(excluding VAT)<br>
Baht<br>
Unit Price";
                e.Row.Cells[21].Text = @"Local Supply<br>
Ex-works Price<br>
(excluding VAT)<br>
Baht<br>
Amount";
                e.Row.Cells[22].Text = @"Local Transpotation,<br>
Contruction and<br>
Installation<br>
(excluding VAT)<br>
Baht<br>
Unit Price";
                e.Row.Cells[23].Text = @"Local Transpotation,<br>
Contruction and<br>
Installation<br>
(excluding VAT)<br>
Baht<br>
Amount";
                e.Row.BackColor = System.Drawing.Color.FromArgb(22, 64, 148);
                e.Row.ForeColor = System.Drawing.Color.FromArgb(255, 255, 255);
                e.Row.Cells[13].Visible = false;
                e.Row.Cells[25].Visible = false;
                e.Row.Cells[26].Visible = false;
                e.Row.Cells[27].Visible = false;
                e.Row.Cells[28].Visible = false;
                e.Row.Cells[29].Visible = false;
                e.Row.Cells[30].Visible = false;
                e.Row.Cells[31].Visible = false;
                e.Row.Cells[32].Visible = false;
                e.Row.Style["font-size"] = "12px";

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;


                //lb_itmNo.Text = drv["rowid"].ToString();
                CheckBox cb_sub = new CheckBox();
                cb_sub.Checked = true;
                cb_sub.ID = "cb_sub";
                e.Row.Cells[0].Controls.Add(cb_sub);
                e.Row.Cells[0].Visible = false;
                ListBox ddl_meterialGrp = new ListBox();
                ddl_meterialGrp.ID = "ddl_meterialGrp";
                DataTable dtmet = new DataTable();
                clsEquipmentMasterdb objEqbb = new clsEquipmentMasterdb();
                dtmet = objEqbb.GetSapMaterialGroup();
                ddl_meterialGrp.Items.Clear();
                ddl_meterialGrp.DataSource = dtmet;
                ddl_meterialGrp.DataTextField = "CODE";
                ddl_meterialGrp.DataValueField = "NAME";
                ddl_meterialGrp.DataBind();
                e.Row.Cells[1].Controls.Add(ddl_meterialGrp);
                if (drv["Meterial Group"].ToString() != "")
                {
                    ddl_meterialGrp.SelectedValue = drv["Meterial Group"].ToString();
                }
                Label lb_codeNo = new Label();
                lb_codeNo.ID = "lb_codeNo";
                lb_codeNo.Text = drv["Code No."].ToString();
                lb_codeNo.Width = 75;
                e.Row.Cells[2].Controls.Add(lb_codeNo);

                Label lb_itmNo = new Label();
                lb_itmNo.ID = "lb_itmNo";
                lb_itmNo.Text = drv["Item No."].ToString();
                TextBox tb_itmNo = new TextBox();
                tb_itmNo.ID = "tb_itmNo";
                tb_itmNo.Width = 60;
                tb_itmNo.Text = drv["Item No."].ToString();
                e.Row.Cells[3].Controls.Add(tb_itmNo);

                Label lb_fDesc = new Label();
                lb_fDesc.ID = "lb_fDesc";
                lb_fDesc.Width = 200;
                e.Row.Cells[4].Controls.Add(lb_fDesc);
                lb_fDesc.Text = drv["Full Description"].ToString();
                TextBox tb_addDesc = new TextBox();
                tb_addDesc.Width = 150;
                tb_addDesc.Height = 25;
                tb_addDesc.ID = "tb_addDesc";
                e.Row.Cells[5].Controls.Add(tb_addDesc);
                e.Row.Cells[5].HorizontalAlign = HorizontalAlign.Center;
                tb_addDesc.Text = drv["Additional Description"].ToString();//

                Label lb_stdDr = new Label();
                lb_stdDr.ID = "lb_stdDr";
                TextBox tb_stdDr = new TextBox();
                tb_stdDr.Width = 150;
                tb_stdDr.Height = 25;
                tb_stdDr.ID = "tb_stdDr";
                tb_stdDr.Text = drv["Standard Drawing/Ref No."].ToString();
                e.Row.Cells[6].Controls.Add(tb_stdDr);

                TextBox tb_reqQTY = new TextBox();
                tb_reqQTY.Width = 30;
                tb_reqQTY.Height = 25;
                tb_reqQTY.ID = "tb_reqQTY";
                tb_reqQTY.Text = drv["Required QTY"].ToString();
                tb_reqQTY.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[7].Controls.Add(tb_reqQTY);
                e.Row.Cells[7].HorizontalAlign = HorizontalAlign.Center;

                TextBox tb_unit = new TextBox();
                Label lb_unit = new Label();
                lb_unit.ID = "lb_unit";
                lb_unit.Text = drv["Unit"].ToString();
                tb_unit.Width = 30;
                tb_unit.Height = 25;
                tb_unit.ID = "tb_unit";
                tb_unit.Text = drv["Unit"].ToString();
                tb_unit.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[8].Controls.Add(lb_unit);
                e.Row.Cells[8].HorizontalAlign = HorizontalAlign.Center;

                Label lb_revQTY = new Label();
                lb_revQTY.ID = "lb_revQTY";
                e.Row.Cells[9].Controls.Add(lb_revQTY);
                Label conrevQTY = new Label();
                conrevQTY.ID = "conrevQTY";
                e.Row.Cells[10].Controls.Add(conrevQTY);

                //TextBox tb_revQTY = new TextBox();
                //tb_revQTY.Width = 40;
                //tb_revQTY.Height = 25;
                //e.Row.Cells[9].Controls.Add(tb_revQTY);
                //TextBox tb_conrevQTY = new TextBox();
                //tb_conrevQTY.Width = 40;
                //tb_conrevQTY.Height = 25;
                //e.Row.Cells[10].Controls.Add(tb_conrevQTY);

                Label lb_bidQTY = new Label();
                lb_bidQTY.ID = "lb_bidQTY";
                lb_bidQTY.Text = drv["Bid QTY"].ToString();
                TextBox tb_bid = new TextBox();
                tb_bid.ID = "tb_bid";
                tb_bid.Width = 30;
                tb_bid.Height = 25;
                tb_bid.Text = drv["Bid QTY"].ToString();
                tb_bid.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[11].Controls.Add(lb_bidQTY);

                LinkButton lnk_res = new LinkButton();
                lnk_res.Text = "Reserve";
                e.Row.Cells[12].Controls.Add(lnk_res);

                Label lb_status = new Label();
                lb_status.ID = "lb_status";
                e.Row.Cells[13].Controls.Add(lb_status);
                e.Row.Cells[13].Visible = false;
                ListBox ddl_inco = new ListBox();
                ddl_inco.ID = "ddl_inco";

                DataTable dtinco = new DataTable();
                dtinco = objJobDb.getDDLIncoterm();
                ddl_inco.Items.Clear();
                ddl_inco.DataSource = dtinco;
                ddl_inco.DataTextField = "incoterm_name";
                ddl_inco.DataValueField = "incoterm_code";
                ddl_inco.DataBind();
                ddl_inco.SelectionMode = ListSelectionMode.Multiple;
                e.Row.Cells[14].Controls.Add(ddl_inco);
                Label lb_inco = new Label();
                lb_inco.ID = "lb_inco";
                lb_inco.Text = drv["Incoterms"].ToString();
                lb_inco.Visible = false;
                //ddl_inco.SelectedIndexChanged+= new EventHandler(newList_SelectedIndexChanged);
                //ddl_inco.AutoPostBack = true;
                e.Row.Cells[14].Controls.Add(lb_inco);
                foreach (ListItem item in ddl_inco.Items)
                {
                    if (lb_inco.Text.IndexOf(item.Value) > -1)
                    {
                        item.Selected = true;
                    }
                }
                //if (drv["Incoterms"].ToString() != "") ddl_inco.SelectedValue = drv["Incoterms"].ToString();
                CheckBox cb_wft = new CheckBox();
                cb_wft.ID = "cb_wft";
                ListBox ddl_wa = new ListBox();
                ddl_wa.ID = "ddl_wa";
                ddl_wa.Items.Clear();
                ddl_wa.Items.Insert(0, new ListItem("Factory Test", "1"));
                ddl_wa.Items.Insert(1, new ListItem("Manual Test", "2"));
                ddl_wa.Items.Insert(2, new ListItem("Routine Test", "3"));
                ddl_wa.SelectionMode = ListSelectionMode.Multiple;
                e.Row.Cells[15].Controls.Add(ddl_wa);
                //if (drv["Waive Test"].ToString() != "")
                //{
                //    ddl_wa.SelectedValue = drv["Waive Test"].ToString();
                //}

                e.Row.Cells[15].HorizontalAlign = HorizontalAlign.Center;


                LinkButton lnk_doc = new LinkButton();
                lnk_doc.Text = "linkdoc";
                e.Row.Cells[16].Controls.Add(lnk_doc);

                TextBox tb_cur = new TextBox();
                tb_cur.Width = 30;
                tb_cur.Height = 25;
                tb_cur.ID = "tb_cur";
                DropDownList ddl_cur = new DropDownList();
                ddl_cur.ID = "ddl_cur";
                ddl_cur.Items.Clear();
                ddl_cur.Items.Insert(0, new ListItem("THB", "THB"));
                ddl_cur.Items.Insert(1, new ListItem("USD", "USD"));
                ddl_cur.Items.Insert(2, new ListItem("EUR", "EUR"));
                ddl_cur.Items.Insert(3, new ListItem("JPY", "JPY"));
                ddl_cur.Items.Insert(4, new ListItem("CNY", "CNY"));
                e.Row.Cells[17].Controls.Add(ddl_cur);
                e.Row.Cells[17].HorizontalAlign = HorizontalAlign.Center;
                if (drv["Currency"].ToString() != "")
                {
                    if (drv["Currency"].ToString() != "") ddl_cur.SelectedValue = drv["Currency"].ToString();
                }
                TextBox tb_supUnit = new TextBox();
                tb_supUnit.Width = 80;
                tb_supUnit.Height = 25;
                tb_supUnit.ID = "tb_supUnit";
                e.Row.Cells[18].Controls.Add(tb_supUnit);
                if (drv["Head1"].ToString() != "") tb_supUnit.Text = String.Format("{0:0.00}", Convert.ToDecimal(drv["Head1"].ToString()));
                tb_supUnit.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[18].HorizontalAlign = HorizontalAlign.Center;

                TextBox tb_supAmou = new TextBox();
                tb_supAmou.Width = 80;
                tb_supAmou.Height = 25;
                tb_supAmou.ID = "tb_supAmou";
                if (drv["Head2"].ToString() != "") tb_supAmou.Text = String.Format("{0:0.00}", Convert.ToDecimal(drv["Head2"].ToString()));
                tb_supAmou.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[19].Controls.Add(tb_supAmou);
                e.Row.Cells[19].HorizontalAlign = HorizontalAlign.Center;

                TextBox tb_localSUnit = new TextBox();
                tb_localSUnit.Width = 80;
                tb_localSUnit.Height = 25;
                tb_localSUnit.ID = "tb_localSUnit";
                if (drv["Head3"].ToString() != "") tb_localSUnit.Text = String.Format("{0:0.00}", Convert.ToDecimal(drv["Head3"].ToString()));
                tb_localSUnit.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[20].Controls.Add(tb_localSUnit);
                e.Row.Cells[20].HorizontalAlign = HorizontalAlign.Center;

                TextBox tb_localSAmou = new TextBox();
                tb_localSAmou.Width = 80;
                tb_localSAmou.Height = 25;
                tb_localSAmou.ID = "tb_localSAmou";
                if (drv["Head4"].ToString() != "") tb_localSAmou.Text = String.Format("{0:0.00}", Convert.ToDecimal(drv["Head4"].ToString()));
                tb_localSAmou.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[21].Controls.Add(tb_localSAmou);
                e.Row.Cells[21].HorizontalAlign = HorizontalAlign.Center;

                TextBox tb_localTUnit = new TextBox();
                tb_localTUnit.Width = 80;
                tb_localTUnit.Height = 25;
                tb_localTUnit.ID = "tb_localTUnit";
                if (drv["Head5"].ToString() != "") tb_localTUnit.Text = String.Format("{0:0.00}", Convert.ToDecimal(drv["Head5"].ToString()));
                tb_localTUnit.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[22].Controls.Add(tb_localTUnit);
                e.Row.Cells[22].HorizontalAlign = HorizontalAlign.Center;

                TextBox tb_localTAmou = new TextBox();
                tb_localTAmou.Width = 80;
                tb_localTAmou.Height = 25;
                tb_localTAmou.ID = "tb_localTAmou";
                if (drv["Head6"].ToString() != "") tb_localTAmou.Text = String.Format("{0:0.00}", Convert.ToDecimal(drv["Head6"].ToString()));
                tb_localTAmou.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                e.Row.Cells[23].Controls.Add(tb_localTAmou);
                e.Row.Cells[23].HorizontalAlign = HorizontalAlign.Center;

                ImageButton btDel = new ImageButton();
                btDel.ImageUrl = "../../images/bin.png";
                btDel.Width = 20;
                btDel.Height = 20;
                btDel.Click += new ImageClickEventHandler(btn_Del);
                Label lb_hidden = new Label();
                lb_hidden.Visible = false;
                lb_hidden.Text = "";
                e.Row.Cells[24].Controls.Add(btDel);
                e.Row.Cells[24].Controls.Add(lb_hidden);
                Label lb_hide = new Label();
                lb_hide.Text = drv["hide"].ToString();
                lb_hide.ID = "lb_hide";
                e.Row.Cells[25].Controls.Add(lb_hide);
                e.Row.Cells[25].Visible = false;
                Label lb_jobno = new Label();
                lb_jobno.Text = drv["jobno"].ToString();
                lb_jobno.ID = "lb_jobno";
                e.Row.Cells[26].Controls.Add(lb_jobno);
                e.Row.Cells[26].Visible = false;
                Label lb_scheno = new Label();
                lb_scheno.Text = drv["scheno"].ToString();
                lb_scheno.ID = "lb_scheno";
                e.Row.Cells[27].Controls.Add(lb_scheno);
                e.Row.Cells[27].Visible = false;
                Label lb_bidno = new Label();
                lb_bidno.ID = "lb_bidno";
                lb_bidno.Text = drv["bidno"].ToString();
                e.Row.Cells[28].Controls.Add(lb_bidno);
                e.Row.Cells[28].Visible = false;

                Label lb_parts = new Label();
                lb_parts.ID = "lb_parts";
                lb_parts.Text = drv["parts"].ToString();
                e.Row.Cells[29].Controls.Add(lb_parts);
                e.Row.Cells[29].Visible = false;

                Label lb_ft = new Label();
                lb_ft.ID = "lb_ft";
                lb_ft.Text = drv["ft"].ToString();
                e.Row.Cells[30].Controls.Add(lb_ft);
                e.Row.Cells[30].Visible = false;

                Label lb_mt = new Label();
                lb_mt.ID = "lb_mt";
                lb_mt.Text = drv["mt"].ToString();
                e.Row.Cells[31].Controls.Add(lb_mt);
                e.Row.Cells[31].Visible = false;

                Label lb_rt = new Label();
                lb_rt.ID = "lb_rt";
                lb_rt.Text = drv["rt"].ToString();
                e.Row.Cells[32].Controls.Add(lb_rt);
                e.Row.Cells[32].Visible = false;

                if (drv["ft"].ToString() == "1")
                {
                    ddl_wa.Items[0].Selected = true;
                }
                if (drv["mt"].ToString() == "1")
                {
                    ddl_wa.Items[1].Selected = true;
                }
                if (drv["rt"].ToString() == "1")
                {
                    ddl_wa.Items[2].Selected = true;
                }
            }
        }
        private void btn_Del(object sender, EventArgs e)
        {
            ImageButton btnDel = (ImageButton)sender;
            GridViewRow gvr = btnDel.NamingContainer as GridViewRow;
            Label lb_itmNo = (Label)gvr.FindControl("lb_itmNo");
            string row = gvr.Cells[25].Text;
            string id = gvr.ClientID;
            objJobDb.delMyTask(row);
            DataTable t = new DataTable();
            //bind_equiList("", "", "", "", t);
            dvhidEq.Style["display"] = "block";
            String div = "Div" + id;
            System.Web.UI.HtmlControls.HtmlControl thisdiv = (System.Web.UI.HtmlControls.HtmlControl)this.FindControl(div);
            thisdiv.Style["display"] = "block";
        }
        private void bt_saveEq(object sender, EventArgs e)
        {
            Button bt = sender as Button;
            string a = bt.ID;
            int number = Convert.ToInt32(a);
            string grid = "GV" + number;
            System.Web.UI.WebControls.GridView gvEq = (System.Web.UI.WebControls.GridView)divTabEqui.FindControl(grid);
            if (gvEq != null)
            {
                foreach (GridViewRow gvRow in gvEq.Rows)
                {
                    CheckBox cb_sub = (CheckBox)gvRow.FindControl("cb_sub");
                    ListBox ddl_meterialGrp = (ListBox)gvRow.FindControl("ddl_meterialGrp");
                    Label lb_codeNo = (Label)gvRow.FindControl("lb_codeNo");
                    Label lb_itmNo = (Label)gvRow.FindControl("lb_itmNo");
                    TextBox tb_itmNo = (TextBox)gvRow.FindControl("tb_itmNo");
                    Label lb_fDesc = (Label)gvRow.FindControl("lb_fDesc");
                    TextBox tb_addDesc = (TextBox)gvRow.FindControl("tb_addDesc");
                    Label lb_stdDr = (Label)gvRow.FindControl("lb_stdDr");
                    TextBox tb_stdDr = (TextBox)gvRow.FindControl("tb_stdDr");
                    TextBox tb_reqQTY = (TextBox)gvRow.FindControl("tb_reqQTY");
                    TextBox tb_unit = (TextBox)gvRow.FindControl("tb_unit");
                    Label lb_unit = (Label)gvRow.FindControl("lb_unit");
                    Label lb_revQTY = (Label)gvRow.FindControl("lb_revQTY");
                    Label conrevQTY = (Label)gvRow.FindControl("conrevQTY");
                    Label lb_bidQTY = (Label)gvRow.FindControl("lb_bidQTY");
                    TextBox tb_bid = (TextBox)gvRow.FindControl("tb_bid");
                    Label lb_status = (Label)gvRow.FindControl("lb_status");
                    ListBox ddl_inco = (ListBox)gvRow.FindControl("ddl_inco");
                    //CheckBox cb_wft = (CheckBox)gvRow.FindControl("cb_wft");
                    //CheckBox cb_tmt = (CheckBox)gvRow.FindControl("cb_tmt");
                    ListBox ddl_wa = (ListBox)gvRow.FindControl("ddl_wa");
                    //TextBox tb_cur = (TextBox)gvRow.FindControl("tb_cur");
                    DropDownList ddl_cur = (DropDownList)gvRow.FindControl("ddl_cur");
                    TextBox tb_supUnit = (TextBox)gvRow.FindControl("tb_supUnit");
                    TextBox tb_supAmou = (TextBox)gvRow.FindControl("tb_supAmou");
                    TextBox tb_localSUnit = (TextBox)gvRow.FindControl("tb_localSUnit");
                    TextBox tb_localSAmou = (TextBox)gvRow.FindControl("tb_localSAmou");
                    TextBox tb_localTUnit = (TextBox)gvRow.FindControl("tb_localTUnit");
                    TextBox tb_localTAmou = (TextBox)gvRow.FindControl("tb_localTAmou");
                    Label lb_hide = (Label)gvRow.FindControl("lb_hide");
                    Label lb_jobno = (Label)gvRow.FindControl("lb_jobno");
                    Label lb_scheno = (Label)gvRow.FindControl("lb_scheno");
                    Label lb_bidno = (Label)gvRow.FindControl("lb_bidno");
                    if (cb_sub.Checked == true)
                    {
                        addMytaskEquipJob objeq = new addMytaskEquipJob();
                        objeq.bid_no = lb_bidno.Text;
                        objeq.job_no = lb_jobno.Text;
                        objeq.rowid = Convert.ToInt32(lb_hide.Text);
                        objeq.schedule_no = Convert.ToInt32(lb_scheno.Text);
                        string val = "";
                        foreach (ListItem listItem in ddl_inco.Items)
                        {
                            if (listItem.Selected)
                            {
                                if (val == "") val = listItem.Value;
                                else val += "," + listItem.Value;
                            }
                        }
                        objeq.equip_incoterm = val;
                        //objeq.equip_incoterm = ddl_inco.SelectedValue;
                        objeq.equip_mas_grp = ddl_meterialGrp.SelectedValue;

                        string[] Muljob = ddl_wa.SelectedValue.Split(',');
                        foreach (ListItem listItem in ddl_wa.Items)
                        {
                            if (listItem.Selected)
                            {
                                if (listItem.Value == "1")
                                {
                                    objeq.factory_test = true;
                                }
                                else if (listItem.Value == "2")
                                {
                                    objeq.manual_test = true;
                                }
                                else if (listItem.Value == "3")
                                {
                                    objeq.routine_test = true;
                                }
                            }
                        }
                        string bidno = "";
                        string bidrev = "";
                        if (Muljob.Length > 1)
                        {
                            bidno = Muljob[0];
                            bidrev = Muljob[1];
                        }
                        //if (ddl_wa.SelectedValue == "1")
                        //{
                        //    objeq.factory_test = true;
                        //    objeq.manual_test = false;
                        //    objeq.routine_test = false;
                        //}
                        //else if (ddl_wa.SelectedValue == "2")
                        //{
                        //    objeq.factory_test = false;
                        //    objeq.manual_test = true;
                        //    objeq.routine_test = false;
                        //}
                        //else if (ddl_wa.SelectedValue == "3")
                        //{
                        //    objeq.factory_test = false;
                        //    objeq.manual_test = false;
                        //    objeq.routine_test = true;
                        //}
                        objeq.equip_item_no = tb_itmNo.Text;
                        objeq.equip_add_desc = tb_addDesc.Text.Replace("'", "''");
                        if (tb_reqQTY.Text != "") objeq.equip_qty = Convert.ToDecimal(tb_reqQTY.Text);
                        objeq.equip_unit = lb_unit.Text;
                        if (lb_bidQTY.Text != "") objeq.equip_bid_qty = Convert.ToInt32(lb_bidQTY.Text);
                        if (tb_supUnit.Text != "") objeq.supply_equip_unit_price = Convert.ToDecimal(tb_supUnit.Text);
                        else objeq.supply_equip_unit_price = default(decimal);
                        if (tb_supAmou.Text != "") objeq.supply_equip_amonut = Convert.ToDecimal(tb_supAmou.Text);
                        else objeq.supply_equip_amonut = default(decimal);
                        if (tb_localSUnit.Text != "") objeq.local_exwork_unit_price = Convert.ToDecimal(tb_localSUnit.Text);
                        else objeq.local_exwork_unit_price = default(decimal);
                        if (tb_localSAmou.Text != "") objeq.local_exwork_amonut = Convert.ToDecimal(tb_localSAmou.Text);
                        else objeq.local_exwork_amonut = default(decimal);
                        if (tb_localTUnit.Text != "") objeq.local_tran_unit_price = Convert.ToDecimal(tb_localTUnit.Text);
                        else objeq.local_tran_unit_price = default(decimal);
                        if (tb_localTAmou.Text != "") objeq.local_tran_amonut = Convert.ToDecimal(tb_localTAmou.Text);
                        else objeq.local_tran_amonut = default(decimal);
                        objeq.currency = ddl_cur.SelectedValue;
                        objeq.equip_standard_drawing = tb_stdDr.Text.Replace("'", "''");
                        objJobDb.updateMyTaskEqui(objeq);
                    }
                }
            }
            //DataTable t = new DataTable();
            //bind_equiList("", "", "", "", t);
            dvhidEq.Style["display"] = "block";
            string div = "DivGv" + number;
            System.Web.UI.HtmlControls.HtmlControl thisdiv = (System.Web.UI.HtmlControls.HtmlControl)this.FindControl(div);
            thisdiv.Style["display"] = "block";
        }
        private void btn_Add(object sender, EventArgs e)
        {
            DataTable dtSel = new DataTable();
            dtSel.Columns.Add(" ");
            dtSel.Columns.Add("Meterial Group");
            dtSel.Columns.Add("Code No.");
            dtSel.Columns.Add("Item No.");
            dtSel.Columns.Add("Full Description");
            dtSel.Columns.Add("Additional Description");
            dtSel.Columns.Add("Standard Drawing/Ref No.");
            dtSel.Columns.Add("Required QTY");
            dtSel.Columns.Add("Unit");
            dtSel.Columns.Add("Reserved QTY");
            dtSel.Columns.Add("Confirmed Reservation QTY");
            dtSel.Columns.Add("Bid QTY");
            dtSel.Columns.Add("Stock Availability");
            dtSel.Columns.Add("Status");
            dtSel.Columns.Add("Incoterms");
            dtSel.Columns.Add("Head7");
            dtSel.Columns.Add("Related Document");
            dtSel.Columns.Add("Currency");
            dtSel.Columns.Add("Head1");
            dtSel.Columns.Add("Head2");
            dtSel.Columns.Add("Head3");
            dtSel.Columns.Add("Head4");
            dtSel.Columns.Add("Head5");
            dtSel.Columns.Add("Head6");
            dtSel.Columns.Add("  ");
            dtSel.Columns.Add("hide");
            dtSel.Columns.Add("jobno");
            dtSel.Columns.Add("scheno");
            dtSel.Columns.Add("bidno");
            dtSel.Columns.Add("parts");
            dtSel.Columns.Add("ft");
            dtSel.Columns.Add("mt");
            dtSel.Columns.Add("rt");
            if (ViewState["GetAllDT"] == null)
            {
                DataTable dtGetGV = new DataTable();
                dtGetGV = objJobDb.getTabEqui(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text);
                for (int i = 0; i < dtGetGV.Rows.Count; i++)
                {
                    string grid = "GV" + i;
                    System.Web.UI.WebControls.GridView gvEq = (System.Web.UI.WebControls.GridView)divTabEqui.FindControl(grid);

                    if (gvEq != null)
                    {
                        foreach (GridViewRow gvRow in gvEq.Rows)
                        {
                            CheckBox cb_sub = (CheckBox)gvRow.FindControl("cb_sub");
                            ListBox ddl_meterialGrp = (ListBox)gvRow.FindControl("ddl_meterialGrp");
                            Label lb_codeNo = (Label)gvRow.FindControl("lb_codeNo");
                            Label lb_itmNo = (Label)gvRow.FindControl("lb_itmNo");
                            TextBox tb_itmNo = (TextBox)gvRow.FindControl("tb_itmNo");
                            Label lb_fDesc = (Label)gvRow.FindControl("lb_fDesc");
                            TextBox tb_addDesc = (TextBox)gvRow.FindControl("tb_addDesc");
                            Label lb_stdDr = (Label)gvRow.FindControl("lb_stdDr");
                            TextBox tb_stdDr = (TextBox)gvRow.FindControl("tb_stdDr");
                            TextBox tb_reqQTY = (TextBox)gvRow.FindControl("tb_reqQTY");
                            TextBox tb_unit = (TextBox)gvRow.FindControl("tb_unit");
                            Label lb_revQTY = (Label)gvRow.FindControl("lb_revQTY");
                            Label conrevQTY = (Label)gvRow.FindControl("conrevQTY");
                            Label lb_bidQTY = (Label)gvRow.FindControl("lb_bidQTY");
                            Label lb_status = (Label)gvRow.FindControl("lb_status");
                            ListBox ddl_inco = (ListBox)gvRow.FindControl("ddl_inco");
                            //CheckBox cb_wft = (CheckBox)gvRow.FindControl("cb_wft");
                            //CheckBox cb_tmt = (CheckBox)gvRow.FindControl("cb_tmt");
                            ListBox ddl_wa = (ListBox)gvRow.FindControl("ddl_wa");
                            //TextBox tb_cur = (TextBox)gvRow.FindControl("tb_cur");
                            DropDownList ddl_cur = (DropDownList)gvRow.FindControl("ddl_cur");
                            TextBox tb_supUnit = (TextBox)gvRow.FindControl("tb_supUnit");
                            TextBox tb_supAmou = (TextBox)gvRow.FindControl("tb_supAmou");
                            TextBox tb_localSUnit = (TextBox)gvRow.FindControl("tb_localSUnit");
                            TextBox tb_localSAmou = (TextBox)gvRow.FindControl("tb_localSAmou");
                            TextBox tb_localTUnit = (TextBox)gvRow.FindControl("tb_localTUnit");
                            TextBox tb_localTAmou = (TextBox)gvRow.FindControl("tb_localTAmou");
                            Label lb_hide = (Label)gvRow.FindControl("lb_hide");
                            Label lb_jobno = (Label)gvRow.FindControl("lb_jobno");
                            Label lb_scheno = (Label)gvRow.FindControl("lb_scheno");
                            Label lb_bidno = (Label)gvRow.FindControl("lb_bidno");
                            Label lb_parts = (Label)gvRow.FindControl("lb_parts");
                            Label lb_ft = (Label)gvRow.FindControl("lb_ft");
                            Label lb_mt = (Label)gvRow.FindControl("lb_mt");
                            Label lb_rt = (Label)gvRow.FindControl("lb_rt");
                            Label lb_inco = (Label)gvRow.FindControl("lb_inco");
                            DataRow dr = dtSel.NewRow();
                            dr[" "] = cb_sub.Checked;
                            //dr["No."] = dtGV.Rows[y]["rowid"].ToString();
                            dr["Meterial Group"] = ddl_meterialGrp.SelectedValue;
                            dr["Code No."] = lb_codeNo.Text;
                            dr["Item No."] = tb_itmNo.Text;
                            dr["Full Description"] = lb_fDesc.Text;
                            dr["Additional Description"] = tb_addDesc.Text;
                            dr["Standard Drawing/Ref No."] = tb_stdDr.Text;
                            dr["Required QTY"] = tb_reqQTY.Text;
                            dr["Unit"] = "";
                            dr["Reserved QTY"] = "";
                            dr["Confirmed Reservation QTY"] = "";
                            dr["Bid QTY"] = "";
                            dr["Stock Availability"] = "";
                            dr["Status"] = "";
                            dr["Incoterms"] = lb_inco.Text;
                            dr["Head7"] = "";
                            dr["Related Document"] = "";


                            dr["Currency"] = "";
                            dr["Head1"] = "";
                            dr["Head2"] = "";
                            dr["Head3"] = "";
                            dr["Head4"] = "";
                            dr["Head5"] = "";
                            dr["Head6"] = "";
                            dr["  "] = "";
                            dr["hide"] = lb_hide.Text;
                            dr["jobno"] = lb_jobno.Text;
                            dr["scheno"] = lb_scheno.Text;
                            dr["bidno"] = lb_bidno.Text;
                            dr["parts"] = lb_parts.Text;
                            dr["ft"] = lb_ft.Text;
                            dr["mt"] = lb_mt.Text;
                            dr["rt"] = lb_rt.Text;
                            dtSel.Rows.Add(dr);
                        }
                    }
                }
            }
            else
            {
                DataTable dtall = new DataTable();
                dtall = (DataTable)ViewState["GetAllDT"];
                for (int i = 0; i < dtall.Rows.Count; i++)
                {
                    DataRow dr = dtSel.NewRow();
                    dr[" "] = dtall.Rows[i][" "].ToString();
                    //dr["No."] = dtGV.Rows[y]["rowid"].ToString();
                    dr["Meterial Group"] = dtall.Rows[i]["Meterial Group"].ToString();
                    dr["Code No."] = dtall.Rows[i]["Code No."].ToString();
                    dr["Item No."] = dtall.Rows[i]["Item No."].ToString();
                    dr["Full Description"] = dtall.Rows[i]["Full Description"].ToString();
                    dr["Additional Description"] = dtall.Rows[i]["Additional Description"].ToString();
                    dr["Standard Drawing/Ref No."] = dtall.Rows[i]["Standard Drawing/Ref No."].ToString();
                    dr["Required QTY"] = dtall.Rows[i]["Required QTY"].ToString();
                    dr["Unit"] = "";
                    dr["Reserved QTY"] = "";
                    dr["Confirmed Reservation QTY"] = "";
                    dr["Bid QTY"] = "";
                    dr["Stock Availability"] = "";
                    dr["Status"] = "";
                    dr["Incoterms"] = dtall.Rows[i]["Incoterms"].ToString();
                    dr["Head7"] = "";
                    dr["Related Document"] = "";


                    dr["Currency"] = "";
                    dr["Head1"] = "";
                    dr["Head2"] = "";
                    dr["Head3"] = "";
                    dr["Head4"] = "";
                    dr["Head5"] = "";
                    dr["Head6"] = "";
                    dr["  "] = "";
                    dr["hide"] = dtall.Rows[i]["hide"].ToString();
                    dr["jobno"] = dtall.Rows[i]["jobno"].ToString();
                    dr["scheno"] = dtall.Rows[i]["scheno"].ToString();
                    dr["bidno"] = dtall.Rows[i]["bidno"].ToString();
                    dr["parts"] = dtall.Rows[i]["parts"].ToString();
                    dr["ft"] = dtall.Rows[i]["ft"].ToString();
                    dr["mt"] = dtall.Rows[i]["mt"].ToString();
                    dr["rt"] = dtall.Rows[i]["rt"].ToString();
                    dtSel.Rows.Add(dr);
                }
            }

            Button bt = sender as Button;
            string flag = "select";
            string path = bt.Text;
            tbAddPath.Text = bt.Text;
            string[] filter = bt.CommandName.Split(',');
            if (filter.Length == 4)
            {
                bind_equiList(path, filter[0], filter[1], flag, dtSel);
                //tb_bidNoPart.Text = filter[0];
                //tb_bidrevPart.Text = filter[3];
                tb_schNoPart.Text = filter[1];
                tb_schNamePart.Text = filter[2];
            }
            dvhidEq.Style["display"] = "block";
            string a = bt.ID.Substring(bt.ID.IndexOf(path) + path.Length);
            int number = Convert.ToInt32(a);
            string div = "DivGv" + number;
            System.Web.UI.HtmlControls.HtmlControl thisdiv = (System.Web.UI.HtmlControls.HtmlControl)this.FindControl(div);
            thisdiv.Style["display"] = "block";
            //dv_addEqui.Style["display"] = "none";

        }

        protected void gvTaskSch_RowDataBound(object sender, GridViewRowEventArgs e)
        {

        }

        protected void gvDrawing_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                //CheckBox CbAll = (CheckBox)e.Row.FindControl("CbAll");
                //CbAll.Attributes.Add("onclick", "javascript:chkAll(" + CbAll.ClientID + ",'" + gvDrawing.ClientID + "');");

            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                gvDrawing.UseAccessibleHeader = true;
                gvDrawing.HeaderRow.TableSection = TableRowSection.TableHeader;
                DataRowView drv = (DataRowView)e.Row.DataItem;

                Label lbNo = (Label)e.Row.FindControl("lbNo");
                Label lbGruop = (Label)e.Row.FindControl("lbGruop");
                Label lbDivis = (Label)e.Row.FindControl("lbDivis");
                Label lbDoct = (Label)e.Row.FindControl("lbDoct");
                Label lbDes = (Label)e.Row.FindControl("lbDes");
                Label lbFilename = (Label)e.Row.FindControl("lbFilename");
                Label lbSheet = (Label)e.Row.FindControl("lbSheet");
                Label lbRevi = (Label)e.Row.FindControl("lbRevi");
                Label lbUpdated = (Label)e.Row.FindControl("lbUpdated");
                Label lbUpby = (Label)e.Row.FindControl("lbUpby");
                LinkButton link_DrawDoc = (LinkButton)e.Row.FindControl("link_DrawDoc");
                Label lbhidDate = (Label)e.Row.FindControl("lbhidDate");
                Label lbDocno = (Label)e.Row.FindControl("lbDocno");
                Label lbBid = (Label)e.Row.FindControl("lbBid");
                Label lbRevbid = (Label)e.Row.FindControl("lbRevbiddr");
                Label lbhidSch = (Label)e.Row.FindControl("lbhidSch");
                Label lbSchnamedr = (Label)e.Row.FindControl("lbSchnamedr");
                Label lbhidjob = (Label)e.Row.FindControl("lbhidjob");
                Label lbhidjobrev = (Label)e.Row.FindControl("lbhidjobrev");
                ImageButton imgDel = (ImageButton)e.Row.FindControl("imgDel");
                Label lbDoctSH = (Label)e.Row.FindControl("lbDoctSH");
                
                //kie21-12
                //ListBox Multiselect_Bid = (ListBox)e.Row.FindControl("Multiselect_Bid");  lbhidjob

                //Label lbCreby = (Label)e.Row.FindControl("lbCreby");
                //Label lbCrebydate = (Label)e.Row.FindControl("lbCrebydate"); 


                link_DrawDoc.Text = " <a href='" + drv["link_doc"].ToString() + "' target='_blank'> ViewDoc </a>";
                int index = Convert.ToInt16(e.Row.RowIndex) + 1;
                lbNo.Text = index.ToString();
                lbGruop.Text = drv["depart_position_name"].ToString();
                lbDivis.Text = drv["section_position_name"].ToString();
                lbDoct.Text = drv["doctype_name"].ToString();
                lbDes.Text = drv["tr_doc_desc"].ToString();
                lbFilename.Text = drv["doc_desc"].ToString();
                lbSheet.Text = drv["doc_sheetno"].ToString();
                lbRevi.Text = drv["doc_revision"].ToString();
                lbDocno.Text = drv["doc_no"].ToString();
                lbBid.Text = drv["bid_no"].ToString();
                lbRevbid.Text = drv["bid_revision"].ToString();
                lbhidSch.Text = drv["schedule_no"].ToString();
                lbSchnamedr.Text = drv["schedule_name"].ToString();
                lbDoctSH.Text= drv["doctype_code"].ToString();
                //lbhidjob.Text = drv["job_no"].ToString();
                //lbhidjobrev.Text = drv["job_revision"].ToString();
                DateTime dt = new DateTime();
                if (drv["tr_updated_datetime"].ToString() != "" && drv["tr_updated_datetime"].ToString() != null)
                {
                    dt = Convert.ToDateTime(drv["tr_updated_datetime"].ToString());
                    lbUpdated.Text = dt.ToString("dd MMMM yyyy", th);
                }
                else
                {
                    lbUpdated.Text = "";
                }
                EmpInfoClass empInfo = new EmpInfoClass();
                if (drv["tr_updated_by"].ToString() != "")
                {
                    Employee emp = empInfo.getInFoByEmpID(drv["tr_updated_by"].ToString());
                    lbUpby.Text = emp.SNAME;
                }
                lbhidDate.Text = drv["updated_datetime"].ToString();
                if (lbGruop.Text == hidDept.Value) imgDel.Enabled = true;
                else imgDel.Enabled = false;
                //lbCrebydate.Text = drv["created_datetime"].ToString();
                //lbCreby.Text = drv["created_by"].ToString();

                    //DataTable dtMulti = objJobDb.bindMultiBid(hidJobNo.Value);
                    //int i = 0;
                    //foreach (DataRow dr in dtMulti.Rows)
                    //{
                    //    Multiselect_Bid.Items.Insert(i, new ListItem(dr["bid_no"].ToString() + "," + dr["schedule_name"].ToString(), dr["bid_no"].ToString() + "," + dr["schedule_no"].ToString() + "," + dr["schedule_name"].ToString()));
                    //    i++;
                    //}
            }
        }

        protected void gvAttFile_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox CbAttAll = (CheckBox)e.Row.FindControl("CbAttAll");
                CbAttAll.Attributes.Add("onclick", "javascript:chkAll(" + CbAttAll.ClientID + ",'" + gvAttFile.ClientID + "');");
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                gvAttFile.UseAccessibleHeader = true;
                gvAttFile.HeaderRow.TableSection = TableRowSection.TableHeader;
                DataRowView drv = (DataRowView)e.Row.DataItem;
                //lbGroup lbDivis lbDrawno lbDescrip lbFilename lbSheetno lbRev lbUpdatedate lbUpby
                Label lbAttno = (Label)e.Row.FindControl("lbAttno");
                Label lbGroup = (Label)e.Row.FindControl("lbGroup");
                Label lbDivis = (Label)e.Row.FindControl("lbDivis");
                Label lbDrawno = (Label)e.Row.FindControl("lbDrawno");
                Label lbDescrip = (Label)e.Row.FindControl("lbDescrip");
                Label lbFilename = (Label)e.Row.FindControl("lbFilename");
                Label lbSheetno = (Label)e.Row.FindControl("lbSheetno");
                Label lbRev = (Label)e.Row.FindControl("lbRev");
                Label lbUpdatedate = (Label)e.Row.FindControl("lbUpdatedate");
                Label lbUpby = (Label)e.Row.FindControl("lbUpby");
                Label lbDocCode = (Label)e.Row.FindControl("lbDocCode");
                Label lbhidDate = (Label)e.Row.FindControl("lbhidDate2");
                CheckBox CbAttChk = (CheckBox)e.Row.FindControl("CbAttChk");
                //Label lbRowid = (Label)e.Row.FindControl("lbRowid");
                //Label lbCreateby = (Label)e.Row.FindControl("lbCreateby");
                //Label lbCreatedate = (Label)e.Row.FindControl("lbCreatedate");


                int index = Convert.ToInt16(e.Row.RowIndex) + 1;
                lbAttno.Text = index.ToString();
                //lbAttno.Text = drv["rowno"].ToString();
                lbGroup.Text = drv["depart_position_name"].ToString();
                lbDivis.Text = drv["section_position_name"].ToString();
                lbDrawno.Text = drv["doc_nodeid"].ToString();
                lbDescrip.Text = drv["doc_desc"].ToString();
                lbFilename.Text = drv["doc_name"].ToString();
                lbSheetno.Text = drv["doc_sheetno"].ToString();
                lbRev.Text = drv["doc_revision_no"].ToString();
                //lbUpdatedate.Text = drv["updated_datetime"].ToString();
                lbUpby.Text = drv["updated_by"].ToString();
                lbhidDate.Text = drv["updated_datetime"].ToString();
                DateTime dt = Convert.ToDateTime(drv["updated_datetime"].ToString());
                lbUpdatedate.Text = dt.ToString("dd MMMM yyyy", th);
                lbDocCode.Text = drv["doctype_code"].ToString();
                //lbCreateby.Text = drv["created_by"].ToString();
                //lbCreatedate.Text = drv["created_datetime"].ToString();
            }
        }

        protected void gvEquipList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                CheckBox CbEqAll = (CheckBox)e.Row.FindControl("CbEqAll");
                CbEqAll.Attributes.Add("onclick", "javascript:chkAllEq(" + CbEqAll.ClientID + ");");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;

                Label lbEqNo = (Label)e.Row.FindControl("lbEqNo");
                TextBox tbPrice = (TextBox)e.Row.FindControl("tbPrice");
                TextBox tbReq = (TextBox)e.Row.FindControl("tbReq");
                TextBox tbRes = (TextBox)e.Row.FindControl("tbRes");
                Label lbConQ = (Label)e.Row.FindControl("lbConQ");
                Label lbBidQ = (Label)e.Row.FindControl("lbBidQ");
                TextBox tbQty = (TextBox)e.Row.FindControl("tbQty");
                Label lbCodeno = (Label)e.Row.FindControl("lbCodeno");
                lbEqNo.Text = drv["Rowno"].ToString();


                tbPrice.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                tbReq.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                tbRes.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                //tbQty.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                //lbConQ.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                if (tbReq.Text != "" && lbConQ.Text != "")
                {
                    lbBidQ.Text = (Convert.ToInt32(tbReq.Text) - Convert.ToInt32(lbConQ.Text)).ToString();
                }
            }
        }

        protected void gvTaskDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                //CheckBox CbTDocAll = (CheckBox)e.Row.FindControl("CbTDocAll");
                //CbTDocAll.Attributes.Add("onclick", "javascript:chkAll(" + CbTDocAll.ClientID + ",'" + gvTaskDoc.ClientID + "');");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbTDocNo = (Label)e.Row.FindControl("lbTDocNo");
                Label lbGroup = (Label)e.Row.FindControl("lbGroup");
                Label lbDivis = (Label)e.Row.FindControl("lbDivis");
                Label lbDescrip = (Label)e.Row.FindControl("lbDescrip");
                Label lbFilename = (Label)e.Row.FindControl("lbFilename");
                Label lbSheetno = (Label)e.Row.FindControl("lbSheetno");
                Label lbRev = (Label)e.Row.FindControl("lbRev");
                Label lbUpdatedate = (Label)e.Row.FindControl("lbUpdatedate");
                Label lbUpby = (Label)e.Row.FindControl("lbUpby");
                Label lbDocCode = (Label)e.Row.FindControl("lbDocCode");
                Label lbBidno = (Label)e.Row.FindControl("lbBidno");
                Label lbRevbid = (Label)e.Row.FindControl("lbRevbid");
                Label lbhidjob = (Label)e.Row.FindControl("lbhidjob");
                Label lbhidjobrev = (Label)e.Row.FindControl("lbhidjobrev");
                Label lbSchnohid = (Label)e.Row.FindControl("lbSchnohid");
                Label lbSchname = (Label)e.Row.FindControl("lbSchname");
                Label lbhidDate = (Label)e.Row.FindControl("lbhidDate3");
                Label lbDocno = (Label)e.Row.FindControl("lbDocno");
                LinkButton link_DrawDoc = (LinkButton)e.Row.FindControl("link_DrawDoc");
                ImageButton imgDocdel = (ImageButton)e.Row.FindControl("imgDocdel");
                Label lbDoctSH = (Label)e.Row.FindControl("lbDoctSH");
                link_DrawDoc.Text = " <a href='" + drv["link_doc"].ToString() + "' target='_blank'> ViewDoc </a>";
                //lbTDocNo.Text = drv["Rowno"].ToString();
                int index = Convert.ToInt16(e.Row.RowIndex) + 1;
                lbTDocNo.Text = index.ToString();
                lbBidno.Text = drv["bid_no"].ToString();
                lbRevbid.Text = drv["bid_revision"].ToString();
                lbSchnohid.Text = drv["schedule_no"].ToString();
                lbSchname.Text = drv["schedule_name"].ToString();
                lbGroup.Text = drv["depart_position_name"].ToString();
                lbDivis.Text = drv["section_position_name"].ToString();
                lbDocCode.Text = drv["doctype_name"].ToString();
                lbDescrip.Text = drv["tr_doc_desc"].ToString();
                lbFilename.Text = drv["doc_desc"].ToString();
                lbSheetno.Text = drv["doc_sheetno"].ToString();
                lbRev.Text = drv["doc_revision"].ToString();
                lbDocno.Text = drv["doc_no"].ToString();
                lbDoctSH.Text = drv["doctype_code"].ToString();
                //lbhidjob.Text = drv["job_no"].ToString();
                //lbhidjobrev.Text = drv["job_revision"].ToString();
                DateTime dt = new DateTime();
                if (drv["updated_datetime"].ToString() != "" && drv["updated_datetime"].ToString() != null)
                {
                    dt = Convert.ToDateTime(drv["updated_datetime"].ToString());
                    lbUpdatedate.Text = dt.ToString("dd MMMM yyyy", th);
                }
                else
                {
                    lbUpdatedate.Text = "";
                }

                lbhidDate.Text = drv["updated_datetime"].ToString();
                //lbUpby.Text = drv["updated_by"].ToString();
                EmpInfoClass empInfo = new EmpInfoClass();
                if (drv["updated_by"].ToString() != "")
                {
                    Employee emp = empInfo.getInFoByEmpID(drv["updated_by"].ToString());
                    lbUpby.Text = emp.SNAME;
                }
                if (lbGroup.Text == hidDept.Value) imgDocdel.Enabled = true;
                else imgDocdel.Enabled = false;

            }
        }

        //protected void gvTemplate_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {

        //        CheckBox CbTemAll = (CheckBox)e.Row.FindControl("CbTemAll");
        //        CbTemAll.Attributes.Add("onclick", "javascript:chkAll(" + CbTemAll.ClientID + ",'" + gvTemplate.ClientID + "');");

        //    }
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        //DataRowView drv = (DataRowView)e.Row.DataItem;
        //        //Label lbAddno = (Label)e.Row.FindControl("lbAddno");

        //        //lbAddno.Text = drv["Rowno"].ToString();

        //    }
        //}

        protected void gvItem_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox cb_all = (CheckBox)e.Row.FindControl("cb_all");
                cb_all.Attributes.Add("onclick", "javascript:chkAll(" + cb_all.ClientID + ",'" + gvItem.ClientID + "');");
                if (rdbBom.Checked == true)
                {
                    e.Row.Cells[12].Visible = true;
                }
                else
                {
                    e.Row.Cells[12].Visible = false;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbItemno = (Label)e.Row.FindControl("lbItemno");
                int no = Convert.ToInt32(e.Row.RowIndex) + 1;
                lbItemno.Text = no.ToString();
                Label lb_division = (Label)e.Row.FindControl("lb_division");
                lb_division.Text = drv["section_position_name"].ToString();
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["depart_position_name"].ToString();
                Label lb_mertGrp = (Label)e.Row.FindControl("lb_mertGrp");
                lb_mertGrp.Text = drv["materialgroup"].ToString();
                Label lb_equiCode = (Label)e.Row.FindControl("lb_equiCode");
                lb_equiCode.Text = drv["equip_code"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                lb_legend.Text = drv["legend"].ToString();
                Label lb_fullDesc = (Label)e.Row.FindControl("lb_fullDesc");
                lb_fullDesc.Text = drv["equip_desc_full"].ToString();
                Label lb_shortDesc = (Label)e.Row.FindControl("lb_shortDesc");
                lb_shortDesc.Text = drv["equip_desc_short"].ToString();
                Label lb_equiType = (Label)e.Row.FindControl("lb_equiType");
                if (drv["equip_type"].ToString() != "") lb_equiType.Text = drv["equip_type"].ToString();
                Label lb_equiGrp = (Label)e.Row.FindControl("lb_equiGrp");
                //lb_equiGrp.Text = drv[""].ToString();
                Label lbUnit = (Label)e.Row.FindControl("lbUnit");
                lbUnit.Text = drv["unit"].ToString();
                Label lb_Part = (Label)e.Row.FindControl("lb_Part");
                if (drv["parts"].ToString() != "") lb_Part.Text = drv["parts"].ToString();
                //Label lb_equipBom = (Label)e.Row.FindControl("lb_equipBom");
                //lb_equipBom.Text = drv["equip_code2"].ToString();
                if (rdbBom.Checked == true)
                {
                    e.Row.Cells[12].Visible = true;
                }
                else
                {
                    e.Row.Cells[12].Visible = false;
                }

            }
        }
        protected void gvItemAdd_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox cb_all = (CheckBox)e.Row.FindControl("cb_all");
                cb_all.Attributes.Add("onclick", "javascript:chkAll(" + cb_all.ClientID + ",'" + gvItem.ClientID + "');");
                if (rdbBomAdd.Checked == true)
                {
                    e.Row.Cells[13].Visible = true;
                }
                else
                {
                    e.Row.Cells[13].Visible = false;
                }
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbItemno = (Label)e.Row.FindControl("lbItemno");
                int no = Convert.ToInt32(e.Row.RowIndex) + 1;
                lbItemno.Text = no.ToString();
                Label lb_division = (Label)e.Row.FindControl("lb_division");
                lb_division.Text = drv["section_position_name"].ToString();
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["depart_position_name"].ToString();
                Label lb_mertGrp = (Label)e.Row.FindControl("lb_mertGrp");
                lb_mertGrp.Text = drv["materialgroup"].ToString();
                Label lb_equiCode = (Label)e.Row.FindControl("lb_equiCode");
                lb_equiCode.Text = drv["equip_code"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                lb_legend.Text = drv["legend"].ToString();
                Label lb_fullDesc = (Label)e.Row.FindControl("lb_fullDesc");
                lb_fullDesc.Text = drv["equip_desc_full"].ToString();
                Label lb_shortDesc = (Label)e.Row.FindControl("lb_shortDesc");
                lb_shortDesc.Text = drv["equip_desc_short"].ToString();
                Label lb_equiType = (Label)e.Row.FindControl("lb_equiType");
                if (drv["equip_type"].ToString() != "") lb_equiType.Text = drv["equip_type"].ToString();
                Label lb_equiGrp = (Label)e.Row.FindControl("lb_equiGrp");
                //lb_equiGrp.Text = drv[""].ToString();
                Label lbUnit = (Label)e.Row.FindControl("lbUnit");
                lbUnit.Text = drv["unit"].ToString();
                Label lb_Part = (Label)e.Row.FindControl("lb_Part");
                if (drv["parts"].ToString() != "") lb_Part.Text = drv["parts"].ToString();
                //Label lb_equipBom = (Label)e.Row.FindControl("lb_equipBom");
                //lb_equipBom.Text = drv["equip_code2"].ToString();
                if (rdbBomAdd.Checked == true)
                {
                    e.Row.Cells[13].Visible = true;
                }
                else
                {
                    e.Row.Cells[13].Visible = false;
                }

            }
        }

        //protected void gvHistory_RowDataBound(object sender, GridViewRowEventArgs e)
        //{
        //    if (e.Row.RowType == DataControlRowType.Header)
        //    {

        //        //CheckBox CbTDocAll = (CheckBox)e.Row.FindControl("CbTDocAll");
        //        //CbTDocAll.Attributes.Add("onclick", "javascript:chkAllTd(" + CbTDocAll.ClientID + ");");

        //    }
        //    if (e.Row.RowType == DataControlRowType.DataRow)
        //    {
        //        //DataRowView drv = (DataRowView)e.Row.DataItem;
        //        //Label lbAddno = (Label)e.Row.FindControl("lbAddno");

        //        //lbAddno.Text = drv["Rowno"].ToString();

        //    }
        //}

        protected void gvDocview_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {

                //CheckBox CbTDocAll = (CheckBox)e.Row.FindControl("CbTDocAll");
                //CbTDocAll.Attributes.Add("onclick", "javascript:chkAllTd(" + CbTDocAll.ClientID + ");");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //DataRowView drv = (DataRowView)e.Row.DataItem;
                //Label lbAddno = (Label)e.Row.FindControl("lbAddno");

                //lbAddno.Text = drv["Rowno"].ToString();

            }
        }

        protected void gvAddDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox CbDAll = (CheckBox)e.Row.FindControl("CbDAll");
                CbDAll.Attributes.Add("onclick", "javascript:chkAll(" + CbDAll.ClientID + ",'" + gvAddDoc.ClientID + "');");

            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                gvAddDoc.UseAccessibleHeader = true;
                gvAddDoc.HeaderRow.TableSection = TableRowSection.TableHeader;
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbDno = (Label)e.Row.FindControl("lbDno");
                Label lbDGroup = (Label)e.Row.FindControl("lbDGroup");
                Label lbDocDivis = (Label)e.Row.FindControl("lbDocDivis");
                Label lbADoctype = (Label)e.Row.FindControl("lbADoctype");
                Label lbADno = (Label)e.Row.FindControl("lbADno");
                Label lbDescrip = (Label)e.Row.FindControl("lbDescription");
                Label lbFilename = (Label)e.Row.FindControl("lbFilename");
                Label lbDSheetno = (Label)e.Row.FindControl("lbDSheetno");
                Label lbADRev = (Label)e.Row.FindControl("lbADRev");
                Label lbADUpdatedate = (Label)e.Row.FindControl("lbADUpdatedate");
                Label lbADUpby = (Label)e.Row.FindControl("lbADUpby");
                Label lbhidDate = (Label)e.Row.FindControl("lbhidADDate");



                int index = Convert.ToInt16(e.Row.RowIndex) + 1;
                lbDno.Text = index.ToString();
                lbDGroup.Text = drv["depart_position_name"].ToString();
                lbDocDivis.Text = drv["section_position_name"].ToString();
                lbADno.Text = drv["doc_nodeid"].ToString();
                lbDescrip.Text = drv["doc_desc"].ToString();
                lbDSheetno.Text = drv["doc_sheetno"].ToString();
                lbADRev.Text = drv["doc_revision_no"].ToString();
                lbFilename.Text = drv["doc_name"].ToString();
                //lbUpdatedate.Text = drv["updated_datetime"].ToString();
                lbADUpby.Text = drv["updated_by"].ToString();
                lbhidDate.Text = drv["updated_datetime"].ToString();
                DateTime dt = Convert.ToDateTime(drv["updated_datetime"].ToString());
                lbADUpdatedate.Text = dt.ToString("dd MMMM yyyy", th);
                lbADoctype.Text = drv["doctype_code"].ToString();

            }
        }
        private void DDLbinddata()
        {
            DataTable dt = new DataTable();
            DataTable dtBid = new DataTable();
            dt = objJobDb.GetJobRelate(hidJobNo.Value, "");
            int i = 0;
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    //DDLBidno.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString()));
                    Multiselect_Job.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString() + "," + dr["bid_revision"].ToString()));
                    Multiselect_DDJob.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString() + "," + dr["bid_revision"].ToString()));
                    Multiselect_DocJob.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString() + "," + dr["bid_revision"].ToString()));
                    //Multiselect_AddJob.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString() + "," + dr["bid_revision"].ToString()));
                    ddl_bidPart.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString() + "," + dr["bid_revision"].ToString()));
                    Multiselect_DocSelectJob.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString() + "," + dr["bid_revision"].ToString()));

                    //Multiselect_AddSch.Items.Insert(i, new ListItem(dr["schedule_name"].ToString(), dr["schedule_no"].ToString()));
                    i++;
                }

            }
            dt = objJobDb.getDDLScheNo(hidJobNo.Value, rev.Value);
            //Multiselect_AddSch.Items.Clear();
            //Multiselect_AddSch.DataSource = dt;
            //Multiselect_AddSch.DataTextField = "schedule_name";
            //Multiselect_AddSch.DataValueField = "schedule_no";
            //Multiselect_AddSch.DataBind();
            ddl_schedulePart.Items.Clear();
            ddl_schedulePart.DataSource = dt;
            ddl_schedulePart.DataTextField = "schedule_name";
            ddl_schedulePart.DataValueField = "schedule_no";
            ddl_schedulePart.DataBind();
            //for (int a  = 0; a < 4; a++)
            //{
            //    Multiselect_AddSch.Items.Insert(0, new ListItem((a + 1).ToString(), (a + 1).ToString()));
            //}

            //dtBid = objJobDb.GetJobRelate(hidJobNo.Value, DDLBidno.SelectedValue);
            //if (dtBid.Rows.Count > 0)
            //{
            //    tbEqRev.Text = dtBid.Rows[0]["bid_revision"].ToString();
            //    tbEqMas.Text = dtBid.Rows[0]["eq_desc"].ToString();
            //    tbEqSchno.Text = dtBid.Rows[0]["schedule_no"].ToString();
            //    tbEqSchname.Text = dtBid.Rows[0]["schedule_name"].ToString();

            //}
            clsMyTaskJob objDB = new clsMyTaskJob();
            DDLAllDoc.Items.Clear();
            dt = objDB.getDDLDcoType("uploadsel");
            DDLAllDoc.DataSource = dt;
            DDLAllDoc.DataTextField = "doctype_name";
            DDLAllDoc.DataValueField = "doctype_code";
            DDLAllDoc.DataBind();

            DDLUpallDoc.Items.Clear();
            dt = objDB.getDDLDcoType("uploaddoc");
            DDLUpallDoc.DataSource = dt;
            DDLUpallDoc.DataTextField = "doctype_name";
            DDLUpallDoc.DataValueField = "doctype_code";
            DDLUpallDoc.DataBind();


            ddlSelectBid.SelectedIndex = 0;
            ddlSelectBid.Items.Clear();
            dt = objJobDb.getDDlItem(txtJobNo.Text, txtJobRev.Text, hidDept.Value, hidSect.Value);

            for (int d = 0; d < dt.Rows.Count; d++)
            {
                ddlSelectBid.Items.Insert(d, new ListItem(dt.Rows[d]["job_no"].ToString() + " Rev " + dt.Rows[d]["job_revision"].ToString(), dt.Rows[d]["job_no"].ToString()));

            }

            ddlSelectBom.SelectedIndex = 0;
            dt = objJobDb.getDDLBom(hidDept.Value, hidSect.Value);
            ddlSelectBom.Items.Clear();
            ddlSelectBom.DataSource = dt;
            ddlSelectBom.DataTextField = "bom_name";
            ddlSelectBom.DataValueField = "bom_code";
            ddlSelectBom.DataBind();

            dt = objDB.getDDLDcoType("design");
            DDLUpload.Items.Clear();
            DDLUpload.DataSource = dt;
            DDLUpload.DataTextField = "doctype_name";
            DDLUpload.DataValueField = "doctype_code";
            DDLUpload.DataBind();


            DDLDoctype.Items.Clear();
            DDLDoctype.Items.Insert(0, new ListItem("Design Drawing", "DD")); //TC EL 
            DDLDoctype.Items.Insert(1, new ListItem("Typical Drawing", "TD"));

            dt = objJobDb.getDDLHistory(hidDept.Value, hidSect.Value, hidAcID.Value, "");

            for (int d = 0; d < dt.Rows.Count; d++)
            {
                Multiselect_HisJob.Items.Insert(d, new ListItem(dt.Rows[d]["job_no"].ToString(), dt.Rows[d]["job_no"].ToString() + "," + dt.Rows[d]["job_revision"].ToString()));

            }
        }

        //protected void DDLBidno_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    DropDownList ddl = (DropDownList)sender;
        //    DataTable dt = new DataTable();
        //    dt = objJobDb.GetJobRelate(hidJobNo.Value, ddl.SelectedValue);

        //    if (dt.Rows.Count > 0)
        //    {
        //        tbEqRev.Text = dt.Rows[0]["bid_revision"].ToString();
        //        tbEqMas.Text = dt.Rows[0]["eq_desc"].ToString();
        //        tbEqSchno.Text = dt.Rows[0]["schedule_no"].ToString();
        //        tbEqSchname.Text = dt.Rows[0]["schedule_name"].ToString();
        //    }

        //    dvhidEq.Style["display"] = "block";
        //}

        protected void btAttsave_Click(object sender, EventArgs e)
        {
            DataTable dtDraw = objJobDb.GetDrawing(hidJobNo.Value, "draw", hidDept.Value, hidSect.Value, txtJobRev.Text);
            bool isChk = false;
            foreach (GridViewRow gvr in gvAttFile.Rows)
            {
                DataTable dt = new DataTable("dtDoc");
                CheckBox CbAttChk = (CheckBox)gvr.FindControl("CbAttChk");
                Label lbAttno = (Label)gvr.FindControl("lbAttno");
                Label lbGroup = (Label)gvr.FindControl("lbGroup");
                Label lbDivis = (Label)gvr.FindControl("lbDivis");
                Label lbDrawno = (Label)gvr.FindControl("lbDrawno");
                Label lbDescrip = (Label)gvr.FindControl("lbDescrip");
                Label lbFilename = (Label)gvr.FindControl("lbFilename");
                Label lbSheetno = (Label)gvr.FindControl("lbSheetno");
                Label lbRev = (Label)gvr.FindControl("lbRev");
                Label lbUpdatedate = (Label)gvr.FindControl("lbUpdatedate");
                Label lbUpby = (Label)gvr.FindControl("lbUpby");
                Label lbDocCode = (Label)gvr.FindControl("lbDocCode");
                //Label lbCreateby = (Label)gvr.FindControl("lbCreateby");
                //Label lbCreatedate = (Label)gvr.FindControl("lbCreatedate");  
                Label lbhidDate = (Label)gvr.FindControl("lbhidDate2");
                //Label lbRowid = (Label)gvr.FindControl("lbRowid");
                string rev = "";
                if (lbRev.Text == "" || lbRev.Text == null) rev = "0"; else rev = lbRev.Text;

                if (CbAttChk.Checked)
                {

                    isChk = true;
                    string[] Muljob = Multiselect_DDJob.SelectedValue.Split(',');
                    string bidno = "";
                    string bidrev = "";
                    if (Muljob.Length > 1)
                    {
                        bidno = Muljob[0];
                        bidrev = Muljob[1];
                    }
                    string[] Mulsch = Multiselect_DDSch.SelectedValue.Split(',');
                    string schno = "";
                    if (Mulsch.Length > 1)
                    {
                        schno = Mulsch[0];
                    }
                    AddDoc objAddDoc = new AddDoc();
                    objAddDoc.job_no = txtJobNo.Text;
                    objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                    if (bidno != "") objAddDoc.bid_no = bidno;
                    else objAddDoc.bid_no = null;
                    if (bidrev != "") objAddDoc.bid_revision = Convert.ToInt32(bidrev);
                    else objAddDoc.bid_revision = null;
                    if (schno != "") objAddDoc.schedule_no = Convert.ToInt32(schno);
                    else objAddDoc.schedule_no = null;
                    if (lbRev.Text != "") objAddDoc.doc_revision_no = Convert.ToInt32(rev);
                    else objAddDoc.doc_revision_no = null;
                    if (lbhidDate.Text != "") objAddDoc.updated_datetime = Convert.ToDateTime(lbhidDate.Text);
                    else objAddDoc.updated_datetime = DateTime.Now;
                    objAddDoc.depart_position_name = lbGroup.Text;
                    objAddDoc.section_position_name = lbDivis.Text;
                    objAddDoc.doctype_code = lbDocCode.Text;
                    if (lbDrawno.Text != "") objAddDoc.doc_nodeid = Convert.ToInt32(lbDrawno.Text);
                    else objAddDoc.doc_nodeid = 0;
                    objAddDoc.doc_name = lbFilename.Text;
                    objAddDoc.doc_sheetno = lbSheetno.Text;
                    objAddDoc.created_datetime = DateTime.Now;
                    objAddDoc.updated_by = lbUpby.Text;
                    objJobDb.InsertAddDoc(objAddDoc);
                    bind_data();
                }
                if (isChk == true)
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Add Successfully');", true);
                }
            }

            dvhiddraw.Style["display"] = "block";

        }

        //protected void lnk_res_Click(object sender, EventArgs e)
        //{
        //    LinkButton lnkbtn = sender as LinkButton;
        //    GridViewRow gvr = lnkbtn.NamingContainer as GridViewRow;
        //    TextBox tbReq = (TextBox)gvr.FindControl("tbReq");
        //    TextBox tbRes = (TextBox)gvr.FindControl("tbRes");
        //    Label lb_codeNo = (Label)gvr.FindControl("lb_codeNo");
        //    Label lb_fDesc2 = (Label)gvr.FindControl("lb_fDesc2");
        //    string A = "A";
        //    string EqCode = lb_codeNo.Text;
        //    string bidno = hidBidNo.Value;
        //    string bidsc = lb_fDesc2.Text;
        //    //Response.Redirect(string.Format("StockReservationRequest.aspx?zuserlogin={0}&zrefbid={1}&zrefbidsc={2}&ztype={3}", hidLogin.Value, tbReq.Text, tbRes.Text, A));
        //    Response.Redirect(string.Format("../stock/StockReservationRequest.aspx?zuserlogin={0}&zjob={1}&zeqcode={2}&zrefbid={3}&zrefbidsc={4}&ztype={5}", hidLogin.Value, hidJobNo.Value, EqCode, bidno, bidsc, A));
        //}

        protected void Multiselect_Job_SelectedIndexChanged(object sender, EventArgs e)
        {

            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value, Multiselect_Job.SelectedItem.Text);
            int i = 0;
            int k = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Rows[k]["schedule_name"].ToString() == "")
                {
                    Multiselect_Sch.SelectedIndex = -1;
                }
                else
                {
                    Multiselect_Sch.Items.Clear();
                    Multiselect_Sch.Items.Insert(i, new ListItem(dr["schedule_name"].ToString(), dr["schedule_no"].ToString() + "," + dr["schedule_name"].ToString()));
                    i++;
                    Multiselect_Sch.SelectedIndex = 0;
                }
                k++;

            }
            
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "block";
            dvAtt.Style["display"] = "table";
            dvAddDocument.Style["display"] = "none";
            dvDocview.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            //TabUploadDoc.Style["display"] = "none";
            //TabSelectDoc.Style["display"] = "none";
            dvhiddraw.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            TabUpload.Attributes.Add("class", "tab-pane active");
            TabSelect.Attributes.Add("class", "tab-pane");
            liUp.Attributes.Add("class", "active");
            //liSel.Attributes.Remove("class");
            dvPopAdd.Style["display"] = "none";
        }
        protected void Multiselect_DDJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value, Multiselect_DDJob.SelectedItem.Text);
            int i = 0;
            int k = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Rows[k]["schedule_name"].ToString() == "")
                {

                }
                else
                {
                    Multiselect_DDSch.Items.Clear();
                    Multiselect_DDSch.Items.Insert(i, new ListItem(dr["schedule_name"].ToString(), dr["schedule_no"].ToString() + "," + dr["schedule_name"].ToString()));
                    i++;
                }
                k++;

            }
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "block";
            dvAtt.Style["display"] = "table";
            dvAddDocument.Style["display"] = "none";
            dvDocview.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            //TabUploadDoc.Style["display"] = "none";
            //TabSelectDoc.Style["display"] = "none";
            dvhiddraw.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "none";

            TabSelect.Attributes.Add("class", "tab-pane active");
            TabUpload.Attributes.Add("class", "tab-pane");
            //liSel.Attributes.Add("class", "active");
            liUp.Attributes.Remove("class");

        }
        protected void DDLDoctype_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtAdddoc = objJobDb.GetAttach("", hidDept.Value, hidSect.Value);
            DataView dv = new DataView(dtAdddoc);
            dv.RowFilter = "doctype_code ='" + DDLDoctype.SelectedValue + "'";
            if (dv.Count > 0)
            {
                gvAttFile.DataSource = dv;
                gvAttFile.DataBind();
            }
            else
            {
                gvAttFile.DataSource = null;
                gvAttFile.DataBind();
            }
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "block";
            dvAtt.Style["display"] = "table";
            dvAddDocument.Style["display"] = "none";
            dvDocview.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            //TabUploadDoc.Style["display"] = "none";
            //TabSelectDoc.Style["display"] = "none";
            dvhiddraw.Style["display"] = "block";
            TabSelect.Attributes.Add("class", "tab-pane active");
            TabUpload.Attributes.Add("class", "tab-pane");
            //liSel.Attributes.Add("class", "active");
            liUp.Attributes.Remove("class");
        }

        protected void btaddDocall_Click(object sender, EventArgs e)
        {
            bool isChk = false;
            foreach (GridViewRow gvr in gvAddDoc.Rows)
            {
                Label lbDGroup = (Label)gvr.FindControl("lbDGroup");
                Label lbDocDivis = (Label)gvr.FindControl("lbDocDivis");
                Label lbADoctype = (Label)gvr.FindControl("lbADoctype");
                Label lbADno = (Label)gvr.FindControl("lbADno");
                Label lbDescrip = (Label)gvr.FindControl("lbDescription");
                Label lbFilename = (Label)gvr.FindControl("lbFilename");
                Label lbDSheetno = (Label)gvr.FindControl("lbDSheetno");
                Label lbADRev = (Label)gvr.FindControl("lbADRev");
                Label lbADUpdatedate = (Label)gvr.FindControl("lbADUpdatedate");
                Label lbADUpby = (Label)gvr.FindControl("lbADUpby");
                Label lbhidDate = (Label)gvr.FindControl("lbhidADDate");
                CheckBox cb = (CheckBox)gvr.FindControl("CbDChk");

                string rev = "";
                if (lbADRev.Text == "" || lbADRev.Text == null) rev = "0"; else rev = lbADRev.Text;

                if (cb.Checked)
                {
                    isChk = true;
                    string[] Muljob = Multiselect_DocSelectJob.SelectedValue.Split(',');
                    string bidno = "";
                    string bidrev = "";
                    if (Muljob.Length > 1)
                    {
                        bidno = Muljob[0];
                        bidrev = Muljob[1];
                    }
                    string[] Mulsch = Multiselect_DocSelectSch.SelectedValue.Split(',');
                    string schno = "";
                    if (Mulsch.Length > 1)
                    {
                        schno = Mulsch[0];
                    }
                    AddDoc objAddDoc = new AddDoc();
                    objAddDoc.job_no = txtJobNo.Text;
                    objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                    if (bidno != "") objAddDoc.bid_no = bidno;
                    else objAddDoc.bid_no = null;
                    if (bidrev != "") objAddDoc.bid_revision = Convert.ToInt32(bidrev);
                    else objAddDoc.bid_revision = null;
                    if (schno != "") objAddDoc.schedule_no = Convert.ToInt32(schno);
                    else objAddDoc.schedule_no = null;
                    if (lbADRev.Text != "") objAddDoc.doc_revision_no = Convert.ToInt32(rev);
                    else objAddDoc.doc_revision_no = null;
                    if (lbhidDate.Text != "") objAddDoc.updated_datetime = Convert.ToDateTime(lbhidDate.Text);
                    else objAddDoc.updated_datetime = DateTime.Now;
                    objAddDoc.depart_position_name = lbDGroup.Text;
                    objAddDoc.section_position_name = lbDocDivis.Text;
                    objAddDoc.doctype_code = lbADoctype.Text;
                    if (lbADno.Text != "") objAddDoc.doc_nodeid = Convert.ToInt32(lbADno.Text);
                    else objAddDoc.doc_nodeid = 0;
                    objAddDoc.doc_name = lbFilename.Text;
                    objAddDoc.doc_sheetno = lbDSheetno.Text;
                    objAddDoc.created_datetime = DateTime.Now;
                    objAddDoc.updated_by = lbADUpby.Text;
                    objJobDb.InsertAddDoc(objAddDoc);
                    bind_data();
                }

                if (isChk == true)
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Add Successfully');", true);
                }
            }
            dvhidDoc.Style["display"] = "block";
        }

        protected void DDLAllDoc_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dtAdddoc = objJobDb.GetAttach("", hidDept.Value, hidSect.Value);
            DataView dv = new DataView(dtAdddoc);
            dv.RowFilter = "doctype_code ='" + DDLAllDoc.SelectedValue + "'";
            if (dv.Count > 0)
            {
                gvAddDoc.DataSource = dv;
                gvAddDoc.DataBind();
            }
            else
            {
                gvAddDoc.DataSource = null;
                gvAddDoc.DataBind();
            }
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "none";
            dvAtt.Style["display"] = "none";
            dvAddDocument.Style["display"] = "table";
            dvDocview.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            //dvSelect.Style["display"] = "block";
            //dvUpload.Style["display"] = "none";
            dvhidDoc.Style["display"] = "block";
            dvHistory.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            TabSelectDoc.Attributes.Add("class", "tab-pane active");
            TabUploadDoc.Attributes.Add("class", "tab-pane");
            liSelDoc.Attributes.Add("class", "active");
            liUpDoc.Attributes.Remove("class");
        }
        protected void btAttcencel_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
            dvhiddraw.Style["display"] = "block";
        }
        //protected void btnCloseDr_Click(object sender, EventArgs e)
        //{
        //    myModal.Style["display"] = "none";
        //    dvhiddraw.Style["display"] = "block";
        //}
        //protected void btcancelDocall_Click(object sender, EventArgs e)
        //{
        //    myModal.Style["display"] = "none";
        //    dvhidDoc.Style["display"] = "block";
        //}
        //protected void btnClosealldoc_Click(object sender, EventArgs e)
        //{
        //    myModal.Style["display"] = "none";
        //    dvhidDoc.Style["display"] = "block";
        //}
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            //string flag = "";
            //if (rdbEquip.Checked == true)
            //{
            //    flag = "Equi";
            //}
            //else if (rdbBom.Checked == true)
            //{
            //    flag = "Bom";
            //}
            //else if (rdbMytemp.Checked == true)
            //{
            //    flag = "Temp";
            //}
            //else if (rdbPrejob.Checked == true)
            //{
            //    flag = "Job";
            //}
            //DataTable dt = objJobDb.getEqui(tbsearchtype.Text, ddlSelectBid.SelectedValue, ddlSelectBom.SelectedValue, hidDept.Value, hidSect.Value, flag);
            //if (dt.Rows.Count > 0)
            //{
            //    gvItem.DataSource = dt;
            //    gvItem.DataBind();
            //    gvItem.UseAccessibleHeader = true;
            //    gvItem.HeaderRow.TableSection = TableRowSection.TableHeader;
            //    gvItem.Visible = true;
            //}
            //else
            //{
            //    gvItem.DataSource = null;
            //    gvItem.DataBind();
            //    gvItem.Visible = true;
            //}
            //myModal.Style["display"] = "block";
            //dvPopAddEq.Style["display"] = "block";
            //dvpopUpload.Style["display"] = "none";
            //dvAddDocument.Style["display"] = "none";
            //dvPopDocview.Style["display"] = "none";
            ////TabUploadDoc.Style["display"] = "none";
            ////TabSelectDoc.Style["display"] = "none";
            //if (rdbEquip.Checked == true)
            //{
            //    rdbEquip.Checked = true;
            //}
            //else if (rdbBom.Checked == true)
            //{
            //    rdbBom.Checked = true;
            //}
            //else if (rdbMytemp.Checked == true)
            //{
            //    rdbMytemp.Checked = true;
            //}
            //else if (rdbPrejob.Checked == true)
            //{
            //    rdbPrejob.Checked = true;
            //}
            bindgvItem();
        }
        protected void bindgvItem()
        {
            DataTable dt = new DataTable();
            string flag = "";
            if (rdbEquip.Checked == true)
            {
                flag = "Equi";
            }
            else if (rdbBom.Checked == true)
            {
                flag = "Bom";
            }
            //else if (rdbMytemp.Checked == true)
            //{
            //    flag = "Temp";
            //}
            //else if (rdbPrejob.Checked == true)
            //{
            //    flag = "Job";
            //}
            string GetAll = hidSearchPart.Value;
            string parts = "";
            if (GetAll == "all" || GetAll == "")
            {
                //foreach (ListItem listItem in ddl_searchPart.Items)
                //{
                //    if (listItem.Value!="all")
                //    {
                //        if (parts == "") parts = "'" + listItem.Value + "'";
                //        else parts += ",'" + listItem.Value + "'";
                //    }
                //}
                parts = "";
            }
            else
            {
                parts = "'" + GetAll + "'";
            }

            if (hidChkSupply.Value == "true")
            {
                dt = objJobDb.getEqui(tbsearchtype.Text, ddlSelectBid.SelectedValue, ddlSelectBom.SelectedValue, hidDept.Value, hidSect.Value, flag, hidAllPart.Value, parts);
            }
            else
            {
                dt = objJobDb.getEqui(tbsearchtype.Text, ddlSelectBid.SelectedValue, ddlSelectBom.SelectedValue, hidDept.Value, hidSect.Value, flag, "", parts);
            }
            //DataTable dt = objJobDb.getEqui(tbsearchtype.Text, ddlSelectBid.SelectedValue, ddlSelectBom.SelectedValue, hidDept.Value, hidSect.Value, flag);
            if (dt.Rows.Count > 0)
            {
                gvItem.DataSource = dt;
                gvItem.DataBind();
                gvItem.UseAccessibleHeader = true;
                gvItem.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvItem.Visible = true;
            }
            else
            {
                gvItem.DataSource = null;
                gvItem.DataBind();
                gvItem.Visible = true;
            }
            btnAddItem.Style["visibility"] = "vissible";
            myModal.Style["display"] = "block";
            dvPopAddEq.Style["display"] = "block";
            dvpopUpload.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dvPopAdd.Style["display"] = "none";
            //TabUploadDoc.Style["display"] = "none";
            //TabSelectDoc.Style["display"] = "none";
            if (rdbEquip.Checked == true)
            {
                rdbEquip.Checked = true;
            }
            else if (rdbBom.Checked == true)
            {
                rdbBom.Checked = true;
            }
            //else if (rdbMytemp.Checked == true)
            //{
            //    rdbMytemp.Checked = true;
            //}
            //else if (rdbPrejob.Checked == true)
            //{
            //    rdbPrejob.Checked = true;
            //}
        }
        protected void btnAddItem_Click(object sender, EventArgs e)
        {
            int i = 1;
            int flag = 0;
            int no = 0;
            if (hidNo.Value != "") no = Convert.ToInt32(hidNo.Value);
            DataTable dt = new DataTable();
            //dt.Columns.Add("dept");
            //dt.Columns.Add("sec");
            //dt.Columns.Add("MeterialGrp");
            //dt.Columns.Add("EquipCode");
            //dt.Columns.Add("Legend");
            //dt.Columns.Add("Part");
            //dt.Columns.Add("FullDesc");
            //dt.Columns.Add("ShortDesc");
            //dt.Columns.Add("EquipType");
            //dt.Columns.Add("EquipGrp");
            //dt.Columns.Add("Unit");
            //dt.Columns.Add("AddDesc");
            //dt.Columns.Add("RequiredQTY");
            //dt.Columns.Add("type");
            //dt.Columns.Add("view");
            //dt.Columns.Add("equipBom");
            dt.Columns.Add("depart_position_name");
            dt.Columns.Add("section_position_name");
            dt.Columns.Add("equip_mas_grp");
            dt.Columns.Add("equip_code");
            dt.Columns.Add("legend");
            dt.Columns.Add("equip_item_no");
            dt.Columns.Add("equip_full_desc");
            dt.Columns.Add("unit");
            dt.Columns.Add("equip_add_desc");
            dt.Columns.Add("equip_standard_drawing");
            dt.Columns.Add("equip_qty");
            dt.Columns.Add("equip_code2");
            dt.Columns.Add("currency");
            dt.Columns.Add("equip_incoterm");
            dt.Columns.Add("factory_test");
            dt.Columns.Add("manual_test");
            dt.Columns.Add("routine_test");
            dt.Columns.Add("waive_test");
            dt.Columns.Add("supply_equip_unit_price");
            dt.Columns.Add("supply_equip_amonut");
            dt.Columns.Add("local_exwork_unit_price");
            dt.Columns.Add("local_exwork_amonut");
            dt.Columns.Add("local_tran_unit_price");
            dt.Columns.Add("local_tran_amonut");
            dt.Columns.Add("type");
            dt.Columns.Add("view");
            dt.Columns.Add("part");
            dt.Columns.Add("edit");
            dt.Columns.Add("bid_no");
            dt.Columns.Add("rowid");
            dt.Columns.Add("Part_Sort");
            string ispart = "";
            //int itmno = 0;
            foreach (GridViewRow grv in gvItem.Rows)
            {
                //CheckBox cb_sub = (CheckBox)grv.FindControl("cb_sub");
                //Label lb_division = (Label)grv.FindControl("lb_division");
                //Label lb_depm = (Label)grv.FindControl("lb_depm");
                //Label lb_mertGrp = (Label)grv.FindControl("lb_mertGrp");
                //Label lb_equiCode = (Label)grv.FindControl("lb_equiCode");
                //Label lb_legend = (Label)grv.FindControl("lb_legend");
                //Label lb_fullDesc = (Label)grv.FindControl("lb_fullDesc");
                //Label lb_shortDesc = (Label)grv.FindControl("lb_shortDesc");
                //Label lb_equiType = (Label)grv.FindControl("lb_equiType");
                //Label lb_equiGrp = (Label)grv.FindControl("lb_equiGrp");
                //Label lbUnit = (Label)grv.FindControl("lbUnit");
                //if (cb_sub.Checked == true)
                //{
                //    addMytaskEquipJob objEqui = new addMytaskEquipJob();
                //    objEqui.section_position_name = lb_division.Text;
                //    objEqui.depart_position_name = lb_depm.Text;
                //    objEqui.job_no = tbAddjob.Text;
                //    if (Multiselect_AddJob.SelectedItem.Text != "") objEqui.bid_no = Multiselect_AddJob.SelectedItem.Text;
                //    else objEqui.bid_no = "0";
                //    objEqui.equip_code = lb_equiCode.Text;
                //    objEqui.created_by = hidLogin.Value;
                //    objEqui.created_datetime = DateTime.Now;
                //    objEqui.updated_by = hidLogin.Value;
                //    objEqui.updated_datetime = DateTime.Now;
                //    objEqui.equip_add_desc = tbAddPath.Text;
                //    if (Multiselect_AddJob.SelectedValue != "") objEqui.bid_revision = Convert.ToInt32(Multiselect_AddJob.SelectedValue); 
                //    else objEqui.bid_revision = 0;

                //    if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);
                //    else objEqui.job_revision = 0;
                //    if (Multiselect_AddSch.SelectedValue != "") objEqui.schedule_no = Convert.ToInt32(Multiselect_AddSch.SelectedValue); 
                //    else objEqui.schedule_no = 0;
                //    objJobDb.insertMyTaskEquip(objEqui);
                //    //bind_equiList();
                //}
                CheckBox cb_sub = (CheckBox)grv.FindControl("cb_sub");
                Label lb_division = (Label)grv.FindControl("lb_division");
                Label lb_depm = (Label)grv.FindControl("lb_depm");
                Label lb_mertGrp = (Label)grv.FindControl("lb_mertGrp");
                //ListBox ddl_meterialGrp = (ListBox)grv.FindControl("ddl_meterialGrp");
                Label lb_equiCode = (Label)grv.FindControl("lb_equiCode");
                Label lb_legend = (Label)grv.FindControl("lb_legend");
                Label lb_fullDesc = (Label)grv.FindControl("lb_fullDesc");
                Label lb_shortDesc = (Label)grv.FindControl("lb_shortDesc");
                Label lb_equiType = (Label)grv.FindControl("lb_equiType");
                Label lb_equiGrp = (Label)grv.FindControl("lb_equiGrp");
                Label lbUnit = (Label)grv.FindControl("lbUnit");
                Label lb_Part = (Label)grv.FindControl("lb_Part");
                Label lb_equipBom = (Label)grv.FindControl("lb_equipBom");
                CheckBox cb_explan = (CheckBox)grv.FindControl("cb_explan");
                if (cb_sub.Checked == true)
                {
                    DataTable dtitmno = new DataTable();
                    byte[] bytes = null;
                    System.Text.ASCIIEncoding encode = new System.Text.ASCIIEncoding();
                    if (hidChkSupply.Value=="true")  dtitmno = objJobDb.GetItmNo_sup(hidJobNo.Value, tbAddPath.Text);
                   else  dtitmno = objJobDb.GetItmNo(hidJobNo.Value, tbAddPath.Text,hidBidNo.Value);
                    if (dtitmno.Rows.Count > 0)
                    {
                        string[] spl = dtitmno.Rows[dtitmno.Rows.Count - 1]["equip_item_no"].ToString().Split('-');
                        if (spl.Length == 2)
                        {
                            if (ViewState["itmno"].ToString() == "")
                            {
                                double itm = double.Parse(spl[1]);
                                int num = Convert.ToInt32(itm) + 1;
                                ispart = tbAddPath.Text + "-" + num;
                                ViewState["itmno"] = num;
                                //ispart = tbAddPath.Text+"-" + (Convert.ToInt32(spl[1])+1);
                                //ViewState["itmno"] = (Convert.ToInt32(spl[1])+1);
                            }
                            else
                            {
                                int num =Convert.ToInt32(ViewState["itmno"]);
                                ViewState["itmno"]= num+=1;
                                ispart = tbAddPath.Text+"-" + ViewState["itmno"];
                            }
                            bytes = encode.GetBytes(ispart);
                        }
                    }
                    else
                    {
                        if (ViewState["itmno"].ToString()=="")
                        {
                            ispart = tbAddPath.Text + "-1";
                            ViewState["itmno"] = "1";
                        }
                        else
                        {
                            int num = Convert.ToInt32(ViewState["itmno"]);
                            ViewState["itmno"] = num += 1;
                            ispart = tbAddPath.Text + "-" + ViewState["itmno"];
                        }
                        bytes = encode.GetBytes(ispart);
                    }
                    DataRow dr = dt.NewRow();
                    if (tbAddPath.Text != "" && hidChkSupply.Value != "true")
                    {
                        addMytaskEquipJob objEqui = new addMytaskEquipJob();
                        //objEqui.equip_mas_grp = ddl_meterialGrp.SelectedValue;
                        objEqui.equip_full_desc = lb_fullDesc.Text.Replace("'", "''");
                        objEqui.section_position_name = lb_division.Text;
                        objEqui.depart_position_name = lb_depm.Text;
                        objEqui.job_no = tbAddjob.Text;
                        if (hidBidNo.Value != "") objEqui.bid_no = hidBidNo.Value;
                        objEqui.activity_id = hidAcID.Value;
                        objEqui.step = "J";
                        objEqui.equip_code = lb_equiCode.Text;
                        objEqui.created_by = hidLogin.Value;
                        objEqui.created_datetime = DateTime.Now;
                        objEqui.updated_by = hidLogin.Value;
                        objEqui.updated_datetime = DateTime.Now;
                        objEqui.path_code = tbAddPath.Text;
                        if (part.Value == tbAddPath.Text)
                        {
                            no++;
                            objEqui.equip_item_no = tbAddPath.Text + "-" + no;

                        }
                        else
                        {
                            objEqui.equip_item_no = tbAddPath.Text + "-" + i;
                            no = i;
                        }
                        //else  objEqui.equip_item_no = tbAddPath.Text + "-" + i;
                        if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        objEqui.equip_unit = lbUnit.Text;
                        if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);//Convert.ToInt32( listItem.Value);
                        if (tb_schNoPart.Text != "") objEqui.schedule_no = Convert.ToInt32(tb_schNoPart.Text);
                        objEqui.is_breakdown = cb_brakedown.Checked;
                        objEqui.schedule_name = tb_schNamePart.Text;

                        //objJobDb.insertMyTaskEquip(objEqui);
                        if (cb_explan.Checked == true)
                        {
                            DataTable dtAdd = objJobDb.getEquipBom(lb_equiCode.Text);
                            dr = dt.NewRow();
                            dr["depart_position_name"] = lb_depm.Text;
                            dr["section_position_name"] = lb_division.Text;
                            dr["equip_mas_grp"] = lb_mertGrp.Text;
                            dr["equip_code"] = lb_equiCode.Text;
                            dr["legend"] = lb_legend.Text;
                            dr["equip_item_no"] = tbAddPath.Text+"-1";// tbAddPath.Text + "-" + (grv.RowIndex + 1).ToString();
                            dr["equip_full_desc"] = lb_fullDesc.Text.Replace("'", "''");
                            dr["unit"] = lbUnit.Text;
                            dr["equip_add_desc"] = "";
                            dr["equip_qty"] = "";
                            dr["type"] = rdbBom.Checked == true ? "1" : "0";
                            dr["view"] = "1";
                            dr["equip_code2"] = lb_equipBom.Text;
                            dr["part"] = tbAddPath.Text;
                            dr["rowid"] = "";
                            bytes = encode.GetBytes(tbAddPath.Text + "-1");
                            dr["Part_Sort"] = String.Join(",", bytes);
                            dt.Rows.Add(dr);

                            if (dtAdd.Rows.Count > 0)
                            {
                                int itm = 1;
                                foreach (DataRow dtRow in dtAdd.Rows)
                                {
                                    dr = dt.NewRow();
                                    dr["depart_position_name"] = dtRow["equip_gong"].ToString();
                                    dr["section_position_name"] = dtRow["equip_pnag"].ToString();
                                    dr["equip_mas_grp"] = dtRow["materialgroup"].ToString();
                                    dr["equip_code"] = dtRow["equip_code"].ToString();
                                    dr["legend"] = dtRow["legend"].ToString();
                                    dr["equip_item_no"] = tbAddPath.Text+"-1."+itm;
                                    dr["equip_full_desc"] = dtRow["equip_desc_full"].ToString().Replace("'", "''");
                                    dr["unit"] = dtRow["unit"].ToString();
                                    dr["equip_add_desc"] = "";
                                    dr["equip_qty"] = "";
                                    dr["type"] = rdbBom.Checked == true ? "1" : "0";
                                    dr["view"] = "1";
                                    dr["equip_code2"] = dtRow["equip_code"].ToString();
                                    dr["part"] = tbAddPath.Text;
                                    dr["edit"] = "2";
                                    dr["rowid"] = "";
                                    bytes = encode.GetBytes(tbAddPath.Text + "-1." + itm);
                                    dr["Part_Sort"] = String.Join(",", bytes);
                                    dt.Rows.Add(dr);
                                    itm++;
                                }
                            }
                        }
                        else
                        {
                            dr = dt.NewRow();
                            dr["depart_position_name"] = lb_depm.Text;
                            dr["section_position_name"] = lb_division.Text;
                            dr["equip_mas_grp"] = lb_mertGrp.Text;
                            dr["equip_code"] = lb_equiCode.Text;
                            dr["legend"] = lb_legend.Text;
                            dr["equip_item_no"] = ispart;// tbAddPath.Text + "-" + (grv.RowIndex + 1).ToString();
                            dr["equip_full_desc"] = lb_fullDesc.Text.Replace("'", "''");
                            dr["unit"] = lbUnit.Text;
                            dr["equip_add_desc"] = "";
                            dr["equip_qty"] = "";
                            dr["type"] = rdbBom.Checked == true ? "1" : "0";
                            dr["view"] = "0";
                            dr["equip_code2"] = lb_equipBom.Text;
                            dr["part"] = tbAddPath.Text;
                            dr["rowid"] = "";
                            dr["Part_Sort"] = String.Join(",", bytes);
                            dt.Rows.Add(dr);
                        }
                        i++;

                    }
                    else if (hidChkSupply.Value == "true")
                    {
                        if (cb_explan.Checked == true)
                        {
                            string part = "";
                            int no2 = 0;
                            int i2 = 1;
                            DataTable dtAdd = objJobDb.getEquipBom(lb_equiCode.Text);
                            foreach (DataRow dtRow in dtAdd.Rows)
                            {
                                string thispart = "";
                                if (part == dtRow["parts"].ToString())
                                {
                                    no2++;
                                    thispart = dtRow["parts"].ToString() + "-" + no2;
                                }
                                else
                                {
                                    thispart = dtRow["parts"].ToString() + "-" + i2;
                                    no2 = i2;
                                }
                                part = dtRow["parts"].ToString();

                                dr = dt.NewRow();
                                dr["depart_position_name"] = dtRow["equip_gong"].ToString();
                                dr["section_position_name"] = dtRow["equip_pnag"].ToString();
                                dr["equip_mas_grp"] = dtRow["materialgroup"].ToString();
                                dr["equip_code"] = dtRow["equip_code"].ToString();
                                dr["legend"] = dtRow["legend"].ToString();
                                dr["equip_item_no"] =  thispart;
                                dr["equip_full_desc"] = dtRow["equip_desc_full"].ToString().Replace("'", "''");
                                dr["unit"] = dtRow["unit"].ToString();
                                dr["equip_add_desc"] = "";
                                dr["equip_qty"] = "";
                                dr["type"] = rdbBom.Checked == true ? "1" : "0";
                                dr["view"] = "1";
                                dr["equip_code2"] = dtRow["equip_code"].ToString();
                                dr["part"] = dtRow["parts"].ToString();
                                dr["edit"] = "2";
                                dr["rowid"] = "";
                                dr["Part_Sort"] = String.Join(",", bytes);
                                dt.Rows.Add(dr);
                            }
                            dr = dt.NewRow();
                            dr["depart_position_name"] = lb_depm.Text;
                            dr["section_position_name"] = lb_division.Text;
                            dr["equip_mas_grp"] = lb_mertGrp.Text;
                            dr["equip_code"] = lb_equiCode.Text;
                            dr["legend"] = lb_legend.Text;
                            dr["equip_item_no"] = lb_Part.Text + "-1";
                            dr["equip_full_desc"] = lb_fullDesc.Text.Replace("'", "''");
                            dr["unit"] = lbUnit.Text;
                            dr["equip_qty"] = "";
                            dr["type"] = rdbBom.Checked == true ? "1" : "0";
                            dr["view"] = "1";
                            dr["equip_code2"] = lb_equipBom.Text;
                            dr["part"] = lb_Part.Text;
                            dr["edit"] = "2";
                            dr["rowid"] = "";
                            dr["Part_Sort"] = String.Join(",", bytes);
                            dt.Rows.Add(dr);
                        }
                        else
                        {
                            DataTable dtPart = objJobDb.sumPart(txtJobNo.Text, txtJobRev.Text, lb_Part.Text);
                            if (dtPart.Rows.Count > 0)
                            {
                                int j = 1;
                                foreach (DataRow item in dtPart.Rows)
                                {
                                    dr = dt.NewRow();
                                    dr["depart_position_name"] = lb_depm.Text;
                                    dr["section_position_name"] = lb_division.Text;
                                    dr["equip_mas_grp"] = lb_mertGrp.Text;
                                    dr["equip_code"] = lb_equiCode.Text;
                                    dr["legend"] = lb_legend.Text;
                                    dr["equip_item_no"] = lb_Part.Text + "-" + j;
                                    dr["equip_full_desc"] = lb_fullDesc.Text.Replace("'", "''");
                                    dr["unit"] = lbUnit.Text;
                                    dr["equip_qty"] = "";
                                    dr["type"] = rdbBom.Checked == true ? "1" : "0";
                                    dr["view"] = "0";
                                    dr["equip_code2"] = lb_equipBom.Text;
                                    dr["part"] = lb_Part.Text;
                                    dr["edit"] = "2";
                                    dr["bid_no"] = item["bid_no"].ToString();
                                    dr["rowid"] = "";
                                    dr["Part_Sort"] = String.Join(",", bytes);
                                    dt.Rows.Add(dr);
                                    j++;
                                }
                            }

                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please add part to the equipment.');", true);
                        myModal.Style["display"] = "block";
                        //dv_addEqui.Style["display"] = "block";
                        bindgvItem();
                        
                    }
                }

            }
            if (rdbOther.Checked == true && tbAddPath.Text != "")
            {
                addMytaskEquipJob objEqui = new addMytaskEquipJob();
                objEqui.equip_full_desc = "";
                objEqui.section_position_name = "";
                objEqui.depart_position_name = "";
                objEqui.job_no = tbAddjob.Text;
                if (hidBidNo.Value != "") objEqui.bid_no = hidBidNo.Value;
                objEqui.activity_id = hidAcID.Value;
                objEqui.step = "J";
                objEqui.equip_code = "";
                objEqui.created_by = hidLogin.Value;
                objEqui.created_datetime = DateTime.Now;
                objEqui.updated_by = hidLogin.Value;
                objEqui.updated_datetime = DateTime.Now;
                objEqui.path_code = tbAddPath.Text;
                objEqui.equip_item_no = tbAddPath.Text + "-" + i;
                if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                objEqui.equip_unit = "";
                if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);//Convert.ToInt32( listItem.Value);
                if (tb_schNoPart.Text != "") objEqui.schedule_no = Convert.ToInt32(tb_schNoPart.Text);
                objEqui.is_breakdown = cb_brakedown.Checked;
                //objJobDb.insertMyTaskEquip(objEqui);
                DataRow dr = dt.NewRow();
                dr["depart_position_name"] = hidDept.Value;
                dr["section_position_name"] = hidSect.Value;
                dr["equip_mas_grp"] = "";
                dr["equip_code"] = "";
                dr["legend"] = "";
                dr["equip_item_no"] = "";
                dr["equip_full_desc"] = "";
                dr["unit"] = "";
                dr["equip_full_desc"] = "";
                dr["equip_qty"] = "";
                dr["type"] = rdbBom.Checked == true ? "1" : "0";
                dr["view"] = "0";
                dr["equip_code2"] = "";
                dr["part"] = "";
                dr["edit"] = "2";
                dr["rowid"] = "";
                dr["Part_Sort"] = "";
                dt.Rows.Add(dr);
            }

            part.Value = tbAddPath.Text;
            hidNo.Value = no.ToString();
            dvhidEq.Style["display"] = "block";
            myModal.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dv_viewLegend.Style.Add("display", "none");
            dvPopAdd.Style["display"] = "none";
            bindgvItem();
            //dt.DefaultView.Sort = "equip_item_no asc";
            DataView dv = dt.DefaultView;
            dv.Sort = "Part_Sort asc";
            dt = dv.ToTable();
            SelEquip(dt);
            if (hidSelPart.Value =="") ViewState["chkflag"] = "1";
            else ViewState["chkflag"] = "";
            //ViewState["chkflag"] = "";
        }
        protected void SelEquip(DataTable dt)
        {
            DataTable dtadd = (DataTable)Session["dtAdd"];
            DataRow row;
            if (dtadd != null)
            {
                foreach (DataRow dtRow in dtadd.Rows)
                {
                    row = dt.NewRow();
                    row["depart_position_name"] = dtRow["depart_position_name"].ToString();
                    row["section_position_name"] = dtRow["section_position_name"].ToString();
                    row["equip_mas_grp"] = dtRow["equip_mas_grp"].ToString();
                    row["equip_code"] = dtRow["equip_code"].ToString();
                    row["legend"] = dtRow["legend"].ToString();
                    row["equip_item_no"] = dtRow["equip_item_no"].ToString();
                    row["equip_full_desc"] = dtRow["equip_full_desc"].ToString();
                    row["unit"] = dtRow["unit"].ToString();
                    row["equip_full_desc"] = dtRow["equip_full_desc"].ToString();
                    row["equip_qty"] = dtRow["equip_qty"].ToString();
                    row["type"] = dtRow["type"].ToString();
                    row["view"] = dtRow["view"].ToString();
                    row["equip_code2"] = dtRow["equip_code2"].ToString();
                    row["part"] = dtRow["part"].ToString();
                    row["edit"] = dtRow["edit"].ToString();
                    row["bid_no"] = dtRow["bid_no"].ToString();
                    row["rowid"] = dtRow["rowid"].ToString();
                    row["Part_Sort"] = dtRow["Part_Sort"].ToString();
                    dt.Rows.Add(row);
                }
            }
            else
            {
                DataTable dteq = (DataTable)ViewState["dataEQ"];
                if (dteq != null)
                {
                    foreach (DataRow item in dteq.Rows)
                    {
                        row = dt.NewRow();
                        row["depart_position_name"] = item["depart_position_name"].ToString();
                        row["section_position_name"] = item["section_position_name"].ToString();
                        row["equip_mas_grp"] = item["equip_mas_grp"].ToString();
                        row["equip_code"] = item["equip_code"].ToString();
                        row["legend"] = item["legend"].ToString();
                        row["equip_item_no"] = item["equip_item_no"].ToString();
                        row["equip_full_desc"] = item["equip_full_desc"].ToString();
                        row["unit"] = item["unit"].ToString();
                        row["equip_full_desc"] = item["equip_full_desc"].ToString();
                        row["equip_qty"] = item["equip_qty"].ToString();
                        row["type"] = item["type"].ToString();
                        row["view"] = item["view"].ToString();
                        row["equip_code2"] = item["equip_code2"].ToString();
                        row["part"] = item["part"].ToString();
                        row["equip_add_desc"] = item["equip_add_desc"].ToString();
                        row["equip_standard_drawing"] = item["equip_standard_drawing"].ToString();
                        row["currency"] = item["currency"].ToString();
                        row["equip_incoterm"] = item["equip_incoterm"].ToString();
                        row["factory_test"] = item["factory_test"].ToString();
                        row["manual_test"] = item["manual_test"].ToString();
                        row["routine_test"] = item["routine_test"].ToString();
                        row["waive_test"] = item["waive_test"].ToString();
                        row["supply_equip_unit_price"] = item["supply_equip_unit_price"].ToString();
                        row["supply_equip_amonut"] = item["supply_equip_amonut"].ToString();
                        row["local_exwork_unit_price"] = item["local_exwork_unit_price"].ToString();
                        row["local_exwork_amonut"] = item["local_exwork_amonut"].ToString();
                        row["local_tran_unit_price"] = item["local_tran_unit_price"].ToString();
                        row["local_tran_amonut"] = item["local_tran_amonut"].ToString();
                        row["edit"] = item["edit"].ToString();
                        row["rowid"] = item["rowid"].ToString();
                        row["Part_Sort"] = item["Part_Sort"].ToString();
                        dt.Rows.Add(row);
                    }

                }
            }
            
            if (dt.Rows.Count > 0)
            {
              
                gv_SelEquip.DataSource = dt;
                gv_SelEquip.DataBind();
                gv_SelEquip.Visible = true;
                btnCancelItem.Visible = true;
                Label12.Visible = true;
                cb_brakedown.Visible = true;
            }
            Session.Add("dtAdd", dt);
        }
        protected void Multiselect_DocJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value, Multiselect_DocJob.SelectedItem.Text);
            int i = 0;
            int k = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Rows[k]["schedule_name"].ToString() == "")
                {

                }
                else
                {
                    Multiselect_DocSch.Items.Clear();
                    Multiselect_DocSch.Items.Insert(i, new ListItem(dr["schedule_name"].ToString(), dr["schedule_no"].ToString() + "," + dr["schedule_name"].ToString()));
                    i++;
                    Multiselect_DocSch.SelectedIndex = 0;
                }
                k++;

            }

            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "none";
            dvAtt.Style["display"] = "none";
            dvAddDocument.Style["display"] = "table";
            dvDocview.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvhidDoc.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            TabUploadDoc.Attributes.Add("class", "tab-pane active");
            TabSelectDoc.Attributes.Add("class", "tab-pane");
            liUpDoc.Attributes.Add("class", "active");
            liSelDoc.Attributes.Remove("class");
            dvPopAdd.Style["display"] = "none";
        }


        protected void btnSaveDr_Click(object sender, EventArgs e)
        {
            Node jobFoldernode = new Node();
            Node DeptFoldernode = new Node();
            Node SectionFoldernode = new Node();
            Node Doctypenode = new Node();
            string docid = "";
            string DoctypeFoldername = DDLUpload.SelectedValue.ToString() + " - " + DDLUpload.SelectedItem.ToString();
            OTFunctions ot = new OTFunctions();
            jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidJobNo.Value);
            if (jobFoldernode == null)
            {
                jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidJobNo.Value);
            }
            DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
            if (DeptFoldernode == null)
            {
                DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
            }
            SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
            if (SectionFoldernode == null)
            {
                SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
            }
            Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            if (Doctypenode == null)
            {
                Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            }
            if (FileUpload1.FileName != "")
            {
                docid = ot.uploadDoc(Doctypenode.ID.ToString(), FileUpload1.FileName, FileUpload1.FileName, FileUpload1.FileBytes, "").ToString();

                string[] Muljob = Multiselect_Job.SelectedValue.Split(',');
                string bidno = "";
                string bidrev = "";
                if (Muljob.Length > 1)
                {
                    bidno = Muljob[0];
                    bidrev = Muljob[1];
                }
                string[] Mulsch = Multiselect_Sch.SelectedValue.Split(',');
                string schno = "";
                string schname = "";
                if (Mulsch.Length > 1)
                {
                    schno = Mulsch[0];
                    schname = Mulsch[1];
                }
                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                if (bidno != "") objAddDoc.bid_no = bidno;
                else objAddDoc.bid_no = null;
                if (bidrev != "") objAddDoc.bid_revision = Convert.ToInt32(bidrev);
                else objAddDoc.bid_revision = null;
                if (schno != "") objAddDoc.schedule_no = Convert.ToInt32(schno);
                else objAddDoc.schedule_no = null;
                objAddDoc.doc_revision_no = null;
                objAddDoc.updated_datetime = DateTime.Now;
                objAddDoc.depart_position_name = hidDept.Value;
                objAddDoc.section_position_name = hidSect.Value;
                objAddDoc.doctype_code = DDLUpload.SelectedValue;
                objAddDoc.doc_nodeid = Convert.ToInt32(docid);
                objAddDoc.doc_name = FileUpload1.FileName;
                objAddDoc.doc_desc = txtDescripttion.Text;
                objAddDoc.doc_sheetno = tb_sheetno.Text; ;
                objAddDoc.created_datetime = DateTime.Now;
                objAddDoc.created_by = hidLogin.Value;
                objAddDoc.updated_by = hidLogin.Value;
                objAddDoc.schedule_name = schname;//schname
                objAddDoc.process_id = hidPID.Value;
                objAddDoc.activId = hidAcID.Value;
                objJobDb.InsertAddDoc(objAddDoc);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Upload Successfully.');", true);
                bind_data();
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Choose File.');", true);
            }
            dvhiddraw.Style["display"] = "block";
        }
        public void DeleteNode(string strNodeId)
        {
            OTFunctions ot = new OTFunctions();
            Node thisNode = ot.getNodeByID(strNodeId);
            if (thisNode != null)
            {
                ot.deleteNodeID(thisNode.ParentID.ToString(), strNodeId);
            }
        }

        protected void imgDel_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgDel = sender as ImageButton;
            GridViewRow gvr = imgDel.NamingContainer as GridViewRow;
            Label lbGruop = (Label)gvr.FindControl("lbGruop");
            Label lbDivis = (Label)gvr.FindControl("lbDivis");
            Label lbDoct = (Label)gvr.FindControl("lbDoct");
            Label lbFilename = (Label)gvr.FindControl("lbFilename");
            Label lbSheet = (Label)gvr.FindControl("lbSheet");
            Label lbRevi = (Label)gvr.FindControl("lbRevi");
            Label lbUpby = (Label)gvr.FindControl("lbUpby");
            Label lbDocno = (Label)gvr.FindControl("lbDocno");
            Label lbBid = (Label)gvr.FindControl("lbBid");
            Label lbRevbid = (Label)gvr.FindControl("lbRevbiddr"); //kie23-12  
            Label lbDes = (Label)gvr.FindControl("lbDes");
            Label lbhidjob = (Label)gvr.FindControl("lbhidjob");
            Label lbhidjobrev = (Label)gvr.FindControl("lbhidjobrev");
            Label lbDoctSH = (Label)gvr.FindControl("lbDoctSH");
            string JobnoHis = "";
            if (Multiselect_HisJob.SelectedValue != "") JobnoHis = Multiselect_HisJob.SelectedItem.Text;
            if (ViewState["history"].ToString() == "1")
            {
                DataTable dtDraw = objJobDb.GetDrawing(JobnoHis, "draw", hidDept.Value, hidSect.Value, txtHisRev.Text);
                if (ViewState["first"] != null && ViewState["first"].ToString() != "") dtDraw = (DataTable)ViewState["first"];

                int a = gvr.RowIndex;
                if (ViewState["first"].ToString() == "")
                {

                    if (dtDraw.Rows.Count > 0)
                    {
                        dtDraw.Rows.RemoveAt(a);
                        gvDrawing.DataSource = dtDraw;
                        gvDrawing.DataBind();
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);
                        ViewState["first"] = dtDraw;
                    }
                }
                else
                {
                    if (dtDraw.Rows.Count > 0)
                    {
                        dtDraw.Rows.RemoveAt(a);
                        gvDrawing.DataSource = dtDraw;
                        gvDrawing.DataBind();
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);

                    }
                }

            }
            else
            {

                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                objAddDoc.bid_no = lbBid.Text;
                if (lbRevbid.Text != "" && lbRevbid.Text != null) objAddDoc.bid_revision = Convert.ToInt32(lbRevbid.Text);
                else objAddDoc.bid_revision = null;
                objAddDoc.schedule_no = null;
                if (lbRevi.Text != "" && lbRevi.Text != null) objAddDoc.doc_revision_no = Convert.ToInt32(lbRevi.Text);
                else objAddDoc.doc_revision_no = null;
                objAddDoc.updated_datetime = DateTime.Now;
                objAddDoc.depart_position_name = lbGruop.Text;
                objAddDoc.section_position_name = lbDivis.Text;
                objAddDoc.doctype_code = lbDoctSH.Text;
                objAddDoc.doc_nodeid = Convert.ToInt32(lbDocno.Text);
                objAddDoc.doc_name = lbFilename.Text;
                objAddDoc.doc_sheetno = lbSheet.Text;
                objAddDoc.created_datetime = DateTime.Now;
                objAddDoc.updated_by = lbUpby.Text;

                objJobDb.DeleteDrawing(objAddDoc);
                DeleteNode(lbDocno.Text);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);
                bind_data();
            }
        }

        protected void imgDocdel_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton imgDel = sender as ImageButton;
            GridViewRow gvr = imgDel.NamingContainer as GridViewRow;
            Label lbTDocNo = (Label)gvr.FindControl("lbTDocNo");
            Label lbGroup = (Label)gvr.FindControl("lbGroup");
            Label lbDivis = (Label)gvr.FindControl("lbDivis");
            Label lbDescrip = (Label)gvr.FindControl("lbDescrip");
            Label lbFilename = (Label)gvr.FindControl("lbFilename");
            Label lbSheetno = (Label)gvr.FindControl("lbSheetno");
            Label lbRev = (Label)gvr.FindControl("lbRev");
            Label lbUpdatedate = (Label)gvr.FindControl("lbUpdatedate");
            Label lbUpby = (Label)gvr.FindControl("lbUpby");
            Label lbDocCode = (Label)gvr.FindControl("lbDocCode");
            Label lbBidno = (Label)gvr.FindControl("lbBidno");

            Label lbSchname = (Label)gvr.FindControl("lbSchname");
            Label lbhidDate = (Label)gvr.FindControl("lbhidDate3");
            Label lbRevbid = (Label)gvr.FindControl("lbRevbid");
            Label lbDocno = (Label)gvr.FindControl("lbDocno");
            Label lbhidjob = (Label)gvr.FindControl("lbhidjob");
            Label lbhidjobrev = (Label)gvr.FindControl("lbhidjobrev");
            Label lbDoctSH = (Label)gvr.FindControl("lbDoctSH");
            string JobnoHis = "";
            if (Multiselect_HisJob.SelectedValue != "") JobnoHis = Multiselect_HisJob.SelectedItem.Text;
            if (ViewState["history"].ToString() == "1")
            {

                DataTable dtTask = objJobDb.GetDrawing(JobnoHis, "", hidDept.Value, hidSect.Value, txtHisRev.Text);
                if (ViewState["firstDoc"] != null && ViewState["firstDoc"].ToString() != "") dtTask = (DataTable)ViewState["firstDoc"];

                int a = gvr.RowIndex;
                if (ViewState["firstDoc"].ToString() == "")
                {

                    if (dtTask.Rows.Count > 0)
                    {
                        dtTask.Rows.RemoveAt(a);
                        gvTaskDoc.DataSource = dtTask;
                        gvTaskDoc.DataBind();
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);
                        ViewState["firstDoc"] = dtTask;
                    }
                }
                else
                {
                    if (dtTask.Rows.Count > 0)
                    {
                        dtTask.Rows.RemoveAt(a);
                        gvTaskDoc.DataSource = dtTask;
                        gvTaskDoc.DataBind();
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);

                    }
                }
            }
            else
            {
                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                objAddDoc.bid_no = lbBidno.Text;
                if (lbRevbid.Text != "" && lbRevbid.Text != null) objAddDoc.bid_revision = Convert.ToInt32(lbRevbid.Text);
                else objAddDoc.bid_revision = null;
                objAddDoc.schedule_no = null;
                if (lbRev.Text != "" && lbRev.Text != null) objAddDoc.doc_revision_no = Convert.ToInt32(lbRev.Text);
                else objAddDoc.doc_revision_no = null;
                objAddDoc.updated_datetime = DateTime.Now;
                objAddDoc.depart_position_name = lbGroup.Text;
                objAddDoc.section_position_name = lbDivis.Text;
                objAddDoc.doctype_code = lbDoctSH.Text;
                objAddDoc.doc_nodeid = Convert.ToInt32(lbDocno.Text);
                objAddDoc.doc_name = lbFilename.Text;
                objAddDoc.doc_sheetno = lbSheetno.Text;
                objAddDoc.created_datetime = DateTime.Now;
                objAddDoc.updated_by = lbUpby.Text;

                objJobDb.DeleteDrawing(objAddDoc);
                DeleteNode(lbDocno.Text);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully');", true);
                bind_data();
            }


        }

        protected void btnUploadFile_Click(object sender, EventArgs e)
        {
            Node jobFoldernode = new Node();
            Node DeptFoldernode = new Node();
            Node SectionFoldernode = new Node();
            Node Doctypenode = new Node();
            string docid = "";
            string DoctypeFoldername = DDLUpallDoc.SelectedValue.ToString() + " - " + DDLUpallDoc.SelectedItem.ToString();
            OTFunctions ot = new OTFunctions();
            jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidJobNo.Value);
            if (jobFoldernode == null)
            {
                jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidJobNo.Value);
            }
            DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
            if (DeptFoldernode == null)
            {
                DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
            }
            SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
            if (SectionFoldernode == null)
            {
                SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
            }
            Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            if (Doctypenode == null)
            {
                Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            }
            if (FileUpload2.FileName != "")
            {
                docid = ot.uploadDoc(Doctypenode.ID.ToString(), FileUpload2.FileName, FileUpload2.FileName, FileUpload2.FileBytes, "").ToString();
                string[] Muljob = Multiselect_DocJob.SelectedValue.Split(',');
                string bidno = "";
                string bidrev = "";
                if (Muljob.Length > 1)
                {
                    bidno = Muljob[0];
                    bidrev = Muljob[1];
                }
                string[] Mulsch = Multiselect_DocSch.SelectedValue.Split(',');
                string schno = "";
                string schname = "";
                if (Mulsch.Length > 1)
                {
                    schno = Mulsch[0];
                    schname = Mulsch[1];
                }
                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                if (bidno != "") objAddDoc.bid_no = bidno;
                else objAddDoc.bid_no = null;
                if (bidrev != "") objAddDoc.bid_revision = Convert.ToInt32(bidrev);
                else objAddDoc.bid_revision = null;
                if (schno != "") objAddDoc.schedule_no = Convert.ToInt32(schno);
                else objAddDoc.schedule_no = null;
                objAddDoc.doc_revision_no = null;
                objAddDoc.updated_datetime = DateTime.Now;
                objAddDoc.depart_position_name = hidDept.Value;
                objAddDoc.section_position_name = hidSect.Value;
                objAddDoc.doctype_code = DDLUpallDoc.SelectedValue;
                objAddDoc.doc_nodeid = Convert.ToInt32(docid);
                objAddDoc.doc_name = FileUpload2.FileName;
                objAddDoc.doc_desc = txtDescript.Text;
                objAddDoc.doc_sheetno = tb_sheetnoDoc.Text;
                objAddDoc.created_datetime = DateTime.Now;
                objAddDoc.updated_by = hidLogin.Value;
                objAddDoc.schedule_name = schname;
                objAddDoc.process_id = hidPID.Value;
                objAddDoc.activId = hidAcID.Value;
                objJobDb.InsertAddDoc(objAddDoc);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Upload Successfully.');", true);
                bind_data();
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Choose File.');", true);
            }
            dvhidDoc.Style["display"] = "block";
        }

        protected void Multiselect_DocSelectJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value, Multiselect_DocSelectJob.SelectedItem.Text);
            int i = 0;
            int k = 0;
            foreach (DataRow dr in dt.Rows)
            {
                if (dt.Rows[k]["schedule_name"].ToString() == "")
                {

                }
                else
                {
                    Multiselect_DocSelectSch.Items.Clear();
                    Multiselect_DocSelectSch.Items.Insert(i, new ListItem(dr["schedule_name"].ToString(), dr["schedule_no"].ToString() + "," + dr["schedule_name"].ToString()));
                    i++;
                }
                k++;

            }
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "none";
            dvAtt.Style["display"] = "table";
            dvAddDocument.Style["display"] = "table";
            dvDocview.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvhidDoc.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            TabSelectDoc.Attributes.Add("class", "tab-pane active");
            TabUploadDoc.Attributes.Add("class", "tab-pane");
            liSelDoc.Attributes.Add("class", "active");
            liUpDoc.Attributes.Remove("class");

        }

        protected void bt_preview_Click(object sender, EventArgs e)
        {
            cExcel excel = new cExcel();
            List<string> listFile = new List<string>();

            string path0 = @"c:\Xls";
            string[] chkdirs = Directory.GetDirectories(@"c:\", "Xls*", SearchOption.TopDirectoryOnly);
            if (chkdirs.Length > 0)
            {
                System.IO.DirectoryInfo di = new DirectoryInfo(path0);
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }

            }

            string path1 = @"c:\PDF";
            string path2 = @"c:\PDF\All_Equipment";
            string path3 = @"c:\PDF\All_Equipment\Equipment.zip";
            DirectoryInfo di1 = Directory.CreateDirectory(path1);
            DirectoryInfo di0 = Directory.CreateDirectory(path0);
            // excel.genExcel(hidLogin.Value, "job", txtJobNo.Text, txtJobRev.Text);
            DataTable dt = objJobDb.GetJobRelate(hidJobNo.Value, "");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (dr["schedule_name"].ToString() != "")
                    {
                        string filename = dr["bid_no"].ToString() + " " + dr["schedule_name"].ToString();
                        //excel.genExcel(filename, hidDept.Value, hidSect.Value, dr["bid_no"].ToString(), dr["schedule_no"].ToString(), dr["job_no"].ToString(), dr["job_revision"].ToString(), dr["schedule_name"].ToString());
                    }
                }
            }
            string[] dirs = Directory.GetDirectories(@"c:\PDF", "All*", SearchOption.TopDirectoryOnly);
            if (dirs.Length > 0)
            {
                var diraa = new DirectoryInfo(dirs[0]);
                diraa.Delete(true);

            }
            DirectoryInfo di2 = Directory.CreateDirectory(path2);

            string[] filePaths = Directory.GetFiles(@"c:\Xls", "*.*", SearchOption.TopDirectoryOnly);
            if (filePaths.Length > 0)
            {
                ZipFile.CreateFromDirectory(path0, path3, CompressionLevel.Optimal, false);
                Response.ContentType = "Application/zip";
                Response.AppendHeader("Content-Disposition", "attachment; filename=Equipment.zip");
                Response.TransmitFile("C:\\PDF\\All_Equipment\\Equipment.zip");
                Response.Flush();
                Response.End();
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Excel is not creating');", true);
            }

        }
        protected void bt_refresh_Click(object sender, EventArgs e)
        {
            DataTable dt = objJobDb.getallEQ(hidJobNo.Value, rev.Value);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objJobDb.insertDocFromAllEQ(dr["equip_code"].ToString(),hidDept.Value,hidSect.Value, hidJobNo.Value, rev.Value,hidAcID.Value,hidLogin.Value,hidPID.Value);
                }
            }
            DataTable dtTask = objJobDb.GetDrawing(hidJobNo.Value, "", hidDept.Value, hidSect.Value, txtJobRev.Text);
            if (dtTask.Rows.Count > 0)
            {
                gvTaskDoc.DataSource = dtTask;
                gvTaskDoc.DataBind();
            }
            else
            {
                gvTaskDoc.DataSource = null;
                gvTaskDoc.DataBind();
            }
            dvhidDoc.Style["display"] = "block";
        }
        protected void btnCancelItem_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";
            gvItem.Visible = false;
            ViewState["chkflag"] = "1";
            Session.Add("dtAdd", null);
            ViewState["itmno"] = "";
        }
        protected void btnpreview_Click(object sender, EventArgs e)
        {
            //kie21-12
            List<AddDoc> genDoc = new List<AddDoc>();
            string filename = "";
            //ref string strError;


            foreach (GridViewRow gvr in gvDrawing.Rows)
            {
                Label lbDoct = (Label)gvr.FindControl("lbDoct");
                Label lbFilename = (Label)gvr.FindControl("lbFilename");
                Label lbRevi = (Label)gvr.FindControl("lbRevi");
                Label lbDocno = (Label)gvr.FindControl("lbDocno");
                Label lbBid = (Label)gvr.FindControl("lbBid");
                Label lbRevbid = (Label)gvr.FindControl("lbRevbiddr");
                Label lbhidSch = (Label)gvr.FindControl("lbhidSch");
                Label lbDes = (Label)gvr.FindControl("lbDes");


                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                if (lbBid.Text != "") objAddDoc.bid_no = lbBid.Text;
                else objAddDoc.bid_no = null;
                if (lbRevbid.Text != "") objAddDoc.bid_revision = Convert.ToInt32(lbRevbid.Text);
                else objAddDoc.bid_revision = null;
                if (lbhidSch.Text != "") objAddDoc.schedule_no = Convert.ToInt32(lbhidSch.Text);
                else objAddDoc.schedule_no = null;
                if (lbRevi.Text != "") objAddDoc.doc_revision_no = Convert.ToInt32(lbRevi.Text);
                else objAddDoc.doc_revision_no = null;
                objAddDoc.doctype_code = lbDoct.Text;
                if (lbDocno.Text != "") objAddDoc.doc_nodeid = Convert.ToInt32(lbDocno.Text);
                else objAddDoc.doc_nodeid = 0;
                objAddDoc.doc_name = lbFilename.Text;
                objAddDoc.doc_desc = lbDes.Text;
                objAddDoc.activId = "";
                genDoc.Add(objAddDoc);

            }

            cExcel genfile = new cExcel();
            filename = genfile.genExcelDrawingJob(genDoc, hidLogin.Value);
            byte[] byte1 = File.ReadAllBytes("C:\\Xls\\" + filename);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "binary/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + filename + ".xls");
            Response.BinaryWrite(byte1);
            Response.Flush();
            Response.End();
            //string strError = "";
            //var name = Regex.Replace(filename, ".xlsx+", string.Empty);
            //genfile.ExportExcelToPdf(new FileInfo("C:\\Xls\\" + name + ".xlsx"), "C:\\PDF\\", ref strError);
            //byte[] byte1 = File.ReadAllBytes("C:\\PDF\\" + name + ".pdf");

            //lstByte.Add(byte1);
            //filename.pdf
        }

        protected void btnSubtask_Click(object sender, EventArgs e)
        {

            if(hidPID.Value!="") objJobDb.updateProcessID(hidJobNo.Value, rev.Value,hidPID.Value);

            List<byte[]> lstByte = new List<byte[]>();

            clsWfPsAPV01 wfp = new clsWfPsAPV01();
            //memo
            DataTable dt = objJobDb.getPs_req(hidJobNo.Value, hidWorkID.Value, txtJobRev.Text);
            //string ps_subject = "";
            //string ps_reqno = "";
            //string requestor_name = "";
            string requestor_status = "";
            string strMergedFileName = "";
            string strProcessID = "";

            if (dt.Rows.Count > 0)
            {
                //ps_subject = dt.Rows[0]["ps_subject"].ToString();
                //ps_reqno = dt.Rows[0]["ps_reqno"].ToString();
                //requestor_name = dt.Rows[0]["requestor_name"].ToString();
                requestor_status = dt.Rows[0]["requestor_status"].ToString();
                strMergedFileName = dt.Rows[0]["ps_reqno"].ToString() + "_" + dt.Rows[0]["job_no"].ToString();
                strProcessID = dt.Rows[0]["process_id"].ToString();
                byte[] byteMemo = wfp.GetMemoByPsRequest(hidJobNo.Value, txtJobRev.Text, strProcessID);
                lstByte.Add(byteMemo);
            }
            //Draw

            List<AddDoc> genDoc = new List<AddDoc>();
            string filename = "";
            string isChk = "";

            DataTable dtDraw = objJobDb.GetDrawing(hidJobNo.Value, "draw", hidDept.Value, hidSect.Value, txtJobRev.Text);
            DataView dvDraw = new DataView(dtDraw);
            dvDraw.RowFilter = " process_id='" + hidPID.Value + "'";

            foreach (DataRowView drv in dvDraw)
            {
                string lbDoct = drv["doctype_code"].ToString();
                string lbFilename = drv["doc_desc"].ToString();
                string lbRevi = drv["doc_revision"].ToString();
                string lbDocno = drv["doc_no"].ToString();
                string lbBid = drv["bid_no"].ToString();
                string lbRevbid = drv["bid_revision"].ToString();
                string lbhidSch = drv["schedule_no"].ToString();
                string lbDes = drv["tr_doc_desc"].ToString();


                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                if (lbBid != "") objAddDoc.bid_no = lbBid;
                else objAddDoc.bid_no = null;
                if (lbRevbid != "") objAddDoc.bid_revision = Convert.ToInt32(lbRevbid);
                else objAddDoc.bid_revision = null;
                if (lbhidSch != "") objAddDoc.schedule_no = Convert.ToInt32(lbhidSch);
                else objAddDoc.schedule_no = null;
                if (lbRevi != "") objAddDoc.doc_revision_no = Convert.ToInt32(lbRevi);
                else objAddDoc.doc_revision_no = null;
                objAddDoc.doctype_code = lbDoct;
                if (lbDocno != "") objAddDoc.doc_nodeid = Convert.ToInt32(lbDocno);
                else objAddDoc.doc_nodeid = 0;
                objAddDoc.doc_name = lbFilename;
                objAddDoc.doc_desc = lbDes;
                objAddDoc.activId = hidAcID.Value;
                genDoc.Add(objAddDoc);

            }

            cExcel genfile = new cExcel();
            filename = genfile.genExcelDrawingJob(genDoc, hidLogin.Value);
            string strError = "";
            var name = Regex.Replace(filename, ".xlsx+", string.Empty);
            genfile.ExportExcelToPdf(new FileInfo("C:\\Xls\\" + name + ".xlsx"), "C:\\PDF\\", ref strError);
            byte[] byte1 = File.ReadAllBytes("C:\\PDF\\" + name + ".pdf");
            lstByte.Add(byte1);


            //Eq
            cExcel excel = new cExcel();
            List<string> listFile = new List<string>();
            //listFile = excel.genExcel(hidLogin.Value, "job", txtJobNo.Text, txtJobRev.Text);
            string strError1 = "";

            //string filename1 = "";
            //Regex.Replace(item.Name, "[^A-Z]+", string.Empty);
            foreach (var filename1 in listFile)
            {
                var name1 = Regex.Replace(filename1, ".xlsx+", string.Empty);
                excel.ExportExcelToPdf(new FileInfo("C:\\Xls\\" + name1 + ".xlsx"), "C:\\PDF\\", ref strError1);
                byte[] byte2 = File.ReadAllBytes("C:\\PDF\\" + name1 + ".pdf");
                lstByte.Add(byte2);

            }

            // memoDocClass memo = new memoDocClass();

            OTFunctions ot = new OTFunctions();
            byte[] result = objJobDb.ConcatAndAddContent(lstByte);

            var nodeId = ot.uploadDoc(strFolderAttNodeId, strMergedFileName + ".pdf", strMergedFileName + ".pdf", result, ""); //strFolderAttNodeId
            clsWfPsAPV01 wfps = new clsWfPsAPV01();
            if (requestor_status == "Submitted")
            {

            }
            else
            {
                wfps.EmpSubmitWf(strProcessID, hidWorkID.Value, hidSubWorkID.Value, hidTaskID.Value);
                isChk = objJobDb.Update_wf_req(hidJobNo.Value, strProcessID, txtJobRev.Text, hidLogin.Value, nodeId.ToString());
                if (isChk != "") ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Submit Successfully.');", true);
            }
            // return nodeId.ToString();

            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void bt_savePart_Click(object sender, EventArgs e)
        {

            foreach (GridViewRow dr in gvAllPart.Rows)
            {
                CheckBox cb_sub = (CheckBox)dr.FindControl("cb_sub");
                Label lb_part = (Label)dr.FindControl("lb_part");
                TextBox tb_Desc = (TextBox)dr.FindControl("tb_Desc");
                TextBox tb_Import = (TextBox)dr.FindControl("tb_Import");
                if (cb_sub.Checked == true)
                {

                    addMytaskEquipJob objEqui = new addMytaskEquipJob();
                    objEqui.activity_id = hidAcID.Value;
                    objEqui.step = "J";
                    objEqui.job_no = tb_jobNoPart.Text;
                    if (tb_revPart.Text != "") objEqui.job_revision = Convert.ToInt32(tb_revPart.Text);
                    objEqui.bid_no = hidBidNo.Value;
                    if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                    if (hidScheNo.Value != "") objEqui.schedule_no = Convert.ToInt32(hidScheNo.Value);
                    else objEqui.schedule_no = null;
                    if (hidScheName.Value != "") objEqui.schedule_name = hidScheName.Value;
                    else objEqui.schedule_name = "";
                    objEqui.depart_position_name = hidDept.Value;
                    objEqui.section_position_name = hidSect.Value;
                    objEqui.part_name = lb_part.Text;
                    if(tb_Desc.Text!="") objEqui.part_description = tb_Desc.Text;
                    else objEqui.part_description = lb_part.Text;
                    objEqui.important = tb_Import.Text;
                    objEqui.created_by = hidLogin.Value;
                    objEqui.created_datetime = DateTime.Now;
                    objEqui.updated_by = hidLogin.Value;
                    objEqui.updated_datetime = DateTime.Now;
                    objEqui.process_id = hidPID.Value;
                    objJobDb.insertMytaskPart(objEqui);

                }
            }
            hidChkTab.Value = "";
            bind_equipment("");
            dvhidEq.Style["display"] = "block";

            //if (tb_part.Text!="")
            //{
            //    string[] Mulbid = ddl_bidPart.SelectedValue.Split(',');
            //    string bidno = "";
            //    string bidrev = "";
            //    if (Mulbid.Length > 1)
            //    {
            //        bidno = Mulbid[0];
            //        bidrev = Mulbid[1];
            //    }
            //    addMytaskEquipJob objEqui = new addMytaskEquipJob();
            //    objEqui.activity_id = hidAcID.Value;
            //    objEqui.step = "J";
            //    objEqui.job_no = tb_jobNoPart.Text;
            //    if (tb_revPart.Text != "") objEqui.job_revision = Convert.ToInt32(tb_revPart.Text);
            //    objEqui.bid_no = bidno;
            //    if (bidrev != "") objEqui.bid_revision = Convert.ToInt32(bidrev);
            //    if (ddl_schedulePart.SelectedValue != "") objEqui.schedule_no = Convert.ToInt32(ddl_schedulePart.SelectedValue);
            //    else objEqui.schedule_no = null;
            //    if (ddl_schedulePart.SelectedValue != "") objEqui.schedule_name = ddl_schedulePart.SelectedItem.Text;
            //    else objEqui.schedule_name = "";
            //    objEqui.depart_position_name = hidDept.Value;
            //    objEqui.section_position_name = hidSect.Value;
            //    objEqui.part_name = tb_part.Text;
            //    objEqui.part_description = tb_partDesc.Text;
            //    objEqui.created_by = hidLogin.Value;
            //    objEqui.created_datetime = DateTime.Now;
            //    objEqui.updated_by = hidLogin.Value;
            //    objEqui.updated_datetime = DateTime.Now;

            //    objJobDb.insertMytaskPart(objEqui);
            //    DataTable t = new DataTable();
            //    bind_equiList("", "", "", "", t);
            //}
            //else
            //{
            //    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please add part.');", true);
            //    myModal.Style["display"] = "block";
            //    dvpopUpload.Style["display"] = "none";
            //    dvAddDocument.Style["display"] = "none";
            //    dv_addPart.Style["display"] = "block";
            //    dvPopAddEq.Style["display"] = "none";
            //    dvPopDocview.Style["display"] = "none";
            //}

        }
        protected void bt_cancPart_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";

        }
        protected void bt_cancView_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            dvhidEq.Style["display"] = "block";
            dvViewBom.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "block";
        }
        protected void btnHistory_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "block";

        }

        protected void btnAddHis_Click(object sender, EventArgs e)
        {
            ViewState["history"] = "1";
            string JobnoHis = "";
            if (Multiselect_HisJob.SelectedValue != "") JobnoHis = Multiselect_HisJob.SelectedItem.Text;//txtHisRev.Text

            DataTable dtDraw = objJobDb.GetDrawing(JobnoHis, "draw", hidDept.Value, hidSect.Value, txtHisRev.Text);
            if (dtDraw.Rows.Count > 0)
            {
                gvDrawing.DataSource = dtDraw;
                gvDrawing.DataBind();
            }
            else
            {
                gvDrawing.DataSource = null;
                gvDrawing.DataBind();
            }
            DataTable dtTask = objJobDb.GetDrawing(JobnoHis, "", hidDept.Value, hidSect.Value, txtHisRev.Text);
            if (dtTask.Rows.Count > 0)
            {
                gvTaskDoc.DataSource = dtTask;
                gvTaskDoc.DataBind();
            }
            else
            {
                gvTaskDoc.DataSource = null;
                gvTaskDoc.DataBind();
            }
            DataTable tt = new DataTable();
            bind_equiList("", "", "", "history", tt);
            if (Multiselect_HisJob.SelectedValue != "")
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Add Successfully.');", true);
                btnHisSave.Visible = true;
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Add Job No.');", true);
                btnHisSave.Visible = false;
            }

        }

        protected void btnCloseHis_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
        }

        protected void Multiselect_HisJob_SelectedIndexChanged(object sender, EventArgs e)
        {
            string JobnoHis = "";
            if (Multiselect_HisJob.SelectedValue != "") JobnoHis = Multiselect_HisJob.SelectedItem.Text;
            DataTable dt = new DataTable();
            dt = objJobDb.getDDLHistory(hidDept.Value, hidSect.Value, hidAcID.Value, JobnoHis);
            if (dt.Rows.Count > 0)
            {
                txtHisRev.Text = dt.Rows[0]["job_revision"].ToString();
            }
            myModal.Style["display"] = "block";
            dvpopUpload.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "block";
        }

        protected void btnHisSave_Click(object sender, EventArgs e)
        {
            ViewState["history"] = "";
            ViewState["first"] = "";
            ViewState["firstDoc"] = "";
            foreach (GridViewRow gvr in gvDrawing.Rows)
            {
                Label lbDoct = (Label)gvr.FindControl("lbDoct");
                Label lbFilename = (Label)gvr.FindControl("lbFilename");
                Label lbRevi = (Label)gvr.FindControl("lbRevi");
                Label lbDocno = (Label)gvr.FindControl("lbDocno");
                Label lbBid = (Label)gvr.FindControl("lbBid");
                Label lbRevbid = (Label)gvr.FindControl("lbRevbiddr");
                Label lbhidSch = (Label)gvr.FindControl("lbhidSch");
                Label lbSchnamedr = (Label)gvr.FindControl("lbSchnamedr");
                Label lbDes = (Label)gvr.FindControl("lbDes");
                Label lbUpby = (Label)gvr.FindControl("lbUpby");
                Node jobFoldernode = new Node();
                Node DeptFoldernode = new Node();
                Node SectionFoldernode = new Node();
                Node Doctypenode = new Node();
                Node Filenode = new Node();
                Node docid = new Node();
                string DoctypeFoldername = lbDoct.Text + " - " + lbDes.Text;
                OTFunctions ot = new OTFunctions();
                jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidJobNo.Value);
                if (jobFoldernode == null)
                {
                    jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidJobNo.Value);
                }
                DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
                if (DeptFoldernode == null)
                {
                    DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
                }
                SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
                if (SectionFoldernode == null)
                {
                    SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
                }
                Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
                if (Doctypenode == null)
                {
                    Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
                }
                Filenode = ot.getNodeByName(Doctypenode.ID.ToString(), lbFilename.Text);
                if (Filenode == null)
                {
                    docid = ot.copyNode(lbDocno.Text, Doctypenode.ID.ToString());
                }
                else
                {
                    DeleteNode(Filenode.ID.ToString());
                    docid = ot.copyNode(lbDocno.Text, Doctypenode.ID.ToString());
                }

                AddDoc objAddDoc = new AddDoc();
                objAddDoc.job_no = txtJobNo.Text;
                objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                if (lbBid.Text != "") objAddDoc.bid_no = lbBid.Text;
                else objAddDoc.bid_no = null;
                if (lbRevbid.Text != "") objAddDoc.bid_revision = Convert.ToInt32(lbRevbid.Text);
                else objAddDoc.bid_revision = null;
                if (lbhidSch.Text != "") objAddDoc.schedule_no = Convert.ToInt32(lbhidSch.Text);
                else objAddDoc.schedule_no = null;
                if (lbRevi.Text != "") objAddDoc.doc_revision_no = Convert.ToInt32(lbRevi.Text);
                else objAddDoc.doc_revision_no = null;
                objAddDoc.doctype_code = lbDoct.Text;

                if (docid.ID.ToString() != "0") objAddDoc.doc_nodeid = Convert.ToInt32(docid.ID.ToString());
                else objAddDoc.doc_nodeid = 0;
                objAddDoc.schedule_name = lbSchnamedr.Text;
                objAddDoc.depart_position_name = hidDept.Value;
                objAddDoc.section_position_name = hidSect.Value;
                objAddDoc.doc_name = lbFilename.Text;
                objAddDoc.doc_desc = lbDes.Text;
                objAddDoc.created_datetime = DateTime.Now;
                objAddDoc.updated_datetime = DateTime.Now;
                objAddDoc.updated_by = lbUpby.Text;
                objJobDb.InsertAddDoc(objAddDoc);

            }
            foreach (GridViewRow gvr in gvTaskDoc.Rows)
            {
                Label lbDocCode = (Label)gvr.FindControl("lbDocCode");
                Label lbFilename = (Label)gvr.FindControl("lbFilename");
                Label lbRev = (Label)gvr.FindControl("lbRev");
                Label lbDocno = (Label)gvr.FindControl("lbDocno");
                Label lbBidno = (Label)gvr.FindControl("lbBidno");
                Label lbRevbid = (Label)gvr.FindControl("lbRevbid");
                Label lbSchnohid = (Label)gvr.FindControl("lbSchnohid");
                Label lbSchname = (Label)gvr.FindControl("lbSchname");
                Label lbDescrip = (Label)gvr.FindControl("lbDescrip");
                Label lbUpby = (Label)gvr.FindControl("lbUpby");
                if (lbDocCode.Text == "DD" || lbDocCode.Text == "TD")
                {
                    //no insert
                }
                else
                {
                    Node jobFoldernode = new Node();
                    Node DeptFoldernode = new Node();
                    Node SectionFoldernode = new Node();
                    Node Doctypenode = new Node();
                    Node Filenode = new Node();
                    Node docid = new Node();
                    string DoctypeFoldername = lbDocCode.Text + " - " + lbDescrip.Text;
                    OTFunctions ot = new OTFunctions();
                    jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidJobNo.Value);
                    if (jobFoldernode == null)
                    {
                        jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidJobNo.Value);
                    }
                    DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
                    if (DeptFoldernode == null)
                    {
                        DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
                    }
                    SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
                    if (SectionFoldernode == null)
                    {
                        SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
                    }
                    Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
                    if (Doctypenode == null)
                    {
                        Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
                    }
                    Filenode = ot.getNodeByName(Doctypenode.ID.ToString(), lbFilename.Text);
                    if (Filenode == null)
                    {
                        docid = ot.copyNode(lbDocno.Text, Doctypenode.ID.ToString());
                    }
                    else
                    {
                        DeleteNode(Filenode.ID.ToString());
                        docid = ot.copyNode(lbDocno.Text, Doctypenode.ID.ToString());
                    }

                    AddDoc objAddDoc = new AddDoc();
                    objAddDoc.job_no = txtJobNo.Text;
                    objAddDoc.job_revision = Convert.ToInt32(txtJobRev.Text);
                    if (lbBidno.Text != "") objAddDoc.bid_no = lbBidno.Text;
                    else objAddDoc.bid_no = null;
                    if (lbRevbid.Text != "") objAddDoc.bid_revision = Convert.ToInt32(lbRevbid.Text);
                    else objAddDoc.bid_revision = null;
                    if (lbSchnohid.Text != "") objAddDoc.schedule_no = Convert.ToInt32(lbSchnohid.Text);
                    else objAddDoc.schedule_no = null;
                    if (lbRev.Text != "") objAddDoc.doc_revision_no = Convert.ToInt32(lbRev.Text);
                    else objAddDoc.doc_revision_no = null;
                    objAddDoc.doctype_code = lbDocCode.Text;
                    if (docid.ID.ToString() != "0") objAddDoc.doc_nodeid = Convert.ToInt32(docid.ID.ToString());
                    else objAddDoc.doc_nodeid = 0;
                    objAddDoc.schedule_name = lbSchname.Text;
                    objAddDoc.depart_position_name = hidDept.Value;
                    objAddDoc.section_position_name = hidSect.Value;
                    objAddDoc.doc_name = lbFilename.Text;
                    objAddDoc.doc_desc = lbDescrip.Text;
                    objAddDoc.created_datetime = DateTime.Now;
                    objAddDoc.updated_datetime = DateTime.Now;
                    objAddDoc.updated_by = lbUpby.Text;
                    objJobDb.InsertAddDoc(objAddDoc);
                }

            }
            bind_data();

            //eq

            string JobnoHis = "";
            if (Multiselect_HisJob.SelectedValue != "") JobnoHis = Multiselect_HisJob.SelectedItem.Text;
            DataTable dtchk = objJobDb.getPartEqup(hidDept.Value, hidSect.Value, hidAcID.Value, JobnoHis, txtHisRev.Text);
            if (dtchk.Rows.Count > 0)
            {

                string bidrev = "";
                string schno = "";
                for (int i = 0; i < dtchk.Rows.Count; i++)
                {

                    schno = dtchk.Rows[i]["schedule_no"].ToString();
                    bidrev = dtchk.Rows[i]["bid_revision"].ToString();
                    addMytaskEquipJob objEqui = new addMytaskEquipJob();
                    objEqui.activity_id = hidAcID.Value;
                    objEqui.step = "J";
                    objEqui.job_no = txtJobNo.Text;
                    if (txtJobRev.Text != "") objEqui.job_revision = Convert.ToInt32(txtJobRev.Text);
                    objEqui.bid_no = dtchk.Rows[i]["bid_no"].ToString(); ;
                    if (bidrev != "") objEqui.bid_revision = Convert.ToInt32(bidrev);
                    if (schno != "") objEqui.schedule_no = Convert.ToInt32(schno);
                    else objEqui.schedule_no = null;
                    objEqui.schedule_name = dtchk.Rows[i]["schedule_name"].ToString();
                    objEqui.depart_position_name = dtchk.Rows[i]["depart_position_name"].ToString();
                    objEqui.section_position_name = dtchk.Rows[i]["section_position_name"].ToString();
                    objEqui.part_name = dtchk.Rows[i]["part_name"].ToString();
                    objEqui.part_description = dtchk.Rows[i]["part_description"].ToString();
                    objEqui.created_by = hidLogin.Value;
                    objEqui.created_datetime = DateTime.Now;
                    objEqui.updated_by = hidLogin.Value;
                    objEqui.updated_datetime = DateTime.Now;

                    objJobDb.insertMytaskPart(objEqui);
                }

            }

            DataTable dt = new DataTable();
            //dt = objJobDb.getTabEqui(hidDept.Value, hidSect.Value, JobnoHis, txtHisRev.Text);
            dt = objJobDb.getEquipCopy(hidDept.Value, hidSect.Value, JobnoHis, txtHisRev.Text);
            if (dt.Rows.Count > 0)
            {
                string bidrev = "";
                string schno = "";
                string fact_test = "";
                string man_test = "";
                string rout_test = "";
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    addMytaskEquipJob objeq = new addMytaskEquipJob();
                    objeq.bid_no = dt.Rows[i]["bid_no"].ToString();
                    bidrev = dt.Rows[i]["bid_revision"].ToString();
                    if (bidrev != "") objeq.bid_revision = Convert.ToInt32(bidrev);
                    objeq.job_no = txtJobNo.Text;
                    objeq.job_revision = Convert.ToInt32(txtJobRev.Text);
                    //if (dt.Rows[i]["rowid"].ToString() != "") rowid = dt.Rows[i]["rowid"].ToString();
                    //objeq.rowid = Convert.ToInt32(rowid);
                    if (dt.Rows[i]["schedule_no"].ToString() != "") schno = dt.Rows[i]["schedule_no"].ToString();
                    objeq.schedule_no = Convert.ToInt32(schno);
                    objeq.schedule_name = dt.Rows[i]["schedule_name"].ToString();

                    objeq.equip_incoterm = dt.Rows[i]["equip_incoterm"].ToString();
                    //objeq.equip_incoterm = ddl_inco.SelectedValue;
                    objeq.equip_mas_grp = dt.Rows[i]["equip_mas_grp"].ToString();
                    fact_test = dt.Rows[i]["factory_test"].ToString();
                    man_test = dt.Rows[i]["manual_test"].ToString();
                    rout_test = dt.Rows[i]["routine_test"].ToString();
                    if (dt.Rows[i]["factory_test"].ToString() != "") objeq.factory_test = Convert.ToBoolean(fact_test);
                    else objeq.factory_test = null;
                    if (dt.Rows[i]["manual_test"].ToString() != "") objeq.manual_test = Convert.ToBoolean(man_test);
                    else objeq.manual_test = null;
                    if (dt.Rows[i]["routine_test"].ToString() != "") objeq.routine_test = Convert.ToBoolean(rout_test);
                    else objeq.routine_test = null;


                    objeq.equip_item_no = dt.Rows[i]["equip_item_no"].ToString();
                    objeq.equip_add_desc = dt.Rows[i]["equip_add_desc"].ToString();
                    if (dt.Rows[i]["equip_qty"].ToString() != "") objeq.equip_qty = Convert.ToDecimal(dt.Rows[i]["equip_qty"].ToString());
                    objeq.equip_unit = dt.Rows[i]["equip_unit"].ToString();
                    if (dt.Rows[i]["equip_bid_qty"].ToString() != "") objeq.equip_bid_qty = Convert.ToInt32(dt.Rows[i]["equip_bid_qty"].ToString());
                    if (dt.Rows[i]["supply_equip_unit_price"].ToString() != "") objeq.supply_equip_unit_price = Convert.ToDecimal(dt.Rows[i]["supply_equip_unit_price"].ToString());
                    else objeq.supply_equip_unit_price = default(decimal);
                    if (dt.Rows[i]["supply_equip_amonut"].ToString() != "") objeq.supply_equip_amonut = Convert.ToDecimal(dt.Rows[i]["supply_equip_amonut"].ToString() != "");
                    else objeq.supply_equip_amonut = default(decimal);
                    if (dt.Rows[i]["local_exwork_unit_price"].ToString() != "") objeq.local_exwork_unit_price = Convert.ToDecimal(dt.Rows[i]["local_exwork_unit_price"].ToString());
                    else objeq.local_exwork_unit_price = default(decimal);
                    if (dt.Rows[i]["local_exwork_amonut"].ToString() != "") objeq.local_exwork_amonut = Convert.ToDecimal(dt.Rows[i]["local_exwork_amonut"].ToString());
                    else objeq.local_exwork_amonut = default(decimal);
                    if (dt.Rows[i]["local_tran_unit_price"].ToString() != "") objeq.local_tran_unit_price = Convert.ToDecimal(dt.Rows[i]["local_tran_unit_price"].ToString());
                    else objeq.local_tran_unit_price = default(decimal);
                    if (dt.Rows[i]["local_tran_amonut"].ToString() != "") objeq.local_tran_amonut = Convert.ToDecimal(dt.Rows[i]["local_tran_amonut"].ToString());
                    else objeq.local_tran_amonut = default(decimal);
                    objeq.currency = dt.Rows[i]["currency"].ToString();
                    objeq.updated_datetime = DateTime.Now;
                    objeq.created_datetime = DateTime.Now;
                    objeq.equip_full_desc = dt.Rows[i]["equip_full_desc"].ToString();
                    objeq.equip_code = dt.Rows[i]["equip_code"].ToString();
                    objeq.depart_position_name = hidDept.Value;
                    objeq.section_position_name = hidSect.Value;
                    objeq.step = dt.Rows[i]["step"].ToString();
                    objeq.path_code = dt.Rows[i]["path_code"].ToString();
                    objeq.activity_id = dt.Rows[i]["activity_id"].ToString();
                    objeq.created_by = hidLogin.Value;
                    objeq.updated_by = hidLogin.Value;
                    objeq.equip_standard_drawing = dt.Rows[i]["equip_standard_drawing"].ToString();
                    objJobDb.insertMyTaskEquip(objeq);
                }

            }



            DataTable t = new DataTable();
            bind_equiList("", "", "", "", t);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true);
            btnHisSave.Visible = false;

        }
        protected void bt_Save_Click(object sender, EventArgs e)
        {

            objJobDb.savePartRemark(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, hidSelPart.Value, tb_remarkprt.Text, tb_descprt.Text, tb_import.Text);
            dvhidEq.Style["display"] = "block";
            bind_equipment("");
            tb_val.Text = "";
            hidSelPart.Value = "";
        }
        //protected void img_delEQ_Click(object sender, ImageClickEventArgs e)
        //{
        //    ImageButton btnDel = (ImageButton)sender;
        //    GridViewRow gvr = btnDel.NamingContainer as GridViewRow;
        //    Label lb_hide = (Label)gvr.FindControl("lb_hide");
        //    objJobDb.delMyTask(lb_hide.Text);
        //    dvhidEq.Style["display"] = "block";
        //    bind_equipment("");
        //}
        protected void gvAllPart_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                CheckBox cb_all = (CheckBox)e.Row.FindControl("cb_all");
                cb_all.Attributes.Add("onclick", "javascript:HeaderClick(" + cb_all.ClientID + ",'" + gvAllPart.ClientID + "');");
            }

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lb_part = (Label)e.Row.FindControl("lb_part");
                lb_part.Text = drv["part_code"].ToString();
                TextBox tb_Desc = (TextBox)e.Row.FindControl("tb_Desc");
                tb_Desc.Text = drv["parts_name"].ToString();
            }
        }
        protected void gv_AddSelEquip_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var row = Convert.ToInt32(e.CommandArgument);
            GridView gv_AddSelEquip = (GridView)sender;
            string code = ((Label)gv_AddSelEquip.Rows[row].FindControl("lb_equiCode")).Text;
            if (e.CommandName == "viewdata")
            {
                DataTable dt = objJobDb.getEquipBom(code);
                if (dt.Rows.Count > 0)
                {
                    gv_viewBOMEQ.DataSource = dt;
                    gv_viewBOMEQ.DataBind();

                }
                else
                {
                    gv_viewBOMEQ.DataSource = null;
                    gv_viewBOMEQ.DataBind();
                }

                myModal.Style["display"] = "block";
                dvViewBomEQ.Style["display"] = "block";
                dvPopAdd.Style["display"] = "none";
                dvhidEq.Style["display"] = "block";
            }
        }
        protected void gv_SelEquip_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var row = Convert.ToInt32(e.CommandArgument);
            GridView gv_SelEquip = (GridView)sender;
            string code = ((Label)gv_SelEquip.Rows[row].FindControl("lb_equiCode")).Text;
            if (e.CommandName == "viewdata")
            {
                DataTable dt = objJobDb.getEquipBom(code);
                if (dt.Rows.Count > 0)
                {
                    gv_viewBOM.DataSource = dt;
                    gv_viewBOM.DataBind();

                }
                else
                {
                    gv_viewBOM.DataSource = null;
                    gv_viewBOM.DataBind();
                }

                myModal.Style["display"] = "block";
                dvViewBom.Style["display"] = "block";
                dvPopAddEq.Style["display"] = "none";
                dvhidEq.Style["display"] = "block";
            }
        }
        protected void gv_AddSelEquip_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[17].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Unit Price";
                e.Row.Cells[18].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Amount";
                e.Row.Cells[19].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[20].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Amount";
                e.Row.Cells[21].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[22].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Amount";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbItemno = (Label)e.Row.FindControl("lbItemno");
                int no = Convert.ToInt32(e.Row.RowIndex) + 1;
                lbItemno.Text = no.ToString();
                Label lb_division = (Label)e.Row.FindControl("lb_division");
                lb_division.Text = drv["section_position_name"].ToString();
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["depart_position_name"].ToString();
                Label lb_mertGrp = (Label)e.Row.FindControl("lb_mertGrp");
                lb_mertGrp.Text = drv["equip_mas_grp"].ToString();
                Label lb_equiCode = (Label)e.Row.FindControl("lb_equiCode");
                lb_equiCode.Text = drv["equip_code"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                lb_legend.Text = drv["legend"].ToString();
                Label lb_Part = (Label)e.Row.FindControl("lb_Part");
                lb_Part.Text = drv["part"].ToString();
                Label lb_fullDesc = (Label)e.Row.FindControl("lb_fullDesc");
                if (drv["equip_full_desc"].ToString().Length > 30) lb_fullDesc.Text = drv["equip_full_desc"].ToString().Substring(0, 30) + "...";
                else lb_fullDesc.Text = drv["equip_full_desc"].ToString();
                lb_fullDesc.ToolTip = drv["equip_full_desc"].ToString();
                Label lb_fullDesc2 = (Label)e.Row.FindControl("lb_fullDesc2");
                lb_fullDesc2.Text = drv["equip_full_desc"].ToString();
                TextBox tb_AddDesc = (TextBox)e.Row.FindControl("tb_AddDesc");
                tb_AddDesc.Text = drv["equip_add_desc"].ToString();
                //TextBox tb_stdDr = (TextBox)e.Row.FindControl("tb_stdDr");
                //tb_stdDr.Text = drv["equip_standard_drawing"].ToString();
                Label lb_stdDr = (Label)e.Row.FindControl("lb_stdDr");
                DataTable dtdoc = objJobDb.getListDoc(lb_equiCode.Text, lb_depm.Text, lb_division.Text);
               // DataTable dtdoc = objJobDb.getListDoc("CVC3SWG03", "กวอ-ส.", "หวอ-ส.");
                foreach (DataRow dr in dtdoc.Rows)
                {
                    if (lb_stdDr.Text == "") lb_stdDr.Text = "-"+dr["doc_name"].ToString();
                    else lb_stdDr.Text += "<br/>-"+dr["doc_name"].ToString();
                }
                TextBox tb_supUnit = (TextBox)e.Row.FindControl("tb_supUnit");
                tb_supUnit.Text = drv["supply_equip_unit_price"].ToString();
                tb_supUnit.Attributes.Add("onblur", "javascript:return AddCalcSOE('" + e.Row.RowIndex + "')");
                TextBox tb_supAmou = (TextBox)e.Row.FindControl("tb_supAmou");
                tb_supAmou.Text = drv["supply_equip_amonut"].ToString();
                TextBox tb_localSUnit = (TextBox)e.Row.FindControl("tb_localSUnit");
                tb_localSUnit.Text = drv["local_exwork_unit_price"].ToString();
                tb_localSUnit.Attributes.Add("onblur", "javascript:return AddCalcLSE('" + e.Row.RowIndex + "')");
                TextBox tb_localSAmou = (TextBox)e.Row.FindControl("tb_localSAmou");
                tb_localSAmou.Text = drv["local_exwork_amonut"].ToString();
                TextBox tb_localTUnit = (TextBox)e.Row.FindControl("tb_localTUnit");
                tb_localTUnit.Text = drv["local_tran_unit_price"].ToString();
                tb_localTUnit.Attributes.Add("onblur", "javascript:return AddCalcLTC('" + e.Row.RowIndex + "')");
                TextBox tb_localTAmou = (TextBox)e.Row.FindControl("tb_localTAmou");
                tb_localTAmou.Text = drv["local_tran_amonut"].ToString();
                TextBox tb_Req = (TextBox)e.Row.FindControl("tb_Req");
                tb_Req.Text = drv["equip_qty"].ToString();
                tb_Req.Attributes.Add("onblur", "javascript:return NumOnly(this)");
                //Label lb_shortDesc = (Label)e.Row.FindControl("lb_shortDesc");
                //lb_shortDesc.Text = drv["ShortDesc"].ToString();
                //Label lb_equiType = (Label)e.Row.FindControl("lb_equiType");
                //lb_equiType.Text = drv[""].ToString();
                //Label lb_equiGrp = (Label)e.Row.FindControl("lb_equiGrp");
                //lb_equiGrp.Text = drv[""].ToString();
                //Label lb_rowid = (Label)e.Row.FindControl("lb_rowid");
                //lb_rowid.Text = drv["rowid"].ToString();
                Label lbUnit = (Label)e.Row.FindControl("lbUnit");
                lbUnit.Text = drv["unit"].ToString();
                if (lbUnit.Text == "Lot" || lbUnit.Text == "Lump sum")
                {
                    tb_Req.Text = lbUnit.Text;
                }
                ImageButton img_view = (ImageButton)e.Row.FindControl("img_view");
                if (drv["view"].ToString() == "0" && drv["type"].ToString() == "1") img_view.Visible = true;
                else img_view.Visible = false;
                Label lb_equipBom = (Label)e.Row.FindControl("lb_equipBom");
                lb_equipBom.Text = drv["equip_code2"].ToString();
                ListBox ddl_inco = (ListBox)e.Row.FindControl("ddl_inco");
                DataTable dtinco = new DataTable();
                dtinco = objJobDb.getDDLIncoterm();
                ddl_inco.Items.Clear();
                ddl_inco.DataSource = dtinco;
                ddl_inco.DataTextField = "incoterm_name";
                ddl_inco.DataValueField = "incoterm_code";
                ddl_inco.DataBind();
                foreach (ListItem item in ddl_inco.Items)
                {
                    if (drv["equip_incoterm"].ToString().IndexOf(item.Value) > -1)
                    {
                        item.Selected = true;
                    }
                }
                ListBox ddl_wa = (ListBox)e.Row.FindControl("ddl_wa");
                ddl_wa.Items.Clear();
                ddl_wa.Items.Insert(0, new ListItem("Factory Test", "1"));
                ddl_wa.Items.Insert(1, new ListItem("Manual Test", "2"));
                ddl_wa.Items.Insert(2, new ListItem("Routine Test", "3"));
                ddl_wa.Items.Insert(3, new ListItem("Waive Test", "4"));
                if (drv["factory_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[0].Selected = true;
                }
                if (drv["manual_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[1].Selected = true;
                }
                if (drv["routine_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[2].Selected = true;
                }
                if (drv["waive_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[3].Selected = true;
                }
                DataTable dtwt = objJobDb.getWaivTest(lb_equiCode.Text);
                if (dtwt.Rows.Count > 0)
                {
                    if (dtwt.Rows[0]["factory_test"].ToString() != "" && dtwt.Rows[0]["factory_test"] != null) ddl_wa.Items[0].Selected = true;
                    if (dtwt.Rows[0]["manual_test"].ToString() != "" && dtwt.Rows[0]["manual_test"] != null) ddl_wa.Items[1].Selected = true;
                    if (dtwt.Rows[0]["routine_test"].ToString() != "" && dtwt.Rows[0]["routine_test"] != null) ddl_wa.Items[2].Selected = true;
                    if (dtwt.Rows[0]["waive_test"].ToString() != "" && dtwt.Rows[0]["waive_test"] != null) ddl_wa.Items[3].Selected = true;
                    if (dtwt.Rows[0]["incoterm"].ToString() != "" && dtwt.Rows[0]["incoterm"] != null)
                    {
                        foreach (ListItem item in ddl_inco.Items)
                        {
                            if (dtwt.Rows[0]["incoterm"].ToString().IndexOf(item.Value) > -1)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }
                DropDownList ddl_cur = (DropDownList)e.Row.FindControl("ddl_cur");
                ddl_cur.Items.Clear();
                ddl_cur.Items.Insert(0, new ListItem("THB", "THB"));
                ddl_cur.Items.Insert(1, new ListItem("USD", "USD"));
                ddl_cur.Items.Insert(2, new ListItem("EUR", "EUR"));
                ddl_cur.Items.Insert(3, new ListItem("JPY", "JPY"));
                ddl_cur.Items.Insert(4, new ListItem("CNY", "CNY"));
                if (drv["currency"].ToString() != "") ddl_cur.SelectedValue = drv["currency"].ToString();
                TextBox tb_itmNo = (TextBox)e.Row.FindControl("tb_itmNo");
                tb_itmNo.Text = drv["equip_item_no"].ToString();
                Label lb_bidNo = (Label)e.Row.FindControl("lb_bidNo");
                lb_bidNo.Text = drv["bid_no"].ToString();
                string bidno = "";
                if (hidBidNo.Value == "Bid Supply") {
                    DataTable dtddl = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                    for (int i = 0; i < dtddl.Rows.Count; i++)
                    {
                        if (bidno == "") bidno ="'"+ dtddl.Rows[i]["bid_no"].ToString()+"'";
                        else bidno += ",'" + dtddl.Rows[i]["bid_no"].ToString() + "'";
                    }
                }
                else
                {
                    bidno = "'"+hidBidNo.Value+ "'";
                }
                DataTable dtDraw = objJobDb.GetDrawingDDL(hidJobNo.Value, "draw", hidDept.Value, hidSect.Value, txtJobRev.Text, bidno);
                ListBox ddl_dr = (ListBox)e.Row.FindControl("ddl_dr");
                ddl_dr.Items.Clear();
                ddl_dr.DataSource = dtDraw;
                ddl_dr.DataTextField = "doc_desc";
                ddl_dr.DataValueField = "doc_no"; 
                ddl_dr.DataBind();
                if (drv["edit"].ToString() == "1")
                {
                    if (lb_depm.Text != hidDept.Value)//no edit 
                    {
                        tb_AddDesc.Enabled = false;
                        //tb_stdDr.Enabled = false;
                        tb_supUnit.Enabled = false;
                        tb_supAmou.Enabled = false;
                        tb_localSUnit.Enabled = false;
                        tb_localSAmou.Enabled = false;
                        tb_localTUnit.Enabled = false;
                        tb_localTAmou.Enabled = false;
                        tb_Req.Enabled = false;
                        ddl_inco.Attributes.Add("disabled", "");
                        ddl_wa.Attributes.Add("disabled", "");
                        ddl_cur.Enabled = false;
                    }
                    else//edit
                    {
                        tb_AddDesc.Enabled = true;
                        //tb_stdDr.Enabled = true;
                        tb_supUnit.Enabled = true;
                        tb_supAmou.Enabled = true;
                        tb_localSUnit.Enabled = true;
                        tb_localSAmou.Enabled = true;
                        tb_localTUnit.Enabled = true;
                        tb_localTAmou.Enabled = true;
                        tb_Req.Enabled = true;
                        ddl_cur.Enabled = true;
                    }
                    DataTable dtchk = objJobDb.chkPartAcc(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, hidLogin.Value);
                    if (dtchk.Rows.Count > 0)
                    {
                        tb_itmNo.Enabled = true;
                    }
                    else
                    {
                        tb_itmNo.Enabled = false;
                    }
                }

            }
        }
        protected void gv_SelEquip_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[17].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Unit Price";
                e.Row.Cells[18].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Amount";
                e.Row.Cells[19].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[20].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Amount";
                e.Row.Cells[21].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[22].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Amount";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbItemno = (Label)e.Row.FindControl("lbItemno");
                int no = Convert.ToInt32(e.Row.RowIndex) + 1;
                lbItemno.Text = no.ToString();
                Label lb_division = (Label)e.Row.FindControl("lb_division");
                lb_division.Text = drv["section_position_name"].ToString();
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["depart_position_name"].ToString();
                Label lb_mertGrp = (Label)e.Row.FindControl("lb_mertGrp");
                lb_mertGrp.Text = drv["equip_mas_grp"].ToString();
                Label lb_equiCode = (Label)e.Row.FindControl("lb_equiCode");
                lb_equiCode.Text = drv["equip_code"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                lb_legend.Text = drv["legend"].ToString();
                Label lb_Part = (Label)e.Row.FindControl("lb_Part");
                lb_Part.Text = drv["part"].ToString();
                Label lb_fullDesc = (Label)e.Row.FindControl("lb_fullDesc");
                if (drv["equip_full_desc"].ToString().Length > 30) lb_fullDesc.Text = drv["equip_full_desc"].ToString().Substring(0, 30) + "...";
                else lb_fullDesc.Text = drv["equip_full_desc"].ToString();
                lb_fullDesc.ToolTip = drv["equip_full_desc"].ToString();
                Label lb_fullDesc2 = (Label)e.Row.FindControl("lb_fullDesc2");
                lb_fullDesc2.Text = drv["equip_full_desc"].ToString();
                TextBox tb_AddDesc = (TextBox)e.Row.FindControl("tb_AddDesc");
                tb_AddDesc.Text = drv["equip_add_desc"].ToString();
                //TextBox tb_stdDr = (TextBox)e.Row.FindControl("tb_stdDr");
                //tb_stdDr.Text = drv["equip_standard_drawing"].ToString();
                Label lb_stdDr = (Label)e.Row.FindControl("lb_stdDr");
                DataTable dtdoc = objJobDb.getListDoc(lb_equiCode.Text, lb_depm.Text, lb_division.Text);
                // DataTable dtdoc = objJobDb.getListDoc("CVC3SWG03", "กวอ-ส.", "หวอ-ส.");
                foreach (DataRow dr in dtdoc.Rows)
                {
                    if (lb_stdDr.Text == "") lb_stdDr.Text = "-" + dr["doc_name"].ToString();
                    else lb_stdDr.Text += "<br/>-" + dr["doc_name"].ToString();
                }
                TextBox tb_supUnit = (TextBox)e.Row.FindControl("tb_supUnit");
                tb_supUnit.Text = drv["supply_equip_unit_price"].ToString();
                tb_supUnit.Attributes.Add("onblur", "javascript:return CalcSOE('" + e.Row.RowIndex + "')");
                TextBox tb_supAmou = (TextBox)e.Row.FindControl("tb_supAmou");
                tb_supAmou.Text = drv["supply_equip_amonut"].ToString();
                TextBox tb_localSUnit = (TextBox)e.Row.FindControl("tb_localSUnit");
                tb_localSUnit.Text = drv["local_exwork_unit_price"].ToString();
                tb_localSUnit.Attributes.Add("onblur", "javascript:return CalcLSE('" + e.Row.RowIndex + "')");
                TextBox tb_localSAmou = (TextBox)e.Row.FindControl("tb_localSAmou");
                tb_localSAmou.Text = drv["local_exwork_amonut"].ToString();
                TextBox tb_localTUnit = (TextBox)e.Row.FindControl("tb_localTUnit");
                tb_localTUnit.Text = drv["local_tran_unit_price"].ToString();
                tb_localTUnit.Attributes.Add("onblur", "javascript:return CalcLTC('" + e.Row.RowIndex + "')");
                TextBox tb_localTAmou = (TextBox)e.Row.FindControl("tb_localTAmou");
                tb_localTAmou.Text = drv["local_tran_amonut"].ToString();
                TextBox tb_Req = (TextBox)e.Row.FindControl("tb_Req");
                tb_Req.Text = drv["equip_qty"].ToString();
                tb_Req.Attributes.Add("onblur", "javascript:return NumOnly(this)");
                Label lb_rowid = (Label)e.Row.FindControl("lb_rowid");

                lb_rowid.Text = drv["rowid"].ToString();
                //Label lb_shortDesc = (Label)e.Row.FindControl("lb_shortDesc");
                //lb_shortDesc.Text = drv["ShortDesc"].ToString();
                //Label lb_equiType = (Label)e.Row.FindControl("lb_equiType");
                //lb_equiType.Text = drv[""].ToString();
                //Label lb_equiGrp = (Label)e.Row.FindControl("lb_equiGrp");
                //lb_equiGrp.Text = drv[""].ToString();
                Label lbUnit = (Label)e.Row.FindControl("lbUnit");
                lbUnit.Text = drv["unit"].ToString();
                if (lbUnit.Text == "Lot" || lbUnit.Text == "Lump sum")
                {
                    tb_Req.Text = lbUnit.Text;
                }
                ImageButton img_view = (ImageButton)e.Row.FindControl("img_view");
                if (drv["view"].ToString() == "0" && drv["type"].ToString() == "1") img_view.Visible = true;
                else img_view.Visible = false;
                Label lb_equipBom = (Label)e.Row.FindControl("lb_equipBom");
                lb_equipBom.Text = drv["equip_code2"].ToString();
                ListBox ddl_inco = (ListBox)e.Row.FindControl("ddl_inco");
                DataTable dtinco = new DataTable();
                dtinco = objJobDb.getDDLIncoterm();
                ddl_inco.Items.Clear();
                ddl_inco.DataSource = dtinco;
                ddl_inco.DataTextField = "incoterm_name";
                ddl_inco.DataValueField = "incoterm_code";
                ddl_inco.DataBind();
                foreach (ListItem item in ddl_inco.Items)
                {
                    if (drv["equip_incoterm"].ToString().IndexOf(item.Value) > -1)
                    {
                        item.Selected = true;
                    }
                }
                ListBox ddl_wa = (ListBox)e.Row.FindControl("ddl_wa");
                ddl_wa.Items.Clear();
                ddl_wa.Items.Insert(0, new ListItem("Factory Test", "1"));
                ddl_wa.Items.Insert(1, new ListItem("Manual Test", "2"));
                ddl_wa.Items.Insert(2, new ListItem("Routine Test", "3"));
                ddl_wa.Items.Insert(3, new ListItem("Waive Test", "4"));
                if (drv["factory_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[0].Selected = true;
                }
                if (drv["manual_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[1].Selected = true;
                }
                if (drv["routine_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[2].Selected = true;
                }
                if (drv["waive_test"].ToString().ToLower() == "true")
                {
                    ddl_wa.Items[3].Selected = true;
                }
                DataTable dtwt = objJobDb.getWaivTest(lb_equiCode.Text);
                if (dtwt.Rows.Count> 0)
                {
                    if(dtwt.Rows[0]["factory_test"].ToString()!="" && dtwt.Rows[0]["factory_test"]!= null) ddl_wa.Items[0].Selected = true;
                    if (dtwt.Rows[0]["manual_test"].ToString() != "" && dtwt.Rows[0]["manual_test"] != null) ddl_wa.Items[1].Selected = true;
                    if (dtwt.Rows[0]["routine_test"].ToString() != "" && dtwt.Rows[0]["routine_test"] != null) ddl_wa.Items[2].Selected = true;
                    if (dtwt.Rows[0]["waive_test"].ToString() != "" && dtwt.Rows[0]["waive_test"] != null) ddl_wa.Items[3].Selected = true;
                    if (dtwt.Rows[0]["incoterm"].ToString() != "" && dtwt.Rows[0]["incoterm"] != null)
                    {
                        foreach (ListItem item in ddl_inco.Items)
                        {
                            if (dtwt.Rows[0]["incoterm"].ToString().IndexOf(item.Value) > -1)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }
                DropDownList ddl_cur = (DropDownList)e.Row.FindControl("ddl_cur");
                ddl_cur.Items.Clear();
                ddl_cur.Items.Insert(0, new ListItem("THB", "THB"));
                ddl_cur.Items.Insert(1, new ListItem("USD", "USD"));
                ddl_cur.Items.Insert(2, new ListItem("EUR", "EUR"));
                ddl_cur.Items.Insert(3, new ListItem("JPY", "JPY"));
                ddl_cur.Items.Insert(4, new ListItem("CNY", "CNY"));
                if (drv["currency"].ToString() != "") ddl_cur.SelectedValue = drv["currency"].ToString();
                TextBox tb_itmNo = (TextBox)e.Row.FindControl("tb_itmNo");
                tb_itmNo.Text = drv["equip_item_no"].ToString();
                Label lb_bidNo = (Label)e.Row.FindControl("lb_bidNo");
                lb_bidNo.Text = drv["bid_no"].ToString();
                string bidno = "";
                if (hidBidNo.Value == "Bid Supply")
                {
                    DataTable dtddl = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                    for (int i = 0; i < dtddl.Rows.Count; i++)
                    {
                        if (bidno == "") bidno = "'" + dtddl.Rows[i]["bid_no"].ToString() + "'";
                        else bidno += ",'" + dtddl.Rows[i]["bid_no"].ToString() + "'";
                    }
                }
                else
                {
                    bidno = "'" + hidBidNo.Value + "'";
                }
                DataTable dtDraw = objJobDb.GetDrawingDDL(hidJobNo.Value, "draw", hidDept.Value, hidSect.Value, txtJobRev.Text, bidno);
                ListBox ddl_dr = (ListBox)e.Row.FindControl("ddl_dr");
                ddl_dr.Items.Clear();
                ddl_dr.DataSource = dtDraw;
                ddl_dr.DataTextField = "doc_desc";
                ddl_dr.DataValueField = "doc_no";
                ddl_dr.DataBind();
                DataTable dtRelDoc = objJobDb.getRelDoc(drv["rowid"].ToString());
                if (dtRelDoc.Rows.Count > 0)
                {
                    int index = 0;
                    foreach (DataRow dr in dtDraw.Rows)
                    {
                        foreach (DataRow item in dtRelDoc.Rows)
                        {
                            if (dr["doc_no"].ToString() == item["doc_nodeid"].ToString())
                                ddl_dr.Items[index].Selected = true;

                        }
                        index++;
                    }
                }
                if (drv["edit"].ToString() == "1")
                {
                    if (lb_depm.Text != hidDept.Value)//no edit 
                    {
                        tb_AddDesc.Enabled = false;
                        //tb_stdDr.Enabled = false;
                        tb_supUnit.Enabled = false;
                        tb_supAmou.Enabled = false;
                        tb_localSUnit.Enabled = false;
                        tb_localSAmou.Enabled = false;
                        tb_localTUnit.Enabled = false;
                        tb_localTAmou.Enabled = false;
                        tb_Req.Enabled = false;
                        ddl_inco.Attributes.Add("disabled", "");
                        ddl_wa.Attributes.Add("disabled", "");
                        ddl_cur.Enabled = false;
                        ddl_dr.Attributes.Add("disabled", "");
                    }
                    else//edit
                    {
                        tb_AddDesc.Enabled = true;
                        //tb_stdDr.Enabled = true;
                        tb_supUnit.Enabled = true;
                        tb_supAmou.Enabled = true;
                        tb_localSUnit.Enabled = true;
                        tb_localSAmou.Enabled = true;
                        tb_localTUnit.Enabled = true;
                        tb_localTAmou.Enabled = true;
                        tb_Req.Enabled = true;
                        ddl_cur.Enabled = true;
                        ddl_dr.Enabled = true;
                    }
                    DataTable dtchk = objJobDb.chkPartAcc(hidDept.Value, hidSect.Value, hidBidNo.Value, hidScheNo.Value, hidLogin.Value);
                    if (dtchk.Rows.Count > 0)
                    {
                        tb_itmNo.Enabled = true;
                    }
                    else
                    {
                        tb_itmNo.Enabled = false;
                    }
                }

            }
        }
        protected void gv_viewBOMEQ_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbItemno = (Label)e.Row.FindControl("lbItemno");
                int no = Convert.ToInt32(e.Row.RowIndex) + 1;
                lbItemno.Text = no.ToString();
                Label lb_division = (Label)e.Row.FindControl("lb_division");
                lb_division.Text = drv["equip_gong"].ToString();
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["equip_pnag"].ToString();
                Label lb_mertGrp = (Label)e.Row.FindControl("lb_mertGrp");
                lb_mertGrp.Text = drv["materialgroup"].ToString();
                Label lb_equiCode = (Label)e.Row.FindControl("lb_equiCode");
                lb_equiCode.Text = drv["equip_code"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                lb_legend.Text = drv["legend"].ToString();
                Label lb_Part = (Label)e.Row.FindControl("lb_Part");
                lb_Part.Text = drv["parts"].ToString();
                Label lb_fullDesc = (Label)e.Row.FindControl("lb_fullDesc");
                lb_fullDesc.Text = drv["equip_desc_full"].ToString();
                Label lb_shortDesc = (Label)e.Row.FindControl("lb_shortDesc");
                lb_shortDesc.Text = drv["equip_desc_short"].ToString();
                Label lb_equiType = (Label)e.Row.FindControl("lb_equiType");
                //lb_equiType.Text = drv[""].ToString();
                Label lb_equiGrp = (Label)e.Row.FindControl("lb_equiGrp");
                //lb_equiGrp.Text = drv[""].ToString();
                Label lbUnit = (Label)e.Row.FindControl("lbUnit");
                lbUnit.Text = drv["unit"].ToString();
                ImageButton img_view = (ImageButton)e.Row.FindControl("img_view");
            }
        }
        protected void gv_viewBOM_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lbItemno = (Label)e.Row.FindControl("lbItemno");
                int no = Convert.ToInt32(e.Row.RowIndex) + 1;
                lbItemno.Text = no.ToString();
                Label lb_division = (Label)e.Row.FindControl("lb_division");
                lb_division.Text = drv["equip_gong"].ToString();
                Label lb_depm = (Label)e.Row.FindControl("lb_depm");
                lb_depm.Text = drv["equip_pnag"].ToString();
                Label lb_mertGrp = (Label)e.Row.FindControl("lb_mertGrp");
                lb_mertGrp.Text = drv["materialgroup"].ToString();
                Label lb_equiCode = (Label)e.Row.FindControl("lb_equiCode");
                lb_equiCode.Text = drv["equip_code"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                lb_legend.Text = drv["legend"].ToString();
                Label lb_Part = (Label)e.Row.FindControl("lb_Part");
                lb_Part.Text = drv["parts"].ToString();
                Label lb_fullDesc = (Label)e.Row.FindControl("lb_fullDesc");
                lb_fullDesc.Text = drv["equip_desc_full"].ToString();
                Label lb_shortDesc = (Label)e.Row.FindControl("lb_shortDesc");
                lb_shortDesc.Text = drv["equip_desc_short"].ToString();
                Label lb_equiType = (Label)e.Row.FindControl("lb_equiType");
                //lb_equiType.Text = drv[""].ToString();
                Label lb_equiGrp = (Label)e.Row.FindControl("lb_equiGrp");
                //lb_equiGrp.Text = drv[""].ToString();
                Label lbUnit = (Label)e.Row.FindControl("lbUnit");
                lbUnit.Text = drv["unit"].ToString();
                ImageButton img_view = (ImageButton)e.Row.FindControl("img_view");
            }
        }
        protected void gv_AddSelEquip_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dtEQ = (DataTable)Session["dtAddEQ"];
            //string strEquipList = "";
            GridViewRow row = (GridViewRow)gv_AddSelEquip.Rows[e.RowIndex];
            Label lb_equiCode = (Label)row.FindControl("lb_equiCode");
            TextBox tb_addDesc = (TextBox)row.FindControl("tb_addDesc");

            //if (hidEq_Code.Value == "") hidEq_Code.Value = "'" + lblitem_code.Text + "'";
            //else hidEq_Code.Value += ",'" + lblitem_code.Text + "'";
            dtEQ.Rows.RemoveAt(index);
            gv_AddSelEquip.DataSource = dtEQ;
            gv_AddSelEquip.DataBind();
            dvhidEq.Style["display"] = "none";
            dvPopAdd.Style["display"] = "block";
            myModal.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            dv_viewLegend.Style.Add("display", "none");
        }
        protected void gv_SelEquip_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dtEQ = (DataTable)ViewState["dataEQ"];
            //string strEquipList = "";
            GridViewRow row = (GridViewRow)gv_SelEquip.Rows[e.RowIndex];
            Label lb_equiCode = (Label)row.FindControl("lb_equiCode");
            TextBox tb_addDesc = (TextBox)row.FindControl("tb_addDesc");
            Label lb_rowid = (Label)row.FindControl("lb_rowid");
            Label lb_Part = (Label)row.FindControl("lb_Part");
            
            if (lb_rowid.Text!="")
            {
                ViewState["chkflag"] = "1";
                objJobDb.delMyTask(lb_rowid.Text);
                //dvhidEq.Style["display"] = "block";
                bind_equipment(lb_Part.Text);
                //DataTable dtEQ2 = (DataTable)ViewState["dataEQ"];
                //gv_SelEquip.DataSource = dtEQ;
                //gv_SelEquip.DataBind();
                dvhidEq.Style["display"] = "block";
                myModal.Style["display"] = "block";
                dv_addPart.Style["display"] = "none";
                dvAddDocument.Style["display"] = "none";
                dvpopUpload.Style["display"] = "none";
                dv_viewLegend.Style.Add("display", "none");
                dvPopDocview.Style["display"] = "none";
                dvHistory.Style["display"] = "none";
                dvPopAdd.Style["display"] = "none";
            }
            else
            {
                if (dtEQ != null)
                {
                    dtEQ.Rows.RemoveAt(index);
                    gv_SelEquip.DataSource = dtEQ;
                    gv_SelEquip.DataBind();
                    dvhidEq.Style["display"] = "block";
                    myModal.Style["display"] = "block";
                    dv_addPart.Style["display"] = "none";
                    dvAddDocument.Style["display"] = "none";
                    dvpopUpload.Style["display"] = "none";
                    dv_viewLegend.Style.Add("display", "none");
                    dvPopDocview.Style["display"] = "none";
                    dvHistory.Style["display"] = "none";
                    dvPopAdd.Style["display"] = "none";
                }
            }
           

        }
        protected void btn_AddEquip_Click(object sender, EventArgs e)
        {
            int i = 1;
            int no = 0;
            int i2 = 1;
            int no2 = 0;
            if (hidNoEQ.Value != "") no = Convert.ToInt32(hidNoEQ.Value);
            string part = "";
            foreach (GridViewRow grv in gv_SelEquip.Rows)
            {
                Label lb_division = (Label)grv.FindControl("lb_division");
                Label lb_depm = (Label)grv.FindControl("lb_depm");
                Label lb_mertGrp = (Label)grv.FindControl("lb_mertGrp");
                //ListBox ddl_meterialGrp = (ListBox)grv.FindControl("ddl_meterialGrp");
                Label lb_equiCode = (Label)grv.FindControl("lb_equiCode");
                Label lb_legend = (Label)grv.FindControl("lb_legend");
                Label lb_fullDesc = (Label)grv.FindControl("lb_fullDesc");
                Label lb_fullDesc2 = (Label)grv.FindControl("lb_fullDesc2");
                Label lb_shortDesc = (Label)grv.FindControl("lb_shortDesc");
                Label lb_equiType = (Label)grv.FindControl("lb_equiType");
                Label lb_equiGrp = (Label)grv.FindControl("lb_equiGrp");
                Label lbUnit = (Label)grv.FindControl("lbUnit");
                Label lb_Part = (Label)grv.FindControl("lb_Part");
                TextBox tb_addDesc = (TextBox)grv.FindControl("tb_addDesc");
                TextBox tb_Req = (TextBox)grv.FindControl("tb_Req");
                TextBox tb_itmNo = (TextBox)grv.FindControl("tb_itmNo");
                TextBox tb_AddDesc = (TextBox)grv.FindControl("tb_AddDesc");
                //TextBox tb_stdDr = (TextBox)grv.FindControl("tb_stdDr");
                ListBox ddl_inco = (ListBox)grv.FindControl("ddl_inco");
                ListBox ddl_wa = (ListBox)grv.FindControl("ddl_wa");
                DropDownList ddl_cur = (DropDownList)grv.FindControl("ddl_cur");
                TextBox tb_supUnit = (TextBox)grv.FindControl("tb_supUnit");
                TextBox tb_supAmou = (TextBox)grv.FindControl("tb_supAmou");
                TextBox tb_localSUnit = (TextBox)grv.FindControl("tb_localSUnit");
                TextBox tb_localSAmou = (TextBox)grv.FindControl("tb_localSAmou");
                TextBox tb_localTUnit = (TextBox)grv.FindControl("tb_localTUnit");
                TextBox tb_localTAmou = (TextBox)grv.FindControl("tb_localTAmou");
                Label lb_bidNo = (Label)grv.FindControl("lb_bidNo");
                ListBox ddl_dr = (ListBox)grv.FindControl("ddl_dr");
                Label lb_rowid = (Label)grv.FindControl("lb_rowid");
                addMytaskEquipJob objEqui = new addMytaskEquipJob();

                string bidno = lb_bidNo.Text == "" ? hidBidNo.Value : lb_bidNo.Text;
                int flag = 0;
                if (lb_rowid.Text == "")
                {
                    DataTable dtVali = objJobDb.validateItm(txtJobNo.Text, bidno, lb_Part.Text, tb_itmNo.Text);
                    if (dtVali.Rows.Count != 0)
                    {
                        flag = 1;
                    }
                }
                if (flag == 0 )
                {
                    if (tb_Req.Text == "Lot" || tb_Req.Text == "Lump sum")
                    {
                        lbUnit.Text = tb_Req.Text;
                        tb_Req.Text = "1";
                    }
                    //objEqui.equip_mas_grp = ddl_meterialGrp.SelectedValue;
                    objEqui.equip_full_desc = lb_fullDesc2.Text.Replace("'", "''");
                    objEqui.section_position_name = lb_division.Text;
                    objEqui.depart_position_name = lb_depm.Text;
                    objEqui.job_no = tbAddjob.Text;
                    if (lb_bidNo.Text != "") objEqui.bid_no = lb_bidNo.Text;
                    else objEqui.bid_no = hidBidNo.Value;
                    objEqui.activity_id = hidAcID.Value;
                    objEqui.step = "J";
                    objEqui.legend = lb_legend.Text;
                    objEqui.equip_code = lb_equiCode.Text;
                    objEqui.created_by = hidLogin.Value;
                    objEqui.created_datetime = DateTime.Now;
                    objEqui.updated_by = hidLogin.Value;
                    objEqui.updated_datetime = DateTime.Now;
                    if (hidChkSupply.Value == "true") objEqui.path_code = lb_Part.Text;
                    else objEqui.path_code = tbAddPath.Text;
                    objEqui.equip_add_desc = tb_addDesc.Text;
                    if (tb_Req.Text != "") objEqui.equip_qty = Convert.ToDecimal(tb_Req.Text);
                    else objEqui.equip_qty = 0;
                    objEqui.equip_item_no = tb_itmNo.Text;
                    string val = "";
                    foreach (ListItem listItem in ddl_inco.Items)
                    {
                        if (listItem.Selected)
                        {
                            if (val == "") val = listItem.Value;
                            else val += "," + listItem.Value;
                        }
                    }
                    objEqui.equip_incoterm = val;
                    foreach (ListItem listItem in ddl_wa.Items)
                    {
                        if (listItem.Selected)
                        {
                            if (listItem.Value == "1")
                            {
                                objEqui.factory_test = true;
                            }
                            else if (listItem.Value == "2")
                            {
                                objEqui.manual_test = true;
                            }
                            else if (listItem.Value == "3")
                            {
                                objEqui.routine_test = true;
                            }
                            else if (listItem.Value == "4")
                            {
                                objEqui.waive_test = true;
                            }
                        }
                    }
                    if (tb_supUnit.Text != "") objEqui.supply_equip_unit_price = Convert.ToDecimal(tb_supUnit.Text);
                    else objEqui.supply_equip_unit_price = default(decimal);
                    if (tb_supAmou.Text != "") objEqui.supply_equip_amonut = Convert.ToDecimal(tb_supAmou.Text);
                    else objEqui.supply_equip_amonut = default(decimal);
                    if (tb_localSUnit.Text != "") objEqui.local_exwork_unit_price = Convert.ToDecimal(tb_localSUnit.Text);
                    else objEqui.local_exwork_unit_price = default(decimal);
                    if (tb_localSAmou.Text != "") objEqui.local_exwork_amonut = Convert.ToDecimal(tb_localSAmou.Text);
                    else objEqui.local_exwork_amonut = default(decimal);
                    if (tb_localTUnit.Text != "") objEqui.local_tran_unit_price = Convert.ToDecimal(tb_localTUnit.Text);
                    else objEqui.local_tran_unit_price = default(decimal);
                    if (tb_localTAmou.Text != "") objEqui.local_tran_amonut = Convert.ToDecimal(tb_localTAmou.Text);
                    else objEqui.local_tran_amonut = default(decimal);
                    objEqui.currency = ddl_cur.SelectedValue;
                    //objEqui.equip_standard_drawing = tb_stdDr.Text.Replace("'", "''");
                    objEqui.equip_add_desc = tb_addDesc.Text.Replace("'", "''");
                    objEqui.equip_mas_grp = lb_mertGrp.Text;

                    //objEqui.equip_item_no = tbAddPath.Text + "-" + i;
                    if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                    objEqui.equip_unit = lbUnit.Text;
                    if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);
                    if (hidScheNo.Value != "") objEqui.schedule_no = Convert.ToInt32(hidScheNo.Value);
                    objEqui.is_breakdown = cb_brakedown.Checked;
                    objEqui.schedule_name = hidScheName.Value;
                    objEqui.process_id = hidPID.Value;
                    objEqui.equip_bid_qty = 0;
                    int rowid = objJobDb.insertMyTaskEquip(objEqui);
                    if (hidChkSupply.Value == "true")
                    {
                        addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                        objAddpart.activity_id = hidAcID.Value;
                        objAddpart.step = "J";
                        objAddpart.job_no = tb_jobNoPart.Text;
                        if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                        objAddpart.bid_no = hidBidNo.Value;
                        if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        else objAddpart.schedule_no = null;
                        if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                        else objAddpart.schedule_name = "";
                        objAddpart.depart_position_name = hidDept.Value;
                        objAddpart.section_position_name = hidSect.Value;
                        objAddpart.part_name = lb_Part.Text;
                        objAddpart.part_description = "";
                        objAddpart.created_by = hidLogin.Value;
                        objAddpart.created_datetime = DateTime.Now;
                        objAddpart.updated_by = hidLogin.Value;
                        objAddpart.updated_datetime = DateTime.Now;
                        objAddpart.process_id = hidPID.Value;
                        objJobDb.insertMytaskPart(objAddpart);
                    }
                    if (hidBidNo.Value == "Bid Supply")
                    {
                        DataTable dtbid = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                        for (int j = 0; j < dtbid.Rows.Count; j++)
                        {
                            objJobDb.getDocNode(lb_equiCode.Text, hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, dtbid.Rows[i]["bid_no"].ToString(), hidAcID.Value, hidLogin.Value, dtbid.Rows[i]["bid_revision"].ToString(), hidScheName.Value, hidScheNo.Value, hidPID.Value);
                        }
                    }
                    else
                    {
                        objJobDb.getDocNode(lb_equiCode.Text, hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, hidBidNo.Value, hidAcID.Value, hidLogin.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value, hidPID.Value);
                    }
                    //objJobDb.getDocNode(lb_equiCode.Text, hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, hidBidNo.Value, hidAcID.Value, hidLogin.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value,hidPID.Value);
                    foreach (ListItem item in ddl_dr.Items)
                    {
                        if (item.Selected == true)
                        {
                            if (hidBidNo.Value == "Bid Supply")
                            {
                                DataTable dtbid = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                                for (int j = 0; j < dtbid.Rows.Count; j++)
                                {
                                    objJobDb.insertMytaskEqDoc(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, dtbid.Rows[i]["bid_no"].ToString(), dtbid.Rows[i]["bid_revision"].ToString(), hidScheName.Value, hidScheNo.Value, lb_equiCode.Text, lb_fullDesc2.Text.Replace("'", "''"), hidLogin.Value, item.Value, "add", hidPID.Value, rowid);
                                }
                            }
                            else
                            {
                                objJobDb.insertMytaskEqDoc(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, hidBidNo.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value, lb_equiCode.Text, lb_fullDesc2.Text.Replace("'", "''"), hidLogin.Value, item.Value, "add", hidPID.Value, rowid);
                            }
                            //objJobDb.insertMytaskEqDoc(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, hidBidNo.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value, lb_equiCode.Text, lb_fullDesc2.Text.Replace("'", "''"), hidLogin.Value, item.Value, "add",hidPID.Value,rowid);
                        }
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('ไม่สามารถบันทึกได้เนื่องจาก Item No. ซ้ำ');", true);
                }
            }
            DataTable dtTask = objJobDb.GetDrawing(hidJobNo.Value, "", hidDept.Value, hidSect.Value, txtJobRev.Text);
            if (dtTask.Rows.Count > 0)
            {
                gvTaskDoc.DataSource = dtTask;
                gvTaskDoc.DataBind();
            }
            else
            {
                gvTaskDoc.DataSource = null;
                gvTaskDoc.DataBind();
            }
            partEQ.Value = tbAddPath.Text;
            hidNoEQ.Value = no.ToString();
            dvhidEq.Style["display"] = "block";
            TabLegend.Attributes.Add("class", "tab-pane ");
            TabEquip.Attributes.Add("class", "tab-pane active");
            li2.Attributes.Remove("class");
            li1.Attributes.Add("class", "active");
            ViewState["chkflag"] = "1";
            bind_equipment(tbAddPath.Text);
            tbsearchtype.Text = "";
            gvItem.DataSource = null;
            gvItem.DataBind();
            gvItem.Visible = false;
            gv_SelEquip.DataSource = null;
            gv_SelEquip.DataBind();
            gv_SelEquip.Visible = false;
           
            Session.Add("dtAdd", null);
        }
        protected void gvLegend_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var xrowid = Convert.ToInt32(e.CommandArgument);
            GridView gvLegend = (GridView)sender;
            string xLegend = ((TextBox)gvLegend.Rows[xrowid].FindControl("gvLegend_txtLegend")).Text;
            string xLegend_QTY = ((TextBox)gvLegend.Rows[xrowid].FindControl("gvLegend_txtQTY")).Text;

            if (e.CommandName == "adddata")
            {
                if (xLegend != "")
                {
                    DataTable dtLegend = (DataTable)Session["dtLegend"];
                    dtLegend.Rows.Clear();
                    foreach (GridViewRow g1 in gvLegend.Rows)
                    {
                        TextBox txtLegend = (TextBox)g1.FindControl("gvLegend_txtLegend");
                        TextBox txtQTY = (TextBox)g1.FindControl("gvLegend_txtQTY");

                        DataRow drOld = dtLegend.NewRow();
                        drOld["legend"] = txtLegend.Text;
                        drOld["legend_qty"] = txtQTY.Text;
                        dtLegend.Rows.Add(drOld);
                    }

                    DataRow drNew = dtLegend.NewRow();
                    drNew["legend"] = "";
                    drNew["legend_qty"] = "0";
                    dtLegend.Rows.Add(drNew);

                    this.gvLegend.DataSource = dtLegend;
                    this.gvLegend.DataBind();

                    Session.Add("dtLegend", dtLegend);

                }
                dvhidEq.Style["display"] = "none";
                myModal.Style["display"] = "block";
                dv_addPart.Style["display"] = "none";
                dvAddDocument.Style["display"] = "none";
                dvpopUpload.Style["display"] = "none";
                dv_viewLegend.Style.Add("display", "none");
                dvPopDocview.Style["display"] = "none";
                dvHistory.Style["display"] = "none";
               // TabEquipAdd.Style["display"] = "none";
                TabEquipAdd.Attributes.Add("class", "tab-pane");
                TabLegend.Attributes.Add("class", "tab-pane active");
                dvPopAdd.Style["display"] = "none";
                dvPopAddEq.Style["display"] = "block";

            }
            else if (e.CommandName == "viewdata")
            {

                if (xLegend != "")
                {
                    DataTable dt = objJobDb.viewLegend(xLegend);
                    if (dt.Rows.Count > 0)
                    {
                        lblEquipCode.Text = dt.Rows[0]["equip_code"].ToString();
                        lblEquipUnit.Text = dt.Rows[0]["unit"].ToString();
                        lblEquipDesc.Text = dt.Rows[0]["equip_desc_full"].ToString();

                    }
                    else
                    {
                        lblEquipCode.Text = "-";
                        lblEquipUnit.Text = "-";
                        lblEquipDesc.Text = "-";
                    }
                    dv_viewLegend.Style.Add("display", "block");
                    myModal.Style["display"] = "block";
                    dv_addPart.Style["display"] = "none";
                    dvAddDocument.Style["display"] = "none";
                    dvpopUpload.Style["display"] = "none";
                    dvhidEq.Style["display"] = "none";
                    dvPopDocview.Style["display"] = "none";
                    dvHistory.Style["display"] = "none";
                    dvPopAddEq.Style["display"] = "none";
                }
                else
                {
                    myModal.Style["display"] = "block";
                    dvhidEq.Style["display"] = "block";
                    dv_addPart.Style["display"] = "none";
                    dvAddDocument.Style["display"] = "none";
                    dvpopUpload.Style["display"] = "none";
                    TabLegend.Attributes.Add("class", "tab-pane active");
                    TabEquip.Attributes.Add("class", "tab-pane");
                    li2.Attributes.Add("class", "active");
                    li1.Attributes.Remove("class");
                    dvPopDocview.Style["display"] = "none";
                    dvHistory.Style["display"] = "none";
                    //dv_viewLegend.Style.Add("display", "block");
                    dvPopAddEq.Style["display"] = "block";
                }
                dvPopAdd.Style["display"] = "none";
            }
            else if (e.CommandName == "removedata")
            {
                DataTable dtLegend = (DataTable)Session["dtLegend"];
                DataRow[] drSelect = dtLegend.Select("legend = '" + xLegend + "'");
                if (drSelect.Length > 0)
                    dtLegend.Rows.Remove(drSelect[0]);

                this.gvLegend.DataSource = dtLegend;
                this.gvLegend.DataBind();

                Session.Add("dtLegend", dtLegend);
                myModal.Style["display"] = "block";
                dvhidEq.Style["display"] = "block";
                dv_addPart.Style["display"] = "none";
                dvAddDocument.Style["display"] = "none";
                dvpopUpload.Style["display"] = "none";
                dvPopAdd.Style["display"] = "none";
            }
            TabLegend.Attributes.Add("class", "tab-pane active");
            TabEquip.Attributes.Add("class", "tab-pane");
            li2.Attributes.Add("class", "active");
            li1.Attributes.Remove("class");
        }
        protected void gvLegendAdd_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            var xrowid = Convert.ToInt32(e.CommandArgument);
            GridView gvLegendAdd = (GridView)sender;
            string xLegend = ((TextBox)gvLegendAdd.Rows[xrowid].FindControl("gvLegend_txtLegend")).Text;
            string xLegend_QTY = ((TextBox)gvLegendAdd.Rows[xrowid].FindControl("gvLegend_txtQTY")).Text;

            if (e.CommandName == "adddata")
            {
                if (xLegend != "")
                {
                    DataTable dtLegendAdd = (DataTable)Session["dtLegendAdd"];
                    dtLegendAdd.Rows.Clear();
                    foreach (GridViewRow g1 in gvLegendAdd.Rows)
                    {
                        TextBox txtLegend = (TextBox)g1.FindControl("gvLegend_txtLegend");
                        TextBox txtQTY = (TextBox)g1.FindControl("gvLegend_txtQTY");

                        DataRow drOld = dtLegendAdd.NewRow();
                        drOld["legend"] = txtLegend.Text;
                        drOld["legend_qty"] = txtQTY.Text;
                        dtLegendAdd.Rows.Add(drOld);
                    }

                    DataRow drNew = dtLegendAdd.NewRow();
                    drNew["legend"] = "";
                    drNew["legend_qty"] = "0";
                    dtLegendAdd.Rows.Add(drNew);

                    this.gvLegendAdd.DataSource = dtLegendAdd;
                    this.gvLegendAdd.DataBind();

                    Session.Add("dtLegendAdd", dtLegendAdd);

                }
                dvPopAddEq.Style["display"] = "none";
                dvhidEq.Style["display"] = "none";
                myModal.Style["display"] = "block";
                dv_addPart.Style["display"] = "none";
                dvAddDocument.Style["display"] = "none";
                dvpopUpload.Style["display"] = "none";
                dv_viewLegend.Style.Add("display", "none");
                dvPopDocview.Style["display"] = "none";
                dvHistory.Style["display"] = "none";
                dvPopAdd.Style["display"] = "block";
            }
            else if (e.CommandName == "viewdata")
            {

                if (xLegend != "")
                {
                    DataTable dt = objJobDb.viewLegend(xLegend);
                    if (dt.Rows.Count > 0)
                    {
                        lblEquipCodeAdd.Text = dt.Rows[0]["equip_code"].ToString();
                        lblEquipUnitAdd.Text = dt.Rows[0]["unit"].ToString();
                        lblEquipDescAdd.Text = dt.Rows[0]["equip_desc_full"].ToString();

                    }
                    else
                    {
                        lblEquipCodeAdd.Text = "-";
                        lblEquipUnitAdd.Text = "-";
                        lblEquipDescAdd.Text = "-";
                    }
                    dv_viewLegendAdd.Style.Add("display", "block");
                    myModal.Style["display"] = "block";
                    dv_addPart.Style["display"] = "none";
                    dvAddDocument.Style["display"] = "none";
                    dvpopUpload.Style["display"] = "none";
                    dvhidEq.Style["display"] = "none";
                    dvPopDocview.Style["display"] = "none";
                    dvHistory.Style["display"] = "none";
                    dvPopAddEq.Style["display"] = "none";
                    dvPopAdd.Style["display"] = "none";
                }
                else
                {
                    dvPopAdd.Style["display"] = "block";
                    myModal.Style["display"] = "block";
                    dvhidEq.Style["display"] = "none";
                    dv_addPart.Style["display"] = "none";
                    dvAddDocument.Style["display"] = "none";
                    dvpopUpload.Style["display"] = "none";
                    TabLegendAdd.Attributes.Add("class", "tab-pane active");
                    TabEquipAdd.Attributes.Add("class", "tab-pane");
                    liLG.Attributes.Add("class", "active");
                    liEQ.Attributes.Remove("class");
                    dvPopDocview.Style["display"] = "none";
                    dvHistory.Style["display"] = "none";
                    //TabEquipAdd.Style["display"] = "none";
                    dvPopAddEq.Style["display"] = "none";
                }

            }
            else if (e.CommandName == "removedata")
            {
                DataTable dtLegendAdd = (DataTable)Session["dtLegendAdd"];
                DataRow[] drSelect = dtLegendAdd.Select("legend = '" + xLegend + "'");
                if (drSelect.Length > 0)
                    dtLegendAdd.Rows.Remove(drSelect[0]);

                this.gvLegendAdd.DataSource = dtLegendAdd;
                this.gvLegendAdd.DataBind();

                Session.Add("dtLegendAdd", dtLegendAdd);
                myModal.Style["display"] = "block";
                dvhidEq.Style["display"] = "none";
                dvPopAdd.Style["display"] = "block";
                dv_addPart.Style["display"] = "none";
                dvAddDocument.Style["display"] = "none";
                dvpopUpload.Style["display"] = "none";
               
            }
            TabLegendAdd.Attributes.Add("class", "tab-pane active");
            TabEquipAdd.Attributes.Add("class", "tab-pane");
            liLG.Attributes.Add("class", "active");
            liEQ.Attributes.Remove("class");
        }
        protected void gvLegendAdd_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                TextBox txtLegend = (TextBox)e.Row.FindControl("gvLegend_txtLegend");
                txtLegend.Text = drv["legend"].ToString();
                ImageButton btnAdd = (ImageButton)e.Row.FindControl("gvLegend_btnAdd");
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("gvLegend_btnRemove");
                TextBox txtQTY = (TextBox)e.Row.FindControl("gvLegend_txtQTY");
                txtQTY.Text = drv["legend_qty"].ToString();
                if (txtLegend.Text != "")
                {
                    btnAdd.Style.Add("display", "none");
                    btnRemove.Style.Add("display", "block");
                }
                else
                {
                    btnAdd.Style.Add("display", "block");
                    btnRemove.Style.Add("display", "none");
                }
            }
        }
        protected void gvLegend_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                TextBox txtLegend = (TextBox)e.Row.FindControl("gvLegend_txtLegend");
                txtLegend.Text = drv["legend"].ToString();
                ImageButton btnAdd = (ImageButton)e.Row.FindControl("gvLegend_btnAdd");
                ImageButton btnRemove = (ImageButton)e.Row.FindControl("gvLegend_btnRemove");
                TextBox txtQTY = (TextBox)e.Row.FindControl("gvLegend_txtQTY");
                txtQTY.Text = drv["legend_qty"].ToString();
                if (txtLegend.Text != "")
                {
                    btnAdd.Style.Add("display", "none");
                    btnRemove.Style.Add("display", "block");
                }
                else
                {
                    btnAdd.Style.Add("display", "block");
                    btnRemove.Style.Add("display", "none");
                }
            }
        }
        protected void bt_close_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            dv_viewLegend.Style.Add("display", "none");
            dvhidEq.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            TabLegend.Attributes.Add("class", "tab-pane active");
            TabEquip.Attributes.Add("class", "tab-pane");
            li2.Attributes.Add("class", "active");
            li1.Attributes.Remove("class");
        }
        protected void bt_closeAdd_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            dvPopAdd.Style["display"] = "block";
            dv_viewLegendAdd.Style.Add("display", "none");
            dvhidEq.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            TabLegendAdd.Attributes.Add("class", "tab-pane active");
            TabEquipAdd.Attributes.Add("class", "tab-pane");
            liLG.Attributes.Add("class", "active");
            liEQ.Attributes.Remove("class");
        }
        protected void bt_cancLegend_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";
        }
        protected void bt_AddLegend_Click(object sender, EventArgs e)
        {
            string flag = "0";
            DataTable tmpdtLG = new DataTable();
            DataTable dtLG = new DataTable();
            dtLG.Columns.Add("dept");
            dtLG.Columns.Add("sect");
            dtLG.Columns.Add("mertGrp");
            dtLG.Columns.Add("equipCode");
            dtLG.Columns.Add("fullDesc");
            dtLG.Columns.Add("shortDesc");
            dtLG.Columns.Add("equipType");
            dtLG.Columns.Add("equipGrp");
            dtLG.Columns.Add("Unit");
            dtLG.Columns.Add("addDesc");
            dtLG.Columns.Add("reqQTY");
            dtLG.Columns.Add("parts");

            foreach (GridViewRow itemgrv in gvLegend.Rows)
            {
                TextBox txtLegend = (TextBox)itemgrv.FindControl("gvLegend_txtLegend");
                TextBox gvLegend_txtQTY = (TextBox)itemgrv.FindControl("gvLegend_txtQTY");
                if (txtLegend.Text != "")
                {
                    tmpdtLG = objJobDb.viewLegend(txtLegend.Text);
                    if (tmpdtLG.Rows.Count > 0)
                    {
                        DataRow dr = dtLG.NewRow();
                        dr["dept"] = tmpdtLG.Rows[0]["depart_position_name"].ToString();
                        dr["sect"] = tmpdtLG.Rows[0]["section_position_name"].ToString();
                        dr["mertGrp"] = tmpdtLG.Rows[0]["materialgroup"].ToString();
                        dr["equipCode"] = tmpdtLG.Rows[0]["equip_code"].ToString();
                        dr["fullDesc"] = tmpdtLG.Rows[0]["equip_desc_full"].ToString();
                        dr["shortDesc"] = tmpdtLG.Rows[0]["equip_desc_short"].ToString();
                        //dr["equipType"] = tmpdtLG.Rows[0]["equip_type"].ToString();
                        //dr["equipGrp"] = tmpdtLG.Rows[0][""].ToString();
                        dr["Unit"] = tmpdtLG.Rows[0]["unit"].ToString();
                        //dr["addDesc"] = tmpdtLG.Rows[0][""].ToString();
                        dr["reqQTY"] = gvLegend_txtQTY.Text;
                        dr["parts"] = tmpdtLG.Rows[0]["parts"].ToString();
                        dtLG.Rows.Add(dr);
                        txtLegend.BorderColor = ColorTranslator.FromHtml("#808080");
                    }
                    else
                    {
                        txtLegend.BorderColor = Color.Red;
                        flag = "1";
                    }
                }
            }
            int i = 1;
            int no = 0;
            if (hidNoLG.Value != "") no = Convert.ToInt32(hidNoLG.Value);
            if (flag == "0")
            {
                int i2 = 1;
                int no2 = 0;
                string part = "";

                foreach (DataRow dr in dtLG.Rows)
                {
                    addMytaskEquipJob objEqui = new addMytaskEquipJob();
                    //objEqui.equip_mas_grp = ddl_meterialGrp.SelectedValue;
                    objEqui.equip_full_desc = dr["fullDesc"].ToString().Replace("'", "''");
                    objEqui.section_position_name = dr["sect"].ToString();
                    objEqui.depart_position_name = dr["dept"].ToString();
                    objEqui.job_no = tbAddjob.Text;
                    objEqui.bid_no = hidBidNo.Value;
                    objEqui.activity_id = hidAcID.Value;
                    objEqui.step = "J";
                    objEqui.equip_code = dr["equipCode"].ToString();
                    objEqui.created_by = hidLogin.Value;
                    objEqui.created_datetime = DateTime.Now;
                    objEqui.updated_by = hidLogin.Value;
                    objEqui.updated_datetime = DateTime.Now;
                    if (hidChkSupply.Value == "true") objEqui.path_code = dr["parts"].ToString();
                    else objEqui.path_code = tbAddPath.Text;
                    objEqui.equip_add_desc = "";
                    if (dr["reqQTY"].ToString() != "") objEqui.equip_qty = Convert.ToDecimal(dr["reqQTY"].ToString());
                    if (hidChkSupply.Value == "true")
                    {
                        if (part == dr["parts"].ToString())
                        {
                            no2++;
                            objEqui.equip_item_no = dr["parts"].ToString() + "-" + no2;
                        }
                        else
                        {
                            objEqui.equip_item_no = dr["parts"].ToString() + "-" + i2;
                            no2 = i2;
                        }
                        part = dr["parts"].ToString();
                    }
                    else
                    {
                        if (partLG.Value == tbAddPath.Text)
                        {
                            no++;
                            objEqui.equip_item_no = tbAddPath.Text + "-" + no;
                        }
                        else
                        {
                            objEqui.equip_item_no = tbAddPath.Text + "-" + i;
                            no = i;
                        }
                    }

                    //objEqui.equip_item_no = tbAddPath.Text + "-" + i;
                    if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                    objEqui.equip_unit = dr["Unit"].ToString();
                    if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);
                    if (hidScheNo.Value != "") objEqui.schedule_no = Convert.ToInt32(hidScheNo.Value);
                    objEqui.is_breakdown = cb_brakedown.Checked;
                    objEqui.schedule_name = hidScheName.Value;
                    objEqui.supply_equip_unit_price = default(decimal);
                    objEqui.supply_equip_amonut = default(decimal);
                    objEqui.local_exwork_unit_price = default(decimal);
                    objEqui.local_exwork_amonut = default(decimal);
                    objEqui.local_tran_unit_price = default(decimal);
                    objEqui.local_tran_amonut = default(decimal);
                    objEqui.factory_test = false;
                    objEqui.manual_test = false;
                    objEqui.routine_test = false;


                    objJobDb.insertMyTaskEquip(objEqui);

                    if (hidChkSupply.Value == "true")
                    {
                        addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                        objAddpart.activity_id = hidAcID.Value;
                        objAddpart.step = "J";
                        objAddpart.job_no = tb_jobNoPart.Text;
                        if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                        objAddpart.bid_no = hidBidNo.Value;
                        if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        else objAddpart.schedule_no = null;
                        if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                        else objAddpart.schedule_name = "";
                        objAddpart.depart_position_name = hidDept.Value;
                        objAddpart.section_position_name = hidSect.Value;
                        objAddpart.part_name = dr["parts"].ToString();
                        objAddpart.part_description = "";
                        objAddpart.created_by = hidLogin.Value;
                        objAddpart.created_datetime = DateTime.Now;
                        objAddpart.updated_by = hidLogin.Value;
                        objAddpart.updated_datetime = DateTime.Now;
                        objJobDb.insertMytaskPart(objAddpart);
                    }
                }
            }
            partLG.Value = tbAddPath.Text;
            hidNoLG.Value = no.ToString();
            myModal.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            TabLegend.Attributes.Add("class", "tab-pane active");
            TabEquip.Attributes.Add("class", "tab-pane");
            li2.Attributes.Add("class", "active");
            li1.Attributes.Remove("class");
            bind_equipment("");
        }
        protected void bt_confdelPart_Click(object sender, EventArgs e)
        {
            objJobDb.delPart(hidDelPart.Value, hidDelBidNo.Value, hidDelSchNo.Value, hidDept.Value, hidSect.Value,hidDelJobNo.Value);
            bind_equipment("");
            OverlayPopup.Style["display"] = "none";
            hidDelPart.Value = "";
            hidDelBidNo.Value = "";
            hidDelSchNo.Value = "";
            hidDelJobNo.Value = "";
        }
        protected void bt_saveConfig_Click(object sender, EventArgs e)
        {
            addconfig_cateJob obj = new addconfig_cateJob();
            foreach (GridViewRow grv in gv_config.Rows)
            {
                Label lb_cateCode = (Label)grv.FindControl("lb_cateCode");
                Label lb_cateName = (Label)grv.FindControl("lb_cateName");
                TextBox tb_reser = (TextBox)grv.FindControl("tb_reser");
                DropDownList ddl_round = (DropDownList)grv.FindControl("ddl_round");
                obj.job_no = txtJobNo.Text;
                obj.job_revision = txtJobRev.Text == "" ? 0 : Convert.ToInt32(txtJobRev.Text);
                obj.bid_no = "";
                obj.bid_revision = 0;
                obj.depart_position_name = hidDept.Value;
                obj.section_position_name = hidSect.Value;
                obj.cate_code = lb_cateCode.Text;
                obj.cate_name = lb_cateName.Text;
                obj.percent_reservation = tb_reser.Text == "" ? 0 : Convert.ToDecimal(tb_reser.Text);
                obj.is_round_up = ddl_round.SelectedValue == "" ? 0 : Convert.ToInt32(ddl_round.SelectedValue);
                obj.created_by = hidLogin.Value;
                obj.created_datetime = DateTime.Now;
                obj.updated_by = hidLogin.Value;
                obj.updated_datetime = DateTime.Now;
                objJobDb.insertconfig_cate(obj);
            }
            getconfig();
        }
        protected void bt_cancConfig_Click(object sender, EventArgs e)
        {
            dvConfig.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";
        }
        protected void gv_config_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lb_cateCode = (Label)e.Row.FindControl("lb_cateCode");
                lb_cateCode.Text = drv["cate_code"].ToString();
                Label lb_cateName = (Label)e.Row.FindControl("lb_cateName");
                lb_cateName.Text = drv["cate_name"].ToString();


                TextBox tb_reser = (TextBox)e.Row.FindControl("tb_reser");
                tb_reser.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                tb_reser.Text = drv["percent_reservation"].ToString();

                DropDownList ddl_round = (DropDownList)e.Row.FindControl("ddl_round");
                ddl_round.Items.Clear();
                ddl_round.Items.Insert(0, new ListItem("Do Not Round Number", "0"));
                ddl_round.Items.Insert(1, new ListItem("Round Number to Five or Ten", "1"));
                if (drv["is_round_up"].ToString() != "")
                {
                    ddl_round.SelectedValue = drv["is_round_up"].ToString();
                }
            }
        }
        protected void btn_saveAddEQ_Click(object sender, EventArgs e)
        {
            DataTable dt = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
            foreach (GridViewRow grv in gv_AddSelEquip.Rows)
            {
                Label lb_division = (Label)grv.FindControl("lb_division");
                Label lb_depm = (Label)grv.FindControl("lb_depm");
                Label lb_mertGrp = (Label)grv.FindControl("lb_mertGrp");
                //ListBox ddl_meterialGrp = (ListBox)grv.FindControl("ddl_meterialGrp");
                Label lb_equiCode = (Label)grv.FindControl("lb_equiCode");
                Label lb_legend = (Label)grv.FindControl("lb_legend");
                Label lb_fullDesc = (Label)grv.FindControl("lb_fullDesc");
                Label lb_fullDesc2 = (Label)grv.FindControl("lb_fullDesc2");
                Label lb_shortDesc = (Label)grv.FindControl("lb_shortDesc");
                Label lb_equiType = (Label)grv.FindControl("lb_equiType");
                Label lb_equiGrp = (Label)grv.FindControl("lb_equiGrp");
                Label lbUnit = (Label)grv.FindControl("lbUnit");
                Label lb_Part = (Label)grv.FindControl("lb_Part");
                TextBox tb_addDesc = (TextBox)grv.FindControl("tb_addDesc");
                TextBox tb_Req = (TextBox)grv.FindControl("tb_Req");
                TextBox tb_itmNo = (TextBox)grv.FindControl("tb_itmNo");
                TextBox tb_AddDesc = (TextBox)grv.FindControl("tb_AddDesc");
                //TextBox tb_stdDr = (TextBox)grv.FindControl("tb_stdDr");
                ListBox ddl_inco = (ListBox)grv.FindControl("ddl_inco");
                ListBox ddl_wa = (ListBox)grv.FindControl("ddl_wa");
                DropDownList ddl_cur = (DropDownList)grv.FindControl("ddl_cur");
                TextBox tb_supUnit = (TextBox)grv.FindControl("tb_supUnit");
                TextBox tb_supAmou = (TextBox)grv.FindControl("tb_supAmou");
                TextBox tb_localSUnit = (TextBox)grv.FindControl("tb_localSUnit");
                TextBox tb_localSAmou = (TextBox)grv.FindControl("tb_localSAmou");
                TextBox tb_localTUnit = (TextBox)grv.FindControl("tb_localTUnit");
                TextBox tb_localTAmou = (TextBox)grv.FindControl("tb_localTAmou");
                Label lb_bidNo = (Label)grv.FindControl("lb_bidNo");
                ListBox ddl_dr = (ListBox)grv.FindControl("ddl_dr");
                Label lb_rowid = (Label)grv.FindControl("lb_rowid");
                addMytaskEquipJob objEqui = new addMytaskEquipJob();
                //objEqui.equip_mas_grp = ddl_meterialGrp.SelectedValue;
                if (tb_Req.Text == "Lot" || tb_Req.Text == "Lump sum")
                {
                    lbUnit.Text = tb_Req.Text;
                    tb_Req.Text = "1";
                }
                if (hidBidNo.Value== "Bid Supply")
                {
                   
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        //int flag = 0;
                        //if (lb_rowid.Text == "")
                        //{
                        //    DataTable dtVali = objJobDb.validateItm(txtJobNo.Text, dt.Rows[i]["bid_no"].ToString(), lb_Part.Text, tb_itmNo.Text);
                        //    if (dtVali.Rows.Count != 0)
                        //    {
                        //        flag = 1;
                        //    }
                        //}
                        DataTable dtVali = objJobDb.validateItm(txtJobNo.Text, dt.Rows[i]["bid_no"].ToString(), lb_Part.Text, tb_itmNo.Text);
                        if (dtVali.Rows.Count == 0)
                        {
                            objEqui.equip_full_desc = lb_fullDesc2.Text.Replace("'", "''");
                            objEqui.section_position_name = lb_division.Text;
                            objEqui.depart_position_name = lb_depm.Text;
                            objEqui.job_no = tbAddjob.Text;
                            objEqui.bid_no = dt.Rows[i]["bid_no"].ToString();
                            objEqui.activity_id = hidAcID.Value;
                            objEqui.step = "J";
                            objEqui.legend = lb_legend.Text;
                            objEqui.equip_code = lb_equiCode.Text;
                            objEqui.created_by = hidLogin.Value;
                            objEqui.created_datetime = DateTime.Now;
                            objEqui.updated_by = hidLogin.Value;
                            objEqui.updated_datetime = DateTime.Now;
                            objEqui.path_code = lb_Part.Text;
                            objEqui.equip_add_desc = tb_addDesc.Text;
                            if (tb_Req.Text != "") objEqui.equip_qty = Convert.ToDecimal(tb_Req.Text);
                            else objEqui.equip_qty = 0;
                            objEqui.equip_item_no = tb_itmNo.Text;
                            string val = "";
                            foreach (ListItem listItem in ddl_inco.Items)
                            {
                                if (listItem.Selected)
                                {
                                    if (val == "") val = listItem.Value;
                                    else val += "," + listItem.Value;
                                }
                            }
                            objEqui.equip_incoterm = val;
                            foreach (ListItem listItem in ddl_wa.Items)
                            {
                                if (listItem.Selected)
                                {
                                    if (listItem.Value == "1")
                                    {
                                        objEqui.factory_test = true;
                                    }
                                    else if (listItem.Value == "2")
                                    {
                                        objEqui.manual_test = true;
                                    }
                                    else if (listItem.Value == "3")
                                    {
                                        objEqui.routine_test = true;
                                    }
                                    else if (listItem.Value == "4")
                                    {
                                        objEqui.waive_test = true;
                                    }
                                }
                            }
                            if (tb_supUnit.Text != "") objEqui.supply_equip_unit_price = Convert.ToDecimal(tb_supUnit.Text);
                            else objEqui.supply_equip_unit_price = default(decimal);
                            if (tb_supAmou.Text != "") objEqui.supply_equip_amonut = Convert.ToDecimal(tb_supAmou.Text);
                            else objEqui.supply_equip_amonut = default(decimal);
                            if (tb_localSUnit.Text != "") objEqui.local_exwork_unit_price = Convert.ToDecimal(tb_localSUnit.Text);
                            else objEqui.local_exwork_unit_price = default(decimal);
                            if (tb_localSAmou.Text != "") objEqui.local_exwork_amonut = Convert.ToDecimal(tb_localSAmou.Text);
                            else objEqui.local_exwork_amonut = default(decimal);
                            if (tb_localTUnit.Text != "") objEqui.local_tran_unit_price = Convert.ToDecimal(tb_localTUnit.Text);
                            else objEqui.local_tran_unit_price = default(decimal);
                            if (tb_localTAmou.Text != "") objEqui.local_tran_amonut = Convert.ToDecimal(tb_localTAmou.Text);
                            else objEqui.local_tran_amonut = default(decimal);
                            objEqui.currency = ddl_cur.SelectedValue;
                            //objEqui.equip_standard_drawing = tb_stdDr.Text.Replace("'", "''");
                            objEqui.equip_add_desc = tb_addDesc.Text.Replace("'", "''");
                            objEqui.equip_mas_grp = lb_mertGrp.Text;

                            if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                            objEqui.equip_unit = lbUnit.Text;
                            if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);
                            if (hidScheNo.Value != "") objEqui.schedule_no = Convert.ToInt32(hidScheNo.Value);
                            objEqui.is_breakdown = cb_brakedownAdd.Checked;
                            objEqui.schedule_name = hidScheName.Value;
                            objEqui.process_id = hidPID.Value;
                            int rowid = objJobDb.insertMyTaskEquip(objEqui);

                            addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                            objAddpart.activity_id = hidAcID.Value;
                            objAddpart.step = "J";
                            objAddpart.job_no = tb_jobNoPart.Text;
                            if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                            objAddpart.bid_no = dt.Rows[i]["bid_no"].ToString();//hidBidNo.Value;
                            if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                            if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                            else objAddpart.schedule_no = null;
                            if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                            else objAddpart.schedule_name = "";
                            objAddpart.depart_position_name = hidDept.Value;
                            objAddpart.section_position_name = hidSect.Value;
                            objAddpart.part_name = lb_Part.Text;
                            objAddpart.part_description = "";
                            objAddpart.created_by = hidLogin.Value;
                            objAddpart.created_datetime = DateTime.Now;
                            objAddpart.updated_by = hidLogin.Value;
                            objAddpart.updated_datetime = DateTime.Now;
                            objAddpart.process_id = hidPID.Value;
                            objJobDb.insertMytaskPart(objAddpart);

                            objJobDb.getDocNode(lb_equiCode.Text, hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, dt.Rows[i]["bid_no"].ToString(), hidAcID.Value, hidLogin.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value, hidPID.Value);
                            foreach (ListItem item in ddl_dr.Items)
                            {
                                if (item.Selected == true)
                                {
                                    objJobDb.insertMytaskEqDoc(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, dt.Rows[i]["bid_no"].ToString(), hidBidRev.Value, hidScheName.Value, hidScheNo.Value, lb_equiCode.Text, lb_fullDesc2.Text.Replace("'", "''"), hidLogin.Value, item.Value, "", hidPID.Value, rowid);
                                }

                            }
                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('ไม่สามารถบันทึกได้เนื่องจาก Item No. ซ้ำ');", true);
                        }
                        
                    }

                }
                else
                {
                    //int flag = 0;
                    //if (lb_rowid.Text == "")
                    //{
                    //    DataTable dtVali = objJobDb.validateItm(txtJobNo.Text, hidBidNo.Value, lb_Part.Text, tb_itmNo.Text);
                    //    if (dtVali.Rows.Count != 0)
                    //    {
                    //        flag = 1;
                    //    }
                    //}
                    DataTable dtVali = objJobDb.validateItm(txtJobNo.Text, hidBidNo.Value, lb_Part.Text, tb_itmNo.Text);
                    if (dtVali.Rows.Count == 0)
                    {
                        objEqui.equip_full_desc = lb_fullDesc2.Text.Replace("'", "''");
                        objEqui.section_position_name = lb_division.Text;
                        objEqui.depart_position_name = lb_depm.Text;
                        objEqui.job_no = tbAddjob.Text;
                        objEqui.bid_no = hidBidNo.Value;
                        objEqui.activity_id = hidAcID.Value;
                        objEqui.step = "J";
                        objEqui.legend = lb_legend.Text;
                        objEqui.equip_code = lb_equiCode.Text;
                        objEqui.created_by = hidLogin.Value;
                        objEqui.created_datetime = DateTime.Now;
                        objEqui.updated_by = hidLogin.Value;
                        objEqui.updated_datetime = DateTime.Now;
                        objEqui.path_code = lb_Part.Text;
                        objEqui.equip_add_desc = tb_addDesc.Text;
                        if (tb_Req.Text != "") objEqui.equip_qty = Convert.ToDecimal(tb_Req.Text);
                        else objEqui.equip_qty = 0;
                        objEqui.equip_item_no = tb_itmNo.Text;
                        string val = "";
                        foreach (ListItem listItem in ddl_inco.Items)
                        {
                            if (listItem.Selected)
                            {
                                if (val == "") val = listItem.Value;
                                else val += "," + listItem.Value;
                            }
                        }
                        objEqui.equip_incoterm = val;
                        foreach (ListItem listItem in ddl_wa.Items)
                        {
                            if (listItem.Selected)
                            {
                                if (listItem.Value == "1")
                                {
                                    objEqui.factory_test = true;
                                }
                                else if (listItem.Value == "2")
                                {
                                    objEqui.manual_test = true;
                                }
                                else if (listItem.Value == "3")
                                {
                                    objEqui.routine_test = true;
                                }
                                else if (listItem.Value == "4")
                                {
                                    objEqui.waive_test = true;
                                }
                            }
                        }
                        if (tb_supUnit.Text != "") objEqui.supply_equip_unit_price = Convert.ToDecimal(tb_supUnit.Text);
                        else objEqui.supply_equip_unit_price = default(decimal);
                        if (tb_supAmou.Text != "") objEqui.supply_equip_amonut = Convert.ToDecimal(tb_supAmou.Text);
                        else objEqui.supply_equip_amonut = default(decimal);
                        if (tb_localSUnit.Text != "") objEqui.local_exwork_unit_price = Convert.ToDecimal(tb_localSUnit.Text);
                        else objEqui.local_exwork_unit_price = default(decimal);
                        if (tb_localSAmou.Text != "") objEqui.local_exwork_amonut = Convert.ToDecimal(tb_localSAmou.Text);
                        else objEqui.local_exwork_amonut = default(decimal);
                        if (tb_localTUnit.Text != "") objEqui.local_tran_unit_price = Convert.ToDecimal(tb_localTUnit.Text);
                        else objEqui.local_tran_unit_price = default(decimal);
                        if (tb_localTAmou.Text != "") objEqui.local_tran_amonut = Convert.ToDecimal(tb_localTAmou.Text);
                        else objEqui.local_tran_amonut = default(decimal);
                        objEqui.currency = ddl_cur.SelectedValue;
                        //objEqui.equip_standard_drawing = tb_stdDr.Text.Replace("'", "''");
                        objEqui.equip_add_desc = tb_addDesc.Text.Replace("'", "''");
                        objEqui.equip_mas_grp = lb_mertGrp.Text;

                        if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        objEqui.equip_unit = lbUnit.Text;
                        if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);
                        if (hidScheNo.Value != "") objEqui.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        objEqui.is_breakdown = cb_brakedownAdd.Checked;
                        objEqui.schedule_name = hidScheName.Value;
                        objEqui.process_id = hidPID.Value;
                        int rowid = objJobDb.insertMyTaskEquip(objEqui);

                        addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                        objAddpart.activity_id = hidAcID.Value;
                        objAddpart.step = "J";
                        objAddpart.job_no = tb_jobNoPart.Text;
                        if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                        objAddpart.bid_no = hidBidNo.Value;
                        if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        else objAddpart.schedule_no = null;
                        if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                        else objAddpart.schedule_name = "";
                        objAddpart.depart_position_name = hidDept.Value;
                        objAddpart.section_position_name = hidSect.Value;
                        objAddpart.part_name = lb_Part.Text;
                        objAddpart.part_description = "";
                        objAddpart.created_by = hidLogin.Value;
                        objAddpart.created_datetime = DateTime.Now;
                        objAddpart.updated_by = hidLogin.Value;
                        objAddpart.updated_datetime = DateTime.Now;
                        objAddpart.process_id = hidPID.Value;
                        objJobDb.insertMytaskPart(objAddpart);

                        objJobDb.getDocNode(lb_equiCode.Text, hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, hidBidNo.Value, hidAcID.Value, hidLogin.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value, hidPID.Value);
                        foreach (ListItem item in ddl_dr.Items)
                        {
                            if (item.Selected == true)
                            {
                                objJobDb.insertMytaskEqDoc(hidDept.Value, hidSect.Value, txtJobNo.Text, txtJobRev.Text, hidBidNo.Value, hidBidRev.Value, hidScheName.Value, hidScheNo.Value, lb_equiCode.Text, lb_fullDesc2.Text.Replace("'", "''"), hidLogin.Value, item.Value, "", hidPID.Value, rowid);
                            }
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('ไม่สามารถบันทึกได้เนื่องจาก Item No. ซ้ำ');", true);
                    }
                    
                }
                

            }
            DataTable dtTask = objJobDb.GetDrawing(hidJobNo.Value, "", hidDept.Value, hidSect.Value, txtJobRev.Text);
            if (dtTask.Rows.Count > 0)
            {
                gvTaskDoc.DataSource = dtTask;
                gvTaskDoc.DataBind();
            }
            else
            {
                gvTaskDoc.DataSource = null;
                gvTaskDoc.DataBind();
            }
            bind_equipment("");
            tbsearchtypeAdd.Text = "";
            gvItemAdd.DataSource = null;
            gvItemAdd.DataBind();
            gvItemAdd.Visible = false;
            gv_AddSelEquip.DataSource = null;
            gv_AddSelEquip.DataBind();
            gv_AddSelEquip.Visible = false;
            Label59.Visible = false;
            cb_brakedownAdd.Visible = false;
            Session.Add("dtAddEQ", null);
        }
        protected void btn_cancAddEQ_Click(object sender, EventArgs e)
        {
            dvPopAdd.Style["display"] = "none";
            myModal.Style["display"] = "none";
            dvhidEq.Style["display"] = "block";
            gvItemAdd.DataSource = null;
            gvItemAdd.DataBind();
            gv_AddSelEquip.DataSource = null;
            gv_AddSelEquip.DataBind();
            Label59.Visible = false;
            cb_brakedownAdd.Visible = false;
            Session.Add("dtAddEQ", null);
            ViewState["addAllPart"] =null;
        }
        protected void btn_saveAddLG_Click(object sender, EventArgs e)
        {
            string flag = "0";
            DataTable tmpdtLG = new DataTable();
            DataTable dtLG = new DataTable();
            dtLG.Columns.Add("dept");
            dtLG.Columns.Add("sect");
            dtLG.Columns.Add("mertGrp");
            dtLG.Columns.Add("equipCode");
            dtLG.Columns.Add("fullDesc");
            dtLG.Columns.Add("shortDesc");
            dtLG.Columns.Add("equipType");
            dtLG.Columns.Add("equipGrp");
            dtLG.Columns.Add("Unit");
            dtLG.Columns.Add("addDesc");
            dtLG.Columns.Add("reqQTY");
            dtLG.Columns.Add("parts");

            foreach (GridViewRow itemgrv in gvLegendAdd.Rows)
            {
                TextBox txtLegend = (TextBox)itemgrv.FindControl("gvLegend_txtLegend");
                TextBox gvLegend_txtQTY = (TextBox)itemgrv.FindControl("gvLegend_txtQTY");
                if (txtLegend.Text != "")
                {
                    tmpdtLG = objJobDb.viewLegend(txtLegend.Text);
                    if (tmpdtLG.Rows.Count > 0)
                    {
                        DataRow dr = dtLG.NewRow();
                        dr["dept"] = tmpdtLG.Rows[0]["depart_position_name"].ToString();
                        dr["sect"] = tmpdtLG.Rows[0]["section_position_name"].ToString();
                        dr["mertGrp"] = tmpdtLG.Rows[0]["materialgroup"].ToString();
                        dr["equipCode"] = tmpdtLG.Rows[0]["equip_code"].ToString();
                        dr["fullDesc"] = tmpdtLG.Rows[0]["equip_desc_full"].ToString();
                        dr["shortDesc"] = tmpdtLG.Rows[0]["equip_desc_short"].ToString();
                        //dr["equipType"] = tmpdtLG.Rows[0]["equip_type"].ToString();
                        //dr["equipGrp"] = tmpdtLG.Rows[0][""].ToString();
                        dr["Unit"] = tmpdtLG.Rows[0]["unit"].ToString();
                        //dr["addDesc"] = tmpdtLG.Rows[0][""].ToString();
                        dr["reqQTY"] = gvLegend_txtQTY.Text;
                        dr["parts"] = tmpdtLG.Rows[0]["parts"].ToString();
                        dtLG.Rows.Add(dr);
                        txtLegend.BorderColor = ColorTranslator.FromHtml("#808080");
                    }
                    else
                    {
                        txtLegend.BorderColor = Color.Red;
                        flag = "1";
                    }
                }
            }
            int i = 1;
            int no = 0;
            //if (hidNoLG.Value != "") no = Convert.ToInt32(hidNoLG.Value);
            if (flag == "0")
            {
                string part = "";

                foreach (DataRow dr in dtLG.Rows)
                {
                    if (dr["parts"].ToString()!="")
                    {

                        addMytaskEquipJob objEqui = new addMytaskEquipJob();
                        //objEqui.equip_mas_grp = ddl_meterialGrp.SelectedValue;
                        objEqui.equip_full_desc = dr["fullDesc"].ToString().Replace("'", "''");
                        objEqui.section_position_name = dr["sect"].ToString();
                        objEqui.depart_position_name = dr["dept"].ToString();
                        objEqui.job_no = tbAddjob.Text;
                        objEqui.bid_no = hidBidNo.Value;
                        objEqui.activity_id = hidAcID.Value;
                        objEqui.step = "J";
                        objEqui.equip_code = dr["equipCode"].ToString();
                        objEqui.created_by = hidLogin.Value;
                        objEqui.created_datetime = DateTime.Now;
                        objEqui.updated_by = hidLogin.Value;
                        objEqui.updated_datetime = DateTime.Now;
                        objEqui.path_code = dr["parts"].ToString();
                        objEqui.equip_add_desc = "";
                        if (dr["reqQTY"].ToString() != "") objEqui.equip_qty = Convert.ToDecimal(dr["reqQTY"].ToString());

                        if (part == dr["parts"].ToString())
                        {
                            no++;
                            objEqui.equip_item_no = dr["parts"].ToString() + "-" + no;
                        }
                        else
                        {
                            objEqui.equip_item_no = dr["parts"].ToString() + "-" + i;
                            no = i;
                        }
                        part = dr["parts"].ToString();
                        if (hidBidRev.Value != "") objEqui.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        objEqui.equip_unit = dr["Unit"].ToString();
                        if (tbAddrev.Text != "") objEqui.job_revision = Convert.ToInt32(tbAddrev.Text);
                        if (hidScheNo.Value != "") objEqui.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        objEqui.is_breakdown = cb_brakedown.Checked;
                        objEqui.schedule_name = hidScheName.Value;
                        objEqui.supply_equip_unit_price = default(decimal);
                        objEqui.supply_equip_amonut = default(decimal);
                        objEqui.local_exwork_unit_price = default(decimal);
                        objEqui.local_exwork_amonut = default(decimal);
                        objEqui.local_tran_unit_price = default(decimal);
                        objEqui.local_tran_amonut = default(decimal);
                        objEqui.factory_test = false;
                        objEqui.manual_test = false;
                        objEqui.routine_test = false;

                        objJobDb.insertMyTaskEquip(objEqui);

                        addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                        objAddpart.activity_id = hidAcID.Value;
                        objAddpart.step = "J";
                        objAddpart.job_no = tb_jobNoPart.Text;
                        if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                        objAddpart.bid_no = hidBidNo.Value;
                        if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        else objAddpart.schedule_no = null;
                        if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                        else objAddpart.schedule_name = "";
                        objAddpart.depart_position_name = hidDept.Value;
                        objAddpart.section_position_name = hidSect.Value;
                        objAddpart.part_name = dr["parts"].ToString();
                        objAddpart.part_description = "";
                        objAddpart.created_by = hidLogin.Value;
                        objAddpart.created_datetime = DateTime.Now;
                        objAddpart.updated_by = hidLogin.Value;
                        objAddpart.updated_datetime = DateTime.Now;
                        objJobDb.insertMytaskPart(objAddpart);
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('ไม่สามารถ Add ได้เนื่องจากไม่ได้ระบุ Part');", true);
                        myModal.Style["display"] = "block";
                        dvPopAdd.Style["display"] = "block";
                    }
                }
            }
            if (flag == "1")
            {
                dvPopAdd.Style["display"] = "block";
                myModal.Style["display"] = "block";
            }
            else
            {
                dvPopAdd.Style["display"] = "none";
                myModal.Style["display"] = "none";
                DataTable dtAdd = new DataTable();
                dtAdd.Columns.Add("legend");
                dtAdd.Columns.Add("legend_qty");
                DataRow drAdd = dtAdd.NewRow();
                drAdd["legend"] = "";
                drAdd["legend_qty"] = "0";
                dtAdd.Rows.Add(drAdd);
                if (dtAdd.Rows.Count > 0)
                {
                    gvLegendAdd.DataSource = dtAdd;
                    gvLegendAdd.DataBind();
                    Session.Add("dtLegendAdd", dtAdd);
                }
            }

            dvhidEq.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dvPopAddEq.Style["display"] = "none";
            TabEquipAdd.Attributes.Add("class", "tab-pane active");
            TabLegendAdd.Attributes.Add("class", "tab-pane");
            liEQ.Attributes.Add("class", "active");
            liLG.Attributes.Remove("class");
            bind_equipment("");
        }
        protected void btn_cancAddLG_Click(object sender, EventArgs e)
        {
            dvPopAdd.Style["display"] = "none";
            myModal.Style["display"] = "none";
            TabEquipAdd.Attributes.Add("class", "tab-pane active");
            TabLegendAdd.Attributes.Add("class", "tab-pane");
            liEQ.Attributes.Add("class", "active");
            liLG.Attributes.Remove("class");
            dvhidEq.Style["display"] = "block";
        }
        protected void btnSearchAdd_Click(object sender, EventArgs e)
        {
            bindgvItemAdd();
        }
        protected void bindgvItemAdd()
        {
            DataTable dt = new DataTable();
            string flag = "";
            if (rdbEquipAdd.Checked == true)
            {
                flag = "Equi";
            }
            else if (rdbBomAdd.Checked == true)
            {
                flag = "Bom";
            }
            //else if (rdbMytempAdd.Checked == true)
            //{
            //    flag = "Temp";
            //}
            //else if (rdbPrejobAdd.Checked == true)
            //{
            //    flag = "Job";
            //}
            
            dt = objJobDb.getEqui(tbsearchtypeAdd.Text, ddlSelectBidAdd.SelectedValue, ddlSelectBomAdd.SelectedValue, hidDept.Value, hidSect.Value, flag, "", "");
  
            //DataTable dt = objJobDb.getEqui(tbsearchtype.Text, ddlSelectBid.SelectedValue, ddlSelectBom.SelectedValue, hidDept.Value, hidSect.Value, flag);
            if (dt.Rows.Count > 0)
            {
                gvItemAdd.DataSource = dt;
                gvItemAdd.DataBind();
                gvItemAdd.UseAccessibleHeader = true;
                gvItemAdd.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvItemAdd.Visible = true;
            }
            else
            {
                gvItemAdd.DataSource = null;
                gvItemAdd.DataBind();
                gvItemAdd.Visible = true;
            }
            btn_selectItem.Style["visibility"] = "vissible";
            myModal.Style["display"] = "block";
            dvPopAddEq.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvPopDocview.Style["display"] = "none";
            dv_addPart.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dvPopAdd.Style["display"] = "block";
            //TabUploadDoc.Style["display"] = "none";
            //TabSelectDoc.Style["display"] = "none";
            if (rdbEquipAdd.Checked == true)
            {
                rdbEquipAdd.Checked = true;
            }
            else if (rdbBomAdd.Checked == true)
            {
                rdbBomAdd.Checked = true;
            }
            //else if (rdbMytempAdd.Checked == true)
            //{
            //    rdbMytempAdd.Checked = true;
            //}
        }
        protected void btn_selectItem_Click(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("depart_position_name");
            dt.Columns.Add("section_position_name");
            dt.Columns.Add("equip_mas_grp");
            dt.Columns.Add("equip_code");
            dt.Columns.Add("legend");
            dt.Columns.Add("equip_item_no");
            dt.Columns.Add("equip_full_desc");
            dt.Columns.Add("unit");
            dt.Columns.Add("equip_add_desc");
            dt.Columns.Add("equip_standard_drawing");
            dt.Columns.Add("equip_qty");
            dt.Columns.Add("equip_code2");
            dt.Columns.Add("currency");
            dt.Columns.Add("equip_incoterm");
            dt.Columns.Add("factory_test");
            dt.Columns.Add("manual_test");
            dt.Columns.Add("routine_test");
            dt.Columns.Add("waive_test");
            dt.Columns.Add("supply_equip_unit_price");
            dt.Columns.Add("supply_equip_amonut");
            dt.Columns.Add("local_exwork_unit_price");
            dt.Columns.Add("local_exwork_amonut");
            dt.Columns.Add("local_tran_unit_price");
            dt.Columns.Add("local_tran_amonut");
            dt.Columns.Add("type");
            dt.Columns.Add("view");
            dt.Columns.Add("part");
            dt.Columns.Add("edit");
            dt.Columns.Add("bid_no");
            dt.Columns.Add("Part_Sort");
            dt.Columns.Add("rowid");
            int i = 1;
            int no = 0;
            string part = "";
            DataTable dtchkpart = new DataTable();
            dtchkpart.Columns.Add("Part_Sort");
            dtchkpart.Columns.Add("part");
            dtchkpart.Columns.Add("equip_item_no");
            foreach (GridViewRow grv in gvItemAdd.Rows)
            {
                
                CheckBox cb_sub = (CheckBox)grv.FindControl("cb_sub");
                Label lb_division = (Label)grv.FindControl("lb_division");
                Label lb_depm = (Label)grv.FindControl("lb_depm");
                Label lb_mertGrp = (Label)grv.FindControl("lb_mertGrp");
                //ListBox ddl_meterialGrp = (ListBox)grv.FindControl("ddl_meterialGrp");
                Label lb_equiCode = (Label)grv.FindControl("lb_equiCode");
                Label lb_legend = (Label)grv.FindControl("lb_legend");
                Label lb_fullDesc = (Label)grv.FindControl("lb_fullDesc");
                Label lb_shortDesc = (Label)grv.FindControl("lb_shortDesc");
                Label lb_equiType = (Label)grv.FindControl("lb_equiType");
                Label lb_equiGrp = (Label)grv.FindControl("lb_equiGrp");
                Label lbUnit = (Label)grv.FindControl("lbUnit");
                Label lb_Part = (Label)grv.FindControl("lb_Part");
                Label lb_equipBom = (Label)grv.FindControl("lb_equipBom");
                CheckBox cb_explan = (CheckBox)grv.FindControl("cb_explan");
                
                if (cb_sub.Checked == true)
                {
                    DataTable dtitmno = new DataTable();
                    if(hidChkSupply.Value=="true") dtitmno = objJobDb.GetItmNo_sup(hidJobNo.Value, lb_Part.Text);
                    else dtitmno = objJobDb.GetItmNo(hidJobNo.Value, lb_Part.Text,hidBidNo.Value);
                    byte[] bytes = null;
                    System.Text.ASCIIEncoding encode = new System.Text.ASCIIEncoding();
                    if (dtitmno.Rows.Count > 0) 
                    {
                        //string[] slp = dtitmno.Rows[0]["equip_item_no"].ToString().Split('-');
                        //itmno = Convert.ToInt32(slp[1]+1);
                        //if (ViewState["addAllPart"].ToString() == "") ViewState["addAllPart"] = lb_Part.Text;
                        //else ViewState["addAllPart"] += "," + lb_Part.Text;
                        //string[] str = ViewState["addAllPart"].ToString().Split(',');
                        DataTable dtpart = (DataTable)ViewState["addAllPart"];
                        if (dtpart!=null)
                        {
                            //List<string> itm = new List<string>();
                            //foreach (string strs in str)
                            //{
                            //    itm.Add(strs);
                            //}
                            //int result = itm.Count(s => s.Contains(lb_Part.Text));
                            //part = lb_Part.Text + "-" + (result + 1).ToString();
                            //if (ViewState["addAllPart"].ToString() == "") ViewState["addAllPart"] = part;
                            //else ViewState["addAllPart"] += "," + part;
                            DataRow[] drSelect = dtpart.Select("part = '" + lb_Part.Text + "'");
                            DataRow drt = dtchkpart.NewRow();
                            if (drSelect.Length >0)
                            {
                                //DataView dtv = drSelect.OrderByDescending(dtpart[""].ToString() => dtpart[""].ToString());
                                DataTable dtTmp = drSelect.CopyToDataTable();
                                IOrderedEnumerable<DataRow> result;
                                result = dtTmp.Select().OrderByDescending(row => row["Part_Sort"]);
                                DataTable dtSort = result.CopyToDataTable();
                                string[] slp = dtSort.Rows[0]["equip_item_no"].ToString().Split('-');
                                if (slp.Length == 2)
                                {
                                    //int num = Convert.ToInt32(slp[1]) + 1;
                                    //part = lb_Part.Text + "-" + num;
                                    double itm = double.Parse(slp[1]);
                                    int num = Convert.ToInt32(itm) + 1;
                                    part = lb_Part.Text + "-" + num;
                                }
                            }
                            else
                            {
                                if (dtitmno.Rows.Count > 0)
                                {
                                    string[] slp = dtitmno.Rows[dtitmno.Rows.Count - 1]["equip_item_no"].ToString().Split('-');
                                    if (slp.Length == 2)
                                    {
                                        double itm = double.Parse(slp[1]);
                                        int num = Convert.ToInt32(itm) + 1;
                                        part = lb_Part.Text + "-" + num;
                                        //int num = Convert.ToInt32(slp[1]) + 1;
                                        //part = lb_Part.Text + "-" + num;
                                    }
                                }
                                else
                                {
                                    part = lb_Part.Text + "-1";
                                }
                            }
                            
                            bytes = encode.GetBytes(part);
                            drt["Part_Sort"] = String.Join(",", bytes);
                            drt["part"] = lb_Part.Text;
                            drt["equip_item_no"] = part;
                            dtchkpart.Rows.Add(drt);

                            DataView dvsort = dtchkpart.DefaultView;
                            dvsort.Sort = "Part_Sort asc";
                            dtchkpart = dvsort.ToTable();
                            ViewState["addAllPart"] = dtchkpart;
                        }
                        else
                        {
                            DataRow drt = dtchkpart.NewRow();
                            if (dtitmno.Rows.Count > 0)
                            {
                                string[] slp = dtitmno.Rows[dtitmno.Rows.Count-1]["equip_item_no"].ToString().Split('-');
                                if (slp.Length==2)
                                {
                                    double itm = double.Parse(slp[1]);
                                    int num = Convert.ToInt32(itm)+1;
                                    part = lb_Part.Text +"-"+ num;
                                }
                            }
                            else
                            {
                                part = lb_Part.Text + "-1";
                            }
                            bytes = encode.GetBytes(part);
                            drt["Part_Sort"] = String.Join(",", bytes);
                            drt["part"] = lb_Part.Text;
                            drt["equip_item_no"] = part;
                            dtchkpart.Rows.Add(drt);

                            DataView dvsort = dtchkpart.DefaultView;
                            dvsort.Sort = "Part_Sort asc";
                            dtchkpart = dvsort.ToTable();
                            ViewState["addAllPart"] = dtchkpart;
                        }
                    }
                    else 
                    {
                        if (ViewState["addAllPartNew"].ToString() == "") ViewState["addAllPartNew"] = lb_Part.Text;
                        else ViewState["addAllPartNew"] += ","+ lb_Part.Text;
                        string[] str = ViewState["addAllPartNew"].ToString().Split(',');
                        if (str.Length > 1)
                        {
                            List<string> itm = new List<string>();
                            foreach (string strs in str)
                            {
                                itm.Add(strs);
                            }
                            int result = itm.Count(s => s.Contains(lb_Part.Text));
                            part = lb_Part.Text +"-"+ result.ToString();
                        }
                        else
                        {
                            part = lb_Part.Text + "-1";
                        }
                        bytes = encode.GetBytes(part);
                        
                    }
                    DataRow dr = dt.NewRow();
                    if (cb_explan.Checked==true)
                    {

                        DataTable dtAdd = objJobDb.getEquipBom(lb_equiCode.Text);
                        if (dtAdd.Rows.Count >0)
                        {
                            dr = dt.NewRow();
                            dr["depart_position_name"] = lb_depm.Text;
                            dr["section_position_name"] = lb_division.Text;
                            dr["equip_mas_grp"] = lb_mertGrp.Text;
                            dr["equip_code"] = lb_equiCode.Text;
                            dr["legend"] = lb_legend.Text;
                            dr["equip_item_no"] = dtAdd.Rows[0]["parts"].ToString()+"-1";//part + "-1";//part;
                            dr["equip_full_desc"] = lb_fullDesc.Text.Replace("'", "''");
                            dr["unit"] = lbUnit.Text;
                            dr["equip_add_desc"] = "";
                            dr["equip_qty"] = "";
                            dr["type"] = rdbBom.Checked == true ? "1" : "0";
                            dr["view"] = "1";
                            dr["equip_code2"] = lb_equipBom.Text;
                            dr["part"] = dtAdd.Rows[0]["parts"].ToString();
                            dr["edit"] = "2";
                            bytes = encode.GetBytes(dtAdd.Rows[0]["parts"].ToString()+"-1");
                            dr["Part_Sort"] = String.Join(",", bytes);
                            dt.Rows.Add(dr);
                            int itm = 1;
                            foreach (DataRow dtRow in dtAdd.Rows)
                            {
                                //string thispart = "";
                                //if (partex == dtRow["parts"].ToString())
                                //{
                                //    no2++;
                                //    thispart = dtRow["parts"].ToString() + "-" + no2;
                                //}
                                //else
                                //{
                                //    thispart = dtRow["parts"].ToString() + "-" + i2;
                                //    no2 = i2;
                                //}
                                //partex = dtRow["parts"].ToString();

                                dr = dt.NewRow();
                                dr["depart_position_name"] = dtRow["equip_gong"].ToString();
                                dr["section_position_name"] = dtRow["equip_pnag"].ToString();
                                dr["equip_mas_grp"] = dtRow["materialgroup"].ToString();
                                dr["equip_code"] = dtRow["equip_code"].ToString();
                                dr["legend"] = dtRow["legend"].ToString();
                                dr["equip_item_no"] = dtRow["parts"].ToString() + "-1." + itm;//part;
                                dr["equip_full_desc"] = dtRow["equip_desc_full"].ToString().Replace("'", "''");
                                dr["unit"] = dtRow["unit"].ToString();
                                dr["equip_add_desc"] = "";
                                dr["equip_qty"] = "";
                                dr["type"] = rdbBom.Checked == true ? "1" : "0";
                                dr["view"] = "1";
                                dr["equip_code2"] = dtRow["equip_code"].ToString();
                                dr["part"] = dtRow["parts"].ToString();
                                dr["edit"] = "2";
                                bytes = encode.GetBytes(dtRow["parts"].ToString() + "-1");
                                dr["Part_Sort"] = String.Join(",", bytes);
                                dt.Rows.Add(dr);
                                itm++;
                            }
                        }
                    }
                    else
                    {
                        if (lb_Part.Text != "")
                        {
                            dr = dt.NewRow();
                            dr["depart_position_name"] = lb_depm.Text;
                            dr["section_position_name"] = lb_division.Text;
                            dr["equip_mas_grp"] = lb_mertGrp.Text;
                            dr["equip_code"] = lb_equiCode.Text;
                            dr["legend"] = lb_legend.Text;
                            //if (part == lb_Part.Text)
                            //{
                            //    no++;
                            //    dr["equip_item_no"] = lb_Part.Text + "-" + no;

                            //}
                            //else
                            //{
                            //    dr["equip_item_no"] = lb_Part.Text + "-" + i;
                            //    no = i;
                            //}
                            //part = lb_Part.Text;
                            //dr["equip_item_no"] = lb_Part.Text + "-" + (grv.RowIndex + 1).ToString();
                            bytes = encode.GetBytes(part);
                            dr["equip_item_no"]= part;
                            dr["equip_full_desc"] = lb_fullDesc.Text.Replace("'", "''");
                            dr["unit"] = lbUnit.Text;
                            dr["equip_add_desc"] = "";
                            dr["equip_qty"] = "";
                            dr["type"] = rdbBomAdd.Checked == true ? "1" : "0";
                            dr["view"] = "0";
                            dr["equip_code2"] = lb_equipBom.Text;
                            dr["part"] = lb_Part.Text;
                            dr["edit"] = "2";
                            dr["Part_Sort"] = String.Join(",", bytes);
                            dt.Rows.Add(dr);

                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('ไม่สามารถ Add ได้เนื่องจากไม่ได้ระบุ Part');", true);
                            myModal.Style["display"] = "block";
                            bindgvItemAdd();

                        }
                    }
                    
                }
            }
            dvhidEq.Style["display"] = "none";
            dvPopAdd.Style["display"] = "block";
            myModal.Style["display"] = "block";
            dv_addPart.Style["display"] = "none";
            dvAddDocument.Style["display"] = "none";
            dvpopUpload.Style["display"] = "none";
            dvHistory.Style["display"] = "none";
            dv_viewLegend.Style.Add("display", "none");
            bindgvItemAdd();
            //dt.DefaultView.Sort = "equip_item_no asc";
            DataView dv = dt.DefaultView;
            dv.Sort = "Part_Sort desc";
            dt = dv.ToTable();
            AddSelEquip(dt);
            //ViewState["addAllPart"] = dt;
            DataView dv2 = dtchkpart.DefaultView;
            dv2.Sort = "Part_Sort desc";
            dtchkpart = dv.ToTable();
            ViewState["addAllPart"] = dtchkpart;
        }
        protected void AddSelEquip(DataTable dt)
        {
            DataTable dtadd = (DataTable)Session["dtAddEQ"];
            DataRow row;
            if (dtadd != null)
            {
                foreach (DataRow dtRow in dtadd.Rows)
                {
                    row = dt.NewRow();
                    row["depart_position_name"] = dtRow["depart_position_name"].ToString();
                    row["section_position_name"] = dtRow["section_position_name"].ToString();
                    row["equip_mas_grp"] = dtRow["equip_mas_grp"].ToString();
                    row["equip_code"] = dtRow["equip_code"].ToString();
                    row["legend"] = dtRow["legend"].ToString();
                    row["equip_item_no"] = dtRow["equip_item_no"].ToString();
                    row["equip_full_desc"] = dtRow["equip_full_desc"].ToString();
                    row["unit"] = dtRow["unit"].ToString();
                    row["equip_full_desc"] = dtRow["equip_full_desc"].ToString();
                    row["equip_qty"] = dtRow["equip_qty"].ToString();
                    row["type"] = dtRow["type"].ToString();
                    row["view"] = dtRow["view"].ToString();
                    row["equip_code2"] = dtRow["equip_code2"].ToString();
                    row["part"] = dtRow["part"].ToString();
                    row["edit"] = dtRow["edit"].ToString();
                    row["bid_no"] = dtRow["bid_no"].ToString();
                    row["Part_Sort"] = dtRow["Part_Sort"].ToString();
                    dt.Rows.Add(row);
                }
            }
            
            if (dt.Rows.Count > 0)
            {
                DataView dv = dt.DefaultView;
                dv.Sort = "Part_Sort asc";
                dt = dv.ToTable();
                gv_AddSelEquip.DataSource = dt;
                gv_AddSelEquip.DataBind();
                gv_AddSelEquip.Visible = true;

                Label59.Visible = true;
                cb_brakedownAdd.Visible = true;
            }
            Session.Add("dtAddEQ", dt);
        }
        protected void bt_cancViewEQ_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
            dvhidEq.Style["display"] = "block";
            dvViewBomEQ.Style["display"] = "none";
            dvPopAdd.Style["display"] = "block";
        }
        protected void bt_Price_Click(object sender, EventArgs e)
        {
            DataTable dt = objJobDb.getallEQ(hidJobNo.Value, rev.Value);
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    objJobDb.getPeiceInsertToEQ(dr["equip_code"].ToString(), hidJobNo.Value, rev.Value);
                }
                bind_equipment("");
                dvhidEq.Style["display"] = "block";
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void bt_saveTemplate_Click(object sender, EventArgs e)
        {
            if (tb_temp.Text != "")
            {
                int tempID = objJobDb.chkTempName(tb_temp.Text,hidJobNo.Value,rev.Value,hidLogin.Value,hidDept.Value,hidSect.Value);
                if (tempID != 0)
                {

                    if (hidBidNo.Value=="Bid Supply")
                    {
                        DataTable dtbid = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                        for (int j = 0; j < dtbid.Rows.Count; j++)
                        {
                            addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                            objAddpart.activity_id = hidAcID.Value;
                            objAddpart.step = "J";
                            objAddpart.job_no = tb_jobNoPart.Text;
                            if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                            objAddpart.bid_no = dtbid.Rows[j]["bid_no"].ToString();
                            if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(dtbid.Rows[j]["bid_revision"].ToString());
                            if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                            else objAddpart.schedule_no = null;
                            if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                            else objAddpart.schedule_name = "";
                            objAddpart.depart_position_name = hidDept.Value;
                            objAddpart.section_position_name = hidSect.Value;
                            objAddpart.part_name = tbAddPath.Text;
                            objAddpart.part_description = "";
                            objAddpart.created_by = hidLogin.Value;
                            objAddpart.created_datetime = DateTime.Now;
                            objAddpart.updated_by = hidLogin.Value;
                            objAddpart.updated_datetime = DateTime.Now;
                            objAddpart.process_id = hidPID.Value;
                            objJobDb.saveTemp(tempID, objAddpart, dtbid.Rows[j]["bid_no"].ToString(), dtbid.Rows[j]["bid_revision"].ToString(), hidJobNo.Value, rev.Value, hidPartTemp.Value, hidPID.Value, hidScheNo.Value, hidScheName.Value, hidDept.Value, hidSect.Value, hidLogin.Value);
                        }
                    }
                    else
                    {
                        addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                        objAddpart.activity_id = hidAcID.Value;
                        objAddpart.step = "J";
                        objAddpart.job_no = tb_jobNoPart.Text;
                        if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                        objAddpart.bid_no = hidBidNo.Value;
                        if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        else objAddpart.schedule_no = null;
                        if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                        else objAddpart.schedule_name = "";
                        objAddpart.depart_position_name = hidDept.Value;
                        objAddpart.section_position_name = hidSect.Value;
                        objAddpart.part_name = tbAddPath.Text;
                        objAddpart.part_description = "";
                        objAddpart.created_by = hidLogin.Value;
                        objAddpart.created_datetime = DateTime.Now;
                        objAddpart.updated_by = hidLogin.Value;
                        objAddpart.updated_datetime = DateTime.Now;
                        objAddpart.process_id = hidPID.Value;
                        objAddpart.part_remark = "";
                        objJobDb.saveTemp(tempID, objAddpart, hidBidNo.Value, hidBidRev.Value, hidJobNo.Value, rev.Value, hidPartTemp.Value, hidPID.Value, hidScheNo.Value, hidScheName.Value, hidDept.Value, hidSect.Value, hidLogin.Value);
                    }
                    saveTempPopup.Style["display"] = "none";
                    tb_temp.Text = "";
                    dvhidEq.Style["display"] = "block";
                    DataTable dttmp = objJobDb.loadTempDDL(hidDept.Value,hidSect.Value);
                    if (dttmp.Rows.Count > 0)
                    {
                        ddl_temp.Items.Clear();
                        ddl_temp.DataSource = dttmp;
                        ddl_temp.DataTextField = "mytemp_name";
                        ddl_temp.DataValueField = "mytemp_id";
                        ddl_temp.DataBind();
                    }
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save template successfully.');", true);
                }
                else
                {
                    int tmpID = objJobDb.getTempID(tb_temp.Text, hidJobNo.Value, rev.Value, hidLogin.Value, hidDept.Value, hidSect.Value);
                    if (hidBidNo.Value == "Bid Supply")
                    {
                        DataTable dtbid = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                        for (int j = 0; j < dtbid.Rows.Count; j++)
                        {
                            addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                            objAddpart.activity_id = hidAcID.Value;
                            objAddpart.step = "J";
                            objAddpart.job_no = tb_jobNoPart.Text;
                            if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                            objAddpart.bid_no = hidBidNo.Value;
                            if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                            if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                            else objAddpart.schedule_no = null;
                            if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                            else objAddpart.schedule_name = "";
                            objAddpart.depart_position_name = hidDept.Value;
                            objAddpart.section_position_name = hidSect.Value;
                            objAddpart.part_name = tbAddPath.Text;
                            objAddpart.part_description = "";
                            objAddpart.created_by = hidLogin.Value;
                            objAddpart.created_datetime = DateTime.Now;
                            objAddpart.updated_by = hidLogin.Value;
                            objAddpart.updated_datetime = DateTime.Now;
                            objAddpart.process_id = hidPID.Value;
                            objAddpart.part_remark = "";
                            objJobDb.updateTemp(tmpID, tb_temp.Text, objAddpart, dtbid.Rows[j]["bid_no"].ToString(), dtbid.Rows[j]["bid_revision"].ToString(), hidJobNo.Value, rev.Value, hidPartTemp.Value, hidPID.Value, hidScheNo.Value, hidScheName.Value, hidDept.Value, hidSect.Value, hidLogin.Value);
                        }
                    }
                    else
                    {
                        addMytaskEquipJob objAddpart = new addMytaskEquipJob();
                        objAddpart.activity_id = hidAcID.Value;
                        objAddpart.step = "J";
                        objAddpart.job_no = tb_jobNoPart.Text;
                        if (tb_revPart.Text != "") objAddpart.job_revision = Convert.ToInt32(tb_revPart.Text);
                        objAddpart.bid_no = hidBidNo.Value;
                        if (hidBidRev.Value != "") objAddpart.bid_revision = Convert.ToInt32(hidBidRev.Value);
                        if (hidScheNo.Value != "") objAddpart.schedule_no = Convert.ToInt32(hidScheNo.Value);
                        else objAddpart.schedule_no = null;
                        if (hidScheName.Value != "") objAddpart.schedule_name = hidScheName.Value;
                        else objAddpart.schedule_name = "";
                        objAddpart.depart_position_name = hidDept.Value;
                        objAddpart.section_position_name = hidSect.Value;
                        objAddpart.part_name = tbAddPath.Text;
                        objAddpart.part_description = "";
                        objAddpart.created_by = hidLogin.Value;
                        objAddpart.created_datetime = DateTime.Now;
                        objAddpart.updated_by = hidLogin.Value;
                        objAddpart.updated_datetime = DateTime.Now;
                        objAddpart.process_id = hidPID.Value;
                        objAddpart.part_remark = "";
                        objJobDb.updateTemp(tmpID, tb_temp.Text, objAddpart, hidBidNo.Value, hidBidRev.Value, hidJobNo.Value, rev.Value, hidPartTemp.Value, hidPID.Value, hidScheNo.Value, hidScheName.Value, hidDept.Value, hidSect.Value, hidLogin.Value);
                    }
                    //p_alert.InnerText = "Template name. ซ้ำ";
                    //p_alert.Style["display"] = "block";
                    //saveTempPopup.Style["display"] = "block";
                    saveTempPopup.Style["display"] = "none";
                    tb_temp.Text = "";
                    dvhidEq.Style["display"] = "block";
                }
            }
            else
            {
                p_alert.InnerText = "โปรดระบุ Template name.";
                p_alert.Style["display"] = "block";
                saveTempPopup.Style["display"] = "block";
            }
        }
        protected void bt_loadTemp_Click(object sender, EventArgs e)
        {
            if (hidBidNo.Value=="Bid Supply")
            {
                DataTable dtbid = objJobDb.getTabBid_sup(txtJobNo.Text, txtJobRev.Text);
                for (int j = 0; j < dtbid.Rows.Count; j++)
                {
                    objJobDb.loadTemp(dtbid.Rows[j]["bid_no"].ToString(), hidJobNo.Value, hidPartTemp.Value, ddl_temp.SelectedValue,hidDept.Value);
                }
            }
            else
            {
                objJobDb.loadTemp(hidBidNo.Value, hidJobNo.Value, hidPartTemp.Value, ddl_temp.SelectedValue, hidDept.Value);
            }
            
            bind_equipment("");
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Load template successfully.');", true);
        }
        protected void SectionAssign()
        {
            DataTable dt = objJobDb.chkSectionWork(hidLogin.Value, hidJobNo.Value, rev.Value, hidAcID.Value);//hidAcID.Value dvequihidden
            //if (dt.Rows.Count > 0)
            //{
            //    if (hidLogin.Value == dt.Rows[0]["assignee_drawing"].ToString()) dvDrawshow.Style["display"] = "block";

            //    if (hidLogin.Value == dt.Rows[0]["assignee_equip"].ToString()) dvOpen.Style["display"] = "block";
            //    else dvhidEq.Style["display"] = "none";

            //    if (hidLogin.Value == dt.Rows[0]["assignee_tech_doc"].ToString()) dvOpenDoc.Style["display"] = "block";

            //}
            //else
            //{
            //    dvOpen.Style["display"] = "none";
            //    dvhidEq.Style["display"] = "none";
            //}
            if (dt.Rows.Count > 0)
            {
                if (hidLogin.Value == dt.Rows[0]["assignee_drawing"].ToString()) pnl_dvhiddraw.Enabled = true;
                else pnl_dvhiddraw.Enabled = false;

                if (hidLogin.Value == dt.Rows[0]["assignee_equip"].ToString()) 
                { 
                     hidchkassign.Value = "assign";
                    bt_Configure.Enabled = true;
                    bt_AddPart.Enabled = true;
                    bt_AddEquip.Enabled = true;
                    bt_EditEquip.Enabled = true;
                    bt_Save.Enabled = true;
                    tb_remarkprt.Enabled = true;
                    tb_descprt.Enabled = true;
                    tb_import.Enabled = true;
                    bt_preview.Enabled = true;
                    bt_Price.Enabled = true;
                    Button1.Enabled = true;
                    bt_saveTemp.Enabled = true;
                }
                else {
                    hidchkassign.Value = "";
                    bt_Configure.Enabled = false;
                    bt_AddPart.Enabled = false;
                    bt_AddEquip.Enabled = false;
                    bt_EditEquip.Enabled = false;
                    bt_Save.Enabled = false;
                    tb_remarkprt.Enabled = false;
                    tb_descprt.Enabled = false;
                    tb_import.Enabled = false;
                    bt_Price.Enabled = false;
                    Button1.Enabled = false;
                    bt_saveTemp.Enabled = false;
                }

                if (hidLogin.Value == dt.Rows[0]["assignee_tech_doc"].ToString()) pnl_dvhidDoc.Enabled = true;
                else pnl_dvhidDoc.Enabled = false;

            }
            else
            {
                pnl_dvhiddraw.Enabled = false;
                hidchkassign.Value = "";
                pnl_dvhidDoc.Enabled = false;
                bt_Configure.Enabled = false;
                bt_AddPart.Enabled = false;
                bt_AddEquip.Enabled = false;
                bt_EditEquip.Enabled = false;
                bt_Save.Enabled = false;
                tb_remarkprt.Enabled = false;
                tb_descprt.Enabled = false;
                tb_import.Enabled = false;
                bt_Price.Enabled = false;
                Button1.Enabled = false;
                bt_saveTemp.Enabled = false;
            }
        }
    }
}