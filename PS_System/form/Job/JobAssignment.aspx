﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="JobAssignment.aspx.cs" Inherits="DemoWeb.form.Job.JobAssignment" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Job Assignment</title>

    <link href="../../Content/w3.css" rel="stylesheet" />
    
    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 100%; text-align: left;" colspan="2"><b>Job Assignment</b></td>
                </tr>
                <tr>
                    <td style="width: 50%; text-align: left;"><b>Job No. : Job-0001</b></td>
                    <td style="width: 50%; text-align: Right;"><a href='JobDashboard.aspx'>View job Dashboard</a></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">
                        <asp:GridView ID="gvDrawing" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvDrawing_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <%--<asp:CheckBoxField DataField="isCheck" id="chkbox" />--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:CheckBox ID="chkSelect" runat="server" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Drawing Design No" DataField="designNo" />
                                <asp:BoundField HeaderText="Status" DataField="status">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:BoundField>

                            </Columns>
                        </asp:GridView>
                    </td>

                </tr>
                <tr>
                    <td>
                        <button onclick="document.getElementById('id01').style.display='block';return false;" class="w3-button w3-gray">Upload Drawing</button>
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%; text-align: left;" colspan="2"><b>Bid / Schedule : xxxxxx </b></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">
                        <asp:GridView ID="gvEquipment" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvEquipment_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:BoundField HeaderText="Equipment Name" DataField="equipmentName" />
                                <asp:BoundField HeaderText="Equipment Type" DataField="equipmentType" />
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Button ID="Button1" runat="server" class="w3-button w3-border-blue-grey" Text="View doc" OnClientClick="document.getElementById('equipment').style.display='block';return false;" />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField HeaderText="Status" DataField="status">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <%--<td>
                        <button onclick="document.getElementById('equipment').style.display='block';return false;" class="w3-button w3-gray">Equipment</button>
                    </td>--%>
                </tr>
                <tr>
                    <td colspan="2" style="width: 50%; text-align: left;"><b>Upload Technical Document</b></td>
                </tr>
                <tr>
                    <td colspan="2" style="padding-bottom: 15px; width: 90%; align-content: space-around">
                        <asp:GridView ID="gvUpLoadDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                            AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" OnRowDataBound="gvUpLoadDoc_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <SortedAscendingCellStyle BackColor="#F1F1F1" />
                            <SortedAscendingHeaderStyle BackColor="#007DBB" />
                            <SortedDescendingCellStyle BackColor="#CAC9C9" />
                            <SortedDescendingHeaderStyle BackColor="#00547E" />
                            <Columns>
                                <asp:BoundField HeaderText="Document type" DataField="docType" />
                                <asp:BoundField HeaderText="Document name" DataField="docName" />
                                <asp:BoundField HeaderText="Bid" DataField="bid" />
                                <asp:BoundField HeaderText="Schedule" DataField="schedule" />
                                <asp:BoundField HeaderText="Status" DataField="status">
                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td>
                        <button onclick="document.getElementById('addDoc').style.display='block';return false;" class="w3-button w3-grey">Upload Drawing</button>
                    </td>
                </tr>
            </table>
        </div>
        <div id="id01" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('id01').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <table>
                        <tr>
                            <td colspan="2">Upload Drawing
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Design :</b>
                            </td>
                            <td style="width: 40%">S0
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Ref Job :</b>
                            </td>
                            <td style="width: 40%">J-xxx
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Ref Bid :</b>
                            </td>
                            <td style="width: 40%">B-xxx
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Drawing Name :</b>
                            </td>
                            <td style="width: 40%">xxxxxxx
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="file" id="myFile" multiple size="50">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onclick="document.getElementById('id01').style.display='none';return false;" class="w3-button w3-green">Save</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="addDoc" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('addDoc').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <table>
                        <tr>
                            <td colspan="2">Upload Document type
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Doctype :</b>
                            </td>
                            <td style="width: 40%">SO
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Ref Job :</b>
                            </td>
                            <td style="width: 40%">J-xxx
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Ref Bid :</b>
                            </td>
                            <td style="width: 40%">B-xxx
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 40%">
                                <b>Document Name :</b>
                            </td>
                            <td style="width: 40%">doc-01
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <input type="file" id="myFile2" multiple size="50">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <button onclick="document.getElementById('addDoc').style.display='none';return false;" class="w3-button w3-green">Save</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div id="equipment" class="w3-modal">
            <div class="w3-modal-content">
                <div class="w3-container">
                    <span onclick="document.getElementById('equipment').style.display='none'" class="w3-button w3-display-topright">&times;</span>
                    <table>
                        <tr>
                            <td style="text-align: left">Equipment Master Data</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvEquipMaster" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Equipment Name" DataField="equipmentName" />
                                        <asp:BoundField HeaderText="Equipment Type" DataField="equipmentType" />
                                        <asp:BoundField HeaderText="Equipment Detail" DataField="equipmentDetail" />
                                        <asp:BoundField HeaderText="Expire Date" DataField="expDate" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left">Equipment Master Data</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="gvDocList" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <Columns>
                                        <asp:BoundField HeaderText="Document Type" DataField="docType" />
                                        <asp:BoundField HeaderText="Document Name" DataField="docName" />
                                        <asp:BoundField HeaderText="Document Detail" DataField="docDetail" />
                                    </Columns>
                                </asp:GridView>
                            </td>
                        </tr>

                    </table>
                </div>
            </div>
        </div>
    </form>
</body>
</html>
