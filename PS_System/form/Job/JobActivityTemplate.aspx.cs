﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using static PS_Library.clsActivityTemplate;
 
namespace PS_System.form.Master
{
    public partial class JobActivityTemplate : System.Web.UI.Page
    {
        clsActivityTemplate objAct = new clsActivityTemplate();
        EmpInfoClass empInfo = new EmpInfoClass();

        private string strEmpIdLogin = string.Empty;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["zuserlogin"] != null)
            {
                strEmpIdLogin = Request.QueryString["zuserlogin"];

            }
            else
            {
                strEmpIdLogin = "z593503";// for test only
            }
            BindingPosition(strEmpIdLogin);

            if (!IsPostBack)
            {
                BindingDdlTemplate();
                BindingDdlEmailTemplate();
                BindingDdlAssignee();
                BindingNextActivity();
                BindingDdlDeptSection();
            }
        }

        #region get data
        private DataTable GetTemplateListById()
        {
            DataTable dt = new DataTable();

            dt = objAct.GetJobTemplate();

            return dt;
        }
        private DataTable GetJobActivityByTemplateId()
        {
            DataTable dt = new DataTable();
            dt = objAct.GetJobActivityByTemplate(ddlTemplateList.SelectedValue, tbxActivityNameSearch.Text.Trim());

            return dt;
        }
        #endregion

        #region binding control event

        private void BindingDdlDeptSection()
        {
            string strDept = hdnDepartment.Value;
            DataTable dt = objAct.GetCommonOrgByDeptShort(strDept);
            DataView view = new DataView(dt);
            DataTable distinctSection = view.ToTable(true, "section");
            DataTable distinctDepartment = view.ToTable(true, "department");
            ddlSectionApp.Items.Clear();
            ddlDepartmentApp.Items.Clear();
            ddlSectionApp.Items.Add(new ListItem("<-- Select -->", string.Empty));
            ddlDepartmentApp.Items.Add(new ListItem("<-- Select -->", string.Empty));
            foreach (DataRow dr in distinctDepartment.Rows)
            {
                if (!String.IsNullOrEmpty(dr["department"].ToString()))
                    ddlDepartmentApp.Items.Add(new ListItem(dr["department"].ToString()));
            }
            foreach (DataRow dr in distinctSection.Rows)
            {
                if (!String.IsNullOrEmpty(dr["section"].ToString()))
                    ddlSectionApp.Items.Add(new ListItem(dr["section"].ToString()));
            }
        }
        private void BindingDdlAssignee()
        {
            EmpInfoClass empInfo = new EmpInfoClass();
            //Employee emp = empInfo.getInFoByEmpID(this.hidLogin.Value);//this.hidLogin.Value

            string[] staffList = empInfo.getEmpIDAndNameInDepartment(hdnDepartment.Value);

            ddlDocAssignee.Items.Clear();
            ddlDrawingAssignee.Items.Clear();
            ddlEquipAssignee.Items.Clear();
            ddlDocAssignee.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlDrawingAssignee.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlEquipAssignee.Items.Insert(0, new ListItem("<-- Select -->", ""));

            foreach (string staffInfo in staffList)
            {
                string[] staff = staffInfo.Split(':');

                ListItem item = new ListItem();

                item.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")";
                //item.Text = staff[1].ToString() + " (" + staff[2].ToString() + ")(" + staff[0].ToString() + ")";
                item.Value = staff[0].ToString();
                ddlDocAssignee.Items.Add(item);
                ddlDrawingAssignee.Items.Add(item);
                ddlEquipAssignee.Items.Add(item);
            }
        }
        private void BindingDdlTemplate()
        {
            ddlTemplateList.Items.Clear();
            DataTable dtTemplateList = new DataTable();
            dtTemplateList = GetTemplateListById();
            ddlTemplateList.Items.Add(new ListItem("Please select template name", ""));

            foreach (DataRow dr in dtTemplateList.Rows)
            {
                ddlTemplateList.Items.Add(new ListItem(dr["temp_name"].ToString(), dr["temp_name"].ToString()));
            }
        }
        private void BindingDdlEmailTemplate()
        {
            ddlEmailTemplate.Items.Clear();
            ddlEmailTemplate.Items.Add(new ListItem("<-- Select -->", ""));
            DataTable dtEmailTemplateList = objAct.GetEmailTemplateList();
            if (dtEmailTemplateList.Rows.Count > 0)
            {
                foreach (DataRow dr in dtEmailTemplateList.Rows)
                {
                    ddlEmailTemplate.Items.Add(new ListItem(dr["email_noti_name"].ToString(), dr["email_noti_id"].ToString()));
                }
                BindingEmailContent();
            }

        }
        private void BindingEmailContent()
        {
            tbxEmailContent.Text = objAct.GetEmailTemplateContentById(ddlEmailTemplate.SelectedValue);
        }
        private void BindingGvTemplateList()
        {
            DataTable dtTemplateList = GetJobActivityByTemplateId();
            gvTemplateList.DataSource = dtTemplateList;
            gvTemplateList.DataBind();

        }
        private void BindingNextActivity()
        {
            DataTable dtActivity = objAct.GetJobNextActivityList();
            lstNextActivity.DataSource = dtActivity;
            lstNextActivity.DataValueField = "CODE";
            lstNextActivity.DataTextField = "NAME";
            lstNextActivity.DataBind();
        }
        private void BindingPosition(string strempid)
        {
            Employee emp = new Employee();
            emp = empInfo.getInFoByEmpID(strempid);

            hdnDepartment.Value = emp.ORGSNAME4;
            hdnSection.Value = emp.ORGSNAME5;
        }

        #endregion

        #region controls event
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGvTemplateList();
        }
        protected void btnSaveAct_Click(object sender, EventArgs e)
        {
            JobActivity objJobActivity = new JobActivity();
            objJobActivity.temp_name = tbxTemplateName.Text;
            objJobActivity.activity_id = tbxActivityId.Text;
            objJobActivity.activity_name = tbxActivityName.Text;
            objJobActivity.assignee_drawing = ddlDrawingAssignee.SelectedValue;//tbxDrawingAssignee.Text;
            objJobActivity.assignee_equip = ddlEquipAssignee.SelectedValue;//tbxEquipAssignee.Text;
            objJobActivity.assignee_tech_doc = ddlDocAssignee.SelectedValue;//tbxDocAssignee.Text;
            objJobActivity.created_by = strEmpIdLogin; // need more logic
            objJobActivity.created_datetime = DateTime.Now;
            if (!String.IsNullOrEmpty(ddlEmailTemplate.SelectedValue))
                objJobActivity.email_noti_id = int.Parse(ddlEmailTemplate.SelectedValue);
            if (!String.IsNullOrEmpty(tbxWeightFactor.Text))
                objJobActivity.weight_factor = float.Parse(tbxWeightFactor.Text);
            try
            {
                if (ddlSectionApp.SelectedValue != string.Empty) objJobActivity.approval_routing += "[" + ddlSectionApp.SelectedValue + "]";
                if (ddlDepartmentApp.SelectedValue != string.Empty) objJobActivity.approval_routing += "[" + ddlDepartmentApp.SelectedValue + "]";
            }
            catch { }
            string strNextAct = string.Empty;
            foreach (ListItem item in lstNextActivity.Items)
            {
                if (item.Selected) strNextAct += strNextAct == string.Empty ? item.Value : "," + item.Value;
            }
            objJobActivity.next_activity_id = strNextAct;


            objJobActivity.req_equip = cbxReqEquip.Checked ? true : false;
            objJobActivity.req_drawing = cbxReqDrawing.Checked ? true : false;
            objJobActivity.req_tech_doc = cbxReqDoc.Checked ? true : false;
            objJobActivity.updated_by = strEmpIdLogin; // need more logic
            objJobActivity.updated_datetime = DateTime.Now;

            string strErrorMsg = IsValidate();
            if (strErrorMsg == string.Empty)
            {
                clsActivityTemplate cls = new clsActivityTemplate();
                if (hdnLastMode.Value == "C")
                {
                    cls.InsertJobActivity(objJobActivity);
                }
                else
                {
                    cls.UpdateJobActivity(objJobActivity);
                }
                BindingGvTemplateList();
                BindingDdlAssignee();
                ResetControl(Page);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp",
                        "alert('" + strErrorMsg + "');", true);
            }
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            BindingDdlAssignee();
            ResetControl(Page);
        }
        protected void ddlEmailTemplate_SelectedIndexChanged(object sender, EventArgs e)
        {
            //BindingEmailContent();
        }
        protected void editModal_Click(object sender, EventArgs e)
        {
            //list2.Items.Clear();
            ImageButton ImageButton = (ImageButton)sender;
            GridViewRow row = (GridViewRow)ImageButton.NamingContainer;
            Label gvLblTemplateName = (Label)row.FindControl("gvLblTemplateName");
            tbxTemplateName.Text = gvLblTemplateName.Text;
            Label gvLblActivityId = (Label)row.FindControl("gvLblActivityId");
            tbxActivityId.Text = gvLblActivityId.Text;
            Label gvLblActivityName = (Label)row.FindControl("gvLblActivityName");
            tbxActivityName.Text = gvLblActivityName.Text;
            CheckBox gvCbxReqDrawing = (CheckBox)row.FindControl("gvCbxReqDrawing");
            cbxReqDrawing.Checked = gvCbxReqDrawing.Checked;
            HiddenField gvHdnDrawingAssignee = (HiddenField)row.FindControl("gvHdnDrawingAssignee");
            if (!String.IsNullOrEmpty(gvHdnDrawingAssignee.Value))
                try
                {
                    ddlDrawingAssignee.SelectedValue = gvHdnDrawingAssignee.Value;
                }
                catch { }
            CheckBox gvCbxReqEquip = (CheckBox)row.FindControl("gvCbxReqEquip");
            cbxReqEquip.Checked = gvCbxReqEquip.Checked;
            HiddenField gvHdnEquipAssignee = (HiddenField)row.FindControl("gvHdnEquipAssignee");
            if (!String.IsNullOrEmpty(gvHdnEquipAssignee.Value))
                try
                {
                    ddlEquipAssignee.SelectedValue = gvHdnEquipAssignee.Value;
                }
                catch { }
            CheckBox gvCbxReqDoc = (CheckBox)row.FindControl("gvCbxReqDoc");
            cbxReqDoc.Checked = gvCbxReqDoc.Checked;
            HiddenField gvHdnDocAssignee = (HiddenField)row.FindControl("gvHdnDocAssignee");
            if (!String.IsNullOrEmpty(gvHdnDocAssignee.Value))
                try
                {
                    ddlDocAssignee.SelectedValue = gvHdnDocAssignee.Value;
                }
                catch { }
            BindingNextActivity();
            HiddenField gvHdnNextActivity = (HiddenField)row.FindControl("gvHdnNextActivity");
            if (!String.IsNullOrEmpty(gvHdnNextActivity.Value))
            {
                string[] splitNextAct = gvHdnNextActivity.Value.Split(',');
                foreach (string strNextAct in splitNextAct)
                {
                    try
                    {
                        lstNextActivity.Items.FindByValue(strNextAct.Trim()).Selected = true;
                    }
                    catch { }
                }
            }
            //tbxNextActivity.Text = gvHdnNextActivity.Value;
            HiddenField gvHdnEmailNotiId = (HiddenField)row.FindControl("gvHdnEmailNotiId");
            if (!String.IsNullOrEmpty(gvHdnEmailNotiId.Value))
            {
                try
                {
                    ddlEmailTemplate.SelectedValue = gvHdnEmailNotiId.Value;
                }
                catch { }
            }
            BindingEmailContent();

            Label gvLblWeightFactor = (Label)row.FindControl("gvLblWeightFactor");
            tbxWeightFactor.Text = gvLblWeightFactor.Text;

            BindingDdlDeptSection();

            Label gvLblAppRoute = (Label)row.FindControl("gvLblAppRoute");
            if (!String.IsNullOrEmpty(gvLblAppRoute.Text))
            {
                string[] arrAppover = GetRoutingApproval(gvLblAppRoute.Text);
                if (arrAppover.Length > 0)
                {
                    try
                    {
                        ddlSectionApp.SelectedValue = arrAppover[0];
                        ddlDepartmentApp.SelectedValue = arrAppover[1];
                    }
                    catch { }
                }
            }



            ScriptManager.RegisterStartupScript(this, GetType(), "displayalertmessage1", "EditClick()", true);//show the modal
        }
        protected void gvTemplateList_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvTemplateList.PageIndex = e.NewPageIndex;
            BindingGvTemplateList();
        }
        protected void gvTemplateList_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label gvLblNextActivity = (Label)e.Row.FindControl("gvLblNextActivity");
                string strNextAct = gvLblNextActivity.Text;
                string strResult = string.Empty;
                DataTable dtTemp;
                if (!String.IsNullOrEmpty(strNextAct))
                {
                    string[] strSplitAct = strNextAct.Split(',');
                    foreach (string strAct in strSplitAct)
                    {
                        dtTemp = objAct.GetJobActivityByActivityId(strAct);
                        if (dtTemp.Rows.Count > 0)
                        {
                            strResult += strResult == string.Empty ? strAct + ":" + dtTemp.Rows[0]["activity_name"].ToString() + "(" + dtTemp.Rows[0]["temp_name"].ToString() + ")" : "</br>" + strAct + ":" + dtTemp.Rows[0]["activity_name"].ToString() + "(" + dtTemp.Rows[0]["temp_name"].ToString() + ")";
                        }
                        else
                        {
                            strResult += strResult == string.Empty ? strAct : "</br>" + strAct;
                        }
                    }
                    gvLblNextActivity.Text = strResult;
                }
                HiddenField gvHdnDrawingAssignee = (HiddenField)e.Row.FindControl("gvHdnDrawingAssignee");
                if (!String.IsNullOrEmpty(gvHdnDrawingAssignee.Value) && gvHdnDrawingAssignee.Value != "null")
                {

                    Employee emp = new Employee();
                    emp = empInfo.getInFoByEmpID(gvHdnDrawingAssignee.Value);
                    Label gvLblDrawingAssignee = (Label)e.Row.FindControl("gvLblDrawingAssignee");
                    gvLblDrawingAssignee.Text = emp.SNAME;
                }

                HiddenField gvHdnEquipAssignee = (HiddenField)e.Row.FindControl("gvHdnEquipAssignee");
                if (!String.IsNullOrEmpty(gvHdnEquipAssignee.Value) && gvHdnEquipAssignee.Value != "null")
                {

                    Employee emp1 = new Employee();
                    emp1 = empInfo.getInFoByEmpID(gvHdnEquipAssignee.Value);
                    Label gvLblEquipAssignee = (Label)e.Row.FindControl("gvLblEquipAssignee");
                    gvLblEquipAssignee.Text = emp1.SNAME;
                }

                HiddenField gvHdnDocAssignee = (HiddenField)e.Row.FindControl("gvHdnDocAssignee");
                if (!String.IsNullOrEmpty(gvHdnDocAssignee.Value) && gvHdnDocAssignee.Value != "null")
                {

                    Employee emp2 = new Employee();
                    emp2 = empInfo.getInFoByEmpID(gvHdnDocAssignee.Value);
                    Label gvLblDocAssignee = (Label)e.Row.FindControl("gvLblDocAssignee");
                    gvLblDocAssignee.Text = emp2.SNAME;
                }

            }
        }
        protected void gvTemplateList_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "del")
            {
                GridViewRow gvTemplateList = (GridViewRow)((ImageButton)e.CommandSource).NamingContainer;
                Label gvLblActivityId = (Label)gvTemplateList.FindControl("gvLblActivityId");
                objAct.gvJobListCmdDelete(gvLblActivityId.Text);

                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp",
                "alert('Deleted Successfully');", true);
                BindingGvTemplateList();
            }
        }
        #endregion

        #region util method
        private string[] GetRoutingApproval(string strRouteApproval)
        {
            return strRouteApproval.Split(new char[] { ']', '[' }, StringSplitOptions.RemoveEmptyEntries);
        }
        private string IsValidate()
        {

            string strError = string.Empty;
            if (tbxActivityId.Text.Trim() == string.Empty)
            {
                strError = "Please input activity ID.";
            }
            else
            {
                if (hdnLastMode.Value == "C")
                {
                    if (objAct.GetJobActivityByActivityId(tbxActivityId.Text.Trim()).Rows.Count > 0)
                    {
                        strError = tbxActivityId.Text.Trim() + " is already exist.";
                    }
                }
            }

            if (tbxTemplateName.Text.Trim() == string.Empty)
            {
                strError = "Please input Template name.";
            }

            if (ddlSectionApp.SelectedValue == string.Empty)
            {
                if (ddlDepartmentApp.SelectedValue != string.Empty)
                {
                    strError = "Please select section approver.";
                }
            }


            return strError;
        }
        public void ResetControl(Page mypage)
        {
            Control myForm = mypage.FindControl("Form1");
            foreach (Control ctrl in myForm.Controls)
            {
                //Clears TextBox
                if (ctrl is System.Web.UI.WebControls.TextBox)
                {
                    (ctrl as TextBox).Text = string.Empty;
                }
                //Clears DropDown Selection
                if (ctrl is System.Web.UI.WebControls.DropDownList)
                {
                    (ctrl as DropDownList).ClearSelection();
                }
                //Clears ListBox Selection
                if (ctrl is System.Web.UI.WebControls.ListBox)
                {
                    (ctrl as ListBox).ClearSelection();
                }
                //Clears CheckBox Selection
                if (ctrl is System.Web.UI.WebControls.CheckBox)
                {
                    (ctrl as CheckBox).Checked = false;
                }
                if (ctrl is System.Web.UI.WebControls.CheckBoxList)
                {
                    (ctrl as CheckBoxList).ClearSelection();
                }

                BindingNextActivity();
                tbxTemplateName.Text = string.Empty;
            }
        }
        #endregion

        #region static method
        [WebMethod]
        public static List<string> GetTemplateNames(string prefixText)
        {
            List<string> lstTemplateName = new List<string>();
            DataTable dt = GetJobTemplateByPrefix(prefixText);
            foreach (DataRow dr in dt.Rows)
            {
                lstTemplateName.Add(dr["temp_name"].ToString());
            }
            return lstTemplateName;
        }
        [WebMethod]
        public static string GetEmailContent(string strEmailTemplateId)
        {
            string strEmailContent = string.Empty;
            try
            {
                strEmailContent = GetEmailTemplateContentByIdSta(strEmailTemplateId);
            }
            catch { }
            return strEmailContent;
        }
        #endregion

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}