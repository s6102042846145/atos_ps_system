﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="frmTC01.aspx.cs" Inherits="PS_System.form.frmTC01"  MaintainScrollPositionOnPostback="true" %> 

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
        <style type="text/css">
        .auto-style1 {
            width: 100%;
            vertical-align: top;
        }

        .auto-style9 {
            width: 100%;
            height: 394px;
        }

        .auto-style11 {
            width: 100%;
            height: 277px;
        }

        html, body {
            padding: 0;
            margin: 0;
            width: 100%;
            height: 100%;
        }

        #overlay {
            position: absolute;
            top: 0px;
            left: 0px;
            background: #ccc;
            width: 100%;
            height: 100%;
            opacity: .75;
            filter: alpha(opacity=75);
            opacity: .75;
            z-index: 999;
        }

        .main-contain {
            position: absolute;
            top: 0px;
            left: 0px;
            width: 100%;
            height: 100%;
            overflow: hidden;
        }

        th.headRowSortAsc a {
            display: inline-block;
            background: url("../Images/down-sort.png") no-repeat;
            background-size: 20px;
            background-position-x: right;
            width: 140px;
            text-decoration: none;
            vertical-align: central;
        }

        th.headRowSortDesc a {
            display: inline-block;
            background: url("../Images/up-sort.png") no-repeat;
            background-size: 20px;
            background-position-x: right;
            width: 140px;
            text-decoration: none;
            vertical-align: central;
        }

        .auto-style26 {
            height: 50px;
        }

        .auto-style28 {
            width: 100%;
        }

        .auto-style30 {
            width: 50%;
            height: 60px;
        }

        .auto-style32 {
            height: 28px;
        }

        .auto-style33 {
            width: 50%;
            height: 60px;
        }

        .auto-style34 {
            width: 50%;
            height: 60px;
        }

        .auto-style35 {
            width: 300px;
        }

        .auto-style36 {
            width: 50%;
            height: 60px;
        }

        .cover-style {
            width: 100%;
            height: 200%;
        }

        .cls_view_doc {
            color: #337ab7;
            cursor: pointer;
            text-decoration: none;
        }

            .cls_view_doc:hover {
                text-decoration: underline;
            }
    </style>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap-multiselect.js"></script>

    <script src="../Scripts/aes.js"></script>

    <script type="text/javascript">
        var urlParams;
        (window.onpopstate = function () {
            var match,
                pl = /\+/g,  // Regex for replacing addition symbol with a space
                search = /([^&=]+)=?([^&]*)/g,
                decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
                query = window.location.search.substring(1);

            urlParams = {};
            while (match = search.exec(query))
                urlParams[decode(match[1])] = decode(match[2]);
        })();

        function loadSessionData() {
            //var tk = urlParams["tk"];
            //if (tk != "") {
            //    var atos = document.getElementById("hidATOS").value;
            //    var atosBytes = aesjs.utils.utf8.toBytes(atos);

            //    var iv = [21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36];
            //    var encryptedBytes = aesjs.utils.hex.toBytes(tk);

            //    var aesCbc = new aesjs.ModeOfOperation.cbc(atosBytes, iv);
            //    var decryptedBytes = aesCbc.decrypt(encryptedBytes);

            //    var decryptedText = aesjs.utils.utf8.fromBytes(decryptedBytes);

            //    document.getElementById("hidLogin").value = decryptedText.replace(/,/g, '');
            //    document.getElementById("hidIsFirstRun").value = "t";
            //    $('#btnSearch').trigger('click');
            //}

            var lo = urlParams["zUserLogin"];
            if (lo != "") {
                document.getElementById("hidLogin").value = lo;
                document.getElementById("hidIsFirstRun").value = "t";
                $('#btnSearch').trigger('click');
            }
        }

        $(function () {
            $("#overlay").fadeOut();
            $(".main-contain").removeClass("main-contain");

            $('[id*=lbTaskCodeBid]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=lbTaskCodeJob]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=lbtasknamebid]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500,
                numberDisplayed: 1
            });

            $('[id*=lbassignbid]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500
            });
            $('[id*=lbstatusbid]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500
            });
            $('[id*=lbtasknamejob]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500
            });

            $('[id*=lbassignjob]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500
            });
            $('[id*=lbstatusjob]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                maxHeight: 500
            });

            $('[id*=ddlBidAssignTo]').multiselect({
                enableFiltering: true,
                numberDisplayed: 1,
                maxHeight: 500
            });
            $('[id*=ddlJobAssignTo]').multiselect({
                enableFiltering: true,
                numberDisplayed: 1,
                maxHeight: 500
            });
            

        });
        
        $(document).keypress(
            function (event) {
                if (event.which == '13') {
                    event.preventDefault();
                }
            });

        function checkDisableStaff(objCHK, nameCHK, type) { //, nameddlStaff

            //var ddlStaff = objCHK.id.replace(nameCHK, nameddlStaff);
            if (objCHK.checked == true) {
                //document.getElementById(ddlStaff).disabled = false;

                if (type == "bid") {
      
                    var gv1Edit = objCHK.id.replace(nameCHK, "gv1Edit");
                    document.getElementById(gv1Edit).style.display = "block";

                    var gv1hidTaskID = objCHK.id.replace(nameCHK, "gv1hidTaskID");
                    if (document.getElementById("hidCheckedBid").value == "") {
                        document.getElementById("hidCheckedBid").value = document.getElementById(gv1hidTaskID).value;
                    } else {
                        document.getElementById("hidCheckedBid").value += "," + document.getElementById(gv1hidTaskID).value;
                    }
                }
                else if (type == "job") {
                    var gv2Edit = objCHK.id.replace(nameCHK, "gv2Edit");
                    document.getElementById(gv2Edit).style.display = "block";

                    var gv2hidTaskID = objCHK.id.replace(nameCHK, "gv2hidTaskID");
                    if (document.getElementById("hidCheckedJob").value == "") {
                        document.getElementById("hidCheckedJob").value = document.getElementById(gv2hidTaskID).value;
                    } else {
                        document.getElementById("hidCheckedJob").value += "," + document.getElementById(gv2hidTaskID).value;
                    }
                }
            }
            else {
                //document.getElementById(ddlStaff).disabled = true;

                if (type == "bid") {
                    var gv1Edit = objCHK.id.replace(nameCHK, "gv1Edit");
                    document.getElementById(gv1Edit).style.display = "none";

                    var gv1hidTaskID = objCHK.id.replace(nameCHK, "gv1hidTaskID");
                    document.getElementById("hidCheckedBid").value = document.getElementById("hidCheckedBid").value.replace(document.getElementById(gv1hidTaskID).value, "");
                    document.getElementById("hidCheckedBid").value = document.getElementById("hidCheckedBid").value.replace(",,", "");
                }
                else if (type == "job") {
                    var gv2Edit = objCHK.id.replace(nameCHK, "gv2Edit");
                    document.getElementById(gv2Edit).style.display = "none";

                    var gv2hidTaskID = objCHK.id.replace(nameCHK, "gv2hidTaskID");
                    document.getElementById("hidCheckedJob").value = document.getElementById("hidCheckedJob").value.replace(document.getElementById(gv2hidTaskID).value, "");
                    document.getElementById("hidCheckedJob").value = document.getElementById("hidCheckedJob").value.replace(",,", "");
                }
            }
        }
        function ConfirmSave() {
            //var confirm_value = document.createElement("INPUT");
            //confirm_value.type = "hidden";
            //confirm_value.name = "confirm_value";
            if (confirm("Do you want to save data?")) {
                return true;
            } else {
                return false;
            }
            //document.forms[0].appendChild(confirm_value);
        }
        function ClosePanelpj() {
            document.getElementById("tdproject").style.display = "none";
        }

        function DisableButton() {
            document.getElementById("btnAssignJob").disabled = true;
            document.getElementById("btnAssignBid").disabled = true;
        }

        function EnableButton() {
            document.getElementById("btnAssignJob").disabled = false;
            document.getElementById("btnAssignBid").disabled = false;
        }
        function checkAll(objRef) {
            var GridView = objRef.parentNode.parentNode.parentNode;
            var inputList = GridView.getElementsByTagName("input");
            for (var i = 0; i < inputList.length; i++) {
                //Get the Cell To find out ColumnIndex
                var row = inputList[i].parentNode.parentNode;
                if (inputList[i].type == "checkbox" && objRef != inputList[i]) {
                    if (objRef.checked) {
                        //row.style.backgroundColor = "aqua";
                        inputList[i].checked = true;
                    }
                    else {
                        //if (row.rowIndex % 2 == 0) {
                        //    //Alternating Row Color
                        //    row.style.backgroundColor = "#C2D69B";
                        //}
                        //else {
                        //    row.style.backgroundColor = "white";
                        //}
                        inputList[i].checked = false;
                    }

                }
            }
        }
        function selectAssignTo(objDLL, objBTN_Name) {
            if (objDLL.value == "") {
                document.getElementById(objBTN_Name).disabled = true;
            }
            else {
                document.getElementById(objBTN_Name).disabled = false;
            }
        }
        window.onbeforeunload = DisableButton;

    </script>
</head>
<body>
    <form id="form1" runat="server">
          <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Outstanding Price Schedule"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>

             <div id="dvTab" runat="server" style="width: 100%;">
                 <br />
              <div id="TabAll" class="container" style="width: 100%;">
                    <ul class="nav nav-tabs">
                            <li id="liTabBid" runat="server" class="active">
                                <a href="#TabBid" data-toggle="tab">Bid</a>
                            </li>
                            <li id="liTabJob" runat="server"><a href="#TabJob" data-toggle="tab">Job</a>
                            </li>
                        </ul>

                   <div class="tab-content ">
                <div class="tab-pane active" id="TabBid" runat="server">

            <table class="auto-style28" >
                <tr>
                    <td class="auto-style34">
                        <asp:Label ID="Label54" runat="server" Text="Filter By Bid" Font-Names="tahoma" Font-Size="9pt"></asp:Label>
                        &nbsp;&nbsp;<asp:ListBox ID="lbTaskCodeBid" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2"></asp:ListBox>
                    </td>
                    <td style="border-style: none; border-width: 250px; max-width: 250px;" class="auto-style30">
                        <asp:Label ID="Label63" runat="server" Text="Filter By Task Name" Font-Names="tahoma" Font-Size="9pt"></asp:Label>
                        &nbsp;&nbsp;<asp:ListBox ID="lbtasknamebid" runat="server" ClientIDMode="Static" Rows="2" SelectionMode="Multiple"></asp:ListBox>

                    </td>
                </tr>
                <tr>
                    <td style="border-style: none; border-width: 250px; max-width: 250px;" class="auto-style34">
                        <asp:Label ID="Label58" runat="server" Font-Names="Tahoma" Font-Size="9pt" Text="Filter By Assign Status"></asp:Label>
                        &nbsp;
                                    <asp:ListBox ID="lbassignbid" runat="server" ClientIDMode="Static" Rows="2" SelectionMode="Multiple">
                                        <asp:ListItem Value="Assign">Assign</asp:ListItem>
                                        <asp:ListItem Value="Not_Assign">Not Assign</asp:ListItem>
                                    </asp:ListBox>
                    </td>
                    <td class="auto-style30">
                        <asp:Label ID="Label57" runat="server" Text="Filter By Task Status" Font-Names="tahoma" Font-Size="9pt"></asp:Label>
                        &nbsp;
                                    <asp:ListBox ID="lbstatusbid" runat="server" ClientIDMode="Static" Rows="2" SelectionMode="Multiple">
                                        <asp:ListItem Value="TK_NotStart">Not Start</asp:ListItem>
                                        <asp:ListItem Value="TK_Active">Active</asp:ListItem>
                                        <asp:ListItem Value="TK_Complete">Complete</asp:ListItem>
                                    </asp:ListBox></td>
                </tr>
                <tr>
                    <td style="border-color: 0; border-style: none; border-width: 0px;" class="auto-style35">

                        <asp:Button ID="btnfilterbid" runat="server" Text="Search Bid"  Height="30px" Width="70px" Font-Names="tahoma" Font-Size="9pt"  OnClick="btnfilterbid_Click"/>
                        &nbsp;&nbsp;
                                    <asp:Button ID="btnClearbid" runat="server" Font-Names="Tahoma" Font-Size="10pt" Height="30px"  Text="Clear Bid" Width="70px" OnClick="btnClearbid_Click" /> 
                    </td>
                </tr>
            </table>
            <table class="auto-style9">
                   <tr>
                    <td colspan="4">
                        <table>
                            <tr>
                                <td class="auto-style32">
                                    <asp:Label ID="Label61" runat="server" Text="Default Assign To.." Font-Names="tahoma" Visible="false"></asp:Label>
                                </td>
                                <td class="auto-style32">
                                <%--    <asp:DropDownList ID="ddlBidAssignTo" runat="server" Width="200px"  onchange="selectAssignTo(this,'btnAssignBid'); return false;">
                                    </asp:DropDownList>--%>
                                    <asp:ListBox ID="ddlBidAssignTo" runat="server" ClientIDMode="Static"  Width="200px"  SelectionMode="Multiple" onchange="selectAssignTo(this,'btnAssignBid');" ></asp:ListBox>
                                </td>
                                <td class="auto-style32">
                                    <asp:Button ID="btnAssignBid" ClientIDMode="Static" runat="server" BorderColor="Silver" BorderWidth="1px" Text="Assign" Width="70px" 
                                        Font-Names="tahoma" Font-Size="9pt" Height="30px" OnClick="btnAssignBid_Click" OnClientClick="if (!ConfirmSave()) return false; " /> 
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style11" colspan="3">
                        <div id="dvBidas" runat="server" style="height:600px;overflow-y:auto">
                            <asp:GridView ID="gv1" 
                                runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="100%"
                                ForeColor="Black"  BorderStyle="Solid" BorderWidth="2px" BorderColor="Black" OnRowDataBound="gv1_RowDataBound" OnRowCommand="gv1_RowCommand">
                                <Columns>
                                             <asp:TemplateField HeaderText="Select" >
                                        <HeaderTemplate >
                                            <asp:CheckBox ID="gv1CheckAll" runat="server" Text="Select All" onclick="checkAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gv1CheckBox" runat="server" Checked='<%# Convert.ToBoolean(Eval("chk_selected")) %>' onclick="checkDisableStaff(this,'gv1CheckBox', 'bid');" /> <%--, 'gv1ddlAssignTo'--%>
                                           <%-- <asp:HiddenField ID="gv1hidTaskID" runat="server" Value='<%# Bind("task_id") %>' />--%>  
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bid" SortExpression="wbs_name">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1wbs_name" runat="server" Text='<%# Bind("wbs_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    
                                        <asp:TemplateField HeaderText="Bid Desc" SortExpression="bid_desc">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1bid_desc" runat="server" Text='<%# Bind("bid_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1bid_doc" Text='<%# Bind("bid_doc") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="3%" />
                                    </asp:TemplateField>
                                                      <asp:TemplateField HeaderText="Bid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1bid_no" runat="server" Text='<%# Bind("proj_short_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="" >
                                        <ItemTemplate>
                                                <asp:ImageButton ID="ibtnSection" CommandName="viewdata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png" />
                                <asp:GridView ID="gv1Sub"  OnRowDataBound="gv1Sub_RowDataBound" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%" Visible="False" EmptyDataText="No data found.">
                                <Columns>

                           
                                    <asp:TemplateField HeaderText="Task Name" Visible="true" SortExpression="task_name">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1Task_name" runat="server" Text='<%# Bind("task_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="180px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Code" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1Task_code" runat="server" Text='<%# Bind("Task_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assign Status" SortExpression="assignstatus">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1Status" runat="server" Text='<%# Bind("assignstatus") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark" Visible="false">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gv1Remark" runat="server" Width="200px"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="taskid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1taskid" runat="server" Text='<%# Bind("task_id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="wbs name" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1wbsname" runat="server" Text='<%# Bind("Org_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Status" Visible="true" SortExpression="status_code">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1statuscode" runat="server" Text='<%# Bind("status_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Plan Start Date" Visible="true" SortExpression="target_start_date">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1PlanStartDate" Text='<%# Eval("target_start_date", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                            <asp:Label ID="gv1hidStartDate" Text='<%# Eval("target_start_date") %>' runat="server" style="display:none;"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Plan End Date" Visible="true" SortExpression="target_end_date">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1PlanEndDate" Text='<%# Eval("target_end_date", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                            <asp:Label ID="gv1hidEndDate" Text='<%# Eval("target_end_date") %>' runat="server" style="display:none;"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                       </Columns>
                                <HeaderStyle BackColor="#009999" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            </asp:GridView>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="220px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Assigned" >
                                        <ItemTemplate>
                                                <asp:ImageButton ID="ibtnAssigned" CommandName="assigndata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png" />
                                <asp:GridView ID="gv1Assigned"  runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%" Visible="False" EmptyDataText="No data found.">
                                <Columns>
              
                                          <asp:TemplateField HeaderText="Assigned To" >
                                        <ItemTemplate>
                                            <asp:Label ID="gv1AssiStaff_name" runat="server" Text='<%# Bind("staff_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Project Name" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv1AssiProject_name" runat="server" Text='<%# Bind("project_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                         
                                       </Columns>
                                <HeaderStyle BackColor="#009999" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            </asp:GridView>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="220px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#009999" ForeColor="White" />
                            </asp:GridView>
                </div>
                    </td>
                </tr>
             
                </table>

                </div>

                 <div class="tab-pane" id="TabJob" runat="server">

                                     <table class="auto-style28" border="0">
                    <tr>
                        <td class="auto-style36" style="max-width: 250px">
                            <asp:Label ID="Label55" runat="server" Text="Filter By Job" Font-Names="Tahoma" Font-Size="9pt" class="auto-style26"></asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="lbTaskCodeJob" runat="server" SelectionMode="Multiple" ClientIDMode="Static" Rows="2" ></asp:ListBox>
                        </td>
                        <td style="border-style: none; border-width: 250px; max-width: 250px;" class="auto-style33">
                            <asp:Label ID="Label64" runat="server" Font-Names="tahoma" Font-Size="9pt" Text="Filter By Task Name"></asp:Label>
                            &nbsp;&nbsp;<asp:ListBox ID="lbtasknamejob" runat="server" ClientIDMode="Static" Rows="2" SelectionMode="Multiple"></asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="auto-style36">
                            <asp:Label ID="Label59" runat="server" Font-Names="Tahoma" Font-Size="9pt" Text="Filter By Assign Status"></asp:Label>
                            &nbsp;
                                        <asp:ListBox ID="lbassignjob" runat="server" ClientIDMode="Static" Rows="2" SelectionMode="Multiple">
                                            <asp:ListItem Value="Assign">Assign</asp:ListItem>
                                            <asp:ListItem Value="Not_Assign">Not Assign</asp:ListItem>
                                            <%--<asp:ListItem Value="Rejected">Rejected</asp:ListItem>--%>
                                        </asp:ListBox>
                        </td>
                        <td style="border-style: none; border-width: 250px; max-width: 250px;" class="auto-style33">
                            <asp:Label ID="Label60" runat="server" Font-Names="Tahoma" Font-Size="9pt" Text="Filter By Task Status"></asp:Label>
                            &nbsp;
                                        <asp:ListBox ID="lbstatusjob" runat="server" ClientIDMode="Static" Rows="2" SelectionMode="Multiple">
                                            <asp:ListItem Value="TK_NotStart">Not Start</asp:ListItem>
                                            <asp:ListItem Value="TK_Active">Active</asp:ListItem>
                                            <asp:ListItem Value="TK_Complete">Complete</asp:ListItem>
                                        </asp:ListBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left;">
                            <asp:Button ID="btnfilterjob" runat="server" Text="Search Job" OnClick="btnfilterjob_Click" Font-Names="tahoma" Font-Size="9pt" Height="30px" Width="70px" />
                            &nbsp;&nbsp;
                                        <asp:Button ID="btnClearjob" runat="server" Font-Names="Tahoma" Font-Size="10pt" Height="30px" OnClick="btnClearjob_Click" Text="Clear Job" Width="70px" />
                        </td>
                    </tr>
                </table>
                       <table>
                <tr>
                    <td>
                        <asp:Label ID="Label62" runat="server" Text="Default Assign To.." Font-Names="tahoma" Visible="false"></asp:Label>
                    </td>
                    <td>
                       <%-- <asp:DropDownList ID="ddlJobAssignTo" runat="server" Width="200px" onchange="selectAssignTo(this, 'btnAssignJob');">
                        </asp:DropDownList>--%>
                         <asp:ListBox ID="ddlJobAssignTo" runat="server" ClientIDMode="Static"  Width="200px"  SelectionMode="Multiple" onchange="selectAssignTo(this,'btnAssignJob');" ></asp:ListBox>
                    </td>
                    <td>
                        <asp:Button ID="btnAssignJob" ClientIDMode="Static" runat="server" BorderColor="Silver" BorderWidth="1px" Text="Assign" Width="70px" 
                            Font-Names="tahoma" Font-Size="9pt"  Height="30px" OnClick="btnAssignJob_Click" OnClientClick="if (!ConfirmSave()) return false;" /> 
                    </td>
                </tr>
            </table>
                 <table class="auto-style9">
                               <tr>
                    <td class="auto-style10" colspan="3">
                        <div id="dvJobas" runat="server" style="height:600px;overflow-y:auto">
                            <asp:GridView ID="gv2" 
                                runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="100%" ForeColor="Black" OnRowCommand="gv2_RowCommand"
                                BorderStyle="Solid" BorderWidth="2px" BorderColor="Black" >
                                <Columns>
                              
                                      <asp:TemplateField HeaderText="Select" >
                                        <HeaderTemplate >
                                            <asp:CheckBox ID="gv2CheckAll" runat="server" Text="Select All" onclick="checkAll(this);" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gv2CheckBox" runat="server" Checked='<%# Convert.ToBoolean(Eval("chk_selected")) %>' onclick="checkDisableStaff(this,'gv2CheckBox', 'bid');" /> <%--, 'gv1ddlAssignTo'--%>
                                           <%-- <asp:HiddenField ID="gv2hidTaskID" runat="server" Value='<%# Bind("task_id") %>' />--%>  
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job" SortExpression="wbs_name">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2wbs_name" runat="server" Text='<%# Bind("wbs_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Job Desc" SortExpression="job_desc">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2job_desc" runat="server" Text='<%# Bind("job_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Documents">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2job_doc" Text='<%# Bind("job_doc") %>' runat="server"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="3%" />
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Job No" Visible="False">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2Job_no" runat="server" Text='<%# Bind("proj_short_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                   
                                     <asp:TemplateField HeaderText="" >
                                        <ItemTemplate>
                                       <asp:ImageButton ID="ibtnSectionJob" CommandName="viewdatajob" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png" />
                                <asp:GridView ID="gv2Sub"  OnRowDataBound="gv2Sub_RowDataBound" runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%" Visible="False" EmptyDataText="No data found.">
                                <Columns>

                                    <asp:TemplateField HeaderText="Task Name" Visible="true" SortExpression="task_name">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2Task_name" runat="server" Text='<%# Bind("task_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="180px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Code" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2Task_code" runat="server" Text='<%# Bind("Task_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Assign Status" SortExpression="assignstatus">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2Status" runat="server" Text='<%# Bind("assignstatus") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Remark" Visible="false">
                                        <ItemTemplate>
                                            <asp:TextBox ID="gv2Remark" runat="server" Width="200px"></asp:TextBox>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="200px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="taskid" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2taskid" runat="server" Text='<%# Bind("task_id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="wbs name" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2wbsname" runat="server" Text='<%# Bind("Org_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="20px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Task Status" Visible="true" SortExpression="status_code">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2statuscode" runat="server" Text='<%# Bind("status_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Plan Start Date" Visible="true" SortExpression="target_start_date">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2PlanStartDate" Text='<%# Eval("target_start_date", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                            <asp:Label ID="gv2hidStartDate" Text='<%# Eval("target_start_date") %>' runat="server" style="display:none;"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Plan End Date" Visible="true" SortExpression="target_end_date">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2PlanEndDate" Text='<%# Eval("target_end_date", "{0:dd/MM/yyyy}") %>' runat="server"></asp:Label>
                                            <asp:Label ID="gv2hidEndDate" Text='<%# Eval("target_end_date") %>' runat="server" style="display:none;"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                                    </asp:TemplateField>
                                       </Columns>
                                <HeaderStyle BackColor="#009999" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            </asp:GridView>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="220px" />
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Assigned">
                                        <ItemTemplate>
                                                <asp:ImageButton ID="ibtnAssignedjob" CommandName="assigndata" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png" />
                                <asp:GridView ID="gv2Assigned"  runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%" Visible="False" EmptyDataText="No data found.">
                                <Columns>
                     
                                          <asp:TemplateField HeaderText="Assigned To" >
                                        <ItemTemplate>
                                            <asp:Label ID="gv2AssiStaff_name" runat="server" Text='<%# Bind("staff_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                         <asp:TemplateField HeaderText="Project Name" Visible="false">
                                        <ItemTemplate>
                                            <asp:Label ID="gv2AssiProject_name" runat="server" Text='<%# Bind("project_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
    
                                       </Columns>
                                <HeaderStyle BackColor="#009999" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            </asp:GridView>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="220px" />
                                    </asp:TemplateField>
                                </Columns>
                                <HeaderStyle BackColor="#009999" ForeColor="White" />
                            </asp:GridView>
                       </div>
                    </td>
                </tr>
            </table>
          
                 </div>


                       </div>
                  </div>
        </div>
                    <asp:HiddenField ID="hidLogin" runat="server" />
                    <asp:HiddenField ID="hidCheckedBid" runat="server" />
                    <asp:HiddenField ID="hidBidTaskStatus" runat="server" />
                    <asp:HiddenField ID="hidBidTaskName" runat="server" />
                    <asp:HiddenField ID="hidBidAssignStatus" runat="server" />
                    <asp:HiddenField ID="hidsbid" runat="server" />
                    <asp:HiddenField ID="hidWorkID" runat="server" />
                    <asp:HiddenField ID="hidRequestorId" runat="server" />
                    <asp:HiddenField ID="hidReviewerId" runat="server" />
                    <asp:HiddenField ID="hidApproverId" runat="server" />
                    <asp:HiddenField ID="hidProjectMgrId" runat="server" />
                    <asp:HiddenField ID="hidADId" runat="server" />
                    <asp:HiddenField ID="hidDirectorId" runat="server" />
                    <asp:HiddenField ID="hidMode" runat="server" />
                    <asp:HiddenField ID="hidTaskAssi" runat="server" />
                    <asp:HiddenField ID="hidJobTaskName" runat="server" />
                    <asp:HiddenField ID="hidJobTaskStatus" runat="server" />
                    <asp:HiddenField ID="hidJobAssignStatus" runat="server" />
                    <asp:HiddenField ID="hidCheckedJob" runat="server" />
                    <asp:HiddenField ID="hidsjob" runat="server" />
    </form>
</body>
</html>
