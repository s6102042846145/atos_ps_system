﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="SupNoticeSearch.aspx.cs" Inherits="PS_System.form.Supplemental.SupNoticeSearch" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
     <style type="text/css">
        .auto-style1 {
            width: 1000px;
        }

        .auto-style5 {
            width: 58px;
        }
       .picker{

            vertical-align:middle;
        }
          .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
         .docview {
            height: 650px;
            width: 100%;
        }
         
          .cls_view_doc {
            color: #337ab7;
            cursor: pointer;
            text-decoration: none;
        }

            .cls_view_doc:hover {
                text-decoration: underline;
            }
    </style>
      
 <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
      <script type="text/javascript">
          $(document).keypress(
              function (event) {
                  if (event.which == '13') {
                      event.preventDefault();
                  }
              });
         
<%--
          $(document).ready(function () {
              maintainScrollPosition();
          });

          function pageLoad() {
              maintainScrollPosition();
          }

          function setScrollPosition(scrollValue) {
              $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }

        function maintainScrollPosition() {
            $("#bigDiv").scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
          }--%>

     
      </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
                <table style="width: 100%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Supplemental Notice Master"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
               <div style="margin:10px">
        <div>
                <table style="font-family: Tahoma; width: 100%;">
                     <tr>
                        <td>
                            <b>Bid No. : </b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlBidno" runat="server" Width="150px"> </asp:DropDownList>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td style="width: 200px">
                            <b>S/N No. : </b>
                        </td>
                        <td style="width: 500px">
                            <asp:TextBox ID="txtSNno" runat="server" Width="500px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td>
                            <b>Subject : </b>
                        </td>
                        <td>
                            <asp:TextBox ID="txtSubject" runat="server" Width="500px"></asp:TextBox>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                            
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnSearch" CssClass="btn btn-primary" runat="server" Text="Search" OnClick="btnSearch_Click"  />
                             &nbsp;<asp:Button ID="btnCreate" CssClass="btn btn-primary" runat="server" Text="Create SN" OnClick="btnCreate_Click" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                </table>
            </div>
       <div style="width: 90%">
                <asp:GridView ID="gvSNlist" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="90%"
                    Visible="true" ClientIDMode="Static" Font-Size="9pt" PageSize="10" BorderStyle="None" Font-Names="tahoma"
                    AllowPaging="true" OnPageIndexChanging="gvSNlist_PageIndexChanging">
                        <HeaderStyle  Font-Bold="True" ForeColor="White" Height="30px" />
                    <Columns>
                   
                        <asp:TemplateField HeaderText="Bid No.">
                            <ItemTemplate>
                                <asp:Label ID="lblBidno" runat="server"  Text='<%# Bind("bid_no")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="S/N No.">
                            <ItemTemplate>
                                <asp:Label ID="lblSN" runat="server"  Text='<%# Bind("supplementalnotice_code")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                          <asp:TemplateField HeaderText="Subject">
                            <ItemTemplate>
                                <asp:Label ID="lblSub" runat="server"  Text='<%# Bind("subject")%>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                       
                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>
                                   <asp:Button ID="btnEdit" CssClass="btn btn-primary" runat="server" Text="Edit" OnClick="btnEdit_Click"   />
                                   <asp:Button ID="btnSendbid" CssClass="btn btn-primary" runat="server" Text="Send Bidder" OnClick="btnSendbid_Click"  />
                              <%--  <asp:HyperLink ID="linkEdit" runat="server" NavigateUrl='<%# GetUrlSN(Eval("supplementalnotice_code"),Eval("bid_no"))%>' >Edit</asp:HyperLink>--%>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>

            </div>
                   </div>
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" /> 
    </form>
</body>
</html>
