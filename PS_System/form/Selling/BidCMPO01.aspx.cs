﻿using PS_Library;
using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidCMPO01 : System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        string zContract_NodeID = ConfigurationManager.AppSettings["Contract_NodeID"].ToString();

        OTFunctions zot = new OTFunctions();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {


                try { hidBidNo.Value = Request.QueryString["bid_no"].ToString(); }
                catch { hidBidNo.Value = ""; }
                try { hidBidRevision.Value = Request.QueryString["bid_revision"].ToString(); }
                catch { hidBidRevision.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); }
                catch { hidLogin.Value = ""; }
                // Debug 
                //pr_no = "PR-1-132021"; 
                hidLogin.Value = "z572470";
                lblMode.Text = hidMode.Value.ToUpper();

                ini_Data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        private string getMode()
        {
            string xmode = "CREATE";

            return xmode;
        }
        private void ini_pr()
        {
            string sql = "select distinct PR_NO as col1, PR_NO as col2 from PS_T_PR order by pr_no ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlPR.Items.Clear();
            ddlPR.DataSource = ds.Tables[0];
            ddlPR.DataTextField = ds.Tables[0].Columns[0].ColumnName;
            ddlPR.DataValueField = ds.Tables[0].Columns[1].ColumnName;
            ddlPR.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Please select PR No.-";
            li.Value = "";
            ddlPR.Items.Insert(0, li);
            ddlPR.SelectedIndex = 0; 
        }
        private void ini_purchaseOrg()
        {
            string sql = "select * from P_M_SAP_PURCHASINGORG order by Name ";
            var ds = ora._getDS(sql, zcommondb);
            ddlPurchaseOrg.Items.Clear();
            ddlPurchaseOrg.DataSource = ds.Tables[0];
            ddlPurchaseOrg.DataTextField = ds.Tables[0].Columns["Name"].ColumnName;
            ddlPurchaseOrg.DataValueField = ds.Tables[0].Columns["Code"].ColumnName;
            ddlPurchaseOrg.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Please select Org.-";
            li.Value = "";
            ddlPurchaseOrg.Items.Insert(0, li);
            try { ddlPurchaseOrg.SelectedValue = "5000"; } catch { ddlPurchaseOrg.SelectedIndex = 0; }
        }
        private void ini_purchaseGroup()
        {
            string sql = "select * from P_M_SAP_PURCHASINGGROUP where LOWER(name)  like '%null%'  order by Name ";
            var ds = ora._getDS(sql, zcommondb);
            ddlPurchaseGroup.Items.Clear();
            ddlPurchaseGroup.DataSource = ds.Tables[0];
            ddlPurchaseGroup.DataTextField = ds.Tables[0].Columns["Name"].ColumnName;
            ddlPurchaseGroup.DataValueField = ds.Tables[0].Columns["Code"].ColumnName;
            ddlPurchaseGroup.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Please select Group.-";
            li.Value = "";
            ddlPurchaseGroup.Items.Insert(0, li);
            ddlPurchaseGroup.SelectedIndex = 0;
        }
        private void ini_Data()
        {
            ini_pr();
            ini_purchaseOrg();
            ini_purchaseGroup();
            ini_bid_no();
           // ini_btnCreateSAPBid(); 
            ini_gv1();
        }
        private void ini_btnCreateSAPBid()
        {
            if (hidBidNo.Value != "")
            {
                btnCreateSAPBid.Enabled = false; 
            }
            else
            {
                string sql = " select * from ps_t_bid_selling_createbid where bid_no = '" + hidBidNo.Value + "' ";
                var ds =  zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    btnCreateSAPBid.Enabled = false;
                    btnCreateSAPBid.CssClass = "btn_normal_gray";
                }
                else
                {
                    btnCreateSAPBid.Enabled = true;
                    btnCreateSAPBid.CssClass = "btn_normal_blue";
                }
            }
        }
        private void ini_bid_no()
        {
            ddlBid.Items.Clear();
            string sql = "select distinct bid_no, bid_no as col2  from ps_t_bid_monitoring order by bid_no ";
            DataSet ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlBid.DataSource = ds.Tables[0];
            ddlBid.DataTextField = ds.Tables[0].Columns[0].ColumnName;
            ddlBid.DataValueField = ds.Tables[0].Columns[1].ColumnName;
            ddlBid.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Select Bid No.-";
            li.Value = "";
            ddlBid.Items.Insert(0, li);
            if (hidBidNo.Value != "")
            {
                try { ddlBid.SelectedValue = hidBidNo.Value; } catch { ddlBid.SelectedIndex = 0; }
            }
            if (hidBidRevision.Value != "") { lblBidRevision.Text = hidBidRevision.Value; }
        }
        private void ini_gv1()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("schedule_name", typeof(string));
            dt.Columns.Add("vendor_code", typeof(string));
            dt.Columns.Add("vendor_name", typeof(string));
            dt.Columns.Add("vendor_type", typeof(string));
            dt.Columns.Add("loa_date", typeof(string));
            dt.Columns.Add("loa_confirmed_date", typeof(string));
            dt.Columns.Add("sap_contract_runno", typeof(string));
            dt.Columns.Add("contract_no", typeof(string));
            dt.Columns.Add("contract_name", typeof(string));
            dt.Columns.Add("doc_node_id", typeof(string));
            dt.Columns.Add("status", typeof(string));
            dt.Columns.Add("bid_no", typeof(string));
            dt.Columns.Add("bid_revision", typeof(string));
            var dr = dt.NewRow();
            dt.Rows.Add(dr);
            gv1.DataSource = dt;
            gv1.DataBind();
        }
        private void get_BidRevision()
        {
            string sql = " select distinct bid_revision from ps_t_bid_selling where bid_no = '" + ddlBid.SelectedValue.ToString() + @"' " +
                " order by bid_revision desc ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {

                lblBidRevision.Text = ds.Tables[0].Rows[0]["bid_revision"].ToString();
                hidBidRevision.Value = lblBidRevision.Text;
            }
        }
        private void checkPR(string xbidNo, string xbid_revision)
        {
            string sql = " select * from ps_t_pr where bid_no = '" + xbidNo + "' and bid_revision= " + xbid_revision + " order by pr_no desc";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                try { ddlPR.SelectedValue = dr["pr_no"].ToString(); }
                catch { ddlPR.SelectedIndex = 0; }

            }
        }
        private void get_BidDesc()
        {
            if (ddlBid.SelectedIndex > 0)
            {
                
                string sql = " select bid_revision, bid_desc from bid_no where bid_no = '" + ddlBid.SelectedValue.ToString() + @"' and status = 'Active' ";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    txtBidDesc.Text = StripHTML(ds.Tables[0].Rows[0]["bid_desc"].ToString());
                    lblBidRevision.Text = ds.Tables[0].Rows[0]["bid_revision"].ToString();
                    //get_BidRevision();
                    hidBidNo.Value = ddlBid.SelectedValue.ToString();
                    hidBidRevision.Value = lblBidRevision.Text.Trim();
                    checkPR(hidBidNo.Value, hidBidRevision.Value);
                }
            }
        }
        private void ini_bidder()
        {
            string sql = @"select distinct a.bidder_id,  (b.vendor_name + '('+a.bidder_id +')' ) as bidder_name , a.bid_no, a.bid_revision
                            from ps_t_bid_selling_fileupload a
                            left join ps_t_bid_selling b on (a.bidder_id = b.bidder_id)
                            ";
            sql += " where a.bid_no = '" + hidBidNo.Value + "' and a.bid_revision = " + hidBidRevision.Value;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            //ddlBidderID.Items.Clear();
            //ddlBidderID.DataSource = ds.Tables[0];
            //ddlBidderID.DataTextField = ds.Tables[0].Columns[1].ColumnName;
            //ddlBidderID.DataValueField = ds.Tables[0].Columns[0].ColumnName;
            //ddlBidderID.DataBind();
            //ListItem li = new ListItem();
            //li.Text = "-Please select Winner-";
            //li.Value = "";
            //ddlBidderID.Items.Insert(0, li);
            //ddlBidderID.SelectedIndex = 0;
        }
        private bool isExistingCMSchedule(string xbidder_id, string xschedule_name, string xvendor_code)
        {
            bool xfound = false;
            string sql = @"select * from ps_t_bid_selling_cmpo 
                        where bidder_id = '"+xbidder_id+"' and bid_no = '" + hidBidNo.Value + @"' and bid_revision = " + hidBidRevision.Value + @" and schedule_name = '" + xschedule_name + "' and vendor_code = '" + xvendor_code + "' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xfound = true; 
            }
            return xfound; 
        }
        private void getWinnerBidder(string xbid_no , string xbid_revision)
        {
            string sql = " select * from [dbo].[ps_t_bid_selling_winner] where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                hidBidderID.Value = dr["winner_bidder_id"].ToString();
            }
            
        }
        private void genData_for_Cmpo(string xbid_no, string xbid_revision)
        {
            getWinnerBidder(xbid_no, xbid_revision);
            string xbidder_id = "";
            string xloa_date = "";
            string xloa_confirm_date = ""; 
            // Check ps_t_bid_selling_cmpo
            string sql = "";
            // Generate 
            sql = @"
                           select  s.bidder_id,  v.vendor_code, v.vendor_name,  sv.bid_no, sv.bid_revision, sv.ref_schedule_no, sv.vendor_type
                            from ps_t_bid_selling_vendor sv
                            left join ps_t_bid_selling s on (
                                sv.bid_no = s.bid_no
                                and sv.bid_revision = s.bid_revision
                                and sv.bidder_id = s.bidder_id)
							left join ps_m_vendor v on (sv.vendor_code = v.vendor_code)
                               where sv.bid_no = '" + hidBidNo.Value + "' and sv.bid_revision = " + hidBidRevision.Value + @" and sv.bidder_id = '" + hidBidderID.Value + "' ";
                          
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            for (int a = 0; a < ds.Tables[0].Rows.Count; a++)
            {
                var dr = ds.Tables[0].Rows[a];
                xbidder_id = dr["bidder_id"].ToString(); 
                if (isExistingCMSchedule(xbidder_id, dr["ref_schedule_no"].ToString(), dr["vendor_code"].ToString()) == false)
                {
                    //
                    sql = @" insert into ps_t_bid_selling_cmpo (
                                bidder_id
                                ,bid_no
                                ,bid_revision
                                ,schedule_name
                                ,description
                                ,vendor_code
                                ,vendor_type
                                ,loa_date
                                ,loa_confirm_date
                                ,sap_contract_runno
                                ,contract_no
                                ,created_by
                                ,created_date
                               
                            ) values (
                            '" + xbidder_id + @"' 
                            ,'" + hidBidNo.Value + @"' 
                            ," + hidBidRevision.Value + @"
                            ,'" + dr["ref_schedule_no"].ToString() + @"' 
                            ,'" + "" + @"' 
                            ,'" + dr["vendor_code"].ToString() + @"' 
                            ,'" + dr["vendor_type"].ToString() + @"' 
                            ,'" + xloa_date + @"' 
                            ,'" + xloa_confirm_date + @"' 
                            ,'" + "" + @"' 
                            ,'" + "" + @"' 
                            ,'" + hidLogin.Value + @"' 
                            ,'" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"' 
                            ) ";
                    zdb.ExecNonQuery(sql, zconnectionstr);

                }
                else
                {
                    // Edit && Update Data
                    sql = @" update ps_t_bid_selling_cmpo 
                                set
                                bidder_id = '" +xbidder_id + @"' 
                                ,bid_no = '" + hidBidNo.Value + @"'
                                ,bid_revision = " + hidBidRevision.Value + @"
                                ,schedule_name = '" + dr["ref_schedule_no"].ToString() + @"'
                                ,description = ''
                                ,vendor_code = '" + dr["vendor_code"].ToString() + @"'
                                ,vendor_type = '" + dr["vendor_type"].ToString() + @"'
                                ,loa_date = '" + xloa_date + @"'
                                ,loa_confirm_date = '" + xloa_confirm_date + @"'
                                ,sap_contract_runno = ''" + "" + @"' '
                                ,contract_no = ''
                                ,created_by = ''" + hidLogin.Value + @"' '
                                ,created_date = '" + System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + @"'
                     where bid_no = '" + hidBidNo.Value + @"' and bid_revision = " + hidBidRevision.Value + " and ref_schedule =  '" + dr["ref_schedule_no"].ToString() + "'  and vendor_code = '" + dr["vendor_code"].ToString() + @"' 
                    ";
                    zdb.ExecNonQuery(sql, zconnectionstr);

                }
            }
        }
        private bool isExistingCMPO(string xbid_no, string xbid_revision)
        {
            bool xfound = false;
            string sql = " select * from ps_t_bid_selling_cmpo where  bid_no ='" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xfound = true; 
            }
            return xfound; 
        }
        private void bind_gv1()
        {
            getWinnerBidder(hidBidNo.Value , hidBidRevision.Value);
            string sql = ""; 
            if (isExistingCMPO(hidBidNo.Value, hidBidRevision.Value)== false)
            {
                genData_for_Cmpo(hidBidNo.Value, hidBidRevision.Value);
            }
            // Bind gv
            sql = @"select c.bid_no,c.bid_revision, c.schedule_name, c.vendor_code, v.vendor_name,c.vendor_type,
                    c.sap_contract_runno,c.contract_no 
                    from ps_t_bid_selling_cmpo c 
                    left join ps_m_vendor as v on (c.vendor_code = v.vendor_code) 
                    where c.bid_no = '" + hidBidNo.Value + "' and c.bid_revision = " + hidBidRevision.Value + " and c.bidder_id = '" + hidBidderID.Value + "' ";
            var ds0 = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds0.Tables[0].Rows.Count > 0)
            {
                gv1.DataSource = ds0;
                gv1.DataBind();
            }else
            { ini_gv1(); }


        }
        private void load_data()
        {
            lblTestRunStatus.Text = "";
            string sql = "";

            if (ddlPR.SelectedIndex > 0)
            {
                sql = "select * from ps_t_pr where pr_no = '" + ddlPR.SelectedValue.ToString() + "' ";
                var dsPR = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (dsPR.Tables[0].Rows.Count > 0)
                {
                    var dr0 = dsPR.Tables[0].Rows[0];
                    hidBidNo.Value = dr0["bid_no"].ToString();
                    hidBidRevision.Value = dr0["bid_revision"].ToString(); 
                    ddlBid.SelectedValue = dr0["bid_no"].ToString();
                    lblBidRevision.Text= dr0["bid_revision"].ToString();
                    try { ddlPurchaseOrg.SelectedValue = dr0["purchase_org"].ToString(); } catch { }
                    try { ddlPurchaseGroup.SelectedValue = dr0["purchase_group"].ToString(); } catch { }

                    get_BidDesc();
                  //  ini_btnCreateSAPBid();
                    
                }
            }
            sql = @"

                    select cmpo.* 
                    from ps_t_bid_selling_cmpo as cmpo 
                    left join ps_t_bid_selling s on (cmpo.bidder_id = s.bidder_id and cmpo.bid_no = s.bid_no )  where cmpo.bid_no = '" + hidBidNo.Value + "' and cmpo.bid_revision = " + hidBidRevision.Value + " ";

            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                ini_bidder();
                bind_gv1();
                txtLOADate.Text =  Convert.ToDateTime( dr["loa_date"]).ToString("dd/MM/yyyy");
                txtLOAConfirmDate.Text = Convert.ToDateTime( dr["loa_confirm_date"]).ToString("dd/MM/yyyy");
                txtSapContractRunno.Text = dr["sap_contract_runno"].ToString();
                txtContractNo.Text = dr["contract_no"].ToString();
                hidContractNodeID.Value = dr["contract_node_id"].ToString();
                lblContractDocument.Text = dr["contract_docname"].ToString();
                lblTestRunStatus.Text = "TEST RUN STATUS :" + dr["testrun_status"].ToString();
            }
            else
            {
                bind_gv1();
            }
        }
        private DateTime converPickerDateToDate(string xddmmyyyy)
        {
            xddmmyyyy = xddmmyyyy.Replace("-", "/");
            DateTime xdate = new DateTime() ;
            xdate = DateTime.ParseExact(xddmmyyyy, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            return xdate; 
        }
        private bool validSave()
        {
            bool xpass = true;
            if (txtLOADate.Text == "") { xpass = false; }
            if (txtLOAConfirmDate.Text == "") { xpass = false; }
          //  if (txtContractNo.Text =="") { xpass = false; }
            return xpass;
        }
        private int getCTNORunno()
        {
            int last_num = 0;
            // Gen CTNO
            string sql = " select * from ps_sap_contract_runno ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                 last_num = int.Parse( dr["sap_contract_runno"].ToString());
            }
            last_num = last_num + 1;
            sql = "update ps_sap_contract_runno set sap_contract_runno = " + last_num;
            zdb.ExecNonQuery(sql, zconnectionstr);
            return last_num;
           
        }
        private string getSubLineJob_Relate(string xbid_no, string xbid_revision)
        {
            string sub_line = "";
            string sql = @"
                select b.bid_no, b.bid_revision, 
                b.bid_desc, j.job_no, j.revision,j.job_desc,j.job_type,j.sub_line, j.sub_line_name,j.sub_line_abbr
                 from bid_no b 
                left join bid_job_relate bj on (b.bid_id = bj.bid_id )
                left join job_no j on (bj.job_id = j.job_id) 
                where b.bid_no = '"+xbid_no+"' and b.bid_revision = "+ xbid_revision;

            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                sub_line = dr["sub_line"].ToString();
            }
            return sub_line;
        }
        private string getCTNO(string xbid_no, string xbid_revision)
        {
            string x = "";
            var xrunno = getCTNORunno();
            string xbid_type = "";
            string xCTNOType = "";
            string xPurchageOrg = "";
            string xPurchaseOrg2 = "1";
            string xYY = "";
            
            if ((System.DateTime.Now.Year )> 2500)
            {
                xYY = (System.DateTime.Now.Year ).ToString().Substring(2, 2);
            }
            else
            {
                xYY = (System.DateTime.Now.Year + 543).ToString().Substring(2, 2);
            }
            string sql = " select * from bid_no where bid_no = '" + xbid_no + "' and bid_revision = " + xbid_revision;
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                xbid_type = dr["bid_type"].ToString();
                if (xbid_type.ToUpper().Contains("EPC SUB"))
                {
                    xCTNOType = "W";
                    xPurchageOrg = "3";
                }
                else if (xbid_type.ToUpper().Contains("EPC LINE"))
                {
                    xCTNOType = "W";
                    xPurchageOrg = "4";
                }
                else
                {
                    xCTNOType = "S";

                }
                if (getSubLineJob_Relate(xbid_no, xbid_revision) == "S")
                {
                    xPurchageOrg = "3";
                }
                else
                {
                    xPurchageOrg = "4";
                }

            }
            if (xbid_no.Length < 17)
            {
                x = xCTNOType + xrunno + "-" + xPurchageOrg + xPurchaseOrg2 + xYY + "-" + xbid_no;
            }
            else
            {
                x = xCTNOType + xrunno + "-" + xPurchageOrg + xPurchaseOrg2 + xYY + "-" + xbid_no.Substring(0,17);
            }
            return x; 
        }
        private void createFolderContract(string xCTNNO)
        {
            string sql = "";
            var xcontract_nodeid = zot.getNodeByName(zContract_NodeID, xCTNNO);
            if (xcontract_nodeid == null)
            {
                xcontract_nodeid = zot.createFolder(zContract_NodeID, xCTNNO);
            }
            //hidContractNodeID.Value = xcontract_nodeid.ID.ToString(); 
            //check folder and create shortcut documents
            // BID DOCUMENT
            var bid_document_nodeid = zot.getNodeByName(xcontract_nodeid.ID.ToString(), "BID DOCUMENT");
            if (xcontract_nodeid == null)
            {
                bid_document_nodeid = zot.createFolder(xcontract_nodeid.ID.ToString(), "BID DOCUMENT");
            }
            sql = " select * from ps_t_bid_doc where bid_no = '"+hidBidNo.Value+"' and bid_revision = "+hidBidRevision.Value+" ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                var xbid_doc = dr["biddoc_node_id"].ToString();
                if (xbid_doc != "")
                {
                    // get Node and create shortcut to BID DOCUMENT
                    var xbid_docnode = zot.getNodeByID(xbid_doc);
                    try
                    {
                        zot.createShortCut(bid_document_nodeid.ID.ToString(), xbid_docnode, true);
                    }
                    catch { }

                }
            }
            // PR
            var pr_nodeid = zot.getNodeByName(xcontract_nodeid.ID.ToString(), "PR");
            if (xcontract_nodeid == null)
            {
                pr_nodeid = zot.createFolder(xcontract_nodeid.ID.ToString(), "PR");
            }
            sql = " select* from ps_t_pr where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value + " ";
            ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                var xpr_doc = dr["pdf_node_id"].ToString();
                if (xpr_doc != "")
                {
                    // get Node and create shortcut to BID DOCUMENT
                    var xpr_docnode = zot.getNodeByID(xpr_doc);
                    try
                    {
                        zot.createShortCut(pr_nodeid.ID.ToString(), xpr_docnode, true);
                    }
                    catch { }

                }
            }
            // TOR
            var tor_nodeid = zot.getNodeByName(xcontract_nodeid.ID.ToString(), "TOR");
            if (xcontract_nodeid == null)
            {
                tor_nodeid = zot.createFolder(xcontract_nodeid.ID.ToString(), "TOR");
            }
            sql = "select * from ps_t_tor where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value + "  ";
            ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {

                    var dr = ds.Tables[0].Rows[i];
                    var xtor_doc = zot.getNodeByID(dr["tor_doc_nodeid"].ToString());
                    if (xtor_doc != null)
                    {
                        try
                        {
                            zot.createShortCut(tor_nodeid.ID.ToString(), xtor_doc, true);
                        }
                        catch
                        {

                        }
                    }
                }
            }


                // PO
                sql = @"
                    select distinct sap_po_num from ps_t_bid_selling_cmpo_item
                    where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value + @" 
                    and contract_no = '"+txtContractNo.Text.Trim()+"' and sap_po_num is not null order by sap_po_num ";

            ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
           
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                var dr = ds.Tables[0].Rows[i];
                var xpo_workspace = zot.getNodeByName("2452080", dr["sap_po_num"].ToString());
                if (xpo_workspace != null)
                {
                    try {
                        zot.createShortCutPO(xcontract_nodeid.ID.ToString(), xpo_workspace, true);
                    }
                    catch
                    {

                    }
                }
            }
            // Shortcut Contract to PO
            sql = @"
                    select distinct sap_po_num from ps_t_bid_selling_cmpo_item
                    where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value + @" 
                    and contract_no = '" + txtContractNo.Text.Trim() + "' and sap_po_num is not null order by sap_po_num ";

            ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
           
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {

                var dr = ds.Tables[0].Rows[i];
                var xpo_workspace = zot.getNodeByName("2452080", dr["sap_po_num"].ToString());
                if (xpo_workspace != null)
                {
                    try
                    {
                        zot.createShortCutCTNO(xpo_workspace.ID.ToString(), xcontract_nodeid, true);
                    }
                    catch
                    {

                    }
                }
            }
        }
        private void SaveData()
        {
            string xresult = ""; 
            if (validSave() == true)
            {
                string xbidder_id = "";
                string xbidno = ddlBid.SelectedValue.ToString();
                string xbid_revision = lblBidRevision.Text;
                string xloa_date = converPickerDateToDate(txtLOADate.Text).ToString("yyyy-MM-dd");
                string xloa_confirmdate = converPickerDateToDate(txtLOAConfirmDate.Text).ToString("yyyy-MM-dd");
                string xschedule_name = "";
                string xvendor_code = "";
                string xsap_contract_runno = txtSapContractRunno.Text;
                if (txtContractNo.Text.Trim() == "")
                {
                    txtContractNo.Text = getCTNO(hidBidNo.Value, hidBidRevision.Value);
                    
                }
                createFolderContract(txtContractNo.Text);
                string xcontract_no = txtContractNo.Text;
               
                // checking bidder & schedule
                for (int a = 0; a < gv1.Rows.Count; a++)
                {
                    xschedule_name = ((Label)(gv1.Rows[a].FindControl("gvScheduleName"))).Text;
                    xvendor_code = ((Label)(gv1.Rows[a].FindControl("gvVendorCode"))).Text;
                    

                    string sql = " update ps_t_bid_selling_cmpo ";
                    sql += " set ";
                    sql += " loa_date = '" + xloa_date + "', ";
                    sql += " loa_confirm_date = '" + xloa_confirmdate + "', ";
                    sql += " sap_contract_runno = '" + xsap_contract_runno + "', ";
                    sql += " contract_no = '" + xcontract_no + "', ";
                    sql += " contract_node_id = '" + hidContractNodeID.Value + "' ";


                    sql += " where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value + " and schedule_name = '" + xschedule_name + "' and vendor_code = '"+ xvendor_code+ "' ";
                    zdb.ExecNonQuery(sql, zconnectionstr);
                  
                }
                try
                {
                    xresult = PS_Library.Contract_Master.Sap_Contract.SendToSap(txtContractNo.Text);
                }
                catch { }
                Response.Write("<script> alert('"+xresult+"');</script>");
               
            }
            else { Response.Write("<script> alert('Cannot Save. Please check your data again.');</script>"); }


        }
        protected void gv1_OnRowCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "getsapno")
            {
                int a = System.Convert.ToInt32(e.CommandArgument);
                ((TextBox)gv1.Rows[a].FindControl("gvSapContractNo")).Text = getContractSAPRunno();
            }
        }
        private string getContractSAPRunno()
        {
            string x = "";
            //dummy_po, purchage_org, purchase_group, doc_date
            x = Sap_PO.genXMLRequestPO6Digit(System.DateTime.Now.ToString("yyyy-MM-dd"));
            
            return x; 
        }
        protected void ddlBid_SelectedIndexChanged(object sender, EventArgs e)
        {
            ddlPR.SelectedIndex = 0;
            lblBidRevision.Text = "";
            txtBidDesc.Text = "";
            txtLOADate.Text = "";
            txtLOAConfirmDate.Text = "";
            txtContractNo.Text = "";
            txtSapContractRunno.Text = ""; 

            get_BidDesc();
            ini_bidder();
            load_data();
            checkStatus();
        }

        protected void ddlBidderID_SelectedIndexChanged(object sender, EventArgs e)
        {
            bind_gv1();
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {

            SaveData();
            // Create Folder 
            createFolderContract(txtContractNo.Text);
            checkStatus();
        }

        protected void btnGeneratePOItems_Click(object sender, EventArgs e)
        {
            string url = "";
            if (txtContractNo.Text.Trim() == "")
            {
                Response.Write("<script> alert('Cannot generate PO-Items. Pleasee create contract first.');</script>");
            }
            else
            {
                //Valid Check create POItems Already
                string sql = "select * from ps_t_bid_selling_cmpo_item where contract_no = '" + txtContractNo.Text + "'";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Response.Write("<script> alert('PO Items already exist.');</script>");
                    url = "AccountCodeSetting.aspx?zmode=VIEW&zcontract_no=" + txtContractNo.Text;
                    Response.Write("<script> window.open('" + url + "');</script>");
                }
                else
                {
                    var msg = "";
                    try
                    {
                        msg = PS_Library.PO.Sap_PO.createDataToSap(txtContractNo.Text.Trim(), 0);
                        // Redirect to Acc Setting Mode Assginment 
                        url = "AccountCodeSetting.aspx?zmode=VIEW&zcontract_no=" + txtContractNo.Text;
                        Response.Write("<script> window.open('" + url + "');</script>");
                    }
                    catch
                    {

                    }
                    Response.Write("<script> alert('"+msg+"');</script>");

                }
                // If No gen PO Items
            }
        }
        private void GeneratePOItems()
        {
            // Check Vendor Type Single, Joint Venture, Consortium

            // Group Schedule, Part, Currency, Vendor
            // Gen PO Items

            Response.Write("<script>window.open('BidcontractMgt2.aspx?contractno="+txtContractNo.Text+"');</script>");
        }

        protected void ddlPR_SelectedIndexChanged(object sender, EventArgs e)
        {
           // load_data();
           
        }

        protected void btnCreateSAPBid_Click(object sender, EventArgs e)
        {
            // Open Form
            Response.Write("<script> window.open('bidcreate.aspx?BidNo="+hidBidNo.Value+"&Revision="+hidBidRevision.Value+"');</script>");
        }

        protected void btnGetSapNo_Click(object sender, EventArgs e)
        {
            if (txtSapContractRunno.Text.Trim() == "")
            {
                txtSapContractRunno.Text = getContractSAPRunno();
            }
            else
            {
                Response.Write("<script>alert('Already got sap running number.');</script>");
            }
        }

        protected void btnPreviewContract_Click(object sender, EventArgs e)
        {
            if (hidContractNodeID.Value != "")
            {
                string url = "https://ecmqacon1.egat.co.th/otcs/cs.exe/app/nodes/" + hidContractNodeID.Value+ "?order_by=name_asc";
                Response.Write("<script> window.open('" + url + "');</script>");
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('You not upload signed contract document yet.');", true);
            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            if (txtContractNo.Text != "")
            {

                var xcontent = fuploadContract.FileBytes;
                var xdocname = fuploadContract.FileName;
                if (xdocname != "")
                {
                    // zot.uploadDoc()
                    var xparent_node = zot.getNodeByName(zContract_NodeID, txtContractNo.Text.Trim());
                    // create Folder
                    if (xparent_node == null)
                    {
                        xparent_node = zot.createFolder(zContract_NodeID, txtContractNo.Text.Trim());
                    }
                    //Upload
                    var doc_node_id = zot.uploadDoc(xparent_node.ID.ToString(), xdocname, xdocname, xcontent, "");
                    hidContractNodeID.Value = doc_node_id.ToString();
                    lblContractDocument.Text = xdocname;
                    string sql = "update [dbo].[ps_t_bid_selling_cmpo] set contract_node_id = '" + hidContractNodeID.Value + "' , contract_docname = '" + lblContractDocument.Text.Trim() + "' " +
                        " where contract_no = '" + txtContractNo.Text.Trim() + "'  ";
                    zdb.ExecNonQuery(sql, zconnectionstr);
                    checkStatus();
                }
                else
                {
                    Response.Write("<script> alert('Cannot upload! please check your data again.');</script>");
                }
            }
            else { Response.Write("<script> alert('Cannot upload because the contract was not created yet.');</script>"); }
        }
        #region checkStatus
        //OpenBid
        private void checkOpenBid(string xbid_no)
        {
            string sql = "";
            bool xfound = true;
            // Check on ECM
            sql = " select* from ps_t_bid_selling_createbid where bid_no = '"+xbid_no+"'";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xfound = true; 
            }
            // Check on SYNC SAP
            if (xfound == false)
            {
                sql = "select * from p_m_sap_bid_master where bid_number = 'TS12-S-02' ";
                var ds2 = ora._getDS(sql, zcommondb);
                if (ds2.Tables[0].Rows.Count > 0)
                {
                    xfound = true;
                }
            }
            if (xfound == true)
            {
                ibtnStatusBid.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusBid.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusBid.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusBid.Height = Unit.Parse("40px");
            }
            ibtnStatusBid.DataBind();
        }
        // LOAConfirm, CreateContract,
        private void checkLOAConfirm(string xbid_no, string xbid_revision)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @" select * from ps_t_bid_selling_cmpo  
                    where bid_no = '"+xbid_no +"' and bid_revision= "+xbid_revision +" and loa_confirm_date is not null";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                    xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null
            
            if (xstatus == true)
            {
                ibtnStatusLOA.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusLOA.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusLOA.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusLOA.Height = Unit.Parse("40px");
            }
            ibtnStatusLOA.DataBind();
        }
        // CreateContract,
        private void checkCreateContract(string xbid_no, string xbid_revision)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @"
                    select * from ps_t_bid_selling_cmpo  
                    where bid_no = '"+xbid_no+"' and bid_revision= "+xbid_revision+" and contract_no is not null and contract_no <> '' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null

            if (xstatus == true)
            {
                ibtnStatusCreateContract.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusCreateContract.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusCreateContract.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusCreateContract.Height = Unit.Parse("40px");
            }
            ibtnStatusCreateContract.DataBind();
        }
        //UploadSignedContract
        private void checkUploadContract(string xbid_no, string xbid_revision)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @"
                    select * from ps_t_bid_selling_cmpo  
                    where bid_no = '" + xbid_no + "' and bid_revision= " + xbid_revision + " and contract_node_id is not null and contract_node_id  <> '' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null

            if (xstatus == true)
            {
                ibtnStatusUploadContract.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusUploadContract.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusUploadContract.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusUploadContract.Height = Unit.Parse("40px");
            }
            ibtnStatusUploadContract.DataBind();
        }
        // GenPOItems, 
        private void checkGenPOItems(string xbid_no, string xbid_revision, string xcontract_no)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @"
                   
                    select * from ps_t_bid_selling_cmpo_item  
                    where bid_no = '"+ xbid_no + "' and bid_revision= "+ xbid_revision + @"
                    and contract_no = '"+ xcontract_no + "'";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null

            if (xstatus == true)
            {
                ibtnStatusGenPOItem.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusGenPOItem.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusGenPOItem.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusGenPOItem.Height = Unit.Parse("40px");
            }
            ibtnStatusGenPOItem.DataBind();
        }
        //AssignAcc
        private void checkSubmittoACC(string xbid_no, string xbid_revision, string xcontract_no)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @"
                   
                    select * from wf_ps_acc_setting
                    where bid_no = '" + xbid_no + "' and bid_revision= " + xbid_revision + @"
                    and contract_no = '" + xcontract_no + "'";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null

            if (xstatus == true)
            {
                ibtnStatusAssignAcc.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusAssignAcc.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusAssignAcc.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusAssignAcc.Height = Unit.Parse("40px");
            }
            ibtnStatusAssignAcc.DataBind();
        }
        //TestRun
        private void checkTestRun(string xbid_no, string xbid_revision, string xcontract_no)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @"
                    select * from ps_t_bid_selling_cmpo
                    where  testrun_status = 'Pass'
                    and bid_no = '" + xbid_no + "' and bid_revision= " + xbid_revision + @"
                    and contract_no = '" + xcontract_no + "'";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null

            if (xstatus == true)
            {
                ibtnStatusTestRun.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusTestRun.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusTestRun.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusTestRun.Height = Unit.Parse("40px");
            }
            ibtnStatusTestRun.DataBind();
        }
        //CreatePO on SAP
        private void checkPOOnSAP(string xbid_no, string xbid_revision, string xcontract_no)
        {
            string sql = "";
            bool xstatus = true;
            // Check on ECM not null
            sql = @"
                    select * from ps_t_bid_selling_cmpo_item
                    where (sap_po_num <> '' and sap_po_num is not null) 
                    and bid_no = '" + xbid_no + "' and bid_revision= " + xbid_revision + @"
                    and contract_no = '" + xcontract_no + "'";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xstatus = true;
            }
            else
            {
                xstatus = false;
            }
            // Check on SYNC SAP
            // Check on ECM not null

            if (xstatus == true)
            {
                ibtnStatusSendToSAP.ImageUrl = @"..\..\images\icon_circle_blue.png";
                ibtnStatusSendToSAP.Height = Unit.Parse("40px");
            }
            else
            {
                ibtnStatusSendToSAP.ImageUrl = @"..\..\images\icon_circle_blank.png";
                ibtnStatusSendToSAP.Height = Unit.Parse("40px");
            }
            ibtnStatusSendToSAP.DataBind();
        }
        private void checkStatus()
        {
            checkOpenBid(ddlBid.SelectedValue.ToString());
            checkLOAConfirm(hidBidNo.Value, hidBidRevision.Value);
            checkCreateContract(hidBidNo.Value, hidBidRevision.Value);
            checkUploadContract(hidBidNo.Value, hidBidRevision.Value);
            checkGenPOItems(hidBidNo.Value, hidBidRevision.Value, txtContractNo.Text );
            checkSubmittoACC(hidBidNo.Value, hidBidRevision.Value, txtContractNo.Text);
            checkTestRun(hidBidNo.Value, hidBidRevision.Value, txtContractNo.Text);
            checkPOOnSAP(hidBidNo.Value, hidBidRevision.Value, txtContractNo.Text);
        }
        #endregion

        protected void ibtnSearchPR_Click(object sender, ImageClickEventArgs e)
        {
            load_data();
        }

        protected void btnSubmitToACC_Click(object sender, EventArgs e)
        {
            // Redirect to Acc Setting Mode Assginment 
            string url = "AccountCodeSetting.aspx?zmode=SUBMIT TO ACC&zcontract_no="+txtContractNo.Text ;
            Response.Write("<script> window.open('"+url+"');</script>");
        }

        protected void btnBIDDoc_Click(object sender, EventArgs e)
        {
            string url = "../TOR/TOR01.aspx?bidno=" + hidBidNo.Value;
            Response.Write("<script> window.open('" + url + "');</script>");
        }

        protected void btnCreatePOOnSAP_Click(object sender, EventArgs e)
        {
            string url = "";
            if (txtContractNo.Text.Trim() == "")
            {
                Response.Write("<script> alert('Cannot generate PO-Items. Pleasee create contract first.');</script>");
            }
            else
            {
                //Valid Check create POItems Already
                string sql = "select * from ps_t_bid_selling_cmpo_item where contract_no = '" + txtContractNo.Text + "'";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                { 
                    var msg = "";
                    try
                    {
                        string batch = "GEN_SAPPO_" + System.DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
                        msg = batch +" : " + PS_Library.PO.Sap_PO.SendToSap(txtContractNo.Text.Trim(), "", batch);
                        // Redirect to Acc Setting Mode Assginment 
                       // url = "AccountCodeSetting.aspx?zmode=VIEW&zcontract_no=" + txtContractNo.Text;
                       // Response.Write("<script> window.open('" + url + "');</script>");
                    }
                    catch
                    {

                    }
                    Response.Write("<script> alert('" + msg + "');</script>");

                }
               
            }
        }

        protected void btnSyncDoc_Click(object sender, EventArgs e)
        {
            createFolderContract(txtContractNo.Text);
            checkStatus();
            Response.Write("<script> alert('-Update Finished-');</script>");
        }

        protected void btnUpdate_Click(object sender, EventArgs e)
        {
            // Update P6
            string xtask_id = "";
            string xmessage = ""; 
            p6Class p6 = new p6Class();
            var dt = p6.getActivityBid(hidBidNo.Value);
            for (int i =0; i < dt.Rows.Count; i++)
            {
                //
                var dr = dt.Rows[i]; 
                if (dr["TASK_NAME"].ToString().Contains("H :") == true)
                {
                    xtask_id = dr["TASK_ID"].ToString();
                    xmessage = dr["TASK_NAME"].ToString(); break;
                }
            }
            if (xtask_id != "")
            {
                var actualLOADate = converPickerDateToDate(txtLOAConfirmDate.Text);
                p6Class.UpdateActualFinishDate(int.Parse(xtask_id), actualLOADate);
                Response.Write("<script> alert('P6 Task > "+xmessage + " > was updated completion - contract signed date. " +"');</script>");
            }
        }
    }
}