﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="PriceSheduleUpView.aspx.cs" Inherits="PS_System.form.Selling.PriceSheduleUpView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container mb-4 mt-4">
                <div class="row mb-4">
                    <h5>EGAT - Price Schedule Upload</h5>
                    <div class="ml-auto">
                        <h5 runat="server" id="h_bidnumberTitle" class="text-primary"></h5>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-header  d-flex justify-content-between align-items-center">
                        รายละเอียดข้อมูลเอกสารประกวดราคา
                                <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Click to expand"><i class="bi bi-justify"></i></a>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-8">
                                    <label>Bid No.</label>
                                    <asp:Label ID="lb_bidnumber" runat="server" CssClass="form-control"></asp:Label>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Bid Version.</label>
                                    <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Bid Description.</label>
                                    <asp:Label ID="txt_bidDescription" runat="server" Height="150" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-header  d-flex justify-content-between align-items-center">
                        รายละเอียดข้อมูลเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <p class="card-title d-flex justify-content-between align-items-center">
                            ข้อมูลผู้ซื้อเอกสารประกวดราคา
                            <asp:LinkButton runat="server" ID="btn_bidWinner" CssClass="btn btn-success" Text='<i class="bi bi-trophy mr-2"></i>Bid Winner' OnClick="btn_bidWinner_Click" Visible="false"></asp:LinkButton>
                        </p>
                        <div class="table-responsive-sm mb-4">
                            <asp:GridView ID="gv_bidder" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv_bidder_RowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField ItemStyle-Width="10">
                                        <ItemTemplate>
                                            <label><%# Container.DataItemIndex + 1 %></label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="bidder_id" HeaderText="ID" />
                                    <asp:BoundField DataField="vendor_code" HeaderText="Vendot Code" />
                                    <asp:BoundField DataField="vendor_name" HeaderText="Vendor Name" />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-Width="130">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btn_delete" Text='<i class="bi bi-arrow-repeat mr-2"></i>Refresh' CssClass="btn btn-danger btn-sm" ToolTip="click to refresh" OnClientClick="if (!confirm('Do you want to refresh?')) return false;" CommandName="IsDelete" CommandArgument='<%#Eval("bidder_id") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                            <small class="text-muted">*คุณจำเป็นต้องรีเฟรช "Refresh" ข้อมูลก่อน ถ้าคุณต้องการที่จะเปลี่ยน Vendor Type</small>
                        </div>
                        <p class="card-title">ข้อมูลผู้ซื้อที่อัปโหลดเอกสารประกวดราคา</p>
                        <div class="table-responsive-sm">
                            <asp:GridView ID="gv_bidSellingVendor" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv_bidSellingVendor_RowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:BoundField DataField="bidder_id" HeaderText="ID" />
                                    <asp:BoundField DataField="ref_schedule_no" HeaderText="Schedule No." />
                                    <asp:BoundField DataField="vendor_name" HeaderText="Vendor Name." />
                                    <asp:BoundField DataField="vendor_type" HeaderText="Vendor Type." />
                                    <asp:BoundField DataField="doctype_name" HeaderText="Doc Type." />
                                    <asp:BoundField DataField="doc_name" HeaderText="Doc Name." />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btn_download" Text='<i class="bi bi-arrow-bar-down"></i>' CssClass="btn btn-primary btn-sm" ToolTip="click to download" CommandName="IsDownload" CommandArgument='<%#Eval("doc_node_id") %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btn_delete" Text='<i class="bi bi-x-circle"></i>' CssClass="btn btn-danger btn-sm" ToolTip="click to delete" OnClientClick="if (!confirm('Do you want to delete?')) return false;" CommandName="IsDelete" CommandArgument='<%#string.Format("{0};{1}",Eval("row_id"),Eval("doc_node_id") ) %>'></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="PriceScheduleUploadData.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                            <asp:LinkButton runat="server" ID="btn_addSchedule" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-upload mr-2"></i>Upload' OnClick="btn_addSchedule_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="gv_bidSellingVendor" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
