﻿using PS_Library;
using PS_Library.PO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidContractMgt2 : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try { hidContractNo.Value = Request.QueryString["contractno"].ToString(); }
                catch
                {
                    hidContractNo.Value = "";
                }
                if (hidContractNo.Value != "")
                {
                    load_contractdata();
                }
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        private void load_contractdata()
        {
            string sql = "select * from ps_t_bid_selling_cmpo where contract_no = '"+ hidContractNo.Value +"' ";
            var ds = zdb.ExecSql_DataSet(sql, zconn);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                hidBidNo.Value = dr["bid_no"].ToString();
                hidBidRevision.Value = dr["bid_revision"].ToString();
                txtBidNo.Text = hidBidNo.Value;
                txtBidRev.Text = hidBidRevision.Value;

            }
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            saveData();
        }
        #region data
        private bool validDataSaving()
        {
            bool xpass = true;
            if (txtBidNo.Text.Trim()=="") { xpass = false; }
            if (txtBidRev.Text.Trim() == "") { xpass = false; }
            if (txtScopeOfWork.Text.Trim() == "") { xpass = false; }
            if (txtProject.Text.Trim() == "") { xpass = false; }
          //  if (txtWithinMonths.Text.Trim() == "") { xpass = false; }
            
            return xpass; 
        }
        private bool isExisting(string xBidNo)
        {
            bool xfound = false;
            string sql = " select * from ps_t_contract_no where bid_no = '"+txtBidNo.Text.Trim() +"' and bid_revision = '"+txtBidRev.Text.Trim()+"' ";
            var dt = zdb.ExecSql_DataTable(sql, zconn);
            if (dt.Rows.Count > 0)
            {
                xfound = true;
              
            }
            return xfound; 
        }
        private string ConvertDateToString(string xdate)
        {
            string xdatestr = "";
            if (xdate != "")
            {
              var xdateValue =  DateTime.ParseExact(xdate, "dd/mm/yyyy", CultureInfo.InvariantCulture);
                xdatestr = "'" +xdateValue.ToString("yyyy-MM-dd")+ "'";
            }
            else { xdatestr = "null"; }
            return xdatestr; 
        }
        private string ConvertDateFormat(string xdate)
        {
            string xdatestr = "";
            if (xdate != "")
            {
                var xdateValue = DateTime.Parse(xdate);
                xdatestr =  xdateValue.ToString("dd/MM/yyyy") ;
            }
            else { xdatestr = ""; }
            return xdatestr;
        }
        private string ConvertNumber(string xnum)
        {
            if (xnum == "") { return  "null"; } else { return xnum.ToString().Replace(",",""); }
        }
        private string ConvertStringToCurrency(string xvalue)
        {
            string xtotal = "";
            if (xvalue == "") { return xtotal; } else { return System.Convert.ToDecimal(xvalue).ToString("0,000.00"); }
          
        }
        private void saveData()
        {
            string sql = "";
            if (validDataSaving() == true)
            {
                if (isExisting(txtBidNo.Text) == false)
                {
                    // Add data
                    sql = @"
                        insert into ps_t_contract_no (
                            bid_no,
                            bid_revision,
	                        scope_of_work,
	                        project,
	                        job_no,
	                        contract_no,
	                        transmission_line_no,
	                        line_description,
	                        line_length,
	                        within_months,
	                        contractor,
	                        purchasing_method,
	                        purchasing_org,
	                        purchasing_group,
	                        LI_date,
	                        comfirm_LI_date,
	                        contract_sign_date,
	                        requisitioner,
	                        plant,
	                        total_thb,
	                        total_usd,
	                        currency1,
	                        currency1_date,
	                        currency1_exchangerate,
	                        currency2,
	                        currency2_date,
	                        currency2_exchangerate,
	                        currency3,
	                        currency3_date,
	                        currency3_exchangerate,
	                        currency4,
	                        currency4_date,
	                        currency4_exchangerate,
	                        Insurance_percent,
	                        customer_clearance,
	                        vat,
	                        local_preference,
                            status
                        ) values
                        (
                            '" + txtBidNo.Text + @"',
                            '" + txtBidRev.Text + @"',
	                        '" + txtScopeOfWork.Text + @"',
	                        '" + txtProject.Text + @"',
	                        '" + txtJobNo.Text + @"',
	                        '" + txtContractNo.Text + @"',
	                        '" + txtTransmissionLineNo.Text + @"',
	                        '" + txtLineDescription.Text + @"',
	                        " + ConvertNumber(txtLineLength.Text) + @",
	                        " + ConvertNumber(txtWithinMonths.Text) + @",
	                        '" + txtContractor.Text + @"',
	                        '" + txtPurchasingMethod.Text + @"',
	                        '" + txtPurchasingOrg.Text + @"',
	                        '" + txtPurchasingGroup.Text + @"',
	                        " + ConvertDateToString(txtLIDate.Text) + @",
	                        " + ConvertDateToString(txtConfirmLIDate.Text) + @",
	                        " + ConvertDateToString(txtSignDate.Text) + @",
	                        '" + txtRequisitioner.Text + @"',
	                        '" + txtPlant.Text + @"',
	                        " + ConvertNumber(txtTotalTHB.Text) + @",
	                        " + ConvertNumber(txtTotalUSD.Text) + @",
	                        '" + txtCurrency1.Text + @"',
	                        " + ConvertDateToString(txtExDate1.Text) + @",
	                        " + ConvertNumber(txtExRate1.Text) + @",
	                        '" + txtCurrency2.Text + @"',
	                        " + ConvertDateToString(txtExDate2.Text) + @",
	                        " + ConvertNumber(txtExRate2.Text) + @",
                            '" + txtCurrency3.Text + @"',
	                        " + ConvertDateToString(txtExDate3.Text) + @",
	                        " + ConvertNumber(txtExRate3.Text) + @",
                            '" + txtCurrency4.Text + @"',
	                        " + ConvertDateToString(txtExDate4.Text) + @",
	                        " + ConvertNumber(txtExRate4.Text) + @",
                            " + ConvertNumber(txtInsurance.Text) + @",
	                        " + ConvertNumber(txtCustomer_Clearance.Text) + @",
	                        " + ConvertNumber(txtVAT.Text) + @",
	                        " + ConvertNumber(txtLocal_Preference.Text) + @",
                            'New'
                        )
                    ";
                    zdb.ExecNonQuery(sql, zconn);
                    Response.Write("<script> alert(' - Saved - ');<script>");
                    txtStatus.Text = "New";
                }
                else if (txtStatus.Text.ToUpper() == "NEW")
                {
                    // Update data
                    sql = @"
                        update ps_t_contract_no  set 
                          
	                        scope_of_work =   '" + txtScopeOfWork.Text + @"',
	                        project =  '" + txtProject.Text + @"',
	                        job_no = '" + txtJobNo.Text + @"',
	                        contract_no=  '" + txtContractNo.Text + @"',
	                        transmission_line_no =  '" + txtTransmissionLineNo.Text + @"' ,
	                        line_description =  '" + txtLineDescription.Text + @"',
	                        line_length =  " + ConvertNumber(txtLineLength.Text) + @",
	                        within_months = " + ConvertNumber(txtWithinMonths.Text) + @",
	                        contractor =  '" + txtContractor.Text + @"',
	                        purchasing_method =  '" + txtPurchasingMethod.Text + @"',
	                        purchasing_org =   '" + txtPurchasingOrg.Text + @"',
	                        purchasing_group =  '" + txtPurchasingGroup.Text + @"',
	                        LI_date =  " + ConvertDateToString(txtLIDate.Text) + @",
	                        comfirm_LI_date =   " + ConvertDateToString(txtConfirmLIDate.Text) + @",
	                        contract_sign_date  =  " + ConvertDateToString(txtSignDate.Text) + @",
	                        requisitioner =  '" + txtRequisitioner.Text + @"',
	                        plant =  '" + txtPlant.Text + @"',
	                        total_thb =   " + ConvertNumber(txtTotalTHB.Text) + @",
	                        total_usd =   " + ConvertNumber(txtTotalUSD.Text) + @",
	                        currency1 =   '" + txtCurrency1.Text + @"',
	                        currency1_date =  " + ConvertDateToString(txtExDate1.Text) + @",
	                        currency1_exchangerate =  " + ConvertNumber(txtExRate1.Text) + @",
	                        currency2 =   '" + txtCurrency2.Text + @"',
	                        currency2_date =  " + ConvertDateToString(txtExDate2.Text) + @",
	                        currency2_exchangerate =  " + ConvertNumber(txtExRate2.Text) + @",
	                        currency3 = '" + txtCurrency3.Text + @"',
	                        currency3_date =   " + ConvertDateToString(txtExDate3.Text) + @",
	                        currency3_exchangerate =  " + ConvertNumber(txtExRate3.Text) + @",
	                        currency4 =   '" + txtCurrency4.Text + @"',
	                        currency4_date =  " + ConvertDateToString(txtExDate4.Text) + @",
	                        currency4_exchangerate =  " + ConvertNumber(txtExRate4.Text) + @" ,
	                        Insurance_percent =  " + ConvertNumber(txtInsurance.Text) + @" ,
	                        customer_clearance =   " + ConvertNumber(txtCustomer_Clearance.Text) + @",
	                        vat =  " + ConvertNumber(txtVAT.Text) + @",
	                        local_preference =   " + ConvertNumber(txtLocal_Preference.Text) + @",
                            status = 'New'
                       
                    ";
                    sql += " where bid_no = '" + txtBidNo.Text + "' and bid_revision = '" + txtBidRev.Text + "' ";
                    zdb.ExecNonQuery(sql, zconn);
                    Response.Write("<script> alert(' - Saved - ');<script>");
                }
                else
                {
                    Response.Write("<script> alert('Warning! : Can not save because this Bid No has submitted to SAP.');</script>");
                }
            }
            else
            {
                Response.Write("<script> alert('Warning! : Can not save. Please check your input data again.');</script>");
            }
            
          
        }
        
        #endregion
        private void findBidDesc()
        {
            DataTable dt = new DataTable();

            clsBidDashbaord objdb = new clsBidDashbaord();
            dt = objdb.GetDetail(txtBidNo.Text );
            if (dt.Rows.Count > 0)
            {

                txtBidNo.Text = dt.Rows[0]["bid_no"].ToString();
                txtBidRev.Text = dt.Rows[0]["bid_revision"].ToString();
                txtBidDescription.Text = dt.Rows[0]["bid_desc"].ToString();
                txtScopeOfWork.Text = dt.Rows[0]["bid_desc"].ToString();

            }
            string sql = "";
            try // Finding project
            {
                sql = "select * from project_no where project_id = '" + txtBidNo.Text.Split("-".ToCharArray(), StringSplitOptions.RemoveEmptyEntries)[0] + "'";
                var dtprj = zdb.ExecSql_DataTable(sql, zconn);
                if (dtprj.Rows.Count > 0) { txtProject.Text = dtprj.Rows[0]["project_name_en"].ToString(); }
            }catch { }
            try // Finding Job
            {
                txtJobNo.Text = " ";
                sql = @"
                        select b.bid_no, b.bid_revision, b.bid_desc
                        , j.job_no, j.revision, j.job_desc,j.job_status,j.status
                        from bid_no b
                        left join bid_job_relate bj on (b.bid_id = bj.bid_id)
                        left join job_no j on (bj.job_id = j.job_id)
                        where b.bid_no = '"+txtBidNo.Text +"' and b.bid_revision = "+txtBidRev.Text +@" and j.job_status = 'Active'

                        ";
                var dtJob = zdb.ExecSql_DataTable(sql, zconn);
                for (int i = 0; i < dtJob.Rows.Count; i++)
                {
                    txtJobNo.Text += " " + dtJob.Rows[i]["job_no"].ToString() + ",";
                }
                txtJobNo.Text = txtJobNo.Text.Trim();
                if (txtJobNo.Text.Length >= 1) { txtJobNo.Text = txtJobNo.Text.Substring(0, txtJobNo.Text.Length-1); }
            }
            catch { }
        }
        private void findBidContractData()
        {
            string sql = " select * from ps_t_contract_no where bid_no = '"+txtBidNo.Text +"' and bid_revision = '"+txtBidRev.Text+"' ";
            var dt = zdb.ExecSql_DataTable(sql, zconn);
            if (dt.Rows.Count > 0)
            {
                var dr = dt.Rows[0];
                txtBidNo.Text = dr["bid_no"].ToString();
                txtBidRev.Text = dr["bid_revision"].ToString();
                txtScopeOfWork.Text = dr["scope_of_work"].ToString();
                txtProject.Text = dr["project"].ToString();
                txtJobNo.Text = dr["job_no"].ToString();
                txtContractNo.Text = dr["contract_no"].ToString();
                txtTransmissionLineNo.Text = dr["transmission_line_no"].ToString();
                txtLineDescription.Text = dr["line_description"].ToString();
                txtLineLength.Text = dr["line_length"].ToString();
                txtWithinMonths.Text = dr["within_months"].ToString();
                txtContractor.Text = dr["contractor"].ToString();
                txtPurchasingMethod.Text = dr["purchasing_method"].ToString();
                txtPurchasingOrg.Text = dr["purchasing_org"].ToString();
                txtPurchasingGroup.Text = dr["purchasing_group"].ToString();
                txtLIDate.Text = ConvertDateFormat( dr["LI_date"].ToString());
                txtConfirmLIDate.Text = ConvertDateFormat(dr["comfirm_LI_date"].ToString());
                txtSignDate.Text = ConvertDateFormat(dr["contract_sign_date"].ToString());
                txtRequisitioner.Text = dr["requisitioner"].ToString();
                txtPlant.Text = dr["plant"].ToString();
                txtTotalTHB.Text = ConvertStringToCurrency(dr["total_thb"].ToString());
                txtTotalUSD.Text = ConvertStringToCurrency( dr["total_usd"].ToString());
                txtCurrency1.Text = dr["currency1"].ToString();
                txtExDate1.Text = ConvertDateFormat(dr["currency1_date"].ToString());
                txtExRate1.Text = dr["currency1_exchangerate"].ToString();
                txtCurrency2.Text = dr["currency2"].ToString();
                txtExDate2.Text = ConvertDateFormat(dr["currency2_date"].ToString());
                txtExRate2.Text = dr["currency2_exchangerate"].ToString();
                txtCurrency3.Text = dr["currency3"].ToString();
                txtExDate3.Text = ConvertDateFormat(dr["currency3_date"].ToString());
                txtExRate3.Text = dr["currency3_exchangerate"].ToString();
                txtCurrency4.Text = dr["currency4"].ToString();
                txtExDate4.Text = ConvertDateFormat(dr["currency4_date"].ToString());
                txtExRate4.Text = dr["currency4_exchangerate"].ToString();
                txtInsurance.Text = dr["Insurance_percent"].ToString();
                txtCustomer_Clearance.Text = dr["customer_clearance"].ToString();
                txtVAT.Text = dr["vat"].ToString();
                txtLocal_Preference.Text = dr["local_preference"].ToString();
	           
                txtStatus.Text = dr["status"].ToString();
                if (dt.Rows[0]["status"].ToString().ToUpper() == "SUBMITTED") { btnSave.Enabled = false; }

            } else { txtStatus.Text = "N/A"; }
            
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            findBidDesc();
            findBidContractData(); 
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
           submitToSAP();
        }
        private  void submitToSAP()
        {
            // Code Gen Data and Submit to SAP.
            //
            string poNumber = Sap_PO.SendToSap(this.txtContractNo.Text,(this.txtBidRev.Text==""?0: Convert.ToInt32(this.txtBidRev.Text)));
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}