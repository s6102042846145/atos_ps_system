﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidTabulation.aspx.cs" Inherits="PS_System.form.Selling.BidTabulation" MaintainScrollPositionOnPostback="true" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
      <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
              .Colsmall {
            width: 60px;
            max-width: 60px;
            min-width: 60px;

            height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
             .Colsmall2 {
            width: 310px;
            max-width: 310px;
            min-width: 310px;

             height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
        .Colsmall3 {
            width: 70px;
            max-width: 70px;
            min-width: 70px;
            
            height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
       .Colsmall4 {
            width: 60px;
            max-width: 60px;
            min-width: 60px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
              .Colsmall-com {
            width: 80px;
            max-width: 80px;
            min-width: 80px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
            .Colsmall-h {
           width: 70px;
            max-width: 70px;
            min-width: 70px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
        .Colsmall-v {
           width: 120px;
            max-width: 120px;
            min-width: 120px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
  /*          #tdHeader   td {
            width: 90px;
            max-width: 90px;
            min-width: 90px;
        }*/
         /*  #tbXlsHeader td {
            width: 80px;
            max-width: 80px;
            min-width: 80px;
        }*/

    #gvVen1 th, #gvVen2 th, #gvVen3 th{
               width: 70px;
            max-width: 70px;
            min-width: 70px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }

 /*       #gvXls td{
                   width: 80px;
            max-width: 80px;
            min-width: 80px;
        }*/

   
    </style>
    
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

       <script>
           $(document).keypress(
               function (event) {
                   if (event.which == '13') {
                       event.preventDefault();
                   }
               });

           $(document).ready(function () {
               $('[id*=ddlVen]').multiselect({
                   enableCaseInsensitiveFiltering: true,
                   enableFiltering: true,
                   maxHeight: 300
               });
               maintainScrollPosition();
           });
           function NumOnlyChk(e) {
               var key;
               if (window.event) // IE
               {
                   key = e.keyCode;
               }
               else if (e.which) // Netscape/Firefox/Opera
               {
                   key = e.which;
               }
               var vchar = String.fromCharCode(key);
               if (((vchar >= "0" && vchar <= "9") && (key != 8) || ((vchar == "." || key == 49) && (key != 8)))) {

                   e.returnValue = true;
               }
               else {
                   return false;
               }
               e.returnValue = true;
               return true;
           }




           function pageLoad() {
               maintainScrollPosition();
           }

           function setScrollPosition(scrollValue) {
               $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }

           function maintainScrollPosition() {
               $("#dvAllVender").scrollLeft($('#<%=hfScrollPosition.ClientID%>').val());
        }

       </script>
</head>
<body>
    <form id="form1" runat="server">
               <div>
            <table style="width: 99%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                     
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Tabulation"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div id="dvMain" runat="server"  style="width:100%;padding-top:10px;">
                      <table style="width: 100%;margin:10px">
                          <tr>
                              <td colspan="4" style="text-align:left; width:8%">
                                     <asp:Label ID="Label6" runat="server" Text="Bid No.: " Width="10%"></asp:Label>
                                      <asp:DropDownList ID="ddlBidno" runat="server" Width="200px"></asp:DropDownList>
                                      &nbsp; <asp:Label ID="lblReqBid" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                    <br /> <asp:Label ID="Label1" runat="server" Text="Schedule Name: " Width="10%"></asp:Label>
                              <asp:DropDownList ID="ddlSch" runat="server" Width="200px" ></asp:DropDownList>
                                    &nbsp; <asp:Label ID="lblReqSch" runat="server" Text="*" ForeColor="red"  Visible="false"></asp:Label>
                                    <br />  <asp:Label ID="Label2" runat="server" Text="Part: " Width="10%"></asp:Label>
                                         <asp:DropDownList ID="ddlPart" runat="server" Width="200px"></asp:DropDownList>
                                       <br />  <asp:Label ID="Label3" runat="server" Text="Vendor:" Width="10%"></asp:Label>
                                  <asp:ListBox ID="ddlVen" runat="server"  Width="200px"  SelectionMode="Multiple" ></asp:ListBox>
                          <br />   <asp:Button ID="btnFil" runat="server" Text="Filter"  class="btn btn-primary" OnClick="btnFil_Click"/>
                                  </td>
                              </tr>
                          </table>
                          <table id="tbShowDetail" runat="server" >
                         <tr>
                       <td >
                       </td>
                             <td style="padding-left:20px;">
                               
                  

                             </td>
                   </tr>
                                      <tr>
                              <td >
                                  <div id="dvShowXls" runat="server" style="padding-left:10px;padding-top:20px">
                                   <div >
                                   <table id="tbXlsHeader" runat="server" style="width:98%">
                                       <tr style="color:white;background-color:#164094;width:34%;text-align:center;font-family:Tahoma;font-size:9pt">
                                           <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label16" runat="server" Text="Part" Font-Bold="True"></asp:Label>
                                           </td>
                                           <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label17" runat="server" Text="Item No." Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label18" runat="server" Text="Equipment Code" Font-Bold="True"></asp:Label>
                                           </td>

                                            <td style="border:1px solid white;" class="Colsmall2">
                                               <asp:Label ID="Label26" runat="server" Text="Description" Font-Bold="True"></asp:Label>
                                           </td>
                                           <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label19" runat="server" Text="SOE. Unit Price" Font-Bold="True" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label20" runat="server" Text="SOE. Amount" Font-Bold="True" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label22" runat="server" Text="LSE. Unit Price" Font-Bold="True" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label23" runat="server" Text="LSE. Amount" Font-Bold="True" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label24" runat="server" Text="LTC. Unit Price" Font-Bold="True" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label25" runat="server" Text="LTC. Amount" Font-Bold="True" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                           </td>
                                       </tr>
                                   </table>
                                              </div>
                                 <div id="dvScroll" runat="server" style="width:100%;overflow:scroll;height:400px;">
                                    <asp:GridView ID="gvXls" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." ShowHeader="False" >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White"  />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_No") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Equipment Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3"/>
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("desc_short") %>' ToolTip='<%# Bind("description") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall2"/>
                                    </asp:TemplateField>
                                  
                                       <asp:TemplateField HeaderText="" >
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh1" runat="server" Text="SOE. Unit Price" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                         <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh2" runat="server" Text="SOE. Amount" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh3" runat="server" Text="LSE. Unit Price" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                <HeaderTemplate>
                                                  <asp:Label ID="lblh4" runat="server" Text="LSE. Amount" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                  <HeaderTemplate>
                                                  <asp:Label ID="lblh5" runat="server" Text="LTC. Unit Price" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh6" runat="server" Text="LTC. Amount" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                 </div>
                                      </div>
                              </td>
                                <td style="vertical-align:top;padding-left:20px;width:100%;position:relative;">
                                   <%--   <div >
                                   <table id="tdHeader" runat="server">
                                       <tr style="color:white;background-color:#164094;width:34%;text-align:center;font-family:Tahoma;font-size:9pt">
                                             <td style="border:1px solid white;" class="Colsmall-v">
                                               <asp:Label ID="Label29" runat="server" Text="Vendor" Font-Bold="True"></asp:Label>
                                           </td>
                                           <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label4" runat="server" Text="Part" Font-Bold="True"></asp:Label>
                                           </td>
                                           <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label5" runat="server" Text="Item No." Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label7" runat="server" Text="Equipment Code" Font-Bold="True"></asp:Label>
                                           </td>
                                           <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label8" runat="server" Text="SOE. Unit Price" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label9" runat="server" Text="SOE. Amount" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label10" runat="server" Text="LSE. Unit Price" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label11" runat="server" Text="LSE. Amount" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label12" runat="server" Text="LTC. Unit Price" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall-h">
                                               <asp:Label ID="Label13" runat="server" Text="LTC. Amount" Font-Bold="True"></asp:Label>
                                           </td>
                                       </tr>
                                   </table>
                                              </div>--%>
                             <%--           <div id="dvNodata" runat="server">
                                                 <table style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;font-family:tahoma;font-size:9pt;width:100%;border-collapse:collapse;">
                                                     <tr align="center" style="border-style:Solid;height:25px">
                                                         <td colspan="12">No data found. </td>
                                                     </tr>
                                                 </table>
                                             </div>--%>
                                     <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
                                          <div id="dvAllVender" runat="server" style="overflow:auto;height:480px;width:85%;position:absolute" onscroll="setScrollPosition(this.scrollLeft);">
                                         
                        <div id="dvmainVen1" runat="server" style="display:table-cell;vertical-align:top;">
                              <asp:Label ID="lblContiam1" runat="server" ></asp:Label>
                                &nbsp; <asp:DropDownList ID="ddlContiam1" runat="server" Width="250px" OnSelectedIndexChanged="ddlContiam1_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:GridView ID="gvVen1" runat="server" Width="34%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  style="margin-right:20px" >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Width="30px"/>
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066"  Height="30px"/>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <Columns>
                                  <asp:TemplateField HeaderText="Vender">
                                        <ItemTemplate>
                                            <asp:Label ID="lbVen1" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  CssClass="Colsmall-v" HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                                            <asp:Label ID="hidVen1Bidder" runat="server" Text='<%# Bind("bidder_id") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  CssClass="Colsmall" HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Equipment Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                               <asp:TemplateField HeaderText="" >
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh1" runat="server" Text="SOE. Unit Price" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                         <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh2" runat="server" Text="SOE. Amount" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh3" runat="server" Text="LSE. Unit Price" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                <HeaderTemplate>
                                                  <asp:Label ID="lblh4" runat="server" Text="LSE. Amount" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                  <HeaderTemplate>
                                                  <asp:Label ID="lblh5" runat="server" Text="LTC. Unit Price" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh6" runat="server" Text="LTC. Amount" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                      <asp:TemplateField HeaderText="%Dif">
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_perDif" runat="server"   Text='<%# Convert.ToDecimal(Eval("per_dif", "{0:0.00}")) %>'></asp:Label>
                                                <asp:Label ID="Label4" runat="server" Text="%"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                                                   <div id="dvNodataVen1" runat="server">
                                                 <table style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;font-family:tahoma;font-size:9pt;width: 710px;border-collapse:collapse;margin-left:20px;">
                                                     <tr align="center" style="border-style:Solid">
                                                         <td colspan="12">No data found. </td>
                                                     </tr>
                                                 </table>
                                             </div>
                   <%--     <div id="dvLineVen2" runat="server" style="display:table-cell;border:2px solid #FF9900;"></div>--%>
                        <div id="dvmainVen2" runat="server" style="display:table-cell;padding-right:20px;padding-left:20px;vertical-align:top;">
                                          &nbsp;&nbsp;         <asp:Label ID="lblContiam2" runat="server" ></asp:Label>
                                &nbsp; <asp:DropDownList ID="ddlContiam2" runat="server" Width="250px" OnSelectedIndexChanged="ddlContiam2_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    <asp:GridView ID="gvVen2" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066"  Height="30px"/>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <Columns>
                                       <asp:TemplateField HeaderText="Vender">
                                        <ItemTemplate>
                                            <asp:Label ID="lbVen2" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  CssClass="Colsmall-v" HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                                                <asp:Label ID="hidVen2Bidder" runat="server" Text='<%# Bind("bidder_id") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="Colsmall" HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_No") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Equipment Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                                                            <asp:TemplateField HeaderText="" >
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh1" runat="server" Text="SOE. Unit Price" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                         <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh2" runat="server" Text="SOE. Amount" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh3" runat="server" Text="LSE. Unit Price" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                <HeaderTemplate>
                                                  <asp:Label ID="lblh4" runat="server" Text="LSE. Amount" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                  <HeaderTemplate>
                                                  <asp:Label ID="lblh5" runat="server" Text="LTC. Unit Price" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" >
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh6" runat="server" Text="LTC. Amount" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%Dif">
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_perDif" runat="server"  Text='<%# Convert.ToDecimal(Eval("per_dif", "{0:0.00}")) %>' ></asp:Label>  
                                                <asp:Label ID="Label44" runat="server" Text="%"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                            
                                </Columns>
                            </asp:GridView>
                        </div>
                                                <div id="dvNodataVen2" runat="server" style="vertical-align:middle">
                                                 <table style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;font-family:tahoma;font-size:9pt;width: 700px;border-collapse:collapse;margin-right:20px;margin-left:20px;">
                                                     <tr align="center" style="border-style:Solid">
                                                         <td colspan="12">No data found. </td>
                                                     </tr>
                                                 </table>
                                             </div>
                  <%--      <div id="dvLineVen3" runat="server" style="display:table-cell;border:2px solid #FF9900"></div>--%>
                        <div id="dvmainVen3" runat="server" style="display:table-cell;margin-right:-85px;padding-left:20px;vertical-align:top;">
                            
                                &nbsp;&nbsp;         <asp:Label ID="lblContiam3" runat="server" ></asp:Label>
                                &nbsp; <asp:DropDownList ID="ddlContiam3" runat="server" Width="250px" OnSelectedIndexChanged="ddlContiam3_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                     <asp:GridView ID="gvVen3" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  style="margin-right:-100px" >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066"  Height="30px"/>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />  
                                <Columns>
                                       <asp:TemplateField HeaderText="Vender">
                                        <ItemTemplate>
                                            <asp:Label ID="lbVen3" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  CssClass="Colsmall-v" HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                                                <asp:Label ID="hidVen3Bidder" runat="server" Text='<%# Bind("bidder_id") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_No") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Equipment Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>' ></asp:Label>
                                        </ItemTemplate>
                                     <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderText="" >
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh1" runat="server" Text="SOE. Unit Price" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                         <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh2" runat="server" Text="SOE. Amount" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh3" runat="server" Text="LSE. Unit Price" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                <HeaderTemplate>
                                                  <asp:Label ID="lblh4" runat="server" Text="LSE. Amount" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                  <HeaderTemplate>
                                                  <asp:Label ID="lblh5" runat="server" Text="LTC. Unit Price" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh6" runat="server" Text="LTC. Amount" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="%Dif">
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_perDif" runat="server"  Text='<%# Convert.ToDecimal(Eval("per_dif", "{0:0.00}")) %>'></asp:Label>  
                                                <asp:Label ID="Label444" runat="server" Text="%"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                                                <div id="dvNodataVen3" runat="server" style="vertical-align:middle">
                                                 <table style="background-color:White;border-color:#CCCCCC;border-width:1px;border-style:None;font-family:tahoma;font-size:9pt;width: 650px;border-collapse:collapse;margin-left:20px;">
                                                     <tr align="center" style="border-style:Solid">
                                                         <td colspan="12">No data found. </td>
                                                     </tr>
                                                 </table>
                                             </div>
                                          </div>
                               
                              </td>
    
                          </tr>
                          </table>
            </div>
      <div id="dvComScore" runat="server">
          <table style="width:100%;margin:10px">
              <tr>
                  <td>
                      <asp:Label ID="Label14" runat="server"  Font-Names="Tahoma" Font-Size="11pt"  Text="Giving Comments and Scores"></asp:Label>
                  </td>
              </tr>
              <tr>
                  <td>
                        <asp:Label ID="Label15" runat="server" Text="Vender:" Width="10%"></asp:Label>
                     <asp:DropDownList ID="ddlCommentVen" runat="server" Width="200px"></asp:DropDownList>
                  </td>
              </tr>
                 <tr>
                  <td>
                        <asp:Label ID="Label27" runat="server" Text="Technical Score:" Width="10%"></asp:Label>
                    <asp:TextBox ID="txtTechScore" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                  </td>
              </tr>
                 <tr>
                  <td>
                        <asp:Label ID="Label28" runat="server" Text="Price Score:" Width="10%"></asp:Label>
                     <asp:TextBox ID="txtPriScore" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="150px"></asp:TextBox>
                  </td>
              </tr>
                 <tr>
                  <td>
                        <asp:Label ID="Label30" runat="server" Text="Comment:" Width="10%"></asp:Label>
                     <asp:TextBox ID="txtComment" runat="server" Rows="3" TextMode="MultiLine"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                  </td>
              </tr>
              <tr>
                  <td>
                        <asp:Button ID="btnAdd" runat="server" Text="Add"  class="btn btn-primary" OnClick="btnAdd_Click" />
                  </td>
              </tr>
              <tr>
                  <td>
     <div id="Div1" runat="server" style="margin:10px;margin-right:30px" >
                                    <asp:GridView ID="gvComScore" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  style="margin-right:20px;" OnRowCommand="gvComScore_RowCommand"  >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Width="30px"/>
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066"  Height="30px"/>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Vendor">
                                        <ItemTemplate>
                                            <asp:Label ID="lblVender" runat="server" Text='<%# Bind("vendor_name") %>'></asp:Label>
                                            <asp:Label ID="lblbidno" runat="server" Text='<%# Bind("bid_no") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblrev" runat="server" Text='<%# Bind("bid_revision") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblsch" runat="server" Text='<%# Bind("schedule_name") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblbidid" runat="server" Text='<%# Bind("bidder_id") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblvencode" runat="server" Text='<%# Bind("vendor_code") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lblrowid" runat="server" Text='<%# Bind("row_id") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Technical Score" >
                                        <ItemTemplate>
                                            <asp:Label ID="lblTecSc" runat="server"  Text='<%# Bind("technical_score") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Price Score">
                                        <ItemTemplate>
                                            <asp:Label ID="lblPriceSc" runat="server" Text='<%# Bind("price_score") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Comment">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComment" runat="server" Text='<%# Bind("comment") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Committee By">
                                        <ItemTemplate>
                                            <asp:Label ID="lblComby" runat="server" Text='<%# Bind("committee_emp_id") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center"  />
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                   <asp:ImageButton ID="imgDelete" ClientIDMode="Static" runat="server" ImageUrl="../../Images/bin.png" CommandName="deletedata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Delete" Width="20" ImageAlign="NotSet"  />
                                        </ItemTemplate>
                                        <ItemStyle  HorizontalAlign="Center" VerticalAlign="Middle" Width="5%"  />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>
                  </td>
              </tr>
          </table>
      </div>
        <div id="dvButton" runat="server" style="margin:10px">
             <asp:Button ID="btnSave" runat="server" Text="Save"  class="btn btn-primary" OnClick="btnSave_Click" />
        </div>
            <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRev" runat="server" />
        <asp:HiddenField ID="hidJobNo" runat="server" />
        <asp:HiddenField ID="hidJobRev" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidMc" runat="server" />
         <asp:HiddenField ID="hidTempid" runat="server" />
    </form>
</body>
</html>
