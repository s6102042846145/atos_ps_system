﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Bid
{
    public partial class BidSellingDescription : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        private void GetBidSellingDescription(string bidno)
        {
            try
            {
                string sql = string.Format(@"select a.bid_no,a.bid_revision,b.bid_desc,c.* from ps_t_bid_selling as a inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision inner join ps_m_vendor as c on a.vendor_code = c.vendor_code  where a.bid_no = '{0}'", bidno);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    string desc;
                    this.txt_bidNo.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();

                    desc = dt.Rows[0]["bid_desc"].ToString().Replace("<div>", "");
                    desc = desc.Replace("</div>", "");
                    desc = desc.Replace("<p>", "");
                    desc = desc.Replace("</p>", "");
                    desc = desc.Replace("<br>", "");
                    desc = desc.Replace("</br>", "");

                    this.txt_bidDescription.Text = desc;

                    this.gv_bidsellingdescription.DataSource = dt;
                    this.gv_bidsellingdescription.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling.aspx';", true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    GetBidSellingDescription(Request.QueryString["bidno"]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling.aspx';", true);
                }
            }
        }

        protected void gv_bidsellingdescription_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "isDelete")
            {
                string sql = string.Format("delete from ps_t_bid_selling where  bid_no = '{0}' and vendor_code = '{1}'", this.txt_bidNo.Text, e.CommandArgument);
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    using (SqlCommand com = new SqlCommand(sql, conn))
                    {
                        com.ExecuteNonQuery();
                    }
                    conn.Close();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('data is deleted.'); window.location='BidSellingDescription?bidno=" + this.txt_bidNo.Text + "';", true);
            }
        }

        protected void gv_bidsellingdescription_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_bidsellingdescription.PageIndex = e.NewPageIndex;
            this.GetBidSellingDescription(Request.QueryString["bidno"]);
        }
    }
}