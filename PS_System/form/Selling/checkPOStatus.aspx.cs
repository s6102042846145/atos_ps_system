﻿using PS_Library;
using PS_Library.PO_Status;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class checkPOStatus: System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        string zContract_NodeID = ConfigurationManager.AppSettings["Contract_NodeID"].ToString();

        OTFunctions zot = new OTFunctions();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                ini_Data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        private void ini_Data()
        {
          
            //ini_bid_no();
        
        }
        //private void ini_bid_no()
        //{
        //    ddlBid.Items.Clear();
        //    string sql = "select distinct bid_no, bid_no as col2  from ps_t_bid_monitoring order by bid_no ";
        //    DataSet ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
        //    ddlBid.DataSource = ds.Tables[0];
        //    ddlBid.DataTextField = ds.Tables[0].Columns[0].ColumnName;
        //    ddlBid.DataValueField = ds.Tables[0].Columns[1].ColumnName;
        //    ddlBid.DataBind();
        //    ListItem li = new ListItem();
        //    li.Text = "-Select Bid No.-";
        //    li.Value = "";
        //    ddlBid.Items.Insert(0, li);
        //    if (hidBidNo.Value != "")
        //    {
        //        try { ddlBid.SelectedValue = hidBidNo.Value; } catch { ddlBid.SelectedIndex = 0; }
        //    }
        //    if (hidBidRevision.Value != "") { lblBidRevision.Text = hidBidRevision.Value; }
        //}
        //private void get_BidRevision()
        //{
        //    string sql = " select distinct bid_revision from ps_t_bid_selling where bid_no = '" + ddlBid.SelectedValue.ToString() + @"' " +
        //        " order by bid_revision desc ";
        //    var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {

        //        lblBidRevision.Text = ds.Tables[0].Rows[0]["bid_revision"].ToString();
        //        hidBidRevision.Value = lblBidRevision.Text;
        //    }
        //}
        //private void get_BidDesc()
        //{
        //    if (ddlBid.SelectedIndex > 0)
        //    {
        //        string sql = " select bid_revision, bid_desc from bid_no where bid_no = '" + ddlBid.SelectedValue.ToString() + @"' and status = 'Active' ";
        //        var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            txtBidDesc.Text = StripHTML(ds.Tables[0].Rows[0]["bid_desc"].ToString());
        //            get_BidRevision();
        //            hidBidNo.Value = ddlBid.SelectedValue.ToString();
        //            hidBidRevision.Value = lblBidRevision.Text.Trim();
        //        }
        //    }
        //}
      

        //protected void ddlBid_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    get_BidDesc();
        //}

        protected void btnCheckNewEquipCode_Click(object sender, EventArgs e)
        {

        }

        protected void btnUpdateFromSap_Click(object sender, EventArgs e)
        {
            string xcno = "";
            string xpo = "";
            if (txtContractNo.Text != "")
            {
                xcno = txtContractNo.Text.Trim();
               
            }
            if (txtPONum.Text != "")
            {
                xpo = txtPONum.Text;
            }
            try
            {
                if (xcno == "" && xpo == "")
                {
                    Response.Write("<script> alert('Please enter data.');</script>");

                }
                else
                {
                    SAP_PO_Status.Po_Status_SendToSap(xcno, xpo);
                    Response.Write("<script> alert('-Finished-');</script>");

                }
            }
            catch { }
          
        }
    }
}