﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="checkMat.aspx.cs" Inherits="PS_System.form.Selling.checkMat" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Check New Equipment</title>
      <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
       <div>
              <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
               
            </table>
        </div>
        <div class="div_95">
            <asp:Label ID="Label22" runat="server" CssClass="Text_400" Text="Check New Material"></asp:Label>
            <table>
                <tr>
                            <td>
                                <label class="Text_200">Bid No.: </label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlBid" runat="server" CssClass="ddl-md" AutoPostBack="True" OnSelectedIndexChanged="ddlBid_SelectedIndexChanged"></asp:DropDownList>
                                <asp:Label ID ="lblBidRevision" runat="server"/>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Bid Desc: </label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBidDesc" runat="server" CssClass="Text_500" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnCheckNewEquipCode"  CssClass="btn_normal_blue" runat="server" Text="Check New Mat." Width="160px" OnClick="btnCheckNewEquipCode_Click" />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
            </table>
       </div>
       <p>

       </p>
        <div id="divData" runat="server" class="div_95">
            
                         <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" 
                        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                        EmptyDataText="No data found." 
                        Font-Names="tahoma" Font-Size="9pt" Width="95%" OnRowCommand="gv1_OnRowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        <Columns>
                             <asp:TemplateField HeaderText="Select" Visible="false">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvSelect"  runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="60px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Schedule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvScheduleName" CssClass="Text_200" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="250px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="gvPart" CssClass="Text_100" runat="server" Text='<%# Bind("part") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="80px" />
                            </asp:TemplateField>
                              <asp:TemplateField HeaderText="Item No">
                                        <ItemTemplate>
                                            <asp:Label ID="gvItemNo" CssClass="Text_100" runat="server" Text='<%# Bind("item_no") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="110px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Equipment Code">
                                        <ItemTemplate>
                                            <asp:Label ID="gvEquipCode" CssClass="Text_100" runat="server" Text='<%# Bind("equip_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="120px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="gvDescription" CssClass="Text_100" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="220px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="SAP Mat.Code">
                                        <ItemTemplate>
                                            <asp:Label ID="gvMatCode" CssClass="Text_100" runat="server" Text='<%# Bind("sap_mat_code") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="220px" />
                            </asp:TemplateField>
                            </Columns>
                    </asp:GridView>
            
                         <br />
                         <asp:TextBox ID="txtNewEquipCode" CssClass="Text_200" runat="server"></asp:TextBox>
            
        &nbsp;<asp:Button ID="btnCreateMat"  CssClass="btn_normal_blue" runat="server" Text="Create" Width="160px" OnClick="btnCreateMat_Click"  />
            
        </div>
                <asp:HiddenField ID="hidBidNo" runat="server" />
                    <asp:HiddenField ID="hidBidRevision" runat="server" />
                    <asp:HiddenField ID="hidLogin" runat="server" />
                    <asp:HiddenField ID="hidMode" runat="server" />
        
    </form>
</body>
</html>
