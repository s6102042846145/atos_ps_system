﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="PriceScheduleFileUp.aspx.cs" Inherits="PS_System.form.Selling.PriceScheduleFuleUp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container mb-4 mt-4">
                <div class="row mb-4">
                    <h5>EGAT - Price Schedule Upload : Digital File Validation</h5>
                    <div class="ml-auto">
                        <h5 runat="server" id="h_bidnumberTitle" class="text-primary"></h5>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header  d-flex justify-content-between align-items-center">
                        รายละเอียดข้อมูลเอกสารประกวดราคา
                                <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Click to expand"><i class="bi bi-justify"></i></a>
                    </div>
                    <div class="card-body">
                        <div class="collapse" id="collapseExample">
                            <div class="form-row">
                                <div class="form-group col-sm-8">
                                    <label>Bid No.</label>
                                    <asp:Label ID="lb_bidnumber" runat="server" CssClass="form-control"></asp:Label>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Bid Version.</label>
                                    <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Bid Description.</label>
                                    <asp:Label ID="txt_bidDescription" runat="server" Height="150" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header">
                        รายละเอียดการอัปโหลดเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="form-row mb-4">
                            <div class="form-group col-4">
                                <label>Bidder type.</label>
                                <asp:DropDownList runat="server" ID="ddl_bidderType" CssClass="form-control">
                                    <asp:ListItem Text="Single Entity" Value="Single Entity"></asp:ListItem>
                                    <asp:ListItem Text="Joint Venture" Value="Joint Venture"></asp:ListItem>
                                    <asp:ListItem Text="Consortium" Value="Consortium"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-8">
                                <label>Bidder</label>
                                <div class="input-group">
                                    <asp:DropDownList runat="server" ID="ddl_vendor" CssClass="form-control"></asp:DropDownList>
                                    <div class="input-group-append">
                                        <asp:LinkButton runat="server" ID="btn_bidderAdd" CssClass="btn btn-primary" Text='<i class="bi bi-plus-circle-dotted mr-2"></i>Add' OnClick="btn_bidderAdd_Click"></asp:LinkButton>

                                    </div>
                                </div>

                            </div>
                        </div>

                        <div class="card-title">
                            <h6 class="text-muted">Price Schedule File Upload</h6>
                        </div>

                        <asp:Panel runat="server" ID="pn_form1" Enabled="false">
                            <div class="form-row mb-4">
                                <div class="form-group col-4">
                                    <label>Bidder type.</label>
                                    <asp:DropDownList runat="server" ID="ddl_bidderType2" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-group col-4">
                                    <label>Bid Vendor.</label>
                                    <asp:DropDownList runat="server" ID="ddl_bidVendor" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-group col-4">
                                    <label>Schedule No.</label>
                                    <asp:DropDownList runat="server" ID="ddl_refSchedule" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-group col-6">
                                    <label>Doc Type.</label>
                                    <asp:DropDownList runat="server" ID="ddl_docType" CssClass="form-control"></asp:DropDownList>
                                </div>
                                <div class="form-group col-6">
                                    <label>Digital File.</label>
                                    <div class="input-group">
                                        <asp:FileUpload runat="server" ID="fup_sinjoi" CssClass="btn btn-sm form-control" AllowMultiple="true" />
                                        <div class="input-group-append">
                                            <asp:LinkButton runat="server" ID="btn_digitalFileUpload" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-upload mr-2"></i>Upload' OnClick="DigitalFileUpload_Click"></asp:LinkButton>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="alert alert-danger" role="alert" runat="server" id="alert_upload" visible="false">
                                ระบบกำลังทำการอัปโหลดเอกสาร... โปรดอย่างปิดบราวเซอร์ โปรดรอสักครู่...
                            </div>
                            <div class="table-responsive-sm">
                                <asp:GridView runat="server" ID="gv_digitalFile" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv_digitalFile_RowCommand">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderStyle CssClass="small" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label runat="server"><%# Container.DataItemIndex + 1 %></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="vendor_type" HeaderText="Type" />
                                        <asp:BoundField DataField="vendor_name" HeaderText="Vendor" />
                                        <asp:BoundField DataField="doc_name" HeaderText="Digital File" />
                                        <asp:BoundField DataField="doctype_name" HeaderText="Document Type" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="File Status">
                                            <ItemTemplate>
                                                <div class="alert alert-success" role="alert">
                                                    File saved.
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="btn_download" Text='<i class="bi bi-arrow-bar-down"></i>' CssClass="btn btn-primary btn-sm" ToolTip="click to download" CommandName="IsDownload" CommandArgument='<%#Eval("doc_node_id") %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:LinkButton runat="server" ID="btn_delete" Text='<i class="bi bi-x-circle"></i>' CssClass="btn btn-danger btn-sm" ToolTip="click to delete" OnClientClick="if (!confirm('Do you want to delete?')) return false;" CommandName="IsDelete" CommandArgument='<%#string.Format("{0};{1}",Eval("row_id"),Eval("doc_node_id"))  %>'></asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                            <div class="table-responsive-sm">
                                <asp:GridView runat="server" ID="gv_invalidFileUp" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv_digitalFile_RowCommand">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderStyle CssClass="small" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label runat="server"><%# Container.DataItemIndex + 1 %></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="vendor_type" HeaderText="Type" />
                                        <asp:BoundField DataField="vendor_name" HeaderText="Vendor" />
                                        <asp:BoundField DataField="doc_name" HeaderText="Digital File" />
                                        <asp:BoundField DataField="doctype_name" HeaderText="Document Type" />
                                        <asp:TemplateField HeaderStyle-HorizontalAlign="Center" HeaderText="File Status">
                                            <ItemTemplate>
                                                <div class="alert alert-danger" role="alert">
                                                    Invalid file.
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </asp:Panel>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="PriceScheduleUploadData.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btn_digitalFileUpload" />
            <asp:PostBackTrigger ControlID="gv_digitalFile" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
