﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidTabulationData : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void getBidWinner(string bidnumber)
        {
            try
            {
                string sql = "select a.*, b.vendor_name, b.vendor_type ";
                sql += " from ps_t_bid_selling_winner  as a left join ps_t_bid_selling_vendor as b on a.vendor_code = b.vendor_code and a.winner_bidder_id = b.bidder_id ";
                sql += string.Format(" where a.bid_no like '%{0}%'", bidnumber);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                this.gv_bidselling.DataSource = dt;
                this.gv_bidselling.DataBind();


                if (dt.Rows.Count > 0)
                {
                    this.alert_datanotfound.Visible = false;
                }
                else
                {
                    this.alert_datanotfound.Visible = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                getBidWinner(string.Empty);
            }
        }

        protected void btn_bidnosearch_Click(object sender, EventArgs e)
        {
            getBidWinner(this.txt_bidnosearch.Text);
        }

        protected void gv_bidselling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_bidselling.PageIndex = e.NewPageIndex;
            getBidWinner(this.txt_bidnosearch.Text);
        }

        protected void gv_bidselling_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "IsDelete")
            {
                string sql = string.Format("delete from ps_t_bid_selling_winner where row_id = {0}", e.CommandArgument);
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    using (SqlCommand com = new SqlCommand(sql, conn))
                    {
                        com.ExecuteNonQuery();
                    }
                    conn.Close();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message",
                    string.Format("alert('data is deleted.'); window.location='BidTabulationData.aspx';"), true);
            }
            else if (e.CommandName == "IsUpload")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');
                Response.Redirect(string.Format("PriceScheduleBidWinner.aspx?bidno={0}&refschedule={1}&bidderid={2}&bidrevision={3}", arg[0], arg[1], arg[2], arg[3]));
            }
        }
    }
}