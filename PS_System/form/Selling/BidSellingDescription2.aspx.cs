﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidSellingDescription2 : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        private void GetBidSellingDescription(string bidno, string bidrevision)
        {
            try
            {
                if (GetBidSelling(bidno, bidrevision))
                {
                    string sql = string.Format(@"select a.bid_no,a.bid_revision,c.* from ps_t_bid_selling as a inner join  ps_m_vendor as c on a.vendor_code = c.vendor_code where a.bid_no = '{0}' and a.bid_revision = {1}", bidno, bidrevision);
                    var dt = zdb.ExecSql_DataTable(sql, zconn);

                    if (dt.Rows.Count > 0)
                    {
                        this.gv_bidsellingdescription.DataSource = dt;
                        this.gv_bidsellingdescription.DataBind();

                        this.alert_datanotfound.Visible = false;
                    }
                    else
                    {
                        this.alert_datanotfound.Visible = true;
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling2.aspx';", true);

                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private bool GetBidSelling(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format(@"select a.bid_no,a.bid_revision,b.bid_desc from ps_t_bid_monitoring as a inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision where a.bid_no = '{0}' and a.bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    //string desc;
                    this.txt_bidNo.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();

                    //desc = dt.Rows[0]["bid_desc"].ToString().Replace("<div>", "");
                    //desc = desc.Replace("</div>", "");
                    //desc = desc.Replace("<p>", "");
                    //desc = desc.Replace("</p>", "");
                    //desc = desc.Replace("<br>", "");
                    //desc = desc.Replace("</br>", "");
                    //desc = desc.Replace("&nbsp;", "");

                    //this.txt_bidDescription.Text = desc;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private void BindVendorData(string stringsearch)
        {
            try
            {
                string sql = string.Format("select * from ps_m_vendor where vendor_code like '%{0}%' or vendor_name like '%{0}%' or egp_no like '%{0}%' or sap_vendor_code like '%{0}%'", stringsearch);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                this.gv_vendor.DataSource = dt;
                this.gv_vendor.DataBind();

                if (dt.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('data not found.');", true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    GetBidSellingDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling2.aspx';", true);
                }
            }
        }

        protected void gv_bidsellingdescription_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "isDelete")
            {
                string sql = string.Format("delete from ps_t_bid_selling where  bid_no = '{0}' and vendor_code = '{1}' and bid_revision = {2}", this.txt_bidNo.Text, e.CommandArgument, this.txt_bidVersion.Text);
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    using (SqlCommand com = new SqlCommand(sql, conn))
                    {
                        com.ExecuteNonQuery();
                    }
                    conn.Close();
                }

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message",
                    string.Format("alert('data is deleted.'); window.location='BidSellingDescription2?bidno={0}&bidrevision={1}';", this.txt_bidNo.Text, this.txt_bidVersion.Text), true);
            }
        }

        protected void gv_bidsellingdescription_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_bidsellingdescription.PageIndex = e.NewPageIndex;
            this.GetBidSellingDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Request.QueryString["bidno"] != null)
            {
                Response.Redirect(string.Format("BidSellingAdd2.aspx?bidno={0}&bidrevision={1}", this.txt_bidNo.Text, this.txt_bidVersion.Text));
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling2.aspx';", true);
            }
        }
        protected void btn_vendorsearch_Click(object sender, EventArgs e)
        {
            if(this.txt_vendorsearch.Text=="")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter your text to search.');", true);
                this.txt_vendorsearch.Focus();
            }
            else
            {
                BindVendorData(this.txt_vendorsearch.Text);
            }
        }

        protected void gv_vendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if(e.CommandName == "isAdd")
            {
                string[] arg = e.CommandArgument.ToString().Split(';');
                try
                {
                    string sql = string.Format("select * from ps_t_bid_selling where  bid_no = '{0}' and vendor_code = '{1}' and bid_revision = {2}", this.txt_bidNo.Text, arg[0], this.txt_bidVersion.Text);
                    var dt = zdb.ExecSql_DataTable(sql, zconn);

                    if (dt.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('duplicate vendor. please try again.');", true);
                    }
                    else
                    {
                        using (SqlConnection conn = new SqlConnection(zconn))
                        {
                            conn.Open();
                            using (SqlCommand cmd = new SqlCommand("sp_ins_selling2", conn))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@bid_no", this.txt_bidNo.Text);
                                cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));
                                cmd.Parameters.AddWithValue("@vendor_code", arg[0]);
                                cmd.Parameters.AddWithValue("@vendor_name", arg[1]);
                                cmd.Parameters.AddWithValue("@bidder_status", "selling");

                                cmd.ExecuteNonQuery();
                                conn.Close();

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                                    string.Format("alert('has been successfully saved.'); window.location='BidSellingDescription2?bidno={0}&bidrevision={1}';", this.txt_bidNo.Text, this.txt_bidVersion.Text), true);
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw ex;
                }

            }
        }
    }
}