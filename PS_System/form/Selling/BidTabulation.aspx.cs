﻿using System;
using PS_Library;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidTabulation : System.Web.UI.Page
    {
        clsBidTabulation objTab = new clsBidTabulation();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z597204";

                tbShowDetail.Style["display"] = "none";
                dvButton.Style["display"] = "none";
                //dvNodata.Style["display"] = "none";
                //dvNodataVen1.Style["display"] = "none"; 
                //dvNodataVen2.Style["display"] = "none";
                //dvNodataVen3.Style["display"] = "none";
                dvComScore.Style["display"] = "none";
                txtTechScore.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                txtPriScore.Attributes.Add("onkeypress", "javascript:return NumOnlyChk(event)");
                ddlContiam1.Visible = false;
                ddlContiam2.Visible = false;
                ddlContiam3.Visible = false;
                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;
                bindDDL();

            }
        }
        private void chkWhoComment()
        {
            DataTable dtComment = objTab.getWhoComment(hidLogin.Value.Replace("z",""));
            if (dtComment.Rows.Count > 0) dvComScore.Style["display"] = "block";
            else dvComScore.Style["display"] = "none";
        }
        private void BindDataComment()
        {
            DataTable dtallComment = objTab.getAllComment();
            if (dtallComment.Rows.Count > 0) 
            { 
                gvComScore.DataSource = dtallComment;
                gvComScore.DataBind();
            }
     
        }
        private void bindDDL()
        {
            ddlBidno.Items.Clear();
            DataTable dt = objTab.getDDL("bidno");
            foreach (DataRow dr in dt.Rows)
            {
                ListItem item1 = new ListItem();
                item1.Text = dr["bid_no"].ToString() + " ( Rev. " + dr["bid_revision"].ToString() + " )";
                item1.Value = dr["bid_no"].ToString() +"|" + dr["bid_revision"].ToString();
                ddlBidno.Items.Add(item1);
            }
            ddlBidno.DataBind();

            ddlSch.Items.Clear();
            DataTable dt2 = objTab.getDDL("");
            DataTable dtsch = dt2.DefaultView.ToTable(true, "schedule_name");
            DataTable dtpart = dt2.DefaultView.ToTable(true, "part");
            if (dtsch.Rows.Count > 0)
            {
                ddlSch.DataSource = dtsch;
                ddlSch.DataTextField = "schedule_name";
                ddlSch.DataValueField = "schedule_name";
                ddlSch.DataBind();
            }

            if (dtpart.Rows.Count > 0)
            {
                ddlPart.Items.Clear();
                ddlPart.DataSource = dtpart;
                ddlPart.DataTextField = "part";
                ddlPart.DataValueField = "part";
                ddlPart.DataBind();
            }

            DataTable dtVen = objTab.getDDLVender();
            DataView dvVentype = new DataView(dtVen);
            if (dvVentype.Count > 0) dvVentype.RowFilter = " vendor_type <>'Vendor'";

            ddlVen.Items.Clear();
            foreach (DataRowView drv in dvVentype)
            {
                ListItem item2 = new ListItem();
                item2.Text = drv["vendor_name"].ToString();
                item2.Value = drv["vendor_code"].ToString() + "|" + drv["bidder_id"].ToString();
                ddlVen.Items.Add(item2);
            }
            ddlVen.DataBind();
            //if (dvVentype.Count > 0)
            //{
            //    ddlVen.Items.Clear();
            //    ddlVen.DataSource = dvVentype;
            //    ddlVen.DataTextField = "vendor_name";
            //    ddlVen.DataValueField = "vendor_code";
            //    ddlVen.DataBind();

            //}
            ddlCommentVen.Items.Clear();
            foreach (DataRowView dr in dvVentype)
            {
                ListItem item1 = new ListItem();
                item1.Text = dr["vendor_name"].ToString();
                item1.Value = dr["vendor_code"].ToString() + "|" + dr["bidder_id"].ToString();
                ddlCommentVen.Items.Add(item1);
            }
            ddlCommentVen.DataBind();

            ddlVen.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlCommentVen.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlBidno.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlSch.Items.Insert(0, new ListItem("<-- Select -->", ""));
            ddlPart.Items.Insert(0, new ListItem("<-- Select -->", ""));
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnFil_Click(object sender, EventArgs e)
        {
            int k = 0;
            bool chkselect = false;
            foreach (ListItem item in ddlVen.Items)
            {

                if (item.Selected)
                {
                    k++;
                    if (k>3) chkselect = true;

                }
            }
            if (chkselect == true)
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Not Selected Vender more than 3 Options.');", true);
            }
            else
            {
                if (ddlBidno.SelectedValue != "" && ddlSch.SelectedValue != "")
                {
                    chkWhoComment();
                    BindDataComment();
                    tbShowDetail.Style["display"] = "block";
                    dvButton.Style["display"] = "block";
                    lblReqBid.Visible = false;
                    lblReqSch.Visible = false;
                    string rowfilter = "";
                    string bidno = "";
                    string rev = "";
                    if (ddlBidno.SelectedValue != "")
                    {
                        string[] sp = ddlBidno.SelectedValue.Split('|');
                        if (sp.Length > 1)
                        {
                            bidno = sp[0];
                            rev = sp[1];
                        }
                    }

                    rowfilter = string.Format("(xt.bid_no = '{0}')", bidno);

                    if (rowfilter == "") rowfilter = string.Format("(xt.schedule_name = '{0}')", ddlSch.SelectedValue);
                    else rowfilter = string.Format("{0} and xt.schedule_name = '{1}'", rowfilter, ddlSch.SelectedValue);

                    if (ddlPart.SelectedValue != "")
                    {
                        if (rowfilter == "") rowfilter = string.Format("(xt.part = '{0}')", ddlPart.SelectedValue);
                        else rowfilter = string.Format("{0} and xt.part = '{1}'", rowfilter, ddlPart.SelectedValue);
                    }
                    string strid = "";
                    string tempid = "";
                  
                    strid = objTab.getTemplateID(bidno, rev, ddlSch.SelectedValue);
                    tempid += (tempid == "" ? "" : ",") + strid;
                    hidTempid.Value = tempid;
                    DataTable dtrowfil = objTab.getDataXls(rowfilter, tempid);
                    DataTable dtvenfil = objTab.getDataVender(rowfilter, tempid);
                    DataView dvVen = new DataView(dtvenfil);
                    bool chk = false;
                    bool chk2 = false;
                    bool chk3 = false;
                    foreach (ListItem item in ddlVen.Items)
                    {

                        if (item.Selected)   
                        {
                            string vcode = "";
                            string bidder = "";
                            string[] spVen = item.Value.Split('|');
                            if (spVen.Length > 1)
                            {
                                vcode = spVen[0];
                                bidder = spVen[1];
                            }
                            DataTable dtChkCon = objTab.getDDLVender2(bidder);
                            
                            if (chk2 == true)
                            {
                                dvVen.RowFilter = "vendor_code ='" + vcode + "'";
                                if (dvVen.Count > 0)
                                {
                                    if (dtChkCon.Rows.Count > 1)
                                    {
                                        lblContiam3.Text = item.Text;
                                        ddlContiam3.Items.Clear();
                                        foreach (DataRow dr in dtChkCon.Rows)
                                        {
                                            ListItem item1 = new ListItem();
                                            item1.Text = dr["vendor_name"].ToString();
                                            item1.Value = dr["vendor_code"].ToString() + "|" + dr["bidder_id"].ToString();
                                            ddlContiam3.Items.Add(item1);
                                        }
                                        ddlContiam3.DataBind();
                                        ddlContiam3.Items.Insert(0, new ListItem("<-- Select -->", ""));
                                        lblContiam3.Visible = true;
                                        ddlContiam3.Visible = true;
                                    }
                                    else
                                    {
                                        lblContiam3.Visible = false;
                                        ddlContiam3.Visible = false;
                                    }
                                    //dvNodataVen3.Style["display"] = "none";
                                    dvmainVen3.Style["display"] = "table-cell";
                                    gvVen3.DataSource = dvVen;
                                    gvVen3.DataBind();
                                    chk3 = true;
                                    dvNodataVen3.Style["display"] = "none";
                                }
                                else
                                {
                                    lblContiam3.Visible = false;
                                    ddlContiam3.Visible = false;
                                    //dvNodataVen3.Style["display"] = "table-cell";
                                    dvmainVen3.Style["display"] = "none";
                                    //dvLineVen3.Style["display"] = "none";
                                }
                                break;
                            }
                            else
                            {
                                dvmainVen3.Style["display"] = "none";
                                //dvLineVen3.Style["display"] = "none";
                            }
                            if (chk == true && chk2 == false)
                            {
                                dvVen.RowFilter = "vendor_code ='" + vcode + "'";
                                if (dvVen.Count > 0)
                                {
                                    if (dtChkCon.Rows.Count > 1)
                                    {
                                        lblContiam2.Text = item.Text;
                                        ddlContiam2.Items.Clear();
                                        foreach (DataRow dr in dtChkCon.Rows)
                                        {
                                            ListItem item1 = new ListItem();
                                            item1.Text = dr["vendor_name"].ToString();
                                            item1.Value = dr["vendor_code"].ToString() + "|" + dr["bidder_id"].ToString();
                                            ddlContiam2.Items.Add(item1);
                                        }
                                        ddlContiam2.DataBind();
                                        ddlContiam2.Items.Insert(0, new ListItem("<-- Select -->", ""));
                                        lblContiam2.Visible = true;
                                        ddlContiam2.Visible = true;
                                       
                                    }
                                    else
                                    {
                                        lblContiam2.Visible = false;
                                        ddlContiam2.Visible = false;
                                    }
                                    //dvNodataVen2.Style["display"] = "none";
                                    dvmainVen2.Style["display"] = "table-cell";
                                    gvVen2.DataSource = dvVen;
                                    gvVen2.DataBind();
                                    chk2 = true;
                                    dvNodataVen2.Style["display"] = "none";
                                }
                                else
                                {
         
                                    //dvNodataVen2.Style["display"] = "table-cell";
                                    dvmainVen2.Style["display"] = "none";
                                    //dvLineVen2.Style["display"] = "none";
                                }

                            }
                            else
                            {
                                lblContiam2.Visible = false;
                                ddlContiam2.Visible = false;
                                //dvNodataVen2.Style["display"] = "table-cell";
                                dvmainVen2.Style["display"] = "none";
                                //dvLineVen2.Style["display"] = "none";
                            }
                            if (chk == false)
                            {
                                dvVen.RowFilter = "vendor_code ='" + vcode + "'";
                                if (dvVen.Count > 0)
                                {
                                    if (dtChkCon.Rows.Count > 1)
                                    {
                                        lblContiam1.Text = item.Text;
                                        ddlContiam1.Items.Clear();
                                        foreach (DataRow dr in dtChkCon.Rows)
                                        {
                                            ListItem item1 = new ListItem();
                                            item1.Text = dr["vendor_name"].ToString();
                                            item1.Value = dr["vendor_code"].ToString() + "|" + dr["bidder_id"].ToString();
                                            ddlContiam1.Items.Add(item1);
                                        }
                                        ddlContiam1.DataBind();
                                        ddlContiam1.Items.Insert(0, new ListItem("<-- Select -->", ""));
                                        lblContiam1.Visible = true;
                                        ddlContiam1.Visible = true;
                                    }
                                    else
                                    {
                                        lblContiam1.Visible = false;
                                        ddlContiam1.Visible = false;
                                    }
                                    dvNodataVen1.Style["display"] = "none";
                                    dvmainVen1.Style["display"] = "table-cell";
                                    gvVen1.DataSource = dvVen;
                                    gvVen1.DataBind();
                                    chk = true;
                                }
                                else
                                {
                                    dvNodataVen1.Style["display"] = "table-cell";
                                    dvmainVen1.Style["display"] = "none";
                                    //dvLineVen2.Style["display"] = "none";
                                }
                            }
                        }
                    }
                    if (chk2 == false)
                    {
                        dvNodataVen2.Style["display"] = "table-cell";
                        dvNodataVen3.Style["display"] = "table-cell";
                     
                        lblContiam2.Visible = false;
                        ddlContiam2.Visible = false;
                        lblContiam3.Visible = false;
                        ddlContiam3.Visible = false;
                    }
                    if (chk == true && chk3 == false)
                    {
                        dvNodataVen3.Style["display"] = "table-cell";
                        lblContiam3.Visible = false;
                        ddlContiam3.Visible = false;
                    }
                    if (ddlVen.SelectedValue != "")
                    {
                        //dvNodata.Style["display"] = "none";
                        dvAllVender.Style["display"] = "block";
                    }
                    else
                    {
                        ddlContiam1.Visible = false;
                        ddlContiam2.Visible = false;
                        ddlContiam3.Visible = false;
                        lblContiam1.Visible = false;
                        lblContiam2.Visible = false;
                        lblContiam3.Visible = false;

                        //dvNodata.Style["display"] = "block";
                        dvAllVender.Style["display"] = "none";
                    }


                    if (dtrowfil.Rows.Count > 0)
                    {
                        gvXls.DataSource = dtrowfil;
                        gvXls.DataBind();
                        dvButton.Style["display"] = "block";
                        dvScroll.Attributes.Add("style", "width:100%;overflow:scroll;height:400px;");
                    }
                    else
                    {
                        gvXls.DataSource = null;
                        gvXls.DataBind();
                        dvButton.Style["display"] = "none";
                        dvScroll.Attributes.Add("style", "width:100%;overflow:hidden;height:400px;");
                    }

                }
                else
                {
                    tbShowDetail.Style["display"] = "none";
                    dvButton.Style["display"] = "none";
                    lblReqBid.Visible = true;
                    lblReqSch.Visible = true;
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select Bid No. and Schedule Name.');", true);
                }
                if (gvComScore.Rows.Count == 0) dvButton.Style["display"] = "none";
                else dvButton.Style["display"] = "block";
            }
   
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            foreach (GridViewRow gvr in gvComScore.Rows)
            {
                string bidno = ((Label)gvr.FindControl("lblbidno")).Text;
                string rev = ((Label)gvr.FindControl("lblrev")).Text;
                string sche = ((Label)gvr.FindControl("lblsch")).Text;
                string bidid = ((Label)gvr.FindControl("lblbidid")).Text;
                string vencode = ((Label)gvr.FindControl("lblvencode")).Text;
                string techsc = ((Label)gvr.FindControl("lblTecSc")).Text;
                string prisc = ((Label)gvr.FindControl("lblPriceSc")).Text;
                string commemt = ((Label)gvr.FindControl("lblComment")).Text;
                string empid = ((Label)gvr.FindControl("lblComby")).Text;
                objTab.insertComment(bidno, rev, empid, sche, bidid, vencode, techsc, prisc, commemt);
            }
        
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully');", true);
        }

        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string bidno = "";
            string rev = "";
            if (ddlBidno.SelectedValue != "")
            {
                string[] sp = ddlBidno.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
            string vencode = "";
            string bidid = "";
            if (ddlCommentVen.SelectedValue != "")
            {
                string[] sp = ddlCommentVen.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    vencode = sp[0];
                    bidid = sp[1];
                }
            }
            if (ddlCommentVen.SelectedValue != "" && txtPriScore.Text != "" && txtTechScore.Text != "")
            {
                DataTable dtallComment = objTab.getAllComment();
                if (dtallComment.Rows.Count > 0)
                {
                    string filter = "committee_emp_id='" + hidLogin.Value + "' and vendor_code='" + vencode + "' and bidder_id ='" + bidid + "'";
                    DataRow[] drSelect = dtallComment.Select(filter);
                    if (drSelect.Length == 0)
                    {
                        DataRow dr = dtallComment.NewRow();
                        dr["bid_no"] = bidno;
                        dr["bid_revision"] = rev;
                        dr["schedule_name"] = ddlSch.SelectedValue;
                        dr["bidder_id"] = bidid;
                        dr["vendor_code"] = vencode;
                        dr["vendor_name"] = ddlCommentVen.SelectedItem.Text;
                        dr["technical_score"] = txtTechScore.Text;
                        dr["price_score"] = txtPriScore.Text;
                        dr["comment"] = txtComment.Text;
                        dr["committee_emp_id"] = hidLogin.Value;
                        dtallComment.Rows.Add(dr);
                    }
                    else
                    {
                        drSelect[0]["technical_score"] = txtTechScore.Text;
                        drSelect[0]["price_score"] = txtPriScore.Text;
                        drSelect[0]["comment"] = txtComment.Text;
                        drSelect[0]["committee_emp_id"] = hidLogin.Value;

                    }

                    gvComScore.DataSource = dtallComment;
                    gvComScore.DataBind();

                }
                else
                {
                    DataTable dtAdd = new DataTable();
                    dtAdd.Columns.Add("bid_no");
                    dtAdd.Columns.Add("bid_revision");
                    dtAdd.Columns.Add("schedule_name");
                    dtAdd.Columns.Add("bidder_id");
                    dtAdd.Columns.Add("vendor_code");
                    dtAdd.Columns.Add("vendor_name");
                    dtAdd.Columns.Add("technical_score");
                    dtAdd.Columns.Add("price_score");
                    dtAdd.Columns.Add("comment");
                    dtAdd.Columns.Add("committee_emp_id");
                    dtAdd.Columns.Add("row_id");
                    DataRow dr = dtAdd.NewRow();
                    dr["bid_no"] = bidno;
                    dr["bid_revision"] = rev;
                    dr["schedule_name"] = ddlSch.SelectedValue;
                    dr["bidder_id"] = bidid;
                    dr["vendor_code"] = vencode;
                    dr["vendor_name"] = ddlCommentVen.SelectedItem.Text;
                    dr["technical_score"] = txtTechScore.Text;
                    dr["price_score"] = txtPriScore.Text;
                    dr["comment"] = txtComment.Text;
                    dr["committee_emp_id"] = hidLogin.Value;
                    dr["row_id"] = "";

                    dtAdd.Rows.Add(dr);
                    gvComScore.DataSource = dtAdd;
                    gvComScore.DataBind();
                }
                if (gvComScore.Rows.Count == 0) dvButton.Style["display"] = "none";
                else dvButton.Style["display"] = "block";
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Add Data Successfully.');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Data.');", true);
            }

        }

        protected void ddlContiam1_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rowfilter = "";
            string bidno = "";
            string rev = "";
            if (ddlBidno.SelectedValue != "")
            {
                string[] sp = ddlBidno.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
            string vcode = "";
            string bidder = "";
            if (ddlContiam1.SelectedValue != "")
            {
                string[] spVen = ddlContiam1.SelectedValue.Split('|');
                if (spVen.Length > 1)
                {
                    vcode = spVen[0];
                    bidder = spVen[1];
                }
            }
            rowfilter = string.Format("(xt.bid_no = '{0}')", bidno);

            if (rowfilter == "") rowfilter = string.Format("(xt.schedule_name = '{0}')", ddlSch.SelectedValue);
            else rowfilter = string.Format("{0} and xt.schedule_name = '{1}'", rowfilter, ddlSch.SelectedValue);

            if (ddlPart.SelectedValue != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(xt.part = '{0}')", ddlPart.SelectedValue);
                else rowfilter = string.Format("{0} and xt.part = '{1}'", rowfilter, ddlPart.SelectedValue);
            }

            if (rowfilter == "") rowfilter = string.Format("(xt.vendor_code = '{0}')", vcode);
            else rowfilter = string.Format("{0} and xt.vendor_code = '{1}'", rowfilter, vcode);

            DataTable dtvenfil = objTab.getDataVender(rowfilter, hidTempid.Value);
            if (dtvenfil.Rows.Count > 0)
            {
                gvVen1.DataSource = dtvenfil;
                gvVen1.DataBind();
            }
            else
            {
                gvVen1.DataSource = null;
                gvVen1.DataBind();
            }
            
        }

        protected void ddlContiam2_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rowfilter = "";
            string bidno = "";
            string rev = "";
            if (ddlBidno.SelectedValue != "")
            {
                string[] sp = ddlBidno.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
            string vcode = "";
            string bidder = "";
            if (ddlContiam2.SelectedValue != "")
            {
                string[] spVen = ddlContiam2.SelectedValue.Split('|');
                if (spVen.Length > 1)
                {
                    vcode = spVen[0];
                    bidder = spVen[1];
                }
            }
            rowfilter = string.Format("(xt.bid_no = '{0}')", bidno);

            if (rowfilter == "") rowfilter = string.Format("(xt.schedule_name = '{0}')", ddlSch.SelectedValue);
            else rowfilter = string.Format("{0} and xt.schedule_name = '{1}'", rowfilter, ddlSch.SelectedValue);

            if (ddlPart.SelectedValue != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(xt.part = '{0}')", ddlPart.SelectedValue);
                else rowfilter = string.Format("{0} and xt.part = '{1}'", rowfilter, ddlPart.SelectedValue);
            }

            if (rowfilter == "") rowfilter = string.Format("(xt.vendor_code = '{0}')", vcode);
            else rowfilter = string.Format("{0} and xt.vendor_code = '{1}'", rowfilter, vcode);

            DataTable dtvenfil = objTab.getDataVender(rowfilter, hidTempid.Value);
            if (dtvenfil.Rows.Count > 0)
            {
                gvVen2.DataSource = dtvenfil;
                gvVen2.DataBind();
            }
            else
            {
                gvVen2.DataSource = null;
                gvVen2.DataBind();
            }
        }

        protected void ddlContiam3_SelectedIndexChanged(object sender, EventArgs e)
        {
            string rowfilter = "";
            string bidno = "";
            string rev = "";
            if (ddlBidno.SelectedValue != "")
            {
                string[] sp = ddlBidno.SelectedValue.Split('|');
                if (sp.Length > 1)
                {
                    bidno = sp[0];
                    rev = sp[1];
                }
            }
            string vcode = "";
            string bidder = "";
            if (ddlContiam3.SelectedValue != "")
            {
                string[] spVen = ddlContiam3.SelectedValue.Split('|');
                if (spVen.Length > 1)
                {
                    vcode = spVen[0];
                    bidder = spVen[1];
                }
            }
            rowfilter = string.Format("(xt.bid_no = '{0}')", bidno);

            if (rowfilter == "") rowfilter = string.Format("(xt.schedule_name = '{0}')", ddlSch.SelectedValue);
            else rowfilter = string.Format("{0} and xt.schedule_name = '{1}'", rowfilter, ddlSch.SelectedValue);

            if (ddlPart.SelectedValue != "")
            {
                if (rowfilter == "") rowfilter = string.Format("(xt.part = '{0}')", ddlPart.SelectedValue);
                else rowfilter = string.Format("{0} and xt.part = '{1}'", rowfilter, ddlPart.SelectedValue);
            }

            if (rowfilter == "") rowfilter = string.Format("(xt.vendor_code = '{0}')", vcode);
            else rowfilter = string.Format("{0} and xt.vendor_code = '{1}'", rowfilter, vcode);

            DataTable dtvenfil = objTab.getDataVender(rowfilter, hidTempid.Value);
            if (dtvenfil.Rows.Count > 0)
            {
                gvVen3.DataSource = dtvenfil;
                gvVen3.DataBind();
            }
            else
            {
                gvVen3.DataSource = null;
                gvVen3.DataBind();
            }
        }

        protected void gvComScore_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "deletedata")
            {
           
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvComScore = (GridView)sender;

                string bidno = ((Label)gvComScore.Rows[xrowid].FindControl("lblbidno")).Text;
                string rowid = ((Label)gvComScore.Rows[xrowid].FindControl("lblrowid")).Text;
                string comby = ((Label)gvComScore.Rows[xrowid].FindControl("lblComby")).Text;
                string rev = ((Label)gvComScore.Rows[xrowid].FindControl("lblrev")).Text;

                objTab.deteleComment(bidno, rev, comby, rowid);
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully.');", true);
                BindDataComment();

            }
        }

      
    }
}