﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="BidTabulationData.aspx.cs" Inherits="PS_System.form.Selling.BidTabulationData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scriptMag" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePal" runat="server">
        <ContentTemplate>
            <div class="container mb-4">
                <div class="row mb-3">
                    <h5>EGAT - Bid Final</h5>
                    <div class="ml-auto">
                        <a href="BidTabulation.aspx" target="_blank" role="button" class="btn btn-primary btn-sm" hidden><i class="bi bi-plus mr-2"></i>Bid Tabulation</a>
                        <a href="BidTabulationAdd.aspx" role="button" class="btn btn-success btn-sm" hidden><i class="bi bi-plus mr-2"></i>Bid Winer</a>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header">
                        ตารางข้อมูลผู้ชนะการซื้อเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <asp:TextBox ID="txt_bidnosearch" runat="server" CssClass="form-control" MaxLength="50" placeholder="bid number search"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_bidnosearch" runat="server" Text='<i class="bi bi-search"></i>' ToolTip="Click to search." OnClick="btn_bidnosearch_Click" CausesValidation="false" CssClass="btn btn-info" />
                                </div>
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary" title="Click to refresh" onclick="window.location.reload();"><i class="bi bi-arrow-repeat"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_datanotfound">
                            Data not found.
                        </div>
                        <div class="table-responsive-sm">
                            <asp:GridView ID="gv_bidselling" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_bidselling_PageIndexChanging"
                                OnRowCommand="gv_bidselling_RowCommand" Font-Names="tahoma" Font-Size="9pt">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="bid_no" HeaderText="Bid Number" />
                                    <asp:BoundField DataField="bid_revision" HeaderText="Bid Revision" />
                                    <asp:BoundField DataField="schedule_name" HeaderText="Schedule Name" />
                                    <asp:BoundField DataField="technical_score" HeaderText="Technical Score" />
                                    <asp:BoundField DataField="price_score" HeaderText="Price Score" />
                                    <asp:BoundField DataField="vendor_name" HeaderText="Vendor Name" />
                                    <asp:BoundField DataField="vendor_type" HeaderText="Type" />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton runat="server" ID="btn_upload" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-upload"></i>' ToolTip="click to upload" CommandName="IsUpload"
                                                CommandArgument='<%#string.Format("{0};{1};{2};{3}",Eval("bid_no"),Eval("schedule_name"),Eval("winner_bidder_id"),Eval("bid_revision")) %>'></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="btn_info" CssClass="btn btn-info btn-sm" Text='<i class="bi bi-info-circle"></i>' ToolTip="click to infomation" CommandName="IsInfo" Visible="false"></asp:LinkButton>
                                            <asp:LinkButton runat="server" ID="btn_delete" CssClass="btn btn-danger btn-sm" Text='<i class="bi bi-x-circle"></i>' ToolTip="click to delete" CommandName="IsDelete" CommandArgument='<%#Eval("row_id") %>' OnClientClick="if (!confirm('Do you want to delete the winner this?')) return false;"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="Index.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
