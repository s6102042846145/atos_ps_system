﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="checkPOStatus.aspx.cs" Inherits="PS_System.form.Selling.checkPOStatus" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Check PO Status</title>
      <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
       <div>
              <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
               
            </table>
        </div>
        <div class="div_95">
            <asp:Label ID="Label22" runat="server" CssClass="Text_400" Text="Check PO Status"></asp:Label>
            <table>
                <tr>
                            <td>
                                <label class="Text_200">Contract No.: </label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtContractNo" runat="server" CssClass="Text_200" ></asp:TextBox>
                               
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">PO No: </label>
                            </td>
                            <td>
                                 <asp:TextBox ID="txtPONum" runat="server" CssClass="Text_200" ></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Button ID="btnUpdateFromSap"  CssClass="btn_normal_blue" runat="server" Text="Update from SAP" Width="160px" OnClick="btnUpdateFromSap_Click"  />
                            </td>
                            <td>
                                &nbsp;</td>
                            <td>
                                &nbsp;</td>
                        </tr>
            </table>
       </div>
        
                <asp:HiddenField ID="hidBidNo" runat="server" />
                    <asp:HiddenField ID="hidBidRevision" runat="server" />
                    <asp:HiddenField ID="hidLogin" runat="server" />
                    <asp:HiddenField ID="hidMode" runat="server" />
        
    </form>
</body>
</html>
