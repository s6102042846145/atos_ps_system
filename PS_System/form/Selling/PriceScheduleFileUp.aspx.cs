﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class PriceScheduleFuleUp : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        clsPriceUpload2 clsPriceUpload = new clsPriceUpload2();
        OTFunctions zot = new OTFunctions();

        cExcel excel = new cExcel();
        private void bindDescription(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format("select bid_no, bid_revision, bid_desc from bid_no where bid_no = '{0}' and bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.h_bidnumberTitle.InnerText = bidno;
                    this.lb_bidnumber.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindVendorData(string bitnumber, string bitrevision)
        {
            try
            {
                string sql = string.Format("select *, (a.bidder_id + ';' + a.vendor_code) as idRef from ps_t_bid_selling  as a left join ps_m_vendor as b on a.vendor_code = b.vendor_code where bid_no = '{0}' and bid_revision = {1}", bitnumber, bitrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_vendor.DataSource = dt;
                    this.ddl_vendor.DataTextField = "vendor_name";
                    this.ddl_vendor.DataValueField = "idRef";
                    this.ddl_vendor.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        bool insBidSelling(string bidderId, string refSchedule, string vendorCode, string vendorName, string vendorType, string fileName, string docType, int docRevision, string nodId)
        {
            try
            {
                SqlTransaction transaction;
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_ins_bid_selling_vendor", conn, transaction))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@bidder_id", bidderId);
                            cmd.Parameters.AddWithValue("@bid_no", this.lb_bidnumber.Text);
                            cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));
                            cmd.Parameters.AddWithValue("@ref_schedule_no", refSchedule);
                            cmd.Parameters.AddWithValue("@vendor_code", vendorCode);
                            cmd.Parameters.AddWithValue("@vendor_name", vendorName);
                            cmd.Parameters.AddWithValue("@vendor_type", vendorType);

                            cmd.ExecuteNonQuery();
                        }

                        using (SqlCommand cmd = new SqlCommand("sp_ins_bid_selling_fileupload", conn, transaction))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@bidder_id", bidderId);
                            cmd.Parameters.AddWithValue("@bid_no", this.lb_bidnumber.Text);
                            cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));
                            cmd.Parameters.AddWithValue("@ref_schedule_no", refSchedule);
                            cmd.Parameters.AddWithValue("@vendor_code", vendorCode);
                            cmd.Parameters.AddWithValue("@doc_type", docType);
                            cmd.Parameters.AddWithValue("@doc_name", fileName);
                            cmd.Parameters.AddWithValue("@doc_version", docRevision);
                            cmd.Parameters.AddWithValue("@parent_node_id", ConfigurationManager.AppSettings["WF_UPLOAD_PS"].ToString());
                            cmd.Parameters.AddWithValue("@doc_node_id", nodId);
                            cmd.Parameters.AddWithValue("@status_bid", "selling");

                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (SqlException sqlEx)
                    {
                        zot.deleteNodeID(nodId, nodId);
                        transaction.Rollback();
                        conn.Close();

                        return false;
                        throw sqlEx;
                    }

                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }
        void dspBidFileUpload(string bidderId)
        {
            try
            {
                //string sql = string.Format("select a.*, b.doctype_name, 'Success' as doc_status from ps_t_bid_selling_fileupload as a left join ps_m_doctype as b on a.doc_type = b.doctype_code where bidder_id = '{0}' and bid_no = '{1}' and vendor_code = '{2}'", bidderId, this.lb_bidnumber.Text, vendorCode);
                string sql = @"select a.*, b.doctype_name, c.vendor_name, c.vendor_type ";
                sql += @"from ps_t_bid_selling_fileupload as a left join ps_m_doctype as b on a.doc_type = b.doctype_code  ";
                sql += @"left join ps_t_bid_selling_vendor as c on a.vendor_code = c.vendor_code ";
                sql += string.Format("where a.bidder_id = '{0}' ", bidderId);

                var dt = zdb.ExecSql_DataTable(sql, zconn);

                this.gv_digitalFile.DataSource = dt;
                this.gv_digitalFile.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void BindVendorData()
        {
            try
            {
                string sql = string.Format("select * from ps_m_vendor");
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_bidVendor.DataSource = dt;
                    this.ddl_bidVendor.DataTextField = "vendor_name";
                    this.ddl_bidVendor.DataValueField = "vendor_code";
                    this.ddl_bidVendor.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void RefSchedule()
        {
            try
            {
                string sql = string.Format("select * from ps_m_bid_schedule where bid_no = '{0}' and bid_revision = {1}", this.lb_bidnumber.Text, this.txt_bidVersion.Text);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_refSchedule.DataSource = dt;
                    this.ddl_refSchedule.DataTextField = "schedule_name";
                    this.ddl_refSchedule.DataValueField = "schedule_name";
                    this.ddl_refSchedule.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    bindDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                    BindVendorData(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);

                    //this.ddl_vendor.Items.Insert(0, new ListItem("-- Select Vendor --"));
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                       string.Format("alert('data not found.'); window.location='PriceScheduleUploadData.aspx';"), true);
                }
            }
        }
        private string uploadDoc(string xbid_no, string xbid_revision, string xschedule_name, string xbidder_id, string xvendor_code, string xdoc_type, string xdoc_name, byte[] xcontent, bool final_upload)
        {
            var WF_NodeID = ConfigurationManager.AppSettings["WF_UPLOAD_PS"].ToString();
            var xbid_nodeid = zot.getNodeByName(WF_NodeID, xbid_no);
            if (xbid_nodeid == null)
            {
                xbid_nodeid = zot.createFolder(WF_NodeID, xbid_no);
            }
            var xschedule_nodeid = zot.getNodeByName(xbid_nodeid.ID.ToString(), xschedule_name);
            if (xschedule_nodeid == null)
            {
                xschedule_nodeid = zot.createFolder(xbid_nodeid.ID.ToString(), xschedule_name);
            }

            var xbidder_nodeid = zot.getNodeByName(xschedule_nodeid.ID.ToString(), xbidder_id);
            if (xbidder_nodeid == null)
            {
                xbidder_nodeid = zot.createFolder(xschedule_nodeid.ID.ToString(), xbidder_id);
            }

            var xvendor_nodeid = zot.getNodeByName(xbidder_nodeid.ID.ToString(), xvendor_code);
            if (xvendor_nodeid == null)
            {
                xvendor_nodeid = zot.createFolder(xbidder_nodeid.ID.ToString(), xvendor_code);
            }

            var xdoctype_nodeid = zot.getNodeByName(xvendor_nodeid.ID.ToString(), xdoc_type);
            if (xdoctype_nodeid == null)
            {
                xdoctype_nodeid = zot.createFolder(xvendor_nodeid.ID.ToString(), xdoc_type);
            }

            xdoc_name = xdoc_name.Replace(":", " ");
            return zot.uploadDoc(xdoctype_nodeid.ID.ToString(), xdoc_name, xdoc_name, xcontent, "").ToString();

        }
        bool checkFileUpload(string bid_no, string bid_revision, string schedule_name, Stream stream)
        {
            //if (excel.validateFile(bid_no, bid_revision, schedule_name, stream))
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            return true;
        }

        void getInvalidFileUpTb()
        {
            DataTable table = new DataTable("InvalidFileUpTb");
            table.Columns.Add("vendor_type");
            table.Columns.Add("vendor_name");
            table.Columns.Add("doc_name");
            table.Columns.Add("doctype_name");

            ViewState["InvalidFileUpTb"] = table;
        }
        protected void DigitalFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.fup_sinjoi.HasFile)
                {
                    this.alert_upload.Visible = true;
                    this.btn_digitalFileUpload.Enabled = false;

                    string nodeid = "";
                    string[] str = this.ddl_vendor.SelectedValue.Split(';');

                    getInvalidFileUpTb();
                    DataTable table = (DataTable)ViewState["InvalidFileUpTb"];

                    foreach (HttpPostedFile uploadedFile in fup_sinjoi.PostedFiles)
                    {
                        using (Stream stream = uploadedFile.InputStream)
                        {
                            if (checkFileUpload(this.lb_bidnumber.Text, this.txt_bidVersion.Text, this.ddl_refSchedule.SelectedValue, stream))
                            {
                                using (BinaryReader br = new BinaryReader(stream))
                                {
                                    byte[] bytes = br.ReadBytes((Int32)stream.Length);

                                    if (bytes.Length > 0)
                                    {
                                        nodeid
                                            = uploadDoc(this.lb_bidnumber.Text, this.txt_bidVersion.Text, this.ddl_refSchedule.SelectedValue, str[0], this.ddl_bidVendor.SelectedValue, this.ddl_docType.SelectedValue, uploadedFile.FileName, bytes, false);

                                        insBidSelling
                                            (str[0], this.ddl_refSchedule.SelectedValue, this.ddl_bidVendor.SelectedValue, this.ddl_bidVendor.SelectedItem.Text, this.ddl_bidderType2.SelectedValue, uploadedFile.FileName, this.ddl_docType.SelectedValue, 0, nodeid);

                                        try
                                        {
                                            excel.uploadData(int.Parse(nodeid), str[0], this.ddl_bidVendor.SelectedValue, this.lb_bidnumber.Text, int.Parse(this.txt_bidVersion.Text), this.ddl_refSchedule.SelectedItem.Text, 0, stream, "123456");
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                table.Rows.Add(this.ddl_bidderType2.SelectedValue, this.ddl_bidVendor.SelectedItem.Text, this.ddl_docType.SelectedItem.Text, uploadedFile.FileName);
                            }
                        }
                    }

                    if (table.Rows.Count > 0)
                    {
                        this.gv_invalidFileUp.DataSource = table;
                        this.gv_invalidFileUp.DataBind();
                        ViewState["InvalidFileUpTb"] = table;
                    }

                    this.alert_upload.Visible = false;
                    this.btn_digitalFileUpload.Enabled = true;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('has been file upload saved.');", true);
                    dspBidFileUpload(str[0]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('file upload not found. please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                this.alert_upload.Visible = false;
                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to save data.');", true);
                //throw ex;
            }
        }
        bool checkBidderType(string bidderId, string bidderType)
        {
            try
            {
                string sql = string.Format("select * from ps_t_bid_selling_vendor where bidder_id = '{0}' and vendor_type = '{1}'", bidderId, bidderType);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                return false;
                throw e;
            }
        }
        protected void btn_bidderAdd_Click(object sender, EventArgs e)
        {
            try
            {
                string[] str = this.ddl_vendor.SelectedValue.Split(';');

                string sql = string.Format("select * from ps_t_bid_selling_vendor where bidder_id = '{0}'", str[0]);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    if (checkBidderType(str[0], this.ddl_bidderType.SelectedValue) == false)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('invalid bidder type of vendor. please try again.');", true);
                        return;
                    }
                }

                this.ddl_bidderType2.Items.Clear();
                this.ddl_bidVendor.Items.Clear();
                this.ddl_refSchedule.Items.Clear();
                this.ddl_docType.DataSource = null;

                this.ddl_docType.DataSource = clsPriceUpload.GetDocType();
                this.ddl_docType.DataTextField = "doctype_name";
                this.ddl_docType.DataValueField = "doctype_code";
                this.ddl_docType.DataBind();

                dspBidFileUpload(str[0]);

                this.pn_form1.Enabled = true;
                this.ddl_docType.SelectedValue = "PSD";

                RefSchedule();

                switch (this.ddl_bidderType.SelectedValue)
                {
                    case "Single Entity":
                    case "Joint Venture":

                        this.ddl_bidderType2.Items.Insert(0, new ListItem(this.ddl_bidderType.SelectedItem.Text, this.ddl_bidderType.SelectedValue));
                        this.ddl_bidVendor.Items.Insert(0, new ListItem(this.ddl_vendor.SelectedItem.Text, str[1]));

                        break;

                    case "Consortium":

                        this.ddl_bidderType2.Items.Insert(0, new ListItem("Consortium"));
                        this.ddl_bidderType2.Items.Insert(1, new ListItem("Vendor"));
                        this.ddl_bidVendor.SelectedValue = str[1];

                        BindVendorData();


                        break;

                    default:
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('bidder type not found. please try again.');", true);
                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gv_digitalFile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "IsDelete")
            {
                try
                {
                    string[] arg = e.CommandArgument.ToString().Split(';');
                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();
                        using (SqlCommand com = new SqlCommand(string.Format("delete from ps_t_bid_selling_fileupload where row_id = {0}", arg[0]), conn))
                        {
                            com.ExecuteNonQuery();

                            zot.deleteNodeID(arg[1], arg[1]);

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('data is deleted.');", true);

                            string[] str = this.ddl_vendor.SelectedValue.Split(';');
                            dspBidFileUpload(str[0]);
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to deleted data.');", true);
                    throw ex;
                }
            }
            else if (e.CommandName == "IsDownload")
            {
                var xcontent = zot.downloadDoc(e.CommandArgument.ToString());
                var xnode = zot.getNodeByID(e.CommandArgument.ToString());

                Response.Clear();
                MemoryStream ms = new MemoryStream(xcontent);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + xnode.Name);
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                Response.End();
            }
        }
    }
}