﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="BidSellingDescription2.aspx.cs" Inherits="PS_System.form.Selling.BidSellingDescription2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scriptMag" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePal" runat="server">
        <ContentTemplate>
            <div class="container mb-4">
                <div class="row mb-4">
                    <h6>EGAT - Bid Selling Management</h6>
                    <div class="ml-auto"></div>
                </div>
                <div class="card mb-4">
                    <div class="card-header">
                        รายละเอียดข้อมูลเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-8">
                                <label>Bid No.</label>
                                <asp:Label ID="txt_bidNo" runat="server" CssClass="form-control"></asp:Label>
                            </div>
                            <div class="form-group col-4" hidden>
                                <label>Bid Version.</label>
                                <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                            </div>
                            <div class="form-group col-12">
                                <label>Bid Description.</label>
                                <asp:Label ID="txt_bidDescription" runat="server" Height="150" CssClass="form-control" Enabled="false"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        การเพิ่มผู้ซื้อเอกสารประกวดราคา
                        <a href="VendorAdd.aspx" target="_blank" role="button" class="btn btn-success btn-sm"><i class="bi bi-person-plus mr-2"></i> ข้อมูลผู้ซื้อเอกสารประกวดราคา</a>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <asp:TextBox ID="txt_vendorsearch" runat="server" CssClass="form-control" MaxLength="50" placeholder="ค้นหาข้อมูลผู้ซื้อ (vendor code, vendor name, sap code, egp no.)"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_vendorsearch" runat="server" Text='<i class="bi bi-search"></i>' ToolTip="click to search." OnClick="btn_vendorsearch_Click" CausesValidation="false" CssClass="btn btn-primary" />
                                </div>
                            </div>
                        </div>
                        <div class="table-responsive-sm">
                            <asp:GridView ID="gv_vendor" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv_vendor_RowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="vendor_code" HeaderText="Code" />--%>
                                    <asp:BoundField DataField="vendor_name" HeaderText="Name" />
                                    <%--<asp:BoundField DataField="egp_no" HeaderText=" EGP No." />--%>
                                    <%--<asp:BoundField DataField="egp_register_date" HeaderText="Date Register" DataFormatString="{0:dd/MM/yyyy}" />--%>
                                    <asp:BoundField DataField="address" HeaderText="Address" />
                                    <asp:BoundField DataField="country" HeaderText="Country" />
                                    <%--<asp:BoundField DataField="contact_person" HeaderText="Name" />--%>
                                    <%--<asp:BoundField DataField="contact_tel" HeaderText="Telephone" />--%>
                                    <asp:TemplateField HeaderStyle-Width="50" HeaderStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <%--<asp:LinkButton CssClass="btn btn-info btn-sm" Text='<i class="bi bi-info-circle"></i>' runat="server" CommandName="isInfo" CommandArgument='<%#Eval("vendor_code")%>' ToolTip="click to view." />--%>
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" Text='<i class="bi bi-plus-circle"></i>' runat="server" CommandName="isAdd" CommandArgument='<%#string.Format("{0};{1}",Eval("vendor_code"),Eval("vendor_name")) %>' ToolTip="click to add."
                                                OnClientClick="if (!confirm('Do you want to add this vendor?')) return false;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header">
                        ตารางข้อมูลข้อมูลผู้ซื้อเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_datanotfound">
                            Data not found.
                        </div>
                        <div class="table-responsive-sm">
                            <asp:GridView ID="gv_bidsellingdescription" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_bidsellingdescription_PageIndexChanging"
                                OnRowCommand="gv_bidsellingdescription_RowCommand" DataKeyNames="vendor_code" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found.">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <%--<asp:BoundField DataField="vendor_code" HeaderText="Code" />--%>
                                    <asp:BoundField DataField="vendor_name" HeaderText="Name" />
                                    <%--<asp:BoundField DataField="egp_no" HeaderText=" EGP No." />--%>
                                    <%--<asp:BoundField DataField="egp_register_date" HeaderText="Date Register" DataFormatString="{0:dd/MM/yyyy}" />--%>
                                    <asp:BoundField DataField="address" HeaderText="Address" />
                                    <asp:BoundField DataField="country" HeaderText="Country" />
                                    <%--<asp:BoundField DataField="contact_person" HeaderText="Name" />--%>
                                    <%--<asp:BoundField DataField="contact_tel" HeaderText="Telephone" />--%>
                                    <asp:TemplateField HeaderStyle-Width="80" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-danger btn-sm" Text='<i class="bi bi-x-circle"></i>' runat="server" CommandName="isDelete" OnClientClick="if (!confirm('Do you want to delete this vendor?')) return false;" CommandArgument='<%#Eval("vendor_code")%>' ToolTip="Click to delete." />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="BidSelling2.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                            <asp:LinkButton ID="Button1" runat="server" Text='<i class="bi bi-plus-circle mr-2"></i> Add Vendor' class="btn btn-success btn-sm" OnClick="Button1_Click" Visible="false" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
