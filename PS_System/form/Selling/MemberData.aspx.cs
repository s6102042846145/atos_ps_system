﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class MemberData : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void GetBidMember(string bidnumber)
        {
            try
            {
                string sql = @"select a.*, b.bid_desc";
                sql += @", (select count(emp_id) from ps_t_bid_selling_committee as b where a.bid_no = b.bid_no and committee_type = 'receive_open ') total_receive_open  ";
                sql += @", (select count(emp_id) from ps_t_bid_selling_committee as b where a.bid_no = b.bid_no and committee_type = 'considered ') total_considered  ";
                sql += @", (select count(emp_id) from ps_t_bid_selling_committee as b where a.bid_no = b.bid_no and committee_type = 'check') total_check ";
                sql += @" from ps_t_bid_monitoring as a ";
                sql += @" inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision";
                sql += " where a.bid_no like '%" + bidnumber + "%'";
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.alert_datanotfound.Visible = false;
                    this.gv_bidmember.DataSource = dt;
                    this.gv_bidmember.DataBind();
                }
                else
                {
                    this.alert_datanotfound.Visible = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetBidMember(string.Empty);
            }
        }

        protected void gv_bidmember_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_bidmember.PageIndex = e.NewPageIndex;
            this.GetBidMember(this.txt_bidnosearch.Text);
        }

        protected void gv_bidmember_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "IsAdd" || e.CommandName == "IsView")
            {
                string[] arg = new string[3];
                arg = e.CommandArgument.ToString().Split(';');

                DropDownList dropDown
                    = (DropDownList)this.gv_bidmember.Rows[int.Parse(arg[2])].FindControl("ddl_committeetype");

                if (e.CommandName == "IsAdd")
                {
                    if (dropDown.SelectedValue == "0")
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please select a committee.');", true);
                    }
                    else
                    {
                        Response.Redirect(string.Format("MemberBidAdd.aspx?bidno={0}&bidrevision={1}&type={2}", arg[0], arg[1], dropDown.SelectedValue));
                    }
                }
                else if (e.CommandName == "IsView")
                {
                    Response.Redirect(string.Format("MemberBidData.aspx?bidno={0}&bidrevision={1}", arg[0], arg[1]));
                }
            }
        }

        protected void btn_bidnosearch_Click(object sender, EventArgs e)
        {
            this.GetBidMember(this.txt_bidnosearch.Text);
        }
    }
}