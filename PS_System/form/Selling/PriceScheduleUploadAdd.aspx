﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="PriceScheduleUploadAdd.aspx.cs" Inherits="PS_System.form.Selling.PriceScheduleUploadAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container mb-4 mt-4">
                <div class="row mb-4">
                    <h5>EGAT - Price Schedule Upload : Digital File Validation</h5>
                    <div class="ml-auto">
                        <h5 runat="server" id="h_bidnumberTitle" class="text-primary"></h5>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header  d-flex justify-content-between align-items-center">
                        รายละเอียดข้อมูลเอกสารประกวดราคา
                                <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Click to expand"><i class="bi bi-justify"></i></a>
                    </div>
                    <div class="collapse" id="collapseExample">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-sm-8">
                                    <label>Bid No.</label>
                                    <asp:Label ID="lb_bidnumber" runat="server" CssClass="form-control"></asp:Label>
                                </div>
                                <div class="form-group col-sm-4">
                                    <label>Bid Version.</label>
                                    <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                                </div>
                                <div class="form-group col-sm-12">
                                    <label>Bid Description.</label>
                                    <asp:Label ID="txt_bidDescription" runat="server" Height="150" CssClass="form-control"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header">
                        รายละเอียดการอัปโหลดเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-6">
                                <label>Bidder</label>
                                <asp:DropDownList runat="server" ID="ddl_vendor" CssClass="form-control">
                                    <asp:ListItem Text="Vendor 1" Value="all"></asp:ListItem>
                                    <asp:ListItem Text="Vendor 2" Value="s1"></asp:ListItem>
                                    <asp:ListItem Text="Vendor 3" Value="s2"></asp:ListItem>
                                    <asp:ListItem Text="Vendor 4" Value="s3"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-6">
                                <label>Bidder type.</label>
                                <asp:DropDownList runat="server" ID="ddl_bidderType" CssClass="form-control">
                                    <asp:ListItem Text="Single Entity" Value="Single Entity"></asp:ListItem>
                                    <asp:ListItem Text="Joint Venture" Value="Joint Venture"></asp:ListItem>
                                    <asp:ListItem Text="Consortium" Value="Consortium"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                            <div class="form-group col-12">
                                <div class="text-right">
                                    <asp:LinkButton runat="server" ID="btn_action" CssClass="btn btn-primary btn-sm" Text='<i class="bi bi-plus-circle mr-2"></i>Add Vendor' OnClick="btn_action_Click"></asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
<%--                    <div class="card-footer">
                        <div class="text-right">
                            <a href="PriceScheduleUploadData.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>--%>
                </div>

                <div class="card mb-4" runat="server">
                    <div class="card-header">
                        รายละเอียดการอัปโหลดเอกสารประกวดราคา Bidder Type :
                        <asp:Label runat="server" ID="lb_bidderType" CssClass="text-primary"></asp:Label>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="" runat="server">
                                <div class="input-group">
                                    <asp:DropDownList runat="server" ID="ddl_joinVendor" CssClass="form-control selectpicker" data-live-search="true"></asp:DropDownList>
                                    <div class="input-group-append">
                                        <asp:LinkButton runat="server" ID="btn_add" CssClass="btn btn-primary" ToolTip="Click to add." Text='<i class="bi bi-plus-circle"></i>' OnClick="btn_add_Click"></asp:LinkButton>
                                    </div>
                                    <div class="input-group-append">
                                        <a href="VendorAdd.aspx" target="_blank" class="btn btn-success" title="Click to new vendor."><i class="bi bi-person-plus"></i></a>
                                    </div>
                                    <div class="input-group-append">
                                        <asp:LinkButton runat="server" ID="btn_reload" CssClass="btn btn-secondary" ToolTip="Click to reload vendor." Text='<i class="bi bi-arrow-repeat"></i>' OnClick="btn_reload_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <div class="form-group col-12">
                                <asp:GridView ID="gv_fileUpload" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" Font-Names="tahoma" Font-Size="9pt"
                                    OnRowDataBound="gv_fileUpload_RowDataBound" OnRowCommand="gv_fileUpload_RowCommand">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Width="10">
                                            <ItemTemplate>
                                                <asp:RadioButton ID="roType" runat="server" onclick="RadioCheck(this);" Checked='<%# Eval("vendor_lead") %>' />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="vendor_name" HeaderText="Vendor" />
                                        <asp:TemplateField HeaderText="Schedule No.">
                                            <ItemTemplate>
                                                <asp:DropDownList runat="server" ID="ddl_RefSchedule" CssClass="form-control form-control-sm"></asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Upload File.">
                                            <ItemTemplate>
                                                <div class="input-group">
                                                    <asp:FileUpload runat="server" ID="fup_documents" CssClass="form-control btn btn-sm" AllowMultiple="true"></asp:FileUpload>
                                                    <div class="input-group-append">
                                                        <asp:LinkButton runat="server" ID="btn_docUpload" CssClass="btn btn-success btn-sm" ToolTip="Click to upload documents." Text='<i class="bi bi-upload"></i>' CommandName="docUpload" CommandArgument="<%# Container.DataItemIndex  %>"></asp:LinkButton>
                                                    </div>
                                                </div>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderStyle CssClass="small" />
                                </asp:GridView>
                            </div>
                            <div class="form-group col-12">
                                <asp:GridView ID="GridView1" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" Font-Names="tahoma" Font-Size="9pt">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:BoundField DataField="vendor_name" HeaderText="Vendor" />
                                        <asp:BoundField DataField="ref_schedule_no" HeaderText="Schedule No." />
                                        <asp:BoundField DataField="doc_name" HeaderText="File." />
                                        <asp:BoundField DataField="file_status" HeaderText="Status" />
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderStyle CssClass="small" />
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card" runat="server">
                    <div class="card-header">
                        รายละเอียดการอัปโหลดเอกสารประกวดราคาอื่นๆ : <span class="text-primary">Other Documents.</span>
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-4">
                                <label class="mr-2">Schedule No.</label>
                                <asp:DropDownList runat="server" ID="ddl_RefSchedule" CssClass="form-control form-control-sm"></asp:DropDownList>
                            </div>
                            <div class="form-group col-4">
                                <label class="mr-2">Other Documents.</label>
                                <asp:DropDownList runat="server" ID="ddl_docType" CssClass="form-control form-control-sm"></asp:DropDownList>
                            </div>
                            <div class="form-group col-4">
                                <label class="mr-2">Upload File.</label>
                                <div class="input-group">
                                    <asp:FileUpload runat="server" ID="fup_otherdocuments" CssClass="form-control btn btn-sm" AllowMultiple="true"></asp:FileUpload>
                                    <div class="input-group-append">
                                        <asp:LinkButton runat="server" ID="btn_docUpload" CssClass="btn btn-success btn-sm" ToolTip="Click to upload documents." Text='<i class="bi bi-upload"></i>' OnClick="btn_docUpload_Click"></asp:LinkButton>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group col-12">
                                <asp:GridView ID="gv_otherFileUpload" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt">
                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderStyle-Width="10">
                                            <ItemTemplate>
                                                <%# Container.DataItemIndex + 1 %>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Schedule No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_ScheduleNo" runat="server" CssClass="form-control"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Documents Type.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_docType" runat="server" CssClass="form-control"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="File Upload.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_fileUpload" runat="server" CssClass="form-control"></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                    <HeaderStyle CssClass="small" />
                                </asp:GridView>
                            </div>
                        </div>
                        <div class="text-right">
                            <asp:LinkButton runat="server" ID="btn_saveOtherDoc" CssClass="btn btn-success" Text='<i class="bi bi-save mr-2"></i>Save' OnClick="btn_saveOtherDoc_Click"></asp:LinkButton>
                        </div>
                    </div>
                </div>
            </div>
            <script type="text/javascript">
                function RadioCheck(rb) {
                    var gv = document.getElementById("<%=gv_fileUpload.ClientID%>");
                    var rbs = gv.getElementsByTagName("input");
                    var row = rb.parentNode.parentNode;

                    for (var i = 0; i < rbs.length; i++) {

                        if (rbs[i].type == "radio") {

                            if (rbs[i].checked && rbs[i] != rb) {

                                rbs[i].checked = false;

                                break;
                            }
                        }
                    }
                }
            </script>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="btn_docUpload" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
