﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidSellingAdd2 : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        private void bindItems()
        {
            try
            {
                string sql = "select * from ps_t_bid_monitoring";
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_bidNo.DataSource = dt;
                    this.ddl_bidNo.DataTextField = "bid_no";
                    this.ddl_bidNo.DataValueField = "bid_no";
                    this.ddl_bidNo.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        private void bindDescription(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format("select bid_no, bid_revision, bid_desc from bid_no where bid_no = '{0}' and bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                //string desc;
                if (dt.Rows.Count > 0)
                {
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();

                    //desc = dt.Rows[0]["bid_desc"].ToString().Replace("<div>", "");
                    //desc = desc.Replace("</div>", "");
                    //desc = desc.Replace("<p>", "");
                    //desc = desc.Replace("</p>", "");
                    //desc = desc.Replace("<br>", "");
                    //desc = desc.Replace("</br>", "");
                    //desc = desc.Replace("&nbsp;", "");

                    //this.txt_bidDescription.Text = desc;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    bindItems();
                    this.ddl_bidNo.SelectedValue = Request.QueryString["bidno"];
                    this.ddl_bidNo.Enabled = false;

                    bindDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                       string.Format("alert('data not found.'); window.location='BidSelling2.aspx';"), true);
                }
            }
        }

        //protected void ddl_bidNo_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    if (this.ddl_bidNo.SelectedValue == "0")
        //    {
        //        this.txt_bidVersion.Text = string.Empty;
        //        this.txt_bidDescription.Text = string.Empty;
        //    }
        //    else
        //    {
        //        bindDescription(this.ddl_bidNo.SelectedValue);
        //    }
        //}

        protected void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                string sql = string.Format("select * from ps_t_bid_selling where  bid_no = '{0}' and vendor_code = '{1}' and bid_revision = {2}", this.ddl_bidNo.SelectedValue, this.txt_venderCode.Text, this.txt_bidVersion.Text);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('duplicate vendor. please try again.');", true);
                    this.txt_venderCode.Focus();
                }
                else
                {
                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("sp_ins_selling", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@bid_no", this.ddl_bidNo.SelectedValue);
                            cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));
                            cmd.Parameters.AddWithValue("@vendor_code", this.txt_venderCode.Text);
                            cmd.Parameters.AddWithValue("@vendor_name", this.txt_companyName.Text);
                            cmd.Parameters.AddWithValue("@bidder_status", "selling");
                            cmd.Parameters.AddWithValue("@tax_id", this.txt_Tax.Text);
                            cmd.Parameters.AddWithValue("@egp_no", this.txt_EGPNo.Text);
                            cmd.Parameters.AddWithValue("@egp_register_date", DateTime.Parse(this.txt_EGPDate.Text));
                            cmd.Parameters.AddWithValue("@address", this.txt_address.Text);
                            cmd.Parameters.AddWithValue("@country", this.txt_country.Text);
                            cmd.Parameters.AddWithValue("@tel_no", this.txt_Tel.Text);
                            cmd.Parameters.AddWithValue("@fax_no", this.txt_Fax.Text);
                            cmd.Parameters.AddWithValue("@contact_person", this.txt_contactName.Text);
                            cmd.Parameters.AddWithValue("@contact_email", this.txt_contactEmail.Text);
                            cmd.Parameters.AddWithValue("@contact_tel", this.txt_contactTel.Text);
                            cmd.Parameters.AddWithValue("@contact_fax", this.txt_contactFax.Text);

                            cmd.ExecuteNonQuery();
                            conn.Close();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                                string.Format("alert('has been successfully saved.'); window.location='BidSellingDescription2?bidno={0}&bidrevision={1}';", this.ddl_bidNo.SelectedValue, this.txt_bidVersion.Text), true);
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btn_vendorsearch_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.txt_venderCode.Text == "") return;

                string sql = string.Format("select * from ps_m_vendor where vendor_code='{0}'", this.txt_venderCode.Text);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.txt_venderCode.Text = dt.Rows[0]["vendor_code"].ToString();
                    this.txt_companyName.Text = dt.Rows[0]["vendor_name"].ToString();
                    this.txt_Tax.Text = dt.Rows[0]["tax_id"].ToString();
                    this.txt_EGPNo.Text = dt.Rows[0]["egp_no"].ToString();
                    this.txt_EGPDate.Text = DateTime.Parse(dt.Rows[0]["egp_register_date"].ToString()).ToString("yyyy-MM-dd");
                    this.txt_address.Text = dt.Rows[0]["address"].ToString();
                    this.txt_country.Text = dt.Rows[0]["country"].ToString();
                    this.txt_Tel.Text = dt.Rows[0]["tel_no"].ToString();
                    this.txt_Fax.Text = dt.Rows[0]["fax_no"].ToString();
                    this.txt_contactName.Text = dt.Rows[0]["contact_person"].ToString();
                    this.txt_contactEmail.Text = dt.Rows[0]["contact_email"].ToString();
                    this.txt_contactTel.Text = dt.Rows[0]["contact_tel"].ToString();
                    this.txt_contactFax.Text = dt.Rows[0]["contact_fax"].ToString();

                    this.txt_companyName.Enabled = false;
                    this.txt_Tax.Enabled = false;
                    this.txt_EGPNo.Enabled = false;
                    this.txt_EGPDate.Enabled = false;
                    this.txt_address.Enabled = false;
                    this.txt_country.Enabled = false;
                    this.txt_Tel.Enabled = false;
                    this.txt_Fax.Enabled = false;
                    this.txt_contactName.Enabled = false;
                    this.txt_contactEmail.Enabled = false;
                    this.txt_contactTel.Enabled = false;
                    this.txt_contactFax.Enabled = false;
                }
                else
                {
                    this.txt_venderCode.Enabled = true;
                    this.txt_companyName.Enabled = true;
                    this.txt_Tax.Enabled = true;
                    this.txt_EGPNo.Enabled = true;
                    this.txt_EGPDate.Enabled = true;
                    this.txt_address.Enabled = true;
                    this.txt_country.Enabled = true;
                    this.txt_Tel.Enabled = true;
                    this.txt_Fax.Enabled = true;
                    this.txt_contactName.Enabled = true;
                    this.txt_contactEmail.Enabled = true;
                    this.txt_contactTel.Enabled = true;
                    this.txt_contactFax.Enabled = true;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('this vendor code can be used.');", true);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}