﻿using PS_Library;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class PriceSheduleUpView : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        OTFunctions zot = new OTFunctions();
        private void bindDescription(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format("select bid_no, bid_revision, bid_desc from bid_no where bid_no = '{0}' and bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.h_bidnumberTitle.InnerText = bidno;
                    this.lb_bidnumber.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindVendorData(string bitnumber, string bitrevision)
        {
            try
            {
                string sql = string.Format("select a.*, (a.bidder_id + ';' + a.vendor_code) as idRef from ps_t_bid_selling  as a left join ps_m_vendor as b on a.vendor_code = b.vendor_code left join ps_t_bid_selling_vendor as c on a.vendor_code = c.vendor_code where a.bid_no = '{0}' and a.bid_revision = {1} order by a.bidder_id", bitnumber, bitrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.gv_bidder.DataSource = dt;
                    this.gv_bidder.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void bidSellingVendor(string bidno, string bidrevision)
        {
            try
            {
                string sql = @" select a.*, b.doctype_name,  c.vendor_name, c.vendor_type from ps_t_bid_selling_fileupload as a left join ps_m_doctype as b on a.doc_type = b.doctype_code  left join ps_t_bid_selling_vendor as c on a.vendor_code = c.vendor_code   ";
                sql += string.Format(" where a.bid_no = '{0}' order by bidder_id", bidno);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.gv_bidSellingVendor.DataSource = dt;
                    this.gv_bidSellingVendor.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void GroupGridView(GridViewRowCollection gvrc, int startIndex, int total)
        {
            if (gvrc.Count < 1)
            {
                return;
            }
            else
            {
                int i, count = 1;

                ArrayList lst = new ArrayList();
                lst.Add(gvrc[0]);

                var ctrl = gvrc[0].Cells[startIndex];

                for (i = 1; i < gvrc.Count; i++)
                {
                    TableCell nextCell = gvrc[i].Cells[startIndex];
                    if (ctrl.Text == nextCell.Text)
                    {
                        count++;
                        nextCell.Visible = false;
                        lst.Add(gvrc[i]);
                    }
                    else
                    {
                        if (count > 1)
                        {
                            ctrl.RowSpan = count;
                            GroupGridView(new GridViewRowCollection(lst), startIndex + 1, total - 1);
                        }
                        count = 1;
                        lst.Clear();
                        ctrl = gvrc[i].Cells[startIndex];
                        lst.Add(gvrc[i]);
                    }
                }

                if (count > 1)
                {
                    ctrl.RowSpan = count;
                    GroupGridView(new GridViewRowCollection(lst), startIndex + 1, total - 1);
                }

                count = 1;
                lst.Clear();
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["bidno"] != null)
            {
                bindDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                bidSellingVendor(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                BindVendorData(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                //GroupGridView(this.gv_bidSellingVendor.Rows, 0, this.gv_bidSellingVendor.Columns.Count - 1);
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                   string.Format("alert('data not found.'); window.location='PriceScheduleUploadData.aspx';"), true);
            }
        }
        protected void gv_bidSellingVendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "IsDelete")
            {
                try
                {
                    string[] arg = e.CommandArgument.ToString().Split(';');
                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();

                        using (SqlCommand com = new SqlCommand(string.Format("delete from ps_t_bid_selling_fileupload where row_id = {0}", arg[0]), conn))
                        {
                            com.ExecuteNonQuery();

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message",
                                string.Format("alert('data is deleted.'); window.location='PriceSheduleUpView?bidno={0}&bidrevision={1}';", this.lb_bidnumber.Text, this.txt_bidVersion.Text), true);
                        }
                        conn.Close();
                    }

                    zot.deleteNodeID(arg[1], arg[1]);
                }
                catch (Exception ex)
                {
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to deleted data.');", true);
                    //throw ex;
                }
            }
            else if (e.CommandName == "IsDownload")
            {
                var xcontent = zot.downloadDoc(e.CommandArgument.ToString());
                var xnode = zot.getNodeByID(e.CommandArgument.ToString());

                Response.Clear();
                MemoryStream ms = new MemoryStream(xcontent);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + xnode.Name);
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                Response.End();
            }
        }

        protected void btn_addSchedule_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("PriceScheduleFileUp.aspx?bidno={0}&bidrevision={1}", this.lb_bidnumber.Text, this.txt_bidVersion.Text));
        }

        protected void gv_bidder_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "IsDelete")
            {
                try
                {
                    string sql = string.Format("select * from ps_t_bid_selling_fileupload where bidder_id = '{0}'", e.CommandArgument);
                    var dt = zdb.ExecSql_DataTable(sql, zconn);

                    if (dt.Rows.Count > 0)
                    {
                        foreach (System.Data.DataRow dataRow in dt.Rows)
                        {
                            zot.deleteNodeID(dataRow["doc_node_id"].ToString(), dataRow["doc_node_id"].ToString());
                        }
                    }

                    SqlTransaction transaction;
                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();
                        transaction = conn.BeginTransaction();

                        try
                        {
                            using (SqlCommand com = new SqlCommand(string.Format("delete from ps_t_bid_selling_vendor where bidder_id  = '{0}'", e.CommandArgument), conn, transaction))
                            {
                                com.ExecuteNonQuery();
                            }
                            using (SqlCommand com = new SqlCommand(string.Format("delete from ps_t_bid_selling_fileupload where bidder_id = '{0}'", e.CommandArgument), conn, transaction))
                            {
                                com.ExecuteNonQuery();
                            }
                            using (SqlCommand com = new SqlCommand(string.Format("delete from ps_up_excel_transection where bidder_id = '{0}'", e.CommandArgument),conn,transaction))
                            {
                                com.ExecuteNonQuery();
                            }

                            transaction.Commit();
                            conn.Close();
                        }
                        catch (SqlException sqlEx)
                        {
                            transaction.Rollback();
                            conn.Close();

                            throw sqlEx;
                        }

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message",
                            string.Format("alert('data is deleted.'); window.location='PriceSheduleUpView?bidno={0}&bidrevision={1}';", this.lb_bidnumber.Text, this.txt_bidVersion.Text), true);
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to deleted data.');", true);
                    throw ex;
                }
            }
        }

        protected void btn_bidWinner_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("PriceScheduleBidWinner.aspx?bidno={0}&bidrevision={1}", this.lb_bidnumber.Text, this.txt_bidVersion.Text));
        }
    }
}