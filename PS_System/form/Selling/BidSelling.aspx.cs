﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Salling
{
    public partial class BidSalling : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void GetBidSelling()
        {
            try
            {
                string sql = @"select bid_no, bid_revision,COUNT(vendor_code) vendor from ps_t_bid_selling group by bid_no, bid_revision";
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.gv_bidselling.DataSource = dt;
                    this.gv_bidselling.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetBidSelling();
            }
        }

        protected void gv_bidselling_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view")
            {
                Response.Redirect(string.Format("BidSellingDescription?bidno={0}", e.CommandArgument));
            }
        }

        protected void gv_bidselling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_bidselling.PageIndex = e.NewPageIndex;
            this.GetBidSelling();
        }
    }
}