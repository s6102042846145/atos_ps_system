﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidCMPO01.aspx.cs" Inherits="PS_System.form.Selling.BidCMPO01" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>CMPO Create</title>
    <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
    $(function () {
        $('[id*=txtLOADate]').datepicker({
            changeMonth: true,
            changeYear: true,
            format: "dd/mm/yyyy",
            language: "tr"
        });
          $('[id*=txtLOAConfirmDate]').datepicker({
            changeMonth: true,
            changeYear: true,
            format: "dd/mm/yyyy",
            language: "tr"
        });
    });
    </script>
    <style type="text/css">
        .auto-style1 {
            font-family: Tahoma;
            font-weight: bold;
            font-size: 10pt;
            color: #232222;
            background-color: #FFFFFF;
        }
        .auto-style2 {
            height: 19px;
        }
        .auto-style3 {
            height: 25px;
        }
        .auto-style4 {
            height: 70px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
              <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
               
            </table>
        </div>
      <div class="div_0" style="margin:5px;" >
                <p>Please identify Purchase Org &amp; Group</p>
                <div id="divHeader" class="div_90">
                    <table  >
                        <tr>
                            <td colspan="2" style="text-align:right">
                                <asp:Label ID ="lblMode" runat="server"/>

                            </td>
                            <td style="text-align:right">
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                 <label class="Text_200">ค้นหาจาก PR No.: </label></td>
                            <td>
                                <asp:DropDownList ID="ddlPR" runat="server" CssClass="ddl-md"  ></asp:DropDownList>

                                <asp:ImageButton ID="ibtnSearchPR" runat="server" Height="30px" ImageUrl="~/images/search.png" OnClick="ibtnSearchPR_Click" />

                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Purchase Org: </label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPurchaseOrg" runat="server" CssClass="ddl-md" AutoPostBack="True" ></asp:DropDownList>
                                </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Purchase Group: </label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlPurchaseGroup" runat="server" CssClass="ddl-md" AutoPostBack="True" ></asp:DropDownList>
                                </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Bid No.: </label>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlBid" runat="server" CssClass="ddl-md" AutoPostBack="True" OnSelectedIndexChanged="ddlBid_SelectedIndexChanged"></asp:DropDownList>
                                <asp:Label ID ="lblBidRevision" runat="server"/>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Bid Desc: </label>
                            </td>
                            <td>
                                <asp:TextBox ID="txtBidDesc" runat="server" CssClass="Text_500" TextMode="MultiLine" ></asp:TextBox>
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">LOA Date: </label></td>
                            <td>
                                <asp:TextBox ID="txtLOADate"  runat="server" CssClass="Text_200"/></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">LOA Cfm.Date: </label></td>
                            <td>
                                <asp:TextBox ID="txtLOAConfirmDate" runat="server" CssClass="Text_200"></asp:TextBox>&nbsp;<asp:Button ID="btnUpdate"    runat="server" CssClass="btn_small_blue" Text="Update" OnClick="btnUpdate_Click" />
                            </td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;<asp:Button ID="btnGetSapNo"  CssClass="btn_normal_blue" runat="server" Text="Get SAP Contract Runno." Width="140px" OnClick="btnGetSapNo_Click" Visible="False"/></td>
                            <td>
                                <asp:TextBox ID="txtSapContractRunno" CssClass="Text_200" runat="server" Visible="False"  ></asp:TextBox></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Contract No.: </label></td>
                            <td>
                                <asp:TextBox ID="txtContractNo" CssClass="Text_200" runat="server" Width="300px" ReadOnly="True"  ></asp:TextBox></td>
                            <td>
                                &nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <label class="Text_200">Upload Contract: </label></td>
                            <td>
                                &nbsp;<asp:FileUpload ID="fuploadContract" CssClass="Text_400" runat="server" />
                                <asp:Button ID="btnUpload"    runat="server" CssClass="btn_small_blue" Text="Upload" OnClick="btnUpload_Click" />
                            </td>
                            <td>
                                <asp:Label ID="lblContractDocument" runat="server" CssClass="Text_400" Visible="False"></asp:Label>
                                <br />
                            </td>
                        </tr>
                    </table>
                </div>
                <p></p>
                <div>
                    <h6><span class="auto-style1">Details</span></h6>

                </div>
                <div id="divData" class="div_90">
                    <table>
                        <tr>
                            <td >
                                &nbsp;</td>
                        </tr>
                         
                    </table>
                         <asp:GridView ID="gv1" runat="server" AutoGenerateColumns="false" 
                        BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
                        EmptyDataText="No data found." 
                        Font-Names="tahoma" Font-Size="9pt" Width="95%" OnRowCommand="gv1_OnRowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        <Columns>
                             <asp:TemplateField HeaderText="Select">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvSelect"  runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="60px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Schedule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvScheduleName" CssClass="Text_200" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="250px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Bidder (Winner)">
                                        <ItemTemplate>
                                            <asp:Label ID="gvVendorCode" CssClass="Text_100" runat="server" Text='<%# Bind("vendor_code") %>'></asp:Label>
                                            <asp:Label ID="gvVendorName" CssClass="Text_200" runat="server" Text='<%# Bind("vendor_Name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Vendor Type">
                                        <ItemTemplate>
                                            <asp:Label ID="gvVendorType" CssClass="Text_200" runat="server" Text='<%# Bind("vendor_type") %>'></asp:Label>
                                            </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                            </asp:TemplateField>
                             <%--<asp:TemplateField HeaderText="Contract Detail">
                                        <ItemTemplate>
                                            <table>
                                              
                                                <tr>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                            
                                            </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                            </asp:TemplateField>--%>
                           
                             <%--   <asp:TemplateField HeaderText="SAP Runno">
                                        <ItemTemplate>
                                          
                                                  
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="200px" />
                            </asp:TemplateField>--%>
                           
<%--                         <asp:TemplateField HeaderText="Upload Signed Doc" >
                                        <ItemTemplate>
                                           
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="200px" />
                            </asp:TemplateField>--%>
                            <%-- <asp:TemplateField HeaderText="Status">
                                        <ItemTemplate>
                                            <asp:Label ID="gvStatus" CssClass="Text_100" runat="server" Text='<%# Bind("status") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="100px" />
                            </asp:TemplateField>--%>
                            <asp:TemplateField HeaderText="Hidden" Visible="false">
                                 <ItemTemplate>
                                    <%--<asp:Label ID="Label1" runat="server" Text='<%# Bind("row_id") %>'/>--%>
                                    <asp:Label ID="gvBidNo" runat="server" Text='<%# Bind("bid_no") %>'/>
                                    <asp:Label ID="gvBidRevision" runat="server" Text='<%# Bind("bid_revision") %>' />
                                 </ItemTemplate>
                                 <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="25px" />
                            </asp:TemplateField>
                            </Columns>
                    </asp:GridView>
                    <br />
                    <asp:Label ID="lblTestRunStatus" runat="server" CssClass="Text_400"></asp:Label>
                    <br />
                    <table cellpadding="0" cellspacing="0" class="caption" style="vertical-align:middle;text-align:center;">
                        <tr>
                            <td class="auto-style2">
                                &nbsp;</td>
                            <td class="auto-style2">
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                            <td class="auto-style2">
                                <asp:Image ID="Image2" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusBid" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png"  />
                            </td>
                            <td class="auto-style2">
                                <asp:Image ID="Image1" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusLOA" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png" />
                            </td>
                          <td class="auto-style2">
                                <asp:Image ID="Image3" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusCreateContract" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png"  />
                            </td>
                            <td class="auto-style2">
                                <asp:Image ID="Image4" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusUploadContract" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png" />
                            </td>
                            <td class="auto-style2">
                                <asp:Image ID="Image5" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusGenPOItem" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png"  />
                            </td>
                            <td class="auto-style2">
                                <asp:Image ID="Image6" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusAssignAcc" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png" />
                            </td>
                            <td class="auto-style2">
                                <asp:Image ID="Image7" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusTestRun" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png"  />
                            </td>
                            <td class="auto-style2">
                                <asp:Image ID="Image8" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                            <td class="auto-style2">
                                <asp:ImageButton ID="ibtnStatusSendToSAP" runat="server" Height="40px" ImageUrl="~/images/icon_circle_blank.png" />
                            </td>
                             <td class="auto-style2">
                                <asp:Image ID="Image9" runat="server" ImageUrl="~/images/icon_line1.png" Width="50px" />
                            </td>
                             <td class="auto-style2">
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                        </tr>
                        </table>
                 <table cellpadding="0" cellspacing="0" style="vertical-align:middle;text-align:center;" class="auto-style4">
                        <tr style="text-align:left;">
                            <td class="auto-style3">
                                &nbsp;</td>
                            <td>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;</td>
                            <td>
                    <asp:Label ID="Label1" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Create BID on SAP</asp:Label>
                            </td>
                            <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label2" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">LOA Cfrm.</asp:Label>
                            </td>
                            <td>
                    &nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label3" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Create Contract</asp:Label>
                            </td>
                            <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label4" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Upload Signed Contract</asp:Label>
                            </td>
                            <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                    <asp:Label ID="Label5" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Generate PO-Items</asp:Label>
                            </td>
                             <td>&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label6" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Submit to ACC</asp:Label>
                                 &nbsp;</td>
                             <td>
                    &nbsp;&nbsp;
                    <asp:Label ID="Label7" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Test Run</asp:Label>
                            </td>
                             <td>
                    <asp:Label ID="Label8" runat="server" CssClass="Text_400" Font-Names="tahoma" Font-Size="9pt" Width="70px">Create PO on SAP</asp:Label>
                            </td>
                             <td>
                                 &nbsp;</td>
                        </tr>
                    </table>
                    </div>
                                <asp:Button ID="btnCreateSAPBid"  CssClass="btn_normal_blue" runat="server" Text="Create SAP BidNo." Width="160px" OnClick="btnCreateSAPBid_Click"/>
                                 <asp:Button ID="btnSave" CssClass="btn_normal_blue" runat="server" Text="Create Contract" OnClick="btnSave_Click" Width="160px" />
                                <asp:Button ID="btnGeneratePOItems" CssClass="btn_normal_blue" runat="server" Text="Generate PO Item"  Width="160px" OnClick="btnGeneratePOItems_Click" />
                                <asp:Button ID="btnSubmitToACC" CssClass="btn_normal_blue" runat="server" Text="Submit to ACC"  Width="160px" OnClick="btnSubmitToACC_Click" />
                                <asp:Button ID="btnCreatePOOnSAP"    runat="server" CssClass="btn_small_blue" Text="Create PO on SAP"  Width="160px" OnClick="btnCreatePOOnSAP_Click" />
                                <asp:Button ID="btnPreviewContract"    runat="server" CssClass="btn_small_blue" Text="Preview Contract" OnClick="btnPreviewContract_Click" Width="160px" />
                                <asp:Button ID="btnBIDDoc" CssClass="btn_normal_gray" runat="server" Text="Bid Documents" Width="160px" OnClick="btnBIDDoc_Click" />
                                <asp:Button ID="btnSyncDoc"    runat="server" CssClass="btn_normal_gray" Text="Sync xECM"  Width="160px" OnClick="btnSyncDoc_Click" />
                          

            </div>
                <asp:HiddenField ID="hidBidNo" runat="server" />
                    <asp:HiddenField ID="hidBidRevision" runat="server" />
                    <asp:HiddenField ID="hidBidderID" runat="server" />
                    <br />
                    <asp:HiddenField ID="hidContractNodeID" runat="server" />
                    <asp:HiddenField ID="hidMode" runat="server" />
                    <br />
                    <asp:HiddenField ID="hidLogin" runat="server" />
                    <br />
                  
    </form>
</body>
</html>
