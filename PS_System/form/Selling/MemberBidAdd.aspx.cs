﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class MemberBidAdd : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        private bool GetBidSelling(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format(@"select a.bid_no,a.bid_revision,b.bid_desc from ps_t_bid_monitoring as a inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision where a.bid_no = '{0}' and a.bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    //string desc;
                    this.txt_bidNo.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();

                    //desc = dt.Rows[0]["bid_desc"].ToString().Replace("<div>", "");
                    //desc = desc.Replace("</div>", "");
                    //desc = desc.Replace("<p>", "");
                    //desc = desc.Replace("</p>", "");
                    //desc = desc.Replace("<br>", "");
                    //desc = desc.Replace("</br>", "");
                    //desc = desc.Replace("&nbsp;", "");

                    //this.txt_bidDescription.Text = desc;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public void GetApproverCommon(string strEmpId, string strFullName, string strSPosition, string strFposition)
        {
            if (strEmpId == "" && strFullName == "" && strSPosition == "" && strFposition == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter your information to search.');", true);

                this.gv_employee.DataSource = null;
                this.gv_employee.DataBind();

                this.alert_results.Visible = false;
            }
            else
            {
                DataTable dt = new DataTable();
                string strFilter = string.Empty;
                string strSql = @" SELECT distinct ID as emp_id, FULLNAME as emp_name, POSITION_SHORT as short_position,  POSITION_FULLNAME as full_position
                            FROM V_EMPLOYEE_SEARCH 
                            WHERE ID IS NOT NULL  ";

                if (!String.IsNullOrEmpty(strEmpId)) strFilter += " AND ID LIKE '%" + strEmpId + "%' ";
                if (!String.IsNullOrEmpty(strFullName)) strFilter += " AND UPPER(FULLNAME) LIKE '%" + strFullName.ToUpper() + "%' ";
                if (!String.IsNullOrEmpty(strSPosition)) strFilter += " AND UPPER(POSITION_SHORT) LIKE '%" + strSPosition.ToUpper() + "%' ";
                if (!String.IsNullOrEmpty(strFposition)) strFilter += " AND UPPER(POSITION_FULLNAME) LIKE '%" + strFposition.ToUpper() + "%' ";

                strSql += strFilter + " ORDER BY FULLNAME ASC";

                dt = ora._getDT(strSql, zcommondb);

                if (dt.Rows.Count > 0)
                {
                    this.gv_employee.DataSource = dt;
                    this.gv_employee.DataBind();

                    this.alert_results.Visible = true;
                    this.alert_datanotfound.Visible = false;
                }
                else
                {
                    this.alert_results.Visible = false;
                    this.alert_datanotfound.Visible = true;
                }

            }
        }

        public DataTable GetDataCommittee(string bidno, string bidrevision, string type)
        {
            try
            {
                string sql = string.Format(@"select * from ps_t_bid_selling_committee where bid_no='{0}' and bid_revision={1} and committee_type='{2}'", bidno, bidrevision, type);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    ViewState["IsUpdate"] = false;
                    return dt;
                }
                else
                {
                    DataTable dataTable = new DataTable("Member");

                    dataTable.Columns.Add("row_id", typeof(Int32));
                    dataTable.Columns.Add("bid_no", typeof(String));
                    dataTable.Columns.Add("bid_revision", typeof(Int32));
                    dataTable.Columns.Add("emp_id", typeof(String));
                    dataTable.Columns.Add("emp_name", typeof(String));
                    dataTable.Columns.Add("emp_short_position", typeof(String));
                    dataTable.Columns.Add("emp_full_position", typeof(String));
                    dataTable.Columns.Add("emp_status", typeof(String));
                    dataTable.Columns.Add("committee_type", typeof(String));

                    return dataTable;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    GetBidSelling(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);

                    ViewState["Member"]
                        = GetDataCommittee(Request.QueryString["bidno"], Request.QueryString["bidrevision"], Request.QueryString["type"]);

                    ViewState["Type"] = Request.QueryString["type"];

                    this.gv_employee2.DataSource = (DataTable)ViewState["Member"];
                    this.gv_employee2.DataBind();

                    if (this.gv_employee2.Rows.Count > 0)
                    {
                        this.alert_notfound.Visible = false;
                        this.alert_found.Visible = true;
                    }
                    else
                    {
                        this.alert_notfound.Visible = true;
                        this.alert_found.Visible = false;
                    }

                    switch (Request.QueryString["type"])
                    {
                        case "receive_open":
                            this.lb_title.Text = "คณะกรรมการรับและเปิดซอง";
                            break;

                        case "considered":
                            this.lb_title.Text = "คณะกรรมการพิจารณา";
                            break;

                        case "check":
                            this.lb_title.Text = "คณะกรรมการตรวจรับ";
                            break;

                        default:
                            break;
                    }

                    this.alert_found.InnerText = this.lb_title.Text;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='MemberData.aspx';", true);
                }
            }
        }

        protected void btn_membersearch_Click(object sender, EventArgs e)
        {
            GetApproverCommon(this.txt_empid.Text, this.txt_fullname.Text, this.txt_sposition.Text, this.txt_fposition.Text);
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["Member"];

            using (SqlConnection conn = new SqlConnection(zconn))
            {
                conn.Open();

                using (SqlCommand cmd = new SqlCommand(string.Format("delete from ps_t_bid_selling_committee where  bid_no = '{0}' and bid_revision = {1} and committee_type='{2}'", this.txt_bidNo.Text, this.txt_bidVersion.Text, ViewState["Type"].ToString()), conn))
                {
                    cmd.ExecuteNonQuery();
                }

                foreach (DataRow r in dt.Rows)
                {
                    using (SqlCommand cmd = new SqlCommand("sp_ins_committee", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@bid_no", this.txt_bidNo.Text);
                        cmd.Parameters.AddWithValue("@bid_revision", this.txt_bidVersion.Text);
                        cmd.Parameters.AddWithValue("@emp_id", r["emp_id"].ToString());
                        cmd.Parameters.AddWithValue("@emp_name", r["emp_name"].ToString());
                        cmd.Parameters.AddWithValue("@emp_short_position", r["emp_short_position"].ToString());
                        cmd.Parameters.AddWithValue("@emp_full_position", r["emp_full_position"].ToString());
                        cmd.Parameters.AddWithValue("@emp_status", r["emp_status"].ToString().ToLower());
                        cmd.Parameters.AddWithValue("@committee_type", r["committee_type"].ToString().ToLower());

                        cmd.ExecuteNonQuery();
                    }
                }

                conn.Close();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('has been successfully saved.'); window.location='MemberData.aspx';", true);
            }
        }

        protected void gv_employee_RowCommand(object sender, GridViewCommandEventArgs e)
        {

            if (e.CommandName == "committee")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = gv_employee.Rows[index];

                dataCommittee(row.Cells[1].Text, row.Cells[2].Text, row.Cells[3].Text, row.Cells[4].Text, "committee");

                this.alert_notfound.Visible = false;
                this.alert_found.Visible = true;

                ViewState["IsUpdate"] = true;
            }
            else if (e.CommandName == "member")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = this.gv_employee.Rows[index];

                dataCommittee(row.Cells[1].Text, row.Cells[2].Text, row.Cells[3].Text, row.Cells[4].Text, "member");

                this.alert_notfound.Visible = false;
                this.alert_found.Visible = true;

                ViewState["IsUpdate"] = true;
            }
        }

        private bool duplicateEmployee(DataTable dt, string empid)
        {
            DataView view = new DataView(dt);
            view.RowFilter = string.Format("emp_id = '{0}'", empid);

            if (view.ToTable().Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        private void dataCommittee(string empid, string fullname, string sposition, string fposition, string status)
        {
            DataTable dt = (DataTable)ViewState["Member"];

            if (duplicateEmployee(dt, empid))
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('This employee is already on the committee.');", true);
            }
            else
            {
                dt.Rows.Add(1, this.txt_bidNo.Text, int.Parse(this.txt_bidVersion.Text), empid, fullname, sposition, fposition, status, ViewState["Type"].ToString());
                ViewState["Member"] = dt;

                this.gv_employee2.DataSource = (DataTable)ViewState["Member"];
                this.gv_employee2.DataBind();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", string.Format("alert('{0} row added.');", fullname), true);
            }
        }

        protected void btn_cancel_Click(object sender, EventArgs e)
        {
            DataTable dt = (DataTable)ViewState["Member"];
            if (dt.Rows.Count > 0)
            {
                if ((Boolean)ViewState["IsUpdate"])
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "none", "confirmBox();", true);
                }
                else
                {
                    Response.Redirect("MemberData.aspx");
                }
            }
            else
            {
                Response.Redirect("MemberData.aspx");
            }
        }

        protected void gv_employee2_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "isDelete")
            {
                int index = Convert.ToInt32(e.CommandArgument);
                GridViewRow row = this.gv_employee2.Rows[index];

                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", string.Format("alert('{0} row deleted.');", row.Cells[2].Text), true);

                DataTable dt = (DataTable)ViewState["Member"];
                dt.Rows.Remove(dt.Rows[index]);

                ViewState["Member"] = dt;
                ViewState["IsUpdate"] = true;

                if (dt.Rows.Count > 0)
                {
                    this.gv_employee2.DataSource = dt;
                    this.DataBind();

                    this.alert_notfound.Visible = false;
                    this.alert_found.Visible = true;
                }
                else
                {
                    this.gv_employee2.DataSource = null;
                    this.DataBind();

                    this.alert_notfound.Visible = false;
                    this.alert_found.Visible = false;
                }
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            this.gv_employee.DataSource = null;
            this.gv_employee.DataBind();
            this.alert_results.Visible = false;
        }

        protected void gv_employee2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gv_employee2.Rows.Count > 0)
            {
                for (int i = 0; i <= this.gv_employee2.Rows.Count - 1; i++)
                {
                    Label label = (Label)this.gv_employee2.Rows[i].FindControl("lb_emp_status");

                    if (label.Text == "committee")
                    {
                        label.Text = "คณะกรรมการ";
                        this.gv_employee2.Rows[i].Cells[5].BackColor = Color.LightGreen;
                    }
                    else if (label.Text == "member")
                    {
                        label.Text = "ผู้ช่วย";
                        this.gv_employee2.Rows[i].Cells[5].BackColor = Color.LightBlue;
                    }
                }
            }
        }

        protected void gv_employee_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_employee.PageIndex = e.NewPageIndex;
            GetApproverCommon(this.txt_empid.Text, this.txt_fullname.Text, this.txt_sposition.Text, this.txt_fposition.Text);
        }
    }
}