﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class PriceScheduleUploadData : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        private void GetBidSelling(string bidnumber)
        {
            try
            {
                string sql = "select a.*, b.bid_desc, ";
                sql += " (select count(vendor_code) from ps_t_bid_selling as b where a.bid_no = b.bid_no and a.bid_revision = b.bid_revision ) total_bidder ";
                sql += " from ps_t_bid_monitoring as a inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision";
                sql += " where a.bid_no like '%" + bidnumber + "%' and (select count(vendor_code) from ps_t_bid_selling as b where a.bid_no = b.bid_no and a.bid_revision = b.bid_revision ) > 0";
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.alert_datanotfound.Visible = false;
                    this.gv_bidselling.DataSource = dt;
                    this.gv_bidselling.DataBind();
                }
                else
                {
                    this.alert_datanotfound.Visible = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetBidSelling(string.Empty);
            }
        }

        protected void btn_bidnosearch_Click(object sender, EventArgs e)
        {
            this.GetBidSelling(this.txt_bidnosearch.Text);
        }

        protected void gv_bidselling_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_bidselling.PageIndex = e.NewPageIndex;
            this.GetBidSelling(this.txt_bidnosearch.Text);
        }

        protected void gv_bidselling_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "view" || e.CommandName == "uploadprice")
            {
                string[] arg = new string[2];
                arg = e.CommandArgument.ToString().Split(';');

                if (e.CommandName == "view")
                {
                    Response.Redirect(string.Format("PriceSheduleUpView.aspx?bidno={0}&bidrevision={1}", arg[0], arg[1]));
                }
                else if (e.CommandName == "uploadprice")
                {
                    Response.Redirect(string.Format("PriceScheduleFileUp.aspx?bidno={0}&bidrevision={1}", arg[0], arg[1]));
                    //Response.Redirect(string.Format("PriceScheduleUploadAdd.aspx?bidno={0}&bidrevision={1}", arg[0], arg[1]));
                }
            }
        }
    }
}