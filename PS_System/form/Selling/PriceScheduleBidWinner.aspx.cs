﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class PriceScheduleBidWinner : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        clsPriceUpload2 clsPriceUpload = new clsPriceUpload2();
        OTFunctions zot = new OTFunctions();

        cExcel excel = new cExcel();

        private void bindDescription(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format("select bid_no, bid_revision, bid_desc from bid_no where bid_no = '{0}' and bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.h_bidnumberTitle.InnerText = bidno;
                    this.lb_bidnumber.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        void dspBidFileUpload(string bidderId)
        {
            try
            {
                string sql = @"select a.*, b.doctype_name, c.vendor_name, c.vendor_type ";
                sql += @"from ps_t_bid_selling_fileupload as a left join ps_m_doctype as b on a.doc_type = b.doctype_code  ";
                sql += @"left join ps_t_bid_selling_vendor as c on a.vendor_code = c.vendor_code ";
                sql += string.Format("where a.bidder_id = '{0}' ", bidderId);

                var dt = zdb.ExecSql_DataTable(sql, zconn);

                this.gv_digitalFile.DataSource = dt;
                this.gv_digitalFile.DataBind();
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void BindVendorData(string bidderId, string scheduleno, string bidnumber)
        {
            try
            {
                string sql = string.Format("select * from ps_t_bid_selling_vendor  where bidder_id = '{0}' and ref_schedule_no = '{1}' and bid_no = '{2}'", bidderId, scheduleno, bidnumber);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    foreach (System.Data.DataRow row in dt.Rows)
                    {
                        if (row["vendor_type"].ToString() != "Vendor")
                        {
                            this.lb_bidType.Text = row["vendor_type"].ToString();
                            this.lb_vendorName.Text = row["vendor_name"].ToString();
                            ViewState["vendorCode"] = row["vendor_code"].ToString();
                        }

                        this.lb_scheduleName.Text = row["ref_schedule_no"].ToString();

                        this.ddl_bidVendor.Items.Insert(0, new ListItem(row["vendor_name"].ToString(), row["vendor_code"].ToString()));
                    }

                    dspBidFileUpload(bidderId);

                    this.ddl_docType.DataSource = clsPriceUpload.GetDocType();
                    this.ddl_docType.DataTextField = "doctype_name";
                    this.ddl_docType.DataValueField = "doctype_code";
                    this.ddl_docType.DataBind();
                    this.ddl_docType.SelectedValue = "PSD";
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    bindDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                    BindVendorData(Request.QueryString["bidderid"], Request.QueryString["refschedule"], Request.QueryString["bidno"]);

                    ViewState["BidderID"] = Request.QueryString["bidderid"];
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                       string.Format("alert('data not found.'); window.location='BidTabulationData.aspx';"), true);
                }
            }
        }
        void getInvalidFileUpTb()
        {
            DataTable table = new DataTable("InvalidFileUpTb");
            table.Columns.Add("vendor_type");
            table.Columns.Add("vendor_name");
            table.Columns.Add("doc_name");
            table.Columns.Add("doctype_name");

            ViewState["InvalidFileUpTb"] = table;
        }
        private string uploadDoc(string xbid_no, string xbid_revision, string xschedule_name, string xbidder_id, string xvendor_code, string xdoc_type, string xdoc_name, byte[] xcontent, bool final_upload, int final_version)
        {
            var WF_NodeID = ConfigurationManager.AppSettings["WF_UPLOAD_PS"].ToString();

            var xbid_nodeid = zot.getNodeByName(WF_NodeID, xbid_no);
            if (xbid_nodeid == null)
            {
                xbid_nodeid = zot.createFolder(WF_NodeID, xbid_no);
            }
            var xschedule_nodeid = zot.getNodeByName(xbid_nodeid.ID.ToString(), xschedule_name);
            if (xschedule_nodeid == null)
            {
                xschedule_nodeid = zot.createFolder(xbid_nodeid.ID.ToString(), xschedule_name);
            }

            if (final_upload)
            {
                var xfinal_nodeid = zot.getNodeByName(xschedule_nodeid.ID.ToString(), "Final Version");
                if (xfinal_nodeid == null)
                {
                    xfinal_nodeid = zot.createFolder(xschedule_nodeid.ID.ToString(), "Final Version");
                }


                var xvendor_nodeid = zot.getNodeByName(xfinal_nodeid.ID.ToString(), xvendor_code);
                if (xvendor_nodeid == null)
                {
                    xvendor_nodeid = zot.createFolder(xfinal_nodeid.ID.ToString(), xvendor_code);
                }

                var xdoctype_nodeid = zot.getNodeByName(xvendor_nodeid.ID.ToString(), xdoc_type);
                if (xdoctype_nodeid == null)
                {
                    xdoctype_nodeid = zot.createFolder(xvendor_nodeid.ID.ToString(), xdoc_type);
                }
                var xversion_nodeid = zot.getNodeByName(xdoctype_nodeid.ID.ToString(), final_version.ToString());
                if (xversion_nodeid == null)
                {
                    xversion_nodeid = zot.createFolder(xdoctype_nodeid.ID.ToString(), final_version.ToString());
                }



                xdoc_name = xdoc_name.Replace(":", " ");
                return zot.uploadDoc(xversion_nodeid.ID.ToString(), xdoc_name, xdoc_name, xcontent, "").ToString();

            }
            else
            {
                var xbidder_nodeid = zot.getNodeByName(xschedule_nodeid.ID.ToString(), xbidder_id);
                if (xbidder_nodeid == null)
                {
                    xbidder_nodeid = zot.createFolder(xschedule_nodeid.ID.ToString(), xbidder_id);
                }

                var xvendor_nodeid = zot.getNodeByName(xbidder_nodeid.ID.ToString(), xvendor_code);
                if (xvendor_nodeid == null)
                {
                    xvendor_nodeid = zot.createFolder(xbidder_nodeid.ID.ToString(), xvendor_code);
                }

                var xdoctype_nodeid = zot.getNodeByName(xvendor_nodeid.ID.ToString(), xdoc_type);
                if (xdoctype_nodeid == null)
                {
                    xdoctype_nodeid = zot.createFolder(xvendor_nodeid.ID.ToString(), xdoc_type);
                }

                xdoc_name = xdoc_name.Replace(":", " ");
                return zot.uploadDoc(xdoctype_nodeid.ID.ToString(), xdoc_name, xdoc_name, xcontent, "").ToString();
            }



        }
        bool insBidSelling(string bidderId, string refSchedule, string vendorCode, string fileName, string docType, int docRevision, string nodId)
        {
            try
            {
                SqlTransaction transaction;
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        using (SqlCommand cmd = new SqlCommand("sp_ins_bid_selling_fileupload", conn, transaction))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@bidder_id", bidderId);
                            cmd.Parameters.AddWithValue("@bid_no", this.lb_bidnumber.Text);
                            cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));
                            cmd.Parameters.AddWithValue("@ref_schedule_no", refSchedule);
                            cmd.Parameters.AddWithValue("@vendor_code", vendorCode);
                            cmd.Parameters.AddWithValue("@doc_type", docType);
                            cmd.Parameters.AddWithValue("@doc_name", fileName);
                            cmd.Parameters.AddWithValue("@doc_version", docRevision);
                            cmd.Parameters.AddWithValue("@parent_node_id", ConfigurationManager.AppSettings["WF_UPLOAD_PS"].ToString());
                            cmd.Parameters.AddWithValue("@doc_node_id", nodId);
                            cmd.Parameters.AddWithValue("@status_bid", "bidwinner");

                            cmd.ExecuteNonQuery();
                        }

                        transaction.Commit();
                    }
                    catch (SqlException sqlEx)
                    {
                        zot.deleteNodeID(nodId, nodId);
                        transaction.Rollback();
                        conn.Close();

                        return false;
                        throw sqlEx;
                    }

                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        int fileBidVersion(string bidderId, string vendorCode, string scheduleNo)
        {
            try
            {
                string sql = string.Format("select  (max(doc_version) + 1 ) as doc_version from ps_t_bid_selling_fileupload where bidder_id = '{0}' and ref_schedule_no = '{1}' and vendor_code = '{2}'", bidderId, scheduleNo, vendorCode);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    return int.Parse(dt.Rows[0]["doc_version"].ToString());
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        protected void btn_digitalFileUpload_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.fup_sinjoi.HasFile)
                {
                    this.alert_upload.Visible = true;
                    this.btn_digitalFileUpload.Enabled = false;

                    string nodeid = "";

                    getInvalidFileUpTb();
                    DataTable table = (DataTable)ViewState["InvalidFileUpTb"];

                    foreach (HttpPostedFile uploadedFile in fup_sinjoi.PostedFiles)
                    {
                        using (Stream stream = uploadedFile.InputStream)
                        {
                            if (checkFileUpload(this.lb_bidnumber.Text, this.txt_bidVersion.Text, this.lb_scheduleName.Text, stream))
                            {
                                using (BinaryReader br = new BinaryReader(stream))
                                {
                                    byte[] bytes = br.ReadBytes((Int32)stream.Length);

                                    if (bytes.Length > 0)
                                    {
                                        int docVersion = fileBidVersion(ViewState["BidderID"].ToString(), ViewState["vendorCode"].ToString(), this.lb_scheduleName.Text);
                                        nodeid
                                            = uploadDoc(this.lb_bidnumber.Text, this.txt_bidVersion.Text, this.lb_scheduleName.Text, ViewState["BidderID"].ToString(), this.ddl_bidVendor.SelectedValue, this.ddl_docType.SelectedValue, uploadedFile.FileName, bytes, true, docVersion);

                                        insBidSelling
                                            (ViewState["BidderID"].ToString(), this.lb_scheduleName.Text, this.ddl_bidVendor.SelectedValue, uploadedFile.FileName, this.ddl_docType.SelectedValue, docVersion, nodeid);

                                        excel.uploadData(int.Parse(nodeid), ViewState["BidderID"].ToString(), this.ddl_bidVendor.SelectedValue, this.lb_bidnumber.Text, int.Parse(this.txt_bidVersion.Text), this.lb_scheduleName.Text, docVersion, stream, "123456");
                                    }
                                    else
                                    {
                                        return;
                                    }
                                }
                            }
                            else
                            {
                                table.Rows.Add(this.ddl_bidVendor.SelectedValue, this.ddl_bidVendor.SelectedItem.Text, this.ddl_docType.SelectedItem.Text, uploadedFile.FileName);
                            }
                        }
                    }

                    if (table.Rows.Count > 0)
                    {
                        this.gv_invalidFileUp.DataSource = table;
                        this.gv_invalidFileUp.DataBind();
                        ViewState["InvalidFileUpTb"] = table;
                    }

                    this.alert_upload.Visible = false;
                    this.btn_digitalFileUpload.Enabled = true;

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('has been file upload saved.');", true);
                    dspBidFileUpload(ViewState["BidderID"].ToString());
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('file upload not found. please try again.');", true);
                }
            }
            catch (Exception ex)
            {
                this.alert_upload.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to save data.');", true);
                throw ex;
            }
        }

        protected void gv_digitalFile_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "IsDelete")
            {
                try
                {
                    string[] arg = e.CommandArgument.ToString().Split(';');
                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();
                        using (SqlCommand com = new SqlCommand(string.Format("delete from ps_t_bid_selling_fileupload where row_id = {0}", arg[0]), conn))
                        {
                            com.ExecuteNonQuery();

                            zot.deleteNodeID(arg[1], arg[1]);

                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('data is deleted.');", true);

                            dspBidFileUpload(ViewState["BidderID"].ToString());
                        }
                        conn.Close();
                    }
                }
                catch (Exception ex)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to deleted data.');", true);
                    throw ex;
                }
            }
            else if (e.CommandName == "IsDownload")
            {
                var xcontent = zot.downloadDoc(e.CommandArgument.ToString());
                var xnode = zot.getNodeByID(e.CommandArgument.ToString());

                Response.Clear();
                MemoryStream ms = new MemoryStream(xcontent);
                Response.ContentType = "application/pdf";
                Response.AddHeader("content-disposition", "attachment;filename=" + xnode.Name);
                Response.Buffer = true;
                ms.WriteTo(Response.OutputStream);
                Response.End();
            }
        }

        protected void gv_invalidFileUp_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }
        bool checkFileUpload(string bid_no, string bid_revision, string schedule_name, Stream stream)
        {
            //if (excel.validateFile(bid_no, bid_revision, schedule_name, stream))
            //{
            //    return true;
            //}
            //else
            //{
            //    return false;
            //}

            return true;
        }
    }
}