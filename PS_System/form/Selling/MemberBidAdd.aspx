﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="MemberBidAdd.aspx.cs" Inherits="PS_System.form.Selling.MemberBidAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scriptMag" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePal" runat="server">
        <ContentTemplate>
            <div class="container mb-4">
                <div class="row mb-4">
                    <h6>EGAT - Committee Data Adding</h6>
                    <div class="ml-auto">
                        <h6>
                            <asp:Label ID="lb_title" runat="server" Text="Label" CssClass="text-primary"></asp:Label>
                        </h6>
                    </div>
                </div>
                <div class="card mb-4">
                    <div class="card-header">
                        ข้อมูลเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="form-row">
                            <div class="form-group col-12">
                            </div>
                            <div class="form-group col-8">
                                <label>Bid No.</label>
                                <asp:Label ID="txt_bidNo" runat="server" CssClass="form-control"></asp:Label>
                            </div>
                            <div class="form-group col-4">
                                <label>Bid Version.</label>
                                <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                            </div>
                            <div class="form-group col-12">
                                <label>Bid Description.</label>
                                <asp:Label ID="txt_bidDescription" runat="server" Height="150" CssClass="form-control"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-body">
                        <h6 class="card-title">เลือกคณะกรรมการและผู้ช่วย</h6>
                        <p class="card-text">ค้นหาคณะกรรมการและผู้ช่วย จากช่องค้นหาด้านล่าง</p>
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <asp:TextBox ID="txt_empid" runat="server" CssClass="form-control" MaxLength="50" placeholder="by Employee ID"></asp:TextBox>
                                <asp:TextBox ID="txt_fullname" runat="server" CssClass="form-control" MaxLength="50" placeholder="by Employee Name"></asp:TextBox>
                                <asp:TextBox ID="txt_sposition" runat="server" CssClass="form-control" MaxLength="50" placeholder="by Short Position"></asp:TextBox>
                                <asp:TextBox ID="txt_fposition" runat="server" CssClass="form-control" MaxLength="50" placeholder="by Full Position"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_membersearch" runat="server" Text='<i class="bi bi-search"></i>' ToolTip="Click to search." OnClick="btn_membersearch_Click" CausesValidation="false" CssClass="btn btn-info" />
                                </div>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_refresh" runat="server" Text='<i class="bi bi-arrow-repeat"></i>' ToolTip="Click to refresh." OnClick="btn_refresh_Click" CausesValidation="false" CssClass="btn btn-secondary" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="alert alert-info text-center" role="alert" runat="server" id="alert_results" visible="false">
                                Search results.
                            </div>
                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_datanotfound" visible="false">
                                Data not found.
                            </div>
                            <asp:GridView ID="gv_employee" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_employee_PageIndexChanging"
                                OnRowCommand="gv_employee_RowCommand" Font-Names="tahoma" Font-Size="9pt">
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="emp_id" HeaderText="Employee ID" />
                                    <asp:BoundField DataField="emp_name" HeaderText="Employee name" />
                                    <asp:BoundField DataField="short_position" HeaderText="Short Position" />
                                    <asp:BoundField DataField="full_position" HeaderText="Full Position" />
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-success btn-sm" CommandName="committee" Text='<i class="bi bi-plus-circle mr-2"></i>กรรมการ' runat="server" CommandArgument="<%# Container.DataItemIndex %>" />
                                            <asp:LinkButton CssClass="btn btn-primary btn-sm" CommandName="member" Text='<i class="bi bi-plus-circle mr-2"></i>ผู้ช่วย' runat="server" CommandArgument="<%# Container.DataItemIndex %>"></asp:LinkButton>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                        <div class="form-group">
                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_notfound">
                                Empty data.
                            </div>
                            <div class="alert alert-info text-center" role="alert" runat="server" id="alert_found" visible="false">
                                List of employee to committee.
                            </div>
                            <asp:GridView ID="gv_employee2" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" OnRowDataBound="gv_employee2_RowDataBound"
                                OnRowCommand="gv_employee2_RowCommand" Font-Names="tahoma" Font-Size="9pt">
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="emp_id" HeaderText="Employee ID" />
                                    <asp:BoundField DataField="emp_name" HeaderText="Employee name" />
                                    <asp:BoundField DataField="emp_short_position" HeaderText="Short Position" />
                                    <asp:BoundField DataField="emp_full_position" HeaderText="Full Position" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lb_emp_status" runat="server" Text='<%# Eval("emp_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:LinkButton CssClass="btn btn-danger btn-sm" Text='<i class="bi bi-x-circle"></i>' ToolTip="Click to delete." runat="server" CommandName="isDelete" CommandArgument="<%# Container.DataItemIndex %>" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <asp:LinkButton ID="btn_cancel" runat="server" Text='<i class="bi bi-arrow-return-left mr-2"></i>Close' CssClass="btn btn-secondary btn-sm" OnClick="btn_cancel_Click" />
                            <asp:LinkButton ID="btn_save" runat="server" Text='<i class="bi bi-save mr-2"></i>Save' CssClass="btn btn-success btn-sm" OnClick="btn_save_Click" OnClientClick="if (!confirm('Do you want to add this committee?')) return false;" />
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript">
        function confirmBox() {
            if (confirm("The data is not recorded yet. Do you want to cancel?")) {
                window.location.href = "MemberData.aspx";
            }
        }
    </script>
</asp:Content>
