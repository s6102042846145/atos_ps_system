﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="BidSelling2.aspx.cs" Inherits="PS_System.form.Selling.BidSelling2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scriptMag" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePal" runat="server">
        <ContentTemplate>
            <div class="container mb-4">
                <div class="row mb-3">
                    <h5>EGAT - Bid Selling Management</h5>
                    <div class="ml-auto">
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        ตารางสรุปข้อมูลผู้ซื้อเอกสารประกวดราคา
                        <%--<a href="VendorAdd.aspx" target="_blank" role="button" class="btn btn-primary btn-sm"><i class="bi bi-person-plus mr-2"></i> New Vendor</a>--%>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <asp:TextBox ID="txt_bidnosearch" runat="server" CssClass="form-control" MaxLength="50" placeholder="bid number search"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_bidnosearch" runat="server" Text='<i class="bi bi-search"></i>' ToolTip="Click to search." OnClick="btn_bidnosearch_Click" CausesValidation="false" CssClass="btn btn-primary" />
                                </div>
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary" title="Click to refresh" onclick="window.location.reload();"><i class="bi bi-arrow-repeat"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_datanotfound">
                            Data not found.
                        </div>
                        <asp:GridView ID="gv_bidselling" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_bidselling_PageIndexChanging"
                            OnRowCommand="gv_bidselling_RowCommand" DataKeyNames="bid_no" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found.">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderStyle-Width="10">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="bid_no" HeaderText="Bid Number" />
                                <%--<asp:BoundField DataField="bid_revision" HeaderText="Bid Revision" />--%>
                                <asp:TemplateField>
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lb_description" Text='<%# Bind("bid_desc") %>' Font-Size="XX-Small"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="total_bidder" HeaderText="Total Bidder" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton CssClass="btn btn-success btn-sm" Text='<i class="bi bi-plus-circle mr-2"></i>Add' runat="server" CommandName="view" CommandArgument='<%#Eval("bid_no") + ";" + Eval("bid_revision")%>' ToolTip="Click to view." />
                                        <asp:LinkButton CssClass="btn btn-success btn-sm" Text='<i class="bi bi-plus-circle"></i>' runat="server" CommandName="addvendor" CommandArgument='<%#Eval("bid_no") + ";" + Eval("bid_revision")%>' ToolTip="Click to add." Visible="false"/>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle CssClass="small" />
                        </asp:GridView>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="Index.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
