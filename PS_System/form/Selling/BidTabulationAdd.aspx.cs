﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidTabulationAdd : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void GetBidSelling()
        {
            try
            {
                //string sql = "select a.*, b.bid_desc,  ";
                //sql += " (select count(vendor_code) from ps_t_bid_selling as b where a.bid_no = b.bid_no and a.bid_revision = b.bid_revision ) total_bidder ";
                //sql += " from ps_t_bid_monitoring as a inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision";

                string sql = "select distinct bid_no, bid_no + ';' + CAST(bid_revision as varchar(10)) as bidno_revision from ps_t_bid_selling";
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_bidNumber.DataSource = dt;
                    this.ddl_bidNumber.DataTextField = "bid_no";
                    this.ddl_bidNumber.DataValueField = "bidno_revision";
                    this.ddl_bidNumber.DataBind();

                    this.ddl_bidNumber.Items.Insert(0, "-- Select Bid Number ---");
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        void RefSchedule(string bidNumber, string bidRevision)
        {
            try
            {
                string sql = string.Format("select * from ps_m_bid_schedule where bid_no = '{0}' and bid_revision = {1}", bidNumber, bidRevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_scheduleName.DataSource = dt;
                    this.ddl_scheduleName.DataTextField = "schedule_name";
                    this.ddl_scheduleName.DataValueField = "schedule_name";
                    this.ddl_scheduleName.DataBind();
                }
                else
                {
                    this.ddl_scheduleName.Items.Clear();
                    this.ddl_scheduleName.Items.Insert(0, "-- Schedule not found ---");
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                GetBidSelling();
                this.ddl_scheduleName.Items.Insert(0, "-- Schedule not found ---");
            }
        }

        protected void ddl_bidNumber_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (this.ddl_bidNumber.Items.Count > 0)
            {
                if(this.ddl_bidNumber.SelectedIndex > 0)
                {
                    string[] vs = this.ddl_bidNumber.SelectedValue.Split(';');
                    RefSchedule(this.ddl_bidNumber.SelectedItem.Text, vs[1]);
                }
            }
        }

        protected void btn_searchBid_Click(object sender, EventArgs e)
        {
            if (this.ddl_bidNumber.Items.Count > 0)
            {
                if (this.ddl_bidNumber.SelectedIndex > 0)
                {
                    //string sql = " select a.*, b.vendor_name, price_score ";
                    //sql += " from ps_t_bid_selling_committee_comment as a left join ps_m_vendor as b on a.vendor_code = b.vendor_code ";

                    string sql = "select a.*, ISNULL(b.technical_score,0) as technical_score, ISNULL(b.price_score,0) as price_score from ps_t_bid_selling_vendor as a left join ps_t_bid_selling_committee_comment as b on a.vendor_code = b.vendor_code ";
                    sql += string.Format(" where a.bid_no = '{0}' and a.ref_schedule_no = '{1}' ", this.ddl_bidNumber.SelectedItem.Text, this.ddl_scheduleName.SelectedValue);

                    using (DataTable dt = zdb.ExecSql_DataTable(sql, zconn))
                    {
                        this.gv_bidSelling.DataSource = dt;
                        this.gv_bidSelling.DataBind();

                        if (dt.Rows.Count > 0)
                        {
                            this.alert_bidTab.Visible = false;
                        }
                        else
                        {
                            this.alert_bidTab.Visible = true;
                            //ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('bidder not found.'); window.location='BidTabulationAdd.aspx';", true);
                        }
                    }

                    string[] vs = this.ddl_bidNumber.SelectedValue.Split(';');

                    this.lb_bidNumber.Text = this.ddl_bidNumber.SelectedItem.Text;
                    this.lb_bidRevision.Value = vs[1];
                    this.lb_scheduleName.Text = this.ddl_scheduleName.SelectedValue;

                    using (DataTable dataTable = zdb.ExecSql_DataTable(string.Format("select *,(bidder_id +';'+vendor_code) as vendor_id from ps_t_bid_selling where bid_no = '{0}'", this.ddl_bidNumber.SelectedItem.Text), zconn))
                    {
                        this.ddl_bidWinner.DataSource = dataTable;
                        this.ddl_bidWinner.DataTextField = "vendor_name";
                        this.ddl_bidWinner.DataValueField = "vendor_id";
                        this.ddl_bidWinner.DataBind();
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('bid number not found.');", true);
                }
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('bid number not found.');", true);
            }
        }

        protected void btn_refresh_Click(object sender, EventArgs e)
        {
            Response.Redirect("BidTabulationAdd.aspx", false);
        }

        bool checkBidWinner(string bidNumber, string bidSchedule)
        {
            try
            {
                string sql = string.Format("select * from ps_t_bid_selling_winner where bid_no = '{0}' and schedule_name = '{1}'", bidNumber, bidSchedule);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.lb_bidNumber.Text == "" || this.lb_bidRevision.Value == "" || this.lb_scheduleName.Text == "")
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('data not found. please try again.');", true);
                    return;
                }
                if (checkBidWinner(this.lb_bidNumber.Text, this.lb_scheduleName.Text))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('bid number has the winner. please try again.');", true);
                    return;
                }
                else
                {
                    string[] vs = this.ddl_bidWinner.SelectedValue.Split(';');

                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();
                        using (SqlCommand cmd = new SqlCommand("sp_ins_bid_selling_winner", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@bid_no", this.lb_bidNumber.Text);
                            cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.lb_bidRevision.Value));
                            cmd.Parameters.AddWithValue("@schedule_name", this.lb_scheduleName.Text);
                            cmd.Parameters.AddWithValue("@winner_bidder_id", vs[0]);
                            cmd.Parameters.AddWithValue("@technical_score", decimal.Parse(this.txt_techScore.Text));
                            cmd.Parameters.AddWithValue("@price_score", decimal.Parse(this.txt_priceScore.Text));
                            cmd.Parameters.AddWithValue("@comment", this.txt_bidComment.Text);
                            cmd.Parameters.AddWithValue("@updated_datetime", DateTime.Now);
                            cmd.Parameters.AddWithValue("@updated_by", "123456");
                            cmd.Parameters.AddWithValue("@vendor_code", vs[1]);

                            cmd.ExecuteNonQuery();
                        }
                        conn.Close();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('has been successful saved.'); window.location='BidTabulationData.aspx';", true);
                    }
                }

            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to save data.');", true);
                throw ex;
            }
        }

        protected void gv_bidSelling_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                //if (e.CommandName == "IsSelect")
                //{
                //    string[] arg = e.CommandArgument.ToString().Split(';');
                //    this.lb_bidderId.Value = arg[1];

                //    using (Label label = (Label)this.gv_bidSelling.Rows[int.Parse(arg[0])].FindControl("lb_vendorCode"))
                //    {
                //        this.lb_vendorCode.Value = label.Text;
                //    }
                //    using (Label label = (Label)this.gv_bidSelling.Rows[int.Parse(arg[0])].FindControl("lb_vendorName"))
                //    {
                //        this.ddl_bidWinner.SelectedItem.Text = label.Text;
                //    }
                //    using (Label label = (Label)this.gv_bidSelling.Rows[int.Parse(arg[0])].FindControl("lb_techScore"))
                //    {
                //        this.txt_techScore.Text = label.Text;
                //    }
                //    using (Label label = (Label)this.gv_bidSelling.Rows[int.Parse(arg[0])].FindControl("lb_priceScore"))
                //    {
                //        this.txt_priceScore.Text = label.Text;
                //    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}