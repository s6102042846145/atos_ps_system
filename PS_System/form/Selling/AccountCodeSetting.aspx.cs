﻿using System;
using PS_Library;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace PS_System.form.Selling
{
    public partial class AccountCodeSetting : System.Web.UI.Page
    {
        clsBidTabulation objTab = new clsBidTabulation();
        CultureInfo th = new CultureInfo("th-TH");
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        string zContract_NodeID = ConfigurationManager.AppSettings["Contract_NodeID"].ToString();

        OTFunctions zot = new OTFunctions();
        #endregion
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    hidLogin.Value = "z597204";

                if (Request.QueryString["zmode"] != null)
                { hidMode.Value = Request.QueryString["zmode"]; }
                else
                { hidMode.Value = "ACC SETTING"; }

                lblMode.Text ="     MODE: "+ hidMode.Value.ToUpper();

                if (Request.QueryString["zcontract_no"] != null)
                { hidContractNo.Value = Request.QueryString["zcontract_no"]; }
                else
                {
                    hidContractNo.Value = "";

                }

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;

                BindData();
                ini_Mode();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        private void ini_Mode()
        {
            lblTitle.Text = "";
            pAccStaff.Visible = false;
            pAssignTo.Visible = false;
            pPurchase.Visible = false;
            pAccApprove.Visible = false; 

            if (hidMode.Value.ToUpper() == "SUBMIT TO ACC")
            {
                lblTitle.Text = hidMode.Value;
                pPurchase.Visible = true;
            }
            else if (hidMode.Value.ToUpper() == "ASSIGN TO")
            {
                lblTitle.Text = hidMode.Value;
                pAssignTo.Visible = true;
            }
            else if (hidMode.Value.ToUpper() == "ACC SETTING")
            {
                lblTitle.Text = hidMode.Value;
                pAccStaff.Visible = true; btnTestRun.Visible = true; btnSave.Visible = true;
            }
            else if (hidMode.Value.ToUpper() == "ACC APPROVE")
            {
                lblTitle.Text = hidMode.Value;
                pAccApprove.Visible = true;
            }
            else if (hidMode.Value.ToUpper().Contains("VIEW"))
            {
                lblTitle.Text = hidMode.Value;
                pAccStaff.Visible = true;btnTestRun.Visible = true; btnSave.Visible = false; 
            }
        }
        private void checkWFData(string xcontract_no)
        {
            string sql = @" select* from wf_ps_acc_setting where contract_no = '"+xcontract_no+ "' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                // Check Submit to Name
                ucEmp1.setByEmpID(dr["submitto_acc_empid"].ToString());
                btnSubmit.Visible = false;
                btnCancel.Visible = false;
            }
        }
        private void BindData()
        {
            DataTable dtallComment = objTab.getDataXls();
            string strid = "";
            string tempid = "";
            foreach (DataRow dr in dtallComment.Rows)
            {
                strid = objTab.getTemplateID(dr["bid_no"].ToString(), dr["bid_revision"].ToString(), dr["schedule_name"].ToString());
                tempid += (tempid == "" ? "" : ",") + strid;
            }
            // DataTable dtTemp = objTab.getDataXls("", tempid.Replace(",", "','"));
            DataTable dtTemp = objTab.getAccXls("");
            if (dtTemp.Rows.Count > 0)
            {
                gvXls.DataSource = dtTemp;
                gvXls.DataBind();
            }
            ddlContact.Items.Clear();
            DataTable dtCon = dtTemp.DefaultView.ToTable(true, "contract_no");
            ddlContact.DataSource = dtCon;
            ddlContact.DataTextField = "contract_no";
            ddlContact.DataValueField = "contract_no";
            ddlContact.DataBind();
            ddlContact.Items.Insert(0, new ListItem("<-- Select -->", ""));
            try { ddlContact.SelectedValue = hidContractNo.Value; }
            catch { ddlContact.SelectedIndex = 0; }
            if (ddlContact.SelectedIndex > 0)
            {
                string sql = "select * from ps_t_bid_selling_cmpo where contract_no = '" + ddlContact.SelectedValue.ToString() + "' ";
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    var dr = ds.Tables[0].Rows[0];
                    hidBidNo.Value = dr["bid_no"].ToString();
                    hidBidRev.Value = dr["bid_revision"].ToString();
                    hidContractNo.Value = dr["contract_no"].ToString();
                    checkWFData(hidContractNo.Value);
                }
            }
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            bool chk = false;
            foreach (GridViewRow gvr in gvXls.Rows)
            {
                string bidno = ((Label)gvr.FindControl("lbBidno")).Text;
                string rev = ((Label)gvr.FindControl("lbRev")).Text;
                string sch = ((Label)gvr.FindControl("lbSch")).Text;
                string itmno = ((Label)gvr.FindControl("lbItmno")).Text;
                string rows = ((Label)gvr.FindControl("lbRow")).Text;
                string gl = ((TextBox)gvr.FindControl("txtGL")).Text;
                string cc = ((TextBox)gvr.FindControl("txtCC")).Text;
                string wbs = ((TextBox)gvr.FindControl("txtWBS")).Text;
                string net = ((TextBox)gvr.FindControl("txtNet")).Text;
                string fund = ((TextBox)gvr.FindControl("txtFund")).Text;
                string asset = ((TextBox)gvr.FindControl("txtAsset")).Text;
                string inorder = ((TextBox)gvr.FindControl("txtInorder")).Text;
                string saleorder = ((TextBox)gvr.FindControl("txtSaleorder")).Text;

                DropDownList ddl = (DropDownList)gvr.FindControl("ddlAccAssi");
                //if (ddl.SelectedValue != "")
                //{

                #region
                if (ddl.SelectedValue == "K")
                {
                    //if (cc != "")
                    //{ 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", cc, "", "", "", "", ddl.SelectedValue, "", "", ""); 
                    //}
                }
                else if (ddl.SelectedValue == "Z")
                {
                    //   if (cc != "")
                    //    { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, gl, cc, "", "", "", rows, ddl.SelectedValue, "", "", ""); 
                    //    }

                }
                else if (ddl.SelectedValue == "F")
                {
                    //   if (inorder != "")
                    //    { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", "", "", "", "", rows, ddl.SelectedValue, "", inorder, ""); 
                    //    }

                }
                else if (ddl.SelectedValue == "J")
                {
                    //   if (cc != "")
                    //   { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", cc, "", "", "", rows, ddl.SelectedValue, "", "", ""); 
                    //    }


                }
                else if (ddl.SelectedValue == "W")
                {
                    //   if (cc != "" && wbs != "")
                    //   { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", cc, wbs, "", "", "", ddl.SelectedValue, "", "", ""); 
                    //   }

                }
                else if (ddl.SelectedValue == "C")
                {
                    //   if (cc != "" && saleorder != "")
                    //   { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", cc, "", "", "", rows, ddl.SelectedValue, "", "", saleorder); 
                    //   }


                }
                else if (ddl.SelectedValue == "A")
                {
                    //  if (asset != "" && inorder != "")
                    //  { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", "", "", "", "", rows, ddl.SelectedValue, asset, inorder, ""); 
                    //  }


                }
                else if (ddl.SelectedValue == "Y")
                {
                    // if (asset != "" && wbs != "")
                    //  {
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", "", wbs, "", "", rows, ddl.SelectedValue, asset, "", ""); 
                    //  }

                }
                else if (ddl.SelectedValue == "P")
                {
                    // if (gl != "" && wbs != "")
                    // {
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, gl, "", wbs, "", "", rows, ddl.SelectedValue, "", "", ""); 
                    // }


                }
                else if (ddl.SelectedValue == "Q")
                {

                    //  if (wbs != "" && fund != "")
                    //  { 
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", "", wbs, "", fund, rows, ddl.SelectedValue, "", "", ""); 
                    //  }


                }
                else if (ddl.SelectedValue == "N")
                {
                    // if (gl != "" && cc != "" && net != "")
                    //{
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, gl, cc, "", net, "", rows, ddl.SelectedValue, "", "", "");
                    //}

                }
                else
                {
                    objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, "", "", "", "", "", rows, "", "", "", "");
                }
                //objTab.updateTemplate(bidno, rev, hidLogin.Value, sch, itmno, gl, cc, wbs, net, fund, rows, ddl.SelectedValue, asset, inorder, saleorder);
                #endregion

                //}
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true); 
                BindData();
                //if (chk == true) { ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true); BindData(); }
                //else ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Data.');", true);
            }
        }

        protected void ddlAccAssi_SelectedIndexChanged(object sender, EventArgs e)
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow gvr = ddl.NamingContainer as GridViewRow;
            TextBox gl = (TextBox)gvr.FindControl("txtGL");
            TextBox cc = (TextBox)gvr.FindControl("txtCC");
            TextBox wbs = (TextBox)gvr.FindControl("txtWBS");
            TextBox net = (TextBox)gvr.FindControl("txtNet");
            TextBox fund = (TextBox)gvr.FindControl("txtFund");
            TextBox asset = (TextBox)gvr.FindControl("txtAsset");
            TextBox inorder = (TextBox)gvr.FindControl("txtInorder");
            TextBox saleorder = (TextBox)gvr.FindControl("txtSaleorder");

            Label reqgl = (Label)gvr.FindControl("lblgl");
            Label reqcc = (Label)gvr.FindControl("lblcc");
            Label reqwbs = (Label)gvr.FindControl("lblwbs");
            Label reqnet = (Label)gvr.FindControl("lblnet");
            Label reqfund = (Label)gvr.FindControl("lblfund");
            Label reqasse = (Label)gvr.FindControl("lblasse");
            Label reqin = (Label)gvr.FindControl("lblinorder");
            Label reqsale = (Label)gvr.FindControl("lblsale");
            if (ddl.SelectedValue =="K")
            {
                reqgl.Visible = false;
                reqcc.Visible = true;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                gl.Enabled = false;
                cc.Enabled = true;
                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                inorder.Enabled = false;
                saleorder.Enabled = false;

            }
            else if (ddl.SelectedValue == "Z")
            {
                reqgl.Visible = true;
                reqcc.Visible = true;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                cc.Enabled = true;
                inorder.Enabled = true;
                gl.Enabled = true;

                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "F")
            {
                reqgl.Visible = false;
                reqcc.Visible = false;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = true;
                reqsale.Visible = false;

                cc.Enabled = true;
                inorder.Enabled = true;

                gl.Enabled = false;
                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "J")
            {
                reqgl.Visible = false;
                reqcc.Visible = true;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                cc.Enabled = true;
                inorder.Enabled = true;

                gl.Enabled = false;
                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "W")
            {
                reqgl.Visible = false;
                reqcc.Visible = true;
                reqwbs.Visible = false;
                reqnet.Visible = true;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                cc.Enabled = true;
                net.Enabled = true;

                gl.Enabled = false;
                wbs.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                inorder.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "C")
            {
                reqgl.Visible = false;
                reqcc.Visible = true;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = true;

                cc.Enabled = true;
                saleorder.Enabled = true;

                gl.Enabled = false;
                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                inorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "A")
            {
                reqgl.Visible = false;
                reqcc.Visible = false;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = true;
                reqin.Visible = true;
                reqsale.Visible = false;

                asset.Enabled = true;
                inorder.Enabled = true;

                gl.Enabled = false;
                cc.Enabled = false;
                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "Y")
            {
                reqgl.Visible = false;
                reqcc.Visible = false;
                reqwbs.Visible = true;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = true;
                reqin.Visible = false;
                reqsale.Visible = false;

                asset.Enabled = true;
                wbs.Enabled = true;

                gl.Enabled = false;
                cc.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                inorder.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "P")
            {
                reqgl.Visible = true;
                reqcc.Visible = false;
                reqwbs.Visible = true;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                gl.Enabled = true;
                wbs.Enabled = true;

                cc.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                inorder.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "Q")
            {
                reqgl.Visible = false;
                reqcc.Visible = false;
                reqwbs.Visible = true;
                reqnet.Visible = false;
                reqfund.Visible = true;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                inorder.Enabled = true;
                wbs.Enabled = true;
                fund.Enabled = true;

                gl.Enabled = false;
                cc.Enabled = false;
                net.Enabled = false;
                asset.Enabled = false;
                saleorder.Enabled = false;
            }
            else if (ddl.SelectedValue == "N")
            {
                reqgl.Visible = true;
                reqcc.Visible = true;
                reqwbs.Visible = false;
                reqnet.Visible = true;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;

                gl.Enabled = true;
                cc.Enabled = true;
                net.Enabled = true;

                wbs.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                inorder.Enabled = false;
                saleorder.Enabled = false;
            }
            else
            {
                reqgl.Visible = false;
                reqcc.Visible = false;
                reqwbs.Visible = false;
                reqnet.Visible = false;
                reqfund.Visible = false;
                reqasse.Visible = false;
                reqin.Visible = false;
                reqsale.Visible = false;
                gl.Enabled = false;
                cc.Enabled = false;
                wbs.Enabled = false;
                net.Enabled = false;
                fund.Enabled = false;
                asset.Enabled = false;
                inorder.Enabled = false;
                saleorder.Enabled = false;
            }
        }

        protected void gvXls_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                DropDownList ddl = (DropDownList)e.Row.FindControl("ddlAccAssi");
                HiddenField hidAcc = (HiddenField)e.Row.FindControl("hidddlAcc");
                TextBox gl = (TextBox)e.Row.FindControl("txtGL");
                TextBox cc = (TextBox)e.Row.FindControl("txtCC");
                TextBox wbs = (TextBox)e.Row.FindControl("txtWBS");
                TextBox net = (TextBox)e.Row.FindControl("txtNet");
                TextBox fund = (TextBox)e.Row.FindControl("txtFund");
                TextBox asset = (TextBox)e.Row.FindControl("txtAsset");
                TextBox inorder = (TextBox)e.Row.FindControl("txtInorder");
                TextBox saleorder = (TextBox)e.Row.FindControl("txtSaleorder");

                Label reqgl = (Label)e.Row.FindControl("lblgl");
                Label reqcc = (Label)e.Row.FindControl("lblcc");
                Label reqwbs = (Label)e.Row.FindControl("lblwbs");
                Label reqnet = (Label)e.Row.FindControl("lblnet");
                Label reqfund = (Label)e.Row.FindControl("lblfund");
                Label reqasse = (Label)e.Row.FindControl("lblasse");
                Label reqin = (Label)e.Row.FindControl("lblinorder");
                Label reqsale = (Label)e.Row.FindControl("lblsale");

                if (drv["sap_doctype"].ToString() == "ZCPO" || drv["sap_doctype"].ToString() == "ZOCP")
                {
                    ddl.SelectedValue = "Y";
                    reqgl.Visible = false;
                    reqcc.Visible = false;
                    reqwbs.Visible = true;
                    reqnet.Visible = false;
                    reqfund.Visible = false;
                    reqasse.Visible = true;
                    reqin.Visible = false;
                    reqsale.Visible = false;

                    asset.Enabled = true;
                    wbs.Enabled = true;

                    gl.Enabled = false;
                    cc.Enabled = false;
                    net.Enabled = false;
                    fund.Enabled = false;
                    inorder.Enabled = false;
                    saleorder.Enabled = false;
                }
                else if (drv["sap_doctype"].ToString() == "ZCSV" || drv["sap_doctype"].ToString() == "ZOCS")
                {
                    ddl.SelectedValue = "N";

                    reqgl.Visible = true;
                    reqcc.Visible = true;
                    reqwbs.Visible = false;
                    reqnet.Visible = true;
                    reqfund.Visible = false;
                    reqasse.Visible = false;
                    reqin.Visible = false;
                    reqsale.Visible = false;

                    gl.Enabled = true;
                    cc.Enabled = true;
                    net.Enabled = true;

                    wbs.Enabled = false;
                    fund.Enabled = false;
                    asset.Enabled = false;
                    inorder.Enabled = false;
                    saleorder.Enabled = false;
                }

                if (hidAcc.Value != "")
                {
                    ddl.SelectedValue = hidAcc.Value;
                    if (ddl.SelectedValue == "K")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = true;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        gl.Enabled = false;
                        cc.Enabled = true;
                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        inorder.Enabled = false;
                        saleorder.Enabled = false;

                    }
                    else if (ddl.SelectedValue == "Z")
                    {
                        reqgl.Visible = true;
                        reqcc.Visible = true;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        cc.Enabled = true;
                        inorder.Enabled = true;
                        gl.Enabled = true;

                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "F")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = false;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = true;
                        reqsale.Visible = false;

                        cc.Enabled = true;
                        inorder.Enabled = true;

                        gl.Enabled = false;
                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "J")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = true;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        cc.Enabled = true;
                        inorder.Enabled = true;

                        gl.Enabled = false;
                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "W")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = true;
                        reqwbs.Visible = false;
                        reqnet.Visible = true;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        cc.Enabled = true;
                        net.Enabled = true;

                        gl.Enabled = false;
                        wbs.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        inorder.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "C")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = true;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = true;

                        cc.Enabled = true;
                        saleorder.Enabled = true;

                        gl.Enabled = false;
                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        inorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "A")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = false;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = true;
                        reqin.Visible = true;
                        reqsale.Visible = false;

                        asset.Enabled = true;
                        inorder.Enabled = true;

                        gl.Enabled = false;
                        cc.Enabled = false;
                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "Y")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = false;
                        reqwbs.Visible = true;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = true;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        asset.Enabled = true;
                        wbs.Enabled = true;

                        gl.Enabled = false;
                        cc.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        inorder.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "P")
                    {
                        reqgl.Visible = true;
                        reqcc.Visible = false;
                        reqwbs.Visible = true;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        gl.Enabled = true;
                        wbs.Enabled = true;

                        cc.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        inorder.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "Q")
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = false;
                        reqwbs.Visible = true;
                        reqnet.Visible = false;
                        reqfund.Visible = true;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        inorder.Enabled = true;
                        wbs.Enabled = true;
                        fund.Enabled = true;

                        gl.Enabled = false;
                        cc.Enabled = false;
                        net.Enabled = false;
                        asset.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else if (ddl.SelectedValue == "N")
                    {
                        reqgl.Visible = true;
                        reqcc.Visible = true;
                        reqwbs.Visible = false;
                        reqnet.Visible = true;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;

                        gl.Enabled = true;
                        cc.Enabled = true;
                        net.Enabled = true;

                        wbs.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        inorder.Enabled = false;
                        saleorder.Enabled = false;
                    }
                    else
                    {
                        reqgl.Visible = false;
                        reqcc.Visible = false;
                        reqwbs.Visible = false;
                        reqnet.Visible = false;
                        reqfund.Visible = false;
                        reqasse.Visible = false;
                        reqin.Visible = false;
                        reqsale.Visible = false;
                        gl.Enabled = false;
                        cc.Enabled = false;
                        wbs.Enabled = false;
                        net.Enabled = false;
                        fund.Enabled = false;
                        asset.Enabled = false;
                        inorder.Enabled = false;
                        saleorder.Enabled = false;
                    }
                }


            }
        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            DataTable dtsch = objTab.getAccXls(ddlContact.SelectedValue);
            gvXls.DataSource = dtsch;
            gvXls.DataBind();
            
        }

        protected void btnTestRun_Click(object sender, EventArgs e)
        {
            if (ddlContact.SelectedIndex > 0)
            {
                string xcontact_no = ddlContact.SelectedValue.ToString();
                hidBatchNo.Value = System.DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
                string MassageSap = "";
                string datet = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss", th);
                PS_Library.PO.Sap_PO.SendToSap(xcontact_no, "Test Run", hidBatchNo.Value);
                // DataTable dt = objTab.getSAPerror(datet);
                DataTable dt = new DataTable();
                string sql = "";
                try
                {
                    sql = " select * from ps_sap_log where api_name = 'PO' and batch_no ='" + hidBatchNo.Value + "'";
                    dt = zdb.ExecSql_DataTable(sql, zconnectionstr);
                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                }


                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        if (dr["error_msg_othr"].ToString() != "")
                        {
                            MassageSap += (MassageSap == "" ? "" : ",") + dr["error_msg_othr"].ToString();
                        }
                    }
                }
                if (MassageSap == "")
                {
                    sql = "update ps_t_bid_selling_cmpo set testrun_status = 'Pass' where contract_no = '" + ddlContact.SelectedValue.ToString() + "' ";
                    zdb.ExecNonQuery(sql, zconnectionstr);
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Pass.');", true);
                }
                else
                {
                    sql = "update ps_t_bid_selling_cmpo set testrun_status = 'Not Pass' where contract_no = '" + ddlContact.SelectedValue.ToString() + "' ";
                    zdb.ExecNonQuery(sql, zconnectionstr);
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('" + MassageSap + "');", true);
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert(' Please select contract no.');", true);
            }
        }
        private bool isExistData(string xbid_no , string xbid_revision)
        {
            bool xfound = false; 
            string sql = "select * from wf_ps_acc_setting where bid_no = '" + xbid_no + "' and bid_revision = '" + xbid_revision + "' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                xfound = true;
            }
            return xfound; 
        }
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            // Submit to ACC

            string sql = "  ";
            if (isExistData(hidBidNo.Value, hidBidRev.Value) == false)
            {
                //Insert 
                sql = @" insert into wf_ps_acc_setting (
                            bid_no,
                            bid_revision,
                            schedule_name,
                            contract_no,
                            submit_date,
                            submitto_acc_empid,
                            submitto_acc_name,
                            submitto_acc_s_position,
                            submitto_acc_f_position
                ) values (
                '" +hidBidNo.Value+ @"' ,
                " + hidBidRev.Value + @" ,
                '" + "" + @"' ,
                '" + ddlContact.SelectedValue.ToString() + @"' ,
                 '" + System.DateTime.Now.ToString("yyyy-MM-dd") + @"' ,
                '" + ucEmp1.getEmpID() + @"' ,
                '" + ucEmp1.getEmpName() + @"' ,
                 '" + ucEmp1.getShortPosition() + @"' ,
                 '" + ucEmp1.getFullPosition() + @"' 
                ) ";
            }
            else
            {
                //Update
                sql = @" update into wf_ps_acc_setting set
                          
                            contract_no ='" + ddlContact.SelectedValue.ToString() + @"',
                            submit_date = '" + System.DateTime.Now.ToString("yyyy-MM-dd") + @"',
                            submitto_acc_empid = '" + ucEmp1.getEmpID() + @"' ,
                            submitto_acc_name = '" + ucEmp1.getEmpName() + @"' ,
                            submitto_acc_s_position = '" + ucEmp1.getShortPosition() + @"' ,
                            submitto_acc_f_position =  '" + ucEmp1.getFullPosition() + @"' 
                        where bid_no =  '" + hidBidNo.Value + @"'  and bid_revision =   " + hidBidRev.Value + @" and contract_no = '" + ddlContact.SelectedValue.ToString() + @"' ";
                
            }
            zdb.ExecNonQuery(sql, zconnectionstr);
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert(' Submitted for account setting successfully.');", true);

        }

        protected void btnAssignTo_Click(object sender, EventArgs e)
        {
            //Assign to 

        }
    }
}