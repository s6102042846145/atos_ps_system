﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AccountCodeSetting.aspx.cs" Inherits="PS_System.form.Selling.AccountCodeSetting" %>

<%@ Register src="../PR/ucEmp.ascx" tagname="ucEmp" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
       <style type="text/css">
        
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
              .Colsmall {
            width: 50px;
            max-width: 50px;
            min-width: 50px;

            height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
             .Colsmall2 {
            width: 200px;
            max-width: 200px;
            min-width: 200px;

             height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
        .Colsmall3 {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
            
            height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
       .Colsmall4 {
            width: 60px;
            max-width: 60px;
            min-width: 60px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
       .Colsmall5 {
            width: 80px;
            max-width: 80px;
            min-width: 80px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
            .Colsmalleq {
            width: 70px;
            max-width: 70px;
            min-width: 70px;

              height: 60px;
            max-height: 60px;
            min-height: 60px;
        }
         
            
  /*          #tdHeader   td {
            width: 90px;
            max-width: 90px;
            min-width: 90px;
        }*/
         /*  #tbXlsHeader td {
            width: 80px;
            max-width: 80px;
            min-width: 80px;
        }*/

/*    #gvXls th{
            width: 75px;
            max-width: 75px;
            min-width: 75px;
        }*/

 /*       #gvXls td{
                   width: 80px;
            max-width: 80px;
            min-width: 80px;
        }*/

   
    </style>
    <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

       <script>
           $(document).keypress(
               function (event) {
                   if (event.which == '13') {
                       event.preventDefault();
                   }
               });

           $(document).ready(function () {
             
               maintainScrollPosition();
           });
           function NumOnlyChk(e) {
               var key;
               if (window.event) // IE
               {
                   key = e.keyCode;
               }
               else if (e.which) // Netscape/Firefox/Opera
               {
                   key = e.which;
               }
               var vchar = String.fromCharCode(key);
               if (((vchar >= "0" && vchar <= "9") && (key != 8) || ((vchar == "." || key == 49) && (key != 8)))) {

                   e.returnValue = true;
               }
               else {
                   return false;
               }
               e.returnValue = true;
               return true;
           }



         function pageLoad() {
               maintainScrollPosition();
           }

           function setScrollPosition(scrollValue) {
               $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }

           function maintainScrollPosition() {
               $("#dvScroll").scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
        }

       </script>
</head>
<body>
    <form id="form1" runat="server">
                 <div>
            <table style="width: 99%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contract Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                     
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Account Code Setting"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="width:100%;max-width:99%;overflow-x:auto">
                <table id="tbShowDetail" runat="server" style="width:100%;">
                       <tr>
                           <td colspan="10" style="padding-left:10px;">
                               <asp:Label ID="Label22" runat="server" Text="Contract No.:"></asp:Label>
                               &nbsp; <asp:DropDownList ID="ddlContact" runat="server" Width="250px"></asp:DropDownList>
                               &nbsp;&nbsp;    <asp:Button ID="btnSearch" runat="server" Text="Search"  class="btn btn-primary" OnClick="btnSearch_Click"  />
                           &nbsp;
                               <asp:Label ID="lblMode" runat="server" CssClass="TEXT_400"></asp:Label>
                           </td>
                       </tr>
                                      <tr>
                              <td colspan="10">
                                  <div id="dvShowXls" runat="server" style="padding-left:10px;">
                                   <div >
                                   <table id="tbXlsHeader" runat="server" style="width:99%">
                                       <tr style="color:white;background-color:#164094;width:34%;text-align:center;font-family:Tahoma;font-size:9pt">
                                            <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label15" runat="server" Text=" PO Doctype" Font-Bold="True"></asp:Label>
                                           </td>
                                             <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label19" runat="server" Text="ECM PO" Font-Bold="True"></asp:Label>
                                           </td>
                                             <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label20" runat="server" Text="ECM PO Item" Font-Bold="True"></asp:Label>
                                           </td>

                                           <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label16" runat="server" Text="Part" Font-Bold="True"></asp:Label>
                                           </td>
                                           <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label17" runat="server" Text="Item No." Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmalleq">
                                               <asp:Label ID="Label18" runat="server" Text="Equipment Code" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall2">
                                               <asp:Label ID="Label26" runat="server" Text="Description" Font-Bold="True"></asp:Label>
                                           </td>
                                               <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label9" runat="server" Text="Account Assignment Name" Font-Bold="True"></asp:Label>
                                           </td>

                                                <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label10" runat="server" Text="Qty" Font-Bold="True"></asp:Label>
                                           </td>
                                             <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label11" runat="server" Text="PO Unit Price" Font-Bold="True"></asp:Label>
                                           </td>
                                             <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label12" runat="server" Text="PO Unit" Font-Bold="True"></asp:Label>
                                           </td>
                                             <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label13" runat="server" Text="PO Total Price" Font-Bold="True"></asp:Label>
                                           </td>
                                             <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label14" runat="server" Text="PO Currency" Font-Bold="True"></asp:Label>
                                           </td>
                                            <%--   <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label19" runat="server" Text="SOE. Unit Price" Font-Bold="True" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label20" runat="server" Text="SOE. Amount" Font-Bold="True" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label22" runat="server" Text="LSE. Unit Price" Font-Bold="True" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label23" runat="server" Text="LSE. Amount" Font-Bold="True" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall4">
                                               <asp:Label ID="Label24" runat="server" Text="LTC. Unit Price" Font-Bold="True" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall">
                                               <asp:Label ID="Label25" runat="server" Text="LTC. Amount" Font-Bold="True" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                           </td>--%>
                                            <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label1" runat="server" Text="GL Account" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall5">
                                               <asp:Label ID="Label2" runat="server" Text="Cost Center" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall5">
                                               <asp:Label ID="Label6" runat="server" Text="Asset" Font-Bold="True"></asp:Label> 
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall5">
                                               <asp:Label ID="Label7" runat="server" Text="Internal Order" Font-Bold="True"></asp:Label>  
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall5">
                                               <asp:Label ID="Label8" runat="server" Text="Sales Order" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label3" runat="server" Text="WBS" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall3">
                                               <asp:Label ID="Label4" runat="server" Text="Network" Font-Bold="True"></asp:Label>
                                           </td>
                                            <td style="border:1px solid white;" class="Colsmall5">
                                               <asp:Label ID="Label5" runat="server" Text="Fund" Font-Bold="True"></asp:Label>
                                           </td>
                                       </tr>
                                   </table>
                                              </div>
                                        <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
                                 <div id="dvScroll" runat="server" style="width:100%;overflow-y:scroll;overflow-x:hidden;height:400px;margin-left:-1px" onscroll="setScrollPosition(this.scrollTop);">
                                    <asp:GridView ID="gvXls" runat="server" Width="95%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." ShowHeader="False" OnRowDataBound="gvXls_RowDataBound" >
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White"  />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                       <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSaptype" runat="server" Text='<%# Bind("sap_doctype") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall"/>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbNum" runat="server" Text='<%# Bind("po_num") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3" />
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPoitm" runat="server" Text='<%# Bind("po_item_no") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Part">
                                        <ItemTemplate>
                                            <asp:Label ID="lbPart" runat="server" Text='<%# Bind("part_no") %>'></asp:Label>
                                               <asp:Label ID="lbBidno" runat="server" Text='<%# Bind("bid_no") %>' Visible="false"></asp:Label>
                                               <asp:Label ID="lbRev" runat="server" Text='<%# Bind("bid_revision") %>' Visible="false"></asp:Label>
                                              <asp:Label ID="lbSch" runat="server" Text='<%# Bind("schedule_name") %>' Visible="false"></asp:Label>
                                              <asp:Label ID="lbRow" runat="server" Text='<%# Bind("row_id") %>' Visible="false"></asp:Label>
                                             <asp:Label ID="lbCont" runat="server" Text='<%# Bind("contract_no") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Item No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbItmno" runat="server" Text='<%# Bind("item_No") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall"/>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Equipment Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="lbEqcode" runat="server" Text='<%# Bind("equip_code") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmalleq"/>
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDesc" runat="server" Text='<%# Bind("desc_short") %>' ToolTip='<%# Bind("po_item_desc") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall2"/>
                                    </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Account Assignment Name">
                                        <ItemTemplate>
                                            <asp:HiddenField ID="hidddlAcc" runat="server" Value='<%# Bind("account_assignment_code") %>' visible="false"/>
                                            <asp:DropDownList ID="ddlAccAssi" runat="server" OnSelectedIndexChanged="ddlAccAssi_SelectedIndexChanged" AutoPostBack="true" Width="100%" >
                                                 <asp:ListItem Text="<-- Select -->" Value=""></asp:ListItem>
                                                 <asp:ListItem Text="ค่าใช้จ่ายหน่วยงาน (K)" Value="K"></asp:ListItem>
                                                 <asp:ListItem Text="Tools งบทำการ (Z)" Value="Z"></asp:ListItem>
                                                 <asp:ListItem Text="คำสั่งซ่อม,ผลิต (F)" Value="F"></asp:ListItem>
                                                 <asp:ListItem Text="Asset change to tool (J)" Value="J"></asp:ListItem>
                                                 <asp:ListItem Text="Tools งบลงทุน (W)" Value="W"></asp:ListItem>
                                                 <asp:ListItem Text="Sales order (C)" Value="C"></asp:ListItem>
                                                 <asp:ListItem Text="สินทรัพย์ (A)" Value="A"></asp:ListItem>
                                                 <asp:ListItem Text="สินทรัพย์งานโครงการ (Y)" Value="Y"></asp:ListItem>
                                                 <asp:ListItem Text="ครุภัณฑ์สั่งทำ (P)" Value="P"></asp:ListItem>
                                                 <asp:ListItem Text="สำรองคลังโครงการ (Q)" Value="Q"></asp:ListItem>
                                                 <asp:ListItem Text="ค่าใช้จ่ายโครงการ (N)" Value="N"></asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3"/>
                                    </asp:TemplateField>
                                   <%--  <asp:TemplateField HeaderText="" >
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh1" runat="server" Text="SOE. Unit Price" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supUnit" runat="server" Text='<%# Eval("cif_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                         <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh2" runat="server" Text="SOE. Amount" ToolTip="Supply of Equipment Foreign Supply CIF Thai Port Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_supAmou" runat="server" Text='<%# Eval("cif_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                              <HeaderTemplate>
                                                  <asp:Label ID="lblh3" runat="server" Text="LSE. Unit Price" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSUnit" runat="server" Text='<%# Eval("ewp_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                <HeaderTemplate>
                                                  <asp:Label ID="lblh4" runat="server" Text="LSE. Amount" ToolTip="Local Supply Ex-works Price (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localSAmou" runat="server" Text='<%# Eval("ewp_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                  <HeaderTemplate>
                                                  <asp:Label ID="lblh5" runat="server" Text="LTC. Unit Price" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Unit Price"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="lb_localTUnit" runat="server" Text='<%# Eval("lci_unit_price", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall4" HorizontalAlign="Center" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="">
                                                 <HeaderTemplate>
                                                  <asp:Label ID="lblh6" runat="server" Text="LTC. Amount" ToolTip="Local Transpotation, Contruction and Installation (excluding VAT) Baht Amount"></asp:Label>
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                 <asp:Label ID="lb_localTAmou" runat="server" Text='<%# Eval("lci_amount", "{0:0.00}") %>'></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle CssClass="Colsmall" HorizontalAlign="Center" />
                                        </asp:TemplateField>--%>
                                     <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbQty" runat="server" Text='<%# Bind("po_qty") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall"/>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUnitPri" runat="server" Text='<%# Bind("po_unit_price") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4"/>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUnit" runat="server" Text='<%# Bind("po_unit") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall"/>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbTotal" runat="server" Text='<%# Bind("po_total_price") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4"/>
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Label ID="lbCurr" runat="server" Text='<%# Bind("po_currency") %>' ></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" CssClass="Colsmall4"/>
                                    </asp:TemplateField>


                                       <asp:TemplateField HeaderText="GL Account" >
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtGL" runat="server" Width="87%" Text='<%# Bind("GL") %>' Enabled="false"></asp:TextBox>
                                                   <asp:Label ID="lblgl" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3"/>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Cost Center">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtCC" runat="server" Width="87%" Text='<%# Bind("CostCenter") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblcc" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall5"/>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtAsset" runat="server" Width="87%" Text='<%# Bind("Asset") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblasse" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall5"/>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtInorder" runat="server" Width="87%" Text='<%# Bind("InternalOrder") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblinorder" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall5"/>
                                        </asp:TemplateField>
                                           <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtSaleorder" runat="server" Width="87%" Text='<%# Bind("SalesOrder") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblsale" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall5"/>
                                        </asp:TemplateField>

                                       <asp:TemplateField HeaderText="WBS">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtWBS" runat="server" Width="87%" Text='<%# Bind("WBSNo") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblwbs" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3"/>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Network">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtNet" runat="server" Width="87%" Text='<%# Bind("Network") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblnet" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall3"/>
                                        </asp:TemplateField>
                                       <asp:TemplateField HeaderText="Fund">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtFund" runat="server" Width="87%" Text='<%# Bind("Fund") %>' Enabled="false"></asp:TextBox>
                                                 <asp:Label ID="lblfund" runat="server" Text="*" ForeColor="red" Visible="false"></asp:Label>
                                            </ItemTemplate>
                                                 <ItemStyle HorizontalAlign="Center" CssClass="Colsmall5"/>
                                        </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                 </div>
                                      </div>
                              </td>
                          </tr>
                    <tr>
                        <td style="padding:10px">
                          
                        </td>
                    </tr>
                          </table>
        </div>
                 <br />
        <div class="div_95">
              &nbsp;<asp:Label ID="lblTitle" runat="server" CssClass="TEXT_400"></asp:Label>
                            <asp:Panel ID="pPurchase" runat="server">
                                <uc1:ucEmp ID="ucEmp1" runat="server" />
                                <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Text="Submit" Width="120px" OnClick="btnSubmit_Click" />
                                <asp:Button ID="btnCancel" runat="server" class="btn btn-primary" Text="Cancel" Width="120px" />
                                <br />
                                </asp:Panel>
                            <asp:Panel ID="pAssignTo" runat="server">
                                <uc1:ucEmp ID="ucEmp2" runat="server" />
                                <asp:Button ID="btnAssignTo" runat="server" class="btn btn-primary" Text="AssignTo" Width="120px" OnClick="btnAssignTo_Click" />
                                <asp:Button ID="btnAssigntoCancel" runat="server" class="btn btn-primary" Text="Cancel" Width="120px" />
                                <br />
                                </asp:Panel>
                            <asp:Panel ID="pAccStaff" runat="server">
                                    <asp:Button ID="btnSave" runat="server" Text="Save"  class="btn btn-primary" OnClick="btnSave_Click" Width="120px" />
                                    <asp:Button ID="btnTestRun" runat="server" class="btn btn-primary" OnClick="btnTestRun_Click" Text="Test Run" Width="120px" />
                                </asp:Panel>
                                <asp:Panel ID="pAccApprove" runat="server">
                                    <asp:TextBox ID="txtAccMgrComment" runat="server" Height="120px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                                    <br />
                                    <asp:Button ID="btnApprove" runat="server" class="btn btn-primary"  Text="Approve" />
                                    <asp:Button ID="btnReject" runat="server" class="btn btn-primary" Text="Reject" Width="120px" />
                                    &nbsp;
                                    <asp:Button ID="btnTestRun0" runat="server" class="btn btn-primary" OnClick="btnTestRun_Click" Text="Test Run" Width="120px" />
                                    <br />
                                </asp:Panel>
        </div>
      <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRev" runat="server" />
        <asp:HiddenField ID="hidJobNo" runat="server" />
        <asp:HiddenField ID="hidJobRev" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
        <asp:HiddenField ID="hidMc" runat="server" />
        <asp:HiddenField ID="hidBatchNo" runat="server" />
                 <br />
        <asp:HiddenField ID="hidMode" runat="server" />
                 <br />
        <asp:HiddenField ID="hidContractNo" runat="server" />
    </form>
</body>
</html>
