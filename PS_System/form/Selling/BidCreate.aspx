﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidCreate.aspx.cs" Inherits="PS_System.form.Bid.BidCreate" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Contract Management</title>

  <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>




    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <script>
        $(function () {


         
            $('#mtsBidNo').multiselect({
                //includeSelectAllOption: true,
                //enableFiltering: true,
                //enableCaseInsensitiveFiltering: true,
                //numberDisplayed: 3,
                maxHeight: 300
            });


        });
        $(function () {
            $(".pickerPlan").datepicker(
                {
                     dateFormat: 'mm-yy' 
                });
            $(".pickerActual").datepicker(
                {
                    dateFormat: 'yy-mm-dd'
                });
        });

    </script>

    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .auto-style1 {
            height: 20px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            height: 24px;
        }
        .auto-style8 {
            height: 10px;
        }
        .auto-style9 {
            height: 27px;
        }
        .auto-style10 {
            height: 23px;
            width: 180px;
        }
        .auto-style11 {
            height: 20px;
            width: 180px;
        }
        .auto-style12 {
            width: 180px;
        }
        .auto-style13 {
            height: 24px;
            width: 180px;
        }
        .auto-style14 {
            height: 10px;
            width: 180px;
        }
        .auto-style15 {
            height: 27px;
            width: 180px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Create"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;" >
            <table class="nav-justified">
                <tr>
                    <td colspan="3" style="background-color: #007ACC; color: #FFFFFF;">
                        <asp:Label ID="Label60" runat="server" Text="Basic Info"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style10">
                        <asp:Label ID="Label1" runat="server" Text="Bid No.:"></asp:Label>
                    </td>
                    <td class="auto-style4">
                                   <asp:ListBox ID="mtsBidNo" runat="server" SelectionMode="Single" OnSelectedIndexChanged="mtsBidNo_Job_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                       
                    </td>
                    <td class="auto-style4">
                        <asp:Label ID="Label62" runat="server" Text="Revision:"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtBidRev" runat="server" Width="67px"></asp:TextBox>
                    &nbsp;</td>
                    <td class="auto-style4">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style11">
                        <asp:Label ID="Label11" runat="server" Text="Bid Status:"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:Label ID="lbStatusbid" runat="server" Text="Active"></asp:Label>
                             
                    </td>
                    <td rowspan="5" style="text-align: left; vertical-align: top">
                        <asp:Label ID="Label52" runat="server" Text="Bid Description:"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtBidDescription" runat="server" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Enabled="False" ReadOnly="True" Rows="3" TextMode="MultiLine" Width="544px" Height="148px"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td rowspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style11">
                        <asp:Label ID="Label12" runat="server" Text="Bid Type:"></asp:Label>
                    </td>
                    <td class="auto-style1">
                                 <asp:DropDownList ID="ddlBidtype" runat="server">
                                     <asp:ListItem Value="01">สายส่ง</asp:ListItem>
                                     <asp:ListItem Value="02">สถานีไฟฟ้า</asp:ListItem>
                                     <asp:ListItem Value="03">สื่อสาร</asp:ListItem>
                                     <asp:ListItem Value="04">โรงไฟฟ้าความร้อน</asp:ListItem>
                                     <asp:ListItem Value="05">โรงไฟฟ้าพลังน้ำ</asp:ListItem>
                                     <asp:ListItem Value="06">โรงไฟฟ้านิวเคลียร์</asp:ListItem>
                                     <asp:ListItem Value="07">โรงไฟฟ้าพลังงานทดแทน</asp:ListItem>
                                     <asp:ListItem Value="08">บำรุงรักษา</asp:ListItem>
                                     <asp:ListItem Value="09">อื่น ๆ</asp:ListItem>
                                 </asp:DropDownList>
                             
                    </td>
                </tr>
                <tr>
                    <td class="auto-style13">
                        <asp:Label ID="Label7" runat="server" Text="Bid Evaluation Approved By:"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtEvaApproBy" runat="server" Width="400px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style12">
                        <asp:Label ID="Label3" runat="server" Text="Bid Evaluation Approved Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtEvaApproDate" runat="server" ReadOnly="True">2021-03-11</asp:TextBox>
                        &nbsp;<asp:Label ID="Label71" runat="server" ForeColor="DimGray" Text="YYYY-MM-DD"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style13">
                        <asp:Label ID="Label13" runat="server" Text="Remark:"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtRemark" runat="server" ReadOnly="true" Height="97px" TextMode="MultiLine" Width="405px"></asp:TextBox>
                    </td>
                    <td class="auto-style5">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style12">
                        <asp:Label ID="Label5" runat="server" Text="Plan Bid Issue:"></asp:Label>
                    </td>
                    <td>
                         <asp:TextBox ID="popPlanBidIssue" CssClass="pickerPlan" runat="server" autocomplete="off"></asp:TextBox>
      
                        &nbsp;<asp:Label ID="Label72" runat="server" ForeColor="DimGray" Text="MMYYYY"></asp:Label>
                    </td>
                    <td>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;</td>
                </tr>
                   <tr>
                    <td class="auto-style15">
                        <asp:Label ID="Label2" runat="server" Text="Plan Open Technical:"></asp:Label>
                    </td>
                    <td class="auto-style9">
                        <asp:TextBox ID="popPlanOpenTec" CssClass="pickerPlan" runat="server" autocomplete="off"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label4" runat="server" ForeColor="DimGray" Text="MMYYYY"></asp:Label>
                    </td>
                    <td class="auto-style9">
                        <asp:Label ID="Label6" runat="server" Text="Plan Open Commercial:"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="popPlanOpenCom" CssClass="pickerPlan" runat="server" autocomplete="off"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label8" runat="server" ForeColor="DimGray" Text="MMYYYY"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style14">
                        <asp:Label ID="Label74" runat="server" Text="Actual Bid Issue:"></asp:Label>
                    </td>
                    <td class="auto-style8">
                        <asp:TextBox ID="popActualBidIss" CssClass="pickerActual" runat="server" autocomplete="off"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label75" runat="server" ForeColor="DimGray" Text="YYYY-MM-DD"></asp:Label>
                    </td>
                    <td class="auto-style8">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style15">
                        <asp:Label ID="txtActualOpenTec" runat="server" Text="Actual Open Technical: "></asp:Label>
                    </td>
                    <td class="auto-style9">
                        <asp:TextBox ID="popActualOpenTec" CssClass="pickerActual" runat="server" autocomplete="off"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label78" runat="server" ForeColor="DimGray" Text="YYYY-MM-DD"></asp:Label>
                    </td>
                    <td class="auto-style9">
                        <asp:Label ID="Label70" runat="server" Text="Actual Open Commercial:"></asp:Label>
                        &nbsp;&nbsp;<asp:TextBox ID="popActualOpenCom" CssClass="pickerActual" runat="server" autocomplete="off"></asp:TextBox>
                        &nbsp;<asp:Label ID="Label77" runat="server" ForeColor="DimGray" Text="YYYY-MM-DD"></asp:Label>
                    </td>
                    <td class="auto-style9">
                        </td>
                </tr>
               
            
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        <asp:Button ID="btnCreate" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="117px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Create" OnClick="btnCreat_Click" />
                        &nbsp;<asp:Button ID="btnEdit" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="117px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Edit" OnClick="btnEdit_Click" />
                        &nbsp;<asp:Button ID="btnDelete" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="117px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Delete" OnClick="btnDelete_Click" Visible="False" />
                        &nbsp;<asp:Button ID="btnCancel" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>



        <asp:HiddenField ID="hidBIdNo" runat="server" />



        <asp:HiddenField ID="hidRevision" runat="server" />



        <asp:HiddenField ID="hidBidname1" runat="server" />



        <asp:HiddenField ID="hidBidname2" runat="server" />



        <asp:HiddenField ID="hidBidname3" runat="server" />



        <asp:HiddenField ID="hidStatus" runat="server" />



        <asp:HiddenField ID="hidEvaApproBy" runat="server" />



        <br />



    </form>
</body>
</html>

