﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Bid
{
    public partial class BidCreate : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        public string commonDB = ConfigurationManager.AppSettings["db_common"].ToString();
        static oraDBUtil oraDBUtil = new oraDBUtil();
        private string empID = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                try
                {
                    if (Request.QueryString["BidNo"] != null)
                    {
                        hidBIdNo.Value = Request.QueryString["BidNo"].ToString();

                        ViewState["Bidno"] = hidBIdNo.Value;
                    }
                    else
                    {
                        hidBIdNo.Value = "TS12-L-02";
                    }

                }
                catch
                {

                }
                try
                {
                    if (Request.QueryString["ApprovBy"] != null)
                    {
                        hidEvaApproBy.Value = Request.QueryString["ApprovBy"].ToString();

                        ViewState["ApprovBy"] = hidBIdNo.Value;
                    }
                    else
                    {
                        hidEvaApproBy.Value = "";
                    }

                }
                catch
                {

                }
                try
                {
                    if (Request.QueryString["Revision"] != null)
                    {
                        hidRevision.Value = Request.QueryString["Revision"].ToString();

                        ViewState["Revision"] = hidRevision.Value;
                    }
                    else
                    {
                        hidRevision.Value = "0";
                    }

                }
                catch
                {

                }
                bindDataBidNo();
                if (hidBIdNo.Value != "")
                {
                    mtsBidNo.SelectedValue = hidBIdNo.Value;
                   // mtBidNo.Disabled = true;


                }
                bindData();


            }
        }


        protected void btnSearch_Click(object sender, EventArgs e)
        {

        }


        private void bindDataBidNo()
        {
            DataTable dt = new DataTable();
            dt = getBidNo();
            int i = 0;
            foreach (DataRow dr in dt.Rows)
            {
                mtsBidNo.Items.Insert(i, new ListItem(dr["bid_no"].ToString(), dr["bid_no"].ToString()));
                i++;
            }

            mtsBidNo.DataBind();
        }


        private void bindDataBidType()
        {
            DataTable dt = new DataTable();
            string bidno = "";
            //foreach (ListItem item in mtBidNo.Items)
            //{
            //    if (item.Selected)
            //    {

            //        bidno = item.Value;


            //    }
            //}
            bidno = mtsBidNo.SelectedValue;
            dt = getBidType(bidno);

            if (dt.Rows.Count > 0)
            {

                if (dt.Rows[0]["sub_line"].ToString() == "L")
                {
                    ddlBidtype.SelectedValue = "01";
                }
                if (dt.Rows[0]["sub_line"].ToString() == "S")
                {
                    ddlBidtype.SelectedValue = "02";
                }
            }


        }
        private void bindData()
        {
            DataTable dt = new DataTable();
            dt = getBidNo(mtsBidNo.SelectedValue);

            if (dt.Rows.Count > 0)
            {
                txtBidDescription.Text = dt.Rows[0]["bid_desc"].ToString();
                hidBidname1.Value = dt.Rows[0]["Bidname"].ToString();
                hidBidname2.Value = dt.Rows[0]["Bidname2"].ToString();
                hidBidname3.Value = dt.Rows[0]["Bidname3"].ToString();
                txtBidRev.Text = hidRevision.Value;
                lbStatusbid.Text = dt.Rows[0]["status"].ToString();
                if (lbStatusbid.Text == "Active")
                {
                    hidStatus.Value = "A";
                }
                if (lbStatusbid.Text == "Cancel")
                {
                    hidStatus.Value = "C";
                }

                bindDataBidType();
                txtRemark.Text = dt.Rows[0]["remark"].ToString();


            }
        }
        private DataTable getBidNo(string bidno)
        {
            string sql = "";
            sql = " select distinct bid.bid_no,[dbo].[udf_StripHTML](bid_desc) as bid_desc,SUBSTRING([dbo].[udf_StripHTML](bid_desc),0,255) as Bidname," +
                  "      SUBSTRING([dbo].[udf_StripHTML](bid_desc),256,255) as Bidname2,SUBSTRING([dbo].[udf_StripHTML](bid_desc),511,255) as Bidname3 ," +
                  "     status,bid_type,SUBSTRING([dbo].[udf_StripHTML](remark),0,255) as Remark,b as PBidissue,c1 as PBidopent, '' as PBidopenc" +
                  "       from bid_no bid " +
                  "  where bid_no = '" + bidno + "' and revision = '" + hidRevision.Value + "'";
            var dt = zdb.ExecSql_DataTable(sql, zconn);
            return dt;
        }
        private DataTable getBidNo()
        {
            string sql = "";
            sql = " select * from bid_no ";
            var dt = zdb.ExecSql_DataTable(sql, zconn);
            return dt;
        }

        private DataTable getBidType(string bidno)
        {
            string sql = "";
            sql = " select sub_line from job_no where job_id = (select top 1 job_id from bid_no b left join bid_job_relate bj on b.bid_id = bj.bid_id where b.bid_no ='" + bidno + "' )";
            var dt = zdb.ExecSql_DataTable(sql, zconn);
            return dt;
        }

        private DataTable get_p_m_sap_bid_master(string bidno)
        {
            string sql = "";
            sql = " select * from p_m_sap_bid_master where bid_number ='" + bidno + "' ";
            //var dt = zdb.ExecSql_DataTable(sql, zconn);
            var dt = oraDBUtil._getDT(sql, commonDB);
            return dt;
        }


        private void insertps_t_bid_selling_createbid()
        {
            DataTable dt = new DataTable();


            //foreach (var item in listXls_temp)
            //{
            string sql = "";
            try
            {
                sql = @"INSERT INTO ps_t_bid_selling_createbid
           ( [bid_no]
      ,[bid_name]
      ,[bid_name2]
      ,[bid_name3]
      ,[bid_status]
      ,[bid_type]
      ,[approve_by]
      ,[approve_date]
      ,[remark]
      ,[plan_bid_issue]
      ,[plan_open_technical]
      ,[plan_open_commercial]
      ,[actual_bid_issue_date]
      ,[actual_open_technical]
      ,[actual_open_commercial]
      ,[updated_by]
      ,[updated_datetime])
     VALUES
           ('" + mtsBidNo.SelectedValue  + @"'
           ,'" + hidBidname1.Value + @"'
           ,'" + hidBidname2.Value + @"'
           ,'" + hidBidname3.Value + @"'
           ,'" + hidStatus.Value + @"'
           ,'" + ddlBidtype.SelectedItem.Value + @"'
           ,'" + txtEvaApproBy.Text + @"'
           ,'" + txtEvaApproDate.Text + @"'
           ,'" + txtRemark.Text + @"'
           ,'" + setForMatDate( popPlanBidIssue.Text) + @"'
           ,'" + setForMatDate(popPlanOpenTec.Text) + @"'
           ,'" + setForMatDate(popPlanOpenCom.Text) + @"'
           ,'" + popActualBidIss.Text + @"'
           ,'" + popActualOpenTec.Text + @"'
           ,'" + popActualOpenCom.Text + @"'
           ,'" + empID + @"'
           ,GETDATE());";

                zdb.ExecNonQuery(sql, zconn);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
            //}

        }
        private string setForMatDate(string dateValue)
            {
             string strDate = "";

            if (dateValue.Length>0)
            {
                strDate = dateValue.Substring(3, 4) + "-" + dateValue.Substring(0, 2) + "-01";
            }

                return strDate;
            }



        private void findBidData(string mode)
        {
            //string sql = "";
            //sql = " select * from bid_no where bid_no = '" + txtBidNo.Text + "' and bid_revision = '" + txtBidRev.Text + "' ";
            //var dt = zdb.ExecSql_DataTable(sql, zconn);
            //if (dt.Rows.Count > 0)
            //{
            //    var dr = dt.Rows[0];
            //    txtBidNo.Text = dr["bid_no"].ToString();
            //    txtBidRev.Text = dr["bid_revision"].ToString();
            //    txtScopeOfWork.Text = dr["scope_of_work"].ToString();
            //    txtProject.Text = dr["project"].ToString();
            //    txtJobNo.Text = dr["job_no"].ToString();
            //    txtContractNo.Text = dr["contract_no"].ToString();
            //    txtTransmissionLineNo.Text = dr["transmission_line_no"].ToString();
            //    txtLineDescription.Text = dr["line_description"].ToString();
            //    txtLineLength.Text = dr["line_length"].ToString();
            //    txtWithinMonths.Text = dr["within_months"].ToString();
            //    txtContractor.Text = dr["contractor"].ToString();
            //    txtPurchasingMethod.Text = dr["purchasing_method"].ToString();
            //    txtPurchasingOrg.Text = dr["purchasing_org"].ToString();
            //    txtPurchasingGroup.Text = dr["purchasing_group"].ToString();
            //    txtLIDate.Text = ConvertDateFormat(dr["LI_date"].ToString());
            //    txtConfirmLIDate.Text = ConvertDateFormat(dr["comfirm_LI_date"].ToString());
            //    txtSignDate.Text = ConvertDateFormat(dr["contract_sign_date"].ToString());
            //    txtRequisitioner.Text = dr["requisitioner"].ToString();
            //    txtPlant.Text = dr["plant"].ToString();
            //    txtTotalTHB.Text = ConvertStringToCurrency(dr["total_thb"].ToString());
            //    txtTotalUSD.Text = ConvertStringToCurrency(dr["total_usd"].ToString());
            //    txtCurrency1.Text = dr["currency1"].ToString();
            //    txtExDate1.Text = ConvertDateFormat(dr["currency1_date"].ToString());
            //    txtExRate1.Text = dr["currency1_exchangerate"].ToString();
            //    txtCurrency2.Text = dr["currency2"].ToString();
            //    txtExDate2.Text = ConvertDateFormat(dr["currency2_date"].ToString());
            //    txtExRate2.Text = dr["currency2_exchangerate"].ToString();
            //    txtCurrency3.Text = dr["currency3"].ToString();
            //    txtExDate3.Text = ConvertDateFormat(dr["currency3_date"].ToString());
            //    txtExRate3.Text = dr["currency3_exchangerate"].ToString();
            //    txtCurrency4.Text = dr["currency4"].ToString();
            //    txtExDate4.Text = ConvertDateFormat(dr["currency4_date"].ToString());
            //    txtExRate4.Text = dr["currency4_exchangerate"].ToString();
            //    txtInsurance.Text = dr["Insurance_percent"].ToString();
            //    txtCustomer_Clearance.Text = dr["customer_clearance"].ToString();
            //    txtVAT.Text = dr["vat"].ToString();
            //    txtLocal_Preference.Text = dr["local_preference"].ToString();

            //    txtStatus.Text = dr["status"].ToString();
            //    if (dt.Rows[0]["status"].ToString().ToUpper() == "SUBMITTED") { btnSave.Enabled = false; }

            //}
            //else { txtStatus.Text = "N/A"; }

        }

        protected void btnCreat_Click(object sender, EventArgs e)
        {

            //insert data
            insertps_t_bid_selling_createbid();


            if (get_p_m_sap_bid_master(mtsBidNo.SelectedValue).Rows.Count == 0)
            {

                //send sap
                string result = PS_Library.BID.Sap_Bid.Bid_SendToSap(mtsBidNo.SelectedValue, "01");
                //string a = PS_Library.Contract_Master.Sap_Contract.SendToSap("W90000-000190-TS12-S-02");
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('"+result + "');", true);
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "alert", "alert('"+ mtsBidNo.SelectedValue + "is already create" + "');", true);
            }

            //string a = PS_Library.PO.Sap_PO.createDataToSap("W90003-000190-TS12-S-02", 0);
            // string a = PS_Library.PO.Sap_PO.SendToSap("W90003-000190-TS12-S-02","");
            //string a = PS_Library.Material_Master.Sap_Material.SendToSap("SV-CVL-00023");
            //cExcel cExcel = new cExcel();
            //List<string> lisrFileName = new List<string>();
            //string a = cExcel.genExcelSumTotal("TS12-S-02", lisrFileName);


        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void mtsBidNo_Job_SelectedIndexChanged(object sender, EventArgs e)
        {

            bindData();
        }

        protected void btnEdit_Click(object sender, EventArgs e)
        {
            //insert data
            insertps_t_bid_selling_createbid();


            if (get_p_m_sap_bid_master(mtsBidNo.SelectedValue).Rows.Count == 0)
            {
                //send sap
                string result = PS_Library.BID.Sap_Bid.Bid_SendToSap(mtsBidNo.SelectedValue, "02");
                //string a = PS_Library.Contract_Master.Sap_Contract.SendToSap("W90000-000190-TS12-S-02");
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            string result = PS_Library.BID.Sap_Bid.Bid_SendToSap(mtsBidNo.SelectedValue, "03");
        }
    }
}