﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class PriceScheduleUploadAdd : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        clsPriceUpload2 clsPriceUpload = new clsPriceUpload2();
        private void bindDescription(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format("select bid_no, bid_revision, bid_desc from bid_no where bid_no = '{0}' and bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.h_bidnumberTitle.InnerText = bidno;
                    this.lb_bidnumber.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindVendorData(string bitnumber, string bitrevision)
        {
            try
            {
                string sql = string.Format("select *, (a.bidder_id + ';' + a.vendor_code) as idRef from ps_t_bid_selling  as a left join ps_m_vendor as b on a.vendor_code = b.vendor_code where bid_no = '{0}' and bid_revision = {1}", bitnumber, bitrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_vendor.DataSource = dt;
                    this.ddl_vendor.DataTextField = "vendor_name";
                    this.ddl_vendor.DataValueField = "idRef";
                    this.ddl_vendor.DataBind();
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private void BindVendorData()
        {
            try
            {
                string sql = string.Format("select * from ps_m_vendor");
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.ddl_joinVendor.DataSource = dt;
                    this.ddl_joinVendor.DataTextField = "vendor_name";
                    this.ddl_joinVendor.DataValueField = "vendor_code";
                    this.ddl_joinVendor.DataBind();

                    this.ddl_joinVendor.Items.Insert(0, new ListItem("-- Select Vendor --"));
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        DataTable RefSchedule(string bitnumber, string bitrevision)
        {
            try
            {
                string sql = string.Format("select * from ps_m_bid_schedule where bid_no = '{0}' and bid_revision = {1}", bitnumber, bitrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);
                return dt;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    bindDescription(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                    BindVendorData(Request.QueryString["bidno"], Request.QueryString["bidrevision"]);
                    BindVendorData();

                    using (DropDownList downList = this.ddl_docType)
                    {
                        downList.DataSource = clsPriceUpload.GetDocType();
                        downList.DataTextField = "doctype_name";
                        downList.DataValueField = "doctype_code";
                        downList.DataBind();

                        downList.Items.Insert(0, new ListItem("-- Select Document Type --"));
                    }

                    using (DropDownList downList = this.ddl_RefSchedule)
                    {
                        downList.DataSource = RefSchedule(this.lb_bidnumber.Text, this.txt_bidVersion.Text);
                        downList.DataTextField = "schedule_name";
                        downList.DataValueField = "job_no";
                        downList.DataBind();

                        downList.Items.Insert(0, new ListItem("-- Select Schedule --"));
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect",
                       string.Format("alert('data not found.'); window.location='PriceScheduleUploadData.aspx';"), true);
                }
            }
        }

        protected void btn_action_Click(object sender, EventArgs e)
        {
            try
            {
                ViewState["VendorUpload"] = GetDataTable();
                DataTable dt = (DataTable)ViewState["VendorUpload"];

                string[] idRef = this.ddl_vendor.SelectedValue.Split(';');
                dt.Rows.Add(idRef[0], this.lb_bidnumber.Text, this.txt_bidVersion, "All Schedule", idRef[1], this.ddl_vendor.SelectedItem.Text, this.ddl_bidderType.SelectedValue, true);

                ViewState["VendorUpload"] = dt;

                this.gv_fileUpload.DataSource = dt;
                this.gv_fileUpload.DataBind();

                switch (this.ddl_bidderType.SelectedValue)
                {
                    case "Single Entity":
                    case "Joint Venture":
                        this.lb_bidderType.Text = this.ddl_bidderType.SelectedValue;

                        using (DropDownList downList = (this.gv_fileUpload.Rows[0].FindControl("ddl_RefSchedule") as DropDownList))
                        {
                            downList.Items.Insert(0, new ListItem("All Select Schedule"));
                            downList.Enabled = false;
                        }

                        break;

                    case "Consortium":
                        this.lb_bidderType.Text = "Consortium";
                        break;

                    default:

                        break;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        DataTable GetDataTable()
        {
            DataTable dataTable = new DataTable("VendorUpload");

            dataTable.Columns.Add("bidder_id", typeof(string));
            dataTable.Columns.Add("bid_no", typeof(string));
            dataTable.Columns.Add("bid_revision", typeof(string));
            dataTable.Columns.Add("ref_schedule_no", typeof(string));
            dataTable.Columns.Add("vendor_code", typeof(string));
            dataTable.Columns.Add("vendor_name", typeof(string));
            dataTable.Columns.Add("vendor_type", typeof(string));
            dataTable.Columns.Add("vendor_lead", typeof(Boolean));

            return dataTable;
        }
        DataTable GetOtherDocDataTable()
        {
            DataTable dataTable = new DataTable("OtherDoc");

            dataTable.Columns.Add("bid_no", typeof(string));
            dataTable.Columns.Add("bid_revision", typeof(int));
            dataTable.Columns.Add("ref_schedule_no", typeof(string));
            dataTable.Columns.Add("doc_type", typeof(string));
            dataTable.Columns.Add("doc_name", typeof(string));
            dataTable.Columns.Add("doc_version", typeof(int));
            dataTable.Columns.Add("doc_node_id", typeof(string));
            dataTable.Columns.Add("status_bid", typeof(string));
            dataTable.Columns.Add("parent_node_id", typeof(string));

            return dataTable;
        }
        protected void btn_add_Click(object sender, EventArgs e)
        {
            try
            {
                if (this.ddl_joinVendor.SelectedIndex == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('vendor not found. please try again.');", true);
                }
                else
                {
                    DataTable dt = (DataTable)ViewState["VendorUpload"];

                    string[] idRef = this.ddl_vendor.SelectedValue.Split(';');
                    dt.Rows.Add(idRef[0], this.lb_bidnumber.Text, this.txt_bidVersion, "", this.ddl_joinVendor.SelectedValue, this.ddl_joinVendor.SelectedItem.Text, this.ddl_bidderType.SelectedValue, false);

                    ViewState["VendorUpload"] = dt;

                    this.gv_fileUpload.DataSource = dt;
                    this.gv_fileUpload.DataBind();

                    if (this.ddl_bidderType.SelectedValue == "2")
                    {
                        for (int i = 0; i < dt.Rows.Count; i++)
                        {
                            this.gv_fileUpload.Rows[i].Cells[1].Enabled = false;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void gv_fileUpload_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    using (DropDownList downList = (e.Row.FindControl("ddl_RefSchedule") as DropDownList))
                    {
                        downList.DataSource = RefSchedule(this.lb_bidnumber.Text, this.txt_bidVersion.Text);
                        downList.DataTextField = "schedule_name";
                        downList.DataValueField = "job_no";
                        downList.DataBind();

                        downList.Items.Insert(0, new ListItem("-- Select Schedule --"));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            //try
            //{
            //    switch (this.ddl_bidderType.SelectedValue)
            //    {
            //        case "Single Entity":
            //        case "Joint Venture":

            //            using (FileUpload fileUpload = (FileUpload)this.gv_fileUpload.Rows[0].FindControl("fup_documents"))
            //            {
            //                if (fileUpload.HasFile)
            //                {
            //                    if (insData((DataTable)ViewState["VendorUpload"]))
            //                    {
            //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('has been successfully saved.');", true);
            //                    }
            //                    else
            //                    {
            //                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to save data.');", true);
            //                    }
            //                }
            //                else
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('file upload not found. please try again.');", true);
            //                }
            //            }

            //            break;

            //        case "Consortium":

            //            bool hasFile = false;
            //            bool selSchedule = false;
            //            DataTable dataTable = (DataTable)ViewState["VendorUpload"];

            //            for (int i = 0; i < dataTable.Rows.Count; i++)
            //            {
            //                using (FileUpload fileUpload = (FileUpload)this.gv_fileUpload.Rows[i].FindControl("fup_documents"))
            //                {
            //                    if (fileUpload.HasFile)
            //                    {
            //                        hasFile = true;
            //                    }
            //                    else
            //                    {
            //                        hasFile = false;
            //                        break;
            //                    }
            //                }
            //                using (DropDownList downList = (DropDownList)this.gv_fileUpload.Rows[i].FindControl("ddl_RefSchedule"))
            //                {
            //                    if (downList.SelectedIndex > 0)
            //                    {
            //                        selSchedule = true;
            //                    }
            //                    else
            //                    {
            //                        selSchedule = false;
            //                        break;
            //                    }
            //                }
            //            }

            //            if (hasFile || selSchedule)
            //            {
            //                if (insData(dataTable))
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('has been successfully saved.');", true);
            //                }
            //                else
            //                {
            //                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('failed to save data.');", true);
            //                }
            //            }
            //            else
            //            {
            //                ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('schedule or file upload not found. please try again.');", true);
            //            }

            //            break;

            //        default:

            //            break;
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
        }
        protected void btn_docUpload_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        bool insData(DataTable dt)
        {
            try
            {
                int i = 0;
                SqlTransaction transaction;
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    transaction = conn.BeginTransaction();

                    try
                    {
                        foreach (DataRow row in dt.Rows)
                        {
                            using (SqlCommand cmd = new SqlCommand("sp_ins_bid_selling_vendor", conn, transaction))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@bidder_id", row["bidder_id"].ToString());
                                cmd.Parameters.AddWithValue("@bid_no", this.lb_bidnumber.Text);
                                cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));

                                using (DropDownList downList = (DropDownList)this.gv_fileUpload.Rows[i].FindControl("ddl_RefSchedule"))
                                {
                                    cmd.Parameters.AddWithValue("@ref_schedule_no", downList.SelectedItem.Text);
                                }

                                cmd.Parameters.AddWithValue("@vendor_code", row["vendor_code"].ToString());
                                cmd.Parameters.AddWithValue("@vendor_name", row["vendor_name"].ToString());

                                using (RadioButton radio = (RadioButton)this.gv_fileUpload.Rows[i].FindControl("roType"))
                                {
                                    if (radio.Checked)
                                    {
                                        cmd.Parameters.AddWithValue("@vendor_type", row["vendor_type"].ToString());
                                    }
                                    else
                                    {
                                        cmd.Parameters.AddWithValue("@vendor_type", "Vendor");
                                    }
                                }

                                cmd.ExecuteNonQuery();
                            }

                            using (SqlCommand cmd = new SqlCommand("sp_ins_bid_selling_fileupload", conn, transaction))
                            {
                                cmd.CommandType = CommandType.StoredProcedure;
                                cmd.Parameters.AddWithValue("@bidder_id", row["bidder_id"].ToString());
                                cmd.Parameters.AddWithValue("@bid_no", this.lb_bidnumber.Text);
                                cmd.Parameters.AddWithValue("@bid_revision", int.Parse(this.txt_bidVersion.Text));

                                using (DropDownList downList = (DropDownList)this.gv_fileUpload.Rows[i].FindControl("ddl_RefSchedule"))
                                {
                                    cmd.Parameters.AddWithValue("@ref_schedule_no", downList.SelectedItem.Text);
                                }

                                cmd.Parameters.AddWithValue("@vendor_code", row["vendor_code"].ToString());
                                cmd.Parameters.AddWithValue("@doc_type", "PSD");

                                using (FileUpload fileUpload = (FileUpload)this.gv_fileUpload.Rows[i].FindControl("fup_documents"))
                                {
                                    cmd.Parameters.AddWithValue("@doc_name", fileUpload.FileName);
                                }

                                cmd.Parameters.AddWithValue("@doc_version", 0);
                                cmd.Parameters.AddWithValue("@parent_node_id", "0000000");
                                cmd.Parameters.AddWithValue("@doc_node_id", "0000000");
                                cmd.Parameters.AddWithValue("@status_bid", "selling");

                                cmd.ExecuteNonQuery();
                            }

                            i++;
                        }

                        transaction.Commit();
                    }
                    catch (SqlException sqlEx)
                    {
                        transaction.Rollback();
                        conn.Close();

                        return false;
                        throw sqlEx;
                    }

                    conn.Close();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
                throw ex;
            }
        }

        protected void btn_reload_Click(object sender, EventArgs e)
        {
            BindVendorData();
        }

        protected void btn_saveOtherDoc_Click(object sender, EventArgs e)
        {

        }

        protected void gv_fileUpload_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            try
            {
                if (e.CommandName == "docUpload")
                {
                    int r = int.Parse(e.CommandArgument.ToString());
                    using(FileUpload fileUpload = (FileUpload)this.gv_fileUpload.Rows[r].FindControl("fup_documents"))
                    {
                        if (fileUpload.HasFile)
                        {
                            foreach (HttpPostedFile uploadedFile in fileUpload.PostedFiles)
                            {

                            }
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('file upload not found. please try again.');", true);
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}