﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="Index.aspx.cs" Inherits="PS_System.form.Selling.Index" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager ID="scriptMag" runat="server"></asp:ScriptManager>
    <asp:UpdatePanel ID="updatePal" runat="server">
        <ContentTemplate>
            <div class="container">
                <nav class="nav">
                    <a class="nav-link active" href="BidSelling2.aspx">Bid Selling</a>
                    <a class="nav-link" href="VendorData2.aspx">Bid Vender</a>
                    <a class="nav-link" href="MemberData.aspx">Bid Committee</a>
                    <a class="nav-link" href="PriceScheduleUploadData.aspx">Bid Price Schedule Upload</a>
                     <a class="nav-link" target="_blank" href="BidTabulation.aspx">Bid Tabulation</a>
                    <a class="nav-link" target="_blank" href="BidTabulationAdd.aspx">Bid Winer</a>
                    <a class="nav-link" href="BidTabulationData.aspx">Bid Final</a>
                </nav>
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Bid selling module test menu.</h1>
                        
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
