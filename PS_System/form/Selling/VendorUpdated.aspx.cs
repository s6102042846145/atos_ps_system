﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class BidSellingUpdated : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();

        private void BindVendorData(string vendor_code)
        {
            try
            {
                string sql = string.Format(@"select * from ps_m_vendor where vendor_code='{0}'", vendor_code);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.txt_venderCode.Text = dt.Rows[0]["vendor_code"].ToString();
                    this.txt_companyName.Text = dt.Rows[0]["vendor_name"].ToString();
                    this.txt_Tax.Text = dt.Rows[0]["tax_id"].ToString();
                    this.txt_EGPNo.Text = dt.Rows[0]["egp_no"].ToString();
                    this.txt_EGPDate.Text = DateTime.Parse(dt.Rows[0]["egp_register_date"].ToString()).ToString("yyyy-MM-dd");
                    this.txt_address.Text = dt.Rows[0]["address"].ToString();
                    this.txt_country.Text = dt.Rows[0]["country"].ToString();
                    this.txt_Tel.Text = dt.Rows[0]["tel_no"].ToString();
                    this.txt_Fax.Text = dt.Rows[0]["fax_no"].ToString();
                    this.txt_contactName.Text = dt.Rows[0]["contact_person"].ToString();
                    this.txt_contactEmail.Text = dt.Rows[0]["contact_email"].ToString();
                    this.txt_contactTel.Text = dt.Rows[0]["contact_tel"].ToString();
                    this.txt_contactFax.Text = dt.Rows[0]["contact_fax"].ToString();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling.aspx';", true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["vendorCode"] != null)
                {
                    BindVendorData(Request.QueryString["vendorCode"]);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling.aspx';", true);
                }
            }
        }

        protected void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(zconn))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand("sp_up_vendor", conn))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@vendor_code", this.txt_venderCode.Text);
                        cmd.Parameters.AddWithValue("@vendor_name", this.txt_companyName.Text);
                        cmd.Parameters.AddWithValue("@tax_id", this.txt_Tax.Text);
                        cmd.Parameters.AddWithValue("@egp_no", this.txt_EGPNo.Text);
                        cmd.Parameters.AddWithValue("@egp_register_date", DateTime.Parse(this.txt_EGPDate.Text));
                        cmd.Parameters.AddWithValue("@address", this.txt_address.Text);
                        cmd.Parameters.AddWithValue("@country", this.txt_country.Text);
                        cmd.Parameters.AddWithValue("@tel_no", this.txt_Tel.Text);
                        cmd.Parameters.AddWithValue("@fax_no", this.txt_Fax.Text);
                        cmd.Parameters.AddWithValue("@contact_person", this.txt_contactName.Text);
                        cmd.Parameters.AddWithValue("@contact_email", this.txt_contactEmail.Text);
                        cmd.Parameters.AddWithValue("@contact_tel", this.txt_contactTel.Text);
                        cmd.Parameters.AddWithValue("@contact_fax", this.txt_contactFax.Text);

                        cmd.ExecuteNonQuery();
                        conn.Close();

                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('has been successfully updated.'); window.location='VendorData.aspx';", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}