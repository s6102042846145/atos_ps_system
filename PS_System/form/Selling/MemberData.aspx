﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="MemberData.aspx.cs" Inherits="PS_System.form.Selling.MemberData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container mb-4 mt-4">
                <div class="row mb-4">
                    <h6>EGAT - Committee Data Management</h6>
                    <div class="ml-auto">
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header">
                        ตารางข้อมูลข้อมูลคณะกรรมการและผู้ช่วย
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <asp:TextBox ID="txt_bidnosearch" runat="server" CssClass="form-control" MaxLength="50" placeholder="bid number search"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_bidnosearch" runat="server" Text='<i class="bi bi-search"></i>' ToolTip="Click to search." OnClick="btn_bidnosearch_Click" CausesValidation="false" CssClass="btn btn-info" />
                                </div>
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary" title="Click to refresh" onclick="window.location.reload();"><i class="bi bi-arrow-repeat"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_datanotfound">
                            Data not found.
                        </div>
                        <asp:GridView ID="gv_bidmember" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_bidmember_PageIndexChanging"
                            OnRowCommand="gv_bidmember_RowCommand" Font-Names="tahoma" Font-Size="9pt">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderStyle-Width="10">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="" HeaderText="Bid Number"/>--%>
                                <asp:TemplateField HeaderText="Bid Number">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lb_bidnumber" Text='<%# Bind("bid_no") %>' ToolTip='<%# Eval("bid_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:BoundField DataField="bid_revision" HeaderText="Bid Revision" />
                                <asp:BoundField DataField="total_receive_open" HeaderText="รับและเปิดซอง" />
                                <asp:BoundField DataField="total_considered" HeaderText="พิจารณา" />
                                <asp:BoundField DataField="total_check" HeaderText="ตรวจรับ" />
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" Text='<i class="bi bi-eye"></i>' ToolTip="Click to view." CssClass="btn btn-info btn-sm" CommandName="IsView" CommandArgument='<%#Eval("bid_no") + ";" + Eval("bid_revision") +";"+Container.DataItemIndex%>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderStyle-Width="200">
                                    <ItemTemplate>
                                        <div class="input-group mb-3">
                                            <asp:DropDownList ID="ddl_committeetype" runat="server">
                                                <asp:ListItem Text="*เลือกคณะกรรมการ*" Value="0" Selected="True"></asp:ListItem>
                                                <asp:ListItem Text="กรรมการรับและเปิดซอง" Value="receive_open"></asp:ListItem>
                                                <asp:ListItem Text="กรรมการพิจารณา" Value="considered"></asp:ListItem>
                                                <asp:ListItem Text="กรรมการตรวจรับ" Value="check"></asp:ListItem>
                                            </asp:DropDownList>
                                            <div class="input-group-prepend">
                                                <asp:LinkButton runat="server" Text='<i class="bi bi-plus-circle"></i>' ToolTip="Click to add." CssClass="btn btn-success btn-sm" CommandName="IsAdd" CommandArgument='<%#Eval("bid_no") + ";" + Eval("bid_revision") +";"+Container.DataItemIndex%>' />
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle CssClass="small" />
                        </asp:GridView>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="Index.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
