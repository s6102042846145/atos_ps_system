﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="BidTabulationAdd.aspx.cs" Inherits="PS_System.form.Selling.BidTabulationAdd" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <div class="container mb-4">
        <div class="row mb-3">
            <h5>EGAT - Bid Winer</h5>
            <div class="ml-auto">
            </div>
        </div>
        <div class="card mb-4">
            <div class="card-header">
                ข้อมูลเอกสารประกวดราคา
            </div>
            <div class="card-body">
                <div class="form-row mb-4">
                    <div class="form-group col-4">
                        <label>Bid Number</label>
                        <asp:DropDownList runat="server" ID="ddl_bidNumber" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddl_bidNumber_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                    <div class="form-group col-8">
                        <label>Schedule Name</label>
                        <div class="input-group">
                            <asp:DropDownList runat="server" ID="ddl_scheduleName" CssClass="form-control"></asp:DropDownList>
                            <div class="input-group-append">
                                <asp:LinkButton runat="server" ID="btn_searchBid" CssClass="btn btn-primary" Text='<i class="bi bi-search mr-2"></i>Search' OnClick="btn_searchBid_Click" CausesValidation="false"></asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group mb-4">
                    <small class="text-muted">Bid Tabulation Data</small>
                    <div class="alert alert-warning" role="alert" runat="server" id="alert_bidTab" visible="false">
                        Data not found.
                    </div>
                    <div class="table-responsive-sm">
                        <asp:GridView ID="gv_bidSelling" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" Font-Names="tahoma" Font-Size="9pt" OnRowCommand="gv_bidSelling_RowCommand">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderStyle-Width="10">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lb_vendorCode" Text='<%#Eval("vendor_code") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Vendor Name">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lb_vendorName" Text='<%#Eval("vendor_name") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Avg. Technical Score">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lb_techScore" Text='<%#Eval("technical_score") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Avg. Price Score">
                                    <ItemTemplate>
                                        <asp:Label runat="server" ID="lb_priceScore" Text='<%#Eval("price_score") %>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50" Visible="false">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="btn_selected" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-check2-square"></i>' ToolTip="click to select" CommandName="IsSelect" CommandArgument='<%# string.Format("{0};{1}",Container.DataItemIndex ,Eval("bidder_id")) %>' CausesValidation="false"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle CssClass="small" />
                        </asp:GridView>
                    </div>
                </div>

                <asp:Panel runat="server" ID="panel_form" Enabled="true">

                    <div class="form-row">
                        <div class="form-group col-4" hidden>
                            <label>Bid Number</label>
                            <asp:Label runat="server" ID="lb_bidNumber" CssClass="form-control"></asp:Label>
                        </div>
                        <div class="form-group col-8" hidden>
                            <label>Schedule Name</label>
                            <asp:Label runat="server" ID="lb_scheduleName" CssClass="form-control"></asp:Label>
                        </div>
                        <div class="form-group col-6">
                            <label>Select Winner</label>
                            <%--<asp:Label runat="server" ID="lb_bidWinner" CssClass="form-control"></asp:Label>--%>
                            <asp:DropDownList runat="server" ID="ddl_bidWinner" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group col-3">
                            <label>Avg. Technical Score</label>
                            <asp:TextBox runat="server" ID="txt_techScore" CssClass="form-control" Text="0"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_techScore" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RangeValidator1" runat="server" ControlToValidate="txt_techScore" Display="Dynamic" CssClass="text-danger" ErrorMessage="Invalid data" ValidationExpression="^\d+(?:\.\d{0,2})?$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group col-3">
                            <label>Avg. Price Score</label>
                            <asp:TextBox runat="server" ID="txt_priceScore" CssClass="form-control" Text="0"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_priceScore" SetFocusOnError="true"></asp:RequiredFieldValidator>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txt_priceScore" Display="Dynamic" CssClass="text-danger" ErrorMessage="Invalid data" ValidationExpression="^\d+(?:\.\d{0,2})?$" SetFocusOnError="True"></asp:RegularExpressionValidator>
                        </div>
                        <div class="form-group col-12">
                            <label>Comment</label>
                            <asp:TextBox runat="server" ID="txt_bidComment" TextMode="MultiLine" Rows="5" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                        </div>
                        <asp:HiddenField runat="server" ID="lb_bidRevision"></asp:HiddenField>
                        <asp:HiddenField runat="server" ID="lb_bidderId"></asp:HiddenField>
                        <asp:HiddenField runat="server" ID="lb_vendorCode"></asp:HiddenField>
                    </div>

                    <div class="text-right">
                        <asp:LinkButton runat="server" ID="btn_refresh" CssClass="btn btn-danger btn-sm" Text='<i class="bi bi-x-square mr-2"></i>Cancel' OnClick="btn_refresh_Click" CausesValidation="false"></asp:LinkButton>
                        <asp:LinkButton runat="server" ID="btn_save" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-save mr-2"></i>Save' OnClick="btn_save_Click" OnClientClick="if (!confirm('Do you want to add this the winner?')) return false;"></asp:LinkButton>
                    </div>

                </asp:Panel>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <a href="BidTabulationData.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
