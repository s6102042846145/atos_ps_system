﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class VendorAdd : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }
        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            try
            {
                //string sql = string.Format("select * from ps_m_vendor where vendor_code='{0}'", this.txt_venderCode.Text);
                //var dt = zdb.ExecSql_DataTable(sql, zconn);

                //if (dt.Rows.Count > 0)
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('duplicate vendor. please try again.');", true);
                //    this.txt_venderCode.Focus();
                //}
                //else
                //{
                    using (SqlConnection conn = new SqlConnection(zconn))
                    {
                        conn.Open();

                        using (SqlCommand cmd = new SqlCommand("sp_ins_vendor", conn))
                        {
                            cmd.CommandType = CommandType.StoredProcedure;
                            cmd.Parameters.AddWithValue("@vendor_code", string.Format("V-{0}",DateTime.Now.ToString("yyyy-MM-dd-HH-ss")));
                            cmd.Parameters.AddWithValue("@vendor_name", this.txt_companyName.Text);
                            cmd.Parameters.AddWithValue("@tax_id", this.txt_Tax.Text);
                            cmd.Parameters.AddWithValue("@egp_no", this.txt_EGPNo.Text);

                            if(this.txt_EGPDate.Text == "")
                            {
                                cmd.Parameters.AddWithValue("@egp_register_date", DBNull.Value);
                            }
                            else
                            {
                                cmd.Parameters.AddWithValue("@egp_register_date", DateTime.Parse(this.txt_EGPDate.Text));
                            }                          
                            cmd.Parameters.AddWithValue("@address", this.txt_address.Text);
                            cmd.Parameters.AddWithValue("@country", this.txt_country.Text);
                            cmd.Parameters.AddWithValue("@tel_no", this.txt_Tel.Text);
                            cmd.Parameters.AddWithValue("@fax_no", this.txt_Fax.Text);
                            cmd.Parameters.AddWithValue("@contact_person", this.txt_contactName.Text);
                            cmd.Parameters.AddWithValue("@contact_email", this.txt_contactEmail.Text);
                            cmd.Parameters.AddWithValue("@contact_tel", this.txt_contactTel.Text);
                            cmd.Parameters.AddWithValue("@contact_fax", this.txt_contactFax.Text);
                            cmd.Parameters.AddWithValue("@sap_vendor_code", this.txt_venderCode.Text);

                            cmd.ExecuteNonQuery();
                        }

                        conn.Close();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('has been successfully saved.'); window.location='VendorData2.aspx';", true);
                    }
                //}
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        //protected void btn_vendorsearch_Click(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        if (this.txt_venderCode.Text == "") return;

        //        string sql = string.Format("select * from ps_m_vendor where vendor_code='{0}'", this.txt_venderCode.Text);
        //        var dt = zdb.ExecSql_DataTable(sql, zconn);

        //        if (dt.Rows.Count > 0)
        //        {
        //            this.txt_sapVendor.Enabled = false;
        //            this.txt_companyName.Enabled = false;
        //            this.txt_Tax.Enabled = false;
        //            this.txt_EGPNo.Enabled = false;
        //            this.txt_EGPDate.Enabled = false;
        //            this.txt_address.Enabled = false;
        //            this.txt_country.Enabled = false;
        //            this.txt_Tel.Enabled = false;
        //            this.txt_Fax.Enabled = false;
        //            this.txt_contactName.Enabled = false;
        //            this.txt_contactEmail.Enabled = false;
        //            this.txt_contactTel.Enabled = false;
        //            this.txt_contactFax.Enabled = false;

        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('duplicate vendor code. please try again.');", true);
        //            this.txt_venderCode.Focus();
        //        }
        //        else
        //        {
        //            this.txt_sapVendor.Enabled = true;
        //            this.txt_companyName.Enabled = true;
        //            this.txt_Tax.Enabled = true;
        //            this.txt_EGPNo.Enabled = true;
        //            this.txt_EGPDate.Enabled = true;
        //            this.txt_address.Enabled = true;
        //            this.txt_country.Enabled = true;
        //            this.txt_Tel.Enabled = true;
        //            this.txt_Fax.Enabled = true;
        //            this.txt_contactName.Enabled = true;
        //            this.txt_contactEmail.Enabled = true;
        //            this.txt_contactTel.Enabled = true;
        //            this.txt_contactFax.Enabled = true;

        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Message", "alert('this vendor code can be used.');", true);
        //            this.txt_companyName.Focus();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }
        //}

        protected void gv_vendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {

        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            if (this.txt_vendorsearch.Text == "")
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('Please enter your text to search.');", true);
                this.txt_vendorsearch.Focus();
            }
            else
            {
                BindVendorData(this.txt_vendorsearch.Text);
            }
        }
        private void BindVendorData(string stringsearch)
        {
            try
            {
                string sql = string.Format("select * from ps_m_vendor where vendor_code like '%{0}%' or vendor_name like '%{0}%' or egp_no like '%{0}%' or sap_vendor_code like '%{0}%'", stringsearch);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                this.gv_vendor.DataSource = dt;
                this.gv_vendor.DataBind();

                if (dt.Rows.Count == 0)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "message", "alert('data not found.');", true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }
}