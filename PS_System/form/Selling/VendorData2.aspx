﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="VendorData2.aspx.cs" Inherits="PS_System.form.Selling.VendorData2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container mb-4 mt-4">
                <div class="row mb-4">
                    <h6>EGAT - Vendor Data Management</h6>
                </div>

                <div class="card">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        ตารางข้อมูลบริษัทผู้ซื้อ 
                        <a href="VendorAdd.aspx" target="_parent" role="button" class="btn btn-success btn-sm"><i class="bi bi-person-plus mr-2"></i> ข้อมูลผู้ซื้อเอกสารประกวดราคา</a>
                    </div>
                    <div class="card-body">
                        <div class="form-group">
                            <div class="input-group mb-3">
                                <asp:TextBox ID="txt_vendorsearch" runat="server" CssClass="form-control" MaxLength="50" placeholder="vendor search (code, name, egp no.)"></asp:TextBox>
                                <div class="input-group-prepend">
                                    <asp:LinkButton ID="btn_vendorsearch" runat="server" Text='<i class="bi bi-search"></i>' ToolTip="Click to search." OnClick="btn_vendorsearch_Click" CausesValidation="false" CssClass="btn btn-primary" />
                                </div>
                                <div class="input-group-prepend">
                                    <button type="button" class="btn btn-secondary" title="Click to refresh" onclick="window.location.reload();"><i class="bi bi-arrow-repeat"></i></button>
                                </div>
                            </div>
                        </div>
                        <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_datanotfound">
                            Data not found.
                        </div>
                        <asp:GridView ID="gv_vendor" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_vendor_PageIndexChanging"
                            OnRowCommand="gv_vendor_RowCommand" DataKeyNames="vendor_code" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found.">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            <RowStyle ForeColor="#000066" />
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            <Columns>
                                <asp:TemplateField HeaderStyle-Width="10">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <%--<asp:BoundField DataField="vendor_code" HeaderText="Code" />--%>
                                <asp:BoundField DataField="vendor_name" HeaderText="Name" />
                                <%--<asp:BoundField DataField="egp_no" HeaderText=" EGP No." />--%>
                                <%--<asp:BoundField DataField="egp_register_date" HeaderText="Date Register" DataFormatString="{0:dd/MM/yyyy}" />--%>
                                <asp:BoundField DataField="address" HeaderText="Address" />
                                <asp:BoundField DataField="country" HeaderText="Country" />
                                <%--<asp:BoundField DataField="contact_person" HeaderText="Name" />--%>
                                <%--<asp:BoundField DataField="contact_tel" HeaderText="Telephone" />--%>
                                <asp:TemplateField HeaderStyle-Width="50">
                                    <ItemTemplate>
                                        <asp:LinkButton CssClass="btn btn-info btn-sm" Text='<i class="bi bi-pencil-square"></i>' runat="server" CommandName="isEdit" CommandArgument='<%#Eval("vendor_code")%>' ToolTip="Click to edit." />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                            <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle CssClass="small" />
                        </asp:GridView>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="Index.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>
