﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidSelling.aspx.cs" Inherits="PS_System.form.Salling.BidSalling" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="bootstrap-4.6.0-dist/css/bootstrap.min.css" rel="stylesheet" />
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container mb-4 mt-4">
            <div class="text-right">
                <a href="BidSellingAdd.aspx" class="btn btn-primary">Add Bid Selling</a>
                <a href="VendorData.aspx" class="btn btn-primary">Update Vendor</a>
            </div>
            <div class="col-12">
                <h5>ข้อมูลผู้ซื้อเอกสารประกวดราคา</h5>
            </div>
            <div class="col-12">
                <asp:GridView ID="gv_bidselling" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_bidselling_PageIndexChanging"
                    OnRowCommand="gv_bidselling_RowCommand" DataKeyNames="bid_no">
                    <Columns>
                        <asp:BoundField DataField="bid_no" HeaderText="Bid Number" />
                        <asp:BoundField DataField="bid_revision" HeaderText="Bid Revision" />
                        <asp:BoundField DataField="vendor" HeaderText="Total Bidder" />
                        <asp:TemplateField HeaderStyle-Width="50">
                            <ItemTemplate>
                                <asp:Button CssClass="btn btn-info" Text="View" runat="server" CommandName="view" CommandArgument='<%#Eval("bid_no")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <HeaderStyle CssClass="small" />
                </asp:GridView>
            </div>
        </div>
    </form>
    <script src="bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
</body>
</html>
