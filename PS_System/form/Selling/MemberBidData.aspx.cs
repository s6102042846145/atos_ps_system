﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class MemberBidData : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void GetBidMemberDescription(string bidno, string bidrevision)
        {
            try
            {
                if (GetBidSelling(bidno, bidrevision))
                {
                    //string sql = string.Format(@"select a.bid_no,a.bid_revision,c.* from ps_t_bid_selling as a inner join  ps_m_vendor as c on a.vendor_code = c.vendor_code where a.bid_no = '{0}' and a.bid_revision = {1}", bidno, bidrevision);
                    //var dt = zdb.ExecSql_DataTable(sql, zconn);

                    //if (dt.Rows.Count > 0)
                    //{
                    //    this.gv_bidsellingdescription.DataSource = dt;
                    //    this.gv_bidsellingdescription.DataBind();

                    //    this.alert_datanotfound.Visible = false;
                    //}
                    //else
                    //{
                    //    this.alert_datanotfound.Visible = true;
                    //}
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling2.aspx';", true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        private bool GetBidSelling(string bidno, string bidrevision)
        {
            try
            {
                string sql = string.Format(@"select a.bid_no,a.bid_revision,b.bid_desc from ps_t_bid_monitoring as a inner join bid_no as b on a.bid_no = b.bid_no and a.bid_revision = b.bid_revision where a.bid_no = '{0}' and a.bid_revision = {1}", bidno, bidrevision);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    //string desc;
                    this.txt_bidNo.Text = bidno;
                    this.txt_bidVersion.Text = dt.Rows[0]["bid_revision"].ToString();
                    this.txt_bidDescription.Text = dt.Rows[0]["bid_desc"].ToString();

                    //desc = dt.Rows[0]["bid_desc"].ToString().Replace("<div>", "");
                    //desc = desc.Replace("</div>", "");
                    //desc = desc.Replace("<p>", "");
                    //desc = desc.Replace("</p>", "");
                    //desc = desc.Replace("<br>", "");
                    //desc = desc.Replace("</br>", "");
                    //desc = desc.Replace("&nbsp;", "");

                    //this.txt_bidDescription.Text = desc;

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public DataTable GetDataCommittee(string bidno, string bidrevision, string type)
        {
            try
            {
                string sql
                    = string.Format(@"select * from ps_t_bid_selling_committee where bid_no='{0}' and bid_revision={1} and committee_type='{2}'", bidno, bidrevision, type);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                return dt;
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["bidno"] != null)
                {
                    string binno = Request.QueryString["bidno"];
                    string bidrevision = Request.QueryString["bidrevision"];

                    ViewState["binno"] = Request.QueryString["bidno"];
                    ViewState["bidrevision"] = Request.QueryString["bidrevision"];

                    GetBidMemberDescription(binno, bidrevision);

                    using (DataTable table = GetDataCommittee(binno, bidrevision, "receive_open"))
                    {
                        if (table.Rows.Count > 0)
                        {
                            this.gv_data1.DataSource = table;
                            this.gv_data1.DataBind();

                            this.alert_notfound1.Visible = false;
                            ViewState["DATA1"] = table;
                        }
                        else
                        {
                            this.alert_notfound1.Visible = true;
                        }
                    }
                    using (DataTable table = GetDataCommittee(binno, bidrevision, "considered"))
                    {
                        if (table.Rows.Count > 0)
                        {
                            this.gv_data2.DataSource = table;
                            this.gv_data2.DataBind();

                            this.alert_notfound2.Visible = false;
                            ViewState["DATA2"] = table;
                        }
                        else
                        {
                            this.alert_notfound2.Visible = true;
                        }
                    }
                    using (DataTable table = GetDataCommittee(binno, bidrevision, "check"))
                    {
                        if (table.Rows.Count > 0)
                        {
                            this.gv_data3.DataSource = table;
                            this.gv_data3.DataBind();

                            this.alert_notfound3.Visible = false;
                            ViewState["DATA3"] = table;
                        }
                        else
                        {
                            this.alert_notfound3.Visible = true;
                        }
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling2.aspx';", true);
                }
            }
        }

        protected void gv_data1_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_data1.PageIndex = e.NewPageIndex;
            this.gv_data1.DataSource = (DataTable)ViewState["DATA1"];
            this.gv_data1.DataBind();
        }

        protected void gv_data1_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gv_data1.Rows.Count > 0)
            {
                for (int i = 0; i <= this.gv_data1.Rows.Count - 1; i++)
                {
                    Label label = (Label)this.gv_data1.Rows[i].FindControl("lb_emp_status");

                    if (label.Text == "committee")
                    {
                        label.Text = "คณะกรรมการ";
                        this.gv_data1.Rows[i].Cells[5].BackColor = Color.LightGreen;
                    }
                    else if (label.Text == "member")
                    {
                        label.Text = "ผู้ช่วย";
                        this.gv_data1.Rows[i].Cells[5].BackColor = Color.LightBlue;
                    }
                }
            }
        }

        protected void gv_data2_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_data2.PageIndex = e.NewPageIndex;
            this.gv_data2.DataSource = (DataTable)ViewState["DATA2"];
            this.gv_data2.DataBind();
        }

        protected void gv_data2_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gv_data2.Rows.Count > 0)
            {
                for (int i = 0; i <= this.gv_data2.Rows.Count - 1; i++)
                {
                    Label label = (Label)this.gv_data2.Rows[i].FindControl("lb_emp_status");

                    if (label.Text == "committee")
                    {
                        label.Text = "คณะกรรมการ";
                        this.gv_data2.Rows[i].Cells[5].BackColor = Color.LightGreen;
                    }
                    else if (label.Text == "member")
                    {
                        label.Text = "ผู้ช่วย";
                        this.gv_data2.Rows[i].Cells[5].BackColor = Color.LightBlue;
                    }
                }
            }
        }

        protected void gv_data3_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_data3.PageIndex = e.NewPageIndex;
            this.gv_data3.DataSource = (DataTable)ViewState["DATA3"];
            this.gv_data3.DataBind();
        }

        protected void gv_data3_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (this.gv_data3.Rows.Count > 0)
            {
                for (int i = 0; i <= this.gv_data3.Rows.Count - 1; i++)
                {
                    Label label = (Label)this.gv_data3.Rows[i].FindControl("lb_emp_status");

                    if (label.Text == "committee")
                    {
                        label.Text = "คณะกรรมการ";
                        this.gv_data3.Rows[i].Cells[5].BackColor = Color.LightGreen;
                    }
                    else if (label.Text == "member")
                    {
                        label.Text = "ผู้ช่วย";
                        this.gv_data3.Rows[i].Cells[5].BackColor = Color.LightBlue;
                    }
                }
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("MemberBidAdd.aspx?bidno={0}&bidrevision={1}&type={2}", ViewState["binno"].ToString(), ViewState["bidrevision"].ToString(), "receive_open"));
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("MemberBidAdd.aspx?bidno={0}&bidrevision={1}&type={2}", ViewState["binno"].ToString(), ViewState["bidrevision"].ToString(), "considered"));
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("MemberBidAdd.aspx?bidno={0}&bidrevision={1}&type={2}", ViewState["binno"].ToString(), ViewState["bidrevision"].ToString(), "check"));
        }
    }
}