﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="MemberBidData.aspx.cs" Inherits="PS_System.form.Selling.MemberBidData" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <asp:ScriptManager runat="server"></asp:ScriptManager>
    <asp:UpdatePanel runat="server">
        <ContentTemplate>
            <div class="container mb-4 mt-4">
                <div class="row mb-4">
                    <h6>EGAT - Committee Data</h6>
                    <div class="ml-auto"></div>
                </div>
                <div class="card mb-4">
                    <div class="card-header">
                        ข้อมูลเอกสารประกวดราคา
                    </div>
                    <div class="card-body">
                        <div class="form-row mb-4">
                            <div class="form-group col-8">
                                <label>Bid No.</label>
                                <asp:Label ID="txt_bidNo" runat="server" CssClass="form-control"></asp:Label>
                            </div>
                            <div class="form-group col-4">
                                <label>Bid Version.</label>
                                <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                            </div>
                            <div class="form-group col-12">
                                <label>Bid Description.</label>
                                <asp:Label ID="txt_bidDescription" runat="server" Height="150" CssClass="form-control"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        ข้อมูลคณะกรรมการรับและเปิดซอง
                        <asp:LinkButton ID="Button1" runat="server" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-plus-circle mr-2"></i>Add Committee' OnClick="Button1_Click" />
                    </div>
                    <div class="card-body">
                        <div class="form-group mb-4">
                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_notfound1">
                                Empty data.
                            </div>
                            <asp:GridView ID="gv_data1" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_data1_PageIndexChanging"
                                OnRowDataBound="gv_data1_RowDataBound" Font-Names="tahoma" Font-Size="9pt">
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="emp_id" HeaderText="Employee ID" />
                                    <asp:BoundField DataField="emp_name" HeaderText="Employee name" />
                                    <asp:BoundField DataField="emp_short_position" HeaderText="Short Position" />
                                    <asp:BoundField DataField="emp_full_position" HeaderText="Full Position" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lb_emp_status" runat="server" Text='<%# Eval("emp_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        ข้อมูลคณะกรรมการพิจารณา
                        <asp:LinkButton ID="Button2" runat="server" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-plus-circle mr-2"></i>Add Committee' OnClick="Button2_Click" />

                    </div>
                    <div class="card-body">
                        <div class="form-group mb-4">
                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_notfound2">
                                Empty data.
                            </div>
                            <asp:GridView ID="gv_data2" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_data2_PageIndexChanging"
                                OnRowDataBound="gv_data2_RowDataBound" Font-Names="tahoma" Font-Size="9pt">
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="emp_id" HeaderText="Employee ID" />
                                    <asp:BoundField DataField="emp_name" HeaderText="Employee name" />
                                    <asp:BoundField DataField="emp_short_position" HeaderText="Short Position" />
                                    <asp:BoundField DataField="emp_full_position" HeaderText="Full Position" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lb_emp_status" runat="server" Text='<%# Eval("emp_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>

                <div class="card mb-4">
                    <div class="card-header d-flex justify-content-between align-items-center">
                        ข้อมูลคณะกรรมการตรวจรับ
                        <asp:LinkButton ID="Button3" runat="server" CssClass="btn btn-success btn-sm" Text='<i class="bi bi-plus-circle mr-2"></i>Add Committee' OnClick="Button3_Click" />
                    </div>
                    <div class="card-body">
                        <div class="form-group mb-4">
                            <div class="alert alert-danger text-center" role="alert" runat="server" id="alert_notfound3">
                                Empty data.
                            </div>
                            <asp:GridView ID="gv_data3" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_data3_PageIndexChanging"
                                OnRowDataBound="gv_data3_RowDataBound" Font-Names="tahoma" Font-Size="9pt">
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderStyle-Width="10">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="emp_id" HeaderText="Employee ID" />
                                    <asp:BoundField DataField="emp_name" HeaderText="Employee name" />
                                    <asp:BoundField DataField="emp_short_position" HeaderText="Short Position" />
                                    <asp:BoundField DataField="emp_full_position" HeaderText="Full Position" />
                                    <asp:TemplateField HeaderText="Status" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:Label ID="lb_emp_status" runat="server" Text='<%# Eval("emp_status") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                                <HeaderStyle CssClass="small" />
                            </asp:GridView>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="text-right">
                            <a href="MemberData.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                        </div>
                    </div>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
