﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class VendorData : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void BindVendorData()
        {
            try
            {
                string sql = string.Format("select * from ps_m_vendor");
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.gv_vendor.DataSource = dt;
                    this.gv_vendor.DataBind();
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "redirect", "alert('data not found.'); window.location='BidSelling.aspx';", true);
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindVendorData();
            }
        }

        protected void gv_vendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_vendor.PageIndex = e.NewPageIndex;
            this.BindVendorData();
        }

        protected void gv_vendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "isEdit")
            {
                Response.Redirect(string.Format("VendorUpdated?vendorCode={0}", e.CommandArgument));
            }
        }
    }
}