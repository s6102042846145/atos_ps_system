﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="BidSellingAdd2.aspx.cs" Inherits="PS_System.form.Selling.BidSellingAdd2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container mb-4 mt-4">
        <div class="row mb-4">
            <h6>EGAT - Bid Selling Management</h6>
            <div class="ml-auto"></div>
        </div>

        <div class="card mb-4">
            <div class="card-header d-flex justify-content-between align-items-center">
                รายละเอียดข้อมูลเอกสารประกวดราคา
                <a class="btn btn-light" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" title="Click to expand"><i class="bi bi-justify"></i></a>
            </div>
            <div class="card-body">
                <div class="collapse" id="collapseExample">
                    <div class="form-row">
                        <div class="form-group col-sm-8">
                            <label>Bid No.</label>
                            <asp:DropDownList ID="ddl_bidNo" runat="server" CssClass="form-control"></asp:DropDownList>
                        </div>
                        <div class="form-group col-sm-4">
                            <label>Bid Version.</label>
                            <asp:Label ID="txt_bidVersion" runat="server" CssClass="form-control"></asp:Label>
                        </div>
                        <div class="form-group col-sm-12">
                            <label>Bid Description.</label>
                            <asp:Label ID="txt_bidDescription" runat="server" Height="200" CssClass="form-control"></asp:Label>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="card mb-4">
            <div class="card-header">
                แบบฟอร์มบันทึกข้อมูลผู้ซื้อเอกสารประกวดราคา
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group  col-sm-4">
                        <label>Vender code.</label>
                        <div class="input-group mb-3">
                            <asp:TextBox ID="txt_venderCode" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                            <div class="input-group-prepend">
                                <asp:LinkButton ID="btn_vendorsearch" runat="server" Text='<i class="bi bi-search"></i>' OnClick="btn_vendorsearch_Click" CausesValidation="false" CssClass="btn btn-info" ToolTip="Click to search."></asp:LinkButton>
                            </div>

                        </div>
                        <asp:RequiredFieldValidator ID="req_venderCode" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_venderCode" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group  col-sm-8">
                        <label>Vender name.</label>
                        <asp:TextBox ID="txt_companyName" runat="server" CssClass="form-control" MaxLength="250" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_companyName" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-sm-12">
                        <label>Vender address.</label>
                        <asp:TextBox ID="txt_address" runat="server" ReadOnly="False" TextMode="MultiLine" Rows="3" CssClass="form-control" MaxLength="1000" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_address" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group col-sm-4">
                        <label>Vender telephone.</label>
                        <asp:TextBox ID="txt_Tel" runat="server" CssClass="form-control" MaxLength="150" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_Tel" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-sm-4">
                        <label>Vender fax.</label>
                        <asp:TextBox ID="txt_Fax" runat="server" CssClass="form-control" MaxLength="150" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_Fax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-sm-4">
                        <label>Vender tax.</label>
                        <asp:TextBox ID="txt_Tax" runat="server" CssClass="form-control" MaxLength="50" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_Tax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group  col-sm-6">
                        <label>EGP no.</label>
                        <asp:TextBox ID="txt_EGPNo" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_EGPNo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group  col-sm-6">
                        <label>EGP register.</label>
                        <asp:TextBox ID="txt_EGPDate" runat="server" CssClass="form-control" TextMode="Date" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_EGPDate" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group col-sm-6">
                        <label>Contact name.</label>
                        <asp:TextBox ID="txt_contactName" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactName" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-sm-6">
                        <label>Contact email.</label>
                        <asp:TextBox ID="txt_contactEmail" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactEmail" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>

                    <div class="form-group col-sm-4">
                        <label>Contact Telephone.</label>
                        <asp:TextBox ID="txt_contactTel" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactTel" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-sm-4">
                        <label>Contact Fax.</label>
                        <asp:TextBox ID="txt_contactFax" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactFax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-sm-4">
                        <label>Country.</label>
                        <asp:TextBox ID="txt_country" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_country" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <a href="BidSelling2.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                    <asp:LinkButton ID="btn_save" runat="server" Text='<i class="bi bi-save mr-2"></i> Save' CssClass="btn btn-success btn-sm" OnClick="btn_save_Click" OnClientClick="if (!confirm('Do you want to add this vendor?')) return false;"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
