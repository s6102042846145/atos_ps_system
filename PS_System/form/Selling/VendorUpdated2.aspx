﻿<%@ Page Title="" Language="C#" MasterPageFile="~/form/Selling/Selling.Master" AutoEventWireup="true" CodeBehind="VendorUpdated2.aspx.cs" Inherits="PS_System.form.Selling.VendorUpdate2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="container mb-4 mt-4">
        <div class="row mb-4">
            <h6>EGAT - Vendor Data Management</h6>
            <div class="ml-auto"></div>
        </div>

        <div class="card">
            <div class="card-header">
                แบบฟอร์มแก้ไขข้อมูลบริษัทผู้ซื้อ
            </div>
            <div class="card-body">
                <div class="form-row">
                    <div class="form-group  col-3" hidden>
                        <label>Vender code.</label>
                        <asp:TextBox ID="txt_venderCode" runat="server" CssClass="form-control" MaxLength="50" ReadOnly="true"></asp:TextBox>
                    </div>
                    <div class="form-group  col-sm-3" hidden>
                        <label>SAP Vender code.</label>
                        <asp:TextBox ID="txt_sapVendor" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="form-group  col-8">
                        <label>Vender name.</label>
                        <asp:TextBox ID="txt_companyName" runat="server" CssClass="form-control" MaxLength="250"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_companyName" SetFocusOnError="true"></asp:RequiredFieldValidator>
                    </div>
                    <div class="form-group col-4">
                        <label>Country.</label>
                        <asp:TextBox ID="txt_country" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group col-12">
                        <label>Vender address.</label>
                        <asp:TextBox ID="txt_address" runat="server" ReadOnly="False" TextMode="MultiLine" Rows="3" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_address" SetFocusOnError="true"></asp:RequiredFieldValidator>--%>
                    </div>

                    <div class="form-group col-4" hidden>
                        <label>Vender telephone.</label>
                        <asp:TextBox ID="txt_Tel" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                    </div>
                    <div class="form-group col-4" hidden>
                        <label>Vender fax.</label>
                        <asp:TextBox ID="txt_Fax" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                    </div>
                    <div class="form-group col-4" hidden>
                        <label>Vender tax.</label>
                        <asp:TextBox ID="txt_Tax" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    </div>

                    <div class="form-group  col-6" hidden>
                        <label>EGP no.</label>
                        <asp:TextBox ID="txt_EGPNo" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group  col-6" hidden>
                        <label>EGP register.</label>
                        <asp:TextBox ID="txt_EGPDate" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                    </div>

                    <div class="form-group col-6" hidden>
                        <label>Contact name.</label>
                        <asp:TextBox ID="txt_contactName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group col-6" hidden>
                        <label>Contact email.</label>
                        <asp:TextBox ID="txt_contactEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>

                    <div class="form-group col-4" hidden>
                        <label>Contact Telephone.</label>
                        <asp:TextBox ID="txt_contactTel" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group col-4" hidden>
                        <label>Contact Fax.</label>
                        <asp:TextBox ID="txt_contactFax" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <div class="text-right">
                    <a href="VendorData2.aspx" class="btn btn-secondary btn-sm"><i class="bi bi-arrow-return-left mr-2"></i>Close</a>
                    <asp:LinkButton ID="btn_save" runat="server" Text='<i class="bi bi-save mr-2"></i> Save' CssClass="btn btn-success btn-sm" OnClick="btn_save_Click" OnClientClick="if (!confirm('Do you want to update this vendor?')) return false;"></asp:LinkButton>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
