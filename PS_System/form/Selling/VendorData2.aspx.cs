﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Selling
{
    public partial class VendorData2 : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        private void BindVendorData(string stringsearch)
        {
            try
            {
                string sql = string.Format("select * from ps_m_vendor where vendor_code like '%{0}%' or vendor_name like '%{0}%' or egp_no like '%{0}%' order by vendor_name", stringsearch);
                var dt = zdb.ExecSql_DataTable(sql, zconn);

                if (dt.Rows.Count > 0)
                {
                    this.alert_datanotfound.Visible = false;
                    this.gv_vendor.DataSource = dt;
                    this.gv_vendor.DataBind();
                }
                else
                {
                    this.alert_datanotfound.Visible = true;
                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BindVendorData(string.Empty);
            }
        }

        protected void gv_vendor_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            this.gv_vendor.PageIndex = e.NewPageIndex;
            this.BindVendorData(this.txt_vendorsearch.Text);
        }

        protected void gv_vendor_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "isEdit")
            {
                Response.Redirect(string.Format("VendorUpdated2?vendorCode={0}", e.CommandArgument));
            }
        }

        protected void btn_vendorsearch_Click(object sender, EventArgs e)
        {
            this.BindVendorData(this.txt_vendorsearch.Text);
        }
    }
}