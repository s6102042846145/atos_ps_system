﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorUpdated.aspx.cs" Inherits="PS_System.form.Selling.BidSellingUpdated" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="bootstrap-4.6.0-dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div class="container mb-4 mt-4">
            <div class="text-right">
                <a href="VendorData.aspx" class="btn btn-secondary">Go Back</a>
            </div>
            <div class="form-row mb-4">
                <div class="form-group  col-12">
                    <h5>รายละเอียดบริษัท</h5>
                </div>

                <div class="form-group  col-4">
                    <label>Vender code.</label>
                    <asp:TextBox ID="txt_venderCode" runat="server" CssClass="form-control" MaxLength="50" ReadOnly="true"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="req_venderCode" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_venderCode" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group  col-8">
                    <label>Vender name.</label>
                    <asp:TextBox ID="txt_companyName" runat="server" CssClass="form-control" MaxLength="250"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_companyName" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>

                <div class="form-group col-12">
                    <label>Vender address.</label>
                    <asp:TextBox ID="txt_address" runat="server" ReadOnly="False" TextMode="MultiLine" Rows="3" CssClass="form-control" MaxLength="1000"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_address" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>

                <div class="form-group col-4">
                    <label>Vender telephone.</label>
                    <asp:TextBox ID="txt_Tel" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_Tel" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-4">
                    <label>Vender fax.</label>
                    <asp:TextBox ID="txt_Fax" runat="server" CssClass="form-control" MaxLength="150"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_Fax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-4">
                    <label>Vender tax.</label>
                    <asp:TextBox ID="txt_Tax" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_Tax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>

                <div class="form-group  col-6">
                    <label>EGP no.</label>
                    <asp:TextBox ID="txt_EGPNo" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_EGPNo" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group  col-6">
                    <label>EGP register.</label>
                    <asp:TextBox ID="txt_EGPDate" runat="server" CssClass="form-control" TextMode="Date"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_EGPDate" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>

                <div class="form-group col-6">
                    <label>Contact name.</label>
                    <asp:TextBox ID="txt_contactName" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactName" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-6">
                    <label>Contact email.</label>
                    <asp:TextBox ID="txt_contactEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactEmail" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>

                <div class="form-group col-4">
                    <label>Contact Telephone.</label>
                    <asp:TextBox ID="txt_contactTel" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactTel" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-4">
                    <label>Contact Fax.</label>
                    <asp:TextBox ID="txt_contactFax" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_contactFax" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group col-4">
                    <label>Country.</label>
                    <asp:TextBox ID="txt_country" runat="server" CssClass="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ErrorMessage="Required" CssClass="text-danger" ControlToValidate="txt_country" SetFocusOnError="true"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="text-right mb-4">
                <asp:Button ID="btn_save" runat="server" Text="Update" CssClass="btn btn-primary" OnClick="btn_save_Click" OnClientClick="if (!confirm('Do you want to update this vendor?')) return false;" />
            </div>
        </div>
    </form>
    <script src="bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
</body>
</html>
