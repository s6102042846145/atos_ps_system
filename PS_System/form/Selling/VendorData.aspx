﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VendorData.aspx.cs" Inherits="PS_System.form.Selling.VendorData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="bootstrap-4.6.0-dist/css/bootstrap.min.css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">

        <div class="container mb-4 mt-4">
            <div class="text-right">
                <a href="BidSelling.aspx" class="btn btn-secondary">Go Back</a>
            </div>
            <div class="col-12">
                <h5>ข้อมูลผู้ซื้อ</h5>
            </div>
            <div class="col-12">
                <asp:GridView ID="gv_vendor" runat="server" CssClass="table table-hover" AutoGenerateColumns="false" AllowPaging="true" PageSize="10" OnPageIndexChanging="gv_vendor_PageIndexChanging" 
                    OnRowCommand="gv_vendor_RowCommand" DataKeyNames="vendor_code">
                    <Columns>
                        <asp:BoundField DataField="vendor_code" HeaderText="Code" />
                        <asp:BoundField DataField="vendor_name" HeaderText="Name" />
                        <asp:BoundField DataField="egp_no" HeaderText=" EGP No." />
                        <asp:BoundField DataField="egp_register_date" HeaderText="Date Register" DataFormatString="{0:dd/MM/yyyy}" />
                        <asp:BoundField DataField="address" HeaderText="Address" />
                        <asp:BoundField DataField="contact_person" HeaderText="Name" />
                        <asp:BoundField DataField="contact_tel" HeaderText="Telephone" />
                        <asp:TemplateField HeaderStyle-Width="50" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>
                                <asp:Button CssClass="btn btn-info" Text="Edit" runat="server" CommandName="isEdit" CommandArgument='<%#Eval("vendor_code")%>' />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                    <HeaderStyle CssClass="small" />
                </asp:GridView>
            </div>
        </div>
    </form>
    <script src="bootstrap-4.6.0-dist/js/bootstrap.min.js"></script>
</body>
</html>
