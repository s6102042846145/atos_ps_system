﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BidContractMgt2.aspx.cs" Inherits="PS_System.form.Selling.BidContractMgt2" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Contract Management</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .auto-style1 {
            height: 20px;
        }
        .auto-style2 {
            height: 20px;
            width: 101px;
        }
        .auto-style3 {
            width: 101px;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            height: 24px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Contract Management"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;" >
            <table style="width: 100%;">
                <tr>
                    <td colspan="3" style="background-color: #007ACC; color: #FFFFFF;">
                        <asp:Label ID="Label60" runat="server" Text="Basic Info"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style4">
                        <asp:Label ID="Label1" runat="server" Text="Bid No.:"></asp:Label>
                    </td>
                    <td class="auto-style4">
                        <asp:TextBox ID="txtInvitationBidNo" runat="server" Width="121px">Invitation to Bid No.</asp:TextBox>
                        <asp:TextBox ID="txtBidNo" runat="server" Width="200px"></asp:TextBox>
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" />
                    </td>
                    <td class="auto-style4">
                        <asp:Label ID="Label62" runat="server" Text="Revision:"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtBidRev" runat="server" ReadOnly="true" Enabled="False" Width="67px"></asp:TextBox>
                    &nbsp;</td>
                    <td class="auto-style4">
                        </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label11" runat="server" Text="Scope of Work:"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtScopeOfWork" runat="server" Width="400px"></asp:TextBox>
                    </td>
                    <td rowspan="7" style="text-align: left; vertical-align: top">
                        <asp:Label ID="Label52" runat="server" Text="Bid Description:"></asp:Label>
                        <asp:TextBox ID="txtBidDescription" runat="server" BorderColor="Silver" BorderStyle="Solid" BorderWidth="1px" Enabled="False" ReadOnly="True" Rows="3" TextMode="MultiLine" Width="544px" Height="148px"></asp:TextBox>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </td>
                    <td rowspan="3">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label12" runat="server" Text="Project:"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtProject" runat="server" Width="400px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Text="Job No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtJobNo" runat="server" Width="400px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Text="Contract No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContractNo" runat="server" Width="400px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label13" runat="server" Text="Transmission Line No.:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTransmissionLineNo" runat="server" ReadOnly="true"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label5" runat="server" Text="Line Description:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLineDescription" runat="server" Width="400px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label14" runat="server" Text="Line Length:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLineLength" runat="server"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label40" runat="server" Text="K.M."></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label41" runat="server" Text="Within Months: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtWithinMonths" runat="server"></asp:TextBox>
                    &nbsp;</td>
                    <td>
                        <asp:Label ID="Label70" runat="server" Text="Status:"></asp:Label>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="txtStatus" runat="server" ReadOnly="True" Enabled="False" Width="67px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #007ACC; color: #FFFFFF;">
                        <asp:Label ID="Label59" runat="server" Text="Purchasing data"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label42" runat="server" Text="Contractor: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtContractor" runat="server" Width="400px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label43" runat="server" Text="Purchasing Method : "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPurchasingMethod" runat="server" Width="400px">03 | ประกวดราคา</asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        <asp:Label ID="Label44" runat="server" Text="Purchasing Org:"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtPurchasingOrg" runat="server" Width="200px">2000</asp:TextBox>
                    </td>
                    <td class="auto-style1">
                    </td>
                    <td class="auto-style1">
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">
                        <asp:Label ID="Label45" runat="server" Text="Purchasing Group:"></asp:Label>
                    </td>
                    <td class="auto-style5">
                        <asp:TextBox ID="txtPurchasingGroup" runat="server" Width="200px"></asp:TextBox>
                    </td>
                    <td class="auto-style5">
                        </td>
                    <td class="auto-style5">
                        </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label46" runat="server" Text="L/I Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLIDate" runat="server"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label67" runat="server" ForeColor="DimGray" Text="dd/mm/yyyy"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label47" runat="server" Text="Confrim L/I Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtConfirmLIDate" runat="server"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label68" runat="server" ForeColor="DimGray" Text="dd/mm/yyyy"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label6" runat="server" Text="Contract Sign Date:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtSignDate" runat="server"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label69" runat="server" ForeColor="DimGray" Text="dd/mm/yyyy"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label48" runat="server" Text="Requisitioner: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtRequisitioner" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label49" runat="server" Text="Plant: "></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtPlant" runat="server"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" style="background-color: #007ACC; color: #FFFFFF;">
                        <asp:Label ID="Label53" runat="server" Text="Total Amont and Currency"></asp:Label>
                    &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label50" runat="server" Text="Total THB."></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTotalTHB" runat="server" Width="250px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label51" runat="server" Text="Total USD."></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtTotalUSD" runat="server" Width="250px"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td colspan="3">
                        &nbsp;
                         <table class="nav-fit">
                            <tr>
                                <td class="auto-style1" colspan="5">
                        <asp:Label ID="Label61" runat="server" Text="Currency &amp; Exchange Rate" Font-Bold="False" Font-Underline="True"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">
                        <asp:Label ID="Label56" runat="server" Text="Currency"></asp:Label>
                                </td>
                                <td class="auto-style1">
                        <asp:TextBox ID="txtCurrency1" runat="server"></asp:TextBox>
                                </td>
                                <td class="auto-style1">
                        <asp:TextBox ID="txtCurrency2" runat="server"></asp:TextBox>
                                </td>
                                <td class="auto-style1">
                        <asp:TextBox ID="txtCurrency3" runat="server"></asp:TextBox>
                                </td>
                                <td class="auto-style1">
                        <asp:TextBox ID="txtCurrency4" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style3">
                        <asp:Label ID="Label57" runat="server" Text="Date"></asp:Label>
                                &nbsp;<asp:Label ID="Label66" runat="server" ForeColor="DimGray" Text="dd/mm/yyyy"></asp:Label>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExDate1" runat="server"></asp:TextBox>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExDate2" runat="server"></asp:TextBox>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExDate3" runat="server"></asp:TextBox>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExDate4" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style3">
                        <asp:Label ID="Label58" runat="server" Text="Exchange Rate"></asp:Label>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExRate1" runat="server"></asp:TextBox>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExRate2" runat="server"></asp:TextBox>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExRate3" runat="server"></asp:TextBox>
                                </td>
                                <td>
                        <asp:TextBox ID="txtExRate4" runat="server"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style3">&nbsp;</td>
                                <td>
                                    <asp:Button ID="btnUpdateRate" runat="server" Text="Update" Width="100px" Visible="False" />
                                </td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label15" runat="server" Text="Insurance Percentage:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtInsurance" runat="server" Text="0.00"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label17" runat="server" Text="Customer Clearance:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtCustomer_Clearance" runat="server" Text="0.01"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label16" runat="server" Text="VAT:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtVAT" runat="server" Text="0.07"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label18" runat="server" Text="Local Preference:"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtLocal_Preference" runat="server" Text="0.07"></asp:TextBox>
                    </td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Save Draft" OnClick="btnSave_Click" />
                        &nbsp;&nbsp;<asp:Button ID="btnSubmit" CssClass="btn btn-success" ClientIDMode="Static" runat="server" Width="165px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Submit to SAP" OnClick="btnSubmit_Click" />
                        &nbsp;&nbsp;<asp:Button ID="btnCancel" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Cancel" />
                    </td>
                </tr>
            </table>
        </div>
        <asp:HiddenField ID="hidContractNo" runat="server" />
        <br />
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRevision" runat="server" />
    </form>
</body>
</html>
