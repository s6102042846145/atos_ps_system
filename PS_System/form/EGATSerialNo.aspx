﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="EGATSerialNo.aspx.cs" Inherits="PS_System.form.EGATSerialNo" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Bid Contract Management</title>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <script src="../../Scripts/jq_1.12.4/jquery-1.12.4.js"></script>
    <link href="../../Scripts/jq_1.12.4/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jq_1.12.4/jquery-ui.js"></script>

    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script src="../../Scripts/aes.js"></script>
    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .auto-style1 {
            height: 20px;
        }
        .auto-style5 {
            height: 26px;
        }
        .auto-style10 {
            height: 20px;
            width: 140px;
        }
        .auto-style13 {
            height: 5px;
            width: 147px;
        }
        .auto-style14 {
            height: 5px;
            width: 140px;
        }
        .auto-style15 {
            height: 5px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - EGAT Serial No."></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding: 10px 10px 10px 10px;" >
            <table style="width: 100%;">
                
                <tr>
                    <td class="auto-style14">
                        <asp:Label ID="Label1" runat="server" Text="Contack No."></asp:Label>
                    </td>
                    <td class="auto-style15">
                                 <select id="mtContackNo" runat="server" name="D4" style="width: 550px">
                                 </select></td>
                    <td class="auto-style13" >
                        <asp:Label ID="Label16" runat="server" Text="Vender No."></asp:Label>
                    </td>
                    <td class="auto-style15">
                                 <select id="mtVenderNo" runat="server" name="D5" style="width: 550px">
                                 </select></td>
                </tr>
                <tr>
                    <td class="auto-style10">
                        <asp:Label ID="Label52" runat="server" Text="Status"></asp:Label>
                    </td>
                    <td class="auto-style1">
                        <asp:TextBox ID="txtStatus" runat="server" Width="550px" ToolTip="Not Start / Complete"></asp:TextBox>
                    </td>
                  
                </tr>
                <tr>
                    <td>
                      <asp:Button ID="btnSearch" runat="server" class="btn btn-primary"  Text="Show Item List" />
                              
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="background-color: #FFFFFF; color: #FFFFFF;" class="auto-style5">
                    &nbsp;</td>
                    <td class="auto-style5">
                        </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <div runat="server" id="dvGvstock" visible="false" style="width: 100%; height: 400px; overflow: scroll">
                                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                        <ContentTemplate>

                                            <asp:GridView ID="gvSerial" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." 
                                                AllowPaging="True"  PageSize="20" ShowHeader = "true"  >
                                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                <RowStyle ForeColor="#000066" />
                                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                                <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="chkSelect" runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Item No.">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("plant_code") %>'></asp:Label>
                                                            <%--Text='<%# Bind("job_no") %>'--%>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Equip Code(Old Mat.)">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbEqCode" runat="server" Text='<%# Bind("code_old") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Equipment">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbEqm" runat="server" Text='<%# Bind("Equipment") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="80px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                  
                                                    <asp:TemplateField HeaderText="QTY">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbQTY" runat="server" Text='<%# Bind("QUANTITY") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="50px" HorizontalAlign="center" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Code A">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbCodeA" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Code B">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbCodeB" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Code C">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbCodeC" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Code D">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbCodeD" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="Code E">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbCodeE" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="To">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbTo" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                     <asp:TemplateField HeaderText="From">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lbFrom" runat="server" Text='<%# Bind("unit_code") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" HorizontalAlign="Left" VerticalAlign="Top" />
                                                    </asp:TemplateField>
                                                </Columns>

                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                    </td>
                </tr>
              
                <tr>
                    <td colspan="4" style="text-align: left; vertical-align: middle; width: 100%;">
                        <asp:Button ID="btnGen" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="175px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Generate Serial No."  />
                        &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnGen0" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="175px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Export to Excel"  />
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>

