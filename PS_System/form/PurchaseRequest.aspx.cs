﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form
{
    public partial class PurchaseRequest : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            bindFristDatagvAttachments();
            bindFristDatagvOffer();
        }

        private void bindFristDatagvAttachments()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("No");
            dt.Columns.Add("DocType");
            dt.Columns.Add("DocNo"); 
            dt.Columns.Add("Description");
            dt.Columns.Add("ViewDoc");
            dt.Columns.Add("Status");

            DataRow dr = dt.NewRow();
            dr["No"] = "";
            dr["DocType"] = "";
            dr["DocNo"] = "";
            dr["Description"] = "";
            dr["ViewDoc"] = "";
            dr["Status"] = "";

            dt.Rows.Add(dr);

            gvAttachments.DataSource = dt;
            gvAttachments.DataBind();

        }
        private void bindFristDatagvOffer()
        {
            DataTable dt = new DataTable();

            dt.Columns.Add("No");

            DataRow dr = dt.NewRow();
            dr["No"] = "1";
            dt.Rows.Add(dr);


            dr = dt.NewRow();
            dr["No"] = "2";
            dt.Rows.Add(dr);

            dr = dt.NewRow();
            dr["No"] = "3";
            dt.Rows.Add(dr);

            gvOffer.DataSource = dt;
            gvOffer.DataBind();

        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
    }
}