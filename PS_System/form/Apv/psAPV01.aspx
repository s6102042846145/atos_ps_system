﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="psAPV01.aspx.cs" Inherits="PS_System.form.Apv.psAPV01" %>

<%@ Register src="../../UserControl/wucComment_Att.ascx" tagname="wucComment_Att" tagprefix="uc1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 138px;
        }

        .auto-style3 {
            width: 800px;
        }

        #docview {
            height: 650px;
            width: 1500px;
                /*893px;*/
        }
        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            /*padding-top: 52px;*/ /* Location of the box */
           /* padding-left: 50px;*/ /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
        .btnclass{
            
            background:white;
            color:#508bcb;
            border:none;
            height:40px;
            width:80px;
            border-radius:5px
        }
        .btnSel{
            
            background:white;
            color:black;
            height:40px;
            width:80px;
            border-left:solid;
            border-top:solid;
            border-right:solid;
            border-bottom:none;
            border-width:thin;
            border-radius:5px 5px 0px 0px;
        }
        .btnclass:hover {
            background: #eeeeee;
        }
        .btnPartSel{
            color:white;
            border-color:#357ebd;
            background-color:#357ebd;
            border:1px solid transparent;
            border-radius:3px;
        }
        .Colsmall {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
        .Colsmall2 {
            width: 80px;
            max-width: 80px;
            min-width: 80px;
        }
        .ColsmallDoc {
            width: 110px;
            max-width: 110px;
            min-width: 110px;
        }
        .ColsmallDoc2 {
            width: 95px;
            max-width: 95px;
            min-width: 95px;
        }
        #gvAllEqui th {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
        #gvTaskDoc th {
            width: 100px;
            max-width: 100px;
            min-width: 100px;
        }
    </style>
    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>
     <link href="../../Content/w3.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-3.4.1.min.js"></script>
    <script>
        function openPopup(x) {
            var myModal = document.getElementById("myModal");
            var dvAllTab = document.getElementById("dvAllTab");         
            if (myModal != null) {
                myModal.style.display = "block";
            }
            dvAllTab.style.display = "block";
            document.getElementById("hidTab").value = "1";
            document.getElementById("dvhidEq").style.display = "none";
            document.getElementById("dvhiddraw").style.display = "none";
            document.getElementById("dvhidDoc").style.display = "none";
        }
        function closePopup(x) {
            var myModal = document.getElementById("myModal");
            var dvAllTab = document.getElementById("dvAllTab");
            if (myModal != null && dvAllTab != null) {
                myModal.style.display = "none";
                dvAllTab.style.display = "none";
                document.getElementById("hidTab").value = "";
            }

        }

        function openTab(chk, ima) {
            var x = document.getElementById(chk);
            var img = document.getElementById(ima);
            if (x.style.display === "none") {
                x.style.display = "block";
                img.src = '../../images/sort_asc.png';

            } else {
                x.style.display = "none";
                img.src = '../../images/sort_desc.png';
            }
        }
        $(document).ready(function () {
            var width = new Array();
            var table = $("table[id*=gvAllEqui]"); //Pass your gridview id here.
            table.find("th").each(function (i) {
                width[i] = $(this).width();
            });
            headerRow = table.find("tr:first");
            headerRow.find("th").each(function (i) {
                $(this).width(width[i]);
            });
            firstRow = table.find("tr:first").next();
            firstRow.find("td").each(function (i) {
                $(this).width(width[i]);
            });
            var header = table.clone();
            header.empty();
            header.append(headerRow);
            //header.append(firstRow);
            header.css("width", width);
            $("#container").before(header);
            table.find("tr:first td").each(function (i) {
                $(this).width(width[i]);
            });
            $("#container").height(700);
            //$("#container").width(table.width() + 20);
            $("#container").append(table);
        });
        $(document).ready(function () {
            var width = new Array();
            var table = $("table[id*=gvTaskDoc]"); //Pass your gridview id here.
            table.find("th").each(function (i) {
                width[i] = $(this).width();
            });
            headerRow = table.find("tr:first");
            headerRow.find("th").each(function (i) {
                $(this).width(width[i]);
            });
            firstRow = table.find("tr:first").next();
            firstRow.find("td").each(function (i) {
                $(this).width(width[i]);
            });
            var header = table.clone();
            header.empty();
            header.append(headerRow);
            //header.append(firstRow);
            header.css("width", width);
            $("#containerDoc").before(header);
            table.find("tr:first td").each(function (i) {
                $(this).width(width[i]);
            });
            $("#containerDoc").height(400);
            //$("#container").width(table.width() + 20);
            $("#containerDoc").append(table);
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel5" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="lblHeader" runat="server" Font-Bold="true" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Job approval workflow"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <table class="auto-style1" style="font-family: tahoma; font-size: 10pt; color: #333333;">
            <%--<tr>
                <td>
                    <asp:Label ID="lblTitle" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Title"></asp:Label>
                </td>
            </tr>--%>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style3">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel9" runat="server" Width="90px">
                                    <asp:Label ID="lblRequestor" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Requestor :"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" Width="500px">
                                    <asp:Label ID="lblRequestorVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>
                                <asp:Panel ID="Panel7" runat="server" Width="60px">
                                    <asp:Label ID="lblSubmitDate" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="DP No.  :"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>
                                <asp:Panel ID="Panel1" runat="server" Width="500px">
                                    <asp:Label ID="lblPsReqNo" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblTo" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="To :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel4" runat="server" Width="500px">
                                    <asp:Label ID="lblToVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>

                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td></td>
                            <td></td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="margin-left: 320px">
                                <asp:Label ID="Label2" runat="server" Font-Bold="true" Font-Names="tahoma" Font-Size="10pt" Text="Subject :"></asp:Label>
                            </td>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" Width="500px">
                                    <asp:Label ID="lblSubjectVal" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                                </asp:Panel>
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                 <td colspan="6" style="text-align:left">
                                <asp:Button ID="bt_view" runat="server" Text="View" onclientclick="openPopup();return false;" class="btn btn-primary"/>
                 </td>
            </tr>
            <tr>
                <td> 
                    <asp:Panel ID="p_PDF" runat="server" BackColor="#666666" Height="650px" Width="1450px">
                        <iframe id="docview" src="about:blank" runat="server"></iframe>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Names="tahoma" Font-Size="9pt" Font-Underline="False" Text="Note"></asp:Label>
                </td>
            </tr>
            <tr>

                <td>
                    <asp:Panel ID="p_Comment" runat="server">
                        <asp:TextBox ID="txtComment" runat="server" Font-Names="tahoma" Font-Size="10pt" Height="80px" TextMode="MultiLine" Width="800px"></asp:TextBox>
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <table cellpadding="0" cellspacing="0" class="auto-style2">
                        <tr>
                            <td>
                                <asp:Button ID="btnApprove" runat="server" Font-Names="Tahoma" Font-Size="10pt" OnClick="btnApprove_Click" Text="Approve" Width="100px"  class="btn btn-primary"/>
                            </td>
                            <td>
                                <asp:Button ID="btnReject" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Reject" Width="100px" OnClick="btnReject_Click" class="btn btn-danger" />
                            </td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <uc1:wucComment_Att ID="wucComment_Att1" runat="server" />
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
         <div id="myModal" runat="server" class="modal">
              <div class="modal-content">
                  <div id="dvAllTab" runat="server" style="display:none;width:100%">
                      <table style="width: 100%;">
                          <tr>
                              <td>
                                  <div id="dvDrawshow" runat="server" onclick="openTab('dvhiddraw','Imag2');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block">
                            <asp:Image ID="Imag2" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="Label14" style="font-size:smaller" runat="server" Text="Bid 1"><b>Design Drawing / Typical Drawing</b></asp:Label>
                                 </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div id="dvhiddraw" runat="server" style="display: none;">
                                      <asp:GridView ID="gvDrawing" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvDrawing_RowDataBound">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbNo" runat="server" Text=' <%# Container.DataItemIndex + 1 %>'></asp:Label>
                                            <asp:Label ID="lbDocno" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="lbGruop" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDivis" runat="server" Text='<%# Bind("section_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDoct" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                             <asp:Label ID="lbDoctSH" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDes" runat="server" Text='<%# Bind("tr_doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbFilename" runat="server" Text='<%# Bind("doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sheet No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSheet" runat="server" Text='<%# Bind("doc_sheetno") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:Label ID="lbRevi" runat="server" Text='<%# Bind("doc_revision") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="30px" HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpdated" runat="server" Text='<%# Bind("tr_updated_datetime") %>'></asp:Label>
                                            <asp:Label ID="lbCreby" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbCrebydate" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbhidDate" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update by">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpby" runat="server" Text='<%# Bind("tr_updated_by") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bid No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbBid" runat="server" Text='<%# Bind("bid_no") %>'></asp:Label>
                                                <asp:Label ID="lbRevbiddr" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbhidSch" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle Width="250px" />
                                    </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Schdule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSchnamedr" runat="server" Text='<%# Bind("schedule_name") %>'></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ViewDoc">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="link_DrawDoc" runat="server">ViewDoc</asp:LinkButton>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                  </div>
                                  <br />
                              </td>
                          </tr>
                          <tr>
                            <td>
                                 <div id="dvOpen" runat="server" onclick="openTab('dvhidEq','Imag1');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block;">
                            <asp:Image ID="Imag1" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="lbbid" runat="server" style="font-size:smaller" Text="Bid 1"><b>Equipment List</b></asp:Label>
                                 </div>
                            </td>
                          </tr>
                          <tr>
                              <td>
                                   <div id="dvhidEq" runat="server" style="display: none;">
                                        <div class="container" id="allTabEquip" runat="server" style="width:100%;">
                                            <ul class="nav nav-tabs" id="ultab" runat="server">
                                            </ul>
                                            <div id="dv_gvEquipRela" runat="server" style="position:relative;overflow-x:auto;overflow-y:hidden;height:450px;">
                                            <div id="dv_gvEquipAbs" runat="server" style="position:absolute;">
                                                <div id="container" style="overflow-y: auto;"> </div>
                                                <asp:GridView ID="gvAllEqui" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                    AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." OnRowDataBound="gvAllEqui_RowDataBound" >
                                    <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" />
                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                    <RowStyle ForeColor="#000066" />
                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="Meterial Group" >
                                            <ItemTemplate >
                                               <%-- <asp:ListBox ID="ddl_meterialGrp" runat="server"></asp:ListBox>--%>
                                                 <asp:Label ID="lb_meterialGrp" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle  CssClass="Colsmall" />
                                            
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Code No.">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_codeNo" runat="server"></asp:Label>
                                                <asp:Label ID="lb_legend" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Item No.">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_itmNo" runat="server" Width="60px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_itmNo" runat="server"></asp:Label>
                                                
                                            </ItemTemplate>
                                                  <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Full Description">
                                            <ItemTemplate>

                                                <asp:Label ID="lb_fDesc" runat="server"></asp:Label>
                                                 <asp:Label ID="lb_fDesc2" runat="server" Visible="false"></asp:Label>
                                                 <asp:Label ID="lb_hide" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_jobno" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_scheno" runat="server" Text="Label" Visible="False"></asp:Label>
                                                <asp:Label ID="lb_bidno" runat="server" Text="Label" Visible="False"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Additional Description">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_addDesc" runat="server"></asp:TextBox>--%>
                                                <asp:Label ID="lb_addDesc" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Required QTY">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_reqQTY" runat="server" Width="25px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_reqQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Center" CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Unit">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_unit" runat="server"></asp:Label>
                                               
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Reserved QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_revQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Confirmed Reservation QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="conrevQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                                  <ItemStyle  CssClass="Colsmall" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Bid QTY">
                                            <ItemTemplate>
                                                <asp:Label ID="lb_bidQTY" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Incoterms">
                                            <ItemTemplate>
                                                <%--<asp:ListBox ID="ddl_inco" runat="server" SelectionMode="Multiple"></asp:ListBox>--%>
                                                <asp:Label ID="lb_inco" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Waive Test">
                                            <ItemTemplate>
                                                <%--<asp:ListBox ID="ddl_wa" runat="server"  SelectionMode="Multiple"></asp:ListBox>--%>
                                                 <asp:Label ID="lb_wa" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Related Document">
                                            <ItemTemplate>
                                               <%-- <asp:LinkButton ID="lnk_doc" runat="server">linkdoc</asp:LinkButton>--%>
                                                 <asp:Label ID="lb_doc" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Currency">
                                            <ItemTemplate>
                                                <%--<asp:DropDownList ID="ddl_cur" runat="server"></asp:DropDownList>--%>
                                                  <asp:Label ID="lb_cur" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOE. Unit Price">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_supUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_supUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="SOE. Amount">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_supAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_supAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LSE. Unit Price">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localSUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localSUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LSE. Amount">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localSAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localSAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LTC. Unit Price">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localTUnit" runat="server"  Width="90px"></asp:TextBox>--%>
                                                <asp:Label ID="lb_localTUnit" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall"  />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="LTC. Amount">
                                            <ItemTemplate>
                                                <%--<asp:TextBox ID="tb_localTAmou" runat="server"  Width="90px"></asp:TextBox>--%>
                                                 <asp:Label ID="lb_localTAmou" runat="server"></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle CssClass="Colsmall2"  />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                                            </div>
                                                <div id="dvPart" runat="server">

                                           </div>
                                            </div>
                                        </div>
                                   </div>
                                  <br />
                              </td>
                          </tr>
                          <tr>
                              <td>
                                   <div id="dvOpenDoc" runat="server" onclick="openTab('dvhidDoc','Imag3');" style="background-color: #164094; width: 100%; height: 30px; text-align: left; color: white; cursor: pointer;display:block;">
                            <asp:Image ID="Imag3" runat="server" ImageUrl="~/images/sort_desc.png" Width="30px" Height="30px" />
                            <asp:Label ID="Label31" style="font-size:smaller" runat="server" Text="Bid 1"><b>Documents</b></asp:Label>
                                    </div>
                              </td>
                          </tr>
                          <tr>
                              <td>
                                  <div id="dvhidDoc" runat="server" style="display: none;">
                                       <div id="dv_gvEquipRelaDoc" runat="server" style="position:relative;overflow-x:auto;overflow-y:hidden;height:450px;">
                                            <div id="dv_gvEquipAbsDoc" runat="server" style="position:absolute;">
                                            <div id="containerDoc" style="overflow-y: auto;"> </div>
                            <asp:GridView ID="gvTaskDoc" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt"  EmptyDataText="No data found." OnRowDataBound="gvTaskDoc_RowDataBound">
                                <FooterStyle BackColor="#164094" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbTDocNo" runat="server" Text='<%# Container.DataItemIndex + 1 %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="กอง">
                                        <ItemTemplate>
                                            <asp:Label ID="lbGroup" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="แผนก">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDivis" runat="server"  Text='<%# Bind("section_position_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Type">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDocCode" runat="server"  Text='<%# Bind("doctype_name") %>'></asp:Label>
                                            <asp:Label ID="lbDoctSH" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="lbDescrip" runat="server"  Text='<%# Bind("tr_doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="File Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbFilename" runat="server"  Text='<%# Bind("doc_desc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Sheet No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSheetno" runat="server"  Text='<%# Bind("doc_sheetno") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Revision">
                                        <ItemTemplate>
                                            <asp:Label ID="lbRev" runat="server"  Text='<%# Bind("doc_revision") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update Date">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpdatedate" runat="server"  Text='<%# Bind("updated_datetime") %>'></asp:Label>
                                            <asp:Label ID="lbhidDate3" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Update by">
                                        <ItemTemplate>
                                            <asp:Label ID="lbUpby" runat="server"  Text='<%# Bind("updated_by") %>'></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Bid No.">
                                        <ItemTemplate>
                                            <asp:Label ID="lbBidno" runat="server"  Text='<%# Bind("bid_no") %>'></asp:Label>
                                            <asp:Label ID="lbRevbid" runat="server" Visible="false"></asp:Label>
                                            <asp:Label ID="lbDocno" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <%--<ItemStyle Width="250px" />--%>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Schdule Name">
                                        <ItemTemplate>
                                            <asp:Label ID="lbSchname" runat="server"  Text='<%# Bind("schedule_name") %>'></asp:Label>
                                            <asp:Label ID="lbSchnohid" runat="server" Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle CssClass="ColsmallDoc"  />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ViewDoc">
                                        <ItemTemplate>
                                            <asp:LinkButton ID="link_DrawDoc" runat="server" >ViewDoc</asp:LinkButton>
                                        </ItemTemplate>
                                        <%--<ItemStyle HorizontalAlign="Center" />--%>
                                        <ItemStyle CssClass="ColsmallDoc2"  />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                                               </div>
                                      </div>
                        </div>
                                  <br />
                              </td>
                          </tr>
                          <tr>
                              <td style="text-align:right">
                                   <asp:Button ID="bt_cancView" runat="server" Text="Close" class="btn btn-danger" onclientclick="closePopup();return false;"/>
                              </td>
                          </tr>
                      </table>
                  </div>
              </div>
         </div>
        <asp:HiddenField ID="hidPSDPNo" runat="server" />
        <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidRequestorId" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" />
        <asp:HiddenField ID="hidWorkID" runat="server" />
        <asp:HiddenField ID="hidSubworkID" runat="server" />
        <asp:HiddenField ID="hidTaskID" runat="server" />
        <asp:HiddenField ID="hidApproverCount" runat="server" />
        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRev" runat="server" />
        <asp:HiddenField ID="hidJobNo" runat="server" />
        <asp:HiddenField ID="hidJobRev" runat="server" />
        <asp:HiddenField ID="hidActivityId" runat="server" />
        <asp:HiddenField ID="hidProcessID" runat="server" />
        <asp:HiddenField ID="hidTab" runat="server" />
        <asp:HiddenField ID="hidNodeID" runat="server" />
        <asp:HiddenField ID="hidChkBid" runat="server" />
    </form>
</body>
</html>
