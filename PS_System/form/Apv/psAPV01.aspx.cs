﻿using PS_Library;
using PS_Library.DocumentManagement;
using PS_Library.WorkflowService;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.Apv
{
    public partial class psAPV01 : System.Web.UI.Page
    {
        private string strParentPdfNodeId = ConfigurationManager.AppSettings["wf_att_folder"].ToString(); //ConfigurationManager.AppSettings["wf_job_att_node_id"].ToString();
        private Node doc_node = new Node();

        public string zisDev = ConfigurationManager.AppSettings["isDev"].ToString();
        public string zmode = "";
        public string zworkid = "";
        public string zsubworkid = "";
        public string ztaskid = "";
        public string zpsdb = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string zhost_name = ConfigurationManager.AppSettings["HOST_NAME"].ToString();
        private string strWfJobMapId = ConfigurationManager.AppSettings["wf_job_map_id"].ToString();
        private string strWfBidMapId = ConfigurationManager.AppSettings["wf_job_map_id"].ToString();


        public EmpInfoClass zempInfo = new EmpInfoClass();
        clsWfPsAPV01 objWf = new clsWfPsAPV01();

        static PS_Library.OTFunctions zotFunctions = new PS_Library.OTFunctions();
        static PS_Library.DbControllerBase zdbUtil = new PS_Library.DbControllerBase();
        static PS_Library.LogHelper zLogHelper = new PS_Library.LogHelper();

        protected void Page_Load(object sender, EventArgs e)
        {

            //TestStartWf();
            //TestStartWfBid();

            if (!IsPostBack)
            {

                try { zmode = Request.QueryString["zmode"].ToString().ToUpper(); }
                catch { zmode = "reviewer"; }
                try { zworkid = Request.QueryString["workid"].ToString().ToUpper(); }
                catch { zworkid = "2817591"; }
                try { zsubworkid = Request.QueryString["subworkid"].ToString().ToUpper(); }
                catch { zsubworkid = "2817591"; }
                try { ztaskid = Request.QueryString["taskid"].ToString().ToUpper(); }
                catch { ztaskid = "1"; }
                try { hidLogin.Value = Request.QueryString["zuserlogin"].ToString(); }
                catch { hidLogin.Value = "z575798"; }


                hidMode.Value = zmode;
                hidWorkID.Value = zworkid;
                hidSubworkID.Value = zsubworkid;
                hidTaskID.Value = ztaskid;
                LoadData();
                bindAllTab();
                bind_equipment();
            }
            else
            {
                if (hidTab.Value == "1")
                {
                    bind_equipment();
                    myModal.Style["display"] = "block";
                    dvAllTab.Style["display"] = "block";
                    dvhidEq.Style["display"] = "block";
                }

            }
        }
        protected void bindAllTab()
        {
            DataTable dtDraw = new DataTable();
           if (hidJobNo.Value!="" && hidJobRev.Value!="") dtDraw = objWf.GetDrawing(hidJobNo.Value, "draw", hidJobRev.Value,"job",hidProcessID.Value);
           else if(hidBidNo.Value!="" && hidBidRev.Value!="") dtDraw = objWf.GetDrawing(hidBidNo.Value, "draw", hidBidRev.Value,"bid", hidProcessID.Value);
            if (dtDraw.Rows.Count > 0)
            {
                gvDrawing.DataSource = dtDraw;
                gvDrawing.DataBind();
            }
            else
            {
                gvDrawing.DataSource = null;
                gvDrawing.DataBind();
            }
            DataTable dtTask = new DataTable();
            if (hidJobNo.Value != "" && hidJobRev.Value != "") dtTask = objWf.GetDrawing(hidJobNo.Value, "", hidJobRev.Value, "job", hidProcessID.Value);
            else if (hidBidNo.Value != "" && hidBidRev.Value != "") dtTask = objWf.GetDrawing(hidBidNo.Value, "", hidBidRev.Value, "bid", hidProcessID.Value);
            if (dtTask.Rows.Count > 0)
            {
                gvTaskDoc.DataSource = dtTask;
                gvTaskDoc.DataBind();
            }
            else
            {
                gvTaskDoc.DataSource = null;
                gvTaskDoc.DataBind();
            }
            
        }
        protected void bind_equipment()
        {
            DataTable dt = new DataTable();
            string mode = "";
            string bidno = "";
            if (hidJobNo.Value != "") { dt = objWf.getTabBid(hidJobNo.Value, hidJobRev.Value); mode = "job"; }
            else if (hidBidNo.Value != "") { dt = objWf.GetSummary(hidBidNo.Value); mode = "bid"; }
            //hidBidNo.Value = "TS12-S-02";
            //dt = objWf.GetSummary(hidBidNo.Value); mode = "bid";
            string schName = "";
            //dt = objWf.GetSummary(hidBidNo.Value); mode = "bid";
            ultab.Controls.Clear();
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                Button bt_part = new Button();
                if (dt.Rows[i]["bid_no"].ToString() != "Bid Supply" && dt.Rows[i]["bid_no"].ToString().Length > 10) bt_part.Text = dt.Rows[i]["bid_no"].ToString().Substring(0, 10) + "...";
                else bt_part.Text = dt.Rows[i]["bid_no"].ToString();
                bt_part.ID = "bt_tab" + i.ToString();
                bt_part.ToolTip = dt.Rows[i]["bid_no"].ToString() + " " + dt.Rows[i]["schedule_name"].ToString();
                bt_part.Width = 100;
                bt_part.CommandName= dt.Rows[i]["schedule_name"].ToString()+","+ dt.Rows[i]["bid_no"].ToString();
                bt_part.Attributes["runat"] = "server";
                if (i == 0)
                {
                    bt_part.CssClass = "btnSel";
                    schName = dt.Rows[i]["schedule_name"].ToString();
                    hidChkBid.Value = dt.Rows[i]["bid_no"].ToString();
                }
                else
                {
                    bt_part.CssClass = "btnclass";
                }
                bt_part.Click += new EventHandler(selTab);
                li.Controls.Add(bt_part);
                ultab.Controls.Add(li);
            }
            //if (hidJobNo.Value != "") { dtpart = objWf.getBtnPaths_job(hidJobNo.Value); mode = "job"; }
            //else if (hidBidNo.Value != "") { dtpart = objWf.getBtnPaths_bid(hidBidNo.Value); mode = "bid"; }
            DataTable dtgv = new DataTable();
            if (hidJobNo.Value != "") { dtgv = objWf.getGVSelPart(hidChkBid.Value, hidBidRev.Value, hidJobNo.Value, hidJobRev.Value, mode, schName, hidProcessID.Value); }
            else if (hidBidNo.Value != "") { dtgv = objWf.getGVSelPart(hidBidNo.Value, hidBidRev.Value, hidJobNo.Value, hidJobRev.Value, mode, schName, hidProcessID.Value); }
            
            if (dtgv.Rows.Count > 0)
            {
                gvAllEqui.DataSource = dtgv;
                gvAllEqui.DataBind();

            }
            else
            {
                gvAllEqui.DataSource = null;
                gvAllEqui.DataBind();

            }

        }
        protected void selTab(object sender, EventArgs e)
        {
            DataTable dt = new DataTable();
            Button bt = sender as Button;
            string schName = "";
            string bidno = "";
            string a = bt.ID.Substring(bt.ID.IndexOf(bt.ID) + bt.ID.Length - 1);
            string[] spl = bt.CommandName.Split(',');
            if (spl.Length == 2)
            {
                schName = spl[0];
                bidno= spl[1];
            }
            DataTable dtgv = new DataTable();
            string mode = "";
            if (hidJobNo.Value != "")
            {
                dt = objWf.getTabBid(hidJobNo.Value, hidJobRev.Value); mode = "job";
                //dtgv = objWf.getGVSelPart(bidno, hidBidRev.Value, hidJobNo.Value, hidJobRev.Value, mode, schName, hidProcessID.Value);
            }
            else if (hidBidNo.Value != "")
            {
                dt = objWf.GetSummary(hidBidNo.Value); mode = "bid";
                //dtgv = objWf.getGVSelPart(hidBidNo.Value, hidBidRev.Value, hidJobNo.Value, hidJobRev.Value, mode, schName, hidProcessID.Value);
            }
            //dt = objWf.GetSummary("TS12-S-02");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string id = "bt_tab" + i;
                Button btn = (Button)this.FindControl(id);
                if (a == i.ToString())
                {
                    btn.CssClass = "btnSel";
                    hidChkBid.Value = dt.Rows[i]["bid_no"].ToString();
                }
                else
                {
                    btn.CssClass = "btnclass";
                }
            }
           
            if (hidJobNo.Value != "")
            {
                //dt = objWf.getTabBid(hidJobNo.Value, hidJobRev.Value); mode = "job";
                dtgv = objWf.getGVSelPart(hidChkBid.Value, hidBidRev.Value, hidJobNo.Value, hidJobRev.Value, mode, schName, hidProcessID.Value);
            }
            else if (hidBidNo.Value != "")
            {
                //dt = objWf.GetSummary(hidBidNo.Value); mode = "bid";
                dtgv = objWf.getGVSelPart(hidBidNo.Value, hidBidRev.Value, hidJobNo.Value, hidJobRev.Value, mode, schName, hidProcessID.Value);
            }

            if (dtgv.Rows.Count > 0)
            {
                gvAllEqui.DataSource = dtgv;
                gvAllEqui.DataBind();
            }
            else
            {
                gvAllEqui.DataSource = null;
                gvAllEqui.DataBind();

            }
        }

        protected void gvDrawing_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton link_DrawDoc = (LinkButton)e.Row.FindControl("link_DrawDoc");
                link_DrawDoc.Text = " <a href='" + drv["link_doc"].ToString() + "' target='_blank'> ViewDoc </a>";
            }
        }
        protected void gvTaskDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                LinkButton link_DrawDoc = (LinkButton)e.Row.FindControl("link_DrawDoc");
                link_DrawDoc.Text = " <a href='" + drv["link_doc"].ToString() + "' target='_blank'> ViewDoc </a>";
            }
        }
        protected void gvAllEqui_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.Header)
            {
                e.Row.Cells[14].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Unit Price";
                e.Row.Cells[15].ToolTip = @"Supply of Equipment 
Foreign Supply 
CIF Thai Port 
Amount";
                e.Row.Cells[16].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[17].ToolTip = @"Local Supply
Ex-works Price
(excluding VAT)
Baht
Amount";
                e.Row.Cells[18].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Unit Price";
                e.Row.Cells[19].ToolTip = @"Local Transpotation,
Contruction and
Installation
(excluding VAT)
Baht
Amount";
            }
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Label lb_meterialGrp = (Label)e.Row.FindControl("lb_meterialGrp");
                lb_meterialGrp.Text = drv["equip_mas_grp"].ToString();
                Label lb_codeNo = (Label)e.Row.FindControl("lb_codeNo");
                lb_codeNo.Text = drv["equip_code"].ToString();
                Label lb_itmNo = (Label)e.Row.FindControl("lb_itmNo");
                lb_itmNo.Text = drv["equip_item_no"].ToString();
                Label lb_fDesc = (Label)e.Row.FindControl("lb_fDesc");
                Label lb_fDesc2 = (Label)e.Row.FindControl("lb_fDesc2");
                lb_fDesc2.Text = drv["equip_full_desc"].ToString();
                lb_fDesc.Text = drv["equip_full_desc"].ToString();
                if (lb_fDesc.Text.Length > 30)
                {
                    lb_fDesc.Text = lb_fDesc.Text.Substring(0, 30) + "...";
                    lb_fDesc.ToolTip = drv["equip_full_desc"].ToString();
                }

                Label lb_addDesc = (Label)e.Row.FindControl("lb_addDesc");
                lb_addDesc.Text = drv["equip_add_desc"].ToString() + " " + drv["equip_standard_drawing"].ToString();
                if (lb_addDesc.Text.Length > 30)
                {
                    lb_addDesc.Text = lb_addDesc.Text.ToString().Substring(0, 30) + "...";
                    lb_addDesc.ToolTip = drv["equip_add_desc"].ToString() + " " + drv["equip_standard_drawing"].ToString();
                }
                //TextBox tb_stdDr = (TextBox)e.Row.FindControl("tb_stdDr");
                //tb_stdDr.Text = drv["equip_standard_drawing"].ToString();
                Label lb_reqQTY = (Label)e.Row.FindControl("lb_reqQTY");
                lb_reqQTY.Text = drv["equip_qty"].ToString();
                Label lb_unit = (Label)e.Row.FindControl("lb_unit");
                lb_unit.Text = drv["equip_unit"].ToString();
                Label lb_revQTY = (Label)e.Row.FindControl("lb_revQTY");
                Label conrevQTY = (Label)e.Row.FindControl("conrevQTY");
                Label lb_bidQTY = (Label)e.Row.FindControl("lb_bidQTY");
                Label lb_inco = (Label)e.Row.FindControl("lb_inco");
                lb_inco.Text = drv["equip_incoterm"].ToString();
                
                string wa = "";
                if (drv["factory_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[0].Selected = true;
                    wa = "factory";
                }
                if (drv["manual_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[1].Selected = true;
                    wa += " manual";
                }
                if (drv["routine_test"].ToString().ToLower() == "true")
                {
                    //ddl_wa.Items[2].Selected = true;
                    wa += " routine";
                }
                Label lb_wa = (Label)e.Row.FindControl("lb_wa");
                lb_wa.Text = wa;
                LinkButton lnk_res = (LinkButton)e.Row.FindControl("lnk_res");
               
                Label lb_cur = (Label)e.Row.FindControl("lb_cur");
                lb_cur.Text = drv["currency"].ToString();

                Label lb_supUnit = (Label)e.Row.FindControl("lb_supUnit");
                if (drv["supply_equip_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["supply_equip_unit_price"]);
                    lb_supUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_supAmou = (Label)e.Row.FindControl("lb_supAmou");
                if (drv["supply_equip_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["supply_equip_amonut"]);
                    lb_supAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localSUnit = (Label)e.Row.FindControl("lb_localSUnit");
                if (drv["local_exwork_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_exwork_unit_price"]);
                    lb_localSUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localSAmou = (Label)e.Row.FindControl("lb_localSAmou");
                if (drv["local_exwork_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_exwork_amonut"]);
                    lb_localSAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localTUnit = (Label)e.Row.FindControl("lb_localTUnit");
                if (drv["local_tran_unit_price"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_tran_unit_price"]);
                    lb_localTUnit.Text = String.Format("{0:0.00}", val);
                }
                Label lb_localTAmou = (Label)e.Row.FindControl("lb_localTAmou");
                if (drv["local_tran_amonut"].ToString() != "")
                {
                    double val = Convert.ToDouble(drv["local_tran_amonut"]);
                    lb_localTAmou.Text = String.Format("{0:0.00}", val);
                }
                Label lb_hide = (Label)e.Row.FindControl("lb_hide");
                lb_hide.Text = drv["rowid"].ToString();
                Label lb_jobno = (Label)e.Row.FindControl("lb_jobno");
                lb_jobno.Text = drv["job_no"].ToString();
                Label lb_scheno = (Label)e.Row.FindControl("lb_scheno");
                lb_scheno.Text = drv["schedule_no"].ToString();
                Label lb_bidno = (Label)e.Row.FindControl("lb_bidno");
                lb_bidno.Text = drv["bid_no"].ToString();
                Label lb_legend = (Label)e.Row.FindControl("lb_legend");
                if (drv["legend"].ToString() != "") lb_legend.Text = "(" + drv["legend"].ToString() + ")";
                Label lb_doc = (Label)e.Row.FindControl("lb_doc");
                //DataTable dtdoc = objJobDb.getListDoc(lb_codeNo.Text, drv["depart_position_name"].ToString(), drv["section_position_name"].ToString());
                //DataTable dtdoc = objJobDb.getListDoc("CVC3SWG03", "กวอ-ส.", "หวอ-ส.");
                //foreach (DataRow dr in dtdoc.Rows)
                //{
                //    if (lb_doc.Text == "") lb_doc.Text = "-" + dr["doc_name"].ToString();
                //    else lb_doc.Text += "<br/>-" + dr["doc_name"].ToString();
                //}
            }
        }
        private void LoadData()
        {

            DataTable dtData = objWf.GetDPDataByWorkId(hidWorkID.Value);
            if (dtData.Rows.Count > 0)
            {
                lblRequestorVal.Text = dtData.Rows[0]["emp_name"].ToString() + " (" + dtData.Rows[0]["emp_position"].ToString() + ")";

                if (zmode.ToLower() == "reviewer")
                {
                    lblToVal.Text = dtData.Rows[1]["emp_name"].ToString() + " (" + dtData.Rows[1]["emp_position"].ToString() + ")";
                }
                else if (zmode.ToLower() == "approver")
                {
                    lblToVal.Text = dtData.Rows[2]["emp_name"].ToString() + " (" + dtData.Rows[2]["emp_position"].ToString() + ")";
                }
                else if (zmode.ToLower() == "pm")
                {
                    lblToVal.Text = dtData.Rows[3]["emp_name"].ToString() + " (" + dtData.Rows[3]["emp_position"].ToString() + ")";
                }
                else if (zmode.ToLower() == "ad")
                {
                    lblToVal.Text = dtData.Rows[4]["emp_name"].ToString() + " (" + dtData.Rows[4]["emp_position"].ToString() + ")";
                }
                else if (zmode.ToLower() == "director")
                {
                    lblToVal.Text = dtData.Rows[5]["emp_name"].ToString() + " (" + dtData.Rows[5]["emp_position"].ToString() + ")";
                }
                else
                {
                    lblToVal.Text = dtData.Rows[6]["emp_name"].ToString() + " (" + dtData.Rows[6]["emp_position"].ToString() + ")";
                }
                lblSubjectVal.Text = dtData.Rows[0]["ps_subject"].ToString();
                hidProcessID.Value = dtData.Rows[0]["process_id"].ToString();
                hidActivityId.Value = dtData.Rows[0]["activity_id"].ToString();
                try { hidJobNo.Value = dtData.Rows[0]["job_no"].ToString(); } catch { }
                try { hidJobRev.Value = dtData.Rows[0]["job_revision"].ToString(); } catch { }
                try { hidBidNo.Value = dtData.Rows[0]["bid_no"].ToString(); } catch { }
                try { hidBidRev.Value = dtData.Rows[0]["bid_revision"].ToString(); } catch { }
                try { hidNodeID.Value = dtData.Rows[0]["doc_node_id"].ToString(); } catch { }
                lblPsReqNo.Text = dtData.Rows[0]["ps_reqno"].ToString();
                txtComment.Text = dtData.Rows[0]["note"].ToString();
                if (!String.IsNullOrEmpty(hidBidNo.Value)) lblHeader.Text = "CM - Bid approval workflow";
                
                wucComment_Att1.BindData(hidNodeID.Value, hidProcessID.Value, hidLogin.Value,false);
                hidApproverCount.Value = (dtData.Rows.Count - 1).ToString();

                // get Memo
                getMemo(hidProcessID.Value);
            }

        }
        private void getMemo(string strProcessId)
        {
            string xdoc_url = "";
            DataTable dt = objWf.GetPsRequestByProcessId(strProcessId);
            string strMergedPdfNodeId = dt.Rows[0]["doc_node_id"].ToString();
            xdoc_url = "https://" + zhost_name + "/tmps/forms/viewCSDoc?docnodeid=" + strMergedPdfNodeId+ "#toolbar=0";
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");
            ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);

        }

        protected void btnApprove_Click(object sender, EventArgs e)
        {
            try
            {
                var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                string sql = "";
                string sqlDetail = "";
                string xform_status = "";
                switch (hidMode.Value.ToUpper())
                {
                    case "REVIEWER"://approver1
                        {
                            xform_status = int.Parse(hidApproverCount.Value) > 1 ? "APPROVER1 APPROVED" : "FINISHED";
                            sql = " update wf_ps_request "
                            + " set "
                            + " form_status = '" + xform_status + "' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                            sqlDetail = "update wf_ps_request_detail "
                            + " set "
                            + " apv_status = 'APPROVED' "
                            + ", emp_apvdate =  GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' "
                            + " and apv_order = 2 ";
                        }; break;
                    case "APPROVER"://approver2
                        {
                            xform_status = int.Parse(hidApproverCount.Value) > 2 ? "APPROVER2 APPROVED" : "FINISHED";
                            sql = " update wf_ps_request "
                            + " set "
                            + " form_status = '" + xform_status + "' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                            sqlDetail = "update wf_ps_request_detail "
                            + " set "
                            + " apv_status = 'APPROVED' "
                            + ", emp_apvdate =  GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' "
                            + " and apv_order = 3 ";
                        }; break;
                    case "PM"://approver3
                        {
                            xform_status = int.Parse(hidApproverCount.Value) > 3 ? "APPROVER3 APPROVED" : "FINISHED";
                            sql = " update wf_ps_request "
                            + " set "
                            + " form_status = '" + xform_status + "' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                            sqlDetail = "update wf_ps_request_detail "
                            + " set "
                            + " apv_status = 'APPROVED' "
                            + ", emp_apvdate =  GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' "
                            + " and apv_order = 4 ";
                        }; break;
                    case "AD"://approver4
                        {
                            xform_status = int.Parse(hidApproverCount.Value) > 4 ? "APPROVER4 APPROVED" : "FINISHED";
                            sql = " update wf_ps_request "
                            + " set "
                            + " form_status = '" + xform_status + "' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                            sqlDetail = "update wf_ps_request_detail "
                            + " set "
                            + " apv_status = 'APPROVED' "
                            + ", emp_apvdate =  GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' "
                            + " and apv_order = 5 ";

                        }; break;
                    case "DIRECTOR"://approver5
                        {
                            xform_status = int.Parse(hidApproverCount.Value) > 5 ? "APPROVER5 APPROVED" : "FINISHED";
                            sql = " update wf_ps_request "
                            + " set "
                            + " form_status = '" + xform_status + "' "
                            + ", note = '" + txtComment.Text + "' "
                            + ", updated_by = '" + hidLogin.Value + "' "
                            + ", updated_datetime = GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' ";

                            sqlDetail = "update wf_ps_request_detail "
                            + " set "
                            + " apv_status = 'APPROVED' "
                            + ", emp_apvdate =  GETDATE() "
                            + " where process_id = '" + hidProcessID.Value + "' "
                            + " and apv_order = 6 ";


                        }; break;
                }

                zdbUtil.ExecNonQuery(sql, zpsdb);
                zdbUtil.ExecNonQuery(sqlDetail, zpsdb);

                // Next WORKFLOW Process
                ApplicationData[] appDatas;
                if (hidWorkID.Value == "")
                {
                    appDatas = !String.IsNullOrEmpty(hidJobNo.Value) ? zotFunctions.getWFStartAppData(strWfJobMapId.ToString()) : zotFunctions.getWFStartAppData(strWfBidMapId.ToString());
                }
                else
                {
                    appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
                }

                AttributeData wfAttr = new AttributeData();
                for (int i = 0; i < appDatas.Length; i++)
                {
                    try
                    {
                        wfAttr = (AttributeData)appDatas[i];
                        break;
                    }
                    catch (Exception ex)
                    {
                        string strError = ex.Message;
                    }
                }

                if (wfAttr != null)
                {
                    if (hidMode.Value.ToUpper() == "REVIEWER") // Review by Dept. Head //approver1
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_STATUS", "APPROVED");
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver1 Approved");
                        if (int.Parse(hidApproverCount.Value) > 1)
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER2");
                        else
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                        }
                    }
                    else if (hidMode.Value.ToUpper() == "APPROVER") // Review by Section Head//approver2
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE2_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE2_STATUS", "APPROVED");
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver2 Approved");
                        if (int.Parse(hidApproverCount.Value) > 2)
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER3");
                        else
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                        // 
                    }
                    else if (hidMode.Value.ToUpper() == "PM") // Review by Dept. Head//approver3
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE3_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE3_STATUS", "APPROVED");
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver3 Approved");
                        if (int.Parse(hidApproverCount.Value) > 3)
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER4");
                        else
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                    }
                    if (hidMode.Value.ToUpper() == "AD")//approver4
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE4_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE4_STATUS", "APPROVED");
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver4 Approved");
                        if (int.Parse(hidApproverCount.Value) > 4)
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER5");
                        else
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                    }
                    else if (hidMode.Value.ToUpper() == "DIRECTOR")//approver5
                    {
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE5_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE5_STATUS", "APPROVED");
                        zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver5 Approved");
                        zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");

                    } 


                    zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
                    MailNoti mailN = new MailNoti();

                    if (zotFunctions.getwfAttrValue(ref wfAttr, "NEXT_STEP") == "ENDED")
                    {
                        objWf.UpdateJobActivtyStatus("FINISHED", hidJobNo.Value, hidActivityId.Value, hidJobRev.Value);
                        if (!String.IsNullOrEmpty(hidBidNo.Value))
                        {
                            UpdateP6Bid(hidBidNo.Value, hidActivityId.Value);
                        }
                        else if (!String.IsNullOrEmpty(hidJobNo.Value))
                        {
                            UpdateP6Job(hidJobNo.Value, hidActivityId.Value);
                        }

                        // mailN.SendEmail("")
                    }

                    Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been approved.');</script>");
                    string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                    string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
                }

                //  Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                //  Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
        }
       
        private void saveComment()
        {
            if (txtComment.Text != "")
            {
                var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                string sql = "";
                sql = " insert into wf_comment_att (process_id, req_no, comment, uploaded_by, uploaded_datetime) " +
                    " values " +
                    " ( " +
                    " '" + hidProcessID.Value + "' , " +
                    " '" + lblPsReqNo.Text + "' , " +
                    " '" + txtComment.Text.Trim().Replace("'", "''") + "' , " +

                    " '" + emp.SNAME + " (" + emp.MC_SHORT + ")', " +
                     " GETDATE() " +
                    " ) ";
                zdbUtil.ExecNonQuery(sql, zpsdb);
                wucComment_Att1.BindData(hidNodeID.Value, hidProcessID.Value, hidLogin.Value, false);
                txtComment.Text = "";
            }
        }
        protected void btnReject_Click(object sender, EventArgs e)
        {

            try
            {
                if (txtComment.Text == "") {
                    Response.Write("<script>alert('Please enter Note for Reject');</script>");
                }
                else
                {
                    saveComment();
                    var emp = zempInfo.getInFoByEmpID(hidLogin.Value);
                    string sql = "";
                    string sqlDetail = "";
                    string xform_status = "";
                    switch (hidMode.Value.ToUpper())
                    {
                        case "EDIT"://approver1
                            {
                                xform_status = "APPROVER1 REJECTED";
                                sql = " update wf_ps_request "
                                + " set "
                                + " form_status = '" + xform_status + "' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                                sqlDetail = "update wf_ps_request_detail "
                                + " set "
                                + " apv_status = 'REJECTED' "
                                + ", emp_apvdate =  GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' "
                                + " and apv_oder = 2 ";
                            }; break;
                        case "REVIEWER"://approver2
                            {
                                xform_status = "APPROVER2 REJECTED";
                                sql = " update wf_ps_request "
                                + " set "
                                + " form_status = '" + xform_status + "' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                                sqlDetail = "update wf_ps_request_detail "
                                + " set "
                                + " apv_status = 'REJECTED' "
                                + ", emp_apvdate =  GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' "
                                + " and apv_oder = 3 ";
                            }; break;
                        case "APPROVER"://approver3
                            {
                                xform_status = "APPROVER3 REJECTED";
                                sql = " update wf_ps_request "
                                + " set "
                                + " form_status = '" + xform_status + "' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                                sqlDetail = "update wf_ps_request_detail "
                                + " set "
                                + " apv_status = 'REJECTED' "
                                + ", emp_apvdate =  GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' "
                                + " and apv_oder = 4 ";
                            }; break;
                        case "PM"://approver4
                            {
                                xform_status = "APPROVER4 REJECTED";
                                sql = " update wf_ps_request "
                                + " set "
                                + " form_status = '" + xform_status + "' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                                sqlDetail = "update wf_ps_request_detail "
                                + " set "
                                + " apv_status = 'REJECTED' "
                                + ", emp_apvdate =  GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' "
                                + " and apv_oder = 5 ";

                            }; break;
                        case "AD"://approver5
                            {
                                xform_status = "APPROVER5 REJECTED";
                                sql = " update wf_ps_request "
                                + " set "
                                + " form_status = '" + xform_status + "' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                                sqlDetail = "update wf_ps_request_detail "
                                + " set "
                                + " apv_status = 'REJECTED' "
                                + ", emp_apvdate =  GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' "
                                + " and apv_oder = 6 ";


                            }; break;
                        case "DIRECTOR"://approver6
                            {
                                xform_status = "APPROVER6 REJECTED";
                                sql = " update wf_ps_request "
                                + " set "
                                + " form_status = '" + xform_status + "' "
                                + ", note = '" + txtComment.Text + "' "
                                + ", updated_by = '" + hidLogin.Value + "' "
                                + ", updated_datetime = GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' ";

                                sqlDetail = "update wf_ps_request_detail "
                                + " set "
                                + " apv_status = 'REJECTED' "
                                + ", emp_apvdate =  GETDATE() "
                                + " where process_id = '" + hidProcessID.Value + "' "
                                + " and apv_oder = 7 ";
                            }; break;
                    }

                    zdbUtil.ExecNonQuery(sql, zpsdb);
                    zdbUtil.ExecNonQuery(sqlDetail, zpsdb);

                    // Next WORKFLOW Process
                    ApplicationData[] appDatas;
                    if (hidWorkID.Value == "")
                    {
                        // appDatas = zotFunctions.getWFStartAppData(strWfJobMapId.ToString());
                        appDatas = !String.IsNullOrEmpty(hidJobNo.Value) ? zotFunctions.getWFStartAppData(strWfJobMapId.ToString()) : zotFunctions.getWFStartAppData(strWfBidMapId.ToString());

                    }
                    else
                    {
                        appDatas = zotFunctions.getWFAppDatas(hidWorkID.Value, hidSubworkID.Value);
                    }

                    AttributeData wfAttr = new AttributeData();
                    for (int i = 0; i < appDatas.Length; i++)
                    {
                        try
                        {
                            wfAttr = (AttributeData)appDatas[i];
                            break;
                        }
                        catch (Exception ex)
                        {
                            string strError = ex.Message;
                        }
                    }

                    if (wfAttr != null)
                    {
                        //if (hidMode.Value.ToUpper() == "REVIEWER") // Review by Section Head
                        //{
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW__DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "REVIEW__STATUS", "Rejected");
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Reviewer Rejected");
                        //}
                        //else if (hidMode.Value.ToUpper() == "APPROVER") // Review by Dept. Head
                        //{
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE_STATUS", "Rejected");
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Dept Head Rejected");
                        //}
                        //if (hidMode.Value.ToUpper() == "PM")
                        //{
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "PM_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "PM_STATUS", "Rejected");
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "PM Rejected");
                        //}
                        //else if (hidMode.Value.ToUpper() == "AD")
                        //{
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "ASST_DIRECTOR_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "ASST_DIRECTOR_STATUS", "Rejected");
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "AD Rejected");
                        //}
                        //else if (hidMode.Value.ToUpper() == "DIRECTOR")
                        //{
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "DIRECTOR_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "DIRECTOR_STATUS", "Rejected");
                        //    zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Director Rejected");

                        //    //updateDirector_Approve();
                        //}
                        if (hidMode.Value.ToUpper() == "REVIEWER") // Review by Dept. Head //approver1
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE1_STATUS", "Rejected");
                            zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver1 Rejected");
                            if (int.Parse(hidApproverCount.Value) > 1)
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER2");
                            else
                            {
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                            }
                        }
                        else if (hidMode.Value.ToUpper() == "APPROVER") // Review by Section Head//approver2
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE2_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE2_STATUS", "Rejected");
                            zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver2 Rejected");
                            if (int.Parse(hidApproverCount.Value) > 2)
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER3");
                            else
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                            // 
                        }
                        else if (hidMode.Value.ToUpper() == "PM") // Review by Dept. Head//approver3
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE3_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE3_STATUS", "Rejected");
                            zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver3 Rejected");
                            if (int.Parse(hidApproverCount.Value) > 3)
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER4");
                            else
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                        }
                        if (hidMode.Value.ToUpper() == "AD")//approver4
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE4_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE4_STATUS", "Rejected");
                            zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver4 Rejected");
                            if (int.Parse(hidApproverCount.Value) > 4)
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "APPROVER5");
                            else
                                zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");
                        }
                        else if (hidMode.Value.ToUpper() == "DIRECTOR")//approver5
                        {
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE5_DATE", System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:sss"));
                            zotFunctions.setwfAttrValue(ref wfAttr, "APPROVE5_STATUS", "Rejected");
                            zotFunctions.setwfAttrValue(ref wfAttr, "WF_STATUS", "Approver5 Rejected");
                            zotFunctions.setwfAttrValue(ref wfAttr, "NEXT_STEP", "ENDED");

                        }
                        zotFunctions.completeWorkItem(hidLogin.Value, hidWorkID.Value, hidSubworkID.Value, hidTaskID.Value, appDatas);
                        if (hidMode.Value.ToUpper() == "DIRECTOR") // 
                        {
                            // EMAIL

                        }
                        else
                        {
                            //MailNoti mail = new MailNoti();
                        }

                        Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been rejected.');</script>");

                        string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
                        string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
                    }

                    Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
                }
                
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
                Page.ClientScript.RegisterStartupScript(this.GetType(), "EnableButton", "EnableButton()", true);
            }
        }

        private void UpdateP6Bid(string strBidNo,string strActivityId)
        {
            // ------------------- update P6 -------------------
            try
            {
                p6Class.AuthP6();
                p6Class p6 = new p6Class();
                DataTable dt = p6.getTaskBid_ByActivityECM(strBidNo, strActivityId);
                if (dt.Rows.Count > 0)
                {
                    string strTaskId = dt.Rows[0]["task_id"].ToString();

                    DateTime actualStartDate = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    p6Class.UpdateActualStartDate(int.Parse(strTaskId), actualStartDate);
                    p6Class.UpdatetaskStatus(int.Parse(strTaskId), "Completed");
                    p6Class.ActionUDFStaffLastupdate(int.Parse(strTaskId), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    string strPrefixMainAc = strActivityId.Substring(0, 1) ;
                    string staMainActivity = strActivityId.Substring(0, 1) + "0000";
                    DataTable dtMain = p6.getTaskBid_ByActivityECM(strBidNo, staMainActivity);
                    if (dtMain.Rows.Count > 0)
                    {
                        string strTaskIdMain = dtMain.Rows[0]["task_id"].ToString();
                        string strSql = @"select ((weight_factor*100) / (select sum(weight_factor)  
                                                     from ps_m_activity_job
                                                     where activity_id like '" + strPrefixMainAc + @"%')) / 100 as percent_complete
                                                from ps_m_activity_job
                                                where activity_id = '"+ strActivityId + "' and is_active = 1";
                        DataTable dtPercentage  = zdbUtil.ExecSql_DataTable(strSql, zpsdb);
                        if (dtPercentage.Rows.Count > 0)
                        {
                            p6Class.UpdatepercentActivity(int.Parse(strTaskIdMain), double.Parse(dtPercentage.Rows[0]["percent_complete"].ToString()));
                            p6Class.UpdatetaskStatus(int.Parse(strTaskIdMain), "InProgress");
                            p6Class.ActionUDFStaffLastupdate(int.Parse(strTaskIdMain), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                    }
                }
                p6Class.Logout();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            // ------------------- update P6 -------------------
        }
        private void UpdateP6Job(string strJobNo, string strActivityId)
        {
            // ------------------- update P6 -------------------
            try
            {
                p6Class.AuthP6();
                p6Class p6 = new p6Class();
                DataTable dt = p6.getTaskJob_ByActivityECM(strJobNo, strActivityId);
                if (dt.Rows.Count > 0)
                {
                    string strTaskId = dt.Rows[0]["task_id"].ToString();

                    DateTime actualStartDate = DateTime.ParseExact(DateTime.Now.ToString("dd/MM/yyyy"), "dd/MM/yyyy", CultureInfo.InvariantCulture);

                    p6Class.UpdateActualStartDate(int.Parse(strTaskId), actualStartDate);
                    p6Class.UpdatetaskStatus(int.Parse(strTaskId), "Completed");
                    p6Class.ActionUDFStaffLastupdate(int.Parse(strTaskId), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));


                    string strPrefixMainAc = strActivityId.Substring(0, 1);
                    string staMainActivity = strActivityId.Substring(0, 1) + "0000";
                    DataTable dtMain = p6.getTaskJob_ByActivityECM(strJobNo, staMainActivity);
                    if (dtMain.Rows.Count > 0)
                    {
                        string strTaskIdMain = dtMain.Rows[0]["task_id"].ToString();
                        string strSql = @"select ((weight_factor*100) / (select sum(weight_factor)  
                                                     from ps_m_activity_job
                                                     where activity_id like '" + strPrefixMainAc + @"%')) / 100 as percent_complete
                                                from ps_m_activity_job
                                                where activity_id = '" + strActivityId + "' and is_active = 1"; 
                        DataTable dtPercentage = zdbUtil.ExecSql_DataTable(strSql, zpsdb);
                        if (dtPercentage.Rows.Count > 0)
                        {
                            p6Class.UpdatepercentActivity(int.Parse(strTaskIdMain), double.Parse(dtPercentage.Rows[0]["percent_complete"].ToString()));
                            p6Class.UpdatetaskStatus(int.Parse(strTaskIdMain), "InProgress");
                            p6Class.ActionUDFStaffLastupdate(int.Parse(strTaskIdMain), System.DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        }
                    }
                }
                p6Class.Logout();
            }
            catch (Exception ex)
            {
                LogHelper.WriteEx(ex);
            }

            // ------------------- update P6 -------------------
        }


        private void TestStartWf()
        {
            //--------------Test Start WF---------------//
            List<clsJobWf> lstWF = new List<clsJobWf>();
            clsWfPsAPV01 objWfPs = new clsWfPsAPV01();
            clsJobWf objJobWf = new clsJobWf();
            objJobWf.strJobNo = "TS12-11-S06";
            objJobWf.iJobRevision = 3;
            objJobWf.strActivityId = "10040";
            lstWF.Add(objJobWf);
            //string strError = objWfPs.StartWf(lstWF);
            //--------------Test Start WF---------------//
        }

        private void TestStartWfBid()
        {
            //--------------Test Start WF---------------//
            List<clsJobWf> lstWF = new List<clsJobWf>();
            clsWfPsAPV01 objWfPs = new clsWfPsAPV01();
            
            clsJobWf objJobWf = new clsJobWf();
            objJobWf.strBidNo = "TS12-TX-02";
            objJobWf.iBidRevision = 0;
            objJobWf.strActivityIdBid = "90020";
            lstWF.Add(objJobWf);
            //string strError = objWfPs.StartWfBid(lstWF);
            //--------------Test Start WF---------------//
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        //private void loadDP_Data()
        //{
        //    DataTable dt = objWf.GetDPDataByWorkId(hidWorkID.Value);
        //    if (dt.Rows.Count > 0)
        //    {
        //        DataRow dr = dt.Rows[0];
        //        lblRequestorVal.Text = dr["requestor_name"].ToString() + " (" + dr["requestor_position"].ToString() + ")";
        //        if (zmode.ToLower() == "edit")
        //        {
        //            lblToVal.Text = dr["requestor_name"].ToString() + " (" + dr["requestor_position"].ToString() + ")";
        //        }
        //        else if (zmode.ToLower() == "reviewer")
        //        {
        //            lblToVal.Text = dr["reviewer_name"].ToString() + " (" + dr["reviewer_position"].ToString() + ")";
        //        }
        //        else if (zmode.ToLower() == "approver")
        //        {
        //            lblToVal.Text = dr["approver_name"].ToString() + " (" + dr["approver_position"].ToString() + ")";
        //        }
        //        else if (zmode.ToLower() == "pm")
        //        {
        //            lblToVal.Text = dr["pm_name"].ToString() + " (" + dr["pm_position"].ToString() + ")";
        //        }
        //        else if (zmode.ToLower() == "ad")
        //        {
        //            lblToVal.Text = dr["assistdirector_name"].ToString() + " (" + dr["assistdirector_position"].ToString() + ")";
        //        }
        //        else
        //        {
        //            lblToVal.Text = dr["director_name"].ToString() + " (" + dr["director_position"].ToString() + ")";
        //        }
        //        lblSubjectVal.Text = dr["ps_subject"].ToString();
        //        hidProcessID.Value = dr["process_id"].ToString();
        //        lblPsReqNo.Text = dr["ps_reqno"].ToString();
        //        txtComment.Text = dr["note"].ToString();
        //        // get Memo
        //        getMemo(hidProcessID.Value);
        //    }
        //}
        //private void loadDataFromWFAttribute()
        //{
        //    var wfAttr = zotFunctions.getWFAttributes(hidWorkID.Value, hidSubworkID.Value);
        //    hidPSDPNo.Value = zotFunctions.getwfAttrValue(ref wfAttr, "ps_reqno");
        //    lblTitle.Text = zotFunctions.getwfAttrValue(ref wfAttr, "ACTIVITY_NAME");
        //    //hidRequestorId.Value = zotFunctions.getwfAttrValue(ref wfAttr, "ASSIGN_TO");
        //    //hidSectionApproverId.Value = zotFunctions.getwfAttrValue(ref wfAttr, "REVIEW_BY");
        //    //hidProjectMgrId.Value = zotFunctions.getwfAttrValue(ref wfAttr, "APPROVE_BY");
        //    //hidPMId.Value = zotFunctions.getwfAttrValue(ref wfAttr, "PM_ID");
        //    //hidADId.Value = zotFunctions.getwfAttrValue(ref wfAttr, "ASST_DIRECTOR_ID");
        //    //hidDirectorId.Value = zotFunctions.getwfAttrValue(ref wfAttr, "DIRECTOR_ID");
        //}
        //private void loadDataFromWFDB()
        //{
        //    DataTable dtData = objWf.GetDPDataByWorkId(hidWorkID.Value);
        //    if (dtData.Rows.Count > 0)
        //    {
        //        hidPSDPNo.Value = dtData.Rows[0]["ps_reqno"].ToString();
        //        lblTitle.Text = dtData.Rows[0]["ps_subject"].ToString();
        //        hidRequestorId.Value = dtData.Rows[0]["requestor_id"].ToString();
        //        //hidSectionApproverId.Value = dtData.Rows[0]["reviewer_id"].ToString();
        //        //hidProjectMgrId.Value = dtData.Rows[0]["approver_id"].ToString();
        //        //hidPMId.Value = dtData.Rows[0]["pm_id"].ToString();
        //        //hidADId.Value = dtData.Rows[0]["assistdirector_id"].ToString();
        //        //hidDirectorId.Value = dtData.Rows[0]["director_id"].ToString();
        //        //hidApproverCount.Value = objWf.GetJobApproverRoutingCount(dtData.Rows[0]["job_no"].ToString(), dtData.Rows[0]["job_revision"].ToString(), dtData.Rows[0]["activity_id"].ToString()).ToString();
        //    }
        //}

    }
}