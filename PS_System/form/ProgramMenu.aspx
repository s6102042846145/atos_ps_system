﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ProgramMenu.aspx.cs" Inherits="PS_System.form.ProgramMenu" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <!-- Bootstrap CSS CDN -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    <!-- Our Custom CSS -->
    <link href="../Vendor/css/style.css" rel="stylesheet" />

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js" integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js" integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous"></script>
    
<script src="../Vendor/js/jquery/jquery.min.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div class="justify-content-center wrapper">
        <!-- Sidebar  -->
        <nav id="sidebar">
            
            <ul class="list-unstyled components">
                <li class="active">
                    <a href="#1Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-align-justify"></i> Menu Group Name</a>
                    <ul class="collapse list-unstyled" id="1Submenu">
                        <li>
                            <a href="#">Program name</a>
                        </li>
                        <li>
                            <a href="#">Program name</a>
                        </li>
                        <li>
                            <a href="#">Program name</a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="#2Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-align-justify"></i> Menu Group Name</a>
                    <ul class="collapse list-unstyled" id="2Submenu">
                         <li>
                            <a href="#">Program name</a>
                        </li>
                        <li>
                            <a href="#">Program name</a>
                        </li>
                        <li>
                            <a href="#">Program name</a>
                        </li>
                    </ul>
                </li>     
                <li>
                    <a href="#3Submenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle"><i class="fas fa-align-justify"></i> Menu Group Name</a>
                    <ul class="collapse list-unstyled" id="3Submenu">
                         <li>
                            <a href="#">Program name</a>
                        </li>
                        <li>
                            <a href="#">Program name</a>
                        </li>
                        <li>
                            <a href="#">Program name</a>
                        </li>
                    </ul>
                </li>   
            </ul>
        </nav>
    </div>

   
    
    </form>
     <!-- jQuery CDN - Slim version (=without AJAX) -->
    <script src="../Vendor/js/jquery/jquery-3.3.1.slim.min.js"></script>
    <!-- Popper.JS -->
    <script src="../Vendor/js/jquery/popper.min.js"></script>
    <!-- Bootstrap JS -->
    <script src="../Vendor/js/jquery/bootstrap.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#sidebarCollapse').on('click', function () {
                $('#sidebar').toggleClass('active');
            });
        });
    </script>
</body>
</html>
