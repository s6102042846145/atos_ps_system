﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DdsrEdit.aspx.cs" Inherits="PS_System.form.DDSR.DdsrEdit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>DDSR Report</title>
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.js"></script>
    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <link href="../../Content/bootstrap.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
    <style>
        .table table tbody tr td a,
        .table table tbody tr td span {
            position: relative;
            float: left;
            padding: 6px 12px;
            margin-left: -1px;
            line-height: 1.42857143;
            color: #337ab7;
            text-decoration: none;
            background-color: #fff;
            border: 1px solid #ddd;
        }

        .table table > tbody > tr > td > span {
            z-index: 3;
            color: #fff;
            cursor: default;
            background-color: #337ab7;
            border-color: #337ab7;
        }

        .table table > tbody > tr > td:first-child > a,
        .table table > tbody > tr > td:first-child > span {
            margin-left: 0;
            border-top-left-radius: 4px;
            border-bottom-left-radius: 4px;
        }

        .table > tbody > tr > td {
            padding: 5px;
            line-height: 1.42857143;
            vertical-align: middle;
        }

        .table table > tbody > tr > td:last-child > a,
        .table table > tbody > tr > td:last-child > span {
            border-top-right-radius: 4px;
            border-bottom-right-radius: 4px;
        }

        .table table > tbody > tr > td > a:hover,
        .table table > tbody > tr > td > span:hover,
        .table table > tbody > tr > td > a:focus,
        .table table > tbody > tr > td > span:focus {
            z-index: 2;
            color: #23527c;
            background-color: #eee;
            border-color: #ddd;
        }

        .mydropdownlist {
            color: black;
            font-size: 11px;
            font-family: Tahoma;
            padding: 2px 5px;
            border-radius: 2px;
            background-color: white;
            font-weight: normal;
        }

        th {
            text-align: center;
        }

        .btn {
            display: inline-block;
            margin-bottom: 0;
            font-weight: normal;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            background-image: none;
            border: 1px solid transparent;
            padding: 6px 12px;
            font-size: 11px;
            line-height: 1.42857143;
            border-radius: 4px;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }

        .auto-style1 {
            width: 100px;
            height: 19px;
        }

        .auto-style2 {
            height: 19px;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(function () {
            $('[id*=lstEquipGroup]').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                disableIfEmpty: true,
                maxHeight: 500,
                length: 500,
                numberDisplayed: 1
            });
        });

        function showPleaseWait() {
            if (document.querySelector("#pleaseWaitDialog") == null) {
                var modalLoading = '<div class="modal" id="pleaseWaitDialog" data-backdrop="static" data-keyboard="false" role="dialog">\
                                        <div class="modal-dialog">\
                                            <div class="modal-content">\
                                                <div class="modal-header">\
                                                    <h4 class="modal-title">Please wait...</h4>\
                                                </div>\
                                                <div class="modal-body">\
                                                    <div class="progress">\
                                                      <div class="progress-bar progress-bar-success progress-bar-striped active" role="progressbar"\
                                                      aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width:100%; height: 40px">\
                                                      </div>\
                                                    </div>\
                                                </div>\
                                            </div>\
                                        </div>\
                                    </div>';
                $(document.body).append(modalLoading);
            }

            $("#pleaseWaitDialog").modal("show");
        }


        /**
         * Hides "Please wait" overlay. See function showPleaseWait().
         */
        function hidePleaseWait() {
            $("#pleaseWaitDialog").modal("hide");
        }
    </script>
</head>
<body>
    <div style="padding-left: 10px; padding-right: 10px; padding-top: 10px; padding-bottom: 10px; font-family: Tahoma; font-size: 11px">
        <form id="form1" runat="server">
            <div>
                <table style="width: 90%;">
                    <tr>
                        <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                            <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                            &nbsp;- CM (Contract Management)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="width: 100%;">
                            <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - DDSR-Edit"></asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
            <div>
                <table style="font-family: Tahoma; width: 100%; padding: 2px 0px 2px 0px;">
                    <tr>
                        <td style="width: 100px; padding: 2px 0px 2px 0px;">
                            <b>Bid No. : </b>
                        </td>
                        <td colspan="4" style="padding: 2px 0px 2px 0px;">
                            <asp:Label ID="lblBidNo" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 2px 0px 2px 0px;" class="auto-style1">
                            <b>Bid Desc. : </b>
                        </td>
                        <td colspan="4" style="padding: 2px 0px 2px 0px;" class="auto-style2">
                            <asp:Label ID="lblBidDesc" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px; padding: 2px 0px 2px 0px;">
                            <b>Schedule Name : </b>
                        </td>
                        <td colspan="4" style="padding: 2px 0px 2px 0px;">
                            <asp:Label ID="lblSchName" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style="padding: 2px 0px 2px 0px; font-size: larger">
                            <b>Design drawing list</b>
                        </td>
                    </tr>
                </table>
            </div>
            <div style="width: 90%">
                <asp:GridView ID="gvMasterDrawing" runat="server" AutoGenerateColumns="False" CssClass="table table-striped table-bordered table-hover" Width="100%"
                    Visible="true" ClientIDMode="Static" Font-Size="11px" PageSize="10"  OnRowDeleting="OnRowGvMasterDrawingDeleting"
                    AllowPaging="true" OnPageIndexChanging="gvMasterDrawing_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                            <ItemStyle Width="3%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Drawing No.">
                            <ItemTemplate>
                                <asp:Label ID="lblDrwNo" runat="server" Text='<%# Bind("drawing_no") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="15%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Drawing Name">
                            <ItemTemplate>
                                <asp:Label ID="lblDrwName" runat="server" Text='<%# Bind("drawing_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="20%" HorizontalAlign="Left" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ref. Job">
                            <ItemTemplate>
                                <asp:Label ID="lblRefJob" runat="server" Text='<%# Bind("ref_job") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="8%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Ref. Part">
                            <ItemTemplate>
                                <asp:Label ID="lblRefPart" runat="server" Text='<%# Bind("ref_part") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Discipline">
                            <ItemTemplate>
                                <asp:Label ID="lblDeptName" runat="server" Text='<%# Bind("depart_position_name") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delete">
                            <ItemTemplate>
                                <asp:ImageButton ID="imgRemove" runat="server" ImageUrl="~/images/bin.png" Height="16px" Width="16px" CommandName="Delete" />
                            </ItemTemplate>
                            <ItemStyle Width="5%" HorizontalAlign="Center" VerticalAlign="Middle" />
                            <HeaderStyle BackColor="#3399ff" HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="divAddValue" style="width: 90%; border: 2px solid black;">
                <table>
                    <tr>
                        <td colspan="2" style="font-size: larger">
                            <b>Add Design drawing list</b>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <b>Drawing No.</b><a style="color:red">*</a>
                        </td>
                        <td style="width: 800px">
                            <asp:TextBox ID="tbxDrawingNo" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <b>Drawing Name.</b><a style="color:red">*</a>
                        </td>
                        <td>
                            <asp:TextBox ID="tbxDrawingName" runat="server"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <b>Ref. Job</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlJob" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 100px">
                            <b>Ref. Part</b>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlPart" runat="server"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <asp:Button ID="btnAdd" CssClass="btn btn-primary" runat="server" Text="Add" OnClick="btnAdd_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <div id="divExport" runat="server" style="width: 90%">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnExport" CssClass="btn btn-primary" runat="server" Text="Save & Export" OnClick="btnExport_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </form>
    </div>
</body>
</html>
