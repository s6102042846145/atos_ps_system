﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
 
namespace PS_System.form.DDSR
{
    public partial class MasterDrawingList : System.Web.UI.Page
    {
        private string strEmpIdLogin = string.Empty;
        clsDdsr objDdsr = new clsDdsr(); 

        protected void Page_Load(object sender, EventArgs e)
        {
            // get login from querystring
            if (Request.QueryString["zuserlogin"] != null)
                strEmpIdLogin = Request.QueryString["zuserlogin"];
            else
                strEmpIdLogin = "z593503";// for test only
             
            if (!IsPostBack)
            {
                BindingBidNo(); 
                BindingJobNo();
                BindingDepartment();
            }
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            BindingGv();
        }  

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string strFileName = "Export_DDSR_RAW_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // export grid view to excel
            DataTable dtResult = (DataTable)ViewState["dt_drw_list"];

            byte[] byte1 = objDdsr.GenExcel(dtResult);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "binary/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
            Response.BinaryWrite(byte1);
            Response.Flush();
            Response.End();
        }

        protected void btnExportDdsr_Click(object sender, EventArgs e)
        {
            Session["ss_bid_no"] = ddlBidNo.SelectedValue;
            Session["ss_sch_name"] = hdnSch.Value;
            Session["ss_dt_drw_list"] = ViewState["dt_drw_list"];
            Response.Redirect("DdsrEdit.aspx");


            //string strFileName = "Export_DDSR_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            //// export grid view to excel
            //DataTable dtResult = (DataTable)ViewState["dt_drw_list"];

            //byte[] byte1 = objDdsr.GenExcelDdSr(dtResult);
            //Response.Clear();
            //Response.Buffer = true;
            //Response.Charset = "";
            //Response.Cache.SetCacheability(HttpCacheability.NoCache);
            //Response.ContentType = "binary/octet-stream";
            //Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
            //Response.BinaryWrite(byte1);
            //Response.Flush();
            //Response.End();
        }
         
        protected void gvMasterDrawing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMasterDrawing.PageIndex = e.NewPageIndex;
            this.BindingGv(); 
        } 

        private void BindingGv()
        {
            DataTable dt = objDdsr.GetDrawingListByBidNo(ddlBidNo.SelectedValue,ddlJobNo.SelectedValue,ddlDept.SelectedValue) ;//new DataTable();// get 
            ViewState["dt_drw_list"] = dt;
            gvMasterDrawing.DataSource = dt;
            gvMasterDrawing.DataBind();
            if (dt.Rows.Count > 0)
            { 
                hdnSch.Value = dt.Rows[0]["ref_sch"].ToString();
                divExport.Visible = true;
            }
            else
            {
                divExport.Visible = false;
            }
        }

        private void BindingBidNo()
        {
            ddlBidNo.Items.Clear();
            DataTable dtBidNo = objDdsr.GetBidNo();
            foreach(DataRow dr in dtBidNo.Rows)
            {
                ddlBidNo.Items.Add(new ListItem(dr["bid_no"].ToString()));
            }
            divExport.Visible = false;
        }

        private void BindingJobNo()
        {
            ddlJobNo.Items.Clear();
            DataTable dtJobNo = objDdsr.GetJobNo();
            foreach (DataRow dr in dtJobNo.Rows)
            {
                ddlJobNo.Items.Add(new ListItem(dr["job_no"].ToString()));
            }
            divExport.Visible = false;
        }

        private void BindingDepartment()
        {
            ddlDept.Items.Clear();
            DataTable dtDept = objDdsr.GetDepartment();
            foreach (DataRow dr in dtDept.Rows)
            {
                ddlDept.Items.Add(new ListItem(dr["depart_position_name"].ToString()));
            }
            divExport.Visible = false;
        }
    }
}