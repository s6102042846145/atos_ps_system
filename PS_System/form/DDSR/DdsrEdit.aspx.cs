﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.DDSR
{
    public partial class DdsrEdit : System.Web.UI.Page
    {
        private string strEmpIdLogin = string.Empty;
        clsDdsr objDdsr = new clsDdsr();

        protected void Page_Load(object sender, EventArgs e)
        {

            if (Request.QueryString["zuserlogin"] != null)
                strEmpIdLogin = Request.QueryString["zuserlogin"];
            else
                strEmpIdLogin = "z593503";// for test only
            if (!IsPostBack)
            {
                BindingMainData();
            }
        }

        private void BindingMainData()
        {
            lblBidNo.Text = Session["ss_bid_no"].ToString();
            lblSchName.Text = Session["ss_sch_name"].ToString();
            DataTable dtDrwingList = (DataTable)Session["ss_dt_drw_list"];
            DataRow[] drListGv = dtDrwingList.Select("bid_no = '" + lblBidNo.Text + "' AND ref_sch = '" + lblSchName.Text + "' ");
            if (drListGv.Length > 0)
            {
                lblBidDesc.Text = drListGv[0]["bid_desc"].ToString();
            }
            DataTable dtBind = new DataTable();
            DataColumn dc = new DataColumn("bid_no");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("bid_desc");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("depart_position_name");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("sub_line_name");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("doc_nodeid");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("type");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("drawing_no");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("drawing_name");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("revision");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("ref_job");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("ref_sch");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("ref_part");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("to_contractor");
            dtBind.Columns.Add(dc);
            dc = new DataColumn("status");
            dtBind.Columns.Add(dc);

            foreach (DataRow row in drListGv)
            {
                dtBind.ImportRow(row);
            }
            ViewState["dt_edit_Ddsr"] = dtBind;
            BindDdlAdd(dtBind);
            BindingGv();


        }


        private void BindDdlAdd(DataTable dtBindGv)
        {
            DataView view = new DataView(dtBindGv);
            DataTable dtDistinctJob = view.ToTable(true, "ref_job");
            DataTable dtDistinctPart = view.ToTable(true, "ref_part");
            ddlJob.Items.Clear();
            ddlPart.Items.Clear();
            foreach (DataRow drJob in dtDistinctJob.Rows)
            {
                ddlJob.Items.Add(new ListItem(drJob["ref_job"].ToString()));
            }
            foreach (DataRow drPart in dtDistinctPart.Rows)
            {
                ddlPart.Items.Add(new ListItem(drPart["ref_part"].ToString()));
            }


        }
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            string strErrMsg = string.Empty;
            bool isValidate = true;
            if (String.IsNullOrEmpty(tbxDrawingName.Text.Trim()))
            {
                strErrMsg = "Please input Drawing Name";
                isValidate = false;
            }
            else if (String.IsNullOrEmpty(tbxDrawingNo.Text.Trim()))
            {
                strErrMsg = "Please input Drawing No.";
                isValidate = false;
            }
            if (isValidate)
            {
                // add to gv
                // add to dt 
                DataTable dtBind = ViewState["dt_edit_Ddsr"] as DataTable;
                DataRow drNew = dtBind.NewRow();
                drNew["bid_no"] = dtBind.Rows[0]["bid_no"].ToString();
                drNew["bid_desc"] = dtBind.Rows[0]["bid_desc"].ToString();
                drNew["depart_position_name"] = dtBind.Rows[0]["depart_position_name"].ToString();
                drNew["sub_line_name"] = dtBind.Rows[0]["sub_line_name"].ToString();
               // drNew["doc_nodeid"] = dtBind.Rows[0]["doc_nodeid"].ToString();
                drNew["type"] = dtBind.Rows[0]["type"].ToString();
                drNew["revision"] = dtBind.Rows[0]["revision"].ToString();
                drNew["ref_sch"] = dtBind.Rows[0]["ref_sch"].ToString();
                drNew["drawing_no"] = tbxDrawingNo.Text;
                drNew["drawing_name"] = tbxDrawingName.Text;
                drNew["ref_job"] = ddlJob.SelectedValue;
                drNew["ref_part"] = ddlPart.SelectedValue;
                dtBind.Rows.Add(drNew);

                ViewState["dt_edit_Ddsr"] = dtBind;
                tbxDrawingNo.Text = string.Empty;
                tbxDrawingName.Text = string.Empty;
                BindDdlAdd(dtBind);
                BindingGv();
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('" + strErrMsg + "');", true);

            }
        }

        protected void btnExport_Click(object sender, EventArgs e)
        {
            string strFileName = "Export_DDSR_" + DateTime.Now.ToString("yyyyMMdd_HHmmssfff");
            // export grid view to excel
            DataTable dtResult = (DataTable)ViewState["dt_edit_Ddsr"];

            byte[] byte1 = objDdsr.GenExcelDdsrQc(dtResult);
            Response.Clear();
            Response.Buffer = true;
            Response.Charset = "";
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.ContentType = "binary/octet-stream";
            Response.AppendHeader("Content-Disposition", "attachment; filename=" + strFileName + ".xlsx");
            Response.BinaryWrite(byte1);
            Response.Flush();
            Response.End();
        }

        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

        protected void gvMasterDrawing_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMasterDrawing.PageIndex = e.NewPageIndex;
            BindingGv();
        }

        private void BindingGv()
        {
            DataTable dt = new DataTable();//objDdsr.GetDrawingListByBidNo(ddlBidNo.SelectedValue);//new DataTable();// get 
            dt = ViewState["dt_edit_Ddsr"] as DataTable;
            gvMasterDrawing.DataSource = dt;
            gvMasterDrawing.DataBind();
            if (dt.Rows.Count > 0)
            {
                divExport.Visible = true;
            }
            else
            {
                divExport.Visible = false;
            }
        }

        protected void OnRowGvMasterDrawingDeleting(object sender, GridViewDeleteEventArgs e)
        {
            int index = Convert.ToInt32(e.RowIndex);
            DataTable dt = ViewState["dt_edit_Ddsr"] as DataTable;
            dt.Rows[index].Delete();
            dt.AcceptChanges();
            ViewState["dtSelect"] = dt;
            gvMasterDrawing.DataSource = dt;
            gvMasterDrawing.DataBind();

        }
    }
}