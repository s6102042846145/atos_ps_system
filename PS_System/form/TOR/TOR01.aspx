﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TOR01.aspx.cs" Inherits="PS_System.form.TOR.TOR01"  MaintainScrollPositionOnPostback="true"%>

<%@ Register Src="~/UserControl/wucComment_Att_TOR.ascx" TagPrefix="uc2" TagName="wucComment_Att" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 1000px;
        }

        .auto-style5 {
            width: 58px;
        }
       .picker{

            vertical-align:middle;
        }
          .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }
         .docview {
            height: 650px;
            width: 100%;
        }
    </style>
      
 <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
      <script type="text/javascript">

          $(function () {
              $(".picker").datepicker();
          });


          $(document).ready(function () {
              maintainScrollPosition();
              maintainScrollPosition2();
          });

          function pageLoad() {
              maintainScrollPosition();
          }

          $(document).keypress(
              function (event) {
                  if (event.which == '13') {
                      event.preventDefault();
                  }
              });

          function setScrollPosition(scrollValue) {
              $('#<%=hfScrollPosition.ClientID%>').val(scrollValue);
        }

        function maintainScrollPosition() {
            $("#bigDiv").scrollTop($('#<%=hfScrollPosition.ClientID%>').val());
          }

          function setScrollPosition2(scrollValue) {
              $('#<%=hfScrollPosition2.ClientID%>').val(scrollValue);
        }

        function maintainScrollPosition2() {
            $("#bigDiv2").scrollTop($('#<%=hfScrollPosition2.ClientID%>').val());
        }
     
      </script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Bid Docuemnt Review"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding:5px 5px 5px 5px;">
            <div id="dvMain" runat="server">
            <table cellpadding="0" cellspacing="0" style="width:98%">
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td>
                        <asp:Label ID="Label5" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Bid No."></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Label ID="lbBidno" runat="server" Font-Names="Tahoma" Font-Size="10pt" ></asp:Label>
                       <%-- <asp:DropDownList ID="ddlBid" runat="server" Width="200px"> </asp:DropDownList>    OnSelectedIndexChanged="ddlTOR_SelectedIndexChanged" --%>
                         &nbsp;&nbsp; &nbsp;&nbsp;
                        <asp:Label ID="Label2" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="TOR Template"></asp:Label>
                        &nbsp;&nbsp;
                        <asp:DropDownList ID="ddlTOR" runat="server" AutoPostBack="true" Width="200px"> </asp:DropDownList>
                         &nbsp;&nbsp;
                        <asp:Button ID="btnSelectTemp" runat="server" Text="Select Tempalte"   CssClass="btn btn-primary"   Font-Names="tahoma" width="150px" OnClientClick="if(!confirm('Do you want to change templates, Will delete all of your old templates?')) return false;"  OnClick="btnSelectTemp_Click"/>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td>
                        <asp:Label ID="Label1" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="TOR - Document Check List and Upload"></asp:Label>
                    </td>
                </tr>
                <tr>
                     <td class="auto-style5">&nbsp;</td>
                     <td >
                         <asp:Button ID="btnUploadAll" runat="server" Text="Upload Document"   CssClass="btn btn-primary"   Font-Names="tahoma" width="150px" OnClick="btnUploadAll_Click" />
                         <asp:Button ID="btnMerge" runat="server" Text="Merge Document"   CssClass="btn btn-primary"   Font-Names="tahoma" width="150px" OnClick="btnMerge_Click"/>
                         <asp:Button ID="btnPreview" runat="server" Text="Preview Document"   CssClass="btn btn-primary"   Font-Names="tahoma" width="150px" OnClick="btnPreview_Click" Visible="false"/>
                    </td>
                </tr>

               <%-- <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td>
                        <asp:Panel ID="Panel1" runat="server" BackColor="WhiteSmoke">
                            &nbsp;
                        </asp:Panel>
                    </td>
                </tr>--%>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td>
                        <br />
                         <asp:HiddenField ID="hfScrollPosition" Value="0" runat="server" />
                        <div id="bigDiv" runat="server" style="height:700px;max-height:650px;overflow-y:auto;width:100%" onscroll="setScrollPosition(this.scrollTop);">
                         <asp:GridView ID="gvTemplate" runat="server" AutoGenerateColumns="False" Width="100%"  CellPadding="4" ForeColor="#333333" Visible="true" EmptyDataText="No data found." OnRowCommand="gvTemplate_RowCommand">
                              <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Vol.">
                                    <ItemTemplate>
                                        <asp:Label ID="lbTemplateCode" runat="server" Text='<%# Bind("tor_temp_code") %>' Visible="false"></asp:Label>
                                           <asp:Label ID="lbDocCode" runat="server" Text='<%# Bind("doctype_code") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lbDoc_Name" runat="server" Text='<%# Bind("doctype_name") %>'  Visible="false"></asp:Label>
                                          <asp:Label ID="lbDoc_Torname" runat="server" Text='<%# Bind("tor_temp_name") %>' Visible="false"></asp:Label>
                                     <%--    <asp:Label ID="lbJobno" runat="server" Text='<%# Bind("job_no") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lbJobrev" runat="server" Text='<%# Bind("job_revision") %>'  Visible="false"></asp:Label>--%>
                                          <asp:Label ID="lbVol" runat="server" Text='<%# Bind("vol_no") %>' ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Section Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbSection" runat="server" Text='<%# Bind("section_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Aticle Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbArticle" runat="server" Text='<%# Bind("article_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="20%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Document">
                                    <ItemTemplate>
                                      
                                          <asp:ImageButton ID="ibtnAssigned" CommandName="viewdoc" CommandArgument="<%# Container.DataItemIndex %>" runat="server" Height="20px" ImageUrl="~/images/plus.png" />
                                <asp:GridView ID="gvDoc"  runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%" Visible="False" EmptyDataText="No data found."
                                     OnRowDataBound="gvDoc_RowDataBound">
                                <Columns>
                                        <asp:TemplateField HeaderText="Selected" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvDoc_cb" runat="server" Checked="true"/>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="5%" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Doctype Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvDoc_Name" runat="server" Text='<%# Bind("doctype_code") %>'></asp:Label>
                                         
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Document Name" >
                                        <ItemTemplate>
                                             <asp:Label ID="gvDoc_DocName" runat="server" Text='<%# Bind("doc_name") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lnkdoc_name" runat="server" Text='<%# Bind("doc_namelnk") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                         <asp:Label ID="lbStatus" runat="server" Text='<%# Bind("doc_status") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Request to Recheck">
                                    <ItemTemplate>
                                        <asp:Button ID="btnSend" runat="server" Text="Send Mail"  CssClass="btn btn-primary" width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnSend_Click" OnClientClick="if(!confirm('Do you want to send mail?')) return false;"/>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Update Document">
                                    <ItemTemplate>
                                        <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-flex;" ></asp:FileUpload>
                                        <asp:Button ID="btnUploadDoc" runat="server" Text="Upload Document" CssClass="btn btn-primary" width="130px"  Font-Names="tahoma" Font-Size="10pt" OnClientClick="if(!confirm('Do you want to upload document?')) return false;" OnClick="btnUploadDoc_Click"/>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Verify Document">
                                    <ItemTemplate>
                                             <asp:Label ID="gvDoc_Docnoid" runat="server" Text='<%# Bind("doc_nodeid") %>' Visible="false"></asp:Label>
                                                      <asp:Label ID="gvDoc_Sheetno" runat="server" Text='<%# Bind("doc_sheetno") %>' Visible="false"></asp:Label>
                                         <asp:Label ID="gvDoc_Torname" runat="server" Text='<%# Bind("tor_temp_name") %>' Visible="false"></asp:Label>
                                        <asp:CheckBox ID="cbCheck" runat="server" OnCheckedChanged="cbCheck_CheckedChanged"   AutoPostBack="true"/>
                                        <br />
                                        <asp:Label ID="gvDoc_Verify" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                       </Columns>
                               <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            </asp:GridView>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="45%" />
                                </asp:TemplateField>

                            </Columns>
                               <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                           <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        </asp:GridView>
 
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style5">&nbsp;</td>
                   <%-- <td>
                        <br />
                        <asp:Label ID="Label11" runat="server" Text="Comment:"></asp:Label>
                        &nbsp; <asp:TextBox ID="txtComment" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="40%" Rows="3" TextMode="MultiLine"></asp:TextBox>
                      <br /> <asp:Button ID="btnSaveCom" runat="server" Text="Save"  CssClass="btn btn-primary"   Font-Names="tahoma" width="100px" style="margin-top:10px" OnClick="btnSaveCom_Click" />
                    </td>--%>
                </tr>
               </table>
                </div>
             <div id="dvSub" runat="server" >
                 <table style="width:100%">
                     <tr>
                     <td class="auto-style5">&nbsp;</td>
                    <td>
                        <asp:Label ID="Label11" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Bid No."></asp:Label>
                        &nbsp;&nbsp;
                        <asp:Label ID="lblBidno" runat="server" Font-Names="Tahoma" Font-Size="10pt" ></asp:Label>
                        </td>
                         </tr>
                     <tr>
                            <td class="auto-style5">&nbsp;</td>
                         <td>  
                             <asp:HiddenField ID="hfScrollPosition2" Value="0" runat="server" />
                             <div id="bigDiv2" runat="server" style="height:700px;max-height:650px;overflow-y:auto;width:100%" onscroll="setScrollPosition2(this.scrollTop);">
                                <asp:GridView ID="gvDocSub"  runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%"  EmptyDataText="No data found."
                                    CellPadding="4" ForeColor="#333333" Visible="true" OnRowDataBound="gvDocSub_RowDataBound">
                                      <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                  

                                    <asp:TemplateField HeaderText="Doctype Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvDocSub_NameFull" runat="server" Text='<%# Bind("doctype_name")%>'></asp:Label>
                                            <asp:Label ID="gvDocSub_Name" runat="server" Text='<%# Bind("doctype_code") %>' Visible="false"></asp:Label>
                                         
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="Document Name" >
                                        <ItemTemplate>
                                             <asp:Label ID="gvDocSub_DocName" runat="server" Text='<%# Bind("doc_name") %>' Visible="false"></asp:Label>
                                            <asp:Label ID="lnkdoc_name" runat="server" Text='<%# Bind("doc_namelnk") %>'></asp:Label>
                                              
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" Width="150px" />
                                    </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Request to Recheck">
                                    <ItemTemplate>
                                        <asp:Button ID="btnSend" runat="server" Text="Send Mail"  CssClass="btn btn-primary" width="100px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnSend_Click" OnClientClick="if(!confirm('Do you want to send mail?')) return false;"/>
                                    </ItemTemplate>
                                    <ItemStyle Width="100px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                      <asp:TemplateField HeaderText="Update Document">
                                    <ItemTemplate>
                                        <asp:FileUpload ID="FileUpload1" runat="server" Style="display: inline-flex;" ></asp:FileUpload>
                                        <asp:Button ID="btnUploadDoc" runat="server" Text="Upload Document" CssClass="btn btn-primary" width="130px"  Font-Names="tahoma" Font-Size="10pt" OnClientClick="if(!confirm('Do you want to upload document?')) return false;" OnClick="btnUploadDoc_Click"/>
                                    </ItemTemplate>
                                    <ItemStyle Width="120px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Verify Document">
                                    <ItemTemplate>
                                             <asp:Label ID="gvDocSub_Docnoid" runat="server" Text='<%# Bind("doc_nodeid") %>' Visible="false"></asp:Label>
                                                      <asp:Label ID="gvDocSub_Sheetno" runat="server" Text='<%# Bind("doc_sheetno") %>' Visible="false"></asp:Label>
                                        <asp:CheckBox ID="cbCheck" runat="server" OnCheckedChanged="cbCheck_CheckedChanged"   AutoPostBack="true"/>
                                        <br />
                                        <asp:Label ID="gvDocSub_Verify" runat="server" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="10%" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                       </Columns>
                          <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                           <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                            </asp:GridView>
                                 </div>
                         </td>
                     </tr>
                 </table>
             </div>
             <table style="width:100%">
                  <tr>
                    <td class="auto-style5">&nbsp;</td>
                      <td>
                          <uc2:wucComment_Att runat="server" ID="wucComment_Att" />
                      </td>
                      </tr>

              <%--  <tr>
                    <td class="auto-style5">&nbsp;</td>
                    <td>
                        <asp:Button ID="Button2" runat="server" Text="Submit Final Bid Doc"  CssClass="btn btn-primary"   Font-Names="tahoma" />
                    </td>
                </tr>--%>
                </table>
              <div id="dvApv" runat="server" style="margin-top:10px" >
                <table cellpadding="0" cellspacing="0" style="width:98%">
                <tr>
                    <td class="auto-style5"></td>
                    <td >
                          <asp:Label ID="Label3" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="บันทึกเห็นชอบ TOR"></asp:Label>
                    </td>
                </tr>
                    <tr>
                        <td style="height:10px"></td>
                        <td>
                               <asp:Label ID="Label7" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Document Type: "></asp:Label>
                             &nbsp;&nbsp;<asp:DropDownList ID="ddlDoctype" runat="server" Width="200px"></asp:DropDownList>
                        </td>
                    </tr>
                <tr><td style="height:10px"></td></tr>
                 <tr>
                     <td class="auto-style5"></td>
                    <td >
                          <asp:Label ID="Label4" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="File: "></asp:Label>
                       &nbsp;&nbsp;  <asp:FileUpload ID="FileUpload2" runat="server" Style="display: inline-block;" ></asp:FileUpload>
                        
                    </td>
                </tr>
                 <tr><td style="height:10px"></td></tr>
                <tr>
                    <td class="auto-style5"></td>
                    <td >
                          <asp:Label ID="Label6" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Approval Date: "></asp:Label>
                        &nbsp;&nbsp;&nbsp; <input type="text" id="Appr_Date" runat="server" class="picker" autocomplete="off"> 
                          &nbsp;&nbsp; <asp:Button ID="btnAddData" runat="server" Text="Add"  CssClass="btn btn-primary"   Font-Names="tahoma"  width="100px" OnClick="btnAddData_Click" />
                    </td>
                </tr>
                <tr>
                   
                    <td class="auto-style5"></td>
                    <td >
                         <br />
                        <asp:GridView ID="gvDocNew"  runat="server" AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="10pt" Width="98%"  >
                                <Columns>
                                     <asp:TemplateField HeaderText="" >
                                        <ItemTemplate>
                                            <asp:CheckBox ID="gvDocNew_cb" runat="server" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Doctype Code" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvDocNew_Doctype" runat="server" Text='<%# Bind("doctype_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                          <asp:TemplateField HeaderText="File Name" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvDocNew_Filename" runat="server" Text='<%# Bind("doc_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                     <asp:TemplateField HeaderText="Approval Date" >
                                        <ItemTemplate>
                                            <asp:Label ID="gvDocNew_AppDate" runat="server" Text='<%# Eval("approve_date", "{0:dd/MM/yyyy}") %>'></asp:Label>
                                             <asp:Label ID="gvDocNew_docid" runat="server" Text='<%# Bind("doc_nodeid") %>' Visible="false"></asp:Label>
                                             <asp:Label ID="gvDocNew_Doccode" runat="server" Text='<%# Bind("doctype_code") %>' Visible="false"></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Action" >
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgdel" runat="server" src="../../images/bin.png" Width="20px" Height="20px" OnClick="imgdel_Click" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                    </asp:TemplateField>
                                       </Columns>
                               <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                    </td>
                </tr>
                <tr><td style="height:10px"></td></tr>
                <tr >
                    <td colspan="2"  style="text-align:right">
                         &nbsp;&nbsp;<asp:Button ID="btnSubmit" runat="server" Text="Submit"  CssClass="btn btn-primary"   Font-Names="tahoma" width="100px" OnClick="btnSubmit_Click"/>
                        &nbsp;&nbsp;<asp:Button ID="btnApv" runat="server" Text="Approve"  CssClass="btn btn-primary"   Font-Names="tahoma"  width="100px" OnClick="btnApv_Click"/>
                        &nbsp;&nbsp;<asp:Button ID="btnRej" runat="server" Text="Reject"  CssClass="btn btn-primary"   Font-Names="tahoma"  width="100px" OnClick="btnRej_Click"/>
                    </td>
                </tr>
            </table>
                </div>
        </div>
           <div id="myModal" runat="server" class="modal">
            <!-- Modal content class="modal"-->
            <div class="modal-content" style="width:90%">
                <table style="width:100%">
                    <tr>
                        <td>
                             <asp:Label ID="Label8" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="Upload Document"></asp:Label>
                        </td>
                    </tr>
                      <tr><td style="height:10px"></td></tr>
                 <tr>
                        <td>
                               <asp:Label ID="Label9" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="Document Type: "></asp:Label>
                             &nbsp;&nbsp;<asp:DropDownList ID="ddlAllDoc" runat="server" Width="200px"></asp:DropDownList>
                        </td>
                    </tr>
                <tr><td style="height:10px"></td></tr>
                 <tr>
                    <td >
                          <asp:Label ID="Label10" runat="server" Font-Names="Tahoma" Font-Size="10pt" Text="File: "></asp:Label>
                       &nbsp;&nbsp;  <asp:FileUpload ID="FileUpload3" runat="server" Style="display: inline-block;" ></asp:FileUpload>
                        
                    </td>
                </tr>
                 <tr><td style="height:10px"></td></tr>
                    <tr>
                        <td style="text-align:right;">
                            <asp:Button ID="btnUploadDoc" runat="server" Text="Upload" class="btn btn-primary" OnClick="btnUploadDoc_Click1" /> &nbsp;&nbsp; <asp:Button ID="btnClose" runat="server" Text="Close" class="btn btn-danger" OnClick="btnClose_Click"/>
                        </td>
                    </tr>
                </table>
            </div>
        </div>

              <div id="myPreview" runat="server" class="modal">
            <!-- Modal content   -->
            <div class="modal-content" style="width:90%;">
                <table align="center" style="width: 95%;margin-top:10px;margin-bottom:10px">
                    <tr>
                         <td align="center">
                       <asp:Panel ID="p_PDF" runat="server"  Height="650px" Width="98%">
                        <iframe id="docview" src="about:blank" runat="server" class="docview"></iframe>
                    </asp:Panel>
                         </td>
                        </tr>
                        <tr><td style="height:10px"></td></tr>
                     <tr>
                         <td style="text-align:right">
                             <asp:Button ID="btnColsePop" runat="server" Text="Close" class="btn btn-danger" OnClick="btnColsePop_Click" />
          
                         </td>
                     </tr>
                     </table>
            </div>
        </div>
         <asp:HiddenField ID="hidLogin" runat="server" />
           <asp:HiddenField ID="hidDept" runat="server" />
        <asp:HiddenField ID="hidSect" runat="server" />
          <asp:HiddenField ID="hidBidno" runat="server" />
        <asp:HiddenField ID="hidRev" runat="server" />
         <asp:HiddenField ID="hidMode" runat="server" />     
        <asp:HiddenField ID="hidWorkID" runat="server" />
        <asp:HiddenField ID="hidSubWorkID" runat="server" />
         <asp:HiddenField ID="hidAcID" runat="server" />
         <asp:HiddenField ID="hidAcname" runat="server" />
         <asp:HiddenField ID="hidTaskID" runat="server" />
         <asp:HiddenField ID="hidNodeid" runat="server" />
        <asp:HiddenField ID="hidMc" runat="server" />
    </form>
</body>
</html>
