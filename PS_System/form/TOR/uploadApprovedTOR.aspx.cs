﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.TOR
{
    public partial class uploadApprovedTOR : System.Web.UI.Page
    {
        #region Public
        private string zconnectionstr = ConfigurationManager.AppSettings["db_ps"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        oraDBUtil ora = new oraDBUtil();
        public string zcommondb = ConfigurationManager.AppSettings["db_common"].ToString();
        string zAttachDocType = "";
        string zPRTemplate = ConfigurationManager.AppSettings["PR_Template"].ToString();
        string zPROutputfile = ConfigurationManager.AppSettings["PR_Outputfile"].ToString();
        string zPR_NodeID = ConfigurationManager.AppSettings["PR_NodeID"].ToString();
        // <add key="TOR_NodeID" value="2986548" />
        string zTOR_NodeID = ConfigurationManager.AppSettings["TOR_NodeID"].ToString();
        string zContract_NodeID = ConfigurationManager.AppSettings["Contract_NodeID"].ToString();
        OTFunctions zot = new OTFunctions();
        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

                
                try { hidBidNo.Value = Request.QueryString["bid_no"].ToString(); }
                catch { hidBidNo.Value = ""; }
                try { hidBidRevision.Value = Request.QueryString["bid_revision"].ToString(); }
                catch { hidBidRevision.Value = ""; }
                try { hidMode.Value = Request.QueryString["zmode"].ToString(); }
                catch { hidMode.Value = ""; }
                // Debug 
                //pr_no = "PR-1-132021"; 
                hidMode.Value = "Upload";
                lblMode.Text = hidMode.Value.ToUpper();
               
                ini_Data();
            }
            else
            {
                this.Page.MaintainScrollPositionOnPostBack = true;
            }
        }
        public static string StripHTML(string input)
        {
            return Regex.Replace(input, "<.*?>", String.Empty);
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        private void ini_Data()
        {
          
            ini_bid_no();
            ini_gvA();
        }
        private void ini_bid_no()
        {
            ddlBid.Items.Clear();
            string sql = "select distinct bid_no, bid_no as col2  from ps_t_bid_monitoring order by bid_no ";
            DataSet ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            ddlBid.DataSource = ds.Tables[0];
            ddlBid.DataTextField = ds.Tables[0].Columns[0].ColumnName;
            ddlBid.DataValueField = ds.Tables[0].Columns[1].ColumnName;
            ddlBid.DataBind();
            ListItem li = new ListItem();
            li.Text = "-Select Bid No.-";
            li.Value = "";
            ddlBid.Items.Insert(0, li);
            if (hidBidNo.Value != "")
            {
                try { ddlBid.SelectedValue = hidBidNo.Value; } catch { ddlBid.SelectedIndex = 0; }
            }
            if (hidBidRevision.Value != "") { txtRevision.Text = hidBidRevision.Value; }
        }
        private void bind_bid(string xbidno, string xrevision)
        {
            string sql = "select *  from bid_no where bid_no = '" + xbidno + "' and bid_revision = " + xrevision + " order by bid_no ";
            DataSet ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var dr = ds.Tables[0].Rows[0];
                txtBidDesc.Text = StripHTML(dr["bid_desc"].ToString());
            }
        }

        protected void ddlBid_SelectedIndexChanged(object sender, EventArgs e)
        {
            //find max revision
            string sql = " select * from ps_t_bid_monitoring  where bid_no = '" + ddlBid.SelectedValue.ToString() + "' order by bid_no asc , bid_revision desc ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                var xbid_revision = ds.Tables[0].Rows[0]["bid_revision"].ToString();
                txtRevision.Text = xbid_revision;
                bind_bid(ddlBid.SelectedValue.ToString(), txtRevision.Text);
                hidBidNo.Value = ddlBid.SelectedValue.ToString();
                hidBidRevision.Value = xbid_revision;
                ini_gvA();
            }
        }

        protected void ibtnSearchBid_Click(object sender, ImageClickEventArgs e)
        {
            bind_bid(ddlBid.SelectedValue.ToString(), txtRevision.Text.Trim());
        }
        private void ini_gvA()
        {
                            /*  bid_no,
                            bid_revision,
                            tor_doc_nodeid,
                            tor_doc_name,
                            description,
                            */
            DataTable dt = new DataTable();

            dt.Columns.Add("itemno", typeof(string));
            dt.Columns.Add("tor_doc_nodeid", typeof(string));
            dt.Columns.Add("description", typeof(string));
            dt.Columns.Add("tor_doc_name", typeof(string));
            if (ddlBid.SelectedIndex > 0)
            {
                string sql = " select * from ps_t_tor where bid_no = '" + ddlBid.SelectedValue + "' and bid_revision = " + hidBidRevision.Value;
                var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        var dr0 = ds.Tables[0].Rows[i];
                        var dr = dt.NewRow();
                        dr["itemno"] = i.ToString();
                        dr["tor_doc_nodeid"] = dr0["tor_doc_nodeid"].ToString();
                        dr["description"] = dr0["description"].ToString();
                        dr["tor_doc_name"] = dr0["tor_doc_name"].ToString();
                        dt.Rows.Add(dr);
                    }
                    
                }
                else
                {
                    //create Bid DOC
                    var dr = dt.NewRow();
                    dr["itemno"] = "1";
                    dr["tor_doc_nodeid"] = "";
                    dr["description"] = "";
                    dr["tor_doc_name"] = "";
                    dt.Rows.Add(dr);
                }
             
            }
            else
            {
                var dr = dt.NewRow();
                dt.Rows.Add(dr);
            }
           
            gvA.DataSource = dt;
            gvA.DataBind();


        }
        protected void gvA_OnRowCommand(object sender, CommandEventArgs e)
        {
            if (e.CommandName == "removedata")
            {

            }
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            var parent_id =   createTORFolder(hidBidNo.Value);
            var doc_name = FileUpload1.FileName;
            var doc_nodeid =  zot.uploadDoc(parent_id, FileUpload1.FileName, FileUpload1.FileName, FileUpload1.FileBytes, "");
            string sql = "select * from ps_t_tor where bid_no = '" + hidBidNo.Value + "' and bid_revision = " + hidBidRevision.Value + " and tor_doc_name = '" + doc_name + "' ";
            var ds = zdb.ExecSql_DataSet(sql, zconnectionstr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                // update
                sql = @" update into ps_t_tor set
                           
                            tor_doc_nodeid = ' '" + doc_nodeid.ToString() + @"' ,
                            tor_doc_name =  '" + doc_name + @"' , 
                            description = '" + txtAttachDesc.Text + @"' 
                            where bid_no =  '" + hidBidNo.Value + @"' and bid_revision =  " + hidBidRevision.Value + @"
                            and tor_doc_name = '" +  doc_name + @"' ";
            }
            else
            {
                //Insert
                sql = @" insert into ps_t_tor (
                            bid_no,
                            bid_revision,
                            tor_doc_nodeid,
                            tor_doc_name,
                            description
                            ) values (
                            '" + hidBidNo.Value + @"' , 
                            " + hidBidRevision.Value + @" , 
                            '" + doc_nodeid.ToString() + @"' , 
                            '" + doc_name + @"' , 
                            '" + txtAttachDesc.Text + @"' 
                            ) ";
            }
            zdb.ExecNonQuery(sql, zconnectionstr);
            ini_gvA();
        }
        private string createTORFolder(string xbid_no)
        {
            string xnodeid = "";
            var xtor_nodeid = zot.getNodeByName(zTOR_NodeID, xbid_no);
            if (xtor_nodeid == null)
            {
                xtor_nodeid = zot.createFolder(zTOR_NodeID, xbid_no);
            }
            hidTORNodeID.Value  = xtor_nodeid.ID.ToString();
            var xbid_no_nodeid = zot.getNodeByName(xtor_nodeid.ID.ToString(),xbid_no);
            if (xbid_no_nodeid == null)
            {
                xbid_no_nodeid = zot.createFolder(xtor_nodeid.ID.ToString(), xbid_no);
            }
            xnodeid = xbid_no_nodeid.ID.ToString();
            return xnodeid;
        }
    }
}