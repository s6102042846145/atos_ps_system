﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="uploadApprovedTOR.aspx.cs" Inherits="PS_System.form.TOR.uploadApprovedTOR" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Upload Approved TOR</title>
         <link href="CustomStyle.css" rel="stylesheet" type="text/css" />   
    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
     <!-- Bootstrap DatePicker -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css"
        type="text/css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"
        type="text/javascript"></script>
    <!-- Bootstrap DatePicker -->
    <script type="text/javascript">
    $(function () {
        $('[id*=txtUploadDate]').datepicker({
            changeMonth: true,
            changeYear: true,
            format: "dd/mm/yyyy",
            language: "tr"
        });
    });
</script>
    <style type="text/css">
        .auto-style1 {
            height: 22px;
        }
        .auto-style2 {
            margin-top: 2px;
            margin-left: 2px;
            font-family: Tahoma;
            font-size: 10pt;
            background-color: transparent;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div  style=" padding: 20px;">
            <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Upload TOR Approved Memo and Related Doc (s)" ForeColor="#0000CC"></asp:Label>
                    </td>
                </tr>
            </table>
            </div>
             <table  class="auto-style22">
                <tr>
                    <td>
                        <asp:Label ID="lbWFID" runat="server"  Text="สำหรับ BID No." CssClass="Label_md"></asp:Label>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlBid" runat="server" OnSelectedIndexChanged="ddlBid_SelectedIndexChanged" CssClass="ddl-md" AutoPostBack="True">
                        </asp:DropDownList>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lbWFID0" runat="server"  Text="Revision" CssClass="Label_md"></asp:Label>
                        &nbsp;&nbsp;&nbsp;<asp:TextBox ID="txtRevision" runat="server" CssClass="auto-style23" Width="102px"></asp:TextBox>
                        &nbsp;&nbsp;<asp:ImageButton ID="ibtnSearchBid" runat="server" Height="23px" ImageUrl="~/images/search.png" OnClick="ibtnSearchBid_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;&nbsp;
                        <asp:Label ID="lblMode" runat="server" Font-Names="tahoma" Font-Size="10pt"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        <asp:TextBox ID="txtBidDesc" CssClass="Text_lg" runat="server" TextMode="MultiLine" Width="800px"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                  <asp:GridView ID="gvA" runat="server" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" EmptyDataText="No data found." Font-Names="tahoma" Font-Size="9pt" Width="96%" OnRowCommand="gvA_OnRowCommand">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                <RowStyle ForeColor="#000066" />
                                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" Font-Names="tahoma" Font-Size="10pt" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F1F1F1" />
                                <SortedAscendingHeaderStyle BackColor="#007DBB" />
                                <SortedDescendingCellStyle BackColor="#CAC9C9" />
                                <SortedDescendingHeaderStyle BackColor="#00547E" />
                                <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                                <Columns>
                                    <asp:TemplateField HeaderText="No.">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_lblNo" CssClass="Text_60" runat="server" Text='<%# Bind("itemno") %>'></asp:Label>
                                            <%--Text='<%# Bind("job_no") %>'--%>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="30px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="doc node id ">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_attachtype" runat="server" Text='<%# Bind("tor_doc_nodeid") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="250px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_Desc" CssClass="Text_200" runat="server" Text='<%# Bind("description") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="300px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Document Name">
                                        <ItemTemplate>
                                            <asp:Label ID="gvA_docname" CssClass="Text_200" runat="server" Text='<%# Bind("tor_doc_name") %>'></asp:Label>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Center" VerticalAlign="Top" Width="200px" />
                                    </asp:TemplateField>
                                 
                                       <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="imgDownload" runat="server" Height="20px" ImageAlign="Right" ImageUrl="~/images/down.png" Width="20px" CommandName="download" CommandArgument='<%# Container.DataItemIndex %>'  />
                                            <asp:ImageButton ID="imgRemove" runat="server" Height="20px" ImageAlign="Right" ImageUrl="~/images/bin.png" Width="20px" CommandName="removedata" CommandArgument='<%# Container.DataItemIndex %>'  />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="center" VerticalAlign="Top" Width="150px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                    </td>
                </tr>
                <tr>
                    <td class="auto-style1">
                        </td>
                    <td class="auto-style1">
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
            <table >
                
                                 <tr>
                                     <td style="background-color: #CCCCCC" >
                                         <asp:Label ID="Label41" runat="server" CssClass="Label_md" Width="150px" Text="input file:"></asp:Label>

                                      </td>
                                     <td style="background-color: #CCCCCC">
                                         <asp:Label ID="Label42" runat="server" CssClass="Label_md" Width="150px" Text="รายละเอียด"></asp:Label>

                                      </td>
                                     <td style="background-color: #CCCCCC" class="auto-style34">&nbsp;</td>
                                    
                                 </tr>
                
                                 <tr>
                                     <td class="ddl-normal" >
                                          <asp:FileUpload ID="FileUpload1"  runat="server" CssClass="auto-style23" Width="260px" />
                                     </td>
                                     <td class="table_body_lefttop" >
                                         &nbsp;
                                          <asp:TextBox ID="txtAttachDesc" runat="server" CssClass="Text_200" Width="302px" ></asp:TextBox>
                                     </td>
                                     <td class="auto-style35" >&nbsp;
                                         <asp:Button ID="btnUpload" runat="server" Text="Upload" CssClass="btn_normal_blue" OnClick="btnUpload_Click" />

                                     </td>
                                    
                                 </tr>
                             </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
             </table>
        </div>

        <asp:HiddenField ID="hidBidNo" runat="server" />
        <asp:HiddenField ID="hidBidRevision" runat="server" />
        <asp:HiddenField ID="hidContractNodeID" runat="server" />
        <asp:HiddenField ID="hidMode" runat="server" />
        <br />
        <asp:HiddenField ID="hidTORNodeID" runat="server" />
    </form>
    
</body>
</html>
