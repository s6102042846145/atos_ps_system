﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TORTemplate.aspx.cs" Inherits="PS_System.form.Bid.TORTemplate" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


    <style type="text/css">
        body {
            font-family: Tahoma;
            font-size: 13px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            padding-top: 100px; /* Location of the box */
            padding-left: 50px; /* Location of the box */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content */
        .modal-content {
            background-color: #fefefe;
            margin: auto;
            padding: 5px;
            border: 1px solid #888;
            width: 95%;
        }

        .treeView img { 
             width:25px; 
             height:25px; 

         }

    </style>
    
 <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>

    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>
  <script >
      $(document).ready(function () {
        $('#Multi_Status').multiselect({
          includeSelectAllOption: true,
          numberDisplayed: 3,
          maxHeight: 300
         });
      });

      $(document).keypress(
          function (event) {
              if (event.which == '13') {
                  event.preventDefault();
              }
          });
     
      function openAddRecord(mode) {
          if (mode == "open") {
              document.getElementById("add_data").style.display = "block";
              document.getElementById("show_data").style.display = "none";

          }
          else {
              document.getElementById("add_data").style.display = "none";
              document.getElementById("show_data").style.display = "block";
              document.getElementById("hidTempCode").value = "";
              document.getElementById("hidStatus").value = "";
          }
          clearData();
          return true;
      }

      function clearData() {
          $('#TxtTempName').val("");
          $('#txtDesc').val("");

      }
      function clearDataModal() {
       
          $("#txtSect").val("");
          $("#txtActicle").val("");
          $("#DDLdoctype").val("");
      }
    
  </script>
</head>
<body>
    <form id="form1" runat="server">
         <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                         <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel2" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - TOR Template"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
       
         <div id="show_data" runat="server" style="padding: 10px 10px 10px 10px;">
            <table style="width: 100%">
                <tr>
                    <td style="width:10%">
                        <asp:Label ID="lblTempName" runat="server" Text="Label">Template Name:</asp:Label>
                    </td>
                    <td style="width:15%">
                        <asp:TextBox ID="TxtSearch" runat="server" Width="250px"></asp:TextBox>
                    </td>
                    <td style="width:6%">
                        <asp:Label ID="lblStatus" runat="server" Text="Label">Status:</asp:Label>
                    </td>
                    <td>
                        <asp:ListBox ID="Multi_Status" runat="server" SelectionMode="Multiple">
                            <asp:ListItem Value="1">Active</asp:ListItem>
                          <asp:ListItem Value="0">In-Active</asp:ListItem>
                        </asp:ListBox>

                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                          <asp:Button ID="btnsearch" runat="server" Text="Search Template" CssClass="btn btn-primary" Width="125px"  Font-Names="tahoma" Font-Size="10pt" OnClick="btnsearch_Click" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4" style="text-align: center; vertical-align: middle; width: 100%">
                        <br />
                        <asp:GridView ID="gvTemplate" runat="server" AutoGenerateColumns="False" Width="600px"
                            Visible="true"  OnRowCommand="gvTemplate_RowCommand" OnRowDataBound="gvTemplate_RowDataBound" EmptyDataText="No data found."
                            class="table table-striped table-bordered table-sm" >
                            <Columns>
                                <asp:TemplateField HeaderText="Template Code">
                                    <ItemTemplate>
                                        <asp:Label ID="lbTemplateCode" runat="server" Text='<%# Bind("tor_temp_code") %>'></asp:Label>
                                          <asp:Label ID="hidActive" runat="server" Text='<%# Bind("is_active") %>' style="display:none;"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Template Name">
                                    <ItemTemplate>
                                        <asp:Label ID="lbTemplateName" runat="server" Text='<%# Bind("tor_temp_name") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Description">
                                    <ItemTemplate>
                                        <asp:Label ID="lbDescription" runat="server" Text='<%# Bind("tor_temp_desc") %>'></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle Width="200px" HorizontalAlign="Center" VerticalAlign="Middle" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Action">
                                    <ItemTemplate>
                                        &nbsp;<asp:ImageButton ID="btnAdd" ClientIDMode="Static" runat="server" ImageUrl="../../Images/add.png" CommandName="adddata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Add New Row" Width="30" ImageAlign="NotSet" />
                                       &nbsp; &nbsp;<asp:ImageButton ID="btnEdit" ClientIDMode="Static" runat="server" ImageUrl="../../Images/edit.png" CommandName="editdata" CommandArgument='<%# Container.DataItemIndex %>'
                                            ToolTip="Edit" Width="30" ImageAlign="NotSet" OnClientClick="if(openAddRecord('open')) return true;"/>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Center" VerticalAlign="Middle" Width="50px" />
                                </asp:TemplateField>
                            </Columns>
                            <HeaderStyle BackColor="#66CCFF" HorizontalAlign="Center" VerticalAlign="Middle" />
                           <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                        </asp:GridView>
                    </td>
                </tr>
             <%--   <tr>
                    <td colspan="6" style="text-align: left; vertical-align: middle; width: 300px;">
                        <asp:Button ID="btnSave" CssClass="btn btn-primary" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Save" OnClick="btnSave_Click" />
                        <asp:Button ID="btnClose" CssClass="btn btn-danger" ClientIDMode="Static" runat="server" Width="100px"
                            Font-Names="tahoma" Font-Size="10pt" Text="Close" />
                    </td>
                </tr>--%>
            </table>
        </div>
            <div id="add_data" style="display: none; padding: 10px 10px 10px 10px;" runat="server">
                <table style="width: 100%;">
                   <tr>
                    <td style="width:10%">
                        <asp:Label ID="Label1" runat="server" Text="Label">Template Name:</asp:Label>
                    </td>
                    <td style="width:17%">
                        <asp:TextBox ID="TxtTempName" runat="server" Width="250px"></asp:TextBox>
                    </td>
                    <td style="width:8%">
                        <asp:Label ID="Label2" runat="server" Text="Label">Description:</asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtDesc" runat="server" Width="250px"></asp:TextBox>
                    </td>
                </tr>
                     <tr>
                    <td colspan="4" style="vertical-align: middle; width: 100%">
            
                        <br />
       
                            <div id="dvTree" runat="server" class="panel panel-info" style="width:fit-content;">
                             
                          <div class="panel-heading">Section</div>
                          <div class="panel-body">
                                  <asp:TreeView id="treeTemp" CssClass="treeView"  runat="server"  OnSelectedNodeChanged="treeTemp_SelectedNodeChanged">
                                              <RootNodeStyle   Font-Size="20px" />
                                              <ParentNodeStyle Font-Size="18px"  />
                                              <LeafNodeStyle Font-Size="16px"  />
                                     
                                      </asp:TreeView>
                          </div>
                              
                            </div>  
             
                    </td>
                </tr>
                        <tr><td> <%--<asp:Button id="btncreate" runat="server" CssClass="btn btn-primary" Text="Create"/>--%>
                            <button onclick="document.getElementById('myModal').style.display='block';clearDataModal();return false;" class="btn btn-primary" >Create</button>
                            </td></tr>
                    <tr>
                        <td colspan="4" style="width: 100%;">
                             <table>
                                    <tr>
                                        <td style="padding: 5px 5px 5px 5px; text-align: left">

                                            <asp:RadioButton ID="rdbAc" runat="server" Text="Active" GroupName="searchtype"  />
                                        </td>
                                        <td style="padding: 5px 5px 5px 5px;">
                                            <asp:RadioButton ID="rdbInAc" runat="server" Text="In-Active" GroupName="searchtype"  />
                                        </td>
                                        
                                    </tr>
                                </table>
                        </td>
                    </tr>
                  <tr>
                    <td colspan="4" style="width: 100%;">
                        <asp:Button ID="btn_save" CssClass="btn btn-primary" runat="server"
                           Text="Save" OnClick="btn_save_Click" />
                        <asp:Button ID="btnClose" CssClass="btn btn-danger" runat="server"
                            Text="Close" OnClientClick="if(openAddRecord('close')) return false;" />
                    </td>
                </tr>
                </table>
        </div>
                <div id="myModal" runat="server"  class="modal"  >
            <!-- Modal content  class="modal" -->
            <div style="width:90%;" class="modal-content" align="center">
                <br />
                <table style="width:95%">
                    <tr>
                            <td style="width:12%;">
                                <asp:Label ID="Label6" runat="server" Text="Label">Vol.:</asp:Label>
                            &nbsp;<asp:DropDownList ID="DDLVol" runat="server">
                                 <asp:ListItem Value="" Text="<-- Select -->"></asp:ListItem>
                                 <asp:ListItem Value="I" Text="I"></asp:ListItem>
                                 <asp:ListItem Value="II" Text="II"></asp:ListItem>
                                 <asp:ListItem Value="III" Text="III"></asp:ListItem>
                                 <asp:ListItem Value="IV" Text="IV"></asp:ListItem>
                                 <asp:ListItem Value="V" Text="V"></asp:ListItem>
                                 <asp:ListItem Value="VI" Text="VI"></asp:ListItem>
                                 <asp:ListItem Value="VII" Text="VII"></asp:ListItem>
                                 <asp:ListItem Value="VIII" Text="VIII"></asp:ListItem>
                                 <asp:ListItem Value="IX" Text="IX"></asp:ListItem>
                                 <asp:ListItem Value="X" Text="X"></asp:ListItem>
                                  </asp:DropDownList>
                              
                        </td>
                        <td style="width:13%;">
                            <asp:Label ID="Label3" runat="server" >Section:</asp:Label>
                            &nbsp;<asp:TextBox ID="txtSect" runat="server" Width="50%" ></asp:TextBox>                    
                        </td>
                          <td > 
                              <asp:Label ID="Label4" runat="server" >Article:</asp:Label>
                            &nbsp;<asp:TextBox ID="txtActicle" runat="server" Width="70%"></asp:TextBox>
                        </td>
                            <td style="width:20%">
                                 &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;<asp:Label ID="Label5" runat="server" Text="Label">Document type:</asp:Label>
                            &nbsp;<asp:DropDownList ID="DDLdoctype" runat="server" Width="40%"></asp:DropDownList>  
                        </td>
                           <td > 
                              <asp:Label ID="Label7" runat="server" >Page No. Format:</asp:Label>
                            &nbsp;<asp:TextBox ID="txtFormat" runat="server" Width="50%"></asp:TextBox>
                        </td>
                        <td >
                                <asp:Label ID="Label8" runat="server" Text="Label">Bid No. Stamp:</asp:Label>
                            <asp:CheckBox ID="cbStamp" runat="server" />
                         <%--   &nbsp;<asp:RadioButton ID="RadioYes" runat="server" Text="Yes" GroupName="searchtype" /> 
                            &nbsp;<asp:RadioButton ID="RadioNo" runat="server" Text="No"  GroupName="searchtype"/>--%>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" style="text-align:right;">
                            <br />
                             <asp:Button id="btnSavetree" runat="server" CssClass="btn btn-primary" Text="save" OnClick="btnSavetree_Click" />
                            &nbsp;&nbsp;<button class="btn btn-danger" onclick="document.getElementById('myModal').style.display='none';return false;">Close</button>
                        </td>
                        
                    </tr>
                </table>
                   
            </div>
        </div>
                <asp:HiddenField ID="hidLogin" runat="server" />
        <asp:HiddenField ID="hidTempCode" runat="server" />
        <asp:HiddenField ID="hidStatus" runat="server" />
    </form>
</body>
</html>
