﻿using PS_Library;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PS_System.form.Bid
{

    public partial class TORTemplate : System.Web.UI.Page
    {
        clsTORTemplate objTORT = new clsTORTemplate();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z560014";
        
                loadgvTemp();
                bind_DDL();
                Multi_Status.Items[0].Selected = true;

                string tooltip = "Section A" + "\n" +
                    "A#" + "\n" +
                    "Example : " +"\n" +
                    "- A1 -"+ "\n" +
                    "- A2 -";

                txtFormat.ToolTip = tooltip;
            }
        }
        protected void loadgvTemp()
        {
            DataTable dt = objTORT.sel_TOR("","1");
            if (dt.Rows.Count > 0)
            {
                gvTemplate.DataSource = dt;
                gvTemplate.DataBind();
                Session.Add("dtTemp", dt);
            }
            else
            {
                dt.Columns.Add("temp_code");
                dt.Columns.Add("temp_name");
                dt.Columns.Add("Description");

                DataRow dr = dt.NewRow();

                dr["temp_code"] = "";
                dr["temp_name"] = "";
                dr["Description"] = "";
                dt.Rows.Add(dr);
                if (dt.Rows.Count > 0)
                {
                    gvTemplate.DataSource = dt;
                    gvTemplate.DataBind();
                    Session.Add("dtTemp", dt);
                }
            }
        }
        protected void bind_DDL()
        {
            DataTable dt = objTORT.sel_Doctype();
            if (dt.Rows.Count > 0)
            {
                int i = 1;
                DDLdoctype.Items.Clear();
                DDLdoctype.Items.Insert(0, new ListItem("<-- Select -->", ""));
                foreach (DataRow dr in dt.Rows)
                {
                    DDLdoctype.Items.Insert(i, new ListItem(dr["doctype_name"].ToString(), dr["doctype_code"].ToString()));
                    i++;
                }
            }
        }
        protected void gvTemplate_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {


            }
        }
        protected void gvTemplate_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "adddata")
            {
                myModal.Style["display"] = "none";
                this.add_data.Style.Add("display", "none");
                this.show_data.Style.Add("display", "block");

                DataTable dtTemp = (DataTable)Session["dtTemp"];
             
                dtTemp.Rows.Clear();
                foreach (GridViewRow g1 in gvTemplate.Rows)
                {
                    Label lbTemplateCode = (Label)g1.FindControl("lbTemplateCode");
                    Label lbTemplateName = (Label)g1.FindControl("lbTemplateName");
                    Label lbDescription = (Label)g1.FindControl("lbDescription");
                    Label hidActive = (Label)g1.FindControl("hidActive");

                    DataRow drOld = dtTemp.NewRow();
                    drOld["tor_temp_code"] = lbTemplateCode.Text;
                    drOld["tor_temp_name"] = lbTemplateName.Text;
                    drOld["tor_temp_desc"] = lbDescription.Text;
                    drOld["is_active"] = hidActive.Text == "" ? DBNull.Value : drOld["is_active"] = hidActive.Text;
                    dtTemp.Rows.Add(drOld);
                }
                
                DataRow drNew = dtTemp.NewRow();
                drNew["tor_temp_code"] = "";
                drNew["tor_temp_name"] = "";
                drNew["tor_temp_desc"] = "";
                drNew["is_active"] = DBNull.Value;

                dtTemp.Rows.Add(drNew);

                this.gvTemplate.DataSource = dtTemp;
                this.gvTemplate.DataBind();

                Session.Add("dtTemp", dtTemp);
            }
            else if (e.CommandName == "editdata")
            {
                myModal.Style["display"] = "none";
                var xrowid = Convert.ToInt32(e.CommandArgument);
                GridView gvTemplate = (GridView)sender;

                string xTempCode = ((Label)gvTemplate.Rows[xrowid].FindControl("lbTemplateCode")).Text;
                string xTempName = ((Label)gvTemplate.Rows[xrowid].FindControl("lbTemplateName")).Text;
                string xDesc = ((Label)gvTemplate.Rows[xrowid].FindControl("lbDescription")).Text;
                string xhidActive = ((Label)gvTemplate.Rows[xrowid].FindControl("hidActive")).Text;
                hidTempCode.Value = xTempCode;
                hidStatus.Value = xhidActive;
                rdbAc.Checked = false;
                rdbInAc.Checked = false;
                this.loadData(xTempCode, xTempName, xDesc, xhidActive);
            }
        }
        protected void btn_save_Click(object sender, EventArgs e)
        {
            if (TxtTempName.Text != "")
            {
                this.add_data.Style.Add("display", "none");
                this.show_data.Style.Add("display", "block");
                myModal.Style["display"] = "none";

                string chkAvtive = "";
                if (rdbAc.Checked) chkAvtive = "1";
                else if (rdbInAc.Checked) chkAvtive = "0";
                
                objTORT.insertTORT(hidTempCode.Value, TxtTempName.Text, txtDesc.Text, chkAvtive, hidLogin.Value);
        
                loadgvTemp();
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Saved Successfully.');", true);
                hidTempCode.Value = "";
                hidStatus.Value = "";
            }
            else
            {
                myModal.Style["display"] = "none";
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Template Name.');", true);
            }
        }
        protected void loadData(string xTempCode, string xTempName, string xDesc, string xhidActive)
        {
            txtDesc.Text = xDesc;
            TxtTempName.Text = xTempName;
            if (xhidActive == "1") rdbAc.Checked = true;
            else if (xhidActive == "0") rdbInAc.Checked = true;
            TreeNode tnVol = new TreeNode();
            TreeNode tnnew = new TreeNode();
            TreeNode tnSec = new TreeNode();
            TreeNode tnArti = new TreeNode();
            TreeNode tnDoc = new TreeNode();
            treeTemp.Nodes.Clear();
            string chkSec = "";
            string chkAr = "";
            string chkDoc = "";
            DataTable dtdis = objTORT.Dissel_BidDoc(hidTempCode.Value);
            if (dtdis.Rows.Count > 0)
            {
                for (int i = 0; i < dtdis.Rows.Count; i++)
                {
                    DataTable dt = objTORT.sel_BidDoc(hidTempCode.Value);
                    DataView dv = new DataView(dt);
                    dv.RowFilter = "vol_no='" + dtdis.Rows[i]["vol_no"].ToString() + "'";
                    if (dv.Count > 0)
                    {
                        tnVol.Text = dv[0]["vol_no"].ToString();
                        tnVol.Value = dv[0]["vol_no"].ToString();

                        for (int j = 0; j < dv.Count; j++) //data
                        {
                            if (chkSec == dv[j]["section_name"].ToString())
                            {

                                if (chkAr == dv[j]["article_name"].ToString())
                                {
                                    //tnArti = new TreeNode();

                                    if (chkDoc == dv[j]["doctype_name"].ToString())
                                    {

                                    }
                                    else if (dv[j]["doctype_name"].ToString() == "")
                                    {


                                    }
                                    else
                                    {
                                        tnDoc = new TreeNode();
                                        tnDoc.Text = dv[j]["doctype_name"].ToString();
                                        tnDoc.Value = dv[j]["doctype_name"].ToString();
                                        tnArti.ChildNodes.Add(tnDoc);

                                    }
                                }
                                else
                                {
                                    if (dv[j]["doctype_name"].ToString() == "")
                                    {
                                        tnArti = new TreeNode();
                                        tnArti.Text = dv[j]["article_name"].ToString();
                                        tnArti.Value = dv[j]["article_name"].ToString();
                                        tnSec.ChildNodes.Add(tnArti);
                                        tnVol.ChildNodes.Add(tnSec);
                                        chkAr = tnArti.Text;

                                    }
                                    else
                                    {
                                        // tnSec = new TreeNode();
                                        tnArti = new TreeNode();
                                        tnDoc = new TreeNode();
                                        tnArti.Text = dv[j]["article_name"].ToString();
                                        tnDoc.Text = dv[j]["doctype_name"].ToString();
                                        tnArti.Value = dv[j]["article_name"].ToString();
                                        tnDoc.Value = dv[j]["doctype_name"].ToString();
                                        tnArti.ChildNodes.Add(tnDoc);
                                        tnSec.ChildNodes.Add(tnArti);
                                        //tnVol.ChildNodes.Add(tnSec);
                                        chkAr = tnArti.Text;
                                        chkDoc = tnDoc.Text;
                                    }
                                }
                            }
                            else
                            {
                                tnSec = new TreeNode();
                                tnSec.Text = dv[j]["section_name"].ToString();
                                tnSec.Value = dv[j]["section_name"].ToString();
                                chkSec = tnSec.Text;
                                if (dv[j]["doctype_name"].ToString() == "")
                                {
                                    tnArti = new TreeNode();
                                    tnArti.Text = dv[j]["article_name"].ToString();
                                    tnArti.Value = dv[j]["article_name"].ToString();
                                    tnSec.ChildNodes.Add(tnArti);
                                    tnVol.ChildNodes.Add(tnSec);
                                    chkAr = tnArti.Text;

                                }
                                else
                                {
                                    //tnSec = new TreeNode();
                                    tnArti = new TreeNode();
                                    tnDoc = new TreeNode();
                                    tnArti.Text = dv[j]["article_name"].ToString();
                                    tnDoc.Text = dv[j]["doctype_name"].ToString();
                                    tnArti.Value = dv[j]["article_name"].ToString();
                                    tnDoc.Value = dv[j]["doctype_name"].ToString();
                                    tnArti.ChildNodes.Add(tnDoc);
                                    tnSec.ChildNodes.Add(tnArti);
                                    tnVol.ChildNodes.Add(tnSec);
                                    chkAr = tnArti.Text;
                                    chkDoc = tnDoc.Text;
                                }
                            }
                        }

                    }
                    treeTemp.Nodes.Add(tnVol);
                    tnVol = new TreeNode();
                    chkSec = "";
                    chkAr = "";
                    chkDoc = "";
                   
                    dvTree.Style["display"] = "block";
                }
            }
            else
            {
                dvTree.Style["display"] = "none";
            }

            this.add_data.Style.Add("display", "block");
            this.show_data.Style.Add("display", "none");

            //Session.Add("dtTemp", dt);
        }

        protected void btnsearch_Click(object sender, EventArgs e)
        {
            string taskstatusbid = "";
            foreach (ListItem item in Multi_Status.Items)
            {
                if (item.Selected)
                {
                    if (taskstatusbid == "")
                    {
                        taskstatusbid = item.Value;
                    }
                    else
                    {
                        taskstatusbid += "," + item.Value;
                    }
                }
            }
            
            DataTable dt = objTORT.sel_TOR(TxtSearch.Text, taskstatusbid);
            if (dt.Rows.Count > 0)
            {
                gvTemplate.DataSource = dt;
                gvTemplate.DataBind();
                Session.Add("dtTemp", dt);
            }
            else
            {
                gvTemplate.DataSource = null;
                gvTemplate.DataBind();
            }
            this.add_data.Style.Add("display", "none");
            this.show_data.Style.Add("display", "block");
            myModal.Style["display"] = "none";
        }
       
        protected void btnSavetree_Click(object sender, EventArgs e)
        {
            TreeNode tnnew = new TreeNode();
            TreeNode tnVol = new TreeNode();
            TreeNode tnSec = new TreeNode();
            TreeNode tnArti = new TreeNode();
            TreeNode tnDoc = new TreeNode();
            treeTemp.Nodes.Clear();
            string chkSec = "";
            string chkAr = "";
            string chkDoc = "";
            if (DDLVol.SelectedValue != "" && txtSect.Text != "" && txtActicle.Text != "")//&& DDLdoctype.SelectedValue != ""
            {
                dvTree.Style["display"] = "block";
                string stamp = "0";
                if (cbStamp.Checked) stamp = "1";
                if (hidTempCode.Value != "") hidTempCode.Value = objTORT.insertBidDoc(txtSect.Text, txtActicle.Text, DDLdoctype.SelectedValue, hidLogin.Value, hidTempCode.Value, DDLVol.SelectedValue, txtFormat.Text, stamp);
                else hidTempCode.Value = objTORT.insertBidDoc(txtSect.Text, txtActicle.Text, DDLdoctype.SelectedValue, hidLogin.Value, "", DDLVol.SelectedValue, txtFormat.Text, stamp);

                DataTable dtdis = objTORT.Dissel_BidDoc(hidTempCode.Value);//root a s
                if (dtdis.Rows.Count > 0)
                {
                    for (int i = 0; i < dtdis.Rows.Count; i++)
                    {
                        DataTable dt = objTORT.sel_BidDoc(hidTempCode.Value);
                        DataView dv = new DataView(dt);
                        dv.RowFilter = "vol_no='" + dtdis.Rows[i]["vol_no"].ToString() + "'";// a b c d
                        if (dv.Count > 0)
                        {
                            tnVol.Text = dv[0]["vol_no"].ToString();
                            tnVol.Value = dv[0]["vol_no"].ToString();
                           
                            for (int j = 0; j < dv.Count; j++) //data
                            {
                                if (chkSec == dv[j]["section_name"].ToString())
                                {
                                  
                                    if (chkAr == dv[j]["article_name"].ToString())
                                    {
                                        //tnArti = new TreeNode();

                                        if (chkDoc == dv[j]["doctype_name"].ToString())
                                        {

                                        }
                                        else if (dv[j]["doctype_name"].ToString() == "")
                                        {


                                        }
                                        else
                                        {
                                            tnDoc = new TreeNode();
                                            tnDoc.Text = dv[j]["doctype_name"].ToString();
                                            tnDoc.Value = dv[j]["doctype_name"].ToString();
                                            tnArti.ChildNodes.Add(tnDoc);

                                        }
                                    }
                                    else
                                    {
                                        if (dv[j]["doctype_name"].ToString() == "")
                                        {
                                            tnArti = new TreeNode();
                                            tnArti.Text = dv[j]["article_name"].ToString();
                                            tnArti.Value = dv[j]["article_name"].ToString();
                                            tnSec.ChildNodes.Add(tnArti);
                                            tnVol.ChildNodes.Add(tnSec);
                                            chkAr = tnArti.Text;

                                        }
                                        else
                                        {
                                           // tnSec = new TreeNode();
                                            tnArti = new TreeNode();
                                            tnDoc = new TreeNode();
                                            tnArti.Text = dv[j]["article_name"].ToString();
                                            tnDoc.Text = dv[j]["doctype_name"].ToString();
                                            tnArti.Value = dv[j]["article_name"].ToString();
                                            tnDoc.Value = dv[j]["doctype_name"].ToString();
                                            tnArti.ChildNodes.Add(tnDoc);
                                            tnSec.ChildNodes.Add(tnArti);
                                            //tnVol.ChildNodes.Add(tnSec);
                                            chkAr = tnArti.Text;
                                            chkDoc = tnDoc.Text;
                                        }
                                    }
                                }
                                else
                                {
                                    tnSec = new TreeNode();
                                    tnSec.Text = dv[j]["section_name"].ToString();
                                    tnSec.Value = dv[j]["section_name"].ToString();
                                    chkSec = tnSec.Text;
                                    if (dv[j]["doctype_name"].ToString() == "")
                                    {
                                        tnArti = new TreeNode();
                                        tnArti.Text = dv[j]["article_name"].ToString();
                                        tnArti.Value = dv[j]["article_name"].ToString();
                                        tnSec.ChildNodes.Add(tnArti);
                                        tnVol.ChildNodes.Add(tnSec);
                                        chkAr = tnArti.Text;

                                    }
                                    else
                                    {
                                        //tnSec = new TreeNode();
                                        tnArti = new TreeNode();
                                        tnDoc = new TreeNode();
                                        tnArti.Text = dv[j]["article_name"].ToString();
                                        tnDoc.Text = dv[j]["doctype_name"].ToString();
                                        tnArti.Value = dv[j]["article_name"].ToString();
                                        tnDoc.Value = dv[j]["doctype_name"].ToString();
                                        tnArti.ChildNodes.Add(tnDoc);
                                        tnSec.ChildNodes.Add(tnArti);
                                        tnVol.ChildNodes.Add(tnSec);
                                        chkAr = tnArti.Text;
                                        chkDoc = tnDoc.Text;
                                    }


                                }
                            }

                        }
                        treeTemp.Nodes.Add(tnVol);
                        tnVol = new TreeNode();
                        chkSec = "";
                        chkAr = "";
                        chkDoc = "";
                    }

                }
                string pathmain = "";
                string pathpar = "";
                string path = "";
                foreach (TreeNode node in treeTemp.Nodes)
                {
                    if (node.Value == DDLVol.SelectedValue)
                    {
                        pathmain = node.ValuePath;
                    }
                    foreach (TreeNode nodech in node.ChildNodes)
                    {
                        if (nodech.Value == txtSect.Text)
                        {
                            pathpar = nodech.ValuePath;
                        }
                        foreach (TreeNode nodechk in nodech.ChildNodes)
                        {
                            if (nodechk.Value == txtActicle.Text)
                            {
                                path = nodechk.ValuePath;
                                break;
                            }

                        }
                    }
                    
                }
                treeTemp.FindNode(pathmain).Expand();
                treeTemp.FindNode(pathpar).Expand();
                treeTemp.FindNode(path).Expand();
             
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please input Data.');", true);
                loadData("", TxtTempName.Text, txtDesc.Text, hidStatus.Value);
            }
         
            //treeTemp.ExpandAll();
            myModal.Style["display"] = "block";
        }

        protected void treeTemp_SelectedNodeChanged(object sender, EventArgs e)
        {
            TreeView tv = (TreeView)sender;
      
            string value = "";
            string path = "";
            string chkmain = "";
            string chkparent = "";
            string chkarti= "";
            string chkdoc = "";

            txtFormat.Text = "";
            value = tv.SelectedNode.Value;
            path = tv.SelectedNode.ValuePath;
            string[] chkpath = path.Split('/');
            if (chkpath.Length == 1)
            {
                chkmain = chkpath[0];
                //chkparent = chkpath[1];
                if (value == chkmain)
                {
                    DDLVol.SelectedIndex = DDLVol.Items.IndexOf(DDLVol.Items.FindByText(chkmain));
                    txtSect.Text = "";
                    txtActicle.Text = "";
                    DDLdoctype.SelectedIndex = 0;
                }
            }
            else if (chkpath.Length == 2)
            {
                chkmain = chkpath[0];
                chkparent = chkpath[1];
                //chkarti = chkpath[2];
                DDLVol.SelectedIndex = DDLVol.Items.IndexOf(DDLVol.Items.FindByText(chkmain));
                txtSect.Text = chkparent;
                txtActicle.Text = "";
                DDLdoctype.SelectedIndex = 0;
            }
            else if (chkpath.Length == 3)
            {
                chkmain = chkpath[0];
                chkparent = chkpath[1];
                chkarti = chkpath[2];
                //chkdoc = chkpath[3];
                DDLVol.SelectedIndex = DDLVol.Items.IndexOf(DDLVol.Items.FindByText(chkmain));
                txtSect.Text = chkparent;
                txtActicle.Text = chkarti;
                DDLdoctype.SelectedIndex = 0;
            }
            else if (chkpath.Length == 5)
            {
                chkmain = chkpath[0];
                chkparent = chkpath[1];
                chkarti = chkpath[2];
                chkdoc = chkpath[3]+"/"+ chkpath[4];
                DDLVol.SelectedIndex = DDLVol.Items.IndexOf(DDLVol.Items.FindByText(chkmain));
                txtSect.Text = chkparent;
                txtActicle.Text = chkarti;
                DDLdoctype.SelectedIndex = DDLdoctype.Items.IndexOf(DDLdoctype.Items.FindByText(chkdoc));
            }
            else if (chkpath.Length > 3)
            {
                chkmain = chkpath[0];
                chkparent = chkpath[1];
                chkarti = chkpath[2];
                chkdoc = chkpath[3];
                DDLVol.SelectedIndex = DDLVol.Items.IndexOf(DDLVol.Items.FindByText(chkmain));
                txtSect.Text = chkparent;
                txtActicle.Text = chkarti;
                DDLdoctype.SelectedIndex = DDLdoctype.Items.IndexOf(DDLdoctype.Items.FindByText(chkdoc));
            }
            myModal.Style["display"] = "block";
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }

    }
}