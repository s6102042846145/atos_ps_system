﻿using PS_Library;
using PS_Library.DocumentManagement;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace PS_System.form.TOR
{
    public partial class TOR01 : System.Web.UI.Page
    {
        private string zconn = ConfigurationManager.AppSettings["db_ps"].ToString();
        public string strFolderAttTaskNodeId = ConfigurationManager.AppSettings["wfps_att_folder"].ToString();
        DbControllerBase zdb = new DbControllerBase();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                if (Request.QueryString["zuserlogin"] != null)
                    this.hidLogin.Value = Request.QueryString["zuserlogin"];
                else
                    this.hidLogin.Value = "z594826";

                if (Request.QueryString["bidno"] != null)
                    this.hidBidno.Value = Request.QueryString["bidno"];
                else
                    this.hidBidno.Value = "TS12-S-02";

                if (Request.QueryString["zmode"] != null)
                    this.hidMode.Value = Request.QueryString["zmode"].ToLower();
                else
                    this.hidMode.Value = "";

                if (Request.QueryString["workid"] != null)
                    hidWorkID.Value = Request.QueryString["workid"];
                else
                    hidWorkID.Value = "";

                if (Request.QueryString["subworkid"] != null)
                    hidSubWorkID.Value = Request.QueryString["subworkid"];
                else
                    hidSubWorkID.Value = "";

                if (Request.QueryString["taskid"] != null)
                    hidTaskID.Value = Request.QueryString["taskid"];
                else
                    hidTaskID.Value = "0";

                EmpInfoClass empInfo = new EmpInfoClass();
                Employee emp = empInfo.getInFoByEmpID(hidLogin.Value);
                hidDept.Value = emp.ORGSNAME4;
                hidSect.Value = emp.ORGSNAME5;
                hidMc.Value = emp.MC_SHORT;
               // hidSect.Value = "หสวส-ส.";//อย่าลืมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมมม
                getWorkflowAttribute();
                ini_bid_no();
                if (hidSect.Value == "หสวส-ส." || hidSect.Value == "หบวส-ส.")
                {
                    dvMain.Style["display"] = "none";
                    lblBidno.Text = hidBidno.Value;
                    bind_gvSN();
                }
                else
                {
                    dvSub.Style["display"] = "none";
                    defalutTemplate();
                    //bind_gv0();
                    lbBidno.Text = hidBidno.Value;
                }
                if (hidMode.Value == "apv") { btnSubmit.Style["display"] = "none"; ddlTOR.Enabled = false; btnSelectTemp.Enabled = false; btnUploadAll.Enabled = false; bind_gvTOR(); }
                else if (hidMode.Value == "req") { btnApv.Style["display"] = "none"; btnRej.Style["display"] = "none"; }
                else if (hidMode.Value == "edit")
                { ddlTOR.Enabled = false; btnSelectTemp.Enabled = false; btnSubmit.Style["display"] = "none"; }

            }
        }
        private void getWorkflowAttribute()
        {

            if ((hidWorkID.Value != "" && hidWorkID.Value != "0") && (hidSubWorkID.Value != "" && hidSubWorkID.Value != "0"))
            {

                OTFunctions ot = new OTFunctions();
                var wfAttrs = ot.getWFAttributes(hidWorkID.Value, hidSubWorkID.Value);
                string pidno = ot.getwfAttrValue(ref wfAttrs, "PID");
                string activity_id = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_ID");
                string activity_name = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_NAME");
                hidAcID.Value = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_ID");
                hidAcname.Value = ot.getwfAttrValue(ref wfAttrs, "ACTIVITY_NAME");
                string strBidNo = string.Empty;
                string strBidRev = string.Empty;
                try
                {
                    strBidNo = ot.getwfAttrValue(ref wfAttrs, "BID_NO");
                    hidBidno.Value = strBidNo;
                    strBidRev = ot.getwfAttrValue(ref wfAttrs, "BID_REV");
                    hidRev.Value = strBidRev;
                    lbBidno.Text = hidBidno.Value;
                }
                catch { }
            }

        }
        private void defalutTemplate() 
        {
            string sql = "";
            sql = "select * from ps_t_bid_doc where bid_no='" + hidBidno.Value + "' and bid_revision='" + hidRev.Value + "'";
            DataTable dt0 = zdb.ExecSql_DataTable(sql, zconn);
            if (dt0.Rows.Count > 0)
            {
                sql = @"select distinct [article_name]
								  ,[bid_no]
								  ,[bid_revision]
								  ,[tor_temp_code]
								  ,[vol_no]
								  ,[section_name]
								  ,[doctype_code]
                                 ,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = dt.doctype_code) as doctype_name
                                ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
								 from ps_t_bid_doc_tor dt where  bid_no='" + hidBidno.Value + "' and  tor_temp_code ='" + dt0.Rows[0]["tor_temp_code"].ToString() + "'";
                DataTable dt01 = zdb.ExecSql_DataTable(sql, zconn);
                if (dt01.Rows.Count > 0)
                {
                    if (hidMode.Value == "edit") ddlTOR.SelectedValue = dt01.Rows[0]["tor_temp_code"].ToString();

                    dt01.DefaultView.Sort = " vol_no";
                    gvTemplate.DataSource = dt01;
                    gvTemplate.DataBind();
                  
                }
                if (dt0.Rows[0]["biddoc_node_id"].ToString() != "")
                {
                    btnPreview.Visible = true;
                    hidNodeid.Value = dt0.Rows[0]["biddoc_node_id"].ToString();
                }
                
            }
            else
            {
                gvTemplate.DataSource = null;
                gvTemplate.DataBind();
            }
            wucComment_Att.BindData("", hidBidno.Value, hidLogin.Value, false);
        }
        private void ini_bid_no()
        {
            //ddlBid.Items.Clear();

            //string sql = "select distinct bid_no,bid_revision from ps_t_mytask_doc where bid_no <>'' order by bid_no ";
            //DataTable ds = zdb.ExecSql_DataTable(sql, zconn);

            //if (ds.Rows.Count > 0)
            //{
            //    ddlBid.Items.Insert(0, new ListItem("<-- Select -->", ""));
            //    for (int i = 0; i < ds.Rows.Count; i++)
            //    {
            //        ddlBid.Items.Insert(i + 1, new ListItem(ds.Rows[i]["bid_no"].ToString(), ds.Rows[i]["bid_no"].ToString() + "|" + ds.Rows[i]["bid_revision"].ToString()));
            //    }

            //}

            clsMyTaskBid obj = new clsMyTaskBid();
            ddlTOR.Items.Clear();
            string sqlTOR = "select * from ps_m_tor_template order by tor_temp_code";
            DataTable dt = zdb.ExecSql_DataTable(sqlTOR, zconn);

            if (dt.Rows.Count > 0)
            {
                ddlTOR.DataSource = dt;
                ddlTOR.DataTextField = "tor_temp_name";
                ddlTOR.DataValueField = "tor_temp_code";
                ddlTOR.DataBind();
                ddlTOR.Items.Insert(0, new ListItem("<-- Select -->", ""));
            }

            string sql = "select bid_no,bid_revision from bid_no where bid_no='" + hidBidno.Value + "' and bid_revision =(select max(bid_revision) from bid_no where bid_no='" + hidBidno.Value + "' )";
            DataTable dtBid = zdb.ExecSql_DataTable(sql, zconn);
            if (dtBid.Rows.Count > 0)
            {
                hidRev.Value = dtBid.Rows[0]["bid_revision"].ToString();
            }
            ddlDoctype.Items.Clear();

            string sqldoc = "select distinct doctype_code,doctype_name from ps_m_doctype where is_select_review =1";
            DataTable dtDoc = zdb.ExecSql_DataTable(sqldoc, zconn);
            if (dtDoc.Rows.Count > 0)
            {
                ddlDoctype.DataSource = dtDoc;
                ddlDoctype.DataTextField = "doctype_name";
                ddlDoctype.DataValueField = "doctype_code";
                ddlDoctype.DataBind();
                ddlDoctype.Items.Insert(0, new ListItem("<-- Select -->", ""));

            }

            ddlAllDoc.Items.Clear();
            string sqldocall = "select distinct doctype_code,doctype_name from ps_m_doctype ";
            DataTable dtDocall = zdb.ExecSql_DataTable(sqldocall, zconn);
            if (dtDocall.Rows.Count > 0)
            {
                ddlAllDoc.DataSource = dtDocall;
                ddlAllDoc.DataTextField = "doctype_name";
                ddlAllDoc.DataValueField = "doctype_code";
                ddlAllDoc.DataBind();
                ddlAllDoc.Items.Insert(0, new ListItem("<-- Select -->", ""));

            }
            
        }
        private void bind_gvSN()
        {
            string sql = @"select * 
,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk 
from ps_t_mytask_doc td
left join ps_m_doctype md on td.doctype_code = md.doctype_code
where is_select_dashboard=1";
            sql += " order by td.doctype_code";
            DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
            if (dt.Rows.Count > 0)
            {
                gvDocSub.DataSource = dt;
                gvDocSub.DataBind();
            }
            else
            {
                gvDocSub.DataSource = null;
                gvDocSub.DataBind();
            }
            wucComment_Att.BindData("", hidBidno.Value, hidLogin.Value, false);

        }
        private void bind_gv0()
        {
            string sql = "";
            if (ddlTOR.SelectedValue != "")
            {
                sql = @"select distinct [article_name]
								  ,[bid_no]
								  ,[bid_revision]
								  ,[tor_temp_code]
								  ,[vol_no]
								  ,[section_name]
								  ,[doctype_code]
                                ,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = dt.doctype_code) as doctype_name
                                ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
								 from ps_t_bid_doc_tor dt where bid_no='" + hidBidno.Value + "' and tor_temp_code='" + ddlTOR.SelectedValue + "'";
            }
            else
            {
                sql = @"select distinct [article_name]
								  ,[bid_no]
								  ,[bid_revision]
								  ,[tor_temp_code]
								  ,[vol_no]
								  ,[section_name]
								  ,[doctype_code]
                                ,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = dt.doctype_code) as doctype_name
                                ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
								 from ps_t_bid_doc_tor dt where bid_no='" + hidBidno.Value + "'";
            }

            sql += " order by vol_no";
            DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
            if (dt.Rows.Count > 0)
            {
                if (hidMode.Value == "edit") ddlTOR.SelectedValue = dt.Rows[0]["tor_temp_code"].ToString();

                gvTemplate.DataSource = dt;
                gvTemplate.DataBind();
            }
            else
            {

                gvTemplate.DataSource = null;
                gvTemplate.DataBind();

            }
            wucComment_Att.BindData("", hidBidno.Value, hidLogin.Value, false);
        }
        private void bind_gvTOR()
        {
            string sql = @"select * ,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = tot.doctype_code) as doctype_name  from ps_t_bid_doc_tor_other tot where bid_no='" + hidBidno.Value + "' and bid_revision ='" + hidRev.Value + "'";
            DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
            gvDocNew.DataSource = dt;
            gvDocNew.DataBind();
        }
        protected void ddlTOR_SelectedIndexChanged(object sender, EventArgs e)
        {
            string sql = "";
            if (ddlTOR.SelectedValue != "")
            {
                sql = @"select distinct article_name,tor_temp_code,vol_no,section_name,bd.doctype_code,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = bd.doctype_code) as doctype_name
  ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = bd.tor_temp_code) as tor_temp_name
                    from ps_m_bid_document bd left join ps_t_mytask_doc td on bd.doctype_code = td.doctype_code where tor_temp_code = '" + ddlTOR.SelectedValue + "'";
            }
            else
            {
                sql = @"select distinct article_name,tor_temp_code,vol_no,section_name,bd.doctype_code,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = bd.doctype_code) as doctype_name
  ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = bd.tor_temp_code) as tor_temp_name
                    from ps_m_bid_document bd left join ps_t_mytask_doc td on bd.doctype_code = td.doctype_code ";
            }
            sql += " order by vol_no";
            DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
            gvTemplate.DataSource = dt;
            gvTemplate.DataBind();
        }

        protected void gvTemplate_RowCommand(object sender, GridViewCommandEventArgs e)
        {
            if (e.CommandName == "viewdoc")
            {
                string sql = "";
                int i = System.Convert.ToInt32(e.CommandArgument);
                GridView gv = ((GridView)gvTemplate.Rows[i].FindControl("gvDoc"));
                string doctype = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                string TemplateCode = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;
                string TemplateName = ((Label)gvTemplate.Rows[i].FindControl("lbDoc_Torname")).Text;

          
                string doctype_code = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                string tor_code = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;
                string section = ((Label)gvTemplate.Rows[i].FindControl("lbSection")).Text;
                string vol = ((Label)gvTemplate.Rows[i].FindControl("lbVol")).Text;
                string article = ((Label)gvTemplate.Rows[i].FindControl("lbArticle")).Text;

                if (gv.Visible == false)
                {
                    try
                    {
                        gv.Visible = true;
                        sql = @" select * from ps_t_mytask_doc  where bid_no='" + hidBidno.Value + "' and doctype_code ='" + doctype_code + "' ";
                        DataTable dttask = zdb.ExecSql_DataTable(sql, zconn);
                        //if (dttask.Rows.Count > 0)
                        //{
                        foreach (DataRow dr in dttask.Rows)
                        {
                            sql = @"select * from ps_t_bid_doc_tor dt where  bid_no='" + hidBidno.Value + "' and  tor_temp_code ='" + tor_code + "' and doctype_code ='" + doctype_code + "' and doc_nodeid ='" + dr["doc_nodeid"].ToString() + "'";
                            DataTable dttor = zdb.ExecSql_DataTable(sql, zconn);
                            if (dttor.Rows.Count > 0)
                            {

                            }
                            else
                            {
                                sql = @"INSERT INTO ps_t_bid_doc_tor
                               ([bid_no]
                          ,[bid_revision]
                          ,[tor_temp_code]
                          ,[vol_no]
                          ,[section_name]
                          ,[article_name]
                          ,[doctype_code]
                          ,[page_no_format]
                          ,[is_stamp_bidno]
                          ,[doc_nodeid]
                          ,[doc_name]
                          ,[doc_sheetno]
                          ,[doc_status]
                          ,[doc_update]
                          ,[is_verify]
                          ,[verify_date]
                          ,[created_by]
                          ,[created_datetime]
                          ,[updated_by]
                          ,[updated_datetime])
                         VALUES
                               ('" + hidBidno.Value + @"'
                               ,'" + hidRev.Value + @"'
                               ,'" + tor_code + @"'
                               ,'" + vol + @"'
                               ,'" + section + @"'
                               ,'" + article + @"'
                               ,'" + doctype_code + @"'
                               ,''
                               ,'0'
                               ,'" + dr["doc_nodeid"].ToString() + @"'
                               ,'" + dr["doc_name"].ToString() + @"'
                               ,'" + dr["doc_sheetno"].ToString() + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + hidLogin.Value + @"'
                               ,GETDATE()
                               ,'" + hidLogin.Value + @"'
                               ,GETDATE())";
                                zdb.ExecNonQuery(sql, zconn);
                            }
                        }
                        // }

                        sql = @"select distinct doc_nodeid,[bid_no]
                                                  ,[bid_revision]
                                                  ,[tor_temp_code]
                                                  ,[vol_no]
                                                  ,[section_name]
                                                  ,[article_name]
                                                  ,[doctype_code]
                                                  ,[page_no_format]
                                                  ,[is_stamp_bidno]
                                                  ,[depart_position_name]
                                                  ,[section_position_name]
                                                  ,[doc_name]
                                                  ,[doc_sheetno]
                                                  ,[doc_status]
                                                  ,[doc_update]
                                                  ,[is_verify]
                                                  ,[verify_date]
                                                  ,[created_by]
                                                  ,[created_datetime]
                                                  ,[updated_by]
                                                  ,[updated_datetime]  
                        ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
                         ,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk  
                        from ps_t_bid_doc_tor dt
                                         where bid_no = '" + hidBidno.Value + @"'
                                         and tor_temp_code = '" + TemplateCode + @"'
                                         and doctype_code = '" + doctype + @"'  and(doc_nodeid is not null and doc_nodeid <> '')";
                        #region

                        //             sql = @"		 select distinct doc_nodeid, bid_no, doc_status,is_verify,verify_date,doctype_code,doc_name as doctype_name,tor_temp_name,tor_temp_code,doc_sheetno,doc_namelnk
                        // from(select bid_no, doc_status,is_verify,verify_date,doctype_code,doc_nodeid,doc_name,tor_temp_code
                        //    ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name,doc_sheetno
                        //  ,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk  
                        //  from ps_t_bid_doc_tor dt
                        //					union
                        //	  select bid_no, '' as doc_status,'0' as is_verify,'' as verify_date,doctype_code,doc_nodeid,doc_name
                        //	  ,'" + TemplateCode + @"' as tor_temp_code  ,'"+ TemplateName + @"' as tor_temp_name,doc_sheetno
                        //	  		    ,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk  
                        //	   from ps_t_mytask_doc td
                        //) as tbb 
                        //where bid_no = '" + hidBidno.Value + @"'
                        //                     and tor_temp_code = '" + TemplateCode + @"'
                        //                     and doctype_code = '" + doctype + @"'  and(doc_nodeid is not null and doc_nodeid <> '')";

                        //             string sql = @"	select *,(CASE WHEN dt.doc_nodeid ='' THEN td.doc_nodeid WHEN dt.doc_nodeid is null THEN td.doc_nodeid ELSE dt.doc_nodeid END) as doc_nodeid, ( CASE WHEN dt.doc_name='' THEN td.doc_name ELSE dt.doc_name END  ) as doc_name_join ,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = dt.doctype_code) as doctype_name,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
                        //                     ,'<a id=''docAtt_' + convert(nvarchar,( CASE WHEN dt.doc_nodeid='' THEN td.doc_nodeid WHEN dt.doc_nodeid is null THEN td.doc_nodeid  ELSE dt.doc_nodeid END  )) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar,( CASE WHEN dt.doc_nodeid='' THEN td.doc_nodeid WHEN dt.doc_nodeid is null THEN td.doc_nodeid  ELSE dt.doc_nodeid END  )) +''' target=''_blank''>' + ( CASE WHEN dt.doc_name='' THEN td.doc_name ELSE dt.doc_name END  ) + '</a>' as doc_namelnk  
                        //                 from ps_t_bid_doc_tor dt 
                        //left join ps_t_mytask_doc td on dt.doctype_code = td.doctype_code 
                        //                     where dt.bid_no = '" + hidBidno.Value + "' and  (td.bid_no ='" + hidBidno.Value + @"' or td.bid_no is null )
                        //                     and tor_temp_code ='" + TemplateCode + @"' 
                        //                     and dt.doctype_code ='" + doctype + @"' and ( dt.doc_nodeid  is not null or td.doc_nodeid is not null or  dt.doc_nodeid <> 0) ";
                        #endregion

                        DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
                        if (dt.Rows.Count > 0)
                        {
                            gv.DataSource = dt;
                            gv.DataBind();
                        }
                    }
                    catch (Exception ex)
                    {

                        LogHelper.WriteEx(ex);
                    }
                }
                else
                {
                    gv.Visible = false;
                }

            }
        }

        protected void btnSend_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = (Button)sender;
                GridViewRow gvr = btn.NamingContainer as GridViewRow;
                string strsql = "";
                if (hidSect.Value == "หสวส-ส." || hidSect.Value == "หบวส-ส.")
                {
                    MailNoti mail = new MailNoti();
                    mail.SendMailTOR(hidLogin.Value, hidBidno.Value, "", "", "1");
                }
                else
                {
                    string[] sp = btn.ClientID.Split('_');
                    string rows = "";
                    string sql = "";
                    //string filename = "";
                    int i = 0;
                    if (sp.Length > 3)
                    {
                        rows = sp[2];
                    }
                    i = (rows == "" ? 0 : Convert.ToInt32(rows));
                   

                    string tor_name = ((Label)gvr.FindControl("gvDoc_Torname")).Text;
                    string docid = ((Label)gvr.FindControl("gvDoc_Docnoid")).Text;
                    string sheetno = ((Label)gvr.FindControl("gvDoc_Sheetno")).Text;
                    string docname = ((Label)gvr.FindControl("gvDoc_DocName")).Text;
                    string doctype_code = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                    string tor_code = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;
                    string section = ((Label)gvTemplate.Rows[i].FindControl("lbSection")).Text;
                    string vol = ((Label)gvTemplate.Rows[i].FindControl("lbVol")).Text;
                    string article = ((Label)gvTemplate.Rows[i].FindControl("lbArticle")).Text;

                    strsql = @"update ps_t_bid_doc_tor set doc_status = 'RECHECKED', doc_update=GETDATE() where bid_no = '" + hidBidno.Value + @"' 
and tor_temp_code='" + tor_code + "' and doctype_code ='" + doctype_code + "' and doc_nodeid ='" + docid + "'";

                    MailNoti mail = new MailNoti();
                    mail.SendMailTOR(hidLogin.Value, hidBidno.Value, tor_name, section,"");
                    zdb.ExecNonQuery(strsql, zconn);
                    defalutTemplate();
                    //bind_gv0();
                }

                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Send Successfully.');", true);

            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }

        protected void cbCheck_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                CheckBox cb = (CheckBox)sender;
                GridViewRow gvr = cb.NamingContainer as GridViewRow;
                string strsql = "";
                if (hidSect.Value == "หสวส-ส." || hidSect.Value == "หบวส-ส.")
                {
                    string doccode = ((Label)gvr.FindControl("gvDocSub_Name")).Text;
                    string docnodeid = ((Label)gvr.FindControl("gvDocSub_Docnoid")).Text;
                    string sql = @"  update ps_t_mytask_doc set is_verify='1'
								 ,verify_by='" + hidLogin.Value + @"'
								 ,verify_position= '" + hidMc.Value + @"'
								 ,verify_datetime =getdate()
                where bid_no= '" + hidBidno.Value + "' and bid_revision= '" + hidRev.Value + "' and doctype_code= '" + doccode + "' and doc_nodeid= '" + docnodeid + "'";
                    zdb.ExecNonQuery(sql, zconn);
                    bind_gvSN();
                }
                else
                {
                    string[] sp = cb.ClientID.Split('_');
                    string rows = "";
                    string sql = "";
                    int i = 0;
                    if (sp.Length > 3)
                    {
                        rows = sp[2];
                    }
                    i = (rows == "" ? 0 : Convert.ToInt32(rows));

                    string tor_name = ((Label)gvr.FindControl("gvDoc_Torname")).Text;
                    string docid = ((Label)gvr.FindControl("gvDoc_Docnoid")).Text;
                    string sheetno = ((Label)gvr.FindControl("gvDoc_Sheetno")).Text;
                    string docname = ((Label)gvr.FindControl("gvDoc_DocName")).Text;
                    string doctype_code = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                    string tor_code = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;

                    string vol = ((Label)gvTemplate.Rows[i].FindControl("lbVol")).Text;
                    string section = ((Label)gvTemplate.Rows[i].FindControl("lbSection")).Text;
                    string article = ((Label)gvTemplate.Rows[i].FindControl("lbArticle")).Text;

                    strsql = @"update ps_t_bid_doc_tor set doc_status = 'VERIFIED', verify_date=GETDATE(),is_verify=1 where bid_no = '" + hidBidno.Value + @"' 
and tor_temp_code='" + tor_code + "' and doctype_code ='" + doctype_code + "' and doc_nodeid ='" + docid + "'";
                    zdb.ExecNonQuery(strsql, zconn);
                    defalutTemplate();
                    //bind_gv0();

                }
               
              
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true);
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        protected void btnUploadDoc_Click(object sender, EventArgs e)
        {

            clsMyTaskBid objDb = new clsMyTaskBid();

            Button btn = (Button)sender;
            string[] sp = btn.ClientID.Split('_');
            string rows = "";
            string sql = "";
            //string filename = "";
            int i = 0;
            if (sp.Length > 3)
            {
                rows = sp[2];
            }
            i = (rows == "" ? 0 : Convert.ToInt32(rows));
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            Node jobFoldernode = new Node();
            Node DeptFoldernode = new Node();
            Node SectionFoldernode = new Node();
            Node Doctypenode = new Node();
            FileUpload filename = (FileUpload)gvr.FindControl("FileUpload1");
            string docname = "";
         
            string doctype_name = "";
            string doctype_code = "";
            string tor_code = "";
            string docid = "";
            string docnodid = "";
            string DoctypeFoldername = "";
            if (hidSect.Value == "หสวส-ส." || hidSect.Value == "หบวส-ส.")
            {
                docname = ((Label)gvr.FindControl("gvDocSub_DocName")).Text;
                doctype_name = ((Label)gvr.FindControl("gvDocSub_NameFull")).Text;
                doctype_code = ((Label)gvr.FindControl("gvDocSub_Name")).Text;
                docnodid = ((Label)gvr.FindControl("gvDocSub_Docnoid")).Text;
                DoctypeFoldername = doctype_code + " - " + doctype_name;
            }
            else
            {
                docname = ((Label)gvr.FindControl("gvDoc_DocName")).Text;
                doctype_name = ((Label)gvTemplate.Rows[i].FindControl("lbDoc_Name")).Text;
                 doctype_code = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                 tor_code = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;
                docnodid = ((Label)gvr.FindControl("gvDoc_Docnoid")).Text;
                
                DoctypeFoldername = doctype_code + " - " + doctype_name;
            }

          
            OTFunctions ot = new OTFunctions();
            jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidBidno.Value);
            if (jobFoldernode == null)
            {
                jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidBidno.Value);
            }
            DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
            if (DeptFoldernode == null)
            {
                DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
            }
            SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
            if (SectionFoldernode == null)
            {
                SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
            }
            Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            if (Doctypenode == null)
            {
                Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            }
            if (filename.FileName != "")
            {
                docid = ot.uploadDoc(Doctypenode.ID.ToString(), filename.FileName, docname, filename.FileBytes, "").ToString();
                try
                {
                    string strsql = "";
                    if (hidSect.Value == "หสวส-ส." || hidSect.Value == "หบวส-ส.")
                    {
                        bind_gvSN();
                    }
                    else
                    {
                        strsql = @"update ps_t_bid_doc_tor set doc_nodeid='"+ docid + "',doc_status = 'UPDATED', doc_update=GETDATE() where bid_no = '" + hidBidno.Value + @"' 
and tor_temp_code='" + tor_code + "' and doctype_code ='" + doctype_code + "' and doc_nodeid ='" + docnodid + "'";
                        zdb.ExecNonQuery(strsql, zconn);
                        defalutTemplate();
                        //bind_gv0();
                    }
             
            
                }
                catch (Exception ex)
                {

                    LogHelper.WriteEx(ex);
                }
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Upload Successfully.');", true);
              
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Choose File.');", true);
            }
        }
        protected void btnSelectTemp_Click(object sender, EventArgs e)
        {
            string sql = "";

            if (ddlTOR.SelectedValue != "")
            {
                sql = @"select distinct [article_name]
								  ,[bid_no]
								  ,[bid_revision]
								  ,[tor_temp_code]
								  ,[vol_no]
								  ,[section_name]
								  ,[doctype_code]
                                 ,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = dt.doctype_code) as doctype_name
                                ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
								 from ps_t_bid_doc_tor dt where  bid_no='" + hidBidno.Value + "' and  tor_temp_code ='" + ddlTOR.SelectedValue + "'";
                DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
                if (dt.Rows.Count > 0)
                {
                    dt.DefaultView.Sort = " vol_no";
                    gvTemplate.DataSource = dt;
                    gvTemplate.DataBind();
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Successfully.');", true);
                }
                else
                {
                    sql = @"select*,(select doctype_name from ps_m_doctype where ps_m_doctype.doctype_code = bd.doctype_code) as doctype_name
                            ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = bd.tor_temp_code) as tor_temp_name
                    from ps_m_bid_document bd left join ps_t_mytask_doc td on bd.doctype_code = td.doctype_code where tor_temp_code ='" + ddlTOR.SelectedValue + "'";
                    DataTable dt2 = zdb.ExecSql_DataTable(sql, zconn);
                    if (dt2.Rows.Count > 0)
                    {
                        try
                        {
                            sql = "delete from ps_t_bid_doc where bid_no='"+ hidBidno.Value + "' and bid_revision='"+ hidRev.Value + "' and tor_temp_code <>'" + ddlTOR.SelectedValue + "'";
                            zdb.ExecNonQuery(sql, zconn);

                            sql = @"INSERT INTO ps_t_bid_doc
                               ([bid_no]
                              ,[bid_revision]
                              ,[tor_temp_code]
                              ,[biddoc_node_id])
                         VALUES
                               ('" + hidBidno.Value + @"'
                               ,'" + hidRev.Value + @"'
                               ,'" + ddlTOR.SelectedValue + @"'
                               ,'" + DBNull.Value + "')";
                            zdb.ExecNonQuery(sql, zconn);

                            sql = "delete from ps_t_bid_doc_tor where bid_no='" + hidBidno.Value + "' and bid_revision='" + hidRev.Value + "' and tor_temp_code <>'" + ddlTOR.SelectedValue + "'";
                            zdb.ExecNonQuery(sql, zconn);

                            foreach (DataRow dr in dt2.Rows)
                            {
                                sql = @"INSERT INTO ps_t_bid_doc_tor
                               ([bid_no]
                          ,[bid_revision]
                          ,[tor_temp_code]
                          ,[vol_no]
                          ,[section_name]
                          ,[article_name]
                          ,[doctype_code]
                          ,[page_no_format]
                          ,[is_stamp_bidno]
                          ,[doc_nodeid]
                          ,[doc_name]
                          ,[doc_sheetno]
                          ,[doc_status]
                          ,[doc_update]
                          ,[is_verify]
                          ,[verify_date]
                          ,[created_by]
                          ,[created_datetime]
                          ,[updated_by]
                          ,[updated_datetime])
                         VALUES
                               ('" + hidBidno.Value + @"'
                               ,'" + hidRev.Value + @"'
                               ,'" + dr["tor_temp_code"].ToString() + @"'
                               ,'" + dr["vol_no"].ToString() + @"'
                               ,'" + dr["section_name"].ToString() + @"'
                               ,'" + dr["article_name"].ToString() + @"'
                               ,'" + dr["doctype_code"].ToString() + @"'
                               ,'" + dr["page_no_format"].ToString() + @"'
                               ,'" + dr["is_stamp_bidno"].ToString() + @"'
                               ,'" + dr["doc_nodeid"].ToString() + @"'
                               ,'" + dr["doc_name"].ToString() + @"'
                               ,'" + dr["doc_sheetno"].ToString() + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + dr["created_by"].ToString() + @"'
                               ,'" + dr["created_datetime"].ToString() + @"'
                               ,'" + dr["updated_by"].ToString() + @"'
                               ,'" + dr["updated_datetime"].ToString() + @"')";
                                zdb.ExecNonQuery(sql, zconn);
                            }
                            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Successfully.');", true);
                        }
                        catch (Exception ex)
                        {

                            LogHelper.WriteEx(ex);
                        }
                        defalutTemplate();
                        //bind_gv0();
                    }
                }


            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Select Template.');", true);
            }

        }
        protected void gvDoc_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Button btnSend = (Button)e.Row.FindControl("btnSend");
                Button btnUploadDoc = (Button)e.Row.FindControl("btnUploadDoc");
                FileUpload FileUpload1 = (FileUpload)e.Row.FindControl("FileUpload1");
                CheckBox cbCheck = (CheckBox)e.Row.FindControl("cbCheck");
                Label gvDoc_Verify = (Label)e.Row.FindControl("gvDoc_Verify");
                DateTime dt = new DateTime();

                if (drv["is_verify"].ToString() == "1")
                {
                    btnSend.Enabled = false;
                    btnUploadDoc.Enabled = false;
                    FileUpload1.Enabled = false;
                    cbCheck.Checked = true;
                    cbCheck.Enabled = false;
                    dt = Convert.ToDateTime(drv["verify_date"]);
                    string verify = "Verified" + "<Br/>" + dt.ToString("dd/MM/yyyy");
                    gvDoc_Verify.Text = verify;
                }
                if (hidMode.Value == "apv")
                {
                    btnSend.Enabled = false;
                    btnUploadDoc.Enabled = false;
                    FileUpload1.Enabled = false;
                    cbCheck.Enabled = false;
                    if (drv["is_verify"].ToString() == "1")
                    {
                        cbCheck.Checked = true;
                        dt = Convert.ToDateTime(drv["verify_date"]);
                        string verify = "Verified" + "<Br/>" + dt.ToString("dd/MM/yyyy");
                        gvDoc_Verify.Text = verify;
                    }
                }
            }
        }
        protected void btnAddData_Click(object sender, EventArgs e)
        {
            Node jobFoldernode = new Node();
            Node DeptFoldernode = new Node();
            Node SectionFoldernode = new Node();
            Node Doctypenode = new Node();
            string docid = "";
            string DoctypeFoldername = ddlDoctype.SelectedValue + " - " + ddlDoctype.SelectedItem.Text;
            OTFunctions ot = new OTFunctions();
            jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidBidno.Value);
            if (jobFoldernode == null)
            {
                jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidBidno.Value);
            }
            DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
            if (DeptFoldernode == null)
            {
                DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
            }
            SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
            if (SectionFoldernode == null)
            {
                SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
            }
            Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            if (Doctypenode == null)
            {
                Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            }
            if (FileUpload2.FileName != "")
            {
                docid = ot.uploadDoc(Doctypenode.ID.ToString(), FileUpload2.FileName, FileUpload2.FileName, FileUpload2.FileBytes, "").ToString();

                try
                {
                    string sql = @"INSERT INTO ps_t_bid_doc_tor_other
                               ([bid_no]
                              ,[bid_revision]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_name]
                              ,[approve_date]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                         VALUES
                               ('" + hidBidno.Value + @"'
                               ,'" + hidRev.Value + @"'
                               ,'" + ddlDoctype.SelectedValue + @"'
                               ,'" + docid + @"'
                               ,'" + FileUpload2.FileName + @"'
                               ,'" + Appr_Date.Value + @"'
                               ,'" + hidLogin.Value + @"'
                               ,GETDATE()
                               ,'" + hidLogin.Value + @"'
                               , GETDATE())";
                    zdb.ExecNonQuery(sql, zconn);

                    bind_gvTOR();
                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                }
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Upload Successfully.');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Choose File.');", true);
            }
        }

        protected void imgdel_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton btn = (ImageButton)sender;
            GridViewRow gvr = btn.NamingContainer as GridViewRow;
            CheckBox cb = (CheckBox)gvr.FindControl("gvDocNew_cb");
            string gvDocNew_docid = ((Label)gvr.FindControl("gvDocNew_docid")).Text;
            string gvDocNew_Doccode = ((Label)gvr.FindControl("gvDocNew_Doccode")).Text;
            if (cb.Checked)
            {
                try
                {
                    string sql = @"delete from ps_t_bid_doc_tor_other where bid_no='" + hidBidno.Value + "' and bid_revision='" + hidRev.Value + "' and doc_nodeid ='" + gvDocNew_docid + "' and doctype_code='" + gvDocNew_Doccode + "'";
                    zdb.ExecNonQuery(sql, zconn);
                    DeleteNode(gvDocNew_docid);
                    ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Delete Successfully.');", true);
                    bind_gvTOR();
                }
                catch (Exception ex)
                {
                    LogHelper.WriteEx(ex);
                }
            }
        }
        public void DeleteNode(string strNodeId)
        {
            OTFunctions ot = new OTFunctions();
            Node thisNode = ot.getNodeByID(strNodeId);
            if (thisNode != null)
            {
                ot.deleteNodeID(thisNode.ParentID.ToString(), strNodeId);
            }
        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            clsMyTaskBid objDB = new clsMyTaskBid();
            clsWfPsAPV01 wfp = new clsWfPsAPV01();
            DataTable dt = objDB.getPs_reqbid(hidBidno.Value, hidWorkID.Value, hidRev.Value);
            string requestor_status = "";
            string strProcessID = "";

            if (dt.Rows.Count > 0)
            {
                requestor_status = dt.Rows[0]["requestor_status"].ToString();
                strProcessID = dt.Rows[0]["process_id"].ToString();


            }

            wfp.EmpSubmitWfbid(strProcessID, hidWorkID.Value, hidSubWorkID.Value, hidTaskID.Value);
            objDB.Update_wf_reqbid(hidBidno.Value, strProcessID, hidRev.Value, hidLogin.Value, "");
            //ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Submit Successfully.');", true);

            Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been approved.');</script>");
            string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);
        }

        protected void btnApv_Click(object sender, EventArgs e)
        {
            clsMyTaskBid objDB = new clsMyTaskBid();
            clsWfPsAPV01 wfp = new clsWfPsAPV01();
            DataTable dt = objDB.getPs_reqbid(hidBidno.Value, hidWorkID.Value, hidRev.Value);
            string requestor_status = "";
            string strProcessID = "";

            if (dt.Rows.Count > 0)
            {
                requestor_status = dt.Rows[0]["requestor_status"].ToString();
                strProcessID = dt.Rows[0]["process_id"].ToString();
            }

            wfp.DocReview_Approver(strProcessID, hidWorkID.Value, hidSubWorkID.Value, hidTaskID.Value, "APPROVED", hidLogin.Value);
            objDB.Update_wf_reqbid(hidBidno.Value, strProcessID, hidRev.Value, hidLogin.Value, "");

            Response.Write("<script> alert('The " + hidMode.Value.Trim() + " has been approved.');</script>");
            string xurl_personal_assignment = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            string jScript = "window.top.location.assign(\"" + xurl_personal_assignment + "\");";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "forceParentLoad", jScript, true);

           // ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Approve Successfully.');", true);
        }

        protected void btnRej_Click(object sender, EventArgs e)
        {
            clsMyTaskBid objDB = new clsMyTaskBid();
            clsWfPsAPV01 wfp = new clsWfPsAPV01();
            DataTable dt = objDB.getPs_reqbid(hidBidno.Value, hidWorkID.Value, hidRev.Value);
            string requestor_status = "";
            string strProcessID = "";

            if (dt.Rows.Count > 0)
            {
                requestor_status = dt.Rows[0]["requestor_status"].ToString();
                strProcessID = dt.Rows[0]["process_id"].ToString();

            }

            wfp.DocReview_Approver(strProcessID, hidWorkID.Value, hidSubWorkID.Value, hidTaskID.Value, "REJECTED", hidLogin.Value);
            objDB.Update_wf_reqbid(hidBidno.Value, strProcessID, hidRev.Value, hidLogin.Value, "");
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Reject Successfully.');", true);
        }

        protected void btnUploadAll_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "block";
        }
        protected void ibtnHome_Click(object sender, ImageClickEventArgs e)
        {
            string strUrl = ConfigurationManager.AppSettings["url_personal_assignment"].ToString();
            Response.Redirect(strUrl);
        }
        protected void btnClose_Click(object sender, EventArgs e)
        {
            myModal.Style["display"] = "none";
        }

        protected void btnUploadDoc_Click1(object sender, EventArgs e)
        {
            Node jobFoldernode = new Node();
            Node DeptFoldernode = new Node();
            Node SectionFoldernode = new Node();
            Node Doctypenode = new Node();
            string docid = "";
            string DoctypeFoldername = ddlAllDoc.SelectedValue + " - " + ddlAllDoc.SelectedItem.Text;
            OTFunctions ot = new OTFunctions();
            jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidBidno.Value);
            if (jobFoldernode == null)
            {
                jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidBidno.Value);
            }
            DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
            if (DeptFoldernode == null)
            {
                DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
            }
            SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
            if (SectionFoldernode == null)
            {
                SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
            }
            Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            if (Doctypenode == null)
            {
                Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), DoctypeFoldername);
            }
            if (FileUpload3.FileName != "")
            {
                docid = ot.uploadDoc(Doctypenode.ID.ToString(), FileUpload3.FileName, FileUpload3.FileName, FileUpload3.FileBytes, "").ToString();

                try
                {
                    string sql = "";
                    string sqlchk = @"select * from ps_t_mytask_doc where bid_no='" + hidBidno.Value + "' and bid_revision ='" + hidRev.Value + "' and depart_position_name='" + hidDept.Value + "' and doctype_code ='" + ddlAllDoc.SelectedValue + "' and doc_nodeid = '" + docid + "'";
                    DataTable dt =  zdb.ExecSql_DataTable(sqlchk, zconn);
                  
                    string chk = @"	 select * from ps_t_bid_doc_tor  where bid_no='" + hidBidno.Value + "' and bid_revision ='" + hidRev.Value + "'  and doctype_code ='" + ddlAllDoc.SelectedValue + "' and doc_nodeid = " + docid + "";
                    DataTable dtchk = zdb.ExecSql_DataTable(chk, zconn);
                    if (dtchk.Rows.Count > 0)
                    {

                    }
                    if (dt.Rows.Count > 0)
                    {
                        
                        sql = @"update ps_t_mytask_doc set doc_name='" + FileUpload3.FileName + "',doc_nodeid='" + docid + "',updated_datetime=getdate()   where bid_no='" + hidBidno.Value + "' and bid_revision ='" + hidRev.Value + "' and depart_position_name='" + hidDept.Value + "' and doctype_code ='" + ddlAllDoc.SelectedValue + "' and doc_nodeid = " + docid + "";
                        zdb.ExecNonQuery(sql, zconn);
                    }
                    if (dt.Rows.Count == 0 && dtchk.Rows.Count == 0)
                    {
                         sql = @"INSERT INTO ps_t_mytask_doc
                               ([activity_id]
                              ,[job_no]
                              ,[job_revision]
                              ,[bid_no]
                              ,[bid_revision]
                              ,[schedule_no]
                              ,[schedule_name]
                              ,[depart_position_name]
                              ,[section_position_name]
                              ,[doctype_code]
                              ,[doc_nodeid]
                              ,[doc_revision_no]
                              ,[doc_name]
                              ,[doc_sheetno]
                              ,[doc_desc]
                              ,[created_by]
                              ,[created_datetime]
                              ,[updated_by]
                              ,[updated_datetime])
                         VALUES
                               ('" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'0'
                               ,'" + hidBidno.Value + @"'
                               ,'" + hidRev.Value + @"'
                               ,'0'
                               ,'" + DBNull.Value + @"'
                               ,'" + hidDept.Value + @"'
                               ,'" + hidSect.Value + @"'
                               ,'" + ddlAllDoc.SelectedValue + @"'
                               ,'" + docid + @"'
                               ,'0'
                               ,'" + FileUpload3.FileName + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + hidLogin.Value + @"'
                               ,GETDATE()
                               ,'" + hidLogin.Value + @"'
                               , GETDATE())";
                        zdb.ExecNonQuery(sql, zconn);
                    }

                    defalutTemplate();
                    //bind_gv0();
                    //bind_gvTOR();
                }
                catch (Exception ex)
                {

                    LogHelper.WriteEx(ex);
                }
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Upload Successfully.');", true);
            }
            else
            {
                ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Please Choose File.');", true);
            }
        }

        protected void btnPreview_Click(object sender, EventArgs e)
        {
            myPreview.Style["display"] = "block";
            string xdoc_url = "";
            xdoc_url = "https://" + ConfigurationManager.AppSettings["HOST_NAME"].ToString() + "/tmps/forms/PreviewDocCS?nodeid=" + hidNodeid.Value + " #toolbar=0";
            Response.Headers.Remove("X-Frame-Options");
            Response.AddHeader("X-Frame-Options", "AllowAll");
            ((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);
        }
        protected void btnMerge_Click(object sender, EventArgs e)
        {
            btnPreview.Visible = true;  
            MargePDF margePDF = new MargePDF();
            OTFunctions ot = new OTFunctions();
            MimeType objtype = new MimeType();
            string strdocid = "";

            int i = 0;
            foreach (GridViewRow dgi in gvTemplate.Rows)
            {
                int index = dgi.RowIndex;
                clickRowCommand(index);
                GridView gvDoc = (GridView)dgi.FindControl("gvDoc");
                foreach (GridViewRow gvr in gvDoc.Rows)
                {
                    CheckBox cb = (CheckBox)gvr.FindControl("gvDoc_cb");
                    if (cb.Checked)
                    {
                        string nodeid = ((Label)gvr.FindControl("gvDoc_Docnoid")).Text;
                        byte[] attach = ot.downloadDoc(nodeid);
                        string type = objtype.GetMimeType(attach);
                        if (type == "application/pdf")
                        {
                            strdocid += (strdocid == "" ? "" : ",") + nodeid;
                            i++;
                        }
                        else { }
                    }
                }
            }
            int k = 0;
            MemoryStream[] arrMem = new MemoryStream[i];
            string[] arrValues = strdocid.Split(',');
            foreach (string value in arrValues)
            {
                byte[] attach = ot.downloadDoc(value);
                MemoryStream mem = new MemoryStream(attach);
                arrMem[k] = mem;
                k++;

            }

            Node jobFoldernode = new Node();
            Node DeptFoldernode = new Node();
            Node SectionFoldernode = new Node();
            Node Doctypenode = new Node();
            string strDoc_NodeID = "";
           
            byte[] allAttach = margePDF.margePDF(arrMem);


            jobFoldernode = ot.getNodeByName(strFolderAttTaskNodeId, hidBidno.Value);
            if (jobFoldernode == null)
            {
                jobFoldernode = ot.createFolder(strFolderAttTaskNodeId, hidBidno.Value);
            }
            DeptFoldernode = ot.getNodeByName(jobFoldernode.ID.ToString(), hidDept.Value);
            if (DeptFoldernode == null)
            {
                DeptFoldernode = ot.createFolder(jobFoldernode.ID.ToString(), hidDept.Value);
            }
            SectionFoldernode = ot.getNodeByName(DeptFoldernode.ID.ToString(), hidSect.Value);
            if (SectionFoldernode == null)
            {
                SectionFoldernode = ot.createFolder(DeptFoldernode.ID.ToString(), hidSect.Value);
            }
            Doctypenode = ot.getNodeByName(SectionFoldernode.ID.ToString(), "TORD - TOR Document");
            if (Doctypenode == null)
            {
                Doctypenode = ot.createFolder(SectionFoldernode.ID.ToString(), "TORD - TOR Document");
            }

            string strParent_NodeID = (Doctypenode == null ? "" : Doctypenode.ID.ToString());

            string strFileName = hidBidno.Value + "_" + DateTime.Now.ToString("yyyMMdd") + ".pdf";
            long margeFile_NodeID = ot.uploadDoc(strParent_NodeID, strFileName, strFileName, allAttach, "");
            strDoc_NodeID = margeFile_NodeID.ToString();
            hidNodeid.Value = strDoc_NodeID;
            ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Merge Successfully.');", true);
            //xdoc_url = "https://" + ConfigurationManager.AppSettings["HOST_NAME"].ToString() + "/tmps/forms/PreviewDocCS?nodeid=" + strDoc_NodeID + " #toolbar=0";
            //Response.Headers.Remove("X-Frame-Options");
            //Response.AddHeader("X-Frame-Options", "AllowAll");
            //((HtmlControl)(form1.FindControl("docview"))).Attributes.Add("src", xdoc_url);
            try
            {
                string sql = "";
                sql = "update  ps_t_bid_doc set biddoc_node_id='"+ strDoc_NodeID + "'  where bid_no='" + hidBidno.Value + "' and bid_revision='" + hidRev.Value + "'";
                zdb.ExecNonQuery(sql, zconn);
                
            }
            catch (Exception ex)
            {

                LogHelper.WriteEx(ex);
            }
        }
        protected void clickRowCommand(int i)
        {
                string sql = "";
                GridView gv = ((GridView)gvTemplate.Rows[i].FindControl("gvDoc"));
                string doctype = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                string TemplateCode = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;
                string TemplateName = ((Label)gvTemplate.Rows[i].FindControl("lbDoc_Torname")).Text;


                string doctype_code = ((Label)gvTemplate.Rows[i].FindControl("lbDocCode")).Text;
                string tor_code = ((Label)gvTemplate.Rows[i].FindControl("lbTemplateCode")).Text;
                string section = ((Label)gvTemplate.Rows[i].FindControl("lbSection")).Text;
                string vol = ((Label)gvTemplate.Rows[i].FindControl("lbVol")).Text;
                string article = ((Label)gvTemplate.Rows[i].FindControl("lbArticle")).Text;

                if (gv.Visible == false)
                {
                    try
                    {
                        //gv.Visible = true;
                        sql = @" select * from ps_t_mytask_doc  where bid_no='" + hidBidno.Value + "' and doctype_code ='" + doctype_code + "' ";
                        DataTable dttask = zdb.ExecSql_DataTable(sql, zconn);
                        if (dttask.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dttask.Rows)
                            {
                                sql = @"select * from ps_t_bid_doc_tor dt where  bid_no='" + hidBidno.Value + "' and  tor_temp_code ='" + tor_code + "' and doctype_code ='" + doctype_code + "' and doc_nodeid ='" + dr["doc_nodeid"].ToString() + "'";
                                DataTable dttor = zdb.ExecSql_DataTable(sql, zconn);
                                if (dttor.Rows.Count > 0)
                                {

                                }
                                else
                                {
                                    sql = @"INSERT INTO ps_t_bid_doc_tor
                               ([bid_no]
                          ,[bid_revision]
                          ,[tor_temp_code]
                          ,[vol_no]
                          ,[section_name]
                          ,[article_name]
                          ,[doctype_code]
                          ,[page_no_format]
                          ,[is_stamp_bidno]
                          ,[doc_nodeid]
                          ,[doc_name]
                          ,[doc_sheetno]
                          ,[doc_status]
                          ,[doc_update]
                          ,[is_verify]
                          ,[verify_date]
                          ,[created_by]
                          ,[created_datetime]
                          ,[updated_by]
                          ,[updated_datetime])
                         VALUES
                               ('" + hidBidno.Value + @"'
                               ,'" + hidRev.Value + @"'
                               ,'" + tor_code + @"'
                               ,'" + vol + @"'
                               ,'" + section + @"'
                               ,'" + article + @"'
                               ,'" + doctype_code + @"'
                               ,''
                               ,'0'
                               ,'" + dr["doc_nodeid"].ToString() + @"'
                               ,'" + dr["doc_name"].ToString() + @"'
                               ,'" + dr["doc_sheetno"].ToString() + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + DBNull.Value + @"'
                               ,'" + hidLogin.Value + @"'
                               ,GETDATE()
                               ,'" + hidLogin.Value + @"'
                               ,GETDATE())";
                                    zdb.ExecNonQuery(sql, zconn);
                                }
                            }
                        }

                        sql = @"select distinct doc_nodeid,[bid_no]
                                      ,[bid_revision]
                                      ,[tor_temp_code]
                                      ,[vol_no]
                                      ,[section_name]
                                      ,[article_name]
                                      ,[doctype_code]
                                      ,[page_no_format]
                                      ,[is_stamp_bidno]
                                      ,[depart_position_name]
                                      ,[section_position_name]
                                      ,[doc_name]
                                      ,[doc_sheetno]
                                      ,[doc_status]
                                      ,[doc_update]
                                      ,[is_verify]
                                      ,[verify_date]
                                      ,[created_by]
                                      ,[created_datetime]
                                      ,[updated_by]
                                      ,[updated_datetime]  
                        ,(select tor_temp_name from ps_m_tor_template tt where tt.tor_temp_code = dt.tor_temp_code) as tor_temp_name
                         ,'<a id=''docAtt_' + convert(nvarchar,doc_nodeid) + '''class=''cls_view_doc'' href=''/tmps/forms/PreviewDocCS.aspx?nodeid=' + convert(nvarchar, doc_nodeid) +''' target=''_blank''>' + doc_name + '</a>' as doc_namelnk  
                        from ps_t_bid_doc_tor dt
                                         where bid_no = '" + hidBidno.Value + @"'
                                         and tor_temp_code = '" + TemplateCode + @"'
                                         and doctype_code = '" + doctype + @"'  and(doc_nodeid is not null and doc_nodeid <> '')";

                        DataTable dt = zdb.ExecSql_DataTable(sql, zconn);
                        if (dt.Rows.Count > 0)
                        {
                            gv.DataSource = dt;
                            gv.DataBind();
                        }
                    }
                    catch (Exception ex)
                    {

                        LogHelper.WriteEx(ex);
                    }
                }
                else
                {
                    gv.Visible = false;
                }

        }

        protected void btnSaveCom_Click(object sender, EventArgs e)
        {
            //if (txtComment.Text != "")
            //{
            //    EmpInfoClass empinfo = new EmpInfoClass();
            //    var emp = empinfo.getInFoByEmpID(hidLogin.Value);
            //    string sql = "";
            //    sql = " insert into wf_comment_att (process_id, req_no, comment, uploaded_by, uploaded_datetime) " +
            //        " values " +
            //        " ( " +
            //        " '" + hidBidno.Value + "' , " +
            //        " '' , " +
            //        " '" + txtComment.Text.Trim().Replace("'", "''") + "' , " +

            //        " '" + emp.SNAME + " (" + emp.MC_SHORT + ")', " +
            //         " GETDATE() " +
            //        " ) ";
            //    zdb.ExecNonQuery(sql, zconn);
            //    wucComment_Att.BindData("", hidBidno.Value, hidLogin.Value, false);
            //    txtComment.Text = "";
            //}
            //ClientScript.RegisterStartupScript(Page.GetType(), "MessagePopUp", "alert('Save Successfully.');", true);
        }
        protected void btnColsePop_Click(object sender, EventArgs e)
        {
            myPreview.Style["display"] = "none";
        }

        public class MimeType
        {
            private static readonly byte[] BMP = { 66, 77 };
            private static readonly byte[] DOC = { 208, 207, 17, 224, 161, 177, 26, 225 };
            private static readonly byte[] EXE_DLL = { 77, 90 };
            private static readonly byte[] GIF = { 71, 73, 70, 56 };
            private static readonly byte[] ICO = { 0, 0, 1, 0 };
            private static readonly byte[] JPG = { 255, 216, 255 };
            private static readonly byte[] MP3 = { 255, 251, 48 };
            private static readonly byte[] OGG = { 79, 103, 103, 83, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0 };
            private static readonly byte[] PDF = { 37, 80, 68, 70, 45, 49, 46 };
            private static readonly byte[] PNG = { 137, 80, 78, 71, 13, 10, 26, 10, 0, 0, 0, 13, 73, 72, 68, 82 };
            private static readonly byte[] RAR = { 82, 97, 114, 33, 26, 7, 0 };
            private static readonly byte[] SWF = { 70, 87, 83 };
            private static readonly byte[] TIFF = { 73, 73, 42, 0 };
            private static readonly byte[] TORRENT = { 100, 56, 58, 97, 110, 110, 111, 117, 110, 99, 101 };
            private static readonly byte[] TTF = { 0, 1, 0, 0, 0 };
            private static readonly byte[] WAV_AVI = { 82, 73, 70, 70 };
            private static readonly byte[] WMV_WMA = { 48, 38, 178, 117, 142, 102, 207, 17, 166, 217, 0, 170, 0, 98, 206, 108 };
            private static readonly byte[] ZIP_DOCX = { 80, 75, 3, 4 };

            public string GetMimeType(byte[] file)
            {
                string mime = "";

                //Get the file extension
                string extension = "";

                //Get the MIME Type
                if (file.Take(2).SequenceEqual(BMP))
                {
                    mime = "image/bmp";
                }
                else if (file.Take(8).SequenceEqual(DOC))
                {
                    mime = "application/msword";
                }
                else if (file.Take(2).SequenceEqual(EXE_DLL))
                {
                    mime = "application/x-msdownload"; //both use same mime type
                }
                else if (file.Take(4).SequenceEqual(GIF))
                {
                    mime = "image/gif";
                }
                else if (file.Take(4).SequenceEqual(ICO))
                {
                    mime = "image/x-icon";
                }
                else if (file.Take(3).SequenceEqual(JPG))
                {
                    mime = "image/jpeg";
                }
                else if (file.Take(3).SequenceEqual(MP3))
                {
                    mime = "audio/mpeg";
                }
                else if (file.Take(14).SequenceEqual(OGG))
                {
                    if (extension == ".OGX")
                    {
                        mime = "application/ogg";
                    }
                    else if (extension == ".OGA")
                    {
                        mime = "audio/ogg";
                    }
                    else
                    {
                        mime = "video/ogg";
                    }
                }
                else if (file.Take(7).SequenceEqual(PDF))
                {
                    mime = "application/pdf";
                }
                else if (file.Take(16).SequenceEqual(PNG))
                {
                    mime = "image/png";
                }
                else if (file.Take(7).SequenceEqual(RAR))
                {
                    mime = "application/x-rar-compressed";
                }
                else if (file.Take(3).SequenceEqual(SWF))
                {
                    mime = "application/x-shockwave-flash";
                }
                else if (file.Take(4).SequenceEqual(TIFF))
                {
                    mime = "image/tiff";
                }
                else if (file.Take(11).SequenceEqual(TORRENT))
                {
                    mime = "application/x-bittorrent";
                }
                else if (file.Take(5).SequenceEqual(TTF))
                {
                    mime = "application/x-font-ttf";
                }
                else if (file.Take(4).SequenceEqual(WAV_AVI))
                {
                    mime = extension == ".AVI" ? "video/x-msvideo" : "audio/x-wav";
                }
                else if (file.Take(16).SequenceEqual(WMV_WMA))
                {
                    mime = extension == ".WMA" ? "audio/x-ms-wma" : "video/x-ms-wmv";
                }
                else if (file.Take(4).SequenceEqual(ZIP_DOCX))
                {
                    mime = extension == ".DOCX" ? "application/vnd.openxmlformats-officedocument.wordprocessingml.document" : "application/x-zip-compressed";
                }

                return mime;
            }
        }

        protected void gvDocSub_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)e.Row.DataItem;
                Button btnSend = (Button)e.Row.FindControl("btnSend");
                Button btnUploadDoc = (Button)e.Row.FindControl("btnUploadDoc");
                FileUpload FileUpload1 = (FileUpload)e.Row.FindControl("FileUpload1");
                CheckBox cbCheck = (CheckBox)e.Row.FindControl("cbCheck");
                Label gvDoc_Verify = (Label)e.Row.FindControl("gvDocSub_Verify");
                DateTime dt = new DateTime();

                if (drv["is_verify"].ToString() == "1")
                {
                    btnSend.Enabled = false;
                    btnUploadDoc.Enabled = false;
                    FileUpload1.Enabled = false;
                    cbCheck.Checked = true;
                    cbCheck.Enabled = false;
                    dt = Convert.ToDateTime(drv["verify_datetime"]);
                    string verify = "Verified" + "<Br/>" + dt.ToString("dd/MM/yyyy");
                    gvDoc_Verify.Text = verify;
                }
                if (hidMode.Value == "apv")
                {
                    btnSend.Enabled = false;
                    btnUploadDoc.Enabled = false;
                    FileUpload1.Enabled = false;
                    cbCheck.Enabled = false;
                    if (drv["is_verify"].ToString() == "1")
                    {
                        cbCheck.Checked = true;
                        dt = Convert.ToDateTime(drv["verify_datetime"]);
                        string verify = "Verified" + "<Br/>" + dt.ToString("dd/MM/yyyy");
                        gvDoc_Verify.Text = verify;
                    }
                }
            }
        }
    }
}