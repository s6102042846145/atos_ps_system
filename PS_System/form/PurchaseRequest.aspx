﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PurchaseRequest.aspx.cs" Inherits="PS_System.form.PurchaseRequest" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        #Text3 {
            width: 302px;
        }

        #Txt_Sub {
            width: 329px;
        }

        #Txt_Work {
            height: 66px;
            width: 1064px;
        }

        #Text1 {
            width: 27px;
        }

        .ddlstyle {
            width: 200px;
        }

        .auto-style7 {
            width: 75%;
            height: 29px;
        }
        .auto-style11 {
            height: 29px;
        }
        .auto-style12 {
            height: 24px;
        }
        .auto-style14 {
            width: 700px;
        }
        .auto-style15 {
            width: 654px;
        }
        .auto-style16 {
            margin-left: 9;
        }
        .auto-style17 {
            margin-bottom: 18;
        }
        .auto-style18 {
            height: 22px;
        }
        </style>

    <link href="../../Content/w3.css" rel="stylesheet" />

    <script src="../../Scripts/jquery-3.4.1.min.js"></script>

    <!--Script for datepicker https://jqueryui.com/datepicker/ -->
    <link href="../../Content/jquery-ui.css" rel="stylesheet" />
    <script src="../../Scripts/jquery-ui.js"></script>

    <link href="../../Content/bootstrap-3.0.3.min.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-3.0.3.min.js"></script>

    <!--Script for multiselect http://davidstutz.github.io/bootstrap-multiselect/#getting-started -->
    <link href="../../Content/bootstrap-multiselect.css" rel="stylesheet" />
    <script src="../../Scripts/bootstrap-multiselect.js"></script>


    <!--Script for GridView Sort Search Paging https://datatables.net/examples/index -->
    <link href="../../Scripts/table/css/addons/datatables.min.css" rel="stylesheet" />
    <script src="../../Scripts/table/js/addons/datatables.min.js"></script>

    <script>
        $(document).ready(function () {
            $('#Multiselect_Plant').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });
        $(document).ready(function () {
            $('#Multiselect_MatGroup').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });
        $(document).ready(function () {
            $('#Multiselect_StorageLo').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });
        $(document).ready(function () {
            $('#Multiselect_EqCode').multiselect({
                includeSelectAllOption: true,
                enableFiltering: true,
                enableCaseInsensitiveFiltering: true,
                numberDisplayed: 3,
                maxHeight: 300
            });
        });

        $(document).ready(function () {
            $.ajax({
                type: "POST",
                url: "StockReservationReport.aspx/GetData",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var names = response.d;
                    alert(names);
                },
                failure: function (response) {
                    alert(response.d);
                }
            });
        });
        $(function () {

            $('#btnSearc1').on('click', function () {

                alert("xxx");
                $.ajax({
                    type: "POST",
                    url: 'form/Stock/StockReservationReport/bindData',
                    data: "{}",
                    contentType: "application/json",
                    success: function (msg) {
                        msg = msg.hasOwnProperty("d") ? msg.d : msg;
                        alert(msg);
                    }
                });


            });
        });
    </script>

</head>

<body>
    <form id="form1" runat="server">
        <div   style=" padding: 20px;">
        <div>
            <table style="width: 100%;">
                <tr>
                    <td colspan="5" style="width: 100%; font-family: Tahoma; font-size: 12pt; font-weight: bold; color: #333333;">
                        <asp:ImageButton ID="ibtnHome" runat="server" Height="35px" ImageUrl="../../images/ot.png" OnClick="ibtnHome_Click" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label ID="Label21" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#FF9900" Text="EGAT"></asp:Label>
                        &nbsp;- CM (Contact Management)
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Panel ID="Panel1" runat="server" BackColor="#FF9900" Height="8pt">
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" style="width: 100%;">
                        <asp:Label ID="Label39" runat="server" Font-Names="Tahoma" Font-Size="11pt" Text="CM - Purchase Request"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
        <div style="padding-left: 10px">
            <table style="width: 100%;">
                <tr>
                    <td colspan="1" >
                        <asp:Label ID="lbWFID" runat="server"  Text="Workflow ID:"></asp:Label>
                    </td>
                    <td  colspan="3" style="width: 80%;">
                        <asp:TextBox ID="txtJobNo" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td  colspan="1" >
                        <asp:Label ID="lbJob" runat="server"  Text="งาน:"></asp:Label>
                    </td>
                    <td colspan="3" style="width: 80%">
                        <asp:TextBox ID="txtJob" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1050px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="1" >
                        <asp:Label ID="lbProject" runat="server"  Text="โครงการ:"></asp:Label>
                    </td>
                    <td colspan="3" >
                        <asp:TextBox ID="txtProject" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1050px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td colspan="5" >
                         <asp:Label ID="lbDetail" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="รายละเอียดขอซื้อหรือขอจ้าง"></asp:Label>
                        
                    </td>
                </tr>
                <tr>
                    <td colspan="1">
                        <asp:Label ID="lbOrganization" runat="server"  Text="ออกในนามหน่วยงาน:"></asp:Label>
                    </td>
                    <td colspan="4" >
                        <asp:TextBox ID="txtOrganization" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1050px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                     <td colspan="1" class="auto-style11" >
                        <asp:Label ID="lbDocumentType" runat="server"  Text="ประเภทเอกสาร:"></asp:Label>
                    </td>
                    <td class="auto-style11" >
                        <asp:DropDownList ID="ddlDocumentType" runat="server" >
                            <asp:ListItem>บันทึกขอซื้อขอจ้าง</asp:ListItem>
                                      
                                    </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                     <td >
                        <asp:Label ID="lbDocNo" runat="server"  Text="เลขที่เอกสาร"></asp:Label>
                    </td>
                    <td style="width: 20%;">
                        <asp:TextBox ID="txtDocno" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>

                    <td >
                        <asp:Label ID="lbDate" runat="server"  Text="ลงวันที่"></asp:Label>
                    </td>
                    <td  >
                        <asp:TextBox ID="txtDate" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lbFrom" runat="server"  Text="จาก:"></asp:Label>
                    </td>
                    <td colspan="3" >
                        <asp:TextBox ID="txtFrom" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbTo" runat="server"  Text="เรียน:"></asp:Label>
                    </td>
                    <td colspan="3" style="width: 75%">
                        <asp:TextBox ID="txtTo" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1050px" ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td >
                        <asp:Label ID="lbCopyTo" runat="server"  Text="สำเนาเรียน:"></asp:Label>
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="txtCopyTo" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1050px"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                    <td class="auto-style11" >
                        <asp:Label ID="Label1" runat="server"  Text="เรื่อง:"></asp:Label>
                    </td>
                    <td colspan="3" class="auto-style7">
                        <asp:TextBox ID="TextBox1" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1050px"></asp:TextBox>
                    </td>
                </tr>
                 <tr>
                     <td class="auto-style11" >
                        <asp:Label ID="Label2" runat="server"  Text="วิธีที่จะซื้อหรือจ้าง:"></asp:Label>
                    </td>
                    <td colspan="3" class="auto-style11" >
                        <asp:CheckBoxList ID="cblPurchase" runat="server" Width="928px" RepeatColumns="3">
                            <asp:ListItem Value="1">ตกลงราคา</asp:ListItem>
                            <asp:ListItem Value="2">สอบราคา</asp:ListItem>
                            <asp:ListItem Value="3">พิเศษ</asp:ListItem>
                            <asp:ListItem Value="4">ประกวดราคา</asp:ListItem>
                            <asp:ListItem Value="5">ประกวดราคา 2 ของ</asp:ListItem>
                            <asp:ListItem Value="6">ประกวดราคา 2 ของ แบบ PO</asp:ListItem>
                        </asp:CheckBoxList>
                    </td>
                </tr>

                  
             </table>
            </div>
        <div>
            <table style="width: 100%;">
                <tr>
                     <td >
                        <asp:Label ID="Label3" runat="server"  Text="ข้อมูลเพิ่มเติม" BackColor="White" ForeColor="#0066FF"></asp:Label>
                    </td>
                    
                </tr>
                <tr>
                    <td  colspan="4" class="auto-style12"   >
                        <asp:Label ID="Label4" runat="server"  Text="1.ราคากลางรวมทั้งหมดเป็นเงิน:"></asp:Label>
                    </td>
                    <td colspan="2" class="auto-style12" >
                        <asp:TextBox ID="TextBox2" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="550px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td  colspan="4">
                        <asp:Label ID="Label5" runat="server"  Text="2.กำหนดขายเอกสารประกวดราคา:"></asp:Label>
                    </td>
                    <td  colspan="2" >
                        <asp:TextBox ID="TextBox3" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1090px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td  colspan="4" class="auto-style11">
                        <asp:Label ID="Label6" runat="server"  Text="3.กำหนดรับและเปิดซองประกวดราคา:"></asp:Label>
                    </td>
                    <td  colspan="2" class="auto-style11" >
                        <asp:TextBox ID="TextBox4" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1090px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td  colspan="4" class="auto-style11">
                        <asp:Label ID="Label7" runat="server"  Text="3.กำหนดรับและเปิดซองประกวดราคา:"></asp:Label>
                    </td>
                    <td   colspan="2" class="auto-style11" >
                        <asp:TextBox ID="TextBox5" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1090px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td   colspan="4">
                        <asp:Label ID="Label8" runat="server"  Text="และซองราคา"></asp:Label>
                    </td>
                    <td   colspan="2" >
                        <asp:TextBox ID="TextBox6" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1090px"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td  colspan="4">
                        <asp:Label ID="Label9" runat="server"  Text="4.กำหนดออกหนังสือสนองรับราคา:"></asp:Label>
                    </td>
                    <td  colspan="2" >
                        <asp:TextBox ID="TextBox7" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1090px"></asp:TextBox>
                    </td>
                </tr>
                  <tr>
                     <td colspan="4" class="auto-style11">
                        <asp:Label ID="Label10" runat="server"  Text="5.กำหนดแล้วเสร็จของงาน:"></asp:Label>
                    </td>
                    <td colspan="2" class="auto-style11">
                        <asp:TextBox ID="TextBox8" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                     
                </tr>
                 <tr>
                     <td colspan="4" class="auto-style12">
                        <asp:Label ID="Label11" runat="server"  Text="กำหนดส่งของ:"></asp:Label>
                    </td>
                    <td colspan="2" class="auto-style12">
                        <asp:TextBox ID="TextBox9" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                     
                </tr>
                 <tr>
                     <td colspan="4" class="auto-style11">
                        <asp:Label ID="Label12" runat="server"  Text="หน่วยงานเจ้าของเรื่อง:"></asp:Label>
                    </td>
                    <td colspan="2" class="auto-style11">
                        <asp:TextBox ID="TextBox10" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="1090px"></asp:TextBox>
                    </td>
                     
                </tr>
                <tr>
                     <td colspan="4">
                        <asp:Label ID="Label13" runat="server"  Text="โทร:"></asp:Label>
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="TextBox11" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                    
                     
                </tr>
                <tr>
                     <td colspan="4" class="auto-style11">
                        <asp:Label ID="Label14" runat="server"  Text="โทรสาร(ถ้ามี):"></asp:Label>
                    </td>
                    <td colspan="2" class="auto-style11">
                        <asp:TextBox ID="TextBox12" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                    </td>
                </tr>
             
            </table>         
     
                        
        </div>
        <div >
              
          <asp:Button ID="btnPreViewDoc" class="w3-button w3-blue" BackColor="#164094" runat="server" Text="Preview Document" />
                       
            
        </div>

        <div>
            <asp:Panel ID="Panel2" runat="server"  >
                <table style="width: 100%;">
                    <tr>
                        <td class="auto-style18">

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="Attachments"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <asp:Button ID="Button1" class="w3-button w3-blue" BackColor="#164094" runat="server" Text="Upload File" />
                             &nbsp;&nbsp;
                             <asp:Button ID="Button2" class="w3-button w3-blue" BackColor="#164094" runat="server" Text="Attache File from OTCS" Width="253px" />
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                   
                    </table>
                     <asp:GridView ID="gvAttachments" runat="server" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3"
                                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." >
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <HeaderStyle BackColor="#164094" Font-Bold="True" ForeColor="White" Height="30px" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                    <Columns>
                        
                        <asp:TemplateField HeaderText="No.">
                            <ItemTemplate>
                                <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("No") %>'></asp:Label>
                                <%--Text='<%# Bind("job_no") %>'--%>
                            </ItemTemplate>
                            <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Document Type">
                            <ItemTemplate>
                                <asp:Label ID="lbDocType" runat="server" Text='<%# Bind("DocType") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="Document No.">
                            <ItemTemplate>
                                <asp:Label ID="lbDocNo" runat="server" Text='<%# Bind("DocNo") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="Description">
                            <ItemTemplate>
                                <asp:Label ID="lbDescription" runat="server" Text='<%# Bind("Description") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="View Doc">
                            <ItemTemplate>
                                <asp:Label ID="lbViewDoc" runat="server" Text='<%# Bind("ViewDoc") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Status">
                            <ItemTemplate>
                                <asp:Label ID="lbViewDoc" runat="server" Text='<%# Bind("Status") %>'></asp:Label>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                       <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server"  ImageAlign="Right" ImageUrl="~/images/bin.png" Height="20px" Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="25px" HorizontalAlign="center" VerticalAlign="Top" />
                            </asp:TemplateField>
                       
              
                    </Columns>
                </asp:GridView>
              
               
                     
                
            </asp:Panel>
        </div>
        
         <div>
            <asp:Panel ID="Panel3" runat="server"  >
                <table>
                    <tr>
                        <td>

                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label16" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="เสนอเพื่อทบทวนร่าง"></asp:Label>
                        </td>
                    </tr>
                    </table>
                   <asp:GridView ID="gvOffer" runat="server" ShowFooter="true" Width="100%" BackColor="White" BorderColor="#CCCCCC" BorderStyle="None"  BorderWidth="1px" CellPadding="3"
                                                AutoGenerateColumns="False" Font-Names="tahoma" Font-Size="9pt" EmptyDataText="No data found." GridLines="None" >
                    
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" Font-Names="tahoma" Font-Size="10pt" />
                    <SortedAscendingCellStyle BackColor="#F1F1F1" />
                    <SortedAscendingHeaderStyle BackColor="#007DBB" />
                    <SortedDescendingCellStyle BackColor="#CAC9C9" />
                    <SortedDescendingHeaderStyle BackColor="#00547E" />
                    <%--<emptydatatemplate>No data found</emptydatatemplate>--%>
                    <EmptyDataRowStyle BorderStyle="Solid" HorizontalAlign="Center" />
                    <Columns>
                        
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:Label ID="lbPlant" runat="server" Text='<%# Bind("No") %>'></asp:Label>
                                <%--Text='<%# Bind("job_no") %>'--%>
                            </ItemTemplate>
                            <ItemStyle Width="30px" HorizontalAlign="Center" VerticalAlign="Top" />
                            <FooterStyle HorizontalAlign="Right" />
                             <FooterTemplate>
                                     <asp:ImageButton ID="ImageButton1" runat="server"  ImageAlign="Right" ImageUrl="~/images/add.png" Height="20px" Width="20px" />
                            </FooterTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="">
                            <ItemTemplate>
                                <asp:DropDownList ID="DropDownList1" runat="server" >
                                    <asp:ListItem>อนุมัติ</asp:ListItem>
                                     <asp:ListItem>ไม่อนุมัติ</asp:ListItem>
                                </asp:DropDownList>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                           <asp:TemplateField HeaderText="ชื่อ - นามสกุล"  ItemStyle-HorizontalAlign="Right">
                            <ItemTemplate>
                                <asp:TextBox ID="TextBox12" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="700px"></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField HeaderText="ต่ำแหน่ง">
                            <ItemTemplate>
                                  <asp:TextBox ID="TextBox12" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="300px"></asp:TextBox>
                            </ItemTemplate>
                            <ItemStyle Width="150px" HorizontalAlign="Center" VerticalAlign="Top" />
                        </asp:TemplateField>
                         <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server"  ImageAlign="Right" ImageUrl="~/images/search.png" Height="20px" Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="25px" HorizontalAlign="center" VerticalAlign="Top" />
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImageButton1" runat="server"  ImageAlign="Right" ImageUrl="~/images/bin.png" Height="20px" Width="20px" />
                                </ItemTemplate>
                                <ItemStyle Width="25px" HorizontalAlign="center" VerticalAlign="Top" />
      
                            </asp:TemplateField>
                      
                    </Columns>
                       
                </asp:GridView>
                        
                 <table style="width: 100%;">
                    <tr>
                        <td  style="width:5%;">

                        </td>
                        <td style="width:15%;">
                             <asp:Label ID="Label17" runat="server"  Text="Requester:"></asp:Label>
                        </td>
                         <td class="auto-style15">
                              <asp:TextBox ID="TextBox14" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="636px"></asp:TextBox>
                        </td>
                         <td>
                             <asp:Label ID="Label18" runat="server"  Text="Position:"></asp:Label>
                        </td>
                         <td>
                             <asp:TextBox ID="TextBox15" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="291px" CssClass="auto-style16"></asp:TextBox>
                        </td>
                    </tr>
                    </table>
            </asp:Panel>
        </div>
          <div>
                <asp:Panel ID="Panel4" runat="server"  >
                <table style="width: 100%;">
              <tr>
                  <td>

                  </td>
              </tr>
                <tr>
                    <td   style="width: 25%;" class="auto-style14"  rowspan="3">
                         <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Names="tahoma" Font-Size="12pt" ForeColor="#0066FF" Text="Remark"></asp:Label>
                        
                    </td>
                    <td  style="width: 75%;" rowspan="3">
                        <asp:TextBox ID="TextBox13" runat="server"  BorderStyle="Solid" BorderColor="#D8D8D8" Width="955px" TextMode="MultiLine" CssClass="auto-style17"></asp:TextBox>
                    </td>
                </tr>
              
                

                  
             </table>
               
                        
                
            </asp:Panel>
           </div>

              <div>
                <asp:Panel ID="Panel5" runat="server"  >
                <table style="width: 100%;">
              <tr>
                  <td class="auto-style18">

                  </td>
              </tr>
                <tr>
                    <td   style="width: 75%;" class="auto-style14"  rowspan="3">
                        
                    </td>
                    <td  style="width: 25%;" rowspan="3">
                        
                        <asp:Button ID="Button3" runat="server" BackColor="#164094" class="w3-button w3-blue" Text="Save" />
                        &nbsp;&nbsp;
                        <asp:Button ID="Button4" runat="server" BackColor="#164094" class="w3-button w3-blue" Text="Submit" />
                        &nbsp;&nbsp;
                        <asp:Button ID="Button5" runat="server" BackColor="#164094" class="w3-button w3-blue" Text="Cancel" />
                        
                    </td>
                </tr>
              
                

                  
             </table>
               
                        
                
            </asp:Panel>
           </div>
        
        </div>
    </form>
    
</body>
</html>

